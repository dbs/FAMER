/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.labeledFile.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;

/**
 * Maps a line from the ground truth file to a {@code Tuple2<String, String>}
 */
public class MapLabeledFileLineToTuple implements FlatMapFunction<String, Tuple3<String, String, Boolean>> {

  /**
   * Tokenizer used to split a ground truth line
   */
  private final String splitter;

  /**
   * The label string in golden truth file for matched pairs
   */
  private final String matchLabel;

  /**
   * Reduce object instantiation
   */
  private final Tuple3<String, String, Boolean> reuseTuple;

  /**
   * Creates an instance of MapLabeledFileLineToTuple
   *
   * @param splitter Tokenizer used to split a ground truth line
   * @param matchLabel The match label in golden truth file
   */
  public MapLabeledFileLineToTuple(String splitter, String matchLabel) {
    this.splitter = splitter;
    this.matchLabel = matchLabel;
    this.reuseTuple = new Tuple3<>();
  }

  @Override
  public void flatMap(String line, Collector<Tuple3<String, String, Boolean>> collector) throws Exception {
    String[] split = line.split(splitter);
    if (split.length == 3) {
      reuseTuple.f0 = split[0].replace("\"", "");
      reuseTuple.f1 = split[1].replace("\"", "");
      reuseTuple.f2 = split[2].equals(matchLabel);
      collector.collect(reuseTuple);
    }
  }
}
