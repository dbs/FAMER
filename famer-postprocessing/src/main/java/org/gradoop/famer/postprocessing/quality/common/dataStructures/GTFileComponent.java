/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.common.dataStructures;

import org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQualityWithGTFile;
import org.gradoop.famer.postprocessing.quality.similarityGraph.SimilarityGraphQualityWithGTFile;

/**
 * The golden truth file component which is used by {@link ClusteringQualityWithGTFile} and
 * {@link SimilarityGraphQualityWithGTFile }
 */
public class GTFileComponent {
  /**
   * The golden truth file path
   */
  private final String groundTruthFilePath;
  /**
   * Tokenizer used to split a ground truth line
   */
  private final String splitter;
  /**
   * The type of golden truth file
   */
  private final GoldenTruthFileType goldenTruthFileType;

  /**
   * Creates an instance of GTFileComponent and initializes the properties
   *
   * @param goldenTruthFilepath The golden truth file path
   * @param splitter            The splitter character
   * @param goldenTruthFileType The type of golden truth file
   */
  public GTFileComponent(String goldenTruthFilepath, String splitter,
    GoldenTruthFileType goldenTruthFileType) {
    this.groundTruthFilePath = goldenTruthFilepath.trim();
    this.splitter = splitter;
    this.goldenTruthFileType = goldenTruthFileType;
  }

  public String getGroundTruthFilePath() {
    return groundTruthFilePath;
  }

  public String getSplitter() {
    return splitter;
  }

  public GoldenTruthFileType getGoldenTruthFileType() {
    return goldenTruthFileType;
  }
}
