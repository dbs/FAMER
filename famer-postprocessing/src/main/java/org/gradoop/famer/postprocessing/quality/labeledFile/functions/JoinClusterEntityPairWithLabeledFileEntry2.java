/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.labeledFile.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;

/**
 * Joins the result of {@link JoinClusterEntityPairWithLabeledFileEntry1} with a pair of clusterId,entityId
 * again where the entityId matches.
 */
public class JoinClusterEntityPairWithLabeledFileEntry2 implements
  JoinFunction<Tuple4<String, String, String, Boolean>, Tuple2<String, String>,
    Tuple5<String, String, String, String, Boolean>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple5<String, String, String, String, Boolean> reuseTuple = new Tuple5<>();

  @Override
  public Tuple5<String, String, String, String, Boolean> join(
    Tuple4<String, String, String, Boolean> in1, Tuple2<String, String> in2) throws Exception {
    if (in1.f1.compareTo(in2.f1) < 0) {
      reuseTuple.f0 = in1.f0;
      reuseTuple.f1 = in1.f1;
      reuseTuple.f2 = in2.f0;
      reuseTuple.f3 = in2.f1;
    } else {
      reuseTuple.f0 = in2.f0;
      reuseTuple.f1 = in2.f1;
      reuseTuple.f2 = in1.f0;
      reuseTuple.f3 = in1.f1;
    }
    reuseTuple.f4 = in1.f3;

    return reuseTuple;
  }
}
