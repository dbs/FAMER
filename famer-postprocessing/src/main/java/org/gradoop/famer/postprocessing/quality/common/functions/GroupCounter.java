/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.common.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.util.Collector;

/**
 * Counts and returns the number of elements of a group in a grouped DataSet
 *
 * @param <T> Type of group elements
 */
public class GroupCounter<T> implements GroupReduceFunction<T, Tuple1<Long>> {

  @Override
  public void reduce(Iterable<T> group, Collector<Tuple1<Long>> out) throws Exception {
    long cnt = 0;
    for (T ignored : group) {
      cnt++;
    }
    out.collect(Tuple1.of(cnt));
  }
}
