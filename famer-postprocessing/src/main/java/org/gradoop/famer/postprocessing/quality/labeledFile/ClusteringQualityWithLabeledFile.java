/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.labeledFile;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.MapVertexToClusterIdAndPropertyValue;
import org.gradoop.famer.postprocessing.quality.common.functions.CheckElementForExistingProperty;
import org.gradoop.famer.postprocessing.quality.common.functions.ValueAnnotator;
import org.gradoop.famer.postprocessing.quality.labeledFile.functions.JoinClusterEntityPairWithLabeledFileEntry1;
import org.gradoop.famer.postprocessing.quality.labeledFile.functions.JoinClusterEntityPairWithLabeledFileEntry2;
import org.gradoop.famer.postprocessing.quality.labeledFile.functions.MapLabeledFileLineToTuple;
import org.gradoop.flink.model.impl.functions.tuple.ObjectTo1;
import org.gradoop.flink.model.impl.operators.count.Count;

/**
 * Computes Precision, Recall, and F-Measure of the input clustered vertices using the given labeled file.
 * It is used when there is a partial Golden Truth file containing pairs of entity ids along with a
 * match/non-match label
 */
public class ClusteringQualityWithLabeledFile extends AbstractPartialQuality {
  /**
   * Creates an instance of ClusteringQualityWithLabeledFile
   *
   * @param clusteredVertices The input clustered vertices
   * @param goldenTruthFilePath The path of partial golden truth file
   * @param splitter The tokenizer for splitting the golden truth file line
   * @param entityIdProperty The property key for the entity id
   * @param matchLabel The match label in golden truth file
   */
  public ClusteringQualityWithLabeledFile(DataSet<EPGMVertex> clusteredVertices, String goldenTruthFilePath,
    String splitter, String entityIdProperty, String matchLabel) throws Exception {
    super(clusteredVertices, goldenTruthFilePath, splitter, entityIdProperty, matchLabel);
  }

  @Override
  public void computeQuality() throws Exception {
    clusteredVertices = clusteredVertices.map(new CheckElementForExistingProperty<>(entityIdProperty));
    computeValues();
    assignValues();
  }

  /**
   * Computes all values
   */
  private void computeValues() {
    ExecutionEnvironment env = clusteredVertices.getExecutionEnvironment();

    DataSet<Tuple3<String, String, Boolean>> labeledPairs = env.readTextFile(labeledDataFilePath)
      .flatMap(new MapLabeledFileLineToTuple(splitter, matchLabel));
    DataSet<Tuple2<String, String>> clusterIdEntityId =
      clusteredVertices.flatMap(new MapVertexToClusterIdAndPropertyValue(entityIdProperty));

    DataSet<Tuple3<String, String, Boolean>> clusteredPairs = clusterIdEntityId.join(labeledPairs)
      .where(1).equalTo(0)
      .with(new JoinClusterEntityPairWithLabeledFileEntry1())
      .join(clusterIdEntityId)
      .where(2).equalTo(1)
      .with(new JoinClusterEntityPairWithLabeledFileEntry2())
      .distinct(1, 3).map(
        (MapFunction<Tuple5<String, String, String, String, Boolean>, Tuple3<String, String, Boolean>>) in ->
          Tuple3.of(in.f0, in.f2, in.f4))
      .returns(new TypeHint<Tuple3<String, String, Boolean>>() { });

    truePositivesSet = Count.count(clusteredPairs
      .filter((FilterFunction<Tuple3<String, String, Boolean>>) in -> in.f0.equals(in.f1) && in.f2))
      .map(new ObjectTo1<>()).map(new ValueAnnotator(TP_FLAG));

    falsePositivesSet = Count.count(clusteredPairs
      .filter((FilterFunction<Tuple3<String, String, Boolean>>) in -> in.f0.equals(in.f1) && !in.f2))
      .map(new ObjectTo1<>()).map(new ValueAnnotator(FP_FLAG));

    gtRecordNoSet = Count.count(labeledPairs
      .filter((FilterFunction<Tuple3<String, String, Boolean>>) in -> in.f2))
      .map(new ObjectTo1<>()).map(new ValueAnnotator(GT_FLAG));
  }
}
