/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.GetPairedVertexIdsWithGoldenTruthIdMatch;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.GetRepetitionForTPandAP;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.MapVertexToClusterIdAndPropertyValue;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.functions.MapVertexToIdAndClusterIdAndPropertyValue;
import org.gradoop.famer.postprocessing.quality.common.QualityComputation;
import org.gradoop.famer.postprocessing.quality.common.functions.CombinationCounter;
import org.gradoop.famer.postprocessing.quality.common.functions.GroupCounter;
import org.gradoop.famer.postprocessing.quality.common.functions.MapVertexToPropertyValue;
import org.gradoop.famer.postprocessing.quality.common.functions.ValueAnnotator;
import org.gradoop.flink.model.impl.functions.epgm.ByProperty;
import org.gradoop.flink.model.impl.functions.tuple.ObjectTo1;
import org.gradoop.flink.model.impl.functions.tuple.Project2To0;
import org.gradoop.flink.model.impl.functions.tuple.Project2To1;
import org.gradoop.flink.model.impl.operators.count.Count;

/**
 * Abstract class all classes for quality measuring on clustered graphs are inheriting from.
 */
public abstract class AbstractClusteringQuality implements QualityComputation {

  /**
   * Flag to mark tuples with the value for true positives
   */
  protected static final String TP_FLAG = "tp";
  /**
   * Flag to mark tuples with the value for all positives
   */
  protected static final String AP_FLAG = "ap";
  /**
   * Flag to mark tuples with the value for ground truth record numbers
   */
  protected static final String GT_FLAG = "gt";
  /**
   * Flag to mark tuples with the value for repaired all positives
   */
  protected static final String REP_AP_FLAG = "repAP";
  /**
   * Flag to mark tuples with the value for repaired true positives
   */
  protected static final String REP_TP_FLAG = "repTP";
  /**
   * Flag to mark tuples with the value for max cluster size
   */
  protected static final String MAX_CS_FLAG = "maxCs";
  /**
   * Flag to mark tuples with the value for min cluster size
   */
  protected static final String MIN_CS_FLAG = "minCs";
  /**
   * Flag to mark tuples with the value for cluster size
   */
  protected static final String SUM_CS_FLAG = "sumCs";
  /**
   * Flag to mark tuples with the value for cluster number
   */
  protected static final String CN_FLAG = "cn";
  /**
   * Flag to mark tuples with the value for singleton cluster number
   */
  protected static final String SINGLE_FLAG = "single";
  /**
   * Whether clusters are overlapping
   */
  protected final boolean hasOverlap;
  /**
   * The input clustered vertices
   */
  protected DataSet<EPGMVertex> clusteredVertices;
  /**
   * The mandatory property key for the id of the real world entity equivalent (golden truth) to a vertex
   */
  protected String goldenTruthIdProperty;

  /**
   * DataSet with {@code Tuple2<TP_FLAG, true positives count>}
   */
  protected DataSet<Tuple2<String, Long>> truePositivesSet;
  /**
   * DataSet with {@code Tuple2<AP_FLAG, all positives count>}
   */
  protected DataSet<Tuple2<String, Long>> allPositivesSet;
  /**
   * DataSet with {@code Tuple2<GT_FLAG, ground truth record number count>}
   */
  protected DataSet<Tuple2<String, Long>> gtRecordNoSet;
  /**
   * DataSet with {@code Tuple2<SUM_CS_FLAG, cluster size>}
   */
  protected DataSet<Tuple2<String, Long>> sumClusterSizeSet;
  /**
   * DataSet with {@code Tuple2<CN_FLAG, clusters count>}
   */
  protected DataSet<Tuple2<String, Long>> clusterNoSet;
  /**
   * DataSet with {@code Tuple2<MIN_CS_FLAG, min cluster size>}
   */
  protected DataSet<Tuple2<String, Long>> minClusterSizeSet;
  /**
   * DataSet with {@code Tuple2<MAX_CS_FLAG, max cluster size>}
   */
  protected DataSet<Tuple2<String, Long>> maxClusterSizeSet;
  /**
   * DataSet with {@code Tuple2<SINGLE_FLAG, singleton clusters count>}
   */
  protected DataSet<Tuple2<String, Long>> singletonSet;
  /**
   * DataSet with {@code Tuple2<REP_AP_FLAG, repaired all positives count>}
   */
  protected DataSet<Tuple2<String, Long>> repAllPositivesSet;
  /**
   * DataSet with {@code Tuple2<REP_TP_FLAG, repaired true positives count>}
   */
  protected DataSet<Tuple2<String, Long>> repTruePositivesSet;

  /**
   * Number of true positives
   */
  protected long truePositives;
  /**
   * Number of all positives
   */
  protected long allPositives;
  /**
   * Number of ground truth records
   */
  protected long gtRecordNo;
  /**
   * Number of clusters
   */
  protected long clusterNo;
  /**
   * Cluster size
   */
  protected long sumClusterSize;
  /**
   * Min cluster size
   */
  protected long minClusterSize;
  /**
   * Max cluster size
   */
  protected long maxClusterSize;
  /**
   * Number of singleton clusters
   */
  protected long singletons;
  /**
   * Number of repaired all positives
   */
  protected long repAllPositiveNo;
  /**
   * Number of repaired true positives
   */
  protected long repTruePositiveNo;

  /**
   * Creates an instance of AbstractClusteringQuality and initializes sets and values
   *
   * @param clusteredVertices The clustered vertices
   * @param hasOverlap        Whether clusters are overlapping
   */
  public AbstractClusteringQuality(DataSet<EPGMVertex> clusteredVertices, boolean hasOverlap) {
    this(clusteredVertices, hasOverlap, "gtId");
  }

  /**
   * Creates an instance of AbstractClusteringQuality and initializes sets and values
   *
   * @param clusteredVertices  The clustered vertices
   * @param hasOverlap         Whether clusters are overlapping
   * @param goldenTruthIdProperty The mandatory property key for the id of the real world entity equivalent
   *                             (golden truth) to a vertex
   */
  public AbstractClusteringQuality(DataSet<EPGMVertex> clusteredVertices, boolean hasOverlap,
    String goldenTruthIdProperty) {
    this.clusteredVertices = clusteredVertices;
    this.goldenTruthIdProperty = goldenTruthIdProperty;
    this.hasOverlap = hasOverlap;
  }

  /**
   * @return Number of true positives
   * @throws Exception thrown if value computation fails
   */
  public long getTruePositives() throws Exception {
    return truePositives;
  }

  /**
   * @return Number of all positives
   * @throws Exception thrown if value computation fails
   */
  public long getAllPositives() throws Exception {
    return allPositives;
  }

  /**
   * @return Number of ground truth records
   * @throws Exception thrown if value computation fails
   */
  public long getGtRecordNo() throws Exception {
    return gtRecordNo;
  }

  /**
   * @return Number of clusters
   * @throws Exception thrown if value computation fails
   */
  public long getClusterNo() throws Exception {
    return clusterNo;
  }

  /**
   * @return precision
   * @throws Exception thrown if value computation fails
   */
  public double computePrecision() throws Exception {
    double precision = (double) truePositives / allPositives;
    return Double.isNaN(precision) ? 0d : precision;
  }

  /**
   * @return recall
   * @throws Exception thrown if value computation fails
   */
  public double computeRecall() throws Exception {
    double recall = (double) truePositives / gtRecordNo;
    return Double.isNaN(recall) ? 0d : recall;
  }

  /**
   * @return f-measure
   * @throws Exception thrown if value computation fails
   */
  public double computeFMeasure() throws Exception {
    double pr = computePrecision();
    double re = computeRecall();
    double fMeasure = 2d * pr * re / (pr + re);
    return Double.isNaN(fMeasure) ? 0d : fMeasure;
  }

  /**
   * @return Min cluster size (bigger than 1)
   * @throws Exception thrown if value computation fails
   */
  public long getMinClusterSize() throws Exception {
    return minClusterSize;
  }

  /**
   * @return Max cluster size
   * @throws Exception thrown if value computation fails
   */
  public long getMaxClusterSize() throws Exception {
    return maxClusterSize;
  }

  /**
   * @return Average cluster size
   * @throws Exception thrown if value computation fails
   */
  public double getAverageClusterSize() throws Exception {
    return (double) sumClusterSize / clusterNo;
  }

  /**
   * Returns the number of singleton clusters, calls {@link #} if necessary
   *
   * @return The number of singleton clusters
   * @throws Exception thrown if value computation fails
   */
  public long getSingletons() throws Exception {
    return singletons;
  }

  /**
   * Computes all values, must be implemented by inheriting classes
   */
  protected abstract void computeValues() throws Exception;

  /**
   * Computes the values and assigns them in the sets.
   */
  protected void computeValuesCommon() {
    clusteredVertices = clusteredVertices.filter(new ByProperty<>(goldenTruthIdProperty));

    gtRecordNoSet = clusteredVertices.map(new MapVertexToPropertyValue(goldenTruthIdProperty))
      .groupBy(0)
      .reduceGroup(new GroupCounter<>())
      .map(new CombinationCounter()).aggregate(Aggregations.SUM, 0)
      .map(new ValueAnnotator(GT_FLAG));

    DataSet<Tuple2<String, String>> clusterIdGoldenTruthId =
      clusteredVertices.flatMap(new MapVertexToClusterIdAndPropertyValue(goldenTruthIdProperty));

    DataSet<Tuple1<Long>> clusterSizes = clusterIdGoldenTruthId.groupBy(0).reduceGroup(new GroupCounter<>());

    allPositivesSet = clusterSizes.map(new CombinationCounter()).aggregate(Aggregations.SUM, 0)
      .map(new ValueAnnotator(AP_FLAG));

    truePositivesSet = clusterIdGoldenTruthId.groupBy(0, 1).reduceGroup(new GroupCounter<>())
      .map(new CombinationCounter()).aggregate(Aggregations.SUM, 0).map(new ValueAnnotator(TP_FLAG));

    singletonSet = clusterSizes.filter((FilterFunction<Tuple1<Long>>) size -> size.f0 == 1)
      .aggregate(Aggregations.SUM, 0).map(new ValueAnnotator(SINGLE_FLAG));

    maxClusterSizeSet = clusterSizes.aggregate(Aggregations.MAX, 0).map(new ValueAnnotator(MAX_CS_FLAG));

    minClusterSizeSet = clusterSizes.filter((FilterFunction<Tuple1<Long>>) size -> size.f0 > 1).
      aggregate(Aggregations.MIN, 0).map(new ValueAnnotator(MIN_CS_FLAG));

    sumClusterSizeSet = clusterSizes.aggregate(Aggregations.SUM, 0).map(new ValueAnnotator(SUM_CS_FLAG));

    clusterNoSet = Count.count(clusterSizes).map(new ObjectTo1<>()).map(new ValueAnnotator(CN_FLAG));

    if (hasOverlap) {
      countRepetitivePairs();
    }
  }

  /**
   * In case of having overlapping clusters, count the number of repetitive true positives and repetitive
   * all positives
   */
  private void countRepetitivePairs() {
    DataSet<Tuple3<String, String, String>> vertexIdClusterIdGoldenTruthId =
      clusteredVertices.flatMap(new MapVertexToIdAndClusterIdAndPropertyValue(goldenTruthIdProperty));
    DataSet<Tuple2<String, Boolean>> pairedVertexIdsIsMatch =
      vertexIdClusterIdGoldenTruthId.groupBy(1).reduceGroup(new GetPairedVertexIdsWithGoldenTruthIdMatch());

    DataSet<Tuple2<Long, Long>> tpRepAPRep =
      pairedVertexIdsIsMatch.groupBy(0).reduceGroup(new GetRepetitionForTPandAP());

    repTruePositivesSet = tpRepAPRep.map(new Project2To0<>()).aggregate(Aggregations.SUM, 0)
      .map(new ValueAnnotator(REP_TP_FLAG));

    repAllPositivesSet = tpRepAPRep.map(new Project2To1<>()).aggregate(Aggregations.SUM, 0)
      .map(new ValueAnnotator(REP_AP_FLAG));
  }

  /**
   * Collects the values from the corresponding sets and assigns them to the values
   */
  protected void assignValues() throws Exception {
    DataSet<Tuple2<String, Long>> sets = truePositivesSet.union(allPositivesSet).union(gtRecordNoSet)
      .union(maxClusterSizeSet).union(minClusterSizeSet).union(clusterNoSet).union(sumClusterSizeSet)
      .union(singletonSet);

    if (hasOverlap) {
      sets = sets.union(repAllPositivesSet).union(repTruePositivesSet);
    }

    for (Tuple2<String, Long> setCountTuple : sets.collect()) {
      switch (setCountTuple.f0) {
      case AP_FLAG:
        allPositives = setCountTuple.f1;
        break;
      case TP_FLAG:
        truePositives = setCountTuple.f1;
        break;
      case GT_FLAG:
        gtRecordNo = setCountTuple.f1;
        break;
      case REP_AP_FLAG:
        repAllPositiveNo = setCountTuple.f1;
        break;
      case REP_TP_FLAG:
        repTruePositiveNo = setCountTuple.f1;
        break;
      case MAX_CS_FLAG:
        maxClusterSize = setCountTuple.f1;
        break;
      case MIN_CS_FLAG:
        minClusterSize = setCountTuple.f1;
        break;
      case SUM_CS_FLAG:
        sumClusterSize = setCountTuple.f1;
        break;
      case CN_FLAG:
        clusterNo = setCountTuple.f1;
        break;
      case SINGLE_FLAG:
        singletons = setCountTuple.f1;
        break;
      default:
        break;
      }
    }
    if (hasOverlap) {
      allPositives -= repAllPositiveNo;
      truePositives -= repTruePositiveNo;
    }
  }
}
