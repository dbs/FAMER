/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.functions.CheckElementForExistingProperty;
import org.gradoop.famer.postprocessing.quality.common.functions.GTIDAssigner;
import org.gradoop.famer.postprocessing.quality.common.functions.MapGroundTruthLineToTuple;
import org.gradoop.famer.postprocessing.quality.common.functions.MapVertexToVertexPropertyValue;
import org.gradoop.famer.postprocessing.quality.common.functions.RepresentativeIdAssigner;
import org.gradoop.famer.postprocessing.quality.common.functions.RepresentativeIdFinder;

/**
 * Computes Precision, Recall, F-Measure of the input clustered vertices as well as some clustering
 * statistics.
 * It is used when there is a Golden Truth file.
 */
public class ClusteringQualityWithGTFile extends AbstractClusteringQuality {

  /**
   * Property key for the entity id
   */
  private final String entityIdProperty;

  /**
   * The object that holds the golden truth file specifications
   */
  private final GTFileComponent gtFileComponent;

  /**
   * Creates an instance of ClusteringQualityWithGTFile
   *
   * @param clusteredVertices The input clustered vertices that are going to be evaluated
   * @param gtFileComponent   The object containing the golden truth file specifications
   * @param entityIdProperty  Property key for the entity id
   * @param hasOverlap        Whether clusters are overlapping
   */
  public ClusteringQualityWithGTFile(DataSet<EPGMVertex> clusteredVertices, GTFileComponent gtFileComponent,
    String entityIdProperty, boolean hasOverlap) {
    super(clusteredVertices, hasOverlap);
    this.gtFileComponent = gtFileComponent;
    this.entityIdProperty = entityIdProperty;
  }

  @Override
  public void computeQuality() throws Exception {
    clusteredVertices = clusteredVertices.map(new CheckElementForExistingProperty<>(entityIdProperty));
    computeValues();
    assignValues();
  }

  @Override
  protected void computeValues() {
    ExecutionEnvironment env = clusteredVertices.getExecutionEnvironment();

    DataSet<Tuple2<String, String>> groundTruthPairs =
      env.readTextFile(gtFileComponent.getGroundTruthFilePath())
        .flatMap(new MapGroundTruthLineToTuple(gtFileComponent.getSplitter()));

    DataSet<Tuple2<String, String>> entityIdRepresentativeId;

    switch (gtFileComponent.getGoldenTruthFileType()) {
    case MATCHED_PAIRS:
      entityIdRepresentativeId = groundTruthPairs.flatMap(new RepresentativeIdFinder())
        .groupBy(0)
        .reduceGroup(new RepresentativeIdAssigner());
      break;
    case ENTITY_ID_GT_ID_PAIR:
      entityIdRepresentativeId = groundTruthPairs;
      break;
    default:
      throw new IllegalArgumentException(
        "ClusteringQualityWithGTFile: The golden truth file type is not supported!");
    }

    DataSet<Tuple2<EPGMVertex, String>> vertexEntityId =
      clusteredVertices.map(new MapVertexToVertexPropertyValue(entityIdProperty));

    clusteredVertices = vertexEntityId.leftOuterJoin(entityIdRepresentativeId)
      .where(1).equalTo(0)
      .with(new GTIDAssigner(entityIdProperty));

    computeValuesCommon();
  }
}
