/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.similarityGraph.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Maps a vertex pair {@code Tuple2<EPGMVertex, EPGMVertex>} to a {@code Tuple2<PropertyValue, PropertyValue>}
 * with the property value to extract from the vertex given by {@link #propertyKey}.
 */
public class MapVertexPairToPropertyValuePair implements
  MapFunction<Tuple2<EPGMVertex, EPGMVertex>, Tuple2<String, String>> {

  /**
   * The key for the property value
   */
  private final String propertyKey;

  /**
   * Creates an instance of MapVertexPairToPropertyValuePair
   *
   * @param propertyKey The key for the property value
   */
  public MapVertexPairToPropertyValuePair(String propertyKey) {
    this.propertyKey = propertyKey;
  }

  @Override
  public Tuple2<String, String> map(Tuple2<EPGMVertex, EPGMVertex> in) {
    return Tuple2.of(
      in.f0.getPropertyValue(propertyKey).toString(), in.f1.getPropertyValue(propertyKey).toString());
  }
}
