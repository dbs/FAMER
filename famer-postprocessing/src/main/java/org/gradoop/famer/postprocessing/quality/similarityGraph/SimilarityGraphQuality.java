/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.similarityGraph;

import org.apache.flink.api.java.DataSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.postprocessing.quality.common.functions.CheckElementForExistingProperty;

/**
 * Computes precision, recall, and f-measure of the input similarity graph. It does not consider transitive
 * closure. Number of positives (true positives + false positives) is the number of edges.
 * It is used when there is no Golden Truth file and only vertices with the same golden truth ids are true
 * positives.
 */
public class SimilarityGraphQuality extends AbstractSimilarityGraphQuality {

  /**
   * Creates an instance of SimilarityGraphQuality
   *
   * @param vertices Vertices of the similarity graph
   * @param edges Edges of the similarity graph
   * @param goldenTruthIdProperty The mandatory property key for the id of the real world entity equivalent
   *                             (golden truth) to a vertex
   */
  public SimilarityGraphQuality(DataSet<EPGMVertex> vertices, DataSet<EPGMEdge> edges,
    String goldenTruthIdProperty) {
    super(vertices, edges, goldenTruthIdProperty);
  }

  @Override
  public void computeQuality() throws Exception {
    vertices = vertices.map(new CheckElementForExistingProperty<>(goldenTruthIdProperty));
    computeValues();
    assignValues();
  }

  @Override
  protected void computeValues() {
    computeValuesCommon();
  }
}
