/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.similarityGraph;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.aggregation.Aggregations;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.postprocessing.quality.common.QualityComputation;
import org.gradoop.famer.postprocessing.quality.common.functions.CombinationCounter;
import org.gradoop.famer.postprocessing.quality.common.functions.GroupCounter;
import org.gradoop.famer.postprocessing.quality.common.functions.MapVertexToPropertyValue;
import org.gradoop.famer.postprocessing.quality.common.functions.ValueAnnotator;
import org.gradoop.famer.postprocessing.quality.similarityGraph.functions.MapVertexPairToPropertyValuePair;
import org.gradoop.famer.postprocessing.quality.similarityGraph.functions.Project3To1And2;
import org.gradoop.flink.model.impl.functions.epgm.ByProperty;
import org.gradoop.flink.model.impl.functions.tuple.ObjectTo1;
import org.gradoop.flink.model.impl.operators.count.Count;

/**
 * Abstract class all classes for quality measuring on similarity graphs are inheriting from.
 */
public abstract class AbstractSimilarityGraphQuality implements QualityComputation {
  /**
   * Flag to mark tuples with the value for true positives
   */
  protected static final String TP_FLAG = "tp";
  /**
   * Flag to mark tuples with the value for all positives
   */
  protected static final String AP_FLAG = "ap";
  /**
   * Flag to mark tuples with the value for ground truth
   */
  protected static final String GT_FLAG = "gt";
  /**
   * Vertices of the similarity graph
   */
  protected DataSet<EPGMVertex> vertices;
  /**
   * Edges of the similarity graph
   */
  protected DataSet<EPGMEdge> edges;
  /**
   * The mandatory property key for the id of the real world entity equivalent (golden truth) to a vertex
   */
  protected String goldenTruthIdProperty;
  /**
   * DataSet with {@code Tuple2<TP_FLAG, true positives count>}
   */
  protected DataSet<Tuple2<String, Long>> truePositivesSet;
  /**
   * DataSet with {@code Tuple2<AP_FLAG, all positives count>}
   */
  protected DataSet<Tuple2<String, Long>> allPositivesSet;
  /**
   * DataSet with {@code Tuple2<GT_FLAG, ground truth record number count>}
   */
  protected DataSet<Tuple2<String, Long>> gtRecordNoSet;
  /**
   * Number of true positives
   */
  protected long truePositives;
  /**
   * Number of all positives
   */
  protected long allPositives;
  /**
   * Number of ground truth records
   */
  protected long gtRecordNo;

  /**
   * Creates an instance of AbstractSimilarityGraphQuality
   *
   * @param vertices Vertices of the similarity graph
   * @param edges    Edges of the similarity graph
   */
  public AbstractSimilarityGraphQuality(DataSet<EPGMVertex> vertices, DataSet<EPGMEdge> edges) {
    this(vertices, edges, "gtId");
  }

  /**
   * Creates an instance of AbstractSimilarityGraphQuality and initializes sets and values
   *
   * @param vertices           Vertices of the similarity graph
   * @param edges              Edges of the similarity graph
   * @param goldenTruthIdProperty The mandatory property key for the id of the real world entity equivalent
   *                             (golden truth) to a vertex
   */
  public AbstractSimilarityGraphQuality(DataSet<EPGMVertex> vertices, DataSet<EPGMEdge> edges,
    String goldenTruthIdProperty) {
    this.vertices = vertices;
    this.edges = edges;
    this.goldenTruthIdProperty = goldenTruthIdProperty;
  }

  /**
   * Returns the number of true positives, calls {@link #computeValues} if necessary
   *
   * @return Number of true positives
   * @throws Exception thrown if value computation fails
   */
  public long getTruePositives() throws Exception {
    return truePositives;
  }

  /**
   * Returns the number of all positives, calls {@link #computeValues} if necessary
   *
   * @return Number of all positives
   * @throws Exception thrown if value computation fails
   */
  public long getAllPositives() throws Exception {
    return allPositives;
  }

  /**
   * Returns the number of ground truth records, calls {@link #computeValues} if necessary
   *
   * @return Number of ground truth records
   * @throws Exception thrown if value computation fails
   */
  public long getGtRecordNo() throws Exception {
    return gtRecordNo;
  }

  /**
   * Computes and returns the precision, calls {@link #computeValues} if necessary
   *
   * @return precision
   * @throws Exception thrown if value computation fails
   */
  public double computePrecision() throws Exception {
    double precision = (double) truePositives / allPositives;
    return Double.isNaN(precision) ? 0d : precision;
  }

  /**
   * Computes and returns the recall, calls {@link #computeValues} if necessary
   *
   * @return recall
   * @throws Exception thrown if value computation fails
   */
  public double computeRecall() throws Exception {
    double recall = (double) truePositives / gtRecordNo;
    return Double.isNaN(recall) ? 0d : recall;
  }

  /**
   * Computes and returns the f-measure, calls {@link #computeValues} if necessary
   *
   * @return f-measure
   * @throws Exception thrown if value computation fails
   */
  public double computeFMeasure() throws Exception {
    double pr = computePrecision();
    double re = computeRecall();
    double fMeasure = 2d * pr * re / (pr + re);
    return Double.isNaN(fMeasure) ? 0d : fMeasure;
  }

  /**
   * Assigns the computed values of datasets for true positives, all positives and ground truth records
   *
   * @throws Exception thrown if value computation fails
   */
  protected void assignValues() throws Exception {
    DataSet<Tuple2<String, Long>> sets = truePositivesSet.union(allPositivesSet).union(gtRecordNoSet);
    for (Tuple2<String, Long> i : sets.collect()) {
      if (i.f0.equals(AP_FLAG)) {
        allPositives = i.f1;
      } else if (i.f0.equals(TP_FLAG)) {
        truePositives = i.f1;
      } else if (i.f0.equals(GT_FLAG)) {
        gtRecordNo = i.f1;
      }
    }
  }

  /**
   * Computes the values for true positives, all positives and ground truth records
   */
  protected void computeValuesCommon() {
    DataSet<Tuple2<String, String>> pairedVertices =
      new EdgeToEdgeSourceVertexTargetVertex(vertices, edges).execute()
        .map(new Project3To1And2<>())
        .map(new MapVertexPairToPropertyValuePair(goldenTruthIdProperty));

    allPositivesSet = Count.count(pairedVertices).map(new ObjectTo1<>()).map(new ValueAnnotator(AP_FLAG));

    truePositivesSet = Count.count(pairedVertices.filter(tuple -> tuple.f0.equals(tuple.f1)))
      .map(new ObjectTo1<>()).map(new ValueAnnotator(TP_FLAG));

    gtRecordNoSet = vertices.filter(new ByProperty<>(goldenTruthIdProperty))
      .map(new MapVertexToPropertyValue(goldenTruthIdProperty))
      .map(new ObjectTo1<>()).groupBy(0)
      .reduceGroup(new GroupCounter<>())
      .map(new CombinationCounter()).aggregate(Aggregations.SUM, 0)
      .map(new ValueAnnotator(GT_FLAG));
  }

  /**
   * Computes all sets, must be implemented by inheriting classes
   */
  protected abstract void computeValues();
}
