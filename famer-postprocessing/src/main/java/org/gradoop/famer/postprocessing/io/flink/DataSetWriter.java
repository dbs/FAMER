/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.io.flink;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.core.fs.FileSystem;

/**
 * Class to write Flinks {@link DataSet} with arbitrary entry types as CSV to disk.
 *
 * @param <T> The {@link DataSet} entry type, must be of type {@link org.apache.flink.api.java.tuple.Tuple}
 */
public class DataSetWriter<T> {

  /**
   * Delimiter for each written row
   */
  private final String rowDelimiter = System.getProperty("line.separator");

  /**
   * Delimiter for each written field in a row
   */
  private final String fieldDelimiter;

  /**
   * Creates a new instance of DataSetWriter
   *
   * @param fieldDelimiter Delimiter for each written field in a row
   */
  public DataSetWriter(String fieldDelimiter) {
    this.fieldDelimiter = fieldDelimiter;
  }

  /**
   * Writes a {@link DataSet} of type {@code <T>} as CSV to the given target path, overwrites existing data.
   *
   * @param dataSet The {@link DataSet} to write
   * @param targetPath The target path
   */
  public void writeDataSetAsCSV(DataSet<T> dataSet, String targetPath) throws Exception {
    writeDataSetAsCSV(dataSet, targetPath, true);
  }

  /**
   * Writes a {@link DataSet} of type {@code <T>} as CSV to the given target path.
   *
   * @param dataSet The {@link DataSet} to write
   * @param targetPath The target path
   * @param overwrite Whether to overwrite existing data
   */
  public void writeDataSetAsCSV(DataSet<T> dataSet, String targetPath, boolean overwrite) throws Exception {
    FileSystem.WriteMode writeMode = overwrite ?
      FileSystem.WriteMode.OVERWRITE : FileSystem.WriteMode.NO_OVERWRITE;

    dataSet.writeAsCsv(targetPath, rowDelimiter, fieldDelimiter, writeMode);
    dataSet.getExecutionEnvironment().execute();
  }
}
