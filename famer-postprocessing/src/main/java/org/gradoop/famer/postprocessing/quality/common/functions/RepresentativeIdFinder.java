/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.common.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * The group flatMap implementation that maps an entity id to its representative id.
 */
public class RepresentativeIdFinder implements
  FlatMapFunction<Tuple2<String, String>, Tuple2<String, String>> {

  @Override
  public void flatMap(Tuple2<String, String> in, Collector<Tuple2<String, String>> out) {
    String representativeId = in.f0;
    if (in.f0.compareTo(in.f1) > 0) {
      representativeId = in.f1;
    }
    out.collect(Tuple2.of(in.f0, representativeId));
    out.collect(Tuple2.of(in.f1, representativeId));
  }
}
