/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.common.model.impl.pojo.EPGMElement;

/**
 * Checks if an EPGMElement has the property given by {@link #propertyKey} which is need for the quality
 * computation. Throws an runtime exception, if the element does not have the property.
 *
 * @param <T> EPGMElement type
 */
public class CheckElementForExistingProperty<T extends EPGMElement> implements MapFunction<T, T> {

  /**
   * The property to check for
   */
  private final String propertyKey;

  /**
   * Creates an instance of CheckElementForExistingProperty
   *
   * @param propertyKey The property to check for
   */
  public CheckElementForExistingProperty(String propertyKey) {
    this.propertyKey = propertyKey;
  }

  @Override
  public T map(T element) throws Exception {
    if (!element.hasProperty(propertyKey)) {
      throw new NullPointerException("EPGMElement has no property: " + propertyKey + " " +
        "- but it is need for quality computation.");
    }
    return element;
  }
}
