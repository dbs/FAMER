/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple1;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Maps a vertex to a {@code Tuple1<PropertyValue>}, with the property value to extract from
 * the vertex given by {@link #propertyKey}.
 */
public class MapVertexToPropertyValue implements MapFunction<EPGMVertex, Tuple1<String>> {

  /**
   * The key for the property value
   */
  private final String propertyKey;

  /**
   * Reduce object instantiation
   */
  private final Tuple1<String> reuseTuple;

  /**
   * Creates an instance of MapVertexToPropertyValue
   *
   * @param propertyKey The key for the property value
   */
  public MapVertexToPropertyValue(String propertyKey) {
    this.propertyKey = propertyKey;
    this.reuseTuple = new Tuple1<>();
  }

  @Override
  public Tuple1<String> map(EPGMVertex vertex) throws Exception {
    reuseTuple.f0 = vertex.getPropertyValue(propertyKey).toString();
    return reuseTuple;
  }
}
