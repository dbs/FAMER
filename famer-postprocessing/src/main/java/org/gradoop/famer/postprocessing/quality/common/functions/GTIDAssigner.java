/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.common.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Stores the computed golden truth id inside the vertex. In case of no golden truth id available for a
 * vertex, it considers the entity id as the golden truth id of the vertex (the vertex is singleton)
 */
public class GTIDAssigner implements
  JoinFunction<Tuple2<EPGMVertex, String>, Tuple2<String, String>, EPGMVertex> {

  /**
   * Property key for the entity id
   */
  private final String entityIdProperty;

  /**
   * Creates an instance of GTIDAssigner
   *
   * @param entityIdProperty Property key for the entity id
   */
  public GTIDAssigner(String entityIdProperty) {
    this.entityIdProperty = entityIdProperty;
  }

  @Override
  public EPGMVertex join(Tuple2<EPGMVertex, String> in1, Tuple2<String, String> in2) {
    if (in2 == null) {
      in1.f0.setProperty("gtId", in1.f0.getPropertyValue(entityIdProperty).toString());
    } else {
      in1.f0.setProperty("gtId", in2.f1);
    }
    return in1.f0;
  }
}
