/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;

/**
 * Annotates a value with its corresponding flag
 */
public class ValueAnnotator implements MapFunction<Tuple1<Long>, Tuple2<String, Long>> {

  /**
   * The flag value
   */
  private final String flag;

  /**
   * Creates an instance of ValueAnnotator
   *
   * @param flag The flag value
   */
  public ValueAnnotator(String flag) {
    this.flag = flag;
  }

  @Override
  public Tuple2<String, Long> map(Tuple1<Long> number) {
    return Tuple2.of(flag, number.f0);
  }
}
