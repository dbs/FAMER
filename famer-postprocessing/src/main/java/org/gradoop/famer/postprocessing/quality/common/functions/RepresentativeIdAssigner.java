/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.common.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * The group reduce implementation that pairs an entity id with its computed representative id
 */
public class RepresentativeIdAssigner implements
  GroupReduceFunction<Tuple2<String, String>, Tuple2<String, String>> {

  @Override
  public void reduce(Iterable<Tuple2<String, String>> group, Collector<Tuple2<String, String>> out) {
    String id = "";
    String representativeId = "";
    for (Tuple2<String, String> item : group) {
      id = item.f0;
      if (representativeId.isEmpty() || representativeId.compareTo(item.f1) > 0) {
        representativeId = item.f1;
      }
    }
    out.collect(Tuple2.of(id, representativeId));
  }
}
