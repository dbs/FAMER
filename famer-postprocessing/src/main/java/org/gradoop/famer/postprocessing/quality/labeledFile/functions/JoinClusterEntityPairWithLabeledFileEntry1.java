/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.labeledFile.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;

/**
 * Joins a pair of clusterId,entityId with an entry from a labeled file where the entityId matches.
 */
public class JoinClusterEntityPairWithLabeledFileEntry1 implements
  JoinFunction<Tuple2<String, String>, Tuple3<String, String, Boolean>,
    Tuple4<String, String, String, Boolean>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple4<String, String, String, Boolean> reuseTuple = new Tuple4<>();

  @Override
  public Tuple4<String, String, String, Boolean> join(Tuple2<String, String> in1,
    Tuple3<String, String, Boolean> in2) throws Exception {
    reuseTuple.f0 = in1.f0;
    reuseTuple.f1 = in1.f1;
    reuseTuple.f2 = in2.f1;
    reuseTuple.f3 = in2.f2;
    return reuseTuple;
  }
}
