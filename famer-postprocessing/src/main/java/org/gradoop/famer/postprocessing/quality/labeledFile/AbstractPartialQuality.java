/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.labeledFile;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.postprocessing.quality.common.QualityComputation;

/**
 * Abstract class all classes for quality measuring using (partial) labeled files for ground truth are
 * inheriting from.
 */
public abstract class AbstractPartialQuality implements QualityComputation {
  /**
   * Flag to mark tuples with the value for true positives
   */
  protected static final String TP_FLAG = "tp";
  /**
   * Flag to mark tuples with the value for all positives
   */
  protected static final String FP_FLAG = "fp";
  /**
   * Flag to mark tuples with the value for ground truth record numbers
   */
  protected static final String GT_FLAG = "gt";
  /**
   * Path to the partial ground truth file
   */
  protected final String labeledDataFilePath;
  /**
   * Tokenizer for the ground truth file lines
   */
  protected final String splitter;
  /**
   * Property key for the entity id
   */
  protected final String entityIdProperty;
  /**
   * The match label String
   */
  protected final String matchLabel;
  /**
   * The input clustered vertices
   */
  protected DataSet<EPGMVertex> clusteredVertices;
  /**
   * DataSet with {@code Tuple2<TP_FLAG, true positives count>}
   */
  protected DataSet<Tuple2<String, Long>> truePositivesSet;
  /**
   * DataSet with {@code Tuple2<FP_FLAG, all positives count>}
   */
  protected DataSet<Tuple2<String, Long>> falsePositivesSet;
  /**
   * DataSet with {@code Tuple2<GT_FLAG, ground truth record number count>}
   */
  protected DataSet<Tuple2<String, Long>> gtRecordNoSet;
  /**
   * Number of true positives
   */
  protected long truePositives;
  /**
   * Number of all positives
   */
  protected long falsePositives;
  /**
   * Number of ground truth records
   */
  protected long gtRecordNo;

  /**
   * Creates an instance of AbstractPartialQuality
   *
   * @param clusteredVertices   The input clustered vertices
   * @param labeledDataFilePath Path to the partial ground truth file
   * @param splitter            Tokenizer for the ground truth file lines
   * @param entityIdProperty       Property key for the entity id
   * @param matchLabel          The match label String
   */
  public AbstractPartialQuality(DataSet<EPGMVertex> clusteredVertices, String labeledDataFilePath,
    String splitter, String entityIdProperty, String matchLabel) {
    this.clusteredVertices = clusteredVertices;
    this.labeledDataFilePath = labeledDataFilePath;
    this.splitter = splitter;
    this.entityIdProperty = entityIdProperty;
    this.matchLabel = matchLabel;
  }

  /**
   * @return Number of true positives
   * @throws Exception thrown if value computation fails
   */
  public long getTruePositives() throws Exception {
    return truePositives;
  }

  /**
   * @return Number of all positives
   * @throws Exception thrown if value computation fails
   */
  public long getFalsePositives() throws Exception {
    return falsePositives;
  }

  /**
   * @return Number of all positives
   * @throws Exception thrown if value computation fails
   */
  public long getAllPositives() throws Exception {
    return falsePositives + truePositives;
  }

  /**
   * @return Number of ground truth records
   * @throws Exception thrown if value computation fails
   */
  public long getGtRecordNo() throws Exception {
    return gtRecordNo;
  }

  /**
   * @return precision
   * @throws Exception thrown if value computation fails
   */
  public double computePrecision() throws Exception {
    double precision = (double) truePositives / (falsePositives + truePositives);
    return Double.isNaN(precision) ? 0d : precision;
  }

  /**
   * @return recall
   * @throws Exception thrown if value computation fails
   */
  public double computeRecall() throws Exception {
    double recall = (double) truePositives / gtRecordNo;
    return Double.isNaN(recall) ? 0d : recall;
  }

  /**
   * @return f-measure
   * @throws Exception thrown if value computation fails
   */
  public double computeFMeasure() throws Exception {
    double pr = computePrecision();
    double re = computeRecall();
    double fMeasure = 2d * pr * re / (pr + re);
    return Double.isNaN(fMeasure) ? 0d : fMeasure;
  }

  /**
   * Assigns the computed values of datasets for true positives, false positives and ground truth records
   *
   * @throws Exception thrown if value computation fails
   */
  protected void assignValues() throws Exception {
    DataSet<Tuple2<String, Long>> sets = truePositivesSet.union(falsePositivesSet).union(gtRecordNoSet);
    for (Tuple2<String, Long> setCountTuple : sets.collect()) {
      switch (setCountTuple.f0) {
      case FP_FLAG:
        falsePositives = setCountTuple.f1;
        break;
      case TP_FLAG:
        truePositives = setCountTuple.f1;
        break;
      case GT_FLAG:
        gtRecordNo = setCountTuple.f1;
        break;
      default:
        break;
      }
    }
  }
}
