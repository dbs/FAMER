/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Maps a vertex to a {@code Tuple3<VertexId, ClusterId, PropertyValue>}, with the property value to extract
 * from the vertex given by {@link #propertyKey}.
 */
public class MapVertexToIdAndClusterIdAndPropertyValue implements
  FlatMapFunction<EPGMVertex, Tuple3<String, String, String>> {

  /**
   * The key for the property value
   */
  private final String propertyKey;

  /**
   * Reduce object instantiation
   */
  private final Tuple3<String, String, String> reuseTuple;

  /**
   * Creates an instance of MapVertexToIdAndClusterIdAndPropertyValue
   *
   * @param propertyKey The key for the property value
   */
  public MapVertexToIdAndClusterIdAndPropertyValue(String propertyKey) {
    this.propertyKey = propertyKey;
    this.reuseTuple = new Tuple3<>();
  }

  @Override
  public void flatMap(EPGMVertex vertex, Collector<Tuple3<String, String, String>> out) {
    if (vertex.hasProperty(propertyKey) && vertex.hasProperty(PropertyNames.CLUSTER_ID)) {
      reuseTuple.f0 = vertex.getId().toString();
      reuseTuple.f2 = vertex.getPropertyValue(propertyKey).toString();
      for (String clusterId : vertex.getPropertyValue(PropertyNames.CLUSTER_ID).toString().split(",")) {
        reuseTuple.f1 = clusterId;
        out.collect(reuseTuple);
      }
    }
  }
}
