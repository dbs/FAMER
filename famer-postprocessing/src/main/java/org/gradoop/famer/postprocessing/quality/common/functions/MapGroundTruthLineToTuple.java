/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.common.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * Maps a line from the ground truth file to a {@code Tuple2<String, String>}
 */
public class MapGroundTruthLineToTuple implements FlatMapFunction<String, Tuple2<String, String>> {

  /**
   * Tokenizer used to split a ground truth line
   */
  private final String splitter;

  /**
   * Reduce object instantiation
   */
  private final Tuple2<String, String> reuseTuple;

  /**
   * Creates an instance of MapGroundTruthLineToTuple
   *
   * @param splitter Tokenizer used to split a ground truth line
   */
  public MapGroundTruthLineToTuple(String splitter) {
    this.splitter = splitter;
    this.reuseTuple = new Tuple2<>();
  }

  @Override
  public void flatMap(String line, Collector<Tuple2<String, String>> out) throws Exception {
    String[] split = line.split(splitter);
    if (split.length >= 2) {
      reuseTuple.f0 = split[0].replace("\"", "");
      reuseTuple.f1 = split[1].replace("\"", "");
      out.collect(reuseTuple);
    }
  }
}
