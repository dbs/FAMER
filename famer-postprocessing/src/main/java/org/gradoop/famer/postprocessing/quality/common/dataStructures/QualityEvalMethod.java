/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.common.dataStructures;

/**
 * Enumeration for the available quality evaluation methods. Each enum value holds the full class name for the
 * corresponding method, which may be used in the famer configuration to instantiate the classes.
 */
public enum QualityEvalMethod {
  /**
   * Evaluate quality for a similarity graph
   */
  SIMILARITY("org.gradoop.famer.postprocessing.quality.similarityGraph.SimilarityGraphQuality"),
  /**
   * Evaluate quality for a similarity graph with given golden truth
   */
  SIMILARITY_GT("org.gradoop.famer.postprocessing.quality.similarityGraph.SimilarityGraphQualityWithGTFile"),
  /**
   * Evaluate quality for a clustering
   */
  CLUSTERING("org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQuality"),
  /**
   * Evaluate quality for a clustering with given golden truth
   */
  CLUSTERING_GT("org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQualityWithGTFile");

  /**
   * The String representation for the class name used for reflections
   */
  private final String fullClassName;

  /**
   * Creates an instance of QualityEvalMethod
   *
   * @param fullClassName The String representation for the class name used for reflections
   */
  QualityEvalMethod(String fullClassName) {
    this.fullClassName = fullClassName;
  }

  /**
   * Returns the package name for the quality evaluation method
   *
   * @return The package name
   */
  public String getFullClassName() {
    return this.fullClassName;
  }
}
