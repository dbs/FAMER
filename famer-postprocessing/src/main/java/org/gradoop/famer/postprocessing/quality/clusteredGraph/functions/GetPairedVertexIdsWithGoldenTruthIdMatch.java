/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.List;

/**
 * Returns all vertex id pairs from each cluster group as {@code Tuple2<VertexIdPair, IsMatch>},
 * with {@code IsMatch} indicating whether the vertices have a matching Golden Truth Id.
 */
public class GetPairedVertexIdsWithGoldenTruthIdMatch implements
  GroupReduceFunction<Tuple3<String, String, String>, Tuple2<String, Boolean>> {

  @Override
  public void reduce(Iterable<Tuple3<String, String, String>> group, Collector<Tuple2<String, Boolean>> out)
    throws Exception {
    List<Tuple3<String, String, String>> groupList = new ArrayList<>();
    for (Tuple3<String, String, String> item : group) {
      groupList.add(item);
    }
    for (int i = 0; i < groupList.size() - 1; i++) {
      Tuple3<String, String, String> item1 = groupList.get(i);
      for (int j = i + 1; j < groupList.size(); j++) {
        Tuple3<String, String, String> item2 = groupList.get(j);
        String pairedVertexIds = item1.f0 + item2.f0;
        if (item1.f0.compareTo(item2.f0) > 0) {
          pairedVertexIds = item2.f0 + item1.f0;
        }
        boolean isMatch = false;
        if (item1.f2.equals(item2.f2)) {
          isMatch = true;
        }
        out.collect(Tuple2.of(pairedVertexIds, isMatch));
      }
    }
  }
}
