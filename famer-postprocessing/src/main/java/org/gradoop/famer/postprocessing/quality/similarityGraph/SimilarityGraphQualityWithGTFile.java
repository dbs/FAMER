/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.similarityGraph;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.functions.CheckElementForExistingProperty;
import org.gradoop.famer.postprocessing.quality.common.functions.GTIDAssigner;
import org.gradoop.famer.postprocessing.quality.common.functions.MapGroundTruthLineToTuple;
import org.gradoop.famer.postprocessing.quality.common.functions.MapVertexToVertexPropertyValue;
import org.gradoop.famer.postprocessing.quality.common.functions.RepresentativeIdAssigner;
import org.gradoop.famer.postprocessing.quality.common.functions.RepresentativeIdFinder;

/**
 * Computes precision, recall, and f-measure of the input similarity graph. It does not consider transitive
 * closure. Number of all positives (true positives + false positives) is the number of edges.
 * It is used when there is a Golden Truth file.
 */
public class SimilarityGraphQualityWithGTFile extends AbstractSimilarityGraphQuality {

  /**
   * The object containing golden truth file specifications
   */
  private final GTFileComponent gtFileComponent;

  /**
   * Property key corresponding to the entity ids
   */
  private final String entityIdProperty;

  /**
   * Creates an instance of SimilarityGraphQualityWithGTFile
   *
   * @param vertices Similarity graph vertices
   * @param edges Similarity graph edges
   * @param gtFileComponent The object containing golden truth file specifications
   * @param entityIdProperty Property key corresponding to the entity ids
   */
  public SimilarityGraphQualityWithGTFile(DataSet<EPGMVertex> vertices, DataSet<EPGMEdge> edges,
    GTFileComponent gtFileComponent, String entityIdProperty) {
    super(vertices, edges);
    this.gtFileComponent = gtFileComponent;
    this.entityIdProperty = entityIdProperty;
  }

  /**
   * Calls the computation of the quality measures
   */
  @Override
  public void computeQuality() throws Exception {
    vertices = vertices.map(new CheckElementForExistingProperty<>(entityIdProperty));
    computeValues();
    assignValues();
  }

  /**
   * Computes all the values and assigns them to the corresponding sets.
   */
  @Override
  protected void computeValues() {
    ExecutionEnvironment env = vertices.getExecutionEnvironment();

    DataSet<Tuple2<String, String>> groundTruthPairs =
      env.readTextFile(gtFileComponent.getGroundTruthFilePath())
        .flatMap(new MapGroundTruthLineToTuple(gtFileComponent.getSplitter()));

    DataSet<Tuple2<String, String>> entityIdRepresentativeId;
    switch (gtFileComponent.getGoldenTruthFileType()) {
    case MATCHED_PAIRS:
      entityIdRepresentativeId = groundTruthPairs.flatMap(new RepresentativeIdFinder())
        .groupBy(0)
        .reduceGroup(new RepresentativeIdAssigner());
      break;
    case ENTITY_ID_GT_ID_PAIR:
      entityIdRepresentativeId = groundTruthPairs;
      break;
    default:
      throw new IllegalArgumentException("The golden truth file type is not supported!");
    }

    DataSet<Tuple2<EPGMVertex, String>> vertexEntityId =
      vertices.map(new MapVertexToVertexPropertyValue(entityIdProperty));

    vertices = vertexEntityId.leftOuterJoin(entityIdRepresentativeId)
      .where(1).equalTo(0)
      .with(new GTIDAssigner(entityIdProperty));

    computeValuesCommon();
  }
}
