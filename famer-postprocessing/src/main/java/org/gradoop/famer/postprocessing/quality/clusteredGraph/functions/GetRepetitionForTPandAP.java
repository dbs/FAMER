/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;

/**
 * Computes the repetition values for true positives (TP) and all positives (AP)
 */
public class GetRepetitionForTPandAP implements
  GroupReduceFunction<Tuple2<String, Boolean>, Tuple2<Long, Long>> {

  @Override
  public void reduce(Iterable<Tuple2<String, Boolean>> group, Collector<Tuple2<Long, Long>> out)
    throws Exception {
    long tpRepetition = 0;
    long apRepetition;
    boolean isMatch = false;
    long cnt = 0;
    for (Tuple2<String, Boolean> item : group) {
      isMatch = item.f1;
      cnt++;
    }
    apRepetition = cnt - 1;
    if (isMatch) {
      tpRepetition = apRepetition;
    }
    out.collect(Tuple2.of(tpRepetition, apRepetition));
  }
}
