/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.parallelClusteringQuality;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.gradoop.famer.clustering.parallelClustering.center.Center;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQualityWithGTFile;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.flink.io.impl.csv.CSVDataSource;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Test class for the parallel {@link Center} algorithm.
 */
public class CenterTest extends GradoopFlinkTestBase {

  private GradoopFlinkConfig config;
  private PrioritySelection prioritySelection;
  private boolean isEdgeBidirected;
  private ClusteringOutputType outputType;
  private int maxIteration;
  private String entityTitleId;
  private boolean hasOverlap;
  private GTFileComponent gtFileComponent;

  @Before
  public void setUpGraph() {
    ExecutionEnvironment env = getExecutionEnvironment();
    config = GradoopFlinkConfig.createConfig(env);
    prioritySelection = PrioritySelection.MAX;
    isEdgeBidirected = false;
    outputType = ClusteringOutputType.GRAPH;
    maxIteration = Integer.MAX_VALUE;
    String goldenTruthFilePath =
      new File(CenterTest.class.getResource("/geoGraphs/pm.csv/").getPath()).getAbsolutePath();
    gtFileComponent = new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    entityTitleId = "clsId";
    hasOverlap = false;
  }

  @Test
  public void graph75Test() throws Exception {
    String testGraphPath =
      new File(CenterTest.class.getResource("/geoGraphs/graph0.75/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new Center(prioritySelection, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.988172429897523, measurer.computePrecision(), 0.009);
    assertEquals(0.861352767023457, measurer.computeRecall(), 0.009);
    assertEquals(0.920409598194425, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph80Test() throws Exception {
    String testGraphPath =
      new File(CenterTest.class.getResource("/geoGraphs/graph0.80/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new Center(prioritySelection, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);


    measurer.computeQuality();

    assertEquals(0.990373529981512, measurer.computePrecision(), 0.009);
    assertEquals(0.854588931906172, measurer.computeRecall(), 0.009);
    assertEquals(0.917479379955272, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph85Test() throws Exception {
    String testGraphPath =
      new File(CenterTest.class.getResource("/geoGraphs/graph0.85/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new Center(prioritySelection, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.993374357937473, measurer.computePrecision(), 0.009);
    assertEquals(0.829514916875427, measurer.computeRecall(), 0.009);
    assertEquals(0.904077023210868, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph90Test() throws Exception {
    String testGraphPath =
      new File(CenterTest.class.getResource("/geoGraphs/graph0.90/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new Center(prioritySelection, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);
    measurer.computeQuality();

    assertEquals(0.99526627291623, measurer.computePrecision(), 0.009);
    assertEquals(0.708654065133227, measurer.computeRecall(), 0.009);
    assertEquals(0.827853749049823, measurer.computeFMeasure(), 0.009);
  }
}
