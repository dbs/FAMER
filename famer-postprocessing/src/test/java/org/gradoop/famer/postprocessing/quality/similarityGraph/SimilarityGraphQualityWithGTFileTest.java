/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.similarityGraph;

import org.apache.flink.runtime.client.JobExecutionException;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * JUnit test for {@link SimilarityGraphQualityWithGTFile}
 */
public class SimilarityGraphQualityWithGTFileTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  @Test(expected = JobExecutionException.class)
  public void testForMissingEntityIdTitleExpectException() throws Exception {
    String goldenTruthFilePath = new File(SimilarityGraphQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testSimGraphGTMatchedPairs.csv").getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "similarityGraph[" +
      "(v0 {})" +
      "(v1 {id:1})" +
      "(v2 {id:2})" +
      "(v3 {id:3})" +
      "(v4 {id:4})" +
      "(v5 {id:5})" +
      "(v6 {id:6})" +
      "(v0 {})-[]->(v1)" +
      "(v0 {})-[]->(v2)" +
      "(v2 {})-[]->(v1)" +
      "(v4 {})-[]->(v5)" +
      "(v4 {})-[]->(v6)" +
      "(v5 {})-[]->(v6)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQualityWithGTFile measurer =
      new SimilarityGraphQualityWithGTFile(inputGraph.getVertices(), inputGraph.getEdges(), gtFileComponent,
        "id");

    measurer.computeQuality();
  }

  @Test
  public void testGTMatchedPairs() throws Exception {
    String goldenTruthFilePath = new File(SimilarityGraphQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testSimGraphGTMatchedPairs.csv").getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "similarityGraph[" +
      "(v0 {id:0})" +
      "(v1 {id:1})" +
      "(v2 {id:2})" +
      "(v3 {id:3})" +
      "(v4 {id:4})" +
      "(v5 {id:5})" +
      "(v6 {id:6})" +
      "(v0 {})-[]->(v1)" +
      "(v0 {})-[]->(v2)" +
      "(v2 {})-[]->(v1)" +
      "(v4 {})-[]->(v5)" +
      "(v4 {})-[]->(v6)" +
      "(v5 {})-[]->(v6)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQualityWithGTFile measurer =
      new SimilarityGraphQualityWithGTFile(inputGraph.getVertices(), inputGraph.getEdges(), gtFileComponent,
        "id");

    measurer.computeQuality();

    assertEquals(4, measurer.getTruePositives(), 0);
    assertEquals(6, measurer.getAllPositives(), 0);
    assertEquals(7, measurer.getGtRecordNo(), 0);
    assertEquals(0.66666, measurer.computePrecision(), 0.000009);
    assertEquals(0.57142, measurer.computeRecall(), 0.000009);
    assertEquals(0.61538, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testGTMatchedPairsSingletons() throws Exception {
    String goldenTruthFilePath = new File(SimilarityGraphQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testSimGraphGTMatchedPairsSingletons.csv").getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "similarityGraph[" +
      "(v0 {id:0})" +
      "(v1 {id:1})" +
      "(v2 {id:2})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQualityWithGTFile measurer =
      new SimilarityGraphQualityWithGTFile(inputGraph.getVertices(), inputGraph.getEdges(), gtFileComponent,
        "id");

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(0, measurer.getAllPositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(0.0, measurer.computePrecision(), 0.0);
    assertEquals(0, measurer.computeRecall(), 0.0);
    assertEquals(0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testGTMatchedPairsWithNoGoldenTruthPair() throws Exception {
    String goldenTruthFilePath = new File(SimilarityGraphQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testSimGraphGTMatchedPairsNoPair.csv").getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "similarityGraph[" +
      "(v0 {id:0})" +
      "(v1 {id:1})" +
      "(v2 {id:2})" +
      "(v0 {})-[]->(v1)" +
      "(v0 {})-[]->(v2)" +
      "(v2 {})-[]->(v1)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQualityWithGTFile measurer =
      new SimilarityGraphQualityWithGTFile(inputGraph.getVertices(), inputGraph.getEdges(), gtFileComponent,
        "id");

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(3, measurer.getAllPositives(), 0);
    assertEquals(0, measurer.getGtRecordNo(), 0);
    assertEquals(0, measurer.computePrecision(), 0.0);
    assertEquals(0, measurer.computeRecall(), 0.0);
    assertEquals(0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testGTMatchedPairsIllFormattedFile() throws Exception {
    String goldenTruthFilePath = new File(SimilarityGraphQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testSimGraphGTMatchedPairsIllFormattedFile.csv").getPath())
      .getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "similarityGraph[" +
      "(v0 {id:0})" +
      "(v1 {id:1})" +
      "(v2 {id:2})" +
      "(v3 {id:3})" +
      "(v4 {id:4})" +
      "(v5 {id:5})" +
      "(v6 {id:6})" +
      "(v0 {})-[]->(v1)" +
      "(v0 {})-[]->(v2)" +
      "(v2 {})-[]->(v1)" +
      "(v4 {})-[]->(v5)" +
      "(v4 {})-[]->(v6)" +
      "(v5 {})-[]->(v6)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQualityWithGTFile measurer =
      new SimilarityGraphQualityWithGTFile(inputGraph.getVertices(), inputGraph.getEdges(), gtFileComponent,
        "id");

    measurer.computeQuality();

    assertEquals(4, measurer.getTruePositives(), 0);
    assertEquals(6, measurer.getAllPositives(), 0);
    assertEquals(7, measurer.getGtRecordNo(), 0);
    assertEquals(0.66666, measurer.computePrecision(), 0.000009);
    assertEquals(0.57142, measurer.computeRecall(), 0.000009);
    assertEquals(0.61538, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testGTEntityIdGTId() throws Exception {
    String goldenTruthFilePath = new File(SimilarityGraphQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testSimGraphGTEntityIdGTId.csv").getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR);
    String graphString = "similarityGraph[" +
      "(v0 {id:0})" +
      "(v1 {id:1})" +
      "(v2 {id:2})" +
      "(v3 {id:3})" +
      "(v4 {id:4})" +
      "(v5 {id:5})" +
      "(v6 {id:6})" +
      "(v0 {})-[]->(v1)" +
      "(v0 {})-[]->(v2)" +
      "(v2 {})-[]->(v1)" +
      "(v4 {})-[]->(v5)" +
      "(v4 {})-[]->(v6)" +
      "(v5 {})-[]->(v6)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQualityWithGTFile measurer =
      new SimilarityGraphQualityWithGTFile(inputGraph.getVertices(), inputGraph.getEdges(), gtFileComponent,
        "id");

    measurer.computeQuality();

    assertEquals(4, measurer.getTruePositives(), 0);
    assertEquals(6, measurer.getAllPositives(), 0);
    assertEquals(7, measurer.getGtRecordNo(), 0);
    assertEquals(0.66666, measurer.computePrecision(), 0.000009);
    assertEquals(0.57142, measurer.computeRecall(), 0.000009);
    assertEquals(0.61538, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testGTEntityIdGTIdSingletons() throws Exception {
    String goldenTruthFilePath = new File(SimilarityGraphQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testSimGraphGTEntityIdGTIdSingletons.csv").getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR);
    String graphString = "similarityGraph[" +
      "(v0 {id:0})" +
      "(v1 {id:1})" +
      "(v2 {id:2})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQualityWithGTFile measurer =
      new SimilarityGraphQualityWithGTFile(inputGraph.getVertices(), inputGraph.getEdges(), gtFileComponent,
        "id");

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(0, measurer.getAllPositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(0.0, measurer.computePrecision(), 0.0);
    assertEquals(0, measurer.computeRecall(), 0.0);
    assertEquals(0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testGTEntityIdGTIdWithNoGoldenTruthPair() throws Exception {
    String goldenTruthFilePath = new File(SimilarityGraphQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testSimGraphGTEntityIdGTIdNoPair.csv").getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR);
    String graphString = "similarityGraph[" +
      "(v0 {id:0})" +
      "(v1 {id:1})" +
      "(v2 {id:2})" +
      "(v0 {})-[]->(v1)" +
      "(v0 {})-[]->(v2)" +
      "(v2 {})-[]->(v1)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQualityWithGTFile measurer =
      new SimilarityGraphQualityWithGTFile(inputGraph.getVertices(), inputGraph.getEdges(), gtFileComponent,
        "id");

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(3, measurer.getAllPositives(), 0);
    assertEquals(0, measurer.getGtRecordNo(), 0);
    assertEquals(0, measurer.computePrecision(), 0.0);
    assertEquals(0, measurer.computeRecall(), 0.0);
    assertEquals(0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testGTEntityIdGTIdIllFormattedFile() throws Exception {
    String goldenTruthFilePath = new File(SimilarityGraphQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testSimGraphGTEntityIdGTIdIllFormattedFile.csv").getPath())
      .getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR);
    String graphString = "similarityGraph[" +
      "(v0 {id:0})" +
      "(v1 {id:1})" +
      "(v2 {id:2})" +
      "(v3 {id:3})" +
      "(v4 {id:4})" +
      "(v5 {id:5})" +
      "(v6 {id:6})" +
      "(v0 {})-[]->(v1)" +
      "(v0 {})-[]->(v2)" +
      "(v2 {})-[]->(v1)" +
      "(v4 {})-[]->(v5)" +
      "(v4 {})-[]->(v6)" +
      "(v5 {})-[]->(v6)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQualityWithGTFile measurer =
      new SimilarityGraphQualityWithGTFile(inputGraph.getVertices(), inputGraph.getEdges(), gtFileComponent,
        "id");

    measurer.computeQuality();

    assertEquals(4, measurer.getTruePositives(), 0);
    assertEquals(6, measurer.getAllPositives(), 0);
    assertEquals(7, measurer.getGtRecordNo(), 0);
    assertEquals(0.66666, measurer.computePrecision(), 0.000009);
    assertEquals(0.57142, measurer.computeRecall(), 0.000009);
    assertEquals(0.61538, measurer.computeFMeasure(), 0.000009);
  }
}
