/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.parallelClusteringQuality;

import org.apache.commons.math3.util.Precision;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.correlationClustering.CorrelationClustering;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQualityWithGTFile;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.flink.io.impl.csv.CSVDataSource;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Test class for the parallel {@link CorrelationClustering} algorithm.
 */
public class CorrelationClusteringTest extends GradoopFlinkTestBase {

  private GradoopFlinkConfig config;
  private double epsilon;
  private boolean isEdgeBidirected;
  private ClusteringOutputType outputType;
  private int maxIteration;
  private String entityTitleId;
  private boolean hasOverlap;
  private GTFileComponent gtFileComponent;

  @Before
  public void setUpGraph() throws Exception {
    ExecutionEnvironment env = getExecutionEnvironment();
    config = GradoopFlinkConfig.createConfig(env);
    epsilon = 0.9;
    isEdgeBidirected = false;
    outputType = ClusteringOutputType.GRAPH;
    maxIteration = Integer.MAX_VALUE;
    String goldenTruthFilePath =
      new File(CorrelationClusteringTest.class.getResource("/geoGraphs/pm.csv/").getPath()).getAbsolutePath();
    gtFileComponent = new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    entityTitleId = "clsId";
    hasOverlap = false;
  }

  @Test
  public void graph75Test() throws Exception {
    String testGraphPath =
      new File(CorrelationClusteringTest.class.getResource("/geoGraphs/graph0.75/").getPath())
        .getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph =
      graph.callForGraph(new CorrelationClustering(epsilon, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.862, Precision.round(measurer.computePrecision(), 3), 0.055);
    assertEquals(0.905, Precision.round(measurer.computeRecall(), 3), 0.055);
    assertEquals(0.883, Precision.round(measurer.computeFMeasure(), 3), 0.055);
  }

  @Test
  public void graph80Test() throws Exception {
    String testGraphPath =
      new File(CorrelationClusteringTest.class.getResource("/geoGraphs/graph0.80/").getPath())
        .getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph =
      graph.callForGraph(new CorrelationClustering(epsilon, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.927, Precision.round(measurer.computePrecision(), 3), 0.029);
    assertEquals(0.930, Precision.round(measurer.computeRecall(), 3), 0.029);
    assertEquals(0.929, Precision.round(measurer.computeFMeasure(), 3), 0.029);
  }

  @Test
  public void graph85Test() throws Exception {
    String testGraphPath =
      new File(CorrelationClusteringTest.class.getResource("/geoGraphs/graph0.85/").getPath())
        .getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph =
      graph.callForGraph(new CorrelationClustering(epsilon, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.968, Precision.round(measurer.computePrecision(), 3), 0.021);
    assertEquals(0.917, Precision.round(measurer.computeRecall(), 3), 0.021);
    assertEquals(0.942, Precision.round(measurer.computeFMeasure(), 3), 0.021);
  }

  @Test
  public void graph90Test() throws Exception {
    String testGraphPath =
      new File(CorrelationClusteringTest.class.getResource("/geoGraphs/graph0.90/").getPath())
        .getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph =
      graph.callForGraph(new CorrelationClustering(epsilon, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.983, Precision.round(measurer.computePrecision(), 3), 0.010);
    assertEquals(0.779, Precision.round(measurer.computeRecall(), 3), 0.010);
    assertEquals(0.869, Precision.round(measurer.computeFMeasure(), 3), 0.010);
  }
}
