/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.labeledFile;

import org.apache.flink.runtime.client.JobExecutionException;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQualityWithGTFileTest;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.io.File;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.junit.Assert.assertEquals;

/**
 * JUnit test for {@link ClusteringQualityWithLabeledFile}
 */
public class ClusteringQualityWithLabeledFileTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  @Test(expected = JobExecutionException.class)
  public void testForMissingEntityIdTitleExpectException() throws Exception {
    String goldenTruthFilePath = new File(
      ClusteringQualityWithGTFileTest.class.getResource("/goldenTruthFiles/testLabeled.csv").getPath())
      .getAbsolutePath();
    String graphString = "clusteredGraph[" +
      "(v0 {" + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c2\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c2\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithLabeledFile measurer =
      new ClusteringQualityWithLabeledFile(inputGraph.getVertices(), goldenTruthFilePath, ",", "id", "true");

    measurer.computeQuality();
  }

  @Test
  public void testWithFPAndTP() throws Exception {
    String goldenTruthFilePath = new File(
      ClusteringQualityWithGTFileTest.class.getResource("/goldenTruthFiles/testLabeled.csv").getPath())
      .getAbsolutePath();
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c2\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c2\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithLabeledFile measurer =
      new ClusteringQualityWithLabeledFile(inputGraph.getVertices(), goldenTruthFilePath, ",", "id", "true");

    measurer.computeQuality();

    assertEquals(1, measurer.getTruePositives(), 0);
    assertEquals(1, measurer.getFalsePositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(0.5, measurer.computePrecision(), 0.0);
    assertEquals(1.0, measurer.computeRecall(), 0.0);
    assertEquals(0.66666, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testWithNoFP() throws Exception {
    String goldenTruthFilePath = new File(
      ClusteringQualityWithGTFileTest.class.getResource("/goldenTruthFiles/testLabeled.csv").getPath())
      .getAbsolutePath();
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c1\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c2\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c2\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithLabeledFile measurer =
      new ClusteringQualityWithLabeledFile(inputGraph.getVertices(), goldenTruthFilePath, ",", "id", "true");

    measurer.computeQuality();

    assertEquals(1, measurer.getTruePositives(), 0);
    assertEquals(0, measurer.getFalsePositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(1.0, measurer.computePrecision(), 0.0);
    assertEquals(1.0, measurer.computeRecall(), 0.0);
    assertEquals(1.0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testWithNoTP() throws Exception {
    String goldenTruthFilePath = new File(
      ClusteringQualityWithGTFileTest.class.getResource("/goldenTruthFiles/testLabeled.csv").getPath())
      .getAbsolutePath();
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c3\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c2\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c2\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithLabeledFile measurer =
      new ClusteringQualityWithLabeledFile(inputGraph.getVertices(), goldenTruthFilePath, ",", "id", "true");

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(1, measurer.getFalsePositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(0.0, measurer.computePrecision(), 0.0);
    assertEquals(0.0, measurer.computeRecall(), 0.0);
    assertEquals(0.0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testWithNoTPAndNoFP() throws Exception {
    String goldenTruthFilePath = new File(
      ClusteringQualityWithGTFileTest.class.getResource("/goldenTruthFiles/testLabeled.csv").getPath())
      .getAbsolutePath();
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c3\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c1\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c2\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c2\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithLabeledFile measurer =
      new ClusteringQualityWithLabeledFile(inputGraph.getVertices(), goldenTruthFilePath, ",", "id", "true");

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(0, measurer.getFalsePositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(0.0, measurer.computePrecision(), 0.0);
    assertEquals(0.0, measurer.computeRecall(), 0.0);
    assertEquals(0.0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testOverlapping() throws Exception {
    String goldenTruthFilePath = new File(
      ClusteringQualityWithGTFileTest.class.getResource("/goldenTruthFiles/testLabeled.csv").getPath())
      .getAbsolutePath();
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1,c4\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1,c4\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2,c5\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c2,c5\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c2\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c5\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithLabeledFile measurer =
      new ClusteringQualityWithLabeledFile(inputGraph.getVertices(), goldenTruthFilePath, ",", "id", "true");

    measurer.computeQuality();

    assertEquals(1, measurer.getTruePositives(), 0);
    assertEquals(1, measurer.getFalsePositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(0.5, measurer.computePrecision(), 0.0);
    assertEquals(1.0, measurer.computeRecall(), 0.0);
    assertEquals(0.66666, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testIllFormattedLabeledFile() throws Exception {
    String goldenTruthFilePath = new File(
      ClusteringQualityWithGTFileTest.class.getResource("/goldenTruthFiles/testLabeledIllFormatted.csv")
        .getPath()).getAbsolutePath();
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c2\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c2\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithLabeledFile measurer =
      new ClusteringQualityWithLabeledFile(inputGraph.getVertices(), goldenTruthFilePath, ",", "id", "true");

    measurer.computeQuality();

    assertEquals(1, measurer.getTruePositives(), 0);
    assertEquals(1, measurer.getFalsePositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(0.5, measurer.computePrecision(), 0.0);
    assertEquals(1.0, measurer.computeRecall(), 0.0);
    assertEquals(0.66666, measurer.computeFMeasure(), 0.000009);
  }
}
