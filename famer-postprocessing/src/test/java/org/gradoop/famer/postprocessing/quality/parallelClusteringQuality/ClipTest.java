/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.parallelClusteringQuality;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.gradoop.famer.clustering.parallelClustering.clip.CLIP;
import org.gradoop.famer.clustering.parallelClustering.clip.dataStructures.CLIPConfig;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQualityWithGTFile;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.flink.io.impl.csv.CSVDataSource;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Test class for the parallel {@link CLIP} algorithm.
 */
public class ClipTest extends GradoopFlinkTestBase {

  private GradoopFlinkConfig config;
  private CLIPConfig clipConfig;
  private ClusteringOutputType outputType;
  private int maxIteration;
  private String entityTitleId;
  private boolean hasOverlap;
  private GTFileComponent gtFileComponent;


  @Before
  public void setUpGraph() throws Exception {
    ExecutionEnvironment env = getExecutionEnvironment();
    config = GradoopFlinkConfig.createConfig(env);
    clipConfig = new CLIPConfig(0.0, 4, false, 0.5, 0.2, 0.3);
    outputType = ClusteringOutputType.GRAPH;
    maxIteration = Integer.MAX_VALUE;
    String goldenTruthFilePath =
      new File(ClipTest.class.getResource("/geoGraphs/pm.csv/").getPath()).getAbsolutePath();
    entityTitleId = "clsId";
    hasOverlap = false;
    gtFileComponent = new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
  }

  @Test
  public void graph75Test() throws Exception {
    String testGraphPath =
      new File(ClipTest.class.getResource("/geoGraphs/graph0.75/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new CLIP(clipConfig, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);


    measurer.computeQuality();

    assertEquals(0.997446018110053, measurer.computePrecision(), 0.009);
    assertEquals(0.978364837166932, measurer.computeRecall(), 0.009);
    assertEquals(0.987813290411589, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph80Test() throws Exception {
    String testGraphPath =
      new File(ClipTest.class.getResource("/geoGraphs/graph0.80/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new CLIP(clipConfig, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);


    measurer.computeQuality();

    assertEquals(0.996961196820944, measurer.computePrecision(), 0.009);
    assertEquals(0.971304941926668, measurer.computeRecall(), 0.009);
    assertEquals(0.983965855346638, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph85Test() throws Exception {
    String testGraphPath =
      new File(ClipTest.class.getResource("/geoGraphs/graph0.85/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new CLIP(clipConfig, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);


    measurer.computeQuality();

    assertEquals(0.996413199426112, measurer.computePrecision(), 0.009);
    assertEquals(0.948986563425188, measurer.computeRecall(), 0.009);
    assertEquals(0.972121777674093, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph90Test() throws Exception {
    String testGraphPath =
      new File(ClipTest.class.getResource("/geoGraphs/graph0.90/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new CLIP(clipConfig, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.997018162103551, measurer.computePrecision(), 0.009);
    assertEquals(0.837622409473924, measurer.computeRecall(), 0.009);
    assertEquals(0.91039603960396, measurer.computeFMeasure(), 0.009);
  }
}
