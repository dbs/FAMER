/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.parallelClusteringQuality;

import org.apache.commons.math3.util.Precision;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.parallelClustering.star.Star;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQualityWithGTFile;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.flink.io.impl.csv.CSVDataSource;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Test class for the parallel {@link Star} algorithm.
 */
public class StarTest extends GradoopFlinkTestBase {

  GTFileComponent gtFileComponent;
  private GradoopFlinkConfig config;
  private PrioritySelection prioritySelection;
  private boolean isEdgeBidirected;
  private ClusteringOutputType outputType;
  private int maxIteration;
  private String entityTitleId;
  private boolean hasOverlap;

  @Before
  public void setUpGraph() {
    ExecutionEnvironment env = getExecutionEnvironment();
    config = GradoopFlinkConfig.createConfig(env);
    prioritySelection = PrioritySelection.MAX;
    isEdgeBidirected = false;
    outputType = ClusteringOutputType.GRAPH;
    maxIteration = Integer.MAX_VALUE;
    String goldenTruthFilePath =
      new File(StarTest.class.getResource("/geoGraphs/pm.csv/").getPath()).getAbsolutePath();
    gtFileComponent = new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    entityTitleId = "clsId";
    hasOverlap = true;
  }

  @Test
  public void graph75Star1Test() throws Exception {
    String testGraphPath =
      new File(StarTest.class.getResource("/geoGraphs/graph0.75/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(
      new Star(prioritySelection, Star.StarType.ONE, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.537, Precision.round(measurer.computePrecision(), 3), 0.009);
    assertEquals(0.978, Precision.round(measurer.computeRecall(), 3), 0.009);
    assertEquals(0.693, Precision.round(measurer.computeFMeasure(), 3), 0.009);
  }

  @Test
  public void graph80Star1Test() throws Exception {
    String testGraphPath =
      new File(StarTest.class.getResource("/geoGraphs/graph0.80/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(
      new Star(prioritySelection, Star.StarType.ONE, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    if (measurer.computePrecision() > 1.0 || measurer.computeRecall() > 1) {
      System.out.println(measurer.computePrecision() + "  " + measurer.computeRecall());
    }
    assertEquals(0.761, Precision.round(measurer.computePrecision(), 3), 0.009);
    assertEquals(0.974, Precision.round(measurer.computeRecall(), 3), 0.009);
    assertEquals(0.854, Precision.round(measurer.computeFMeasure(), 3), 0.009);
  }

  @Test
  public void graph85Star1Test() throws Exception {
    String testGraphPath =
      new File(StarTest.class.getResource("/geoGraphs/graph0.85/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(
      new Star(prioritySelection, Star.StarType.ONE, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.900, Precision.round(measurer.computePrecision(), 3), 0.009);
    assertEquals(0.952, Precision.round(measurer.computeRecall(), 3), 0.009);
    assertEquals(0.926, Precision.round(measurer.computeFMeasure(), 3), 0.009);
  }

  @Test
  public void graph90Star1Test() throws Exception {
    String testGraphPath =
      new File(StarTest.class.getResource("/geoGraphs/graph0.90/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(
      new Star(prioritySelection, Star.StarType.ONE, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.960, Precision.round(measurer.computePrecision(), 3), 0.009);
    assertEquals(0.838, Precision.round(measurer.computeRecall(), 3), 0.009);
    assertEquals(0.895, Precision.round(measurer.computeFMeasure(), 3), 0.009);
  }

  @Test
  public void graph75Star2Test() throws Exception {
    String testGraphPath =
      new File(StarTest.class.getResource("/geoGraphs/graph0.75/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(
      new Star(prioritySelection, Star.StarType.TWO, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);
    measurer.computeQuality();

    assertEquals(0.880, Precision.round(measurer.computePrecision(), 3), 0.009);
    assertEquals(0.977, Precision.round(measurer.computeRecall(), 3), 0.009);
    assertEquals(0.926, Precision.round(measurer.computeFMeasure(), 3), 0.009);
  }

  @Test
  public void graph80Star2Test() throws Exception {
    String testGraphPath =
      new File(StarTest.class.getResource("/geoGraphs/graph0.80/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(
      new Star(prioritySelection, Star.StarType.TWO, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.935, Precision.round(measurer.computePrecision(), 3), 0.009);
    assertEquals(0.969, Precision.round(measurer.computeRecall(), 3), 0.009);
    assertEquals(0.952, Precision.round(measurer.computeFMeasure(), 3), 0.009);
  }

  @Test
  public void graph85Star2Test() throws Exception {
    String testGraphPath =
      new File(StarTest.class.getResource("/geoGraphs/graph0.85/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(
      new Star(prioritySelection, Star.StarType.TWO, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.968, Precision.round(measurer.computePrecision(), 3), 0.009);
    assertEquals(0.941, Precision.round(measurer.computeRecall(), 3), 0.009);
    assertEquals(0.954, Precision.round(measurer.computeFMeasure(), 3), 0.009);
  }

  @Test
  public void graph90Star2Test() throws Exception {
    String testGraphPath =
      new File(StarTest.class.getResource("/geoGraphs/graph0.90/").getPath()).getAbsolutePath();
    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(
      new Star(prioritySelection, Star.StarType.TWO, isEdgeBidirected, outputType, maxIteration));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);
    measurer.computeQuality();

    assertEquals(0.979, Precision.round(measurer.computePrecision(), 3), 0.009);
    assertEquals(0.803, Precision.round(measurer.computeRecall(), 3), 0.009);
    assertEquals(0.882, Precision.round(measurer.computeFMeasure(), 3), 0.009);
  }
}
