/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.parallelClusteringQuality;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.HierarchicalClusteringScatterGather;
import org.gradoop.famer.clustering.serialClustering.SerialHierarchicalClustering;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQualityWithGTFile;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.flink.io.impl.csv.CSVDataSource;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;
import org.junit.Before;
import org.junit.Test;

import java.io.File;

import static org.junit.Assert.assertEquals;

/**
 * Test class for the parallel {@link HierarchicalClusteringScatterGather} algorithm.
 */
public class HierarchicalClusteringTest extends GradoopFlinkTestBase {
  private GradoopFlinkConfig config;
  private PrioritySelection prioritySelection;
  private ClusteringOutputType outputType;
  private int maxIteration;
  private boolean isEdgesBiDirected;
  private String entityTitleId;
  private boolean hasOverlap;
  private GTFileComponent gtFileComponent;

  @Before
  public void setUpGraph() {
    ExecutionEnvironment env = getExecutionEnvironment();
    config = GradoopFlinkConfig.createConfig(env);
    prioritySelection = PrioritySelection.MAX;
    outputType = ClusteringOutputType.GRAPH;
    maxIteration = Integer.MAX_VALUE;
    isEdgesBiDirected = false;
    String goldenTruthFilePath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/pm.csv/").getPath()).getAbsolutePath();
    gtFileComponent = new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    entityTitleId = "clsId";
    hasOverlap = false;
  }

  @Test
  public void graph75TestSingleLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.75/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 0.75, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.0794095424138, measurer.computePrecision(), 0.0000000000009);
    assertEquals(0.981325438396721, measurer.computeRecall(), 0.0000000000009);
    assertEquals(0.146929450676851, measurer.computeFMeasure(), 0.0000000000009);
  }

  @Test
  public void graph80TestSingleLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.80/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 0.8, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.499416433239963, measurer.computePrecision(), 0.0000000000009);
    assertEquals(0.974493281712594, measurer.computeRecall(), 0.0000000000009);
    assertEquals(0.660390462227024, measurer.computeFMeasure(), 0.0000000000009);
  }

  @Test
  public void graph85TestSingleLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.85/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 0.85, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.831279809220986, measurer.computePrecision(), 0.0000000000009);
    assertEquals(0.952630380323389, measurer.computeRecall(), 0.0000000000009);
    assertEquals(0.887827655735965, measurer.computeFMeasure(), 0.0000000000009);
  }

  @Test
  public void graph90TestSingleLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.90/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 0.9, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.941056391936718, measurer.computePrecision(), 0.0000000000009);
    assertEquals(0.839899795035299, measurer.computeRecall(), 0.0000000000009);
    assertEquals(0.887605294825511, measurer.computeFMeasure(), 0.0000000000009);
  }

  @Test
  public void graph75TestCompleteLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.75/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 0.75, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.998, measurer.computePrecision(), 0.009);
    assertEquals(0.966, measurer.computeRecall(), 0.009);
    assertEquals(0.982, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph80TestCompleteLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.80/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 0.8, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.998, measurer.computePrecision(), 0.009);
    assertEquals(0.949, measurer.computeRecall(), 0.009);
    assertEquals(0.972, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph85TestCompleteLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.85/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 0.85, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.998, measurer.computePrecision(), 0.009);
    assertEquals(0.918, measurer.computeRecall(), 0.009);
    assertEquals(0.957, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph90TestCompleteLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.90/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 0.9, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.998, measurer.computePrecision(), 0.009);
    assertEquals(0.761, measurer.computeRecall(), 0.009);
    assertEquals(0.863, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph75TestAverageLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.75/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 0.75, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.998, measurer.computePrecision(), 0.009);
    assertEquals(0.966, measurer.computeRecall(), 0.009);
    assertEquals(0.982, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph80TestAverageLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.80/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 0.80, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.998, measurer.computePrecision(), 0.009);
    assertEquals(0.952, measurer.computeRecall(), 0.009);
    assertEquals(0.974, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph85TestAverageLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.85/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 0.85, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.997, measurer.computePrecision(), 0.009);
    assertEquals(0.918, measurer.computeRecall(), 0.009);
    assertEquals(0.956, measurer.computeFMeasure(), 0.009);
  }

  @Test
  public void graph90TestAverageLinkage() throws Exception {
    String testGraphPath =
      new File(HierarchicalClusteringTest.class.getResource("/geoGraphs/graph0.90/").getPath()).getAbsolutePath();

    CSVDataSource csvDataSource = new CSVDataSource(testGraphPath, config);
    LogicalGraph graph = csvDataSource.getLogicalGraph();
    graph = graph.callForGraph(new HierarchicalClusteringScatterGather(outputType, maxIteration,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 0.9, prioritySelection, isEdgesBiDirected));
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(graph.getVertices(), gtFileComponent, entityTitleId, hasOverlap);

    measurer.computeQuality();

    assertEquals(0.998, measurer.computePrecision(), 0.009);
    assertEquals(0.761, measurer.computeRecall(), 0.009);
    assertEquals(0.864, measurer.computeFMeasure(), 0.009);
  }
}
