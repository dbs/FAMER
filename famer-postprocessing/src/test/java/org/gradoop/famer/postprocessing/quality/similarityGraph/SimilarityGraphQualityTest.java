/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.similarityGraph;

import org.apache.flink.runtime.client.JobExecutionException;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * JUnit test for {@link SimilarityGraphQuality}
 */
public class SimilarityGraphQualityTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  @Test(expected = JobExecutionException.class)
  public void testForMissingGoldenTruthIdTitleExpectException() throws Exception {
    String graphString = "similarityGraph[" +
      "(v0 {id:0})" +
      "(v1 {id:1, gtId: \"gt2\"})" +
      "(v2 {id:2, gtId: \"gt2\"})" +
      "(v3 {id:3, gtId: \"gt3\"})" +
      "(v4 {id:4, gtId: \"gt3\"})" +
      "(v5 {id:5, gtId: \"gt3\"})" +
      "(v6 {id:6, gtId: \"gt3\"})" +
      "(v0 {})-[]->(v1)" +
      "(v0 {})-[]->(v2)" +
      "(v2 {})-[]->(v1)" +
      "(v4 {})-[]->(v5)" +
      "(v4 {})-[]->(v6)" +
      "(v5 {})-[]->(v6)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQuality measurer =
      new SimilarityGraphQuality(inputGraph.getVertices(), inputGraph.getEdges(), "gtId");

    measurer.computeQuality();
  }

  @Test
  public void test() throws Exception {
    String graphString = "similarityGraph[" +
      "(v0 {id:0, gtId: \"gt1\"})" +
      "(v1 {id:1, gtId: \"gt2\"})" +
      "(v2 {id:2, gtId: \"gt2\"})" +
      "(v3 {id:3, gtId: \"gt3\"})" +
      "(v4 {id:4, gtId: \"gt3\"})" +
      "(v5 {id:5, gtId: \"gt3\"})" +
      "(v6 {id:6, gtId: \"gt3\"})" +
      "(v0 {})-[]->(v1)" +
      "(v0 {})-[]->(v2)" +
      "(v2 {})-[]->(v1)" +
      "(v4 {})-[]->(v5)" +
      "(v4 {})-[]->(v6)" +
      "(v5 {})-[]->(v6)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQuality measurer =
      new SimilarityGraphQuality(inputGraph.getVertices(), inputGraph.getEdges(), "gtId");

    measurer.computeQuality();

    assertEquals(4, measurer.getTruePositives(), 0);
    assertEquals(6, measurer.getAllPositives(), 0);
    assertEquals(7, measurer.getGtRecordNo(), 0);
    assertEquals(0.66666, measurer.computePrecision(), 0.000009);
    assertEquals(0.57142, measurer.computeRecall(), 0.000009);
    assertEquals(0.61538, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testSingletons() throws Exception {
    String graphString = "similarityGraph[" +
      "(v0 {id:0, gtId: \"gt1\"})" +
      "(v1 {id:1, gtId: \"gt2\"})" +
      "(v2 {id:2, gtId: \"gt2\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQuality measurer =
      new SimilarityGraphQuality(inputGraph.getVertices(), inputGraph.getEdges(), "gtId");

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(0, measurer.getAllPositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(0.0, measurer.computePrecision(), 0.0);
    assertEquals(0, measurer.computeRecall(), 0.0);
    assertEquals(0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testWithNoGoldenTruthPair() throws Exception {
    String graphString = "similarityGraph[" +
      "(v0 {id:0, gtId: \"gt1\"})" +
      "(v1 {id:1, gtId: \"gt2\"})" +
      "(v2 {id:2, gtId: \"gt3\"})" +
      "(v0 {})-[]->(v1)" +
      "(v0 {})-[]->(v2)" +
      "(v2 {})-[]->(v1)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");

    SimilarityGraphQuality measurer =
      new SimilarityGraphQuality(inputGraph.getVertices(), inputGraph.getEdges(), "gtId");

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(3, measurer.getAllPositives(), 0);
    assertEquals(0, measurer.getGtRecordNo(), 0);
    assertEquals(0, measurer.computePrecision(), 0.0);
    assertEquals(0, measurer.computeRecall(), 0.0);
    assertEquals(0, measurer.computeFMeasure(), 0.0);
  }
}
