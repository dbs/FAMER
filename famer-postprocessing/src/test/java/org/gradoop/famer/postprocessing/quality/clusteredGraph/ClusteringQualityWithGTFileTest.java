/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.postprocessing.quality.clusteredGraph;

import org.apache.flink.runtime.client.JobExecutionException;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.io.File;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.junit.Assert.assertEquals;

/**
 * JUnit test for {@link ClusteringQualityWithGTFile}
 */
public class ClusteringQualityWithGTFileTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  @Test(expected = JobExecutionException.class)
  public void testForMissingEntityIdTitleExpectException() throws Exception {
    String goldenTruthFilePath = new File(ClusteringQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testGTMatchedPairsWithoutOverlappingClusters.csv").getPath())
      .getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "clusteredGraph[" +
      "(v0 {" + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c3\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c3\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", false);

    measurer.computeQuality();
  }

  @Test
  public void testGTMatchedPairsWithoutOverlappingClusters() throws Exception {
    String goldenTruthFilePath = new File(ClusteringQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testGTMatchedPairsWithoutOverlappingClusters.csv").getPath())
      .getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c3\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c3\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", false);

    measurer.computeQuality();

    assertEquals(4, measurer.getTruePositives(), 0);
    assertEquals(6, measurer.getAllPositives(), 0);
    assertEquals(7, measurer.getGtRecordNo(), 0);
    assertEquals(3, measurer.getClusterNo(), 0);
    assertEquals(3, measurer.getMaxClusterSize(), 0);
    assertEquals(3, measurer.getMinClusterSize(), 0);
    assertEquals(1, measurer.getSingletons(), 0);
    assertEquals(2.33333, measurer.getAverageClusterSize(), 0.000009);
    assertEquals(0.66666, measurer.computePrecision(), 0.000009);
    assertEquals(0.57142, measurer.computeRecall(), 0.000009);
    assertEquals(0.61538, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testGTMatchedPairsSingletons() throws Exception {
    String goldenTruthFilePath = new File(
      ClusteringQualityWithGTFileTest.class.getResource("/goldenTruthFiles/testGTMatchedPairsSingletons.csv")
        .getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c2\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", false);

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(0, measurer.getAllPositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(3, measurer.getClusterNo(), 0);
    assertEquals(1, measurer.getMaxClusterSize(), 0);
    assertEquals(0, measurer.getMinClusterSize(), 0);
    assertEquals(3, measurer.getSingletons(), 0);
    assertEquals(1, measurer.getAverageClusterSize(), 0.0);
    assertEquals(0.0, measurer.computePrecision(), 0.0);
    assertEquals(0, measurer.computeRecall(), 0.0);
    assertEquals(0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testGTMatchedPairsWithNoGoldenTruthPair() throws Exception {
    String goldenTruthFilePath = new File(ClusteringQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testGTMatchedPairsWithNoGoldenTruthPair.csv").getPath())
      .getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", false);

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(3, measurer.getAllPositives(), 0);
    assertEquals(0, measurer.getGtRecordNo(), 0);
    assertEquals(1, measurer.getClusterNo(), 0);
    assertEquals(3, measurer.getMaxClusterSize(), 0);
    assertEquals(3, measurer.getMinClusterSize(), 0);
    assertEquals(0, measurer.getSingletons(), 0);
    assertEquals(3, measurer.getAverageClusterSize(), 0.0);
    assertEquals(0, measurer.computePrecision(), 0.0);
    assertEquals(0, measurer.computeRecall(), 0.0);
    assertEquals(0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testGTMatchedPairsWithOverlappingClusters() throws Exception {
    String goldenTruthFilePath = new File(ClusteringQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testGTMatchedPairsWithOverlappingClusters.csv").getPath())
      .getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1,c2\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1,c2,c3\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c2\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c3\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c4\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c4\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c5\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", true);

    measurer.computeQuality();

    assertEquals(2, measurer.getTruePositives(), 0);
    assertEquals(5, measurer.getAllPositives(), 0);
    assertEquals(5, measurer.getGtRecordNo(), 0);
    assertEquals(5, measurer.getClusterNo(), 0);
    assertEquals(3, measurer.getMaxClusterSize(), 0);
    assertEquals(2, measurer.getMinClusterSize(), 0);
    assertEquals(1, measurer.getSingletons(), 0);
    assertEquals(2, measurer.getAverageClusterSize(), 0.000009);
    assertEquals(0.4, measurer.computePrecision(), 0.000009);
    assertEquals(0.4, measurer.computeRecall(), 0.000009);
    assertEquals(0.4, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testGTMatchedPairsIllFormattedGTFile() throws Exception {
    String goldenTruthFilePath = new File(ClusteringQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testGTMatchedPairsIllFormattedGTFile.csv").getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.MATCHED_PAIRS);
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c3\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c3\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", false);

    measurer.computeQuality();

    assertEquals(4, measurer.getTruePositives(), 0);
    assertEquals(6, measurer.getAllPositives(), 0);
    assertEquals(7, measurer.getGtRecordNo(), 0);
    assertEquals(3, measurer.getClusterNo(), 0);
    assertEquals(3, measurer.getMaxClusterSize(), 0);
    assertEquals(3, measurer.getMinClusterSize(), 0);
    assertEquals(1, measurer.getSingletons(), 0);
    assertEquals(2.33333, measurer.getAverageClusterSize(), 0.000009);
    assertEquals(0.66666, measurer.computePrecision(), 0.000009);
    assertEquals(0.57142, measurer.computeRecall(), 0.000009);
    assertEquals(0.61538, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testGTEntityIdGTIdWithoutOverlappingClusters() throws Exception {
    String goldenTruthFilePath = new File(ClusteringQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testGTEntityIdGTIdWithoutOverlappingClusters.csv").getPath())
      .getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR);
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c3\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c3\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", false);

    measurer.computeQuality();

    assertEquals(4, measurer.getTruePositives(), 0);
    assertEquals(6, measurer.getAllPositives(), 0);
    assertEquals(7, measurer.getGtRecordNo(), 0);
    assertEquals(3, measurer.getClusterNo(), 0);
    assertEquals(3, measurer.getMaxClusterSize(), 0);
    assertEquals(3, measurer.getMinClusterSize(), 0);
    assertEquals(1, measurer.getSingletons(), 0);
    assertEquals(2.33333, measurer.getAverageClusterSize(), 0.000009);
    assertEquals(0.66666, measurer.computePrecision(), 0.000009);
    assertEquals(0.57142, measurer.computeRecall(), 0.000009);
    assertEquals(0.61538, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testGTEntityIdGTIdSingletons() throws Exception {
    String goldenTruthFilePath = new File(
      ClusteringQualityWithGTFileTest.class.getResource("/goldenTruthFiles/testGTEntityIdGTIdSingletons.csv")
        .getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR);
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c2\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", false);

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(0, measurer.getAllPositives(), 0);
    assertEquals(1, measurer.getGtRecordNo(), 0);
    assertEquals(3, measurer.getClusterNo(), 0);
    assertEquals(1, measurer.getMaxClusterSize(), 0);
    assertEquals(0, measurer.getMinClusterSize(), 0);
    assertEquals(3, measurer.getSingletons(), 0);
    assertEquals(1, measurer.getAverageClusterSize(), 0.0);
    assertEquals(0.0, measurer.computePrecision(), 0.0);
    assertEquals(0, measurer.computeRecall(), 0.0);
    assertEquals(0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testGTEntityIdGTIdWithNoGoldenTruthPair() throws Exception {
    String goldenTruthFilePath = new File(ClusteringQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testGTEntityIdGTIdWithNoGoldenTruthPair.csv").getPath())
      .getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR);
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", false);

    measurer.computeQuality();

    assertEquals(0, measurer.getTruePositives(), 0);
    assertEquals(3, measurer.getAllPositives(), 0);
    assertEquals(0, measurer.getGtRecordNo(), 0);
    assertEquals(1, measurer.getClusterNo(), 0);
    assertEquals(3, measurer.getMaxClusterSize(), 0);
    assertEquals(3, measurer.getMinClusterSize(), 0);
    assertEquals(0, measurer.getSingletons(), 0);
    assertEquals(3, measurer.getAverageClusterSize(), 0.0);
    assertEquals(0, measurer.computePrecision(), 0.0);
    assertEquals(0, measurer.computeRecall(), 0.0);
    assertEquals(0, measurer.computeFMeasure(), 0.0);
  }

  @Test
  public void testGTEntityIdGTIdWithOverlappingClusters() throws Exception {
    String goldenTruthFilePath = new File(ClusteringQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testGTEntityIdGTIdWithOverlappingClusters.csv").getPath())
      .getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR);
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1,c2\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1,c2,c3\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c2\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c3\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c4\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c4\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c5\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", true);

    measurer.computeQuality();

    assertEquals(2, measurer.getTruePositives(), 0);
    assertEquals(5, measurer.getAllPositives(), 0);
    assertEquals(5, measurer.getGtRecordNo(), 0);
    assertEquals(5, measurer.getClusterNo(), 0);
    assertEquals(3, measurer.getMaxClusterSize(), 0);
    assertEquals(2, measurer.getMinClusterSize(), 0);
    assertEquals(1, measurer.getSingletons(), 0);
    assertEquals(2, measurer.getAverageClusterSize(), 0.000009);
    assertEquals(0.4, measurer.computePrecision(), 0.000009);
    assertEquals(0.4, measurer.computeRecall(), 0.000009);
    assertEquals(0.4, measurer.computeFMeasure(), 0.000009);
  }

  @Test
  public void testGTEntityIdGTIdIllFormattedGTFile() throws Exception {
    String goldenTruthFilePath = new File(ClusteringQualityWithGTFileTest.class
      .getResource("/goldenTruthFiles/testGTEntityIdGTIdIllFormattedGTFile.csv").getPath()).getAbsolutePath();
    GTFileComponent gtFileComponent =
      new GTFileComponent(goldenTruthFilePath, ",", GoldenTruthFileType.ENTITY_ID_GT_ID_PAIR);
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c3\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c3\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
    ClusteringQualityWithGTFile measurer =
      new ClusteringQualityWithGTFile(inputGraph.getVertices(), gtFileComponent, "id", false);

    measurer.computeQuality();

    assertEquals(4, measurer.getTruePositives(), 0);
    assertEquals(6, measurer.getAllPositives(), 0);
    assertEquals(7, measurer.getGtRecordNo(), 0);
    assertEquals(3, measurer.getClusterNo(), 0);
    assertEquals(3, measurer.getMaxClusterSize(), 0);
    assertEquals(3, measurer.getMinClusterSize(), 0);
    assertEquals(1, measurer.getSingletons(), 0);
    assertEquals(2.33333, measurer.getAverageClusterSize(), 0.000009);
    assertEquals(0.66666, measurer.computePrecision(), 0.000009);
    assertEquals(0.57142, measurer.computeRecall(), 0.000009);
    assertEquals(0.61538, measurer.computeFMeasure(), 0.000009);
  }
}
