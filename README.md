[![Apache License, Version 2.0, January 2004](https://img.shields.io/github/license/apache/maven.svg?label=License)](https://www.apache.org/licenses/LICENSE-2.0)

## FAMER: FAst Multi-source Entity Resolution system

[FAMER](https://dbs.uni-leipzig.de/de/research/projects/object_matching/famer) is a scalable open source
 (ALv2) research framework for distributed multi-source entity resolution. It can construct similarity
  graphs for entities of multiple sources based on different linking schemes; existing links from the Web
   of Data could also be used to build the similarity graph. FAMER also provides several entity clustering
    schemes. They use the similarity graph to determine groups of matching entities aiming at maximizing
     the similarity between entities within a cluster and minimizing the similarity between entities of
      different clusters. Moreover, FAMER is able to repair clusters, e.g. that are overlapping and/or
       source-inconsistent.
 
FAMER is also able to perform the incremental matching process. The approach uses a so-called clustered
 similarity graph, i.e., a similarity graph reflecting already determined clusters. The input of the
  workflow is a stream of new entities from existing sources or from a new source plus the already
   determined clustered similarity graph from previous iterations. The incremental Linking and Clustering
   /Repairing part supports two general approaches for integrating the group of new entities into clusters.
    In the base (non-repairing) approach the new entities are either added to a similar existing cluster
    or they form a new cluster. A more sophisticated approach is able to repair existing clusters to
     achieve a better cluster assignment for new entities by reclustering a portion of the existing
      clustered graph. The output of incremental clustering is a fully clustered graph. The clusters can
       optionally be fused in the Fusion component so that all entities are represented by a single entity
        called cluster representative.

FAMER is implemented using [Apache Flink](http://flink.apache.org/) so that the calculation of similarity
 graphs and the clustering approaches can be executed in parallel on clusters of variable size. In addition,
  we utilize [Gradoop](http://www.gradoop.com) with its underlying graph structures. We have also developed
   a visualization tool, SIMG-Viz to visually analyze the similarity graphs and clusters determined by FAMER.

**Main Aims**
 * Efficient parallel execution of match workflows in the cloud
 * Efficient application of clustering schemes for entity matching
 * Efficient methods for entity matching repairing
 * Efficient parallel execution for incremental linking and clustering.

Note, that FAMER is **work in progress** which means APIs may change. It is currently used as a proof of
 concept implementation and far from production ready.

The project's documentation can be found in our [Wiki](https://git.informatik.uni-leipzig.de/dbs/FAMER/-/wikis/home).

## Build FAMER from source
 * FAMER requires Java 8 with Maven and is currently running on Flink 1.9.3
 * Clone FAMER into your local file system:
   * `git clone https://git.informatik.uni-leipzig.de/dbs/FAMER.git`
 * Build an executable JAR containing all dependencies, this process also runs all tests and code checks:
   * `cd FAMER`
   * `mvn clean install`
 * The built JAR can than be executed with a given JSON configuration, see [here](https://git.informatik.uni-leipzig.de/dbs/FAMER/-/wikis/home#famer-usage-with-json-configuration) for more details

## Use FAMER via Maven
To include FAMER as a Maven dependency in your project, add the following to your pom.xml:
 * declare remote gitlab repository
```
<repositories>
    <repository>
        <id>gitlab-f</id>
        <url>https://git.informatik.uni-leipzig.de/api/v4/projects/933/packages/maven</url>
    </repository>
</repositories>
```
 * include FAMER configuration package (or any other FAMER package available under [packages](https://git.informatik.uni-leipzig.de/dbs/FAMER/-/packages))
```
<dependencies>
    <dependency>
        <groupId>org.gradoop.famer</groupId>
        <artifactId>famer-configuration</artifactId>
        <version>0.1.0-SNAPSHOT</version>
    </dependency>
</dependencies>
```


 
## Service Desk
 If you encounter any problems while using FAMER, discover bugs or have suggestions for additional features, you can report this via [EMail](mailto:gitmail+dbs-famer-933-issue-@informatik.uni-leipzig.de).
 We will do our best to solve these problems. Please describe the desired feature or a problem in as much detail as possible and attach any error messages that may appear. As soon as an issue is opened you will receive an email. For further contributions to this issue you can reply to the email address given in the header under "Reply to" - the content of your email will then be added as a new contribution to the issue comments.
