/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.cartesianProduct;

import org.apache.flink.api.common.functions.CrossFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.methods.BlockingExecutor;
import org.gradoop.famer.linking.blocking.methods.dataStructures.CartesianProductComponent;

/**
 * The implementation of the parallel cartesian product.
 */
public class CartesianProduct implements BlockingExecutor {

  /**
   * Blocking component structure with required parameters and helper methods
   */
  private final CartesianProductComponent cartesianProductComponent;

  /**
   * Creates an instance of CartesianProduct
   *
   * @param cartesianProductComponent Blocking component structure with required parameters and helper methods
   */
  public CartesianProduct(CartesianProductComponent cartesianProductComponent) {
    this.cartesianProductComponent = cartesianProductComponent;
  }

  @Override
  public DataSet<Tuple2<EPGMVertex, EPGMVertex>> generatePairedVertices(DataSet<EPGMVertex> vertices) {

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> crossedVertices = vertices.cross(vertices)
      .with((CrossFunction<EPGMVertex, EPGMVertex, Tuple2<EPGMVertex, EPGMVertex>>) (v1, v2) -> {
        // avoid self-pairs (loops) and symmetric pairs
        if (v1.getId().compareTo(v2.getId()) < 0) {
          if (cartesianProductComponent.isAllowedPair(v1, v2)) {
            return Tuple2.of(v1, v2);
          }
        }
        // this fake loop pair will be filtered out later
        return Tuple2.of(v1, v1);
      }).returns(new TypeHint<Tuple2<EPGMVertex, EPGMVertex>>() { });

    // filter self-loops
    crossedVertices = crossedVertices.filter(c -> !c.f0.getId().equals(c.f1.getId()));

    return crossedVertices;
  }
}
