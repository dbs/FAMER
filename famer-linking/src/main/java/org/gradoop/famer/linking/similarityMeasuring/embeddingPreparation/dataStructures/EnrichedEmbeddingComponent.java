/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.embeddingPreparation.dataStructures;

import org.gradoop.famer.linking.similarityMeasuring.dataStructures.EmbeddingComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponentBaseConfig;

import java.net.URI;

/**
 * Extended class from EmbeddingComponent, contains information to deal with FastText.
 */
public class EnrichedEmbeddingComponent extends EmbeddingComponent {

  /**
   * Available fast text models
   */
  public enum FastTextModelMode {
    /**
     * USe word-vector file
     */
    USE_WORD_VECTORS,
    /**
     * Use pretrained model
     */
    USE_PRE_TRAINED_MODEL,
    /**
     * Train new model
     */
    TRAIN_NEW_MODEL
  }

  /**
   * Available modes for model training
   */
  public enum FastTextTrainingMode {
    /**
     * Continuous Bag Of Words, prediction via context
     */
    CBOW,
    /**
     * Prediction via nearby words
     */
    SKIPGRAM
  }

  /**
   * Which model FastText uses
   */
  private final FastTextModelMode fastTextModelMode;

  /**
   * How the FastText model is trained
   */
  private final FastTextTrainingMode fastTextTrainingMode;

  /**
   * URI for the input file, which can be either the word-vector file or the file for the pretrained model
   */
  private final URI inputFileUri;

  /**
   * URI for the word-vector file created by FastText
   */
  private final URI outputFileUri;

  /**
   * Bucket size for the training model
   */
  private final int trainingModelBucket;

  /**
   * Dimension for the training model
   */
  private final int trainingModelDimension;

  /**
   * Minimal word count for the training model
   */
  private final int trainingModelMinWordCount;

  /**
   * Creates an instance of EnrichedEmbeddingComponent
   *
   * @param baseConfig The base configuration for the similarity component
   * @param fastTextModelMode Which model FastText uses
   * @param fastTextTrainingMode How the FastText model is trained
   * @param inputFileUri URI for the input file, which can be either the word-vector file or the file for
   *                     the pretrained model
   * @param outputFileUri URI for the word-vector file created by FastText
   * @param trainingModelBucket Bucket size for the training model
   * @param trainingModelDimension Dimension for the training model
   * @param trainingModelMinWordCount Minimal word count for the training model
   */
  public EnrichedEmbeddingComponent(SimilarityComponentBaseConfig baseConfig,
    FastTextModelMode fastTextModelMode, FastTextTrainingMode fastTextTrainingMode, String inputFileUri,
    String outputFileUri, int trainingModelBucket, int trainingModelDimension,
    int trainingModelMinWordCount) {
    super(baseConfig, null);
    this.fastTextModelMode = fastTextModelMode;
    this.fastTextTrainingMode = fastTextTrainingMode;
    this.inputFileUri = URI.create(inputFileUri);
    this.outputFileUri = URI.create(outputFileUri);
    this.trainingModelBucket = trainingModelBucket;
    this.trainingModelDimension = trainingModelDimension;
    this.trainingModelMinWordCount = trainingModelMinWordCount;
  }

  /**
   * Whether to use given word-vectors
   *
   * @return True, if given word-vectors shall be used
   */
  public boolean isUseWordVectorsEnabled() {
    return fastTextModelMode.equals(FastTextModelMode.USE_WORD_VECTORS);
  }

  /**
   * Whether to use a pretrained model to create the word-vectors
   *
   * @return True, if a pretrained model shall be used
   */
  public boolean isUsePretrainedModelEnabled() {
    return fastTextModelMode.equals(FastTextModelMode.USE_PRE_TRAINED_MODEL);
  }

  /**
   * Whether to train a new model to create the word-vectors
   *
   * @return True, if a new model shall be used
   */
  public boolean isTrainModelEnabled() {
    return fastTextModelMode.equals(FastTextModelMode.TRAIN_NEW_MODEL);
  }

  /**
   * Whether CBOW is used for training
   *
   * @return True, if CBOW is used
   */
  public boolean isUseCBOWTraining() {
    return fastTextTrainingMode.equals(FastTextTrainingMode.CBOW);
  }

  /**
   * Whether Skipgram is used for training
   *
   * @return True, if Skipgram is used
   */
  public boolean isUseSkipgramTraining() {
    return fastTextTrainingMode.equals(FastTextTrainingMode.SKIPGRAM);
  }

  /**
   * Returns the input file URI
   *
   * @return the input file URI
   */
  public URI getInputFileUri() {
    return inputFileUri;
  }

  /**
   * Returns the output file URI
   *
   * @return the output file URI
   */
  public URI getOutputFileUri() {
    return outputFileUri;
  }

  /**
   * Returns the training model dimension
   *
   * @return the training model dimension
   */
  public int getTrainingModelDimension() {
    return trainingModelDimension;
  }

  /**
   * Returns the training model minimal word count
   *
   * @return the training model minimal word count
   */
  public int getTrainingModelMinWordCount() {
    return trainingModelMinWordCount;
  }

  /**
   * Returns the training model bucket size
   *
   * @return the training model bucket size
   */
  public int getTrainingModelBucket() {
    return trainingModelBucket;
  }
}
