/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

/**
 * Computes the similarity degree of two strings using the EditDistanceLevenshtein method.
 */
public class EditDistanceLevenshtein implements SimilarityComputation<String> {

  @Override
  public double computeSimilarity(String str1, String str2) throws Exception {
    if (str1 == null) {
      throw new NullPointerException("str1 must not be null");
    }
    if (str2 == null) {
      throw new NullPointerException("str2 must not be null");
    }
    int m = str1.length();
    int n = str2.length();
    if (m == 0 || n == 0) {
      return 0.0;
    }
    char[] str1Array = str1.toCharArray();
    char[] str2Array = str2.toCharArray();
    short[][] dis = new short[m + 1][n + 1];
    for (int i = 0; i <= n; i++) {
      dis[0][i] = (short) i;
    }
    for (int i = 0; i <= m; i++) {
      dis[i][0] = (short) i;
    }
    for (int i = 1; i <= m; i++) {
      for (int j = 1; j <= n; j++) {
        if (str1Array[i - 1] == str2Array[j - 1]) {
          dis[i][j] = dis[i - 1][j - 1];
        } else {
          dis[i][j] = (short) Math.min(dis[i - 1][j] + 1, dis[i][j - 1] + 1);
          dis[i][j] = (short) Math.min(dis[i][j], dis[i - 1][j - 1] + 1);
        }
      }
    }

    return 1 - dis[m - 1][n - 1] / (double) Math.max(m, n);
  }

  @Override
  public String parsePropertyValue(PropertyValue value) {
    return getString(value);
  }
}
