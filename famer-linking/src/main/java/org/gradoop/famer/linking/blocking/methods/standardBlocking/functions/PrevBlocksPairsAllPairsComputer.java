/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Computes the number of all previous pairs for each block and also the number of all pairs. Returns a
 * {Tuple5<BlockingKey, BlockSize, Index, PrevBlocksPairs, AllPairs>}.
 */
public class PrevBlocksPairsAllPairsComputer implements
  GroupReduceFunction<Tuple3<String, Long, Long>, Tuple5<String, Long, Long, Long, Long>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple5<String, Long, Long, Long, Long> reuseTuple = new Tuple5<>();

  @Override
  public void reduce(Iterable<Tuple3<String, Long, Long>> group,
    Collector<Tuple5<String, Long, Long, Long, Long>> out) {

    List<Tuple3<String, Long, Long>> groupAsList = new ArrayList<>();
    group.forEach(groupAsList::add);

    @SuppressWarnings("unchecked")
    Tuple3<String, Long, Long>[] keySizeIndexArray = groupAsList.toArray(new Tuple3[groupAsList.size()]);

    Arrays.sort(keySizeIndexArray, Comparator.comparing(tuple -> tuple.f2));
    long allPairs = 0L;
    long[] sumPrev = new long[keySizeIndexArray.length];

    for (int i = 0; i < keySizeIndexArray.length; i++) {
      sumPrev[i] = allPairs;
      long size = keySizeIndexArray[i].f1;
      allPairs += (size * (size - 1)) / 2;
    }

    for (int i = 0; i < keySizeIndexArray.length; i++) {
      reuseTuple.f0 = keySizeIndexArray[i].f0;
      reuseTuple.f1 = keySizeIndexArray[i].f1;
      reuseTuple.f2 = keySizeIndexArray[i].f2;
      reuseTuple.f3 = sumPrev[i];
      reuseTuple.f4 = allPairs;
      out.collect(reuseTuple);
    }
  }
}
