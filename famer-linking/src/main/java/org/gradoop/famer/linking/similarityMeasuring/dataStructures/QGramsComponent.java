/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.methods.QGrams;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

/**
 * Similarity component which uses {@link QGrams}
 */
public class QGramsComponent extends SimilarityComponent {

  /**
   * The q parameter
   */
  private final int length;

  /**
   * The padding parameter
   */
  private final boolean padding;

  /**
   * The second method for computing similarity
   */
  private final QGrams.SecondMethod method;

  /**
   * Creates an instance of QGramsComponent from the given json object.
   *
   * @param config The json object that contains the configuration.
   */
  public QGramsComponent(JSONObject config) {
    super(config);
    try {
      this.length = config.getInt("length");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("QGramsComponent: value for length " +
        "could not be found or parsed to int.", ex);
    }
    try {
      this.padding = config.getBoolean("padding");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("QGramsComponent: value for padding " +
        "could not be found or parsed to boolean.", ex);
    }
    try {
      this.method = QGrams.SecondMethod.valueOf(config.getString("secondMethod"));
    } catch (Exception ex) {
      throw new UnsupportedOperationException("QGramsComponent: value for secondMethod " +
        "could not be found or parsed.", ex);
    }
  }

  /**
   * Creates an instance of QGramsComponent
   *
   * @param baseConfig The base configuration for the similarity component
   * @param length The q parameter
   * @param padding The padding parameter
   * @param secondMethod The second method for computing similarity
   */
  public QGramsComponent(SimilarityComponentBaseConfig baseConfig, int length, boolean padding,
    QGrams.SecondMethod secondMethod) {
    super(baseConfig);
    this.length = length;
    this.padding = padding;
    this.method = secondMethod;
  }

  @Override
  public SimilarityComputation<String> buildSimilarityComputation() {
    return new QGrams(length, padding, method);
  }

  public int getLength() {
    return length;
  }

  public boolean isPadding() {
    return padding;
  }

  public QGrams.SecondMethod getMethod() {
    return method;
  }
}
