/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

/**
 * The structure that keeps the similarity value
 */
public class SimilarityField {

  /**
   * The unique identifier of the similarity field
   */
  private final String id;

  /**
   * The computed similarity value
   */
  private final double similarityValue;

  /**
   * The effective coefficient of the similarity value
   */
  private final double weight;

  /**
   * Creates an instance of SimilarityField
   *
   * @param id The unique identifier of the SimilarityField
   * @param similarityValue The computed similarity value
   * @param weight The effective coefficient of the similarity value
   */
  public SimilarityField(String id, double similarityValue, double weight) {
    this.id = id;
    this.similarityValue = similarityValue;
    this.weight = weight;
  }

  /**
   * Returns the similarity value
   *
   * @return the similarity value
   */
  public double getSimilarityValue() {
    return similarityValue;
  }

  /**
   * Returns the unique identifier of the similarity field
   *
   * @return the unique identifier of the similarity field
   */
  public String getId() {
    return id;
  }

  /**
   * Returns the effective coefficient of the similarity value
   *
   * @return the effective coefficient of the similarity value
   */
  public double getWeight() {
    return weight;
  }
}
