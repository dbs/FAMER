/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Joins a {@code Tuple3<BlockingKey, PartitionId, StartPoint>} with a
 * {@code Tuple3<Vertex, BlockingKey, PartitionId>} where BlockingKey and PartitionId is the same. Returns a
 * {@code Tuple4<Vertex, BlockingKey, PartitionId, StartPoint>}.
 */
@FunctionAnnotation.ForwardedFieldsFirst("f2->f3")
@FunctionAnnotation.ForwardedFieldsSecond("f0;f1;f2")
public class JoinOnBlockingKeyPartitionIdAndAttachStartPoint
  implements JoinFunction<Tuple3<String, Integer, Long>, Tuple3<EPGMVertex, String, Integer>,
  Tuple4<EPGMVertex, String, Integer, Long>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple4<EPGMVertex, String, Integer, Long> reuseTuple = new Tuple4<>();

  @Override
  public Tuple4<EPGMVertex, String, Integer, Long> join(
    Tuple3<String, Integer, Long> blockingKeyPartitionIdStartPoint,
    Tuple3<EPGMVertex, String, Integer> vertexBlockingKeyPartitionId) throws Exception {
    reuseTuple.f0 = vertexBlockingKeyPartitionId.f0;
    reuseTuple.f1 = vertexBlockingKeyPartitionId.f1;
    reuseTuple.f2 = vertexBlockingKeyPartitionId.f2;
    reuseTuple.f3 = blockingKeyPartitionIdStartPoint.f2;
    return reuseTuple;
  }
}
