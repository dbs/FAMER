/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

import java.util.Arrays;

/**
 * Computes the similarity degree of two strings using the ExtendedJaccard method
 */
public class ExtendedJaccard implements SimilarityComputation<String> {

  /**
   * The tokenizer string parameter
   */
  private final String tokenizer;

  /**
   * The threshold a similarity value must reach or exceed
   */
  private final double threshold;

  /**
   * The threshold used for the nested jaro winkler algorithm
   */
  private final double jaroWinklerThreshold;

  /**
   * Creates an instance of ExtendedJaccard
   *
   * @param tokenizer The tokenizer string parameter
   * @param threshold The threshold a similarity value must reach or exceed
   * @param jaroWinklerThreshold The threshold used for the nested jaro winkler algorithm
   */
  public ExtendedJaccard(String tokenizer, double threshold, double jaroWinklerThreshold) {
    this.tokenizer = tokenizer;
    this.threshold = threshold;
    this.jaroWinklerThreshold = jaroWinklerThreshold;
  }

  @Override
  public double computeSimilarity(String str1, String str2) throws Exception {
    if (str1 == null) {
      throw new NullPointerException("str1 must not be null");
    }
    if (str2 == null) {
      throw new NullPointerException("str2 must not be null");
    }
    double simDegree;
    String[] str1tokens = str1.split(tokenizer);
    String[] str2tokens = str2.split(tokenizer);
    JaroWinkler jaroWinkler = new JaroWinkler(jaroWinklerThreshold);

    Boolean[] similarStr1tokens = new Boolean[str1tokens.length];
    Boolean[] similarStr2tokens = new Boolean[str2tokens.length];

    Arrays.fill(similarStr1tokens, false);
    Arrays.fill(similarStr2tokens, false);

    for (int i = 0; i < str1tokens.length; i++) {
      for (int j = 0; j < str2tokens.length; j++) {
        double jaroWinklerSim = jaroWinkler.computeSimilarity(str1tokens[i], str2tokens[j]);
        if (jaroWinklerSim >= threshold) {
          similarStr1tokens[i] = true;
          similarStr2tokens[j] = true;
        }
      }
    }
    int similarStr1tokenNo = 0;
    int similarStr2tokenNo = 0;

    for (boolean similarStr1token : similarStr1tokens) {
      if (similarStr1token) {
        similarStr1tokenNo++;
      }
    }

    for (boolean similarStr2token : similarStr2tokens) {
      if (similarStr2token) {
        similarStr2tokenNo++;
      }
    }

    int similarTokensSet = Math.max(similarStr1tokenNo, similarStr2tokenNo);

    simDegree = (double) similarTokensSet /
      (similarStr1tokens.length - similarStr1tokenNo + similarStr2tokens.length - similarStr2tokenNo +
        similarTokensSet);
    return simDegree;
  }

  @Override
  public String parsePropertyValue(PropertyValue value) {
    return getString(value);
  }
}
