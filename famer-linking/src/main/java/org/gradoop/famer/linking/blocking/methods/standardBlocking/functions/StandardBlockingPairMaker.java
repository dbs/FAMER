/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.GroupCombineFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingComponent;

import java.util.ArrayList;
import java.util.List;

import static org.gradoop.famer.linking.blocking.methods.standardBlocking.StandardBlocking.KEY_ADDED_FLAG;

/**
 * Takes a sorted group of {@code Tuple5<Vertex, BlockingKey, Index, IsLastReducer, ReducerId>}, grouped by
 * BlockingKey and sorted by Index ascending. Creates pairs of vertices for each group.
 */
public class StandardBlockingPairMaker implements
  GroupCombineFunction<Tuple5<EPGMVertex, String, Long, Boolean, Integer>, Tuple2<EPGMVertex, EPGMVertex>> {

  /**
   * Blocking component structure with required parameters and helper methods
   */
  private StandardBlockingComponent standardBlockingComponent;

  /**
   * Reduce object instantiation
   */
  private final Tuple2<EPGMVertex, EPGMVertex> reuseTuple;

  /**
   * Creates an instance of StandardBlockingPairMaker
   *
   * @param standardBlockingComponent Blocking component structure with required parameters and helper methods
   */
  public StandardBlockingPairMaker(StandardBlockingComponent standardBlockingComponent) {
    this.standardBlockingComponent = standardBlockingComponent;
    this.reuseTuple = new Tuple2<>();
  }

  @Override
  public void combine(Iterable<Tuple5<EPGMVertex, String, Long, Boolean, Integer>> group,
    Collector<Tuple2<EPGMVertex, EPGMVertex>> out) {
    List<Tuple3<EPGMVertex, Boolean, Boolean>> vertices = new ArrayList<>();

    for (Tuple5<EPGMVertex, String, Long, Boolean, Integer> groupItem : group) {
      boolean hasKAFlag = false;
      if (groupItem.f0.hasProperty(KEY_ADDED_FLAG)) {
        hasKAFlag = true;
        groupItem.f0.removeProperty(KEY_ADDED_FLAG);
      }
      vertices.add(Tuple3.of(groupItem.f0, groupItem.f3, hasKAFlag));
    }

    @SuppressWarnings("unchecked")
    Tuple3<EPGMVertex, Boolean, Boolean>[] verticesArray = vertices.toArray(new Tuple3[vertices.size()]);

    int verticesArraySize = verticesArray.length;
    for (int i = 0; i < verticesArraySize && verticesArray[i].f1; i++) {
      for (int j = i + 1; j < verticesArraySize; j++) {

        EPGMVertex vertex1 = verticesArray[i].f0;
        EPGMVertex vertex2 = verticesArray[j].f0;

        if (verticesArray[i].f2 && verticesArray[j].f2) {
          continue;
        }

        if (standardBlockingComponent.isAllowedPair(vertex1, vertex2)) {
          reuseTuple.f0 = vertex1;
          reuseTuple.f1 = vertex2;
          out.collect(reuseTuple);
        }
      }
    }
  }
}
