/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.properties.PropertyValue;

import java.util.List;

/**
 * Computes the similarity degree of two geographical points from their geographical coordinates
 */
public class GeoDistance implements SimilarityComputation<Tuple2<PropertyValue, PropertyValue>> {

  /**
   * The maximum tolerated distance in km
   */
  private double maxToleratedDistance;

  /**
   * Creates an instance of GeoDistance
   *
   * @param maxToleratedDistance The maximum tolerated distance in km
   */
  public GeoDistance(double maxToleratedDistance) {
    this.maxToleratedDistance = maxToleratedDistance;
  }

  /**
   * Computes the similarity degree of geographical coordinates
   *
   * @param value1 The first geographical coordinate to compare.
   * @param value2 The second geographical coordinate to compare.
   *
   * @return The similarity value in the range [0, 1]
   *
   * @throws NullPointerException if value1 or value2 is null.
   */
  @Override
  public double computeSimilarity(Tuple2<PropertyValue, PropertyValue> value1,
    Tuple2<PropertyValue, PropertyValue> value2) throws Exception {
    if (value1 == null) {
      throw new NullPointerException("value1 must not be null");
    }
    if (value2 == null) {
      throw new NullPointerException("value2 must not be null");
    }
    double distance = computeDistance(value1, value2);
    if (distance > maxToleratedDistance) {
      return 0;
    }

    return 1 - (distance / maxToleratedDistance);
  }

  @Override
  public Tuple2<PropertyValue, PropertyValue> parsePropertyValue(PropertyValue value) {
    List<PropertyValue> list = value.getList();
    return Tuple2.of(list.get(0), list.get(1));
  }

  /**
   * Computes the distance between two geographical coordinates
   *
   * @param coordinates1 The first geographical coordinate
   * @param coordinates2 The second geographical coordinate
   *
   * @return The distance in km
   *
   * @throws NullPointerException if coordinates1 or coordinates2 is null
   */
  private double computeDistance(Tuple2<PropertyValue, PropertyValue> coordinates1,
    Tuple2<PropertyValue, PropertyValue> coordinates2) {
    if (coordinates1.f0 == null || coordinates1.f1 == null) {
      throw new NullPointerException("coordinates1 must not be null");
    }
    if (coordinates2.f0 == null || coordinates2.f1 == null) {
      throw new NullPointerException("coordinates2 must not be null");
    }
    //input parameters
    double lat1 = coordinates1.f0.getDouble();
    double lon1 = coordinates1.f1.getDouble();
    double lat2 = coordinates2.f0.getDouble();
    double lon2 = coordinates2.f1.getDouble();

    if ((lat1 == lat2) && (lon1 == lon2)) {
      return 0;
    } else {
      double theta = lon1 - lon2;
      double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) +
        Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
      dist = Math.acos(dist);
      dist = Math.toDegrees(dist);
      dist = dist * 60 * 1.1515;
      dist = dist * 1.609344;
      return dist;
    }
  }
}
