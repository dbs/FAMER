/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

import java.util.ArrayList;
import java.util.List;

/**
 * Computes the similarity degree of two strings using the QGram method
 */
public class QGrams implements SimilarityComputation<String> {

  /**
   * Second methods used for the similarity computation
   */
  public enum SecondMethod {
    /**
     * Use overlap
     */
    OVERLAP,
    /**
     * USe jaccard
     */
    JACCARD,
    /**
     * Use dice
     */
    DICE
  }

  /**
   * The q parameter
   */
  private final int length;

  /**
   * The padding parameter
   */
  private final boolean padding;

  /**
   * The second method for computing similarity
   */
  private final SecondMethod secondMethod;

  /**
   * Creates an instance of QGrams
   *
   * @param length The q parameterv
   * @param padding The padding parameter
   * @param secondMethod The second method for computing similarity
   */
  public QGrams(int length, boolean padding, SecondMethod secondMethod) {
    this.length = length;
    this.padding = padding;
    this.secondMethod = secondMethod;
  }

  @Override
  public double computeSimilarity(String str1, String str2) throws Exception {
    if (str1 == null) {
      throw new NullPointerException("str1 must not be null");
    }
    if (str2 == null) {
      throw new NullPointerException("str2 must not be null");
    }
    if (str1.length() < length || str2.length() < length) {
      return str1.equals(str2) ? 1d : 0d;
    }

    double simDegree = 0d;
    List<String> str1QGrams = new ArrayList<>();
    List<String> str2QGrams = new ArrayList<>();
    if (padding) {
      if (str1.length() >= length) {
        str1QGrams.add(str1.substring(0, length - 1));
        str1QGrams.add(str1.substring(str1.length() - length + 1));
      }
      if (str2.length() >= length) {
        str2QGrams.add(str2.substring(0, length - 1));
        str2QGrams.add(str2.substring(str2.length() - length + 1));
      }
    }
    for (int i = 0; i < str1.length() && i + length <= str1.length(); i++) {
      str1QGrams.add(str1.substring(i, i + length));
    }
    for (int i = 0; i < str2.length() && i + length <= str2.length(); i++) {
      str2QGrams.add(str2.substring(i, i + length));
    }

    int str1QGramsSize = str1QGrams.size();
    int str2QGramsSize = str2QGrams.size();

    for (String s : str1QGrams) {
      if (str2QGrams.contains(s)) {
        simDegree++;
        str2QGrams.remove(s);
      }
    }

    switch (secondMethod) {
    case OVERLAP:
      simDegree = simDegree / Math.min(str1QGramsSize, str2QGramsSize);
      break;
    case JACCARD:
      simDegree = simDegree / (str1QGramsSize + str2QGramsSize - simDegree);
      break;
    case DICE:
      simDegree = (float) 2 * simDegree / (str1QGramsSize + str2QGramsSize);
      break;
    default:
      // JACARD
      simDegree = simDegree / (str1QGramsSize + str2QGramsSize - simDegree);
    }
    return simDegree;
  }

  @Override
  public String parsePropertyValue(PropertyValue value) {
    return getString(value);
  }
}
