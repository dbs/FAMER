/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.KeyGeneratorComponent;

import java.util.List;

import static org.gradoop.famer.linking.common.Utils.cleanAttributeValue;

/**
 * FlatMap function that generates the blocking key for each vertex of the input graph using the given
 * {@link #keyGeneratorComponent}. Returns a DataSet of {@code Tuple2<Vertex, BlockingKey>}
 */
public class BlockingKeyGenerator implements FlatMapFunction<EPGMVertex, Tuple2<EPGMVertex, String>> {

  /**
   * The structure that provides the properties and the method for key generation
   */
  private final KeyGeneratorComponent keyGeneratorComponent;

  /**
   * Creates an instance of BlockingKeyGenerator
   *
   * @param keyGeneratorComponent The key generation component that keeps all properties as well as the
   *                               generating method
   */
  public BlockingKeyGenerator(KeyGeneratorComponent keyGeneratorComponent) {
    this.keyGeneratorComponent = keyGeneratorComponent;
  }

  /**
   * Checks whether the key generation method  could generate more than one key for each vertex
   *
   * @return false when the key list contains only one key per entity (vertex)
   */
  public boolean returnsMultipleKeys() {
    return keyGeneratorComponent.buildKeyGenerator().returnsMultipleKeys();
  }

  @Override
  public void flatMap(EPGMVertex vertex, Collector<Tuple2<EPGMVertex, String>> output) throws Exception {

    String attributeValue = "";

    if (vertex.hasProperty(keyGeneratorComponent.getAttribute())) {
      attributeValue = vertex.getPropertyValue(keyGeneratorComponent.getAttribute()).toString();
      attributeValue = cleanAttributeValue(attributeValue);
    }

    if (attributeValue.equals("")) {
      output.collect(Tuple2.of(vertex, ""));
    } else {
      List<String> keys = keyGeneratorComponent.buildKeyGenerator().generateKey(attributeValue);
      for (String key : keys) {
        output.collect(Tuple2.of(vertex, key));
      }
    }
  }
}
