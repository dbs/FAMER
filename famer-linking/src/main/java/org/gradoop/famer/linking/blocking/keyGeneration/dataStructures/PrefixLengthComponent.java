/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.blocking.keyGeneration.methods.KeyGenerator;
import org.gradoop.famer.linking.blocking.keyGeneration.methods.PrefixLength;

/**
 * Key generator component which uses {@link PrefixLength}.
 */
public class PrefixLengthComponent extends KeyGeneratorComponent {

  /**
   * The length of the blocking key
   */
  private final int prefixLength;

  /**
   * Creates an instance of PrefixLengthComponent
   *
   * @param attribute The property / attribute name for generating the blocking keys
   * @param prefixLength The length of the blocking key
   */
  public PrefixLengthComponent(String attribute, int prefixLength) {
    super(attribute);
    this.prefixLength = prefixLength;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The key generation component json config part
   */
  public PrefixLengthComponent(JSONObject jsonConfig) {
    super(jsonConfig);
    try {
      this.prefixLength = jsonConfig.getInt("prefixLength");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("PrefixLengthComponent: value for prefixLength could not " +
        "be found or parsed to int", ex);
    }
  }

  public int getPrefixLength() {
    return prefixLength;
  }

  @Override
  public KeyGenerator buildKeyGenerator() {
    return new PrefixLength(prefixLength);
  }
}
