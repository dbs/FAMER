/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.linking;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMEdgeFactory;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.BlockMaker;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponent;
import org.gradoop.famer.linking.linking.dataStructures.LinkerComponent;
import org.gradoop.famer.linking.linking.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.linking.linking.functions.LinkMaker;
import org.gradoop.famer.linking.linking.functions.Project3To1And2;
import org.gradoop.famer.linking.selection.Selector;
import org.gradoop.famer.linking.similarityMeasuring.SimilarityMeasurer;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.EmbeddingComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;

/**
 * The parallel implementation of the linking task
 */
public class Linker {

  /**
   * The property that keeps the similarity value of an edge
   */
  public static final String SIM_VALUE = "value";

  /**
   * The property that keeps the name of the origin of the entity
   */
  public static final String GRAPH_LABEL = "graphLabel";

  /**
   * Linker component structure with required parameters and helper methods
   */
  private final LinkerComponent linkerComponent;

  /**
   * Creates an instance of Linker
   *
   * @param linkerComponent Linker component structure with required parameters and helper methods
   */
  public Linker(LinkerComponent linkerComponent) {
    this.linkerComponent = linkerComponent;
  }

  /**
   * Registers cache files for about word embedding
   *
   * @param env The Flink execution environment
   * @param linkerComponent Linker component structure with required parameters and helper methods
   */
  public static void registerCachedFiles(ExecutionEnvironment env, LinkerComponent linkerComponent) {
    linkerComponent.getSimilarityComponents().stream()
      .filter(c -> c instanceof EmbeddingComponent)
      .forEach(c -> {
        String wordVectorsFileUri = ((EmbeddingComponent) c).getWordVectorsFileUri().toString();
        env.registerCachedFile(wordVectorsFileUri, ((EmbeddingComponent) c).getDistributedCacheKey());
      });
  }

  /**
   * Executes the linking for a {@link LogicalGraph}
   *
   * @param input The input {@link LogicalGraph}
   *
   * @return The similarity graph
   */
  public LogicalGraph executeLinking(LogicalGraph input) {
    return createSimilarityGraph(input.getConfig(), input.getVertices(), input.getEdges());
  }

  /**
   * Executes the linking for a {@link GraphCollection}
   *
   * @param input The input {@link GraphCollection}
   *
   * @return The similarity graph
   */
  public LogicalGraph executeLinking(GraphCollection input) {
    return createSimilarityGraph(input.getConfig(), input.getVertices(), input.getEdges());
  }

  /**
   * Executes parallel linking by creating links between input vertices. It combines the steps of linking
   * process listed as step 1: BLOCKING, step 2: SIMILARITY MEASURING and step 3: SELECTION
   *
   * @param config The gradoop flink config from the input graph
   * @param inputVertices The vertices from the input graph
   * @param inputEdges The edges from the input graph
   *
   * @return The output similarity {@link LogicalGraph} with linked vertices
   */
  private LogicalGraph createSimilarityGraph(GradoopFlinkConfig config, DataSet<EPGMVertex> inputVertices,
    DataSet<EPGMEdge> inputEdges) {
    /* PRE-PROCESSING */
    ExecutionEnvironment env = config.getExecutionEnvironment();
    registerCachedFiles(env, linkerComponent);
    /* Step 1: BLOCKING */
    DataSet<Tuple2<EPGMVertex, EPGMVertex>> blockedVertices = runBlocking(inputVertices, inputEdges);

    /* Step 2: SIMILARITY MEASURING */
    DataSet<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> blockedVerticesSimilarityFields =
      runSimilarityComputation(blockedVertices);

    /* Step 3: SELECTION */
    DataSet<Tuple3<EPGMVertex, EPGMVertex, Double>> blockedVerticesSimilarityDegree =
      runSimilaritySelectionAndAggregation(blockedVerticesSimilarityFields);

    /* POST-PROCESSING */
    DataSet<EPGMEdge> edges = blockedVerticesSimilarityDegree.map(new LinkMaker(
      new EPGMEdgeFactory(), SIM_VALUE,
      linkerComponent.isTagNeighborCluster(), linkerComponent.isTagNewLink()));

    if (linkerComponent.isKeepCurrentEdges() && !linkerComponent.isRecomputeSimilarityForCurrentEdges()) {
      edges = edges.union(inputEdges);
    }

    return config.getLogicalGraphFactory().fromDataSets(inputVertices, edges);
  }

  /**
   * Runs the blocking for the given graph elements
   *
   * @param inputVertices The vertices of the graph
   * @param inputEdges The edges of the graph
   *
   * @return The blocked vertices as pairs
   */
  public DataSet<Tuple2<EPGMVertex, EPGMVertex>> runBlocking(DataSet<EPGMVertex> inputVertices,
    DataSet<EPGMEdge> inputEdges) {
    DataSet<Tuple2<EPGMVertex, EPGMVertex>> blockedVertices = null;
    for (BlockingComponent blockingComp : linkerComponent.getBlockingComponents()) {
      DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
        new BlockMaker(blockingComp).execute(inputVertices);

      blockedVertices = (blockedVertices == null) ? pairedVertices : blockedVertices.union(pairedVertices);
    }
    assert blockedVertices != null;

    // old comment: remove this feature and block completely - decide whether to use this or the following
    // TODO: check if this is still up-to-date
    if (linkerComponent.isKeepCurrentEdges() && linkerComponent.isRecomputeSimilarityForCurrentEdges()) {
      DataSet<Tuple2<EPGMVertex, EPGMVertex>> existingLinks =
        EdgeToEdgeSourceVertexTargetVertex.execute(inputVertices, inputEdges).map(new Project3To1And2<>());
      blockedVertices = blockedVertices.union(existingLinks);
    }

    return blockedVertices;
  }

  /**
   * Runs the similarity computation for the given blocked vertices
   *
   * @param blockedVertices The blocked vertices as pairs
   *
   * @return The blocked vertices with their corresponding list of similarity fields
   */
  public DataSet<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> runSimilarityComputation(
    DataSet<Tuple2<EPGMVertex, EPGMVertex>> blockedVertices) {
    return blockedVertices.flatMap(new SimilarityMeasurer(linkerComponent.getSimilarityComponents()));
  }

  /**
   * Runs the selection and aggregation of the similarities for the given blocked vertices
   *
   * @param blockedVerticesSimilarityFields The blocked vertices with their corresponding list of
   *                                        similarity fields
   *
   * @return The blocked vertices with their corresponding similarity value
   */
  public DataSet<Tuple3<EPGMVertex, EPGMVertex, Double>> runSimilaritySelectionAndAggregation(
    DataSet<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> blockedVerticesSimilarityFields) {
    return blockedVerticesSimilarityFields.flatMap(new Selector(linkerComponent));
  }
}
