/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring;

import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityField;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

import java.io.Serializable;

/**
 * Computes the similarity of two property values and create the corresponding SimilarityField
 */
public class SimilarityComputer implements Serializable {

  /**
   * Structure for the required parameters and helper methods
   */
  private final SimilarityComponent similarityComponent;

  /**
   * Creates an instance of SimilarityComputer
   *
   * @param similarityComponent Structure for the required parameters and helper methods
   */
  public SimilarityComputer(SimilarityComponent similarityComponent) {
    this.similarityComponent = similarityComponent;
  }

  /**
   * Computes the similarity of two property values and creates the corresponding similarity field
   *
   * @param value1 The first property value
   * @param value2 The second property value
   *
   * @return generated {@link SimilarityField} with the computed similarity value
   */
  public SimilarityField computeSimilarity(PropertyValue value1, PropertyValue value2) throws Exception {
    SimilarityComputation computation = similarityComponent.buildSimilarityComputation();
    double simDegree = computation.computeSimilarity(value1, value2);

    return new SimilarityField(similarityComponent.getComponentId(), simDegree,
      similarityComponent.getWeight());
  }
}
