/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.linking.dataStructures.LinkerComponent;
import org.gradoop.famer.linking.selection.dataStructures.SelectionComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityField;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;

/**
 * <pre>
 * Selects paired vertices based on the selection strategy specified in selection component.
 *
 * The selection component may have both or one of these two main sub-component:
 * 1) selectionRule and
 * 2) aggregationRule,
 *
 * therefore there will be 4 different combinations as follows:
 * 1) no selectionRule, no aggregationRule -> output: (pairedVertices, weightedAverageSimValue)
 * 2) no selectionRule, has aggregationRule ->
 *  2-1) if (aggregationRule has no rule but it has aggregationThreshold)
 *       if (weightedAverageSimValue >= aggregationThreshold)
 *       -> output: (pairedVertices, weightedAverageSimValue)
 *  2-2) if (aggregationRule has rule and also has aggregationThreshold)
 *       if (computedSimValue >= aggregationThreshold)
 *       -> output: (pairedVertices, weightedAverageSimValue)
 * 3) has selectionRuleEnabled, no aggregationRuleEnabled  ->
 *    if (selectionRule is satisfied) -> output: (pairedVertices, weightedAverageSimValue)
 * 4) has selectionRuleEnabled and has aggregationRuleEnabled ->
 *    if (selectionRule is satisfied && computedSimValue >= aggregationThreshold)
 *    -> output: (pairedVertices, computedSimValue)
 * </pre>
 */
public class Selector implements
  FlatMapFunction<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>,
    Tuple3<EPGMVertex, EPGMVertex, Double>> {

  /**
   * Linker component structure with required parameters and helper methods
   */
  private final LinkerComponent linkerComponent;

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMVertex, EPGMVertex, Double> reuseTuple;

  /**
   * Creates an instance of Selector
   *
   * @param linkerComponent Linker component structure with required parameters and helper methods
   */
  public Selector(LinkerComponent linkerComponent) {
    this.linkerComponent = linkerComponent;
    this.reuseTuple = new Tuple3<>();
  }

  @Override
  public void flatMap(Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList> vertexPairSimFieldList,
    Collector<Tuple3<EPGMVertex, EPGMVertex, Double>> out) throws Exception {

    SelectionComponent selectionComponent = linkerComponent.getSelectionComponent();

    double similaritiesSum = 0.0;
    double weightSum = 0.0;
    for (SimilarityField similarityField : vertexPairSimFieldList.f2.getSimilarityFields()) {
      similaritiesSum += similarityField.getSimilarityValue() * similarityField.getWeight();
      weightSum += similarityField.getWeight();
    }
    double aggSimilarityValue;
    boolean isAggregationEnabled = selectionComponent.isAggregationEnabled();
    boolean isSelectionEnabled = selectionComponent.isSelectionEnabled();

      /* if aggregation rule is enabled and it contains a rule for computing aggregated similarity, then
      aggregated similarity is computed regarding the rule */

    if (isAggregationEnabled && selectionComponent.getAggregatorRule().hasAggregatorRuleComponents()) {
      aggSimilarityValue =
        selectionComponent.getAggregatorRule().computeAggregatedSimilarity(vertexPairSimFieldList.f2);
    } else {
      aggSimilarityValue = similaritiesSum / weightSum;
    }
    // fill result tuple
    reuseTuple.f0 = vertexPairSimFieldList.f0;
    reuseTuple.f1 = vertexPairSimFieldList.f1;
    reuseTuple.f2 = aggSimilarityValue;

    /* if selection rule is enabled and if the rule is satisfied, the paired vertices are selected */
    if (isSelectionEnabled) {
      if (selectionComponent.getSelectionRule().check(vertexPairSimFieldList.f2)) {
          /* if aggregated rule is enabled and (aggregated similarity value >= aggregation threshold), then
             the vertex pair is selected */
        if (isAggregationEnabled) {
          if (aggSimilarityValue >= selectionComponent.getAggregatorRule().getAggregationThreshold()) {
            out.collect(reuseTuple);
          }
        } else {
            /* if only selection rule is enabled, vertex pair is selected without checking the aggregation
               threshold */
          out.collect(reuseTuple);
        }
      }
    } else {
        /* if selection rule is not enabled and (aggregation rule is enabled && (aggregated
        similarity value >= aggregation threshold)) ||  the aggregation rule is not enabled, the paired
        vertices are selected */
      if (isAggregationEnabled) {
        if (aggSimilarityValue >= selectionComponent.getAggregatorRule().getAggregationThreshold()) {
          out.collect(reuseTuple);
        }
      } else {
        out.collect(reuseTuple);
      }
    }
  }
}
