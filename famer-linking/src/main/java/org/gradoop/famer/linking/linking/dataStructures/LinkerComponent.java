/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.linking.dataStructures;

import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponent;
import org.gradoop.famer.linking.linking.Linker;
import org.gradoop.famer.linking.selection.dataStructures.SelectionComponent;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;

import java.io.Serializable;
import java.util.List;

/**
 * The structure that keeps the required input parameters as well as required helper methods needed for
 * {@link Linker}
 */
public class LinkerComponent implements Serializable {

  /**
   * The list of blocking components needed for linking. Blocking is the 1st step of linking
   */
  private List<BlockingComponent> blockingComponents;

  /**
   * The list of similarity components needed for linking. Similarity computation is the 2nd step of linking
   */
  private List<SimilarityComponent> similarityComponents;

  /**
   * The selection component needed for linking. Selection computation is the 3rd step of linking
   */
  private SelectionComponent selectionComponent;

  /**
   * The parameter that specifies whether the current (already existing) links of the input graphs are
   * going to be kept in linking output
   */
  private boolean keepCurrentEdges;

  /**
   * The parameter that specifies whether the similarity degree of the current (already existing) links are
   * going to be recomputed by new similarity components
   */
  private boolean recomputeSimilarityForCurrentEdges;

  /**
   * Specifies whether the neighbor cluster id of a new entity (vertex) should be stored as a propertyValue
   * in the connecting edge.
   *
   * Related to famer-incremental
   */
  private boolean tagNeighborCluster;

  /**
   * Whether the edge between newly added entities (vertices) should be tagged with a property value
   * 'newLink', regarding the edge status.
   *
   * Related to famer-incremental
   */
  private boolean tagNewLink;

  /**
   * Creates an instance of LinkerComponent
   */
  public LinkerComponent() { }

  /**
   * Creates an instance of LinkerComponent
   *
   * @param blockingComponents The list of blocking components needed for linking
   * @param similarityComponents The list of similarity components needed for linking
   * @param selectionComponent The selection component needed for linking
   * @param keepCurrentEdges The parameter that specifies whether the current (already existing) links of
   *                         the input graphs are going to be kept in linking output
   * @param recomputeSimilarityForCurrentEdges The parameter that specifies whether the similarity degree
   *                                           of the current (already existing) links are going to be
   *                                           recomputed by new similarity components
   */
  public LinkerComponent(List<BlockingComponent> blockingComponents,
    List<SimilarityComponent> similarityComponents, SelectionComponent selectionComponent,
    boolean keepCurrentEdges, boolean recomputeSimilarityForCurrentEdges) {
    this(blockingComponents, similarityComponents, selectionComponent, keepCurrentEdges,
      recomputeSimilarityForCurrentEdges, false, false);
  }

  /**
   * Creates an instance of LinkerComponent, extended parameters for famer incremental
   *
   * @param blockingComponents The list of blocking components needed for linking
   * @param similarityComponents The list of similarity components needed for linking
   * @param selectionComponent The selection component needed for linking
   * @param keepCurrentEdges The parameter that specifies whether the current (already existing) links of
   *                         the input graphs are going to be kept in linking output
   * @param recomputeSimilarityForCurrentEdges The parameter that specifies whether the similarity degree
   *                                           of the current (already existing) links are going to be
   *                                           recomputed by new similarity components
   * @param tagNeighborCluster Specifies whether the neighbor cluster id of a new entity (vertex) should be
   *                          stored as a propertyValue in the connecting edge (famer-incremental).
   * @param tagNewLink Whether the link between newly added entities (vertices) should be tagged with a
   *                   property value newLink', regarding the edge status (famer-incremental).
   */
  public LinkerComponent(List<BlockingComponent> blockingComponents,
    List<SimilarityComponent> similarityComponents, SelectionComponent selectionComponent,
    boolean keepCurrentEdges, boolean recomputeSimilarityForCurrentEdges, boolean tagNeighborCluster,
    boolean tagNewLink) {
    this.blockingComponents = blockingComponents;
    this.similarityComponents = similarityComponents;
    this.selectionComponent = selectionComponent;
    this.keepCurrentEdges = keepCurrentEdges;
    this.recomputeSimilarityForCurrentEdges = recomputeSimilarityForCurrentEdges;
    this.tagNeighborCluster = tagNeighborCluster;
    this.tagNewLink = tagNewLink;
  }

  /**
   * Returns the list of blocking components
   *
   * @return list of blocking components
   */
  public List<BlockingComponent> getBlockingComponents() {
    return blockingComponents;
  }

  /**
   * Sets the list of blocking components
   *
   * @param blockingComponents list of blocking components to set
   */
  public void setBlockingComponents(List<BlockingComponent> blockingComponents) {
    this.blockingComponents = blockingComponents;
  }

  /**
   * Returns the list of similarity components
   *
   * @return list of similarity components
   */
  public List<SimilarityComponent> getSimilarityComponents() {
    return similarityComponents;
  }

  /**
   * Sets the list of similarity components
   *
   * @param similarityComponents list of similarity components to set
   */
  public void setSimilarityComponents(List<SimilarityComponent> similarityComponents) {
    this.similarityComponents = similarityComponents;
  }

  /**
   * Returns the selection component
   *
   * @return the selection component
   */
  public SelectionComponent getSelectionComponent() {
    return selectionComponent;
  }

  /**
   * Sets the selection component
   *
   * @param selectionComponent the selection component to set
   */
  public void setSelectionComponent(SelectionComponent selectionComponent) {
    this.selectionComponent = selectionComponent;
  }

  /**
   * Returns whether to keep the already existing links of the input graph in the linking output
   *
   * @return whether to keep the already existing links of the input graph in the linking output
   */
  public boolean isKeepCurrentEdges() {
    return keepCurrentEdges;
  }

  /**
   * Returns whether to tag the neighbor cluster of the new entity (vertex)
   *
   * @return whether to tag the neighbor cluster of the new entity (vertex)
   */
  public boolean isTagNeighborCluster() {
    return tagNeighborCluster;
  }

  /**
   * Returns whether to tag the link between newly added entities (vertices)
   *
   * @return whether to tag the link between newly added entities (vertices)
   */
  public boolean isTagNewLink() {
    return tagNewLink;
  }

  /**
   * Sets whether to keep the already existing links of the input graph in the linking output
   *
   * @param keepCurrentEdges whether to keep the already existing links of the input graph in the linking
   *                         output
   */
  public void setKeepCurrentEdges(boolean keepCurrentEdges) {
    this.keepCurrentEdges = keepCurrentEdges;
  }

  /**
   * Returns whether to recompute the similarity degree of the already existing links of the input graph
   * by new similarity components
   *
   * @return whether to recompute the similarity degree of the already existing links of the input graph
   *         by new similarity components
   */
  public boolean isRecomputeSimilarityForCurrentEdges() {
    return recomputeSimilarityForCurrentEdges;
  }

  /**
   * Sets whether to recompute the similarity degree of the already existing links of the input graph by
   * new similarity components
   *
   * @param recomputeSimilarityForCurrentEdges whether to recompute the similarity degree of the already
   *                                           existing links of the input graph by new similarity components
   */
  public void setRecomputeSimilarityForCurrentEdges(boolean recomputeSimilarityForCurrentEdges) {
    this.recomputeSimilarityForCurrentEdges = recomputeSimilarityForCurrentEdges;
  }

  /**
   * Checks whether the blocking process generates redundant pairs
   *
   * @return true, if the blocking component generates redundant pairs
   */
  public boolean hasRedundantPairs() {
    if ((blockingComponents.size() > 1) || (isKeepCurrentEdges() && isRecomputeSimilarityForCurrentEdges())) {
      return true;
    }
    return blockingComponents.get(0).hasRedundantPairs();
  }
}
