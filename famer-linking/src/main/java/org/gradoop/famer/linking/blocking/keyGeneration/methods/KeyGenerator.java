/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.methods;

import java.util.List;

/**
 * The interface implemented by all key generation method classes.
 */
public interface KeyGenerator {

  /**
   * Parses the property value and generates the key.
   *
   * @param attributeValue the value of the attribute to define the key
   *
   * @return list of generated keys from the input attribute value
   */
  List<String> generateKey(String attributeValue);

  /**
   * Checks whether the key generation method could generate more than one key for each vertex
   *
   * @return false when the key list contains only one key per entity (vertex)
   */
  boolean returnsMultipleKeys();
}
