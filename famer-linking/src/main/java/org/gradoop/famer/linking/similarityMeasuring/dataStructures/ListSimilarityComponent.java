/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;


import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.linking.similarityMeasuring.methods.ListSimilarity;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

import java.util.List;

/**
 * Computes the similarity of a list property. To achieve this a further SimilarityComponent and
 * aggregation strategy is needed. The list elements of the source and target object are compared element
 * wise with the given SimilarityComponent. The resulting similarities are aggregated to a single
 * similarity value with the given aggregation strategy.
 * <p>
 * To configure this component in a similarity config (json) a field "aggregationStrategy" with
 * the aggregation strategy and a field "SimilarityComponent" containing a whole similarity component is
 * needed.
 */
public class ListSimilarityComponent extends SimilarityComponent {

  /**
   * The SimilarityComputation object needed for computing similarity of each pair
   */
  private final SimilarityComponent elementSimilarityComponent;

  /**
   * The method for aggregating similarity values of pairs to a single value
   */
  private final ListSimilarity.AggregationMethod aggregationMethod;

  /**
   * Creates an instance of ListSimilarityComponent
   *
   * @param baseConfig The base configuration for the similarity component
   * @param elementSimilarityComponent The SimilarityComputation object needed for computing similarity of
   *                                   each pair
   * @param aggregationMethod The method for aggregating similarity values of pairs to a single value
   */
  public ListSimilarityComponent(SimilarityComponentBaseConfig baseConfig,
    SimilarityComponent elementSimilarityComponent, ListSimilarity.AggregationMethod aggregationMethod) {
    super(baseConfig);
    this.elementSimilarityComponent = elementSimilarityComponent;
    this.aggregationMethod = aggregationMethod;
  }

  @Override
  public SimilarityComputation<List<PropertyValue>> buildSimilarityComputation() throws Exception {
    return new ListSimilarity(elementSimilarityComponent.buildSimilarityComputation(), aggregationMethod);
  }

}
