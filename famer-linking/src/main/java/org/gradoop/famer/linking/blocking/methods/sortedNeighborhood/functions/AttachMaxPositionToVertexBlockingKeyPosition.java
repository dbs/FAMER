/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions;

import org.apache.flink.api.common.functions.CrossFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Attaches the computed maximal position to a {@code Tuple3<Vertex, BlockingKey, Position>}
 */
@FunctionAnnotation.ForwardedFields("f0;f1;f2")
public class AttachMaxPositionToVertexBlockingKeyPosition implements
  CrossFunction<Tuple3<EPGMVertex, String, Long>, Long, Tuple4<EPGMVertex, String, Long, Long>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple4<EPGMVertex, String, Long, Long> reuseTuple = new Tuple4<>();

  @Override
  public Tuple4<EPGMVertex, String, Long, Long> cross(
    Tuple3<EPGMVertex, String, Long> vertexBlockingKeyPosition, Long maxPosition) throws Exception {
    reuseTuple.f0 = vertexBlockingKeyPosition.f0;
    reuseTuple.f1 = vertexBlockingKeyPosition.f1;
    reuseTuple.f2 = vertexBlockingKeyPosition.f2;
    reuseTuple.f3 = maxPosition;
    return reuseTuple;
  }
}
