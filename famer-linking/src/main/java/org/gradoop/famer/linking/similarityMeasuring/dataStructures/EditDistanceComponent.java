/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.methods.EditDistanceLevenshtein;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

/**
 * Similarity component which uses {@link EditDistanceLevenshtein}
 */
public class EditDistanceComponent extends SimilarityComponent {

  /**
   * Creates an instance of EditDistanceComponent from the given json object.
   *
   * @param config The json object that contains the configuration.
   */
  public EditDistanceComponent(JSONObject config) {
    super(config);
  }

  /**
   * Creates an instance of EditDistanceComponent.
   *
   * @param baseConfig The base configuration for the similarity component
   */
  public EditDistanceComponent(SimilarityComponentBaseConfig baseConfig) {
    super(baseConfig);
  }

  @Override
  public SimilarityComputation<String> buildSimilarityComputation() {
    return new EditDistanceLevenshtein();
  }
}
