/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection.dataStructures.selectionRule;

import java.io.Serializable;

/**
 * The structure that keeps the required parameters and helper methods needed for a condition of a
 * {@link SelectionRule}
 */
public class Condition implements Serializable {

  /**
   * The unique identifier of a condition
   */
  private String id;

  /**
   * The id of the similarity component that the condition is defined on
   */
  private String similarityFieldId;

  /**
   * The operator of the condition (>, <, >=, <=, ==, !=)
   */
  private ConditionOperator operator;

  /**
   * The specified threshold for the condition
   */
  private double threshold;

  /**
   * The default condition result, if the similarity field is not present
   * (when one of the attribute value of one of the vertices is null)
   */
  private boolean defaultOnAttributeNull;

  /**
   * Creates an instance of Condition
   *
   * @param id                     The unique identifier of a condition
   * @param similarityFieldId      The id of the similarity component that the condition is defined on
   * @param operator               The operator of the condition (>, <, >=, <=, ==, !=)
   * @param threshold              The specified threshold for the condition
   * @param defaultOnAttributeNull The default condition result, if the similarity field is not present
   */
  public Condition(String id, String similarityFieldId, ConditionOperator operator, double threshold,
    boolean defaultOnAttributeNull) {
    this.id = id;
    this.similarityFieldId = similarityFieldId;
    this.operator = operator;
    this.threshold = threshold;
    this.defaultOnAttributeNull = defaultOnAttributeNull;
  }

  /**
   * Creates an instance of Condition.
   * {@link Condition#defaultOnAttributeNull} is set to false by default.
   *
   * @param id                The unique identifier of a condition
   * @param similarityFieldId The id of the similarity component that the condition is defined on
   * @param operator          The operator of the condition (>, <, >=, <=, ==, !=)
   * @param threshold         The specified threshold for the condition
   * @see Condition#Condition(String, String, ConditionOperator, double, boolean)
   */
  public Condition(String id, String similarityFieldId, ConditionOperator operator, double threshold) {
    this(id, similarityFieldId, operator, threshold, false);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getSimilarityFieldId() {
    return similarityFieldId;
  }

  public void setSimilarityFieldId(String similarityFieldId) {
    this.similarityFieldId = similarityFieldId;
  }

  public ConditionOperator getOperator() {
    return operator;
  }

  public void setOperator(ConditionOperator operator) {
    this.operator = operator;
  }

  public double getThreshold() {
    return threshold;
  }

  public void setThreshold(double threshold) {
    this.threshold = threshold;
  }

  public boolean getDefaultOnAttributeNull() {
    return defaultOnAttributeNull;
  }

  public void setDefaultOnAttributeNull(boolean defaultOnAttributeNull) {
    this.defaultOnAttributeNull = defaultOnAttributeNull;
  }

  /**
   * Checks whether the condition is satisfied
   *
   * @param similarityValue The input that is going to be compared with threshold
   *
   * @return Returns true, if the condition is satisfied
   */
  public boolean checkCondition(double similarityValue) {
    switch (operator) {
    case EQUAL:
      if (similarityValue == threshold) {
        return true;
      }
      break;
    case GREATER:
      if (similarityValue > threshold) {
        return true;
      }
      break;
    case GREATER_EQUAL:
      if (similarityValue >= threshold) {
        return true;
      }
      break;
    case NOT_EQUAL:
      if (similarityValue != threshold) {
        return true;
      }
      break;
    case SMALLER:
      if (similarityValue < threshold) {
        return true;
      }
      break;
    case SMALLER_EQUAL:
      if (similarityValue <= threshold) {
        return true;
      }
      break;
    default:
      throw new IllegalArgumentException("Illegal operator! ");
    }
    return false;
  }
}
