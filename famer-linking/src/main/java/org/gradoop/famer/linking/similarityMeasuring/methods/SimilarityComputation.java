/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

import static org.gradoop.famer.linking.common.Utils.cleanAttributeValue;

/**
 * The interface for Computing the similarity. All similarity computation methods must implement it
 *
 * @param <T> This describes the type of property values that are compared
 */
public interface SimilarityComputation<T> {

  /**
   * Computes the similarity of two Objects of Type T
   *
   * @param value1 The first value
   * @param value2 The second value
   *
   * @return The computed similarity value between value1 and value2
   *
   * @throws Exception thrown on errors while computing the similarity
   */
  double computeSimilarity(T value1, T value2) throws Exception;

  /**
   * Parses the property value
   *
   * @param value The propertyValue
   *
   * @return The value of property value
   */
  T parsePropertyValue(PropertyValue value);

  /**
   * Computes the similarity of two property values. All similarity computation methods must implement it
   *
   * @param value1 The first property value
   * @param value2 The second property value
   *
   * @return The computed similarity value
   *
   * @throws Exception thrown on errors while computing the similarity
   */
  default double computeSimilarity(PropertyValue value1, PropertyValue value2) throws Exception {
    return computeSimilarity(parsePropertyValue(value1), parsePropertyValue(value2));
  }

  /**
   * returns the value of a property value as a String
   *
   * @param value The propertyValue
   *
   * @return The value of property as String
   */
  default String getString(PropertyValue value) {
    String str = value.toString();
    return cleanAttributeValue(str);
  }

  /**
   * returns the value of a property value as a Number
   *
   * @param value The propertyValue
   *
   * @return The value of property as Number
   */
  default Number getNumber(PropertyValue value) {
    return value.isNumber() ? (Number) value.getObject() : Double.parseDouble(value.toString());
  }

}
