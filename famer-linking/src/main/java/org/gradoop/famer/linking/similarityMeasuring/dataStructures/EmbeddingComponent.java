/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.methods.Embedding;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

import java.io.File;
import java.net.URI;
import java.util.UUID;

/**
 * Similarity component which uses {@link Embedding}.
 */
public class EmbeddingComponent extends SimilarityComponent {

  /**
   * Generated key used for Flinks distributed cache
   */
  private final String distributedCacheKey;

  /**
   * Word-vector file that is used to calculate the similarity between two vectors
   */
  private File wordVectorsFile;

  /**
   * URI for the word-vector file that is used to calculate the similarity between two vectors
   */
  private URI wordVectorsFileUri;

  /**
   * Creates an instance of EmbeddingComponent from the given json object.
   *
   * @param config The json object that contains the configuration.
   */
  public EmbeddingComponent(JSONObject config) {
    super(config);
    this.distributedCacheKey = UUID.randomUUID().toString();
    try {
      this.wordVectorsFileUri = checkPathCreateURI(config.getString("wordVectorsFileUri"));
    } catch (Exception ex) {
      throw new UnsupportedOperationException("EmbeddingComponent: value for wordVectorsFileUri could " +
        "not be found or parsed to URI.", ex);
    }
  }

  /**
   * Creates an instance of EmbeddingComponent and generates the key used for Flinks distributed cache
   *
   * @param baseConfig The base configuration for the similarity component
   * @param wordVectorsFileUri URI for the word-vector file that is used to calculate the similarity
   *                           between two vectors
   */
  public EmbeddingComponent(SimilarityComponentBaseConfig baseConfig, URI wordVectorsFileUri) {
    super(baseConfig);
    this.distributedCacheKey = UUID.randomUUID().toString();
    this.wordVectorsFileUri = wordVectorsFileUri;
  }

  /**
   * Create an URI from a given string, checks for existing scheme
   *
   * @param path Path to create the URI from
   *
   * @return The URI
   */
  private URI checkPathCreateURI(String path) {
    if (!path.isEmpty()) {
      URI uri = URI.create(path);
      if (uri.getScheme() == null) {
        String rawPath = uri.getRawPath();
        uri = URI.create("file:" + rawPath);
      }
      return uri;
    }
    return null;
  }

  /**
   * Returns the generated key used for Flinks distributed cache
   *
   * @return The key
   */
  public String getDistributedCacheKey() {
    return distributedCacheKey;
  }

  /**
   * Returns the URI for the word-vector file
   *
   * @return The URI for the word-vector file
   */
  public URI getWordVectorsFileUri() {
    return wordVectorsFileUri;
  }

  /**
   * Sets the URI for the word-vector file
   *
   * @param wordVectorsFileUri The URI to set
   */
  public void setWordVectorsFileUri(URI wordVectorsFileUri) {
    this.wordVectorsFileUri = wordVectorsFileUri;
  }

  /**
   * Returns the word-vector file
   *
   * @return The file
   */
  public File getWordVectorsFile() {
    return wordVectorsFile;
  }

  /**
   * Sets the word-vector file
   *
   * @param wordVectorsFile The file to set
   */
  public void setWordVectorsFile(File wordVectorsFile) {
    this.wordVectorsFile = wordVectorsFile;
  }

  @Override
  public SimilarityComputation<String> buildSimilarityComputation() throws Exception {
    return new Embedding(wordVectorsFile);
  }
}
