/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

/**
 * Computes the similarity degree of two numbers using the NumericalSimilarity method with a defined
 * maximum distance
 */
public class NumericalSimilarityWithMaxDistance implements SimilarityComputation<Number> {

  /**
   * The maximum tolerated distance
   */
  private final double maxToleratedDistance;

  /**
   * Creates an instance of NumericalSimilarityWithMaxDistance
   *
   * @param maxToleratedDistance The maximum tolerated distance
   */
  public NumericalSimilarityWithMaxDistance(double maxToleratedDistance) {
    this.maxToleratedDistance = maxToleratedDistance;
  }

  @Override
  public double computeSimilarity(Number number1, Number number2) throws Exception {
    if (number1 == null) {
      throw new NullPointerException("number1 must not be null");
    }
    if (number2 == null) {
      throw new NullPointerException("number2 must not be null");
    }
    double val1 = number1.doubleValue();
    double val2 = number2.doubleValue();
    double simDegree;
    if (Math.abs(val1 - val2) < maxToleratedDistance) {
      simDegree = 1 - (Math.abs(val1 - val2) / maxToleratedDistance);
    } else {
      simDegree = 0;
    }

    return simDegree;
  }

  @Override
  public Number parsePropertyValue(PropertyValue value) {
    return getNumber(value);
  }
}
