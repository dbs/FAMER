/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;
import org.gradoop.famer.linking.similarityMeasuring.methods.TruncateEnd;

/**
 * Similarity component which uses {@link TruncateEnd}.
 */
public class TruncateEndComponent extends SimilarityComponent {

  /**
   * The length parameter
   */
  private final int length;

  /**
   * Creates an instance of TruncateEndComponent from the given json object.
   *
   * @param config The json object that contains the configuration.
   */
  public TruncateEndComponent(JSONObject config) {
    super(config);
    try {
      config.getInt("length");
    } catch (Exception ex) {
      throw new UnsupportedOperationException(config.optString("length") +
        " could not be parsed to int.", ex);
    }
    this.length = config.optInt("length");
  }

  /**
   * Creates an instance of TruncateEndComponent
   *
   * @param baseConfig The base configuration for the similarity component
   * @param length The length parameter
   */
  public TruncateEndComponent(SimilarityComponentBaseConfig baseConfig, int length) {
    super(baseConfig);
    this.length = length;
  }

  @Override
  public SimilarityComputation<String> buildSimilarityComputation() {
    return new TruncateEnd(length);
  }

  public int getLength() {
    return length;
  }
}
