/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.blocking.keyGeneration.methods.FullAttribute;
import org.gradoop.famer.linking.blocking.keyGeneration.methods.KeyGenerator;

/**
 * Key generator component which uses {@link FullAttribute}.
 */
public class FullAttributeComponent extends KeyGeneratorComponent {

  /**
   * Creates an instance of FullAttributeComponent
   *
   * @param attribute The property / attribute name for generating the blocking keys
   */
  public FullAttributeComponent(String attribute) {
    super(attribute);
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The key generation component json config part
   */
  public FullAttributeComponent(JSONObject jsonConfig) {
    super(jsonConfig);
  }

  @Override
  public KeyGenerator buildKeyGenerator() {
    return new FullAttribute();
  }
}
