/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection.dataStructures;

import org.gradoop.famer.linking.selection.Selector;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRule;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.ArithmeticOperator;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.LogicalOperator;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRule;

import java.io.Serializable;

/**
 * The structure that keeps the required parameters and helper methods needed for {@link Selector}
 */
public class SelectionComponent implements Serializable {

  /**
   * The aggregator rule - a combination of similarity field ids and {@link ArithmeticOperator}
   */
  private AggregatorRule aggregatorRule;

  /**
   * The selection rule - a combination of conditions and {@link LogicalOperator}
   */
  private SelectionRule selectionRule;

  /**
   * Creates an empty instance of SelectionComponent
   */
  public SelectionComponent() {
    this(null, null);
  }

  /**
   * Creates an instance of SelectionComponent with an aggregatorRule
   *
   * @param aggregatorRule The aggregatorRule of the selectionComponent
   */
  public SelectionComponent(AggregatorRule aggregatorRule) {
    this(null, aggregatorRule);
  }

  /**
   * Creates an instance of SelectionComponent with a selectionRule
   *
   * @param selectionRule The selectionRule of the selectionComponent
   */
  public SelectionComponent(SelectionRule selectionRule) {
    this(selectionRule, null);
  }

  /**
   * Creates an instance of SelectionComponent
   *
   * @param selectionRule  The selectionRule of the selectionComponent
   * @param aggregatorRule The aggregatorRule of the selectionComponent
   */
  public SelectionComponent(SelectionRule selectionRule,
    AggregatorRule aggregatorRule) {
    this.selectionRule = selectionRule;
    this.aggregatorRule = aggregatorRule;
  }

  public AggregatorRule getAggregatorRule() {
    return aggregatorRule;
  }

  public void setAggregatorRule(AggregatorRule aggregatorRule) {
    this.aggregatorRule = aggregatorRule;
  }

  public SelectionRule getSelectionRule() {
    return selectionRule;
  }

  public void setSelectionRule(SelectionRule selectionRule) {
    this.selectionRule = selectionRule;
  }

  /**
   * Checks whether a selectionRule is applied in selection process
   *
   * @return false if selectionRule is {@code null}
   */
  public boolean isSelectionEnabled() {
    return selectionRule != null;
  }

  /**
   * Checks whether an aggregatorRule is applied in selection process
   *
   * @return false if aggregatorRule is {@code null}
   */
  public boolean isAggregationEnabled() {
    return aggregatorRule != null;
  }
}
