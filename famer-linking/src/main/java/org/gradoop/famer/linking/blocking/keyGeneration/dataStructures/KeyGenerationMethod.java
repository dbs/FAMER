/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.dataStructures;

/**
 * Available methods to generate the blocking keys
 */
public enum KeyGenerationMethod {
  /**
   * Use the full attribute as blocking key
   */
  FULL_ATTRIBUTE,
  /**
   * Use n initial letters from the attribute as blocking key
   */
  PREFIX_LENGTH,
  /**
   * Build q-grams from the attribute and use them as blocking keys
   */
  Q_GRAMS,
  /**
   * Split the attribute by a given tokenizer into a list of blocking keys
   */
  WORD_TOKENIZER,
}
