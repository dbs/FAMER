/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.methods.NumericalSimilarityWithMaxPercentage;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

/**
 * Similarity component which uses {@link NumericalSimilarityWithMaxPercentage}
 */
public class NumericalSimilarityWithMaxPercentageComponent extends SimilarityComponent {

  /**
   * The maximum tolerated percentage
   */
  private final double maxToleratedPercentage;

  /**
   * Creates an instance of NumericalSimilarityWithMaxPercentageComponent from the given json object.
   *
   * @param config The json object that contains the configuration.
   */
  public NumericalSimilarityWithMaxPercentageComponent(JSONObject config) {
    super(config);
    try {
      this.maxToleratedPercentage = config.getDouble("maxToleratedPercentage");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("NumericalSimilarityWithMaxPercentageComponent: value for " +
        "maxToleratedPercentage could not be found or parsed to double.", ex);
    }
  }

  /**
   * Creates an instance of NumericalSimilarityWithMaxPercentageComponent
   *
   * @param baseConfig             The base configuration for the similarity component
   * @param maxToleratedPercentage The maximum tolerated percentage
   */
  public NumericalSimilarityWithMaxPercentageComponent(SimilarityComponentBaseConfig baseConfig,
    double maxToleratedPercentage) {
    super(baseConfig);
    this.maxToleratedPercentage = maxToleratedPercentage;
  }

  @Override
  public SimilarityComputation<Number> buildSimilarityComputation() {
    return new NumericalSimilarityWithMaxPercentage(maxToleratedPercentage);
  }

  public double getMaxToleratedPercentage() {
    return maxToleratedPercentage;
  }
}
