/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static org.gradoop.famer.linking.common.Utils.cleanAttributeValue;

/**
 * Computes the similarity degree of two list of values using the user specified similarity computation method
 */
public class ListSimilarity implements SimilarityComputation<List<PropertyValue>> {

  /**
   * The Similarity computation object needed for computing similarity of each pair
   */
  private final SimilarityComputation elementSimilarityComputation;

  /**
   * The method for aggregating similarity values of pairs to a single value
   */
  private final AggregationMethod aggregationMethod;

  /**
   * Creates an instance of ListSimilarity
   *
   * @param elementSimilarityComputation The similarity computation object needed for computing similarity
   *                                     of each pair
   * @param aggregationMethod The method for aggregating similarity values of pairs to a single value
   */
  public ListSimilarity(SimilarityComputation elementSimilarityComputation,
    AggregationMethod aggregationMethod) {
    this.elementSimilarityComputation = elementSimilarityComputation;
    this.aggregationMethod = aggregationMethod;
  }

  @SuppressWarnings("unchecked")
  @Override
  public double computeSimilarity(List<PropertyValue> list1, List<PropertyValue> list2) throws Exception {
    if (list1 == null) {
      throw new NullPointerException("list1 must not be null");
    }
    if (list2 == null) {
      throw new NullPointerException("list2 must not be null");
    }
    List<Double> elementSimilarities = new ArrayList<>();
    for (PropertyValue list1Element : list1) {
      for (PropertyValue list2Element : list2) {
        double simDeg;
        if (elementSimilarityComputation.parsePropertyValue(list1Element) instanceof String) {
          String str1 = cleanAttributeValue(list1Element.toString());
          String str2 = cleanAttributeValue(list2Element.toString());
          simDeg = elementSimilarityComputation.computeSimilarity(str1, str2);
        } else {
          simDeg = elementSimilarityComputation.computeSimilarity(list1Element, list2Element);
        }
        elementSimilarities.add(simDeg);
      }
    }
    return aggregationMethod.apply(elementSimilarities);
  }

  @Override
  public List<PropertyValue> parsePropertyValue(PropertyValue value) {
    return value.getList();
  }

  /**
   * The interface is for computing the aggregated similarity of two lists. It is implemented by
   * aggregation methods
   */
  public interface AggregationMethod extends Function<List<Double>, Double>, Serializable {

    /**
     * Computes the aggregated similarity of two lists
     *
     * @param name The name of selected aggregation method
     *
     * @return The selected AggregationMethod object
     */
    static AggregationMethod fromString(String name) {
      switch (name) {
      case "AggregationMin":
        return new AggregationMethodMin();
      case "AggregationMax":
        return new AggregationMethodMax();
      case "AggregationAv":
        return new AggregationMethodAverage();
      default:
        throw new IllegalArgumentException("AggregationMethod must be one of " + "Aggregation{Min|Max|Av}");
      }
    }
  }

  /**
   * Returns the minimum similarity value as the aggregated similarity value
   */
  public static class AggregationMethodMin implements AggregationMethod {
    @Override
    public Double apply(List<Double> similarities) {
      return similarities.stream().min(Double::compareTo).orElse(0d);
    }
  }

  /**
   * Returns the maximum similarity value as the aggregated similarity value
   */
  public static class AggregationMethodMax implements AggregationMethod {
    @Override
    public Double apply(List<Double> similarities) {
      return similarities.stream().max(Double::compareTo).orElse(0d);
    }
  }

  /**
   * Returns The average similarity value as the aggregated similarity value
   */
  public static class AggregationMethodAverage implements AggregationMethod {
    @Override
    public Double apply(List<Double> similarities) {
      return similarities.stream().mapToDouble(d -> d).average().orElse(0d);
    }
  }

}
