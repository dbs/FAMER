/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.GroupCombineFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Combines a group of {@code Tuple3<Vertex, BlockingKey, PartitionId>}, grouped by PartitionId and by
 * BlockingKey, and returns a {@code Tuple3<BlockingKey, PartitionId, Count>}, where Count is the number of
 * items for this group.
 */
public class PartitionEntitiesCounter implements
  GroupCombineFunction<Tuple3<EPGMVertex, String, Integer>, Tuple3<String, Integer, Long>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<String, Integer, Long> reuseTuple = new Tuple3<>();

  @Override
  public void combine(Iterable<Tuple3<EPGMVertex, String, Integer>> group,
    Collector<Tuple3<String, Integer, Long>> out) {
    String blockingKey = "";
    long count = 0L;
    int partitionId = 0;
    for (Tuple3<EPGMVertex, String, Integer> groupItem : group) {
      blockingKey = groupItem.f1;
      partitionId = groupItem.f2;
      count++;
    }
    reuseTuple.f0 = blockingKey;
    reuseTuple.f1 = partitionId;
    reuseTuple.f2 = count;
    out.collect(reuseTuple);
  }
}
