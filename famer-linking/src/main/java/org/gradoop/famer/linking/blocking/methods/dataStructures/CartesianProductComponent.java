/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.blocking.methods.BlockingExecutor;
import org.gradoop.famer.linking.blocking.methods.cartesianProduct.CartesianProduct;

/**
 * Blocking component which uses {@link CartesianProduct}
 */
public class CartesianProductComponent extends BlockingComponent {

  /**
   * Creates an instance of CartesianProductComponent
   *
   * @param baseConfig The base configuration for the blocking component
   */
  public CartesianProductComponent(BlockingComponentBaseConfig baseConfig) {
    super(baseConfig);
  }

  /**
   * Constructor used for json parsing
   *
   * @param baseConfig The base configuration for the blocking component
   * @param jsonConfig The blocking component json config part
   */
  public CartesianProductComponent(BlockingComponentBaseConfig baseConfig, JSONObject jsonConfig) {
    super(baseConfig);
  }

  @Override
  public BlockingExecutor buildBlockingExecutor() {
    return new CartesianProduct(this);
  }
}
