/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;

/**
 * Takes a {@code Tuple2<BlockingKey, BlockSize>} and attaches the computed unique long id.
 */
@FunctionAnnotation.ReadFields("f1")
@FunctionAnnotation.ForwardedFields("f0->f2")
public class AddUniqueLongIdToBlock implements
  MapFunction<Tuple2<Long, Tuple2<String, Long>>, Tuple3<String, Long, Long>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<String, Long, Long> reuseTuple = new Tuple3<>();

  @Override
  public Tuple3<String, Long, Long> map(Tuple2<Long, Tuple2<String, Long>> idToBlockingKeyBlockSize)
    throws Exception {
    reuseTuple.f0 = idToBlockingKeyBlockSize.f1.f0;
    reuseTuple.f1 = idToBlockingKeyBlockSize.f1.f1;
    reuseTuple.f2 = idToBlockingKeyBlockSize.f0;
    return reuseTuple;
  }
}
