/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

/**
 * Computes the similarity degree of two strings using the MongeElkan-JaroWinkler method
 */
public class MongeElkanJaroWinkler implements SimilarityComputation<String> {

  /**
   * The tokenizer string
   */
  private final String tokenizer;

  /**
   * The JaroWinkler threshold
   */
  private final double jaroWinklerThreshold;

  /**
   * Creates an instance of MongeElkanJaroWinkler
   *
   * @param tokenizer The tokenizer string
   * @param jaroWinklerThreshold The JaroWinkler threshold
   */
  public MongeElkanJaroWinkler(String tokenizer, double jaroWinklerThreshold) {
    this.tokenizer = tokenizer;
    this.jaroWinklerThreshold = jaroWinklerThreshold;
  }

  @Override
  public double computeSimilarity(String str1, String str2) throws Exception {
    if (str1 == null) {
      throw new NullPointerException("str1 must not be null");
    }
    if (str2 == null) {
      throw new NullPointerException("str2 must not be null");
    }
    double simdegree = 0;
    String[] str1tokens = str1.split(tokenizer);
    String[] str2tokens = str2.split(tokenizer);

    JaroWinkler jaroWinkler = new JaroWinkler(jaroWinklerThreshold);
    for (String s1 : str1tokens) {
      double max = 0;
      for (String s2 : str2tokens) {
        double jaroWinklerSim = jaroWinkler.computeSimilarity(s1, s2);
        if (jaroWinklerSim > max) {
          max = jaroWinklerSim;
        }
      }
      simdegree += max;
    }
    simdegree /= str1tokens.length;

    return simdegree;
  }

  @Override
  public String parsePropertyValue(PropertyValue value) {
    return getString(value);
  }
}
