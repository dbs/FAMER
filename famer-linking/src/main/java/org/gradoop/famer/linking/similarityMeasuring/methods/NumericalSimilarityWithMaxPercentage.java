/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.gradoop.common.model.impl.properties.PropertyValue;

/**
 * Computes the similarity degree of two numbers using the NumericalSimilarity method with a defined
 * maximum percentage of distance
 */
public class NumericalSimilarityWithMaxPercentage implements SimilarityComputation<Number> {

  /**
   * The maximum tolerated percentage
   */
  private final double maxToleratedPercentage;

  /**
   * Creates an instance of NumericalSimilarityWithMaxPercentage
   *
   * @param maxToleratedPercentage The maximum tolerated percentage
   */
  public NumericalSimilarityWithMaxPercentage(double maxToleratedPercentage) {
    this.maxToleratedPercentage = maxToleratedPercentage;
  }

  @Override
  public double computeSimilarity(Number number1, Number number2) {
    if (number1 == null) {
      throw new NullPointerException("number1 must not be null");
    }
    if (number2 == null) {
      throw new NullPointerException("number2 must not be null");
    }
    double val1 = number1.doubleValue();
    double val2 = number2.doubleValue();
    double simDegree;
    double pc = Math.abs(val1 - val2) * 100 / Math.max(val1, val2);
    if (pc < maxToleratedPercentage) {
      simDegree = 1 - (pc / maxToleratedPercentage);
    } else {
      simDegree = 0;
    }

    return simDegree;
  }

  @Override
  public Number parsePropertyValue(PropertyValue value) {
    return getNumber(value);
  }
}
