/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.blocking.methods.BlockingExecutor;
import org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.SortedNeighborhood;

/**
 * Blocking component which uses {@link SortedNeighborhood}
 */
public class SortedNeighborhoodComponent extends BlockingComponent {

  /**
   * The window size
   */
  private final int windowSize;

  /**
   * The maximum desired system cores that will be used for load balancing
   */
  private final int parallelismDegree;

  /**
   * Creates an instance of SortedNeighborhoodComponent
   *
   * @param baseConfig The base configuration for the blocking component
   * @param windowSize The window size
   * @param parallelismDegree The maximum desired system cores that will be used for load balancing
   */
  public SortedNeighborhoodComponent(BlockingComponentBaseConfig baseConfig, int windowSize,
    int parallelismDegree) {
    super(baseConfig);
    this.windowSize = windowSize;
    this.parallelismDegree = parallelismDegree;
  }

  /**
   * Constructor used for json parsing
   *
   * @param baseConfig The base configuration for the blocking component
   * @param jsonConfig The blocking component json config part
   */
  public SortedNeighborhoodComponent(BlockingComponentBaseConfig baseConfig, JSONObject jsonConfig) {
    super(baseConfig);
    try {
      this.windowSize = jsonConfig.getInt("windowSize");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("SortedNeighborhoodComponent: value for windowSize could not " +
        "be found or parsed to int", ex);
    }
    try {
      this.parallelismDegree = jsonConfig.getInt("parallelismDegree");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("SortedNeighborhoodComponent: value for parallelismDegree " +
        "could not be found parsed to int", ex);
    }
  }

  /**
   * Returns the window size used for the sorted neighborhood algorithm
   *
   * @return The window size
   */
  public int getWindowSize() {
    return windowSize;
  }

  /**
   * Returns the maximum desired system cores that will be used for load balancing
   *
   * @return The maximum desired system cores that will be used for load balancing
   */
  public int getParallelismDegree() {
    return parallelismDegree;
  }

  @Override
  public BlockingExecutor buildBlockingExecutor() {
    return new SortedNeighborhood(this);
  }
}
