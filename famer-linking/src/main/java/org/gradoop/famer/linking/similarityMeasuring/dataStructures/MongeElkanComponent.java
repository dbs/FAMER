/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.similarityMeasuring.methods.MongeElkanJaroWinkler;
import org.gradoop.famer.linking.similarityMeasuring.methods.SimilarityComputation;

/**
 * Similarity component which uses {@link MongeElkanComponent}.
 */
public class MongeElkanComponent extends SimilarityComponent {

  /**
   * The tokenizer string
   */
  private final String tokenizer;

  /**
   * The JaroWinkler threshold
   */
  private final double jaroWinklerThreshold;

  /**
   * Creates an instance of MongeElkanComponent from the given json object.
   *
   * @param config The json object that contains the configuration.
   */
  public MongeElkanComponent(JSONObject config) {
    super(config);
    try {
      this.jaroWinklerThreshold = config.getDouble("jaroWinklerThreshold");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("MongeElkanComponent: value for jaroWinklerThreshold " +
        "could not be found or parsed to double.", ex);
    }
    try {
      this.tokenizer = config.getString("tokenizer");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("MongeElkanComponent: value for tokenizer " +
        "could not be found or parsed to string.", ex);
    }
  }

  /**
   * Creates an instance of MongeElkanComponent
   *
   * @param baseConfig The base configuration for the similarity component
   * @param tokenizer The tokenizer string
   * @param jaroWinklerThreshold The JaroWinkler threshold
   */
  public MongeElkanComponent(SimilarityComponentBaseConfig baseConfig, String tokenizer,
    double jaroWinklerThreshold) {
    super(baseConfig);
    this.jaroWinklerThreshold = jaroWinklerThreshold;
    this.tokenizer = tokenizer;
  }

  @Override
  public SimilarityComputation<String> buildSimilarityComputation() {
    return new MongeElkanJaroWinkler(tokenizer, jaroWinklerThreshold);
  }

  public String getTokenizer() {
    return tokenizer;
  }

  public double getJaroWinklerThreshold() {
    return jaroWinklerThreshold;
  }
}
