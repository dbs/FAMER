/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.dataStructures;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.linking.blocking.keyGeneration.methods.KeyGenerator;
import org.gradoop.famer.linking.blocking.keyGeneration.methods.QGrams;

/**
 * Key generator component which uses {@link QGrams}.
 */
public class QGramsComponent extends KeyGeneratorComponent {

  /**
   * Number of characters used for q-gram building, e.g. q = 2 for bi-grams
   */
  private final int q;

  /**
   * The minimum threshold t (t <= 1) that decides the minimum relative length, of the shortest q-gram
   */
  private final double threshold;

  /**
   * Creates an instance of QGramsComponent
   *
   * @param attribute The property / attribute name for generating the blocking keys
   * @param q Number of characters used for q-gram building, e.g q = 2 for bi-grams
   * @param threshold The minimum threshold t (t <= 1) that decides the minimum relative length, of the
   *                  shortest q-gram
   */
  public QGramsComponent(String attribute, int q, double threshold) {
    super(attribute);
    this.q = q;
    this.threshold = threshold;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The key generation component json config part
   */
  public QGramsComponent(JSONObject jsonConfig) {
    super(jsonConfig);
    try {
      this.q = jsonConfig.getInt("qgramNo");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("QGramsComponent: value for qgramNo could not " +
        "be found or parsed to int", ex);
    }
    try {
      this.threshold = jsonConfig.getDouble("qgramThreshold");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("QGramsComponent: value for qgramThreshold could not " +
        "be found or parsed to double", ex);
    }
  }

  public int getQ() {
    return q;
  }

  public double getThreshold() {
    return threshold;
  }

  @Override
  public KeyGenerator buildKeyGenerator() {
    return new QGrams(q, threshold);
  }
}
