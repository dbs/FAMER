/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection.dataStructures.aggregatorRule;

import java.io.Serializable;

/**
 * The structure that keeps the required parameters and helper methods needed for creating and applying an
 * {@link AggregatorRule}
 */
public class AggregatorRuleComponent implements Serializable {

  /**
   * Specifies the type of a aggregator rule component such as (, ), algebraic operator, similarity field, ...
   */
  private AggregatorRuleComponentType aggregatorRuleComponentType;

  /**
   * keeps the value of a aggregator component
   */
  private String value;

  /**
   * Creates an instance of AggregatorRuleComponent
   *
   * @param aggregatorRuleComponentType Specifies the type of a aggregator rule component
   * @param value keeps the value of a aggregator component
   */
  public AggregatorRuleComponent(AggregatorRuleComponentType aggregatorRuleComponentType, String value) {
    this.aggregatorRuleComponentType = aggregatorRuleComponentType;
    this.value = value;
  }

  public AggregatorRuleComponentType getAggregatorRuleComponentType() {
    return aggregatorRuleComponentType;
  }

  public void setAggregatorRuleComponentType(AggregatorRuleComponentType aggregatorRuleComponentType) {
    this.aggregatorRuleComponentType = aggregatorRuleComponentType;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
