/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.sortedNeighborhood.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Map function to extend a {@code Tuple2<Vertex, BlockingKey>} to a
 * {@code Tuple3<Vertex, BlockingKey, Position>}
 */
@FunctionAnnotation.ForwardedFields("f0;f1")
public class ExtendVertexBlockingKeyWithPosition implements
  MapFunction<Tuple2<EPGMVertex, String>, Tuple3<EPGMVertex, String, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMVertex, String, String> reuseTuple = new Tuple3<>();

  @Override
  public Tuple3<EPGMVertex, String, String> map(Tuple2<EPGMVertex, String> vertexKeyTuple) throws Exception {
    reuseTuple.f0 = vertexKeyTuple.f0;
    reuseTuple.f1 = vertexKeyTuple.f1;
    reuseTuple.f2 = "";
    return reuseTuple;
  }
}
