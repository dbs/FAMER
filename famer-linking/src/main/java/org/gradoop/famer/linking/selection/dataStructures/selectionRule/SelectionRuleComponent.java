/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection.dataStructures.selectionRule;

import java.io.Serializable;

/**
 * The structure that keeps the required parameters and helper methods needed for creating and applying a
 * {@link SelectionRule}
 */
public class SelectionRuleComponent implements Serializable {

  /**
   * Specifies the type of a rule component such as (, ), operator, condition, ...
   */
  private SelectionRuleComponentType selectionRuleComponentType;

  /**
   * Keeps the value of a rule component
   */
  private String value;

  /**
   * Creates an instance of SelectionRuleComponent
   *
   * @param selectionRuleComponentType Specifies the type of a rule component
   * @param value keeps the value of a rule component
   */
  public SelectionRuleComponent(SelectionRuleComponentType selectionRuleComponentType, String value) {
    this.selectionRuleComponentType = selectionRuleComponentType;
    this.value = value;
  }

  public SelectionRuleComponentType getSelectionRuleComponentType() {
    return selectionRuleComponentType;
  }

  public void setSelectionRuleComponentType(SelectionRuleComponentType selectionRuleComponentType) {
    this.selectionRuleComponentType = selectionRuleComponentType;
  }

  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }
}
