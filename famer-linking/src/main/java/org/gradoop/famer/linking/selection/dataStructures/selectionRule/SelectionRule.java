/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection.dataStructures.selectionRule;

import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityField;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

/**
 * The structure that contains the selection rule as well as the method for evaluating condition values
 * against the rule
 */
public class SelectionRule implements Serializable {

  /**
   * The list of conditions for the selection rule
   */
  private List<Condition> conditions;

  /**
   * The used selection rule components
   */
  private List<SelectionRuleComponent> selectionRuleComponents;

  /**
   * Creates an instance of SelectionRuleComponent
   *
   * @param conditions The list of conditions for the selection rule
   * @param selectionRuleComponents The used selection rule components
   */
  public SelectionRule(List<Condition> conditions, List<SelectionRuleComponent> selectionRuleComponents) {
    this.conditions = conditions;
    // we use arrayList methods later, be sure selectionRuleComponents is of this type
    this.selectionRuleComponents = new ArrayList<>(selectionRuleComponents);
    wrapSelectionRule();
  }

  public List<Condition> getConditions() {
    return conditions;
  }

  public void setConditions(List<Condition> conditions) {
    this.conditions = conditions;
  }

  public List<SelectionRuleComponent> getSelectionRuleComponents() {
    return selectionRuleComponents;
  }

  /**
   * Sets the selection rule component and wraps it with opening and closing parenthesis
   *
   * @param selectionRuleComponents The selection rule components to set
   */
  public void setSelectionRuleComponents(List<SelectionRuleComponent> selectionRuleComponents) {
    this.selectionRuleComponents = selectionRuleComponents;
    wrapSelectionRule();
  }

  /**
   * Wraps selection rules entered by user with "(" and ")"
   */
  private void wrapSelectionRule() {
    this.selectionRuleComponents.add(0,
      new SelectionRuleComponent(SelectionRuleComponentType.OPEN_PARENTHESIS, "("));
    int size = this.selectionRuleComponents.size();
    this.selectionRuleComponents.add(size,
      new SelectionRuleComponent(SelectionRuleComponentType.CLOSE_PARENTHESIS, ")"));
  }

  /**
   * Computes condition values from similarity fields
   *
   * @param similarityFieldList list of similarities
   *
   * @return Returns the list of condition values
   */
  private Map<String, Boolean> computeConditionValues(SimilarityFieldList similarityFieldList) {
    Map<String, Boolean> conditionValues = new HashMap<>();
    for (Condition condition : conditions) {
      SimilarityField similarityField =
        similarityFieldList.getSimilarityField(condition.getSimilarityFieldId());

      if (similarityField != null) {
        conditionValues.put(condition.getId(),
          condition.checkCondition(similarityField.getSimilarityValue()));
      } else {
        // if the conditions do not exist, set the default result
        conditionValues.put(condition.getId(), condition.getDefaultOnAttributeNull());
      }
    }
    return conditionValues;
  }

  /**
   * Evaluates condition values against the selection rule conditions
   *
   * @param similarityFieldList list of similarities
   *
   * @return Returns true, if the rule is satisfied
   */
  public boolean check(SimilarityFieldList similarityFieldList) {
    Map<String, Boolean> conditionValues = computeConditionValues(similarityFieldList);
    // use of last-in-first-out (LIFO) Stack
    Stack<SelectionRuleComponent> parsingStack = new Stack<>();

    for (SelectionRuleComponent component : selectionRuleComponents) {
      SelectionRuleComponentType componentType = component.getSelectionRuleComponentType();
      switch (componentType) {
      case OPEN_PARENTHESIS:
      case CONDITION:
      case SELECTION_OPERATOR:
      case COMPUTED_EXPRESSION:
        parsingStack.push(component);
        break;
      case CLOSE_PARENTHESIS:
        evaluateSubRule(parsingStack, conditionValues);
        break;
      default:
        throw new IllegalArgumentException("Illegal rule component!");
      }
    }
    return Boolean.parseBoolean(parsingStack.pop().getValue());
  }

  // TODO: Maybe it can be implemented more efficiently by bit-wise operations

  /**
   * Evaluates the sub-expression between open and close parentheses in the parsingStack. It pops the
   * conditionValues of the parsingStack and pushes the result of the evaluation as a COMPUTED_EXPRESSION
   * instead. The corresponding values to each condition of the rule component or the value of already
   * computed expression is retrieved from the input parameter conditionValues
   *
   * @param parsingStack The stack the contains the expression
   * @param conditionValues The values of the checked conditions (true, false)
   */
  private void evaluateSubRule(Stack<SelectionRuleComponent> parsingStack,
    Map<String, Boolean> conditionValues) {
    Boolean result = null;
    SelectionRuleComponent selectionRuleComponent;
    SelectionRuleComponentType selectionRuleComponentType;
    Stack<LogicalOperator> logicalOperatorStack = new Stack<>();

    do {
      selectionRuleComponent = parsingStack.pop();
      selectionRuleComponentType = selectionRuleComponent.getSelectionRuleComponentType();
      Boolean conditionValue;
      switch (selectionRuleComponentType) {
      case CONDITION:
        conditionValue = conditionValues.get(selectionRuleComponent.getValue());
        if (result == null) {
          result = conditionValue;
        } else {
          LogicalOperator operator = logicalOperatorStack.pop();
          result = compute(operator, result, conditionValue);
          SelectionRuleComponent newSelectionRuleComponent =
            new SelectionRuleComponent(SelectionRuleComponentType.COMPUTED_EXPRESSION, result.toString());
          parsingStack.push(newSelectionRuleComponent);
          result = null;
        }
        break;
      case COMPUTED_EXPRESSION:
        conditionValue = Boolean.parseBoolean(selectionRuleComponent.getValue());
        if (result == null) {
          result = conditionValue;
        } else {
          LogicalOperator operator = logicalOperatorStack.pop();
          result = compute(operator, result, conditionValue);
          SelectionRuleComponent newSelectionRuleComponent =
            new SelectionRuleComponent(SelectionRuleComponentType.COMPUTED_EXPRESSION, result.toString());
          parsingStack.push(newSelectionRuleComponent);
          result = null;
        }
        break;
      case SELECTION_OPERATOR:
        logicalOperatorStack.push(LogicalOperator.valueOf(selectionRuleComponent.getValue()));
        break;
      default:
        if (!selectionRuleComponentType.equals(SelectionRuleComponentType.OPEN_PARENTHESIS)) {
          throw new IllegalArgumentException("Illegal selection rule component!");
        }
      }
    } while (!selectionRuleComponentType.equals(SelectionRuleComponentType.OPEN_PARENTHESIS));
    assert result != null;
    SelectionRuleComponent newSelectionRuleComponent =
      new SelectionRuleComponent(SelectionRuleComponentType.COMPUTED_EXPRESSION, result.toString());
    parsingStack.push(newSelectionRuleComponent);
  }

  /**
   * Computes the result of applying one operator (AND/OR) on two operands
   *
   * @param operator The logical operator
   * @param operand1 The first operand
   * @param operand2 The second operand
   *
   * @return Returns null, if the operator is neither OR or AND as well as when one operand is null
   */
  private Boolean compute(LogicalOperator operator, Boolean operand1, Boolean operand2) {
    Boolean result = null;
    if (operator.equals(LogicalOperator.OR)) {
      result = operand1 | operand2;
    } else if (operator.equals(LogicalOperator.AND)) {
      result = operand1 & operand2;
    }
    return result;
  }
}
