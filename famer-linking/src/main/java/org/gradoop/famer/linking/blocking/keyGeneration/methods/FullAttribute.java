/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration.methods;

import java.util.Collections;
import java.util.List;

/**
 * Returns the full attribute value as key.
 */
public class FullAttribute implements KeyGenerator {

  @Override
  public List<String> generateKey(String attributeValue) {
    return Collections.singletonList(attributeValue);
  }

  @Override
  public boolean returnsMultipleKeys() {
    return false;
  }
}

