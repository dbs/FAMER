/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Maps a vertex pair to a {@code Tuple3<Vertex1, Vertex2, CombinedIds>} where the order of Vertex1
 * and Vertex2 in the tuple as well as the order of the ids combined depends on the compared vertex ids.
 */
@FunctionAnnotation.ReadFields("f0;f1")
public class CreateSortedVertexPairsWithCombinedId implements
  MapFunction<Tuple2<EPGMVertex, EPGMVertex>, Tuple3<EPGMVertex, EPGMVertex, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMVertex, EPGMVertex, String> reuseTuple = new Tuple3<>();

  @Override
  public Tuple3<EPGMVertex, EPGMVertex, String> map(Tuple2<EPGMVertex, EPGMVertex> vertexPairTuple)
    throws Exception {
    String id1 = vertexPairTuple.f0.getId().toString();
    String id2 = vertexPairTuple.f1.getId().toString();
    if (id1.compareTo(id2) < 0) {
      reuseTuple.f0 = vertexPairTuple.f0;
      reuseTuple.f1 = vertexPairTuple.f1;
      reuseTuple.f2 = id1 + "," + id2;
      return reuseTuple;
    } else {
      reuseTuple.f0 = vertexPairTuple.f1;
      reuseTuple.f1 = vertexPairTuple.f0;
      reuseTuple.f2 = id2 + "," + id1;
      return reuseTuple;
    }
  }
}
