/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.standardBlocking.functions;

import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Gets the number of each parallel sub task as PartitionId and returns a
 * {@code Tuple3<Vertex, BlockingKey, PartitionId>}
 */
@FunctionAnnotation.ForwardedFields("f0;f1")
public final class PartitionIdDiscoverer extends
  RichMapFunction<Tuple2<EPGMVertex, String>, Tuple3<EPGMVertex, String, Integer>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMVertex, String, Integer> reuseTuple = new Tuple3<>();

  @Override
  public Tuple3<EPGMVertex, String, Integer> map(Tuple2<EPGMVertex, String> vertexBlockingKey)
    throws Exception {
    int partitionId = getRuntimeContext().getIndexOfThisSubtask();
    reuseTuple.f0 = vertexBlockingKey.f0;
    reuseTuple.f1 = vertexBlockingKey.f1;
    reuseTuple.f2 = partitionId;
    return reuseTuple;
  }
}
