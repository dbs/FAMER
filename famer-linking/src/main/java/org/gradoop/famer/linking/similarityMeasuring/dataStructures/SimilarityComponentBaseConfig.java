/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.dataStructures;

/**
 * The base configuration class for a similarity component. Wraps commonly used objects for all similarity
 * components.
 */
public class SimilarityComponentBaseConfig {
  // TODO: disambiguate parameter names and adjust them in SimilarityComponent, too
  //  see: https://git.informatik.uni-leipzig.de/dbs/FAMER/wikis/Linking-Configuration-(JSON)
  /**
   * The unique identifier of the component object
   */
  private final String componentId;

  /**
   * The attribute/property title of the source graph
   */
  private final String sourceAttribute;

  /**
   * The attribute/property title of the target graph
   */
  private final String targetAttribute;

  /**
   * The graph label of the source graph which specifies the DATA SOURCE of an entity and its corresponding
   * value is stored as property in each vertex
   */
  private final String sourceGraph;

  /**
   * The graph label of the target graph which specifies the DATA SOURCE of an entity and its corresponding
   * value is stored as property in each vertex
   */
  private final String targetGraph;

  /**
   * The label of the source graph
   */
  private final String sourceLabel;

  /**
   * The label of the target graph
   */
  private final String targetLabel;

  /**
   * The effective coefficient of the similarity value
   */
  private final double weight;

  /**
   * Creates an instance of SimilarityComponentBaseConfig
   *
   * @param componentId      The unique identifier of the component object
   * @param sourceGraph The graph label of the source graph
   * @param sourceLabel      The label of the source graph
   * @param sourceAttribute  The attribute/property title of the source graph
   * @param targetGraph The graph label of the target graph
   * @param targetLabel      The label of the target graph
   * @param targetAttribute  The attribute/property title of the target graph
   * @param weight           The effective coefficient of the similarity value
   */
  public SimilarityComponentBaseConfig(String componentId, String sourceGraph, String sourceLabel,
    String sourceAttribute, String targetGraph, String targetLabel, String targetAttribute,
    double weight) {
    this.componentId = componentId;
    this.sourceAttribute = sourceAttribute;
    this.targetAttribute = targetAttribute;
    this.sourceGraph = sourceGraph;
    this.targetGraph = targetGraph;
    this.sourceLabel = sourceLabel;
    this.targetLabel = targetLabel;
    this.weight = weight;
  }

  public String getComponentId() {
    return componentId;
  }

  public String getSourceAttribute() {
    return sourceAttribute;
  }

  public String getTargetAttribute() {
    return targetAttribute;
  }

  public String getSourceGraph() {
    return sourceGraph;
  }

  public String getTargetGraph() {
    return targetGraph;
  }

  public String getSourceLabel() {
    return sourceLabel;
  }

  public String getTargetLabel() {
    return targetLabel;
  }

  public double getWeight() {
    return weight;
  }
}
