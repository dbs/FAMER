/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link EditDistanceLevenshtein}
 */
public class EditDistanceLevenshteinTest {

  private EditDistanceLevenshtein editDistanceLevenshtein = new EditDistanceLevenshtein();

  @Test(expected = Exception.class)
  public void testComputeSimilarityForFirstNullValue() throws Exception {
    editDistanceLevenshtein.computeSimilarity(null, "");
  }

  @Test(expected = Exception.class)
  public void testComputeSimilarityForSecondNullValue() throws Exception {
    editDistanceLevenshtein.computeSimilarity("", null);
  }

  @Test
  public void testComputeSimilarity() throws Exception {
    double result1 = editDistanceLevenshtein.computeSimilarity("ABC", "ABC");
    assertEquals(1d, result1, 0d);

    double result2 = editDistanceLevenshtein.computeSimilarity("ABC", "");
    assertEquals(0d, result2, 0d);

    double result3 = editDistanceLevenshtein.computeSimilarity("ABC", "A");
    assertEquals(1d / 3d, result3, 0.0001);

    double compare1 = editDistanceLevenshtein.computeSimilarity("ABCDEF", "ABCDF");
    double compare2 = editDistanceLevenshtein.computeSimilarity("ABCDEF", "ABCCEF");
    assertEquals(compare1, compare2, 0.0001);
  }
}
