/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponentBaseConfig;
import org.gradoop.famer.linking.blocking.methods.dataStructures.CartesianProductComponent;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.gradoop.famer.linking.blocking.TestUtils.collectAndTransformResultTuples;
import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test class for checking graph pair generation
 */
public class GraphPairTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  private BlockingComponentBaseConfig baseConfig;

  private BlockingComponent blockingComponent;

  @Before
  public void setUp() {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(bob:Person {id:2, name:\"Bob\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(carol:Person {id:3, name:\"Carol\", " + GRAPH_LABEL + ":\"g2\"})" +
      "(dave:Person {id:4, name:\"Dave\", " + GRAPH_LABEL + ":\"g2\"})" +
      "(eve:Person {id:5, name:\"Eve\", " + GRAPH_LABEL + ":\"g3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("*");
    graphPairs.put("*", values);
    Map<String, Set<String>> categoryPairs = new HashMap<>();
    categoryPairs.put("*", values);
    baseConfig = new BlockingComponentBaseConfig(null, graphPairs, categoryPairs);
  }

  @Test
  public void testStarStar() throws Exception {
    blockingComponent = new CartesianProductComponent(baseConfig);
    BlockMaker blockMaker = new BlockMaker(blockingComponent);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices = blockMaker.execute(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(Tuple2.of("Alice", "Bob"),
      Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"), Tuple2.of("Alice", "Eve"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"), Tuple2.of("Bob", "Eve"),
      Tuple2.of("Carol", "Dave"), Tuple2.of("Carol", "Eve"),
      Tuple2.of("Dave", "Eve"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(10, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testPlusPlus() throws Exception {
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("+");
    graphPairs.put("+", values);
    baseConfig.setGraphPairs(graphPairs);
    blockingComponent = new CartesianProductComponent(baseConfig);
    BlockMaker blockMaker = new BlockMaker(blockingComponent);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices = blockMaker.execute(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"), Tuple2.of("Alice", "Eve"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"), Tuple2.of("Bob", "Eve"),
      Tuple2.of("Carol", "Eve"), Tuple2.of("Dave", "Eve"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(8, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testXStar() throws Exception {
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("g1");
    graphPairs.put("*", values);
    Set<String> reverseValues = new HashSet<>();
    reverseValues.add("*");
    graphPairs.put("g1", reverseValues);
    baseConfig.setGraphPairs(graphPairs);
    blockingComponent = new CartesianProductComponent(baseConfig);
    BlockMaker blockMaker = new BlockMaker(blockingComponent);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices = blockMaker.execute(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(Tuple2.of("Alice", "Bob"),
      Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"), Tuple2.of("Alice", "Eve"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"), Tuple2.of("Bob", "Eve"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(7, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testStarX() throws Exception {
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("*");
    graphPairs.put("g1", values);
    Set<String> reverseValues = new HashSet<>();
    reverseValues.add("g1");
    graphPairs.put("*", reverseValues);
    baseConfig.setGraphPairs(graphPairs);
    blockingComponent = new CartesianProductComponent(baseConfig);
    BlockMaker blockMaker = new BlockMaker(blockingComponent);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices = blockMaker.execute(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(Tuple2.of("Alice", "Bob"),
      Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"), Tuple2.of("Alice", "Eve"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"), Tuple2.of("Bob", "Eve"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(7, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testXPlus() throws Exception {
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("g1");
    graphPairs.put("+", values);
    Set<String> reverseValues = new HashSet<>();
    reverseValues.add("g1");
    graphPairs.put("+", reverseValues);
    baseConfig.setGraphPairs(graphPairs);
    blockingComponent = new CartesianProductComponent(baseConfig);
    BlockMaker blockMaker = new BlockMaker(blockingComponent);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices = blockMaker.execute(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"), Tuple2.of("Alice", "Eve"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"), Tuple2.of("Bob", "Eve"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(6, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testPlusX() throws Exception {
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("+");
    graphPairs.put("g1", values);
    Set<String> reverseValues = new HashSet<>();
    reverseValues.add("g1");
    graphPairs.put("+", reverseValues);
    baseConfig.setGraphPairs(graphPairs);
    blockingComponent = new CartesianProductComponent(baseConfig);
    BlockMaker blockMaker = new BlockMaker(blockingComponent);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices = blockMaker.execute(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"), Tuple2.of("Alice", "Eve"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"), Tuple2.of("Bob", "Eve"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(6, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testXY() throws Exception {
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("g2");
    graphPairs.put("g1", values);
    Set<String> reverseValues = new HashSet<>();
    reverseValues.add("g2");
    graphPairs.put("g1", reverseValues);
    baseConfig.setGraphPairs(graphPairs);
    blockingComponent = new CartesianProductComponent(baseConfig);
    BlockMaker blockMaker = new BlockMaker(blockingComponent);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices = blockMaker.execute(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(4, result.size());
    assertEquals(expectedResult, result);
  }
}
