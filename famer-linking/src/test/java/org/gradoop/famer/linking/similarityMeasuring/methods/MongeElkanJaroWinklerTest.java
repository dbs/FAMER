/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link MongeElkanJaroWinkler}
 */
public class MongeElkanJaroWinklerTest {

  private MongeElkanJaroWinkler mongeElkanJaroWinkler = new MongeElkanJaroWinkler(" ", 0.5);

  @Test(expected = Exception.class)
  public void testComputeSimilarityForFirstNullValue() throws Exception {
    mongeElkanJaroWinkler.computeSimilarity(null, "");
  }

  @Test(expected = Exception.class)
  public void testComputeSimilarityForSecondNullValue() throws Exception {
    mongeElkanJaroWinkler.computeSimilarity("", null);
  }

  @Test
  public void testComputeSimilarity() throws Exception {
    double result1 = mongeElkanJaroWinkler.computeSimilarity("ABC", "");

    assertEquals(0d, result1, 0d);

    double result2 = mongeElkanJaroWinkler.computeSimilarity("ABC", "ABC");

    assertEquals(1d, result2, 0d);

    double result3 = mongeElkanJaroWinkler.computeSimilarity("My string", "My tsring");

    assertEquals(0.972, result3, 0.001);
  }
}
