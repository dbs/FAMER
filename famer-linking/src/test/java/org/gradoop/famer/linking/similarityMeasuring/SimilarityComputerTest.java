/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring;

import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponentBaseConfig;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityField;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.TruncateEndComponent;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link SimilarityComputer}
 */
public class SimilarityComputerTest {

  @Test
  public void testComputeSimilarityWithTruncateEndComponent() throws Exception {
    String componentId = "id";
    String sourceGraphLabel = "sourceGraphLabel";
    String sourceLabel = "sourceLabel";
    String sourceAttribute = "sourceAttribute";
    String targetGraphLabel = "targetGraphLabel";
    String targetLabel = "targetLabel";
    String targetAttribute = "targetAttribute";
    double weight = 1d;

    SimilarityComponentBaseConfig baseConfig = new SimilarityComponentBaseConfig(componentId,
      sourceGraphLabel, sourceLabel, sourceAttribute, targetGraphLabel, targetLabel, targetAttribute, weight);

    TruncateEndComponent truncateEndComponent = new TruncateEndComponent(baseConfig, 2);

    SimilarityComputer similarityComputer = new SimilarityComputer(truncateEndComponent);

    SimilarityField similarityField =
      similarityComputer.computeSimilarity(PropertyValue.create("ABC"), PropertyValue.create("DBC"));

    assertEquals(1d, similarityField.getSimilarityValue(), 0d);
    assertEquals(1d, similarityField.getWeight(), 0d);
    assertEquals(componentId, similarityField.getId());
  }
}
