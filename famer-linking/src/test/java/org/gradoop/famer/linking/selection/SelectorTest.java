/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.famer.linking.linking.dataStructures.LinkerComponent;
import org.gradoop.famer.linking.selection.dataStructures.SelectionComponent;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRule;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRuleComponent;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRuleComponentType;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.Condition;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.ConditionOperator;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRule;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRuleComponent;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRuleComponentType;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityField;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test class for {@link Selector}
 */
public class SelectorTest extends GradoopFlinkTestBase {

  private List<Condition> conditions;

  private List<SelectionRuleComponent> selectionRuleComponents;

  private List<AggregatorRuleComponent> aggregatorRuleComponents;

  private DataSet<Tuple3<EPGMVertex, EPGMVertex, SimilarityFieldList>> blockedVerticesSimFieldList;

  @Before
  public void setUp() throws Exception {
    conditions = new ArrayList<>();
    selectionRuleComponents = new ArrayList<>();
    aggregatorRuleComponents = new ArrayList<>();

    EPGMVertex vertexA = createVertex("A");
    EPGMVertex vertexB = createVertex("B");

    SimilarityFieldList similarityFieldListAB = createSimilarityFieldList(0.5, 0.6);

    blockedVerticesSimFieldList = getExecutionEnvironment().fromElements(
      Tuple3.of(vertexA, vertexB, similarityFieldListAB));
  }

  @Test
  public void testSelectorWithNoRule() throws Exception {
    LinkerComponent linkerComponent = new LinkerComponent();
    linkerComponent.setSelectionComponent(new SelectionComponent());

    Selector selector = new Selector(linkerComponent);

    double result = blockedVerticesSimFieldList.flatMap(selector).collect().get(0).f2;

    assertEquals((0.5 + 0.6) / 2d, result, 0d);
  }

  @Test
  public void testSelectorWithSelectionRule() throws Exception {
    // create conditions -> compute simValue only if simField1 is equal 0.5
    conditions = Collections.singletonList(
      new Condition("equal", "sim1", ConditionOperator.EQUAL, 0.5));

    selectionRuleComponents = Collections.singletonList(
      new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "equal"));

    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);
    SelectionComponent selectionComponent = new SelectionComponent(selectionRule);

    LinkerComponent linkerComponent = new LinkerComponent();
    linkerComponent.setSelectionComponent(selectionComponent);

    Selector selector = new Selector(linkerComponent);

    double result = blockedVerticesSimFieldList.flatMap(selector).collect().get(0).f2;

    assertEquals((0.5 + 0.6) / 2d, result, 0d);
  }

  @Test
  public void testSelectorWitAggregationRule() throws Exception {
    // create aggregation -> sum up both simValues
    aggregatorRuleComponents.add(
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim1"));
    aggregatorRuleComponents.add(
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "PLUS"));
    aggregatorRuleComponents.add(
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim2"));

    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 0d);
    SelectionComponent selectionComponent = new SelectionComponent(aggregatorRule);

    LinkerComponent linkerComponent = new LinkerComponent();
    linkerComponent.setSelectionComponent(selectionComponent);

    Selector selector = new Selector(linkerComponent);

    double result = blockedVerticesSimFieldList.flatMap(selector).collect().get(0).f2;

    assertEquals(0.5 + 0.6, result, 0d);
  }

  @Test
  public void testSelectorWithSelectionAndAggregationRule() throws Exception {
    // create conditions -> compute simValue only if simField1 is equal 0.5
    conditions = Collections.singletonList(
      new Condition("equal", "sim1", ConditionOperator.EQUAL, 0.5));

    selectionRuleComponents = Collections.singletonList(
      new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "equal"));

    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);

    // create aggregation -> sum up both simValues
    aggregatorRuleComponents.add(
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim1"));
    aggregatorRuleComponents.add(
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "PLUS"));
    aggregatorRuleComponents.add(
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim2"));

    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 0d);

    SelectionComponent selectionComponent = new SelectionComponent(selectionRule, aggregatorRule);

    LinkerComponent linkerComponent = new LinkerComponent();
    linkerComponent.setSelectionComponent(selectionComponent);

    Selector selector = new Selector(linkerComponent);

    double result = blockedVerticesSimFieldList.flatMap(selector).collect().get(0).f2;

    assertEquals(0.5 + 0.6, result, 0d);
  }

  @Test
  public void testSelectorWithSelectionAndAggregationRuleForAggregationThreshold() throws Exception {
    // create conditions -> compute simValue only if simField1 is equal 0.5
    conditions = Collections.singletonList(
      new Condition("equal", "sim1", ConditionOperator.EQUAL, 0.5));

    selectionRuleComponents = Collections.singletonList(
      new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "equal"));

    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);

    // create aggregation -> sum up both simValues
    aggregatorRuleComponents.add(
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim1"));
    aggregatorRuleComponents.add(
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "PLUS"));
    aggregatorRuleComponents.add(
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim2"));

    // result must be greater 2.0 to be returned, we expect an empty result list
    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 2d);

    SelectionComponent selectionComponent = new SelectionComponent(selectionRule, aggregatorRule);

    LinkerComponent linkerComponent = new LinkerComponent();
    linkerComponent.setSelectionComponent(selectionComponent);

    Selector selector = new Selector(linkerComponent);

    assertTrue(blockedVerticesSimFieldList.flatMap(selector).collect().isEmpty());
  }

  private EPGMVertex createVertex(String id) {
    Properties properties = new Properties();
    properties.set("id", id);

    EPGMVertex vertex = new EPGMVertex();
    vertex.setId(GradoopId.get());
    vertex.setLabel("");
    vertex.setProperties(properties);

    return vertex;
  }

  private SimilarityFieldList createSimilarityFieldList(double sim1, double sim2) {
    SimilarityFieldList similarityFieldList = new SimilarityFieldList();

    SimilarityField similarityField1 = new SimilarityField("sim1", sim1, 1.0);
    SimilarityField similarityField2 = new SimilarityField("sim2", sim2, 1.0);

    similarityFieldList.add(similarityField1);
    similarityFieldList.add(similarityField2);

    return similarityFieldList;
  }
}
