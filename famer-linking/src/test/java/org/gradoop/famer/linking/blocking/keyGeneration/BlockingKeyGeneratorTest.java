/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.keyGeneration;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.FullAttributeComponent;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.PrefixLengthComponent;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.QGramsComponent;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.WordTokenizerComponent;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test class for {@link BlockingKeyGenerator}
 */
public class BlockingKeyGeneratorTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  private final String idProperty = "id";

  private final String attribute = "name";

  @Before
  public void setUp() {
    String graphString = "input[" +
      "(alice:Person {id: 1, name : \"Alice\"})" +
      "(bob:Person {id: 2, name : \"Bob\"})" +
      "(carol:Person {id: 3, name : \"Carol\"})" +
      "(dave:Person {id: 4, name : \"\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");
  }

  @Test
  public void testBlockingKeyGeneratorForFullAttribute() throws Exception {
    BlockingKeyGenerator blockingKeyGenerator =
      new BlockingKeyGenerator(new FullAttributeComponent(attribute));

    DataSet<Tuple2<EPGMVertex, String>> vertexBlockingKey =
      inputGraph.getVertices().flatMap(blockingKeyGenerator);

    List<Tuple2<Integer, String>> expectedResult =
      Arrays.asList(Tuple2.of(1, "alice"), Tuple2.of(2, "bob"), Tuple2.of(3, "carol"), Tuple2.of(4, ""));

    List<Tuple2<Integer, String>> result = vertexBlockingKey.collect().stream()
      .map(tuple -> Tuple2.of(tuple.f0.getPropertyValue(idProperty).getInt(), tuple.f1))
      .sorted(Comparator.comparing(o -> o.f0))
      .collect(Collectors.toList());

    assertFalse(result.isEmpty());
    assertEquals(4, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testBlockingKeyGeneratorForPrefixLength() throws Exception {
    BlockingKeyGenerator blockingKeyGenerator =
      new BlockingKeyGenerator(new PrefixLengthComponent(attribute, 3));

    DataSet<Tuple2<EPGMVertex, String>> vertexBlockingKey =
      inputGraph.getVertices().flatMap(blockingKeyGenerator);

    List<Tuple2<Integer, String>> expectedResult =
      Arrays.asList(Tuple2.of(1, "ali"), Tuple2.of(2, "bob"), Tuple2.of(3, "car"), Tuple2.of(4, ""));

    List<Tuple2<Integer, String>> result = vertexBlockingKey.collect().stream()
      .map(tuple -> Tuple2.of(tuple.f0.getPropertyValue(idProperty).getInt(), tuple.f1))
      .sorted(Comparator.comparing(o -> o.f0))
      .collect(Collectors.toList());

    assertFalse(result.isEmpty());
    assertEquals(4, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testBlockingKeyGeneratorForQGrams() throws Exception {
    BlockingKeyGenerator blockingKeyGenerator =
      new BlockingKeyGenerator(new QGramsComponent(attribute, 3, 1.0));

    DataSet<Tuple2<EPGMVertex, String>> vertexBlockingKey =
      inputGraph.getVertices().flatMap(blockingKeyGenerator);

    List<Tuple2<Integer, String>> expectedResult =
      Arrays.asList(Tuple2.of(1, "alilicice"), Tuple2.of(2, "bob"), Tuple2.of(3, "cararorol"),
        Tuple2.of(4, ""));

    List<Tuple2<Integer, String>> result = vertexBlockingKey.collect().stream()
      .map(tuple -> Tuple2.of(tuple.f0.getPropertyValue(idProperty).getInt(), tuple.f1))
      .sorted(Comparator.comparing(o -> o.f0))
      .collect(Collectors.toList());

    assertFalse(result.isEmpty());
    assertEquals(4, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testBlockingKeyGeneratorForWordTokenizer() throws Exception {
    BlockingKeyGenerator blockingKeyGenerator =
      new BlockingKeyGenerator(new WordTokenizerComponent(attribute, "i"));

    DataSet<Tuple2<EPGMVertex, String>> vertexBlockingKey =
      inputGraph.getVertices().flatMap(blockingKeyGenerator);

    List<Tuple2<Integer, String>> expectedResult =
      Arrays.asList(Tuple2.of(1, "al"), Tuple2.of(1, "ce"), Tuple2.of(2, "bob"), Tuple2.of(3, "carol"),
        Tuple2.of(4, ""));

    List<Tuple2<Integer, String>> result = vertexBlockingKey.collect().stream()
      .map(tuple -> Tuple2.of(tuple.f0.getPropertyValue(idProperty).getInt(), tuple.f1))
      .sorted(Comparator.comparing(o -> o.f0))
      .collect(Collectors.toList());

    assertFalse(result.isEmpty());
    assertEquals(5, result.size());
    assertEquals(expectedResult, result);
  }
}
