/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link NumericalSimilarityWithMaxPercentage}
 */
public class NumericalSimilarityWithMaxPercentageTest {

  private NumericalSimilarityWithMaxPercentage numericalSimilarityWithMaxPercentage =
    new NumericalSimilarityWithMaxPercentage(10d);

  @Test(expected = Exception.class)
  public void testComputeSimilarityForFirstNullValue() throws Exception {
    numericalSimilarityWithMaxPercentage.computeSimilarity(null, 1d);
  }

  @Test(expected = Exception.class)
  public void testComputeSimilarityForSecondNullValue() throws Exception {
    numericalSimilarityWithMaxPercentage.computeSimilarity(1d, null);
  }

  @Test
  public void testComputeSimilarity() throws Exception {
    double result1 = numericalSimilarityWithMaxPercentage.computeSimilarity(1d, 0.89);

    assertEquals(0d, result1, 0d);

    double result2 = numericalSimilarityWithMaxPercentage.computeSimilarity(1d, 1d);

    assertEquals(1d, result2, 0d);

    double result3 = numericalSimilarityWithMaxPercentage.computeSimilarity(1d, 0.95);

    assertEquals(0.5, result3, 0.0001);
  }
}
