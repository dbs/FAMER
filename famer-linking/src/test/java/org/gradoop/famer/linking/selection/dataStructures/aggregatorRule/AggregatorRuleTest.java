/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection.dataStructures.aggregatorRule;

import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityField;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link AggregatorRule}
 */
public class AggregatorRuleTest {

  private List<AggregatorRuleComponent> aggregatorRuleComponents;

  private SimilarityFieldList similarityFieldList;

  @Before
  public void setUp() {
    aggregatorRuleComponents = new ArrayList<>();
    similarityFieldList = new SimilarityFieldList();
  }

  @Test
  public void testAggregateWithMissingValue() {
    AggregatorRuleComponent simField1 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim1");
    AggregatorRuleComponent plus =
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "PLUS");
    AggregatorRuleComponent simField2 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim2");

    aggregatorRuleComponents.add(simField1);
    aggregatorRuleComponents.add(plus);
    aggregatorRuleComponents.add(simField2);

    similarityFieldList.add(new SimilarityField("sim2", 1d, 1d));

    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 0d);

    assertEquals(1d, aggregatorRule.computeAggregatedSimilarity(similarityFieldList), 0d);
  }

  @Test
  public void testAggregateWithMinus() {
    AggregatorRuleComponent simField1 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim1");
    AggregatorRuleComponent minus =
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "MINUS");
    AggregatorRuleComponent simField2 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim2");

    aggregatorRuleComponents.add(simField1);
    aggregatorRuleComponents.add(minus);
    aggregatorRuleComponents.add(simField2);

    similarityFieldList.add(new SimilarityField("sim1", 2d, 1d));
    similarityFieldList.add(new SimilarityField("sim2", 1d, 1d));

    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 0d);

    assertEquals(1d, aggregatorRule.computeAggregatedSimilarity(similarityFieldList), 0d);
  }

  @Test
  public void testAggregateWithPlus() {
    AggregatorRuleComponent simField1 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim1");
    AggregatorRuleComponent plus =
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "PLUS");
    AggregatorRuleComponent simField2 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim2");

    aggregatorRuleComponents.add(simField1);
    aggregatorRuleComponents.add(plus);
    aggregatorRuleComponents.add(simField2);

    similarityFieldList.add(new SimilarityField("sim1", 2d, 1d));
    similarityFieldList.add(new SimilarityField("sim2", 1d, 1d));

    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 0d);

    assertEquals(3d, aggregatorRule.computeAggregatedSimilarity(similarityFieldList), 0d);
  }

  @Test
  public void testAggregateWithMultiply() {
    AggregatorRuleComponent simField1 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim1");
    AggregatorRuleComponent multiply =
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "MULTIPLY");
    AggregatorRuleComponent simField2 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim2");

    aggregatorRuleComponents.add(simField1);
    aggregatorRuleComponents.add(multiply);
    aggregatorRuleComponents.add(simField2);

    similarityFieldList.add(new SimilarityField("sim1", 2d, 1d));
    similarityFieldList.add(new SimilarityField("sim2", 2d, 1d));

    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 0d);

    assertEquals(4d, aggregatorRule.computeAggregatedSimilarity(similarityFieldList), 0d);
  }

  @Test
  public void testAggregateWithDivision() {
    AggregatorRuleComponent simField1 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim1");
    AggregatorRuleComponent division =
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "DIVISION");
    AggregatorRuleComponent simField2 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim2");

    aggregatorRuleComponents.add(simField1);
    aggregatorRuleComponents.add(division);
    aggregatorRuleComponents.add(simField2);

    similarityFieldList.add(new SimilarityField("sim1", 4d, 1d));
    similarityFieldList.add(new SimilarityField("sim2", 2d, 1d));

    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 0d);

    assertEquals(2d, aggregatorRule.computeAggregatedSimilarity(similarityFieldList), 0d);
  }

  @Test
  public void testAggregateWithConstant() {
    AggregatorRuleComponent simField1 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim1");
    AggregatorRuleComponent plus =
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "PLUS");
    AggregatorRuleComponent constant =
      new AggregatorRuleComponent(AggregatorRuleComponentType.CONSTANT, "10");

    aggregatorRuleComponents.add(simField1);
    aggregatorRuleComponents.add(plus);
    aggregatorRuleComponents.add(constant);

    similarityFieldList.add(new SimilarityField("sim1", 2d, 1d));

    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 0d);

    assertEquals(12d, aggregatorRule.computeAggregatedSimilarity(similarityFieldList), 0d);
  }

  @Test
  public void testAggregateWithCombination() {
    // define rule components for rule: (1 * 2) - (3 / 4) + 10 - aggregates to 11.25
    AggregatorRuleComponent openPar =
      new AggregatorRuleComponent(AggregatorRuleComponentType.OPEN_PARENTHESIS, "(");
    AggregatorRuleComponent closePar =
      new AggregatorRuleComponent(AggregatorRuleComponentType.CLOSE_PARENTHESIS, ")");
    AggregatorRuleComponent minus =
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "MINUS");
    AggregatorRuleComponent plus =
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "PLUS");
    AggregatorRuleComponent multiply =
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "MULTIPLY");
    AggregatorRuleComponent division =
      new AggregatorRuleComponent(AggregatorRuleComponentType.ARITHMETIC_OPERATOR, "DIVISION");
    AggregatorRuleComponent constantTen =
      new AggregatorRuleComponent(AggregatorRuleComponentType.CONSTANT, "10");
    AggregatorRuleComponent simField1 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim1");
    AggregatorRuleComponent simField2 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim2");
    AggregatorRuleComponent simField3 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim3");
    AggregatorRuleComponent simField4 =
      new AggregatorRuleComponent(AggregatorRuleComponentType.SIMILARITY_FIELD_ID, "sim4");

    aggregatorRuleComponents.add(openPar);
    aggregatorRuleComponents.add(simField1);
    aggregatorRuleComponents.add(multiply);
    aggregatorRuleComponents.add(simField2);
    aggregatorRuleComponents.add(closePar);
    aggregatorRuleComponents.add(minus);
    aggregatorRuleComponents.add(openPar);
    aggregatorRuleComponents.add(simField3);
    aggregatorRuleComponents.add(division);
    aggregatorRuleComponents.add(simField4);
    aggregatorRuleComponents.add(closePar);
    aggregatorRuleComponents.add(plus);
    aggregatorRuleComponents.add(constantTen);

    SimilarityField similarityField1 = new SimilarityField("sim1", 1d, 1d);
    SimilarityField similarityField2 = new SimilarityField("sim2", 2d, 1d);
    SimilarityField similarityField3 = new SimilarityField("sim3", 3d, 1d);
    SimilarityField similarityField4 = new SimilarityField("sim4", 4d, 1d);
    similarityFieldList.add(similarityField1);
    similarityFieldList.add(similarityField2);
    similarityFieldList.add(similarityField3);
    similarityFieldList.add(similarityField4);

    AggregatorRule aggregatorRule = new AggregatorRule(aggregatorRuleComponents, 0d);

    double result = aggregatorRule.computeAggregatedSimilarity(similarityFieldList);

    assertEquals(11.25, result, 0d);
  }
}
