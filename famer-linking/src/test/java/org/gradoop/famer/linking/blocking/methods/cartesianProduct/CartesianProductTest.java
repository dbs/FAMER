/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.cartesianProduct;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.keyGeneration.BlockingKeyGenerator;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.FullAttributeComponent;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.KeyGeneratorComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponentBaseConfig;
import org.gradoop.famer.linking.blocking.methods.dataStructures.CartesianProductComponent;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.gradoop.famer.linking.blocking.TestUtils.collectAndTransformResultTuples;
import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for {@link CartesianProduct}
 */
public class CartesianProductTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  private BlockingComponentBaseConfig baseConfig;

  @Before
  public void setUp() {
    KeyGeneratorComponent keyGeneratorComponent = new FullAttributeComponent("name");
    BlockingKeyGenerator blockingKeyGenerator = new BlockingKeyGenerator(keyGeneratorComponent);
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("*");
    graphPairs.put("*", values);
    Map<String, Set<String>> categoryPairs = new HashMap<>();
    categoryPairs.put("*", values);
    baseConfig = new BlockingComponentBaseConfig(blockingKeyGenerator, graphPairs, categoryPairs);
  }

  /**
   * Test if CartesianProduct works correct when "allowed graph pairs" contains "*"
   */
  @Test
  public void testGeneratePairedVerticesForAllGraphPairs() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(bob:Person {id:2, name:\"Bob\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(carol:Person {id:3, name:\"Carol\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(dave:Person {id:4, name:\"Dave\", " + GRAPH_LABEL + ":\"g1\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    CartesianProductComponent cartesianProductComponent = new CartesianProductComponent(baseConfig);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new CartesianProduct(cartesianProductComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Bob"), Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"),
      Tuple2.of("Carol", "Dave"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(6, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * Test if CartesianProduct works correct when "allowed graph pairs" is limited
   */
  @Test
  public void testGeneratePairedVerticesForLimitedGraphPairs() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(bob:Person {id:2, name:\"Bob\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(carol:Person {id:3, name:\"Carol\", " + GRAPH_LABEL + ":\"g2\"})" +
      "(dave:Person {id:4, name:\"Dave\", " + GRAPH_LABEL + ":\"g3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    Map<String, Set<String>> limitedGraphPairs = new HashMap<>();
    // g1 is limited to g1,g2,g3
    Set<String> value1 = new HashSet<>();
    value1.add("g1");
    value1.add("g2");
    value1.add("g3");
    limitedGraphPairs.put("g1", value1);
    // g2 is limited to g1
    Set<String> value2 = new HashSet<>();
    value2.add("g1");
    limitedGraphPairs.put("g2", value2);
    // g3 is limited to g1
    Set<String> value3 = new HashSet<>();
    value3.add("g1");
    limitedGraphPairs.put("g3", value3);

    baseConfig.setGraphPairs(limitedGraphPairs);

    CartesianProductComponent cartesianProductComponent = new CartesianProductComponent(baseConfig);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new CartesianProduct(cartesianProductComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Bob"), Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(5, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * Test if CartesianProduct works correct when "allowed category pairs" is limited
   */
  @Test
  public void testGeneratePairedVerticesForLimitedCategoryPairs() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(bob:Teacher {id:2, name:\"Bob\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(carol:Teacher {id:3, name:\"Carol\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(dave:Person {id:4, name:\"Dave\", " + GRAPH_LABEL + ":\"g1\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    Map<String, Set<String>> limitedCategoryPairs = new HashMap<>();
    // Person is limited to Teacher
    Set<String> value1 = new HashSet<>();
    value1.add("Teacher");
    limitedCategoryPairs.put("Person", value1);
    // Teacher is limited to Teacher
    Set<String> value2 = new HashSet<>();
    value2.add("Teacher");
    limitedCategoryPairs.put("Teacher", value2);

    baseConfig.setCategoryPairs(limitedCategoryPairs);

    CartesianProductComponent cartesianProductComponent = new CartesianProductComponent(baseConfig);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new CartesianProduct(cartesianProductComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Bob"), Tuple2.of("Alice", "Carol"), Tuple2.of("Bob", "Carol"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(3, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * Test if CartesianProduct works correct when the input graph has no vertices
   */
  @Test
  public void testGeneratePairedVerticesForEmptyGraph() throws Exception {
    inputGraph = getLoaderFromString("input[]").getLogicalGraphByVariable("input");

    CartesianProductComponent cartesianProductComponent = new CartesianProductComponent(baseConfig);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new CartesianProduct(cartesianProductComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<EPGMVertex, EPGMVertex>> result = pairedVertices.collect();

    assertTrue(result.isEmpty());
  }

  /**
   * Test if CartesianProduct works correct when the input graph has only one vertex
   */
  @Test
  public void testGeneratePairedVerticesForOneVertex() throws Exception {
    inputGraph = getLoaderFromString("input[(alice:Person {id:1, name:\"Alice\"})]")
      .getLogicalGraphByVariable("input");

    CartesianProductComponent cartesianProductComponent = new CartesianProductComponent(baseConfig);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new CartesianProduct(cartesianProductComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<EPGMVertex, EPGMVertex>> result = pairedVertices.collect();

    assertTrue(result.isEmpty());
  }

  /**
   * Test if CartesianProduct throws exception when vertices miss the graphLabel property
   */
  @Test(expected = Exception.class)
  public void testGeneratePairedVerticesForMissingGraphLabelProperty() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\"})" +
      "(bob:Teacher {id:2, name:\"Bob\"})" +
      "(carol:Teacher {id:3, name:\"Carol\"})" +
      "(dave:Person {id:4, name:\"Dave\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    CartesianProductComponent cartesianProductComponent = new CartesianProductComponent(baseConfig);

    new CartesianProduct(cartesianProductComponent).generatePairedVertices(inputGraph.getVertices())
      .collect();
  }
}
