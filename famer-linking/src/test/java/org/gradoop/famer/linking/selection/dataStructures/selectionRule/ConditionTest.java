/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection.dataStructures.selectionRule;

import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for {@link Condition}
 */
public class ConditionTest {

  private Condition condition;

  @Test
  public void testCheckConditionEqual() {
    condition = new Condition("id", "simId", ConditionOperator.EQUAL, 1d);

    assertTrue(condition.checkCondition(1d));

    assertFalse(condition.checkCondition(2d));
  }

  @Test
  public void testCheckConditionGreater() {
    condition = new Condition("id", "simId", ConditionOperator.GREATER, 1d);

    assertTrue(condition.checkCondition(2d));

    assertFalse(condition.checkCondition(1d));
  }

  @Test
  public void testCheckConditionGreaterEqual() {
    condition = new Condition("id", "simId", ConditionOperator.GREATER_EQUAL, 1d);

    assertTrue(condition.checkCondition(1d));
    assertTrue(condition.checkCondition(2d));

    assertFalse(condition.checkCondition(0d));
  }

  @Test
  public void testCheckConditionNotEqual() {
    condition = new Condition("id", "simId", ConditionOperator.NOT_EQUAL, 1d);

    assertTrue(condition.checkCondition(2d));

    assertFalse(condition.checkCondition(1d));
  }

  @Test
  public void testCheckConditionSmaller() {
    condition = new Condition("id", "simId", ConditionOperator.SMALLER, 1d);

    assertTrue(condition.checkCondition(0d));

    assertFalse(condition.checkCondition(1d));
  }

  @Test
  public void testCheckConditionSmallerEqual() {
    condition = new Condition("id", "simId", ConditionOperator.SMALLER_EQUAL, 1d);

    assertTrue(condition.checkCondition(0d));
    assertTrue(condition.checkCondition(1d));

    assertFalse(condition.checkCondition(2d));
  }

  @Test(expected = Exception.class)
  public void testCheckConditionInvalidOperator() {
    new Condition("id", "simId", null, 1d).checkCondition(1d);
  }
}
