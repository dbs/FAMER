/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.selection.dataStructures.selectionRule;

import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityField;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityFieldList;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for {@link SelectionRule}
 */
public class SelectionRuleTest {

  private List<Condition> conditions;

  private List<SelectionRuleComponent> selectionRuleComponents;

  private SimilarityFieldList similarityFieldList;

  @Before
  public void setUp() {
    conditions = new ArrayList<>();
    selectionRuleComponents = new ArrayList<>();
    similarityFieldList = new SimilarityFieldList();
  }

  @Test
  public void testSelectionWithEqual() {
    conditions.add(new Condition("equal", "sim", ConditionOperator.EQUAL, 1d));

    selectionRuleComponents.add(new SelectionRuleComponent(SelectionRuleComponentType.CONDITION,
      "equal"));

    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);

    similarityFieldList.add(new SimilarityField("sim", 1d, 1d));

    assertTrue(selectionRule.check(similarityFieldList));

    similarityFieldList.getSimilarityFields().clear();
    similarityFieldList.add(new SimilarityField("sim", 0d, 1d));

    assertFalse(selectionRule.check(similarityFieldList));
  }

  @Test
  public void testSelectionWithNotEqual() {
    conditions.add(new Condition("notEqual", "sim", ConditionOperator.NOT_EQUAL, 1d));

    selectionRuleComponents.add(new SelectionRuleComponent(SelectionRuleComponentType.CONDITION,
      "notEqual"));

    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);

    similarityFieldList.add(new SimilarityField("sim", 0d, 1d));

    assertTrue(selectionRule.check(similarityFieldList));

    similarityFieldList.getSimilarityFields().clear();
    similarityFieldList.add(new SimilarityField("sim", 1d, 1d));

    assertFalse(selectionRule.check(similarityFieldList));
  }

  @Test
  public void testSelectionWithGreater() {
    conditions.add(new Condition("greater", "sim", ConditionOperator.GREATER, 1d));

    selectionRuleComponents.add(new SelectionRuleComponent(SelectionRuleComponentType.CONDITION,
      "greater"));

    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);

    similarityFieldList.add(new SimilarityField("sim", 2d, 1d));

    assertTrue(selectionRule.check(similarityFieldList));

    similarityFieldList.getSimilarityFields().clear();
    similarityFieldList.add(new SimilarityField("sim", 1d, 1d));

    assertFalse(selectionRule.check(similarityFieldList));
  }

  @Test
  public void testSelectionWithGreaterEqual() {
    conditions.add(new Condition("greaterEqual", "sim", ConditionOperator.GREATER_EQUAL, 1d));

    selectionRuleComponents.add(new SelectionRuleComponent(SelectionRuleComponentType.CONDITION,
      "greaterEqual"));

    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);

    similarityFieldList.add(new SimilarityField("sim", 1d, 1d));

    assertTrue(selectionRule.check(similarityFieldList));

    similarityFieldList.getSimilarityFields().clear();
    similarityFieldList.add(new SimilarityField("sim", 2d, 1d));

    assertTrue(selectionRule.check(similarityFieldList));

    similarityFieldList.getSimilarityFields().clear();
    similarityFieldList.add(new SimilarityField("sim", 0d, 1d));

    assertFalse(selectionRule.check(similarityFieldList));
  }

  @Test
  public void testSelectionWithSmaller() {
    conditions.add(new Condition("smaller", "sim", ConditionOperator.SMALLER, 1d));

    selectionRuleComponents.add(new SelectionRuleComponent(SelectionRuleComponentType.CONDITION,
      "smaller"));

    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);

    similarityFieldList.getSimilarityFields().clear();
    similarityFieldList.add(new SimilarityField("sim", 0d, 1d));

    assertTrue(selectionRule.check(similarityFieldList));

    similarityFieldList.getSimilarityFields().clear();
    similarityFieldList.add(new SimilarityField("sim", 1d, 1d));

    assertFalse(selectionRule.check(similarityFieldList));
  }

  @Test
  public void testSelectionWithSmallerEqual() {
    conditions.add(new Condition("smallerEqual", "sim", ConditionOperator.SMALLER_EQUAL, 1d));

    selectionRuleComponents.add(new SelectionRuleComponent(SelectionRuleComponentType.CONDITION,
      "smallerEqual"));

    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);

    similarityFieldList.add(new SimilarityField("sim", 1d, 1d));

    assertTrue(selectionRule.check(similarityFieldList));

    similarityFieldList.getSimilarityFields().clear();
    similarityFieldList.add(new SimilarityField("sim", 0d, 1d));

    assertTrue(selectionRule.check(similarityFieldList));

    similarityFieldList.getSimilarityFields().clear();
    similarityFieldList.add(new SimilarityField("sim", 2d, 1d));

    assertFalse(selectionRule.check(similarityFieldList));
  }

  @Test
  public void testSelectionWithMissingSimilarityField() {
    conditions.add(new Condition("equal", "sim", ConditionOperator.EQUAL, 1d));
    selectionRuleComponents.add(new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "equal"));
    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);
    assertFalse(selectionRule.check(similarityFieldList));

    conditions.clear();
    conditions.add(new Condition("equal", "sim", ConditionOperator.EQUAL, 1d, true));
    selectionRule = new SelectionRule(conditions, selectionRuleComponents);
    assertTrue(selectionRule.check(similarityFieldList));

    conditions.clear();
    conditions.add(new Condition("equal", "sim", ConditionOperator.EQUAL, 1d, false));
    selectionRule = new SelectionRule(conditions, selectionRuleComponents);
    assertFalse(selectionRule.check(similarityFieldList));
  }

  @Test
  public void testSelectionWithCombinationOfSelectionOperators() {
    // create conditions
    Condition condEqual1 = new Condition("cond1", "sim1", ConditionOperator.EQUAL, 1d);
    Condition condEqual2 = new Condition("cond2", "sim2", ConditionOperator.EQUAL, 1d);
    Condition condGreater1 = new Condition("cond3", "sim3", ConditionOperator.GREATER, 0.5);
    Condition condGreater2 = new Condition("cond4", "sim4", ConditionOperator.GREATER, 0.5);
    conditions.add(condEqual1);
    conditions.add(condEqual2);
    conditions.add(condGreater1);
    conditions.add(condGreater2);

    // create selection rule components for rule: (Condition1 AND Condition2) OR (Condition3 AND Condition4)
    SelectionRuleComponent openPar =
      new SelectionRuleComponent(SelectionRuleComponentType.OPEN_PARENTHESIS, "(");
    SelectionRuleComponent closePar =
      new SelectionRuleComponent(SelectionRuleComponentType.CLOSE_PARENTHESIS, ")");
    SelectionRuleComponent andComp =
      new SelectionRuleComponent(SelectionRuleComponentType.SELECTION_OPERATOR, "AND");
    SelectionRuleComponent orComp =
      new SelectionRuleComponent(SelectionRuleComponentType.SELECTION_OPERATOR, "OR");

    SelectionRuleComponent cond1Comp =
      new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "cond1");
    SelectionRuleComponent cond2Comp =
      new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "cond2");
    SelectionRuleComponent cond3Comp =
      new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "cond3");
    SelectionRuleComponent cond4Comp =
      new SelectionRuleComponent(SelectionRuleComponentType.CONDITION, "cond4");

    selectionRuleComponents.add(openPar);
    selectionRuleComponents.add(cond1Comp);
    selectionRuleComponents.add(andComp);
    selectionRuleComponents.add(cond2Comp);
    selectionRuleComponents.add(closePar);
    selectionRuleComponents.add(orComp);
    selectionRuleComponents.add(openPar);
    selectionRuleComponents.add(cond3Comp);
    selectionRuleComponents.add(andComp);
    selectionRuleComponents.add(cond4Comp);
    selectionRuleComponents.add(closePar);

    SelectionRule selectionRule = new SelectionRule(conditions, selectionRuleComponents);

    // create similarity fields for successful evaluation against conditions
    SimilarityField similarityField1 = new SimilarityField("sim1", 1d, 1d);
    SimilarityField similarityField2 = new SimilarityField("sim2", 1d, 1d);
    SimilarityField similarityField3 = new SimilarityField("sim3", 1d, 1d);
    SimilarityField similarityField4 = new SimilarityField("sim4", 1d, 1d);
    similarityFieldList.add(similarityField1);
    similarityFieldList.add(similarityField2);
    similarityFieldList.add(similarityField3);
    similarityFieldList.add(similarityField4);

    assertTrue(selectionRule.check(similarityFieldList));

    // create similarity fields for failing evaluation against conditions
    similarityFieldList = new SimilarityFieldList();
    similarityField1 = new SimilarityField("sim1", 0d, 1d);
    similarityField2 = new SimilarityField("sim2", 1d, 1d);
    similarityField3 = new SimilarityField("sim3", 0d, 1d);
    similarityField4 = new SimilarityField("sim4", 1d, 1d);
    similarityFieldList.add(similarityField1);
    similarityFieldList.add(similarityField2);
    similarityFieldList.add(similarityField3);
    similarityFieldList.add(similarityField4);

    assertFalse(selectionRule.check(similarityFieldList));
  }
}
