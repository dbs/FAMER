/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.keyGeneration.BlockingKeyGenerator;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.FullAttributeComponent;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.KeyGeneratorComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponentBaseConfig;
import org.gradoop.famer.linking.blocking.methods.dataStructures.CartesianProductComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.SortedNeighborhoodComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.StandardBlockingComponent;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.gradoop.famer.linking.blocking.TestUtils.collectAndTransformResultTuples;
import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * Test class for {@link BlockMaker}
 */
public class BlockMakerTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  private BlockingComponentBaseConfig baseConfig;

  private BlockingComponent blockingComponent;

  private final int parallelismDegree = 4;

  @Before
  public void setUp() {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", key: \"a\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(bob:Person {id:2, name:\"Bob\", key: \"a\", " + GRAPH_LABEL + ":\"g2\"})" +
      "(carol:Person {id:3, name:\"Carol\", key: \"b\", " + GRAPH_LABEL + ":\"g3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    KeyGeneratorComponent keyGeneratorComponent = new FullAttributeComponent("key");
    BlockingKeyGenerator blockingKeyGenerator = new BlockingKeyGenerator(keyGeneratorComponent);
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("*");
    graphPairs.put("*", values);
    Map<String, Set<String>> categoryPairs = new HashMap<>();
    categoryPairs.put("*", values);
    baseConfig = new BlockingComponentBaseConfig(blockingKeyGenerator, graphPairs, categoryPairs);
  }

  @Test
  public void testExecuteWithCartesianProduct() throws Exception {
    blockingComponent = new CartesianProductComponent(baseConfig);
    BlockMaker blockMaker = new BlockMaker(blockingComponent);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices = blockMaker.execute(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Bob"), Tuple2.of("Alice", "Carol"), Tuple2.of("Bob", "Carol"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(3, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testExecuteWithSortedNeighborhood() throws Exception {
    blockingComponent = new SortedNeighborhoodComponent(baseConfig, 2, parallelismDegree);
    BlockMaker blockMaker = new BlockMaker(blockingComponent);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices = blockMaker.execute(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Bob"), Tuple2.of("Bob", "Carol"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(2, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testExecuteWithStandardBlocking() throws Exception {
    blockingComponent = new StandardBlockingComponent(baseConfig, parallelismDegree);
    BlockMaker blockMaker = new BlockMaker(blockingComponent);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices = blockMaker.execute(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Collections.singletonList(Tuple2.of("Alice", "Bob"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices, "name");

    assertFalse(result.isEmpty());
    assertEquals(1, result.size());
    assertEquals(expectedResult, result);
  }
}
