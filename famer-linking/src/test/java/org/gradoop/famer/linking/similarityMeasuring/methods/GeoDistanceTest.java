/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.similarityMeasuring.methods;

import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Test class for {@link GeoDistance}
 */
public class GeoDistanceTest {

  private GeoDistance geoDistance = new GeoDistance(50d);

  @Test(expected = Exception.class)
  public void testComputeSimilarityForFirstNullValue() throws Exception {
    geoDistance.computeSimilarity(null, Tuple2.of(PropertyValue.create(0d), PropertyValue.create(0d)));
  }

  @Test(expected = Exception.class)
  public void testComputeSimilarityForSecondNullValue() throws Exception {
    geoDistance.computeSimilarity(Tuple2.of(PropertyValue.create(0d), PropertyValue.create(0d)), null);
  }

  @Test
  public void testComputeSimilarity() throws Exception {
    Tuple2<PropertyValue, PropertyValue> leipzig =
      Tuple2.of(PropertyValue.create(51.343479), PropertyValue.create(12.387772));

    Tuple2<PropertyValue, PropertyValue> markkleeberg =
      Tuple2.of(PropertyValue.create(51.2748681), PropertyValue.create(12.3664574));

    Tuple2<PropertyValue, PropertyValue> berlin =
      Tuple2.of(PropertyValue.create(52.5200), PropertyValue.create(13.4050));

    double result1 = geoDistance.computeSimilarity(leipzig, markkleeberg);
    double result2 = geoDistance.computeSimilarity(leipzig, berlin);
    double result3 = geoDistance.computeSimilarity(leipzig, leipzig);

    assertEquals(0.844, result1, 0.001d);
    assertEquals(0d, result2, 0d);
    assertEquals(1d, result3, 0d);
  }
}
