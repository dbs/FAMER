/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.linking.blocking.methods.sortedNeighborhood;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.linking.blocking.keyGeneration.BlockingKeyGenerator;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.FullAttributeComponent;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.KeyGeneratorComponent;
import org.gradoop.famer.linking.blocking.methods.dataStructures.BlockingComponentBaseConfig;
import org.gradoop.famer.linking.blocking.methods.dataStructures.SortedNeighborhoodComponent;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Test class for {@link SortedNeighborhood}
 */
public class SortedNeighborhoodTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  private BlockingComponentBaseConfig baseConfig;

  private final int parallelismDegree = 4;

  @Before
  public void setUp() {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(bob:Person {id:2, name:\"Bob\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(carol:Person {id:3, name:\"Carol\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(dave:Person {id:4, name:\"Dave\", " + GRAPH_LABEL + ":\"g1\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    KeyGeneratorComponent keyGeneratorComponent = new FullAttributeComponent("name");
    BlockingKeyGenerator blockingKeyGenerator = new BlockingKeyGenerator(keyGeneratorComponent);
    Map<String, Set<String>> graphPairs = new HashMap<>();
    Set<String> values = new HashSet<>();
    values.add("*");
    graphPairs.put("*", values);
    Map<String, Set<String>> categoryPairs = new HashMap<>();
    categoryPairs.put("*", values);
    baseConfig = new BlockingComponentBaseConfig(blockingKeyGenerator, graphPairs, categoryPairs);
  }

  /**
   * For windowSize = 1, no pair should be created
   */
  @Test
  public void testGeneratePairedVerticesWithWindowSize1() throws Exception {
    SortedNeighborhoodComponent sortedNeighborhoodComponent =
      new SortedNeighborhoodComponent(baseConfig, 1, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new SortedNeighborhood(sortedNeighborhoodComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<EPGMVertex, EPGMVertex>> result = pairedVertices.collect();

    assertTrue(result.isEmpty());
  }

  /**
   * For windowSize = 2, 3 pairs should be created
   */
  @Test
  public void testGeneratePairedVerticesWithWindowSize2() throws Exception {
    SortedNeighborhoodComponent sortedNeighborhoodComponent =
      new SortedNeighborhoodComponent(baseConfig, 2, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new SortedNeighborhood(sortedNeighborhoodComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Bob"), Tuple2.of("Bob", "Carol"), Tuple2.of("Carol", "Dave"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices);

    assertFalse(result.isEmpty());
    assertEquals(3, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * For windowSize = 4, 6 pairs should be created
   */
  @Test
  public void testGeneratePairedVerticesWithWindowSize4() throws Exception {
    SortedNeighborhoodComponent sortedNeighborhoodComponent =
      new SortedNeighborhoodComponent(baseConfig, 4, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new SortedNeighborhood(sortedNeighborhoodComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Bob"), Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"),
      Tuple2.of("Carol", "Dave"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices);

    assertFalse(result.isEmpty());
    assertEquals(6, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * For windowSize > number of vertices, 6 pairs should be created
   */
  @Test
  public void testGeneratePairedVerticesWithWindowSizeMax() throws Exception {
    SortedNeighborhoodComponent sortedNeighborhoodComponent =
      new SortedNeighborhoodComponent(baseConfig, Integer.MAX_VALUE, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new SortedNeighborhood(sortedNeighborhoodComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Bob"), Tuple2.of("Alice", "Carol"), Tuple2.of("Alice", "Dave"),
      Tuple2.of("Bob", "Carol"), Tuple2.of("Bob", "Dave"),
      Tuple2.of("Carol", "Dave"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices);

    assertFalse(result.isEmpty());
    assertEquals(6, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * For windowSize = 2 and limited "allowed graph pairs", 2 pairs should be created
   */
  @Test
  public void testGeneratePairedVerticesWithWindowSize2AndLimitedGraphPairs() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(bob:Person {id:2, name:\"Bob\", " + GRAPH_LABEL + ":\"g2\"})" +
      "(carol:Person {id:3, name:\"Carol\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(dave:Person {id:4, name:\"Dave\", " + GRAPH_LABEL + ":\"g3\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    Map<String, Set<String>> limitedGraphPairs = new HashMap<>();
    // g1 is limited to g2
    Set<String> value1 = new HashSet<>();
    value1.add("g2");
    limitedGraphPairs.put("g1", value1);
    // g2 is limited to g1
    Set<String> value2 = new HashSet<>();
    value2.add("g1");
    limitedGraphPairs.put("g2", value2);

    baseConfig.setGraphPairs(limitedGraphPairs);

    SortedNeighborhoodComponent sortedNeighborhoodComponent =
      new SortedNeighborhoodComponent(baseConfig, 2, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new SortedNeighborhood(sortedNeighborhoodComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("Alice", "Bob"), Tuple2.of("Bob", "Carol"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices);

    assertFalse(result.isEmpty());
    assertEquals(2, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * For windowSize = 2 and limited "allowed category pairs", 1 pair should be created
   */
  @Test
  public void testGeneratePairedVerticesWithWindowSize2AndLimitedCategoryPairs() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(bob:Teacher {id:2, name:\"Bob\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(carol:Person {id:3, name:\"Carol\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(dave:Person {id:4, name:\"Dave\", " + GRAPH_LABEL + ":\"g1\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    Map<String, Set<String>> limitedCategoryPairs = new HashMap<>();
    // Person is limited to Teacher
    Set<String> value1 = new HashSet<>();
    value1.add("Teacher");
    limitedCategoryPairs.put("Person", value1);
    // Teacher is limited to Teacher
    Set<String> value2 = new HashSet<>();
    value2.add("Teacher");
    limitedCategoryPairs.put("Teacher", value2);

    baseConfig.setCategoryPairs(limitedCategoryPairs);

    SortedNeighborhoodComponent sortedNeighborhoodComponent =
      new SortedNeighborhoodComponent(baseConfig, 2, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new SortedNeighborhood(sortedNeighborhoodComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<String, String>> expectedResult = Collections.singletonList(Tuple2.of("Alice", "Bob"));

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(pairedVertices);

    assertFalse(result.isEmpty());
    assertEquals(1, result.size());
    assertEquals(expectedResult, result);
  }

  /**
   * Test if SortedNeighborhood works correct when the input graph has no vertices
   */
  @Test
  public void testGeneratePairedVerticesForEmptyGraph() throws Exception {
    inputGraph = getLoaderFromString("input[]").getLogicalGraphByVariable("input");

    SortedNeighborhoodComponent sortedNeighborhoodComponent =
      new SortedNeighborhoodComponent(baseConfig, 2, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new SortedNeighborhood(sortedNeighborhoodComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<EPGMVertex, EPGMVertex>> result = pairedVertices.collect();

    assertTrue(result.isEmpty());
  }

  /**
   * Test if SortedNeighborhood works correct when the input graph has only one vertex
   */
  @Test
  public void testGeneratePairedVerticesForOneVertex() throws Exception {
    inputGraph = getLoaderFromString("input[(alice:Person {id:1, name:\"Alice\"})]")
      .getLogicalGraphByVariable("input");

    SortedNeighborhoodComponent sortedNeighborhoodComponent =
      new SortedNeighborhoodComponent(baseConfig, 2, parallelismDegree);

    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices =
      new SortedNeighborhood(sortedNeighborhoodComponent).generatePairedVertices(inputGraph.getVertices());

    List<Tuple2<EPGMVertex, EPGMVertex>> result = pairedVertices.collect();

    assertTrue(result.isEmpty());
  }

  /**
   * Test if SortedNeighborhood throws exception when vertices miss the graphLabel property
   */
  @Test(expected = Exception.class)
  public void testGeneratePairedVerticesForMissingGraphLabelProperty() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\"})" +
      "(bob:Teacher {id:2, name:\"Bob\"})" +
      "(carol:Teacher {id:3, name:\"Carol\"})" +
      "(dave:Person {id:4, name:\"Dave\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    SortedNeighborhoodComponent sortedNeighborhoodComponent =
      new SortedNeighborhoodComponent(baseConfig, 2, parallelismDegree);

    new SortedNeighborhood(sortedNeighborhoodComponent).generatePairedVertices(inputGraph.getVertices())
      .collect();
  }

  /**
   * Collects the paired vertices to a list and transforms them to a tuple with the vertex names
   *
   * @param pairedVertices The paired vertices
   *
   * @return A list with the name tuples
   *
   * @throws Exception Thrown on errors while collecting from DataSet
   */
  private List<Tuple2<String, String>> collectAndTransformResultTuples(
    DataSet<Tuple2<EPGMVertex, EPGMVertex>> pairedVertices) throws Exception {

    return pairedVertices.collect().stream()
      .map(tuple -> Tuple2.of(
        tuple.f0.getPropertyValue("name").getString(),
        tuple.f1.getPropertyValue("name").getString()))
      .sorted(Comparator.comparing(tuple -> tuple.f0 + tuple.f1))
      .collect(Collectors.toList());
  }
}
