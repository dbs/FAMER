/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip;

import com.google.common.collect.Lists;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.clip.dataStructures.CLIPConfig;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;
import static org.gradoop.famer.clustering.common.PropertyNames.SIM_VALUE;
import static org.gradoop.famer.clustering.common.PropertyNames.VERTEX_PRIORITY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test class for the parallel {@link CLIP} algorithm. Test graph data and expected clustering result is
 * taken from:
 * https://dbs.uni-leipzig.de/en/publication/title/using_link_features_for_entity_clustering_in_knowledge_graphs
 */
public class CLIPTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  private CLIPConfig clipConfig;

  @Before
  public void setUpGraph() throws Exception {
    String graphString = "inputGraph[" +
      "/* cluster 1 */" +
      "(v0 {id:\"a0\", " + GRAPH_LABEL + ":\"A\"})" +
      "(v1 {id:\"b0\", " + GRAPH_LABEL + ":\"B\"})" +
      "(v2 {id:\"c0\", " + GRAPH_LABEL + ":\"C\"})" +
      "(v3 {id:\"d0\", " + GRAPH_LABEL + ":\"D\"})" +
      "(v0)-[e0 {" + SIM_VALUE + ":1.0}]->(v1)" +
      "(v0)-[e1 {" + SIM_VALUE + ":1.0}]->(v2)" +
      "(v0)-[e2 {" + SIM_VALUE + ":1.0}]->(v3)" +
      "(v1)-[e3 {" + SIM_VALUE + ":1.0}]->(v2)" +
      "(v2)-[e4 {" + SIM_VALUE + ":1.0}]->(v3)" +
      "/* cluster 2 */" +
      "(v4 {id:\"a1\", " + GRAPH_LABEL + ":\"A\"})" +
      "(v5 {id:\"b1\", " + GRAPH_LABEL + ":\"B\"})" +
      "(v6 {id:\"c1\", " + GRAPH_LABEL + ":\"C\"})" +
      "(v1)-[e5 {" + SIM_VALUE + ":0.85}]->(v6)" +
      "(v3)-[e6 {" + SIM_VALUE + ":0.9}]->(v6)" +
      "(v4)-[e7 {" + SIM_VALUE + ":0.8}]->(v5)" +
      "(v4)-[e8 {" + SIM_VALUE + ":0.8}]->(v6)" +
      "(v5)-[e9 {" + SIM_VALUE + ":0.9}]->(v6)" +
      "/* cluster 3 */" +
      "(v7 {id:\"a2\", " + GRAPH_LABEL + ":\"A\"})" +
      "(v8 {id:\"b2\", " + GRAPH_LABEL + ":\"B\"})" +
      "(v9 {id:\"c2\", " + GRAPH_LABEL + ":\"C\"})" +
      "(v10 {id:\"d2\", " + GRAPH_LABEL + ":\"D\"})" +
      "(v4)-[e10 {" + SIM_VALUE + ":0.85}]->(v8)" +
      "(v4)-[e11 {" + SIM_VALUE + ":0.8}]->(v10)" +
      "(v7)-[e12 {" + SIM_VALUE + ":1.0}]->(v8)" +
      "(v7)-[e13 {" + SIM_VALUE + ":0.98}]->(v9)" +
      "(v9)-[e14 {" + SIM_VALUE + ":0.96}]->(v10)" +
      "/* cluster 4 */" +
      "(v11 {id:\"a3\", " + GRAPH_LABEL + ":\"A\"})" +
      "(v12 {id:\"b3\", " + GRAPH_LABEL + ":\"B\"})" +
      "(v8)-[e15 {" + SIM_VALUE + ":0.95}]->(v11)" +
      "(v11)-[e17 {" + SIM_VALUE + ":1.0}]->(v12)" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("inputGraph");

    clipConfig = new CLIPConfig();
  }

  @Test
  public void testClustering() throws Exception {
    LogicalGraph clusteredGraph =
      new CLIP(clipConfig, ClusteringOutputType.GRAPH, Integer.MAX_VALUE).execute(inputGraph);

    // check vertices and extract cluster id
    DataSet<Tuple2<String, EPGMVertex>> vertices = clusteredGraph.getVertices().flatMap(
      (FlatMapFunction<EPGMVertex, Tuple2<String, EPGMVertex>>) (vertex, out) -> {
        assertNotNull("vertex has no clusterId property",
          vertex.getPropertyValue(CLUSTER_ID));
        assertNotNull("vertex has no vertex priority property",
          vertex.getPropertyValue(VERTEX_PRIORITY));
        out.collect(Tuple2.of(vertex.getPropertyValue(CLUSTER_ID).getString(), vertex));
      })
      .returns(new TypeHint<Tuple2<String, EPGMVertex>>() { });

    // collect cluster lists
    List<List<EPGMVertex>> clusterLists = vertices.groupBy(0)
      .reduceGroup((GroupReduceFunction<Tuple2<String, EPGMVertex>, List<EPGMVertex>>) (group, out) ->
        out.collect(Lists.newArrayList(group).stream().map(tuple -> tuple.f1).collect(Collectors.toList())))
      .returns(new TypeHint<List<EPGMVertex>>() { })
      .collect();

    // check clusters
    assertEquals("expected 4 clusters, but found " + clusterLists.size(),
      4, clusterLists.size());
  }
}
