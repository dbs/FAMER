/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils.ApTestUtils;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test class for {@link SparseMscdAffinityPropagationSeqTest}
 */
public class SparseMscdAffinityPropagationSeqTest extends AbstractMscdAffinityPropagationSeqTest {

  /**
   * Get a new instance of {@link SparseMscdAffinityPropagationSeq}.
   *
   * @param similarityMatrix similarity matrix
   * @param preference self-similarity parameter
   * @return new instance of {@link SparseMscdAffinityPropagationSeq}.
   */
  @Override
  protected AbstractMscdAffinityPropagationSeq getNewApInstance(double[][] similarityMatrix, double preference) {
    return new SparseMscdAffinityPropagationSeq(similarityMatrix, preference, nul);
  }

  /**
   * Test the calculation of Eta.
   * Eta[i][j] = -1 * max_{k != j}(S[i][k] + A[i][k] + Theta[i][k])
   */
  @Test
  public void apPart1OneOfNConstraintTestUsingPlayData() throws Exception {
    // ### input
    double[][] s = {
      {3, 2, 1, nul, nul},
      {5, 1, 4, 2, 3}
    };

    double[][] a = {
      {1, 1, 1, nul, nul},
      {1, 1, 1, 1, 1}
    };

    double[][] theta = {
      {2, 2, 2, nul, nul},
      {2, 2, 2, 2, 2}
    };

    // ### intermediate result

    // Beta = S + A + Theta:
    // 6, 5, 4, nul, nul
    // 8, 4, 7, 5, 6

    // ### expected output

    double[][] expectedEtaForSparse = {
      {-5, -6, -6, nul, nul},
      {-7, -8, -8, -8, -8}
    };

    Table<Integer, Integer, Double> aTable = ApTestUtils.sparseMatrixToTable(a, nul);
    Table<Integer, Integer, Double> sTable = ApTestUtils.sparseMatrixToTable(s, nul);
    Table<Integer, Integer, Double> thetaTable = ApTestUtils.sparseMatrixToTable(theta, nul);
    Table<Integer, Integer, Double> etaTable = HashBasedTable.create();

    SparseMscdAffinityPropagationSeq.affinityPiece1getEta(aTable, sTable, thetaTable, etaTable);

    final int nrRows = s.length;
    final int nrCols = s[0].length;

    for (int i = 0; i < nrRows; i++) {
      for (int j = 0; j < nrCols; j++) {
        if (expectedEtaForSparse[i][j] == nul) {
          assertFalse(etaTable.contains(i, j));
        } else {
          assertEquals(expectedEtaForSparse[i][j], etaTable.get(i, j), 0.00001);
        }
      }
    }
  }

  /**
   * Test the calculation of Theta.
   * Gamma_kj = S_ij + A_ij + Eta_ij
   * Theta_ij = min[0, -1 * max_{k != i}(Gamma_kj)]
   * => -1 * maximal column value of all other cells of that column (of the same clean source!), only negative
   * values taken
   */
  @Test
  public void apPart2CleanSourcesConstraint() throws Exception {
    // ### input
    double[][] s = {
      {3, 5, 10},
      {2, -5, 2},
      {6, 8, 10},
      {nul, -3, 3},
      {nul, 7, 4},
      {3, 2, 1}
    };

    double[][] eta = {
      {1, 1, 1},
      {1, 1, 1},
      {1, 1, 1},
      {nul, 1, 1},
      {nul, 1, 1},
      {1, 1, 1}
    };

    double[][] a = {
      {-4, -4, -4},
      {-4, -4, -4},
      {-4, -4, -4},
      {nul, -4, -4},
      {nul, -4, -4},
      {-4, -4, -4}
    };

    String[] rowCleanSrc = {"b", "a", "b", "", "a", "b"};

    // ### intermediate result

    // Gamma = S + Eta + A:
    // 0       2       7      b
    // -1     -8      -1          a
    // 3       5       7      b
    // nul    -6       0              -
    // nul    4        1          a
    // 0     -1       -2      b

    // ### expected output

    double[][] expectedThetaSparse = {
      {-3, -5, -7},
      {nul, -4, -1},
      {0, -2, -7},
      {nul, nul, nul},
      {nul, 0, 0},
      {-3, -5, -7}
    };

    Table<Integer, Integer, Double> aTable = ApTestUtils.sparseMatrixToTable(a, nul);
    Table<Integer, Integer, Double> sTable = ApTestUtils.sparseMatrixToTable(s, nul);
    Table<Integer, Integer, Double> etaTable = ApTestUtils.sparseMatrixToTable(eta, nul);
    Table<Integer, Integer, Double> thetaTable = HashBasedTable.create();
    Multimap<String, Integer> cleanSourceElementIndices = ApTestUtils.cleanSourcesArrayToMultiMap(rowCleanSrc);

    SparseMscdAffinityPropagationSeq.affinityPiece2getTheta(aTable, sTable, thetaTable, etaTable, cleanSourceElementIndices);

    final int nrRows = s.length;
    final int nrCols = s[0].length;

    for (int i = 0; i < nrRows; i++) {
      for (int j = 0; j < nrCols; j++) {
        if (expectedThetaSparse[i][j] == nul) {
          assertFalse(thetaTable.contains(i, j));
        } else {
          assertEquals(expectedThetaSparse[i][j], thetaTable.get(i, j), 0.00001);
        }
      }
    }
  }

  /**
   * Test the calculation of A and R.
   * R_ij = S_ij + Eta_ij + Theta_ij
   *
   * <p>
   * A_ij = Sum_{k != j}( max(0, R_kj )                           for diagonal (i == j)
   * <ul>
   *   <li>=> Column-Sum of positive elements without self</li>
   * </ul>
   *
   * <p>
   * A_ij = min[0,  R_jj + Sum_{k != i,j}(max(0, R_kj))]          for non-diagonal (i != j)
   * <ul>
   *   <li>=> Column-Sum of positive elements without self and diagonal  + diagonal(also negative)</li>
   *   <li>=> take only values <= 0</li>
   * </ul>
   *
   * <p>
   * The tested configuration tests all the special cases of A and R calculation:
   * <ul>
   *   <li>damping</li>
   *   <li>A results > 0</li>
   *   <li>A results < 0</li>
   *   <li>positive diagonal value</li>
   *   <li>negative diagonal value</li>
   * </ul>
   */
  @Test
  public void apPart3ExemplarConsistencyConstraint() {
    // ### input
    double[][] s = {
      {3, 5, 1},
      {2, -5, 2},
      {-6, -4, -30},
      {nul, -3, 3},
      {nul, -7, 4}
    };

    double[][] eta = {
      {1, 1, 1},
      {1, 1, 1},
      {1, 1, 1},
      {nul, 1, 1},
      {nul, 1, 1}
    };

    double[][] theta = {
      {2, 2, 2},
      {2, 2, 2},
      {2, 2, 2},
      {nul, 2, 2},
      {nul, 2, 2}
    };

    double[][] r = {
      {3, 3, 3},
      {3, -3, 3},
      {-3, -3, -3},
      {nul, 0, 3},
      {nul, -3, -3}
    };

    double[][] a = {
      {-4, -4, -4},
      {-4, -4, -4},
      {-4, -4, -4},
      {nul, -4, -4},
      {nul, -4, -4}
    };

    // ### intermediate result

    // R = S + Eta + Theta:          damped
    // 6, 8,  4                         1.2,   1.6      0.8
    // 5, -2,  5                          1     -0.4      1
    // -3, -1, -27                        -0.6    -0.2     -5.4
    // nul, 0,  6                        nul    0         1.2
    // nul, -4,  7                        nul    -0.8     1.4

    // ### expected output

    // damping R = oldR * dampingFactor + newR * (1-dampingFactor)
    double[][] expectedR = {
      {3.6, 4, 3.2},
      {3.4, -2.8, 3.4},
      {-3, -2.6, -7.8},
      {nul, 0, 3.6},
      {nul, -3.2, -1}
    };

    // ### intermediate result

    // A                        damped
    // 3.4    -2.8    -0.8         0.68    -0.56     -0.16
    // 0     4       -1              0     0.8      -0.2
    // 0     0     10,2             0      0     2.04
    // nul    0     -1,2           nul     0     -0.24
    // nul    0      0           nul     0       0

    // damping A = oldA * dampingFactor + newA * (1-dampingFactor)
    // -4 * 0.8 = -3.2

    // ### expected output

    double[][] expectedA = {
      {-2.52, -3.76, -3.36},
      {-3.2, -2.4, -3.4},
      {-3.2, -3.2, -1.16},
      {nul, -3.2, -3.44},
      {nul, -3.2, -3.2}
    };

    Table<Integer, Integer, Double> aTable = ApTestUtils.sparseMatrixToTable(a, nul);
    Table<Integer, Integer, Double> sTable = ApTestUtils.sparseMatrixToTable(s, nul);
    Table<Integer, Integer, Double> etaTable = ApTestUtils.sparseMatrixToTable(eta, nul);
    Table<Integer, Integer, Double> thetaTable = ApTestUtils.sparseMatrixToTable(theta, nul);
    Table<Integer, Integer, Double> rTable = ApTestUtils.sparseMatrixToTable(r, nul);

    SparseMscdAffinityPropagationSeq.affinityPiece3getA(aTable, sTable, thetaTable, etaTable, rTable, 0.8);

    final int nrRows = s.length;
    final int nrCols = s[0].length;

    for (int i = 0; i < nrRows; i++) {
      for (int j = 0; j < nrCols; j++) {
        if (expectedR[i][j] == nul) {
          assertFalse(rTable.contains(i, j));
        } else {
          assertEquals(expectedR[i][j], rTable.get(i, j), 0.00001);
        }
        if (expectedA[i][j] == nul) {
          assertFalse(rTable.contains(i, j));
        } else {
          assertEquals(expectedA[i][j], aTable.get(i, j), 0.00001);
        }
      }
    }
  }
}
