/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering;

import org.apache.commons.math3.util.Pair;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Test Class for {@link SerialHierarchicalClustering} algorithm
 */
public class SerialHierarchicalClusteringTest {
  private final String testGraphPath =
    new File(SerialStarTest.class.getResource("/serialClustering/testGraph/").getPath()).getAbsolutePath();

  private final String verticesPath = testGraphPath + File.separator + "vertices.csv";

  private final String edgesPath = testGraphPath + File.separator + "edges.csv";

  private final String outputPath = Files.createTempFile("testResult", " .csv").toString();

  public SerialHierarchicalClusteringTest() throws IOException {
  }

  @Test
  public void testClusteringForHierarchicalSingleLinkage() throws Exception {
    new SerialHierarchicalClustering(verticesPath, edgesPath, outputPath,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 0.49, false).execute();
    createClusterListsAndRunCommonTests(5);
  }

  @Test
  public void testClusteringForHierarchicalSingleLinkageUpperBound() throws Exception {
    new SerialHierarchicalClustering(verticesPath, edgesPath, outputPath,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 1.0, false).execute();
    createClusterListsAndRunCommonTests(11);
  }

  @Test
  public void testClusteringForHierarchicalSingleLinkageLowerBound() throws Exception {
    new SerialHierarchicalClustering(verticesPath, edgesPath, outputPath,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 0.0, false).execute();
    createClusterListsAndRunCommonTests(4);
  }

  @Test
  public void testClusteringForHierarchicalCompleteLinkage() throws Exception {
    new SerialHierarchicalClustering(verticesPath, edgesPath, outputPath,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 0.49, false).execute();
    createClusterListsAndRunCommonTests(5);
  }

  @Test
  public void testClusteringForHierarchicalCompleteLinkageUpperBound() throws Exception {
    new SerialHierarchicalClustering(verticesPath, edgesPath, outputPath,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 1.0, false).execute();
    createClusterListsAndRunCommonTests(11);
  }

  @Test
  public void testClusteringForHierarchicalSingleCompleteLowerBound() throws Exception {
    new SerialHierarchicalClustering(verticesPath, edgesPath, outputPath,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 0.0, false).execute();
    createClusterListsAndRunCommonTests(5);
  }

  @Test
  public void testClusteringForHierarchicalAverageLinkage() throws Exception {
    new SerialHierarchicalClustering(verticesPath, edgesPath, outputPath,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 0.49, false).execute();
    createClusterListsAndRunCommonTests(5);
  }

  @Test
  public void testClusteringForHierarchicalAverageLinkageUpperBound() throws Exception {
    new SerialHierarchicalClustering(verticesPath, edgesPath, outputPath,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 1.0, false).execute();
    createClusterListsAndRunCommonTests(11);
  }

  @Test
  public void testClusteringForHierarchicalAverageLinkageLowerBound() throws Exception {
    new SerialHierarchicalClustering(verticesPath, edgesPath, outputPath,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 0.0, false).execute();
    createClusterListsAndRunCommonTests(4);
  }

  /**
   * Creates a map entry for each cluster, holding each of the cluster vertices as
   * {@code Pair<VertexId, ClusterId>}. Checks if the expected number of clusters was created.
   *
   * @throws IOException Thrown on errors while reading the clustering result file.
   */
  private void createClusterListsAndRunCommonTests(int clusterSize) throws IOException {
    List<Pair<String, String>> results = new ArrayList<>();

    Files.lines(Paths.get(outputPath), StandardCharsets.UTF_8).forEach(entry -> {
      String[] lineData = entry.split(",", 2);
      String[] clusters = lineData[1].split(",");
      for (String cluster : clusters) {
        results.add(Pair.create(lineData[0], cluster));
      }
    });

    Map<String, List<Pair<String, String>>> clusters =
      results.stream().collect(Collectors.groupingBy(Pair::getSecond));

    assertEquals("expected 5 clusters, but found " + clusters.size(), clusterSize, clusters.size());
  }

}
