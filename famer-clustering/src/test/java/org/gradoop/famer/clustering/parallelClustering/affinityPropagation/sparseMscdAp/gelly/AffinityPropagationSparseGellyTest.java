/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.flink.graph.Graph;
import org.apache.flink.graph.Vertex;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.AbstractAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.AffinityPropagationTestBase;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils.ConfigParsingTestUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.ApVertexValueTuple;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils.ApTestUtils;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test class for the parallel {@link AffinityPropagationSparseGelly} algorithm.
 */
public class AffinityPropagationSparseGellyTest extends AffinityPropagationTestBase {

  /**
   * Create a new instance of AffinityPropagationSparseGelly
   *
   * @return new instance of AffinityPropagationSparseGelly
   */
  @Override
  protected AbstractAffinityPropagation getNewApInstance() {
    return new AffinityPropagationSparseGelly();
  }

  /**
   * Cast {@link #ap} to {@link AffinityPropagationSparseGelly}
   *
   * @return {@link #ap} as {@link AffinityPropagationSparseGelly}
   */
  private AffinityPropagationSparseGelly getApSparseGelly() {
    return (AffinityPropagationSparseGelly) ap;
  }

  /**
   * Test, whether a json config file for the {@link ApConfig} is correctly parsed.
   *
   * @throws IOException if the json config file could nor be read correctly from disk.
   * @throws JSONException if the json config file could not be parsed correctly for a certain parameter.
   */
  @Test
  public void testConfigParsing() throws IOException, JSONException {
    String jsonConfigFilePath = new File(AffinityPropagationSparseGellyTest.class.getResource(
      "/parallelClustering/affinityPropagation/common/testApConfig.json").getFile()).getAbsolutePath();

    String jsonString = StringUtils.newStringUtf8(Files.readAllBytes(Paths.get(jsonConfigFilePath)));
    JSONObject jsonConfig = new JSONObject(jsonString).getJSONObject("clustering");

    AffinityPropagationSparseGelly ap = new AffinityPropagationSparseGelly(jsonConfig);
    ConfigParsingTestUtils.testApConfigParsing(ap);
  }

  /**
   * Test whether the preference parameter is correctly set by gelly preprocessing.
   */
  @Test
  public void testSettingThePreferenceParameter() throws Exception {
    Graph<GradoopId, ApVertexValueTuple, Double> outputGraph;
    List<Vertex<GradoopId, ApVertexValueTuple>> resultVertices;
    LogicalGraph inputGraph;
    double[][] s;
    String[] sources;
    List<String> cleanSources;
    double expectedPreferenceDirtySource;
    double expectedPreferenceCleanSource;
    double expectedDamping;

    apConfig.setMaxApIteration(2000);
    apConfig.setDampingFactor(0.5);
    expectedDamping = 0.5;

    s = new double[][] {
      {nul, 0.3,  0.5,  0.6},
      {0.3, nul,  0.8,  0.4},
      {0.5, 0.8,  nul,  0.7},
      {0.6, 0.4,  0.7,  nul} };

    sources = new String[] { "", "", "", "", "" };
    cleanSources = new ArrayList<>();

    /* preference percentile:
    0.3   0.4   0.5   0.6   0.7   0.8
    25 = 0.375
    50 = 0.55
    75 = 0.725
     */

    // 1) min & percentile
    preferenceConfig.setPreferenceUseMinSimilarityDirtySrc(true);
    preferenceConfig.setPreferencePercentileCleanSrc(50);

    expectedPreferenceDirtySource = 0.3;
    expectedPreferenceCleanSource = 0.55;

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    outputGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);

    resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkGellyParameterAdaption(resultVertices, expectedPreferenceDirtySource,
      expectedPreferenceCleanSource, expectedDamping);

    // 2) percentile & min
    preferenceConfig.setPreferencePercentileDirtySrc(25);
    preferenceConfig.setPreferenceUseMinSimilarityCleanSrc(true);

    expectedPreferenceDirtySource = 0.375;
    expectedPreferenceCleanSource = 0.3;

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    outputGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);

    resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkGellyParameterAdaption(resultVertices, expectedPreferenceDirtySource,
      expectedPreferenceCleanSource, expectedDamping);

    // 3) percentile & fixval
    preferenceConfig.setPreferencePercentileDirtySrc(75);
    preferenceConfig.setPreferenceFixValueCleanSrc(0.123);

    expectedPreferenceDirtySource = 0.725;
    expectedPreferenceCleanSource = 0.123;

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    outputGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);

    resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkGellyParameterAdaption(resultVertices, expectedPreferenceDirtySource,
      expectedPreferenceCleanSource, expectedDamping);

    // 4) fixval & percentile
    preferenceConfig.setPreferenceFixValueDirtySrc(0.345);
    preferenceConfig.setPreferencePercentileCleanSrc(75);

    expectedPreferenceDirtySource = 0.345;
    expectedPreferenceCleanSource = 0.725;

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    outputGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);

    resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkGellyParameterAdaption(resultVertices, expectedPreferenceDirtySource,
      expectedPreferenceCleanSource, expectedDamping);
  }

  /**
   * Test whether the parameters for preference and damping are correctly adapted during the gelly MSCD-AP
   * process.
   */
  @Test
  public void testParameterAdaption() throws Exception {
    Graph<GradoopId, ApVertexValueTuple, Double> outputGraph;
    List<Vertex<GradoopId, ApVertexValueTuple>> resultVertices;
    LogicalGraph inputGraph;
    double[][] s;
    String[] sources;
    List<String> cleanSources;
    double expectedPreferenceDirtySource;
    double expectedPreferenceCleanSource;
    double expectedDamping;

    s = new double[][] {
      {nul, 0.2,  nul},
      {0.2, nul,  0.8},
      {nul, 0.8,  nul}};

    sources = new String[] { "", "", "" };
    cleanSources = new ArrayList<>();

    apConfig.setDampingFactor(0.5);
    preferenceConfig.setPreferenceFixValueDirtySrc(0.4);
    preferenceConfig.setPreferenceFixValueCleanSrc(0.5);
    preferenceConfig.setPreferenceAdaptionStep(0.05);
    apConfig.setMaxAdaptionIteration(3);
    apConfig.setConvergenceIter(3);

    // 1) reduce preference values 5 times

    apConfig.setMaxApIteration(15);  // 5 adaptions (3 * 5)
    expectedPreferenceDirtySource = 0.15;
    expectedPreferenceCleanSource = 0.25;
    expectedDamping = 0.5;

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);

    // use the 3 steps instead of runClustering to check the processing information of AP, which is
    // discarded when the gelly graph is converted to a logical graph
    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    outputGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    outputGraph = getApSparseGelly().executeAffinityPropagation(outputGraph, env);

    resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkGellyParameterAdaption(resultVertices, expectedPreferenceDirtySource,
      expectedPreferenceCleanSource, expectedDamping);

    // 2) adapt 10 times => switch adaption direction from decrease to increase, keeping the distance
    // between prefCleanSrc and prefDirty Src

    apConfig.setMaxApIteration(30);  // 10 adaptions (3 * 10)
    expectedPreferenceDirtySource = 0.5;
    expectedPreferenceCleanSource = 0.6;
    expectedDamping = 0.50;

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    outputGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    outputGraph = getApSparseGelly().executeAffinityPropagation(outputGraph, env);

    resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkGellyParameterAdaption(resultVertices, expectedPreferenceDirtySource,
      expectedPreferenceCleanSource, expectedDamping);

    // 3) adapt 20 times => raise damping

    apConfig.setMaxApIteration(60);  // 20 adaptions (3 * 20)
    expectedPreferenceDirtySource = 0.35;
    expectedPreferenceCleanSource = 0.45;
    expectedDamping = 0.60;

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    outputGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    outputGraph = getApSparseGelly().executeAffinityPropagation(outputGraph, env);

    resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkGellyParameterAdaption(resultVertices, expectedPreferenceDirtySource,
      expectedPreferenceCleanSource, expectedDamping);
  }

  /**
   * Test if the preprocessing creates correctly the expected singletons.
   */
  @Test
  public void testPreprocessingSingletons() throws Exception {
    List<Vertex<GradoopId, ApVertexValueTuple>> resultVertices;
    Graph<GradoopId, ApVertexValueTuple, Double> gellyGraph;
    LogicalGraph inputGraph;
    double[][] s;
    String[] sources;
    List<String> cleanSources;
    List<Integer> expectedSingletons;

    s = new double[][] {
      {nul,       nul,    nul,    nul,      nul},
      {nul,     nul,      0.8,    nul,    0.9},
      {nul,     0.8,    nul,      nul,    0.6},
      {nul,       nul,    nul,    nul,      nul},
      {nul,     0.9,    0.6,    nul,    nul}};

    sources = new String[] { "", "", "", "", "" };
    cleanSources = new ArrayList<>();
    expectedSingletons = Arrays.asList(0, 3);
    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setDampingFactor(0.8);
    preferenceConfig.setPreferenceFixValueDirtySrc(0.45);
    preferenceConfig.setPreferenceFixValueCleanSrc(0.6);
    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    for (Vertex<GradoopId, ApVertexValueTuple> vertex : resultVertices) {
      if (expectedSingletons.contains(ApTestUtils.gradoopIdToInteger(vertex.getId()))) {
        assertTrue(vertex.getValue().isComponentConverged());
        assertTrue(vertex.getValue().isExemplar());
        assertEquals(vertex.getValue().getVertexId(), vertex.getValue().getClusterId());
      } else {
        assertFalse(vertex.getValue().isComponentConverged());
        assertFalse(vertex.getValue().isExemplar());
      }
    }
  }

  /**
   * Test if the preprocessing correctly detects the components, where all similarities are equal.
   * Therefore test different similarity graph structures to see, that information is in gelly correctly
   * propagated through connected components.
   */
  @Test
  public void testPreprocessingAllEqualComponents() throws Exception {
    List<Vertex<GradoopId, ApVertexValueTuple>> resultVertices;
    LogicalGraph inputGraph;
    double[][] s;
    String[] sources;
    List<String> cleanSources;
    List<Integer> expectedSingletons;
    int[][] expectedClusters;
    Graph<GradoopId, ApVertexValueTuple, Double> gellyGraph;

    apConfig.setAllSameSimClusteringThreshold(0.7);

    // 1) Test components all equal

    s = new double[][] {
      {nul,       0.5,    nul,    nul,      nul,     nul,    nul},
      {0.5,       nul,    nul,    nul,      nul,     nul,    nul},
      {nul,       nul,    nul,    0.7,      nul,     nul,    nul},
      {nul,       nul,    0.7,    nul,      nul,     nul,    nul},
      {nul,       nul,    nul,    nul,      nul,     0.9,    0.9},
      {nul,       nul,    nul,    nul,      0.9,     nul,    0.9},
      {nul,       nul,    nul,    nul,      0.9,     0.9,    nul}};

    // 0,1  < threshold
    // 2,3  > threshold
    // 3,4,5 > threshold

    sources = new String[] { "", "", "", "", "", "", "" };
    cleanSources = new ArrayList<>();
    expectedSingletons = Arrays.asList(0, 1);
    expectedClusters = new int[][] {{2, 3}, {4, 5, 6}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);

    // 2) Test component not all equal

    s = new double[][] {
      {nul,       0.5,    nul,    nul,      nul,     nul,    nul},
      {0.5,       nul,    nul,    nul,      nul,     nul,    nul},
      {nul,       nul,    nul,    0.7,      nul,     nul,    nul},
      {nul,       nul,    0.7,    nul,      nul,     nul,    nul},
      {nul,       nul,    nul,    nul,      nul,     0.9,    0.8},
      {nul,       nul,    nul,    nul,      0.8,     nul,    0.9},
      {nul,       nul,    nul,    nul,      0.9,     0.9,    nul}};

    sources = new String[] { "", "", "", "", "", "", "" };
    cleanSources = new ArrayList<>();
    expectedSingletons = Arrays.asList(0, 1);
    expectedClusters = new int[][] {{2, 3}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);

    // 3.1) Test huge sparse component (long path) all equal

    // 0-1-2-3-4-5-6-(7,8)
    s = new double[][] {
      {nul,   0.8,    nul,    nul,      nul,     nul,    nul,   nul,    nul},
      {0.8,   nul,    0.8,    nul,      nul,     nul,    nul,   nul,    nul},
      {nul,   0.8,    nul,    0.8,      nul,     nul,    nul,   nul,    nul},
      {nul,   nul,    0.8,    nul,      0.8,     nul,    nul,   nul,    nul},
      {nul,   nul,    nul,    0.8,      nul,     0.8,    nul,   nul,    nul},
      {nul,   nul,    nul,    nul,      0.8,     nul,    0.8,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     0.8,    nul,   0.8,    0.8},
      {nul,   nul,    nul,    nul,      nul,     nul,    0.8,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     nul,    0.8,   nul,    nul}};

    sources = new String[] { "", "", "", "", "", "", "", "", "" };
    cleanSources = new ArrayList<>();
    expectedSingletons = new ArrayList<>();
    expectedClusters = new int[][] {{0, 1, 2, 3, 4, 5, 6, 7, 8}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);

    // 3.2) with clean sources

    sources = new String[] { "a", "b", "c", "a", "b", "", "c", "a", "b" };
    cleanSources = Arrays.asList("a", "b", "c");
    expectedSingletons = Arrays.asList(0, 1, 2, 3, 4);
    expectedClusters = new int[][] {{5, 6, 7, 8}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);

    // 3.3) with clean sources (with edge removing)
    // edges between same clean source vertices are removed => edge 2-3 is removed

    sources = new String[] { "c", "a", "b", "b", "a", "", "a", "c", "c" };
    cleanSources = Arrays.asList("a", "b", "c");
    expectedSingletons = Arrays.asList(4, 7);
    expectedClusters = new int[][] {{0, 1, 2}, {3, 5, 6, 8}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);

    // 4) Test huge sparse component (long path) not all equal - info at the end

    s = new double[][] {
      {nul,   0.8,    nul,    nul,      nul,     nul,    nul,   nul,    nul},
      {0.8,   nul,    0.8,    nul,      nul,     nul,    nul,   nul,    nul},
      {nul,   0.8,    nul,    0.8,      nul,     nul,    nul,   nul,    nul},
      {nul,   nul,    0.8,    nul,      0.8,     nul,    nul,   nul,    nul},
      {nul,   nul,    nul,    0.8,      nul,     0.8,    nul,   nul,    nul},
      {nul,   nul,    nul,    nul,      0.8,     nul,    0.8,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     0.8,    nul,   0.8,    0.7},
      {nul,   nul,    nul,    nul,      nul,     nul,    0.8,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     nul,    0.7,   nul,    nul}};

    sources = new String[] { "", "", "", "", "", "", "", "", "" };
    cleanSources = new ArrayList<>();
    expectedSingletons = new ArrayList<>();
    expectedClusters = new int[][] {};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);

    // 5) Test huge sparse component (long path) not all equal - info in the middle
    // 4-5-6-7-8-0-1-2-3

    s = new double[][] {
      {nul,   0.8,    nul,    nul,      nul,     nul,    nul,   nul,    0.8},
      {0.8,   nul,    0.8,    nul,      nul,     nul,    nul,   nul,    nul},
      {nul,   0.8,    nul,    0.8,      nul,     nul,    nul,   nul,    nul},
      {nul,   nul,    0.8,    nul,      nul,     nul,    nul,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     0.8,    nul,   nul,    nul},
      {nul,   nul,    nul,    nul,      0.8,     nul,    0.8,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     0.8,    nul,   0.8,    nul},
      {nul,   nul,    nul,    nul,      nul,     nul,    0.8,   nul,    0.8},
      {0.8,   nul,    nul,    nul,      nul,     nul,    nul,   0.8,    nul}};

    sources = new String[] { "a", "b", "c", "a", "b", "c", "a", "b", "c" };
    cleanSources = Arrays.asList("a", "b", "c");
    expectedSingletons = Arrays.asList(0, 1, 2, 3, 4, 5);
    expectedClusters = new int[][] {{6, 7, 8}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);

    // 6) 2 components

    s = new double[][] {
      {nul,   0.8,    nul,    nul,      nul,     nul,    nul,   nul,    nul},
      {0.8,   nul,    0.8,    nul,      nul,     nul,    nul,   nul,    nul},
      {nul,   0.8,    nul,    0.8,      nul,     nul,    nul,   nul,    nul},
      {nul,   nul,    0.8,    nul,      0.8,     nul,    nul,   nul,    nul},
      {nul,   nul,    nul,    0.8,      nul,     nul,    nul,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     nul,    0.6,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     0.6,    nul,   0.6,    0.6},
      {nul,   nul,    nul,    nul,      nul,     nul,    0.6,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     nul,    0.6,   nul,    nul}};

    sources = new String[] { "", "", "", "", "", "", "", "", "" };
    cleanSources = new ArrayList<>();
    expectedSingletons = Arrays.asList(5, 6, 7, 8);
    expectedClusters = new int[][] {{0, 1, 2, 3, 4}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);

    // 7) everything combined

    s = new double[][] {
      {nul,   0.8,    nul,    nul,      nul,     nul,    nul,   nul,    nul},
      {0.8,   nul,    0.8,    nul,      nul,     nul,    nul,   nul,    nul},
      {nul,   0.8,    nul,    0.8,      nul,     nul,    nul,   nul,    nul},
      {nul,   nul,    0.8,    nul,      0.8,     nul,    nul,   nul,    nul},
      {nul,   nul,    nul,    0.8,      nul,     nul,    nul,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     nul,    0.7,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     0.7,    nul,   0.9,    nul},
      {nul,   nul,    nul,    nul,      nul,     nul,    0.9,   nul,    nul},
      {nul,   nul,    nul,    nul,      nul,     nul,    nul,   nul,    nul}};

    sources = new String[] { "", "", "", "", "", "", "", "", "" };
    cleanSources = new ArrayList<>();
    expectedSingletons = Collections.singletonList(8);
    expectedClusters = new int[][] {{0, 1, 2, 3, 4}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);
  }

  @Override
  @Test
  public void testAllEqualComponents() throws Exception {
    /* tested:
     - full cluster > threshold:  0,1,2  => similarities of each to each > threshold
     - sparse cluster > threshold: 3,4,5  => similarity 4-5 is null
     - full cluster < threshold:  6,7,8  => similarities of each to each < threshold
     - sparse cluster < threshold: 9,10,11  => similarity 9-11 is null
     */

    List<Vertex<GradoopId, ApVertexValueTuple>> resultVertices;
    LogicalGraph inputGraph;
    double[][] s;
    String[] sources;
    List<String> cleanSources;
    List<Integer> expectedSingletons;
    int[][] expectedClusters;
    Graph<GradoopId, ApVertexValueTuple, Double> gellyGraph;

    apConfig.setAllSameSimClusteringThreshold(0.7);

    s = new double[][] {
      {nul, 0.7, 0.7, nul, nul, nul, nul, nul, nul, nul, nul, nul},
      {0.7, nul, 0.7, nul, nul, nul, nul, nul, nul, nul, nul, nul},
      {0.7, 0.7, nul, nul, nul, nul, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, nul, 0.8, 0.8, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, 0.8, nul, nul, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, 0.8, nul, nul, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, nul, 0.5, 0.5, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, 0.5, nul, 0.5, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, 0.5, 0.5, nul, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, nul, nul, nul, nul, 0.4, nul},
      {nul, nul, nul, nul, nul, nul, nul, nul, nul, 0.4, nul, 0.4},
      {nul, nul, nul, nul, nul, nul, nul, nul, nul, nul, 0.4, nul}
    };

    // 1. test without clean sources
    sources = new String[] { "", "", "", "", "", "", "", "", "", "", "", "" };
    cleanSources = new ArrayList<>();
    expectedClusters = new int[][] {{0, 1, 2}, {3, 4, 5}};
    expectedSingletons = Arrays.asList(6, 7, 8, 9, 10, 11);

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);

    // 2. test with clean sources (edge 3-4 is deleted, because they are from the same source)
    sources = new String[] { "a", "", "a", "b", "b", "", "", "", "", "", "", "" };
    cleanSources = Arrays.asList("a", "b");
    expectedClusters = new int[][] {{1, 2}, {3, 5}};
    expectedSingletons = Arrays.asList(0, 4, 6, 7, 8, 9, 10, 11);

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);

    // 3. Test mixed with singletons and not allEqualComponents
    s = new double[][] {
      {nul, 0.7, 0.7, nul, nul, nul, nul, nul},
      {0.7, nul, 0.7, nul, nul, nul, nul, nul},
      {0.7, 0.7, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, nul, 0.9, 0.8, nul, nul},
      {nul, nul, nul, 0.9, nul, nul, nul, nul},
      {nul, nul, nul, 0.8, nul, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, nul, nul}
    };

    sources = new String[] { "b", "", "b", "", "", "", "a", "" };
    cleanSources = Arrays.asList("a", "b");
    expectedClusters = new int[][] {{1, 2}};
    expectedSingletons = Arrays.asList(0, 6, 7);

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    inputGraph = getApSparseGelly().preprocessInputGraph(inputGraph);
    gellyGraph = getApSparseGelly().prepareGellyGraphForAP(inputGraph);
    resultVertices = gellyGraph.getVertices().collect();

    ApTestUtils.compareGellyPreprocessingResult(resultVertices, expectedSingletons, expectedClusters);
  }
}
