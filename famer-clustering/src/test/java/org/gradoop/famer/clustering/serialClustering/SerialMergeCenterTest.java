/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering;

import org.apache.commons.math3.util.Pair;
import org.gradoop.famer.clustering.serialClustering.dataStructures.PrioritySelection;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * Test class for the {@link SerialMergeCenter} algorithm.
 */
public class SerialMergeCenterTest {

  private final String testGraphPath =
    new File(SerialMergeCenterTest.class.getResource("/serialClustering/testGraph/").getPath())
      .getAbsolutePath();
  private final String verticesPath = testGraphPath + File.separator + "vertices.csv";
  private final String edgesPath = testGraphPath + File.separator + "edges.csv";
  private final String outputPath = Files.createTempFile("testResult", ".csv").toString();

  public SerialMergeCenterTest() throws IOException {
  }

  @Test
  public void testClusteringForPrioritySelectionMaxWithoutClusterMerging() throws Exception {
    new SerialMergeCenter(PrioritySelection.MAX, 1.0, verticesPath, edgesPath, outputPath).execute();

    Map<String, List<Pair<String, String>>> clusterLists = createClusterLists();

    assertEquals("expected 5 clusters, but found " + clusterLists.size(), 5, clusterLists.size());
  }

  @Test
  public void testClusteringForPrioritySelectionMinWithoutClusterMerging() throws Exception {
    new SerialMergeCenter(PrioritySelection.MIN, 1.0, verticesPath, edgesPath, outputPath).execute();

    Map<String, List<Pair<String, String>>> clusterLists = createClusterLists();

    assertEquals("expected 5 clusters, but found " + clusterLists.size(), 5, clusterLists.size());
  }

  @Test
  public void testClusteringForPrioritySelectionMaxWithClusterMerging() throws Exception {
    new SerialMergeCenter(PrioritySelection.MAX, 0.0, verticesPath, edgesPath, outputPath).execute();

    Map<String, List<Pair<String, String>>> clusterLists = createClusterLists();

    assertEquals("expected 4 clusters, but found " + clusterLists.size(), 4, clusterLists.size());
  }

  @Test
  public void testClusteringForPrioritySelectionMinWithClusterMerging() throws Exception {
    new SerialMergeCenter(PrioritySelection.MIN, 0.0, verticesPath, edgesPath, outputPath).execute();

    Map<String, List<Pair<String, String>>> clusterLists = createClusterLists();

    assertEquals("expected 4 clusters, but found " + clusterLists.size(), 4, clusterLists.size());
  }

  /**
   * Creates a map entry for each cluster, holding each of the cluster vertices as
   * {@code Pair<VertexId, ClusterId>}
   *
   * @return The clusters as mapping from cluster id to a list of cluster vertices
   * as {@code Pair<VertexId, ClusterId>}
   * @throws IOException Thrown on errors while reading the clustering result file.
   */
  private Map<String, List<Pair<String, String>>> createClusterLists() throws IOException {
    List<Pair<String, String>> results = new ArrayList<>();

    Files.lines(Paths.get(outputPath), StandardCharsets.UTF_8).forEach(entry -> {
      String[] lineData = entry.split(",", 2);
      results.add(Pair.create(lineData[0], lineData[1]));
    });

    return results.stream().collect(Collectors.groupingBy(Pair::getSecond));
  }
}
