/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms;

import org.junit.Test;

import java.util.Random;

import static org.junit.Assert.*;

/**
 * Test class for {@link HungarianAlgorithmSeq}
 */
public class HungarianAlgorithmSeqTest {

  private static final String FAIL_MESSAGE = "hungarian bipartite assignment result not as expected";

  /**
   * Test hungarian bipartite assignment for a symmetric cost matrix.
   */
  @Test
  public void testSymmetric() {
    double[][] costs = {
      {3, 2, 1, 7},
      {5, 6, 3, 8},
      {8, 2, 4, 5},
      {3, 9, 2, 8}
    };

    int[] expectedResult = {1, 2, 3, 0};

    HungarianAlgorithmSeq hu = new HungarianAlgorithmSeq(costs);
    int[] resHU = hu.execute();
    assertArrayEquals(FAIL_MESSAGE, expectedResult, resHU);
  }

  /**
   * Test hungarian bipartite assignment for a asymmetric cost matrix with more workers than jobs.
   */
  @Test
  public void testAsymmetricMoreWorkers() {
    // rows = workers
    // cols = jobs
    // => more workers than jobs

    double[][] costs = {
      {3, 2, 1, 7},
      {5, 6, 3, 8},
      {8, 2, 4, 5},
      {3, 9, 2, 8},
      {5, 9, 4, 8}
    };

    int[] expectedResult = {1, 2, 3, 0, -1};

    HungarianAlgorithmSeq hu = new HungarianAlgorithmSeq(costs);
    int[] resHU = hu.execute();
    assertArrayEquals(FAIL_MESSAGE, expectedResult, resHU);
  }

  /**
   * Test hungarian bipartite assignment for a asymmetric cost matrix with more workers than jobs.
   */
  @Test
  public void testAsymmetricMoreWorkersDifferentOrder() {
    // rows = workers
    // cols = jobs
    // => more workers than jobs
    double[][] costs = {
      {3, 2, 1, 7},
      {5, 6, 3, 8},
      {8, 2, 4, 5},
      {5, 9, 4, 8},
      {3, 9, 2, 8}
    };

    int[] expectedResult = {1, 2, 3, -1, 0};

    HungarianAlgorithmSeq hu = new HungarianAlgorithmSeq(costs);
    int[] resHU = hu.execute();
    assertArrayEquals(FAIL_MESSAGE, expectedResult, resHU);
  }

  /**
   * Test hungarian bipartite assignment for a asymmetric cost matrix with more jobs than workers.
   */
  @Test
  public void testAsymmetricMoreJobs() {
    // rows = workers
    // cols = jobs
    // => more jobs than workers
    double[][] costs = {
      {3, 2, 1, 7},
      {5, 6, 3, 8},
      {8, 2, 4, 5}
    };

    int[] expectedResult = {2, 0, 1};

    HungarianAlgorithmSeq hu = new HungarianAlgorithmSeq(costs);
    int[] resHU = hu.execute();
    assertArrayEquals(FAIL_MESSAGE, expectedResult, resHU);
  }

  /**
   * Test hungarian bipartite assignment for a decimal cost matrix of similarity values.
   */
  @Test
  public void testSimilarities() {
    double[][] costs = {
//      {3, 2, 1, 7},
//      {5, 6, 3, 8},
//      {8, 2, 4, 5},
//      {3, 9, 2, 8}
      {0.7, 0.8, 0.9, 0.3},
      {0.5, 0.4, 0.7, 0.2},
      {0.2, 0.8, 0.6, 0.5},
      {0.7, 0.1, 0.8, 0.2}
    };

    // invert the values, because the hungarian algorithm is minimizing cost, but we search maximal
    // similarities
    for (int i = 0; i < costs.length; i++) {
      for (int j = 0; j < costs[0].length; j++) {
        costs[i][j] = 1 - costs[i][j];
      }
    }

    int[] expectedResult = {1, 2, 3, 0};
    // 0,8+0,7+0,6+0,7  = 2,8

    HungarianAlgorithmSeq hu = new HungarianAlgorithmSeq(costs);
    int[] resHU = hu.execute();
    assertArrayEquals(FAIL_MESSAGE, expectedResult, resHU);
  }

  /**
   * Test hungarian bipartite assignment for a decimal cost matrix of noised similarity values.
   */
  @Test
  public void testSimilaritiesNoise() {
    Random r = new Random(1L);

    double[][] costs = {
      {0.7, 0.8, 0.9, 0.3},
      {0.5, 0.4, 0.7, 0.2},
      {0.2, 0.8, 0.6, 0.5},
      {0.7, 0.1, 0.8, 0.2}
    };

    for (int i = 0; i < costs.length; i++) {
      for (int j = 0; j < costs[0].length; j++) {
        costs[i][j] = 1 - costs[i][j] - 0.01 * r.nextDouble();
      }
    }

    int[] expectedResult = {1, 2, 3, 0};
    // 0,8+0,7+0,6+0,7  = 2,8

    HungarianAlgorithmSeq hu = new HungarianAlgorithmSeq(costs);
    int[] resHU = hu.execute();
    assertArrayEquals(FAIL_MESSAGE, expectedResult, resHU);
  }

  /**
   * Test hungarian bipartite assignment for a sparse decimal cost matrix of similarity values.
   */
  @Test
  public void testSparse() {
    double nul = -100;

    double[][] costs = {
      {nul, 0.8, 0.9, 0.3},
      {0.5, 0.4, 0.7, nul},
      {nul, 0.8, 0.6, 0.5},
      {0.7, nul, 0.8, nul}
    };

    for (int i = 0; i < costs.length; i++) {
      for (int j = 0; j < costs[0].length; j++) {
        costs[i][j] = 1 - costs[i][j];
      }
    }

    int[] expectedResult = {1, 2, 3, 0};

    HungarianAlgorithmSeq hu = new HungarianAlgorithmSeq(costs);
    int[] resHU = hu.execute();
    assertArrayEquals(FAIL_MESSAGE, expectedResult, resHU);
  }
}
