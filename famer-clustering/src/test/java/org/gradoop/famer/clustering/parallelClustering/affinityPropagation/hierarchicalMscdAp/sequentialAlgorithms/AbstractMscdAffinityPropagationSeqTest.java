/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp
  .sequentialAlgorithms;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils.ApTestUtils;
import org.junit.Test;

/**
 * Abstract test class for sequential implementations of MSCD Affinity Propagation.
 */
public abstract class AbstractMscdAffinityPropagationSeqTest {

  /**
   * Null value for sparse matrices to mark an edge as not present.
   */
  protected final double nul = Double.NEGATIVE_INFINITY;

  /**
   * Get a new instance of the derived sequential Affinity Propagation implementation.
   *
   * @param similarityMatrix NxN similarity matrix for N dataPoints. Cell ij represents the similarity
   *                         between dataPoint i and dataPoint j.
   * @param preference self-similarity parameter
   * @return new instance of the derived sequential Affinity Propagation implementation.
   */
  protected abstract AbstractMscdAffinityPropagationSeq getNewApInstance(
    double[][] similarityMatrix, double preference);

  /**
   * Test the cluster assignment if all similarities of S are equal.
   */
  @Test
  public void testAllSame() {
    double[][] simMatrix = {
      {0.6, 0.6, 0.6, 0.6, 0.6},
      {0.6, 0.6, 0.6, 0.6, 0.6},
      {0.6, 0.6, 0.6, 0.6, 0.6},
      {0.6, 0.6, 0.6, 0.6, 0.6},
      {0.6, 0.6, 0.6, 0.6, 0.6}};

    AbstractMscdAffinityPropagationSeq ap;
    Multimap<String, Integer> cleanSourceElementIndices = ArrayListMultimap.create();
    ApConfig apConfig = new ApConfig();

    // ### Standard AP
    // no threshold defined && no sources defined => all in the same cluster
    ap = getNewApInstance(simMatrix, simMatrix[0][0]);
    ap.fit();
    ApTestUtils.confirmApResult(ap, new int[] {0, 0, 0, 0, 0}, new Integer[] {0});

    // threshold > similarity && no source definitions => all in different clusters
    apConfig.setAllSameSimClusteringThreshold(0.7);
    ap = getNewApInstance(simMatrix, simMatrix[0][0]);
    ap.setApConfig(apConfig);
    ap.fit();
    ApTestUtils.confirmApResult(ap, new int[] {0, 1, 2, 3, 4}, new Integer[] {0, 1, 2, 3, 4});

    // threshold <= similarity && no source definitions => all in the same cluster
    apConfig.setAllSameSimClusteringThreshold(0.6);
    ap = getNewApInstance(simMatrix, simMatrix[0][0]);
    ap.setApConfig(apConfig);
    ap.fit();
    ApTestUtils.confirmApResult(ap, new int[] {0, 0, 0, 0, 0}, new Integer[] {0});

    // ### AP for MSCD
    // SOURCES = 0 1 0 0 1
    // CLEAN_SOURCES: 0
    cleanSourceElementIndices.clear();
    cleanSourceElementIndices.put("0", 0);
    cleanSourceElementIndices.put("0", 2);
    cleanSourceElementIndices.put("0", 3);

    // no threshold && cleanSources defined => all in the same cluster, but clean sources separated
    ap = getNewApInstance(simMatrix, simMatrix[0][0]);
    ap.setCleanSourceElementIndices(cleanSourceElementIndices);
    ap.fit();
    ApTestUtils.confirmApResult(ap, new int[] {0, 0, 1, 2, 0}, new Integer[] {0, 2, 3});

    // threshold > similarity && cleanSources defined => all in different clusters
    apConfig.setAllSameSimClusteringThreshold(0.7);
    ap = getNewApInstance(simMatrix, simMatrix[0][0]);
    ap.setApConfig(apConfig);
    ap.setCleanSourceElementIndices(cleanSourceElementIndices);

    ap.fit();
    ApTestUtils.confirmApResult(ap, new int[] {0, 1, 2, 3, 4}, new Integer[] {0, 1, 2, 3, 4});

    // threshold <= similarity && cleanSources defined => all in the same cluster, but clean sources separated
    apConfig.setAllSameSimClusteringThreshold(0.6);
    ap = getNewApInstance(simMatrix, simMatrix[0][0]);
    ap.setCleanSourceElementIndices(cleanSourceElementIndices);
    ap.setApConfig(apConfig);
    ap.fit();
    ApTestUtils.confirmApResult(ap, new int[] {0, 0, 1, 2, 0}, new Integer[] {0, 2, 3});

    // CLEAN_SOURCES: 0, 1
    cleanSourceElementIndices.put("1", 1);
    cleanSourceElementIndices.put("1", 4);

    // no threshold && cleanSources defined => all in the same cluster, but clean sources separated
    ap = getNewApInstance(simMatrix, simMatrix[0][0]);
    ap.setCleanSourceElementIndices(cleanSourceElementIndices);
    ap.fit();
    ApTestUtils.confirmApResult(ap, new int[] {0, 0, 1, 2, 1}, new Integer[] {0, 2, 3});

    // CLEAN_SOURCES: 1
    // threshold <= similarity && cleanSources defined => all in the same cluster, but clean sources separated
    cleanSourceElementIndices.clear();
    cleanSourceElementIndices.put("1", 1);
    cleanSourceElementIndices.put("1", 4);

    apConfig.setAllSameSimClusteringThreshold(0.6);
    ap = getNewApInstance(simMatrix, simMatrix[0][0]);
    ap.setCleanSourceElementIndices(cleanSourceElementIndices);
    ap.setApConfig(apConfig);
    ap.fit();
    ApTestUtils.confirmApResult(ap, new int[] {0, 0, 0, 0, 1}, new Integer[] {0, 4});
  }

  /**
   * MSCD-AP and traditional AP for the clustering of the example for the thesis.
   */
  @Test
  public void thesisExampleMscdApVsStdAp() {
    double[][] s = {
      {nul, 0.2, nul, nul, nul},
      {0.2, nul, 0.9, 0.3, nul},
      {nul, 0.9, nul, nul, 0.8},
      {nul, 0.3, nul, nul, 0.2},
      {nul, nul, 0.8, 0.2, nul}
    };

    // LABELS: 0 1 1 2 1    (Dirty ER)
    // LABELS: 0 1 1 2 3    (MSCD ER, with clean src a={1,4})

    ArrayListMultimap<String, Integer> cleanSourceElementIndices = ArrayListMultimap.create();

    // test standard AP

    AbstractMscdAffinityPropagationSeq ap = getNewApInstance(s, 0.5);
    ApConfig apConfig = new ApConfig();
    apConfig.setConvergenceIter(10);
    apConfig.setMaxAdaptionIteration(200);
    apConfig.setNoiseDecimalPlace(-1);
    apConfig.setAllSameSimClusteringThreshold(0.7);
    ap.setApConfig(apConfig);
    ap.setCleanSourceElementIndices(cleanSourceElementIndices);

    ap.fit();

    int[] expectedLabels = { 0, 1, 1, 2, 1 };
    Integer[] expectedExemplars = { 0, 2, 3 };
    ApTestUtils.confirmApResult(ap, expectedLabels, expectedExemplars);

    // test MSCD AP => separate dataPoints of source a

    cleanSourceElementIndices.put("a", 1);
    cleanSourceElementIndices.put("a", 4);

    ap = getNewApInstance(s, 0.3);
    ap.setPreferenceDirtySrc(0.3);
    ap.setPreferenceCleanSrc(0.7);
    ap.setApConfig(apConfig);
    ap.setCleanSourceElementIndices(cleanSourceElementIndices);

    ap.fit();

    expectedLabels = new int[] { 0, 1, 1, 2, 3 };
    expectedExemplars = new Integer[] { 0, 1, 3, 4 };
    ApTestUtils.confirmApResult(ap, expectedLabels, expectedExemplars);
  }

  /**
   * Example to show that AP does not interpret similarity values. It just clusters by the difference
   * between the values.
   */
  @Test
  public void thesisExampleApDoesNotInterpretSimilarities() {
    double[][] s1 = {
      {nul, 0.5,  nul, nul,  nul},
      {0.5, nul,  0.8, 0.45, nul},
      {nul, 0.8,  nul, nul,  0.7},
      {nul, 0.45, nul, nul,  0.5},
      {nul, nul,  0.7, 0.5,  nul}
    };

    double[][] s2 = {
      {nul, 0.1,  nul, nul,  nul},
      {0.1, nul,  0.4, 0.05, nul},
      {nul, 0.4,  nul, nul,  0.3},
      {nul, 0.05, nul, nul,  0.1},
      {nul, nul,  0.3, 0.1,  nul}
    };

    // LABELS: 0 1 1 2 1
    int[] expectedLabels = { 0, 1, 1, 2, 1 };
    Integer[] expectedExemplars = { 0, 2, 3 };
    ApConfig apConfig = new ApConfig();
    apConfig.setNoiseDecimalPlace(-1);
    apConfig.setMaxAdaptionIteration(2000);
    apConfig.setConvergenceIter(15);

    AbstractMscdAffinityPropagationSeq ap = getNewApInstance(s1, 0.45);
    ap.setApConfig(apConfig);
    ap.fit();

    ApTestUtils.confirmApResult(ap, expectedLabels, expectedExemplars);

    ap = getNewApInstance(s2, 0.05);
    ap.setApConfig(apConfig);
    ap.fit();

    ApTestUtils.confirmApResult(ap, expectedLabels, expectedExemplars);
  }

  /**
   * Example to show that AP splits clusters too much, because it does not interpret similarity values.
   * Using the minimum similarity as preference splits the component into two clusters.
   */
  @Test
  public void thesisExampleApSplitTooMuch() {
    double[][] s = {
      {nul, 0.8,  1.0,  0.9, nul},
      {0.8, nul,  0.85, 0.9, nul},
      {1.0, 0.85, nul,  nul, 0.9},
      {0.9, 0.9,  nul,  nul, 1.0},
      {nul, nul,  0.9,  1.0, nul}
    };
    ApConfig apConfig = new ApConfig();
    apConfig.setNoiseDecimalPlace(-1);
    apConfig.setMaxAdaptionIteration(2000);
    apConfig.setConvergenceIter(15);

    // LABELS should be: 0 0 0 0 0
    // but AP tend's to split too much => this behaviour is described in the thesis

    AbstractMscdAffinityPropagationSeq ap = getNewApInstance(s, 0.8);
    ap.setApConfig(apConfig);
    ap.fit();

    ApTestUtils.confirmApResult(ap, new int[] { 0, 1, 0, 1, 1 }, new Integer[] {0, 3});
  }

  /**
   * Test the execution of the sequential AP implementation for standard AP (no clean sources) and MSCD-AP
   * (clean sources defined).
   */
  @Test
  public void testStdApAndMscdAp() {
    double[][] simMatrix = {
      {1,   0.2, 0.1, 0,   0.1},
      {0.2, 1,   0.8, 0.3, 0.9},
      {0.1, 0.8, 1,   0.1, 0.6},
      {0,   0.3, 0.1, 1,   0.2},
      {0.1, 0.9, 0.6, 0.2, 1}
    };
    ApConfig apConfig = new ApConfig();
    apConfig.setNoiseDecimalPlace(-1);

    // Labels: 0 1 1 2 1

    AbstractMscdAffinityPropagationSeq ap;
    ArrayListMultimap<String, Integer> cleanSourceElementIndices = ArrayListMultimap.create();

    // without sources defined
    ap = getNewApInstance(simMatrix, 0.3);
    ap.setApConfig(apConfig);
    ap.fit();
    ApTestUtils.confirmApResult(ap, new int[] {0, 1, 1, 2, 1}, new Integer[] {0, 1, 3});

    // with clean source a={1,2}
    cleanSourceElementIndices.put("a", 1);
    cleanSourceElementIndices.put("a", 2);
    ap = getNewApInstance(simMatrix, 0.4);
    ap.setPreferenceCleanSrc(0.7);
    ap.setCleanSourceElementIndices(cleanSourceElementIndices);
    ap.setApConfig(apConfig);
    ap.fit();
    ApTestUtils.confirmApResult(ap, new int[] {0, 1, 2, 3, 1}, new Integer[] {0, 1, 2, 3});
  }
}
