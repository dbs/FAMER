/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.connectedComponents;

import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Test class for the {@link ConnectedComponents} algorithm.
 */
public class ConnectedComponentsTest extends GradoopFlinkTestBase {

  private final String clusterIdPrefix = "prefix_";

  private final String similarityEdgeLabel = "similarityEdge";

  private LogicalGraph inputGraph;

  @Before
  public void setUpGraph() throws Exception {
    String graphString = "inputGraph[" +
      "/* connected component 1 */" +
      "(v0 {id:0, component:1})" +
      "(v1 {id:1, component:1})" +
      "(v0)-[e0]->(v1)" +
      "/* connected component 2 */" +
      "(v2 {id:2, component:2})" +
      "(v3 {id:3, component:2})" +
      "(v4 {id:4, component:2})" +
      "(v2)-[e1]->(v3)" +
      "(v2)-[e2]->(v4)" +
      "(v3)-[e3]->(v4)" +
      "/* connected component 3 */" +
      "(v5 {id:5, component:3})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("inputGraph");
  }

  @Test
  public void testAnnotateConnectedComponentsForAllEdgesWithoutPrefix() throws Exception {
    GraphCollection result = new ConnectedComponents(Integer.MAX_VALUE)
      .execute(inputGraph).splitBy(CLUSTER_ID);

    GraphCollection expected = inputGraph.splitBy("component");

    collectAndAssertTrue(result.equalsByGraphElementIds(expected));
  }

  @Test
  public void testAnnotateConnectedComponentsForAllEdgesWithPrefix() throws Exception {
    GraphCollection result = new ConnectedComponents(Integer.MAX_VALUE, clusterIdPrefix)
      .execute(inputGraph).splitBy(CLUSTER_ID);

    GraphCollection expected = inputGraph.splitBy("component");

    collectAndAssertTrue(result.equalsByGraphElementIds(expected));

    result.getVertices().collect().forEach(vertex -> {
      assertTrue("vertex has no cluster id prefix, but should have",
        vertex.getPropertyValue(CLUSTER_ID).getString().startsWith(clusterIdPrefix));
    });
  }

  @Test
  public void testAnnotateConnectedComponentsForSimilarityEdgesWithoutPrefix() throws Exception {
    String graphString = "similarityGraph[" +
      "/* connected component 1 */" +
      "(v0 {id:0, component:1})" +
      "(v1 {id:1, component:1})" +
      "(v0)-[e0:similarityEdge]->(v1)" +
      "/* connected component 2 */" +
      "(v2 {id:2, component:2})" +
      "(v3 {id:3, component:2})" +
      "(v4 {id:4, component:2})" +
      "(v2)-[e1:similarityEdge]->(v3)" +
      "(v2)-[e2:similarityEdge]->(v4)" +
      "(v3)-[e3:similarityEdge]->(v4)" +
      "/* connected component 3, connecting edge has no similarity label */" +
      "(v5 {id:5, component:3})" +
      "(v4)-[e4]->(v5)" +
      "]";
    LogicalGraph similarityGraph = getLoaderFromString(graphString)
      .getLogicalGraphByVariable("similarityGraph");

    LogicalGraph result = new ConnectedComponents(Integer.MAX_VALUE, "", similarityEdgeLabel)
        .execute(similarityGraph);

    long componentCount = result.splitBy(CLUSTER_ID).getGraphHeads().collect().size();

    assertEquals("expected 3 connected components, but found " + componentCount, 3L, componentCount);
  }
}
