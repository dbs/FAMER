/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms;

import com.google.common.collect.Multimap;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils.ApTestUtils;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Test class for {@link MscdAffinityPropagationSeq}.
 */
public class MscdAffinityPropagationSeqTest extends AbstractMscdAffinityPropagationSeqTest {

  /**
   * Get a new instance of {@link MscdAffinityPropagationSeq}
   *
   * @param similarityMatrix similarity matrix
   * @param preference self-similarity parameter
   * @return new instance of {@link MscdAffinityPropagationSeq}
   */
  @Override
  protected AbstractMscdAffinityPropagationSeq getNewApInstance(double[][] similarityMatrix, double preference) {
    return new MscdAffinityPropagationSeq(similarityMatrix, preference);
  }

  /**
   * Test the calculation of Eta.
   * Eta[i][j] = -1 * max_{k != j}(S[i][k] + A[i][k] + Theta[i][k])
   */
  @Test
  public void apPart1OneOfNConstraintTestingRealData() {
    // ### input
    double[][] s = {
      {0.3, 0.2, nul, nul, nul},
      {0.2, 0.5, 0.8, 0.3, nul},
      {nul, 0.8, 0.3, nul, 0.6},
      {nul, 0.3, nul, 0.3, 0.2},
      {nul, nul, 0.6, 0.2, 0.5}
    };

    double[][] a = {
      {0, -0.025, nul, nul, nul},
      {0, 0.05, -0.125, 0.0, nul},
      {nul, -0.075, 0.05, nul, -0.025},
      {nul, -0.025, nul, 0, -0.025},
      {nul, nul, -0.075, 0, 0}
    };

    double[][] theta = {
      {0, 0, nul, nul, nul},
      {0, 0, -0.1, 0, nul},
      {nul, 0, 0, nul, 0},
      {nul, 0, nul, 0, 0},
      {nul, nul, -0.3, 0, 0}
    };

    // ### intermediate result

    // Beta = S + A + Theta:
    // 0,3    0,175     nul       nul     nul
    // 0,2    0,55      0,575     0,3     nul
    // nul    0,725     0,35      nul     0,575
    // nul    0,275     nul       0,3     0,175
    // nul    nul       0,225     0,2     0,5

    // ### expected output

    double[][] expectedEta = {
      {-0.175, -0.3, -0.3, -0.3, -0.3},
      {-0.575, -0.575, -0.55, -0.575, -0.575},
      {-0.725, -0.575, -0.725, -0.725, -0.725},
      {-0.3, -0.3, -0.3, -0.275, -0.3},
      {-0.5, -0.5, -0.5, -0.5, -0.225}
    };

    double[][] eta = {
      {0, 0, nul, nul, nul},
      {0, 0, 0, 0, nul},
      {nul, 0, 0, nul, 0},
      {nul, 0, nul, 0, 0},
      {nul, nul, 0, 0, 0}
    };

    MscdAffinityPropagationSeq.affinityPiece1getEta(a, s, theta, eta);

    for (int i = 0; i < s.length; i++) {
      for (int j = 0; j < s.length; j++) {
        assertEquals(expectedEta[i][j], eta[i][j], 0.00001);
      }
    }
  }

  /**
   * Test the calculation of Eta.
   * Eta[i][j] = -1 * max_{k != j}(S[i][k] + A[i][k] + Theta[i][k])
   */
  @Test
  public void apPart1OneOfNConstraintTestingPlayData() {
    // ### input
    double[][] s = {
      {3, 2, 1, nul, nul},
      {5, 1, 4, 2, 3}
    };

    double[][] a = {
      {1, 1, 1, nul, nul},
      {1, 1, 1, 1, 1}
    };

    double[][] theta = {
      {2, 2, 2, nul, nul},
      {2, 2, 2, 2, 2}
    };

    // ### intermediate result

    // Beta = S + A + Theta:
    // 6, 5, 4, nul, nul
    // 8, 4, 7, 5, 6

    // ### expected output

    double[][] expectedEta = {
      {-5, -6, -6, -6, -6},
      {-7, -8, -8, -8, -8}
    };

    double[][] eta = new double[2][5];

    MscdAffinityPropagationSeq.affinityPiece1getEta(a, s, theta, eta);

    final int nrRows = s.length;
    final int nrCols = s[0].length;

    for (int i = 0; i < nrRows; i++) {
      for (int j = 0; j < nrCols; j++) {
        assertEquals(expectedEta[i][j], eta[i][j], 0.00001);
      }
    }
  }

  /**
   * Test the calculation of Theta.
   * Gamma_kj = S_ij + A_ij + Eta_ij
   * Theta_ij = min[0, -1 * max_{k != i}(Gamma_kj)]
   * => -1 * maximal column value of all other cells of that column (of the same clean source!), only negative
   * values taken
   */
  @Test
  public void apPart2CleanSourcesConstraint() {
    // ### input
    double[][] s = {
      {3, 5, 10},
      {2, -5, 2},
      {6, 8, 10},
      {nul, -3, 3},
      {nul, 7, 4},
      {3, 2, 1}
    };

    double[][] eta = {
      {1, 1, 1},
      {1, 1, 1},
      {1, 1, 1},
      {nul, 1, 1},
      {nul, 1, 1},
      {1, 1, 1}
    };

    double[][] a = {
      {-4, -4, -4},
      {-4, -4, -4},
      {-4, -4, -4},
      {nul, -4, -4},
      {nul, -4, -4},
      {-4, -4, -4}
    };

    String[] rowCleanSrc = {"b", "a", "b", "", "a", "b"};

    // ### intermediate result

    // Gamma = S + Eta + A:
    // 0       2       7      b
    // -1     -8      -1          a
    // 3       5       7      b
    // nul    -6       0              -
    // nul    4        1          a
    // 0     -1       -2      b

    // ### expected output

    double[][] expectedTheta = {
      {-3, -5, -7},
      {0, -4, -1},
      {0, -2, -7},
      {0, 0, 0},
      {0, 0, 0},
      {-3, -5, -7}
    };

    double[][] theta = new double[6][3];

    Multimap<String, Integer> cleanSourceElementIndices = ApTestUtils.cleanSourcesArrayToMultiMap(rowCleanSrc);

    MscdAffinityPropagationSeq.affinityPiece2getTheta(a, s, theta, eta, cleanSourceElementIndices);

    final int nrRows = s.length;
    final int nrCols = s[0].length;

    for (int i = 0; i < nrRows; i++) {
      for (int j = 0; j < nrCols; j++) {
        assertEquals(expectedTheta[i][j], theta[i][j], 0.00001);
      }
    }
  }

  /**
   * Test the calculation of A and R.
   * R_ij = S_ij + Eta_ij + Theta_ij
   *
   * <p>
   * A_ij = Sum_{k != j}( max(0, R_kj )                           for diagonal (i == j)
   * <ul>
   *   <li>=> Column-Sum of positive elements without self</li>
   * </ul>
   *
   * <p>
   * A_ij = min[0,  R_jj + Sum_{k != i,j}(max(0, R_kj))]          for non-diagonal (i != j)
   * <ul>
   *   <li>=> Column-Sum of positive elements without self and diagonal  + diagonal(also negative)</li>
   *   <li>=> take only values <= 0</li>
   * </ul>
   *
   * <p>
   * The tested configuration tests all the special cases of A and R calculation:
   * <ul>
   *   <li>damping</li>
   *   <li>A results > 0</li>
   *   <li>A results < 0</li>
   *   <li>positive diagonal value</li>
   *   <li>negative diagonal value</li>
   * </ul>
   */
  @Test
  public void apPart3ExemplarConistencyConstraint() {
    // ### input

    double[][] s = {
      {3, 5, 1},
      {2, -5, 2},
      {-6, -4, -30},
      {nul, -3, 3},
      {nul, -7, 4}
    };

    double[][] eta = {
      {1, 1, 1},
      {1, 1, 1},
      {1, 1, 1},
      {nul, 1, 1},
      {nul, 1, 1}
    };

    double[][] theta = {
      {2, 2, 2},
      {2, 2, 2},
      {2, 2, 2},
      {nul, 2, 2},
      {nul, 2, 2}
    };

    double[][] r = {
      {3, 3, 3},
      {3, -3, 3},
      {-3, -3, -3},
      {nul, 0, 3},
      {nul, -3, -3}
    };

    double[][] a = {
      {-4, -4, -4},
      {-4, -4, -4},
      {-4, -4, -4},
      {nul, -4, -4},
      {nul, -4, -4}
    };

    // ### intermediate result

    // R = S + Eta + Theta:          damped
    // 6, 8,  4                         1.2,   1.6      0.8
    // 5, -2,  5                          1     -0.4      1
    // -3, -1, -27                        -0.6    -0.2     -5.4
    // nul, 0,  6                        nul    0         1.2
    // nul, -4,  7                        nul    -0.8     1.4

    // ### expected output

    // damping R = oldR * dampingFactor + newR * (1-dampingFactor)
    double[][] expectedR = {
      {3.6, 4, 3.2},
      {3.4, -2.8, 3.4},
      {-3, -2.6, -7.8},
      {nul, 0, 3.6},
      {nul, -3.2, -1}
    };

    // ### intermediate result

    // A                        damped
    // 3.4    -2.8    -0.8         0.68    -0.56     -0.16
    // 0     4       -1              0     0.8      -0.2
    // 0     0     10,2             0      0     2.04
    // nul    0     -1,2           nul     0     -0.24
    // nul    0      0           nul     0       0

    // damping A = oldA * dampingFactor + newA * (1-dampingFactor)
    // -4 * 0.8 = -3.2

    // ### expected output

    double[][] expectedA = {
      {-2.52, -3.76, -3.36},
      {-3.2, -2.4, -3.4},
      {-3.2, -3.2, -1.16},
      {nul, -3.2, -3.44},
      {nul, -3.2, -3.2}
    };

    MscdAffinityPropagationSeq.affinityPiece3getA(a, s, theta, eta, r, 0.8);

    final int nrRows = s.length;
    final int nrCols = s[0].length;

    for (int i = 0; i < nrRows; i++) {
      for (int j = 0; j < nrCols; j++) {
        assertEquals(expectedR[i][j], r[i][j], 0.00001);
        assertEquals(expectedA[i][j], a[i][j], 0.00001);
      }
    }
  }
}
