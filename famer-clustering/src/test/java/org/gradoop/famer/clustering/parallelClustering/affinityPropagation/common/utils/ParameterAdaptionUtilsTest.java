/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.junit.Assert.*;

/**
 * Test class for {@link ParameterAdaptionUtils}
 */
public class ParameterAdaptionUtilsTest {

  /**
   * Used to test, whether a method throws an expected exception.
   */
  @Rule
  public ExpectedException exceptionRule = ExpectedException.none();

  /**
   * Test the correct parameter increasing, decreasing and direction switch in
   * {@link ParameterAdaptionUtils#adaptParameterValue}.
   */
  @Test
  public void testParameterAdaption() {

    double newValue;

    // test damping adaption
    double startValue = 0.5;
    double step = 0.1;
    double upperBoundExclusive = 1.0;
    double lowerBoundExclusive = 0.49;
    boolean startWithDecrease = false;

    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.5, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(0.6, newValue, 0.0000001);

    // upper limit hit => finish
    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.96, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(-2d, newValue, 0.0000001);

    // upper limit exact hit
    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.9, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(-2d, newValue, 0.0000001);

    startValue = 0.8;

    // upper limit hit => go on with decrease
    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.9, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(0.7, newValue, 0.0000001);

    // decrease
    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.6, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(0.5, newValue, 0.0000001);

    // lower limit hit => finish
    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.5, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(-2d, newValue, 0.0000001);

    // lower limit exact hit
    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.59, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(-2d, newValue, 0.0000001);

    // test preference adaption
    startValue = 0.925;
    step = 0.05;
    upperBoundExclusive = 1.0;
    lowerBoundExclusive = 0;
    startWithDecrease = true;

    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.925, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(0.875, newValue, 0.0000001);

    // rise again after decrease
    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.025, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(0.975, newValue, 0.0000001);

    // lower limit exact hit
    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.05, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(0.975, newValue, 0.0000001);

    // upper limit hit => finish
    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.975, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(-2d, newValue, 0.0000001);

    // upper limit exact hit
    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 0.95, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(-2d, newValue, 0.0000001);

    // change to decrease when start value - step >= upperBound
    startValue = 1.1;
    step = 0.05;
    upperBoundExclusive = 1.0;
    lowerBoundExclusive = 0;
    startWithDecrease = false;

    newValue = ParameterAdaptionUtils.adaptParameterValue(startValue, 1.1, step, upperBoundExclusive,
      lowerBoundExclusive, startWithDecrease);
    assertEquals(1.05, newValue, 0.0000001);
  }

  /**
   * Test the dependent parameter adaption of clean-source-preference, dirty-source-preference and the
   * damping factor, using {@link ParameterAdaptionUtils#getNewParameterValues}.
   */
  @Test
  public void testDependentParameterAdaption() throws ConvergenceException {

    // 1) test adapting preference for dirty and clean sources together with clean > dirty
    double origPrefDirtySrc = 0.1;
    double origPrefCleanSrc = 0.5;
    ApConfig apConfig = new ApConfig();
    apConfig.getPreferenceConfig().setPreferenceAdaptionStep(0.05);
    apConfig.setDampingAdaptionStep(0.1);

    // expected results for <prefDirtySrc, prefCleanSrc, damping>
    double[][] expectedResults = {
      {0.05, 0.45, 0.5},
      {0.00, 0.40, 0.5},
      {0.15, 0.55, 0.5},
      {0.20, 0.60, 0.5},
      {0.25, 0.65, 0.5},
      {0.30, 0.70, 0.5},
      {0.35, 0.75, 0.5},
      {0.40, 0.80, 0.5},
      {0.45, 0.85, 0.5},
      {0.50, 0.90, 0.5},
      {0.55, 0.95, 0.5},
      {0.60, 1.00, 0.5},
      {0.10, 0.50, 0.6},
      {0.05, 0.45, 0.6},
      {0.00, 0.40, 0.6},
      {0.15, 0.55, 0.6}
    };

    executeDependentDampingAdaptionTest(expectedResults, origPrefDirtySrc, origPrefCleanSrc, apConfig);

    // 2) test adapting preference for dirty and clean sources together with dirty > clean
    origPrefDirtySrc = 0.5;
    origPrefCleanSrc = 0.1;

    expectedResults = new double[][] {
      {0.45, 0.05, 0.5},
      {0.40, 0.00, 0.5},
      {0.55, 0.15, 0.5},
      {0.60, 0.20, 0.5},
      {0.65, 0.25, 0.5},
      {0.70, 0.30, 0.5},
      {0.75, 0.35, 0.5},
      {0.80, 0.40, 0.5},
      {0.85, 0.45, 0.5},
      {0.90, 0.50, 0.5},
      {0.95, 0.55, 0.5},
      {1.00, 0.60, 0.5},
      {0.50, 0.10, 0.6},
      {0.45, 0.05, 0.6},
      {0.40, 0.00, 0.6},
      {0.55, 0.15, 0.6}
    };

    executeDependentDampingAdaptionTest(expectedResults, origPrefDirtySrc, origPrefCleanSrc, apConfig);

    // 3) test adapting preference for dirty and clean sources together with same value
    origPrefDirtySrc = 0.2;
    origPrefCleanSrc = 0.2;

    expectedResults = new double[][] {
      {0.15, 0.15, 0.5},
      {0.10, 0.10, 0.5},
      {0.05, 0.05, 0.5},
      {0.00, 0.00, 0.5},
      {0.25, 0.25, 0.5},
      {0.30, 0.30, 0.5},
      {0.35, 0.35, 0.5},
      {0.40, 0.40, 0.5},
      {0.45, 0.45, 0.5},
      {0.50, 0.50, 0.5},
      {0.55, 0.55, 0.5},
      {0.60, 0.60, 0.5},
      {0.65, 0.65, 0.5},
      {0.70, 0.70, 0.5},
      {0.75, 0.75, 0.5},
      {0.80, 0.80, 0.5},
      {0.85, 0.85, 0.5},
      {0.90, 0.90, 0.5},
      {0.95, 0.95, 0.5},
      {1.00, 1.00, 0.5},
      {0.20, 0.20, 0.6},
      {0.15, 0.15, 0.6},
      {0.10, 0.10, 0.6},
      {0.05, 0.05, 0.6},
      {0.00, 0.00, 0.6},
      {0.25, 0.25, 0.6},
      {0.30, 0.30, 0.6}
    };

    executeDependentDampingAdaptionTest(expectedResults, origPrefDirtySrc, origPrefCleanSrc, apConfig);
  }

  /**
   * Test, whether the dependent parameter adaption in {@link ParameterAdaptionUtils#getNewParameterValues}
   * throws the expected {@link ConvergenceException}, when all possible configurations were tried.
   *
   * @throws ConvergenceException expected exception, when all possible configurations were tried
   */
  @Test
  public void testDependentParameterAdaptionLimit() throws ConvergenceException {

    double origPrefDirtySrc = 0.1;
    double origPrefCleanSrc = 0.5;
    ApConfig apConfig = new ApConfig();
    apConfig.getPreferenceConfig().setPreferenceAdaptionStep(0.05);
    apConfig.setDampingAdaptionStep(0.1);

    /* tries = (13 * 5) -1
     * => 13 tries for one raise of dmp.
     * => 5 dmp raises until everything tried.
     * => -1 because first try = 0
     */
    int expectedTries = 64;

    exceptionRule.expect(ConvergenceException.class);
    exceptionRule.expectMessage(String.format("The algorithm could not converge for a connected " +
      "component. It tried all possible parameter settings. (%d tries)", expectedTries));

    double currentPrefDirtySrc = origPrefDirtySrc;
    double currentPrefCleanSrc = origPrefCleanSrc;
    double currentDamping = apConfig.getDampingFactor();

    ParameterAdaptionUtils adaptionUtils;
    for (int i = 0; i < 1000; i++) {
      adaptionUtils = ParameterAdaptionUtils.getNewParameterValues(origPrefDirtySrc,
        origPrefCleanSrc, currentPrefDirtySrc,
        currentPrefCleanSrc, currentDamping, i, apConfig);

      currentPrefDirtySrc = adaptionUtils.getNewPreferenceDirtySrc();
      currentPrefCleanSrc = adaptionUtils.getNewPreferenceCleanSrc();
      currentDamping = adaptionUtils.getNewDampingFactor();
    }
  }

  /**
   * Execute the dependent parameter adaption, using {@link ParameterAdaptionUtils#getNewParameterValues} and
   * compare the expected results with the actual ones.
   *
   * @param expectedResults expected results for <prefDirtySrc, prefCleanSrc, damping>
   * @param origPrefDirtySrc starting value of the dirty-source-preference
   * @param origPrefCleanSrc starting value of the clean-source-preference
   * @param apConfig configuration of the AP clustering algorithm, containing the starting value for the
   *                 damping factor and the adaption steps of preference and damping
   * @throws ConvergenceException when all possible values were tried
   */
  private void executeDependentDampingAdaptionTest(double[][] expectedResults, double origPrefDirtySrc,
    double origPrefCleanSrc, ApConfig apConfig) throws ConvergenceException {

    double currentPrefDirtySrc = origPrefDirtySrc;
    double currentPrefCleanSrc = origPrefCleanSrc;
    double currentDamping = apConfig.getDampingFactor();

    ParameterAdaptionUtils adaptionUtils;
    for (int i = 0; i < expectedResults.length; i++) {
      adaptionUtils = ParameterAdaptionUtils.getNewParameterValues(origPrefDirtySrc,
        origPrefCleanSrc, currentPrefDirtySrc,
        currentPrefCleanSrc, currentDamping, i, apConfig);

      currentPrefDirtySrc = adaptionUtils.getNewPreferenceDirtySrc();
      currentPrefCleanSrc = adaptionUtils.getNewPreferenceCleanSrc();
      currentDamping = adaptionUtils.getNewDampingFactor();

      assertEquals("adapted preference for dirty sources not as expected (adaption step: " + i + ")",
        expectedResults[i][0], currentPrefDirtySrc, 0.00001);
      assertEquals("adapted preference for clean sources not as expected (adaption step: " + i + ")",
        expectedResults[i][1], currentPrefCleanSrc, 0.00001);
      assertEquals("adapted damping factor not as expected (adaption step: " + i + ")",
        expectedResults[i][2], currentDamping, 0.00001);
    }
  }
}
