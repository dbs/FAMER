/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.flink.runtime.client.JobExecutionException;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.AbstractAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.AffinityPropagationTestBase;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils.ApTestUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils.ConfigParsingTestUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.dataStuctures.HapConfig;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test class for the parallel {@link HierarchicalAffinityPropagation} algorithm.
 */
public class HierarchicalAffinityPropagationTest extends AffinityPropagationTestBase {

  @Override
  protected AbstractAffinityPropagation getNewApInstance() {
    return new HierarchicalAffinityPropagation();
  }

  /**
   * Cast {@link #ap} to {@link HierarchicalAffinityPropagation}
   *
   * @return {@link #ap} as {@link HierarchicalAffinityPropagation}
   */
  private HierarchicalAffinityPropagation getHap() {
    return (HierarchicalAffinityPropagation) ap;
  }

  /**
   * Get the {@link HapConfig} of the current {@link HierarchicalAffinityPropagation} instance.
   *
   * @return {@link HapConfig} of the current {@link HierarchicalAffinityPropagation} instance
   */
  private HapConfig getHapConfig() {
    return getHap().getHapConfig();
  }

  /**
   * Test, whether a json config file for the {@link HapConfig} is correctly parsed.
   *
   * @throws IOException if the json config file could nor be read correctly from disk.
   * @throws JSONException if the json config file could not be parsed correctly for a certain parameter.
   */
  @Test
  public void testConfigParsing() throws IOException, JSONException {

    String jsonConfigFilePath = new File(HierarchicalAffinityPropagationTest.class.getResource(
      "/parallelClustering/affinityPropagation/common/testHapConfig.json").getFile()).getAbsolutePath();

    String jsonString = StringUtils.newStringUtf8(Files.readAllBytes(Paths.get(jsonConfigFilePath)));
    JSONObject jsonConfig = new JSONObject(jsonString).getJSONObject("clustering");

    HierarchicalAffinityPropagation hap = new HierarchicalAffinityPropagation(jsonConfig);

    ConfigParsingTestUtils.testApConfigParsing(hap);
    ConfigParsingTestUtils.testHapConfigParsing(hap);
  }

  /**
   * Test the exemplar assignment for clean source dataPoints using the hungarian algorithm in
   * {@link HierarchicalAffinityPropagation#assignDataPointsToExemplarsByHungarianMethod}.
   */
  @Test
  public void testHungarianAssignment() throws Exception {
    double nul = Double.NEGATIVE_INFINITY;

    LogicalGraph inputGraph;
    double[][] s;
    int[][] expectedClusters;
    String[] sources;
    List<String> cleanSources;
    Integer[] exemplars;
    List<EPGMVertex> assignedVertices;
    EPGMVertex v0;

    // 1. Standard Assignment by highest similarity

    s = new double[][] {
      {nul, 0.2, nul, nul, nul},
      {0.2, nul, 0.7, 0.3, 0.9},
      {nul, 0.7, nul, nul, 0.8},
      {nul, 0.3, nul, nul, 0.2},
      {nul, 0.9, 0.8, 0.2, nul}
    };

    sources = new String[] { "", "", "", "", "" };
    cleanSources = Collections.singletonList("a");
    expectedClusters = new int[][] {{0, 1, 3}, {2, 4}};
    exemplars = new Integer[] { 1, 4 };

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    assignedVertices =
      ApTestUtils.getHungarianAssignedVertices(getHap(), inputGraph, exemplars, env).collect();

    ApTestUtils.compareClusteringResult(expectedClusters, assignedVertices);

    // 2. Clean Source separation - move point 3 to other subCluster

    sources = new String[] { "a", "", "", "a", "" };
    cleanSources = Collections.singletonList("a");
    expectedClusters = new int[][] {{0, 1}, {2, 3, 4}};
    exemplars = new Integer[] { 1, 4 };

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    createNewApInstance(true);
    apConfig.setCleanSources(cleanSources);

    assignedVertices =
      ApTestUtils.getHungarianAssignedVertices(getHap(), inputGraph, exemplars, env).collect();

    ApTestUtils.compareClusteringResult(expectedClusters, assignedVertices);

    // 3. Clean Source separation - new SubCluster for point 0 (hna - hungarian not assigned)

    s = new double[][] {
      {nul, 0.2, nul, nul, nul},
      {0.2, nul, 0.7, 0.3, 0.9},
      {nul, 0.7, nul, nul, 0.8},
      {nul, 0.3, nul, nul, nul},
      {nul, 0.9, 0.8, 0.2, nul}
    };

    sources = new String[] { "a", "", "", "a", "" };
    cleanSources = Collections.singletonList("a");
    expectedClusters = new int[][] {{0}, {1, 3}, {2, 4}};
    exemplars = new Integer[] { 1, 4 };

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    createNewApInstance(true);
    apConfig.setCleanSources(cleanSources);

    assignedVertices =
      ApTestUtils.getHungarianAssignedVertices(getHap(), inputGraph, exemplars, env).collect();

    ApTestUtils.compareClusteringResult(expectedClusters, assignedVertices);

    v0 = ApTestUtils.getVertexWithIdFromCollection(0, assignedVertices);
    assertEquals("0-hna-0", v0.getPropertyValue(PropertyNames.CLUSTER_ID).getString());

    // 4. No Edge to Exemplar (point 0 has only edges to non-exemplars)

    s = new double[][] {
      {nul, nul, 0.7, 0.8, nul},
      {nul, nul, 0.7, 0.3, 0.9},
      {0.7, 0.7, nul, nul, 0.8},
      {0.8, 0.3, nul, nul, 0.2},
      {nul, 0.9, 0.8, 0.2, nul}
    };

    sources = new String[] { "a", "", "", "a", "" };
    cleanSources = Collections.singletonList("a");
    expectedClusters = new int[][] {{0}, {1, 3}, {2, 4}};
    exemplars = new Integer[] { 1, 4 };

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    createNewApInstance(true);
    apConfig.setCleanSources(cleanSources);

    assignedVertices =
      ApTestUtils.getHungarianAssignedVertices(getHap(), inputGraph, exemplars, env).collect();

    ApTestUtils.compareClusteringResult(expectedClusters, assignedVertices);

    v0 = ApTestUtils.getVertexWithIdFromCollection(0, assignedVertices);
    assertEquals("0-ne-0", v0.getPropertyValue(PropertyNames.CLUSTER_ID).getString());
  }

  @Override
  @Test
  public void testNotConvergingMaxApIterations() throws Throwable {
    LogicalGraph outputGraph;
    LogicalGraph inputGraph;

    double[][] s = {
      {nul, 0.2, nul, nul,  nul},
      {0.2, nul, 0.8, 0.3,  0.9},
      {nul, 0.8, nul, nul,  0.6},
      {nul, 0.3, nul, nul,  0.2},
      {nul, 0.9, 0.6, 0.72, nul}
    };

    String[] sources = new String[] { "", "a", "", "", "a" };
    List<String> cleanSources = Collections.singletonList("a");

    apConfig.setDampingFactor(0.8);
    apConfig.setCleanSources(cleanSources);
    // set parameters so that ap can not converge
    int maxIterations = 16;
    apConfig.setMaxApIteration(maxIterations);
    apConfig.getPreferenceConfig().setPreferenceFixValueDirtySrc(0.3);
    apConfig.getPreferenceConfig().setPreferenceFixValueCleanSrc(0.5);
    apConfig.setMaxAdaptionIteration(15);
    apConfig.setConvergenceIter(15);

    // 1) too many adaptions => create singletons for unconverged components
    apConfig.setSingletonsForUnconvergedComponents(true);
    int[][] expectedClusters = {{0}, {1}, {2}, {3}, {4}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    outputGraph = ap.execute(inputGraph);
    List<EPGMVertex> resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);

    // 2) too many adaptions => no convergence exception
    createNewApInstance(true);
    apConfig.setSingletonsForUnconvergedComponents(false);

    /* tries = (17 * 5) -1
     * => 17 tries for one raise of dmp.
     * => 5 dmp raises until everything tried.
     * => -1 because first try = 0
     */
    int expectedTries = 84;

    exceptionRule.expect(ConvergenceException.class);
    exceptionRule.expectMessage(String.format("The algorithm could not converge for a connected " +
      "component. It tried all possible parameter settings. (%d tries)", expectedTries));

    outputGraph = ap.execute(inputGraph);

    // extract the ConvergenceException from the thrown JobExecutionException
    try {
      outputGraph.getVertices().collect();  // collect, to execute the Flink job without a sink
    } catch (JobExecutionException e) {
      throw e.getCause();
    }
  }

  /**
   * Test an execution of MSCD-HAP were a component must be partitioned. In this test case, a sparse
   * similarity matrix is tested. All vertices must be labeled as {@link PropertyNames#IS_HAP_VERTEX}.
   * Global exemplars must have a {@link PropertyNames#HAP_HIERARCHY_DEPTH} of at least 1.
   */
  @Test
  public void testPartitionedExecutionSparse() throws Exception {

    double[][] s = {
      {nul, 0.8, 0.7, nul, nul, 0.6, nul, nul, 0.8, nul, nul, nul},
      {0.8, nul, 0.9, nul, nul, nul, nul, nul, nul, 0.5, nul, nul},
      {0.7, 0.9, nul, nul, nul, nul, nul, nul, 0.3, nul, 0.7, nul},
      {nul, nul, nul, nul, 0.8, 0.9, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, 0.8, nul, nul, nul, nul, nul, 0.8, nul, 0.5},
      {0.6, nul, nul, 0.9, nul, nul, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, nul, 0.5, 0.8, nul, nul, 0.9},
      {nul, nul, nul, nul, nul, nul, 0.5, nul, 0.9, nul, nul, nul},
      {0.8, nul, 0.3, nul, nul, nul, 0.8, 0.9, nul, nul, nul, nul},
      {nul, 0.5, nul, nul, 0.8, nul, nul, nul, nul, nul, 0.4, nul},
      {nul, nul, 0.7, nul, nul, nul, nul, nul, nul, 0.4, nul, 0.7},
      {nul, nul, nul, nul, 0.5, nul, 0.9, nul, nul, nul, 0.7, nul}
    };

    apConfig.setDampingFactor(0.5);
    apConfig.setNoiseDecimalPlace(-1);
    preferenceConfig.setPreferenceFixValueDirtySrc(0.4);
    preferenceConfig.setPreferenceFixValueCleanSrc(0.4);
    getHapConfig().setMaxPartitionSize(3);
    getHapConfig().setMaxHierarchyDepth(10);

    // a) test standard HAP with max-similarity Assignment

    String[] sources = new String[] { "", "", "", "", "", "", "", "", "", "", "", "" };
    List<String> cleanSources = new ArrayList<>();

    LogicalGraph inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);
    LogicalGraph outputGraph = ap.execute(inputGraph);
    List<EPGMVertex> resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkHapClusteringResult(resultVertices, 1, 1);

    // b) test MSCD-HAP with Hungarian Assignment

    sources = new String[] { "a", "b", "c", "d", "a", "b", "c", "d", "a", "b", "c", "d" };
    cleanSources = Arrays.asList("a", "b", "c", "d");

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);
    outputGraph = ap.execute(inputGraph);
    resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkHapClusteringResult(resultVertices, 1, 1);
  }

  /**
   * Test a execution of MSCD-HAP were a component must be partitioned. In this test case, a complete
   * similarity graph is tested. All vertices must be labeled as {@link PropertyNames#IS_HAP_VERTEX}.
   * Global exemplars must have a {@link PropertyNames#HAP_HIERARCHY_DEPTH} of at least 1.
   */
  @Test
  public void testPartitionedExecutionComplete() throws Exception {

    double[][] s = {
      {nul, 0.8, 0.7, 0.9, 0.8, 0.6, 0.7, 0.6, 0.8, 0.8, 0.3, 0.1},
      {0.8, nul, 0.9, 0.8, 0.5, 0.1, 0.4, 0.1, 0.9, 0.5, 0.6, 0.8},
      {0.7, 0.9, nul, 0.5, 0.1, 0.7, 0.2, 0.3, 0.3, 0.6, 0.7, 0.6},
      {0.9, 0.8, 0.5, nul, 0.8, 0.9, 0.3, 0.6, 0.9, 0.1, 0.8, 0.9},
      {0.8, 0.5, 0.1, 0.8, nul, 0.5, 0.8, 0.9, 0.7, 0.8, 0.3, 0.5},
      {0.6, 0.1, 0.7, 0.9, 0.5, nul, 0.6, 0.1, 0.7, 0.4, 0.8, 0.3},
      {0.7, 0.4, 0.2, 0.3, 0.8, 0.6, nul, 0.5, 0.8, 0.5, 0.6, 0.9},
      {0.6, 0.1, 0.3, 0.6, 0.9, 0.1, 0.5, nul, 0.9, 0.2, 0.9, 0.1},
      {0.8, 0.9, 0.3, 0.9, 0.7, 0.7, 0.8, 0.9, nul, 0.3, 0.1, 0.5},
      {0.8, 0.5, 0.6, 0.1, 0.8, 0.4, 0.5, 0.2, 0.3, nul, 0.4, 0.8},
      {0.3, 0.6, 0.7, 0.8, 0.3, 0.8, 0.6, 0.9, 0.1, 0.4, nul, 0.7},
      {0.1, 0.8, 0.6, 0.9, 0.5, 0.3, 0.9, 0.1, 0.5, 0.8, 0.7, nul}
    };

    apConfig.setDampingFactor(0.5);
    apConfig.setNoiseDecimalPlace(-1);
    preferenceConfig.setPreferenceFixValueDirtySrc(0.4);
    preferenceConfig.setPreferenceFixValueCleanSrc(0.4);
    getHapConfig().setMaxPartitionSize(3);
    getHapConfig().setMaxHierarchyDepth(10);

    // a) test standard HAP with max-similarity Assignment

    String[] sources = new String[] { "", "", "", "", "", "", "", "", "", "", "", "" };
    List<String> cleanSources = new ArrayList<>();

    LogicalGraph inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);
    LogicalGraph outputGraph = ap.execute(inputGraph);
    List<EPGMVertex> resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkHapClusteringResult(resultVertices, 1, 1);

    // b) test MSCD-HAP with Hungarian Assignment

    sources = new String[] { "a", "b", "c", "d", "a", "b", "c", "d", "a", "b", "c", "d" };
    cleanSources = Arrays.asList("a", "b", "c", "d");

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);
    outputGraph = ap.execute(inputGraph);
    resultVertices = outputGraph.getVertices().collect();

    ApTestUtils.checkHapClusteringResult(resultVertices, 1, 1);
  }
}
