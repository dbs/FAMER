/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.operators.IterativeDataSet;
import org.apache.flink.runtime.client.JobExecutionException;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.AbstractAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.AffinityPropagationTestBase;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils.ConfigParsingTestUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils.ApTestUtils;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Test class for the parallel {@link AffinityPropagationSparseDS} algorithm.
 */
public class AffinityPropagationSparseDSTest extends AffinityPropagationTestBase {

  /**
   * Create a new instance of AffinityPropagationSparseDS
   *
   * @return new instance of AffinityPropagationSparseDS
   */
  @Override
  protected AbstractAffinityPropagation getNewApInstance() {
    return new AffinityPropagationSparseDS();
  }

  /**
   * Cast {@link #ap} to {@link AffinityPropagationSparseDS}
   *
   * @return {@link #ap} as {@link AffinityPropagationSparseDS}
   */
  private AffinityPropagationSparseDS getApSparseDs() {
    return (AffinityPropagationSparseDS) ap;
  }

  /**
   * Test, whether a json config file for the {@link ApConfig} is correctly parsed.
   *
   * @throws IOException if the json config file could nor be read correctly from disk.
   * @throws JSONException if the json config file could not be parsed correctly for a certain parameter.
   */
  @Test
  public void testConfigParsing() throws IOException, JSONException {
    String jsonConfigFilePath = new File(AffinityPropagationSparseDSTest.class.getResource(
      "/parallelClustering/affinityPropagation/common/testApConfig.json").getFile()).getAbsolutePath();

    String jsonString = StringUtils.newStringUtf8(Files.readAllBytes(Paths.get(jsonConfigFilePath)));
    JSONObject jsonConfig = new JSONObject(jsonString).getJSONObject("clustering");

    AffinityPropagationSparseDS ap = new AffinityPropagationSparseDS(jsonConfig);
    ConfigParsingTestUtils.testApConfigParsing(ap);
  }

  /**
   * Test the calculation of Eta.
   * Eta[i][j] = -1 * max_{k != j}(S[i][k] + A[i][k] + Theta[i][k])
   */
  @Test
  public void apPart1OneOfNConstraintUsingRealData() throws Exception {
    // ### input
    double[][] s = {
      {0.3, 0.2, nul, nul, nul},
      {0.2, 0.5, 0.8, 0.3, nul},
      {nul, 0.8, 0.3, nul, 0.6},
      {nul, 0.3, nul, 0.3, 0.2},
      {nul, nul, 0.6, 0.2, 0.5}
    };

    double[][] a = {
      {0, -0.025, nul, nul, nul},
      {0, 0.05, -0.125, 0.0, nul},
      {nul, -0.075, 0.05, nul, -0.025},
      {nul, -0.025, nul, 0, -0.025},
      {nul, nul, -0.075, 0, 0}
    };

    double[][] theta = {
      {0, 0, nul, nul, nul},
      {0, 0, -0.1, 0, nul},
      {nul, 0, 0, nul, 0},
      {nul, 0, nul, 0, 0},
      {nul, nul, -0.3, 0, 0}
    };

    List<ApMultiMatrixCell> inputList = ApTestUtils.createApMultiMatrixDataSet(
      nul, s, s, a, null, null, theta, null, apConfig);

    // ### intermediate result

    // Beta = S + A + Theta:
    // 0,3    0,175     nul       nul     nul
    // 0,2    0,55      0,575     0,3     nul
    // nul    0,725     0,35      nul     0,575
    // nul    0,275     nul       0,3     0,175
    // nul    nul       0,225     0,2     0,5

    // ### expected output

    double[][] expectedEta = {
      {-0.175, -0.3, nul, nul, nul},
      {-0.575, -0.575, -0.55, -0.575, nul},
      {nul, -0.575, -0.725, nul, -0.725},
      {nul, -0.3, nul, -0.275, -0.3},
      {nul, nul, -0.5, -0.5, -0.225}
    };

    DataSet<ApMultiMatrixCell> inputDataSet = env.fromCollection(inputList);
    List<ApMultiMatrixCell> result = getApSparseDs().apPart1OneOfNConstraint(inputDataSet).collect();

    for (ApMultiMatrixCell cell : result) {
      int i = ApTestUtils.gradoopIdToInteger(cell.getRowId());
      int j = ApTestUtils.gradoopIdToInteger(cell.getColumnId());
      assertEquals(expectedEta[i][j], cell.getEta(), 0.00001);
    }

    assertEquals(15, result.size());
  }

  /**
   * Test the calculation of Eta.
   * Eta[i][j] = -1 * max_{k != j}(S[i][k] + A[i][k] + Theta[i][k])
   */
  @Test
  public void apPart1OneOfNConstraintUsingPlayData() throws Exception {
    // ### input
    double[][] s = {
      {3, 2, 1, nul, nul},
      {5, 1, 4, 2, 3}
    };

    double[][] a = {
      {1, 1, 1, nul, nul},
      {1, 1, 1, 1, 1}
    };

    double[][] theta = {
      {2, 2, 2, nul, nul},
      {2, 2, 2, 2, 2}
    };

    List<ApMultiMatrixCell> inputList = ApTestUtils.createApMultiMatrixDataSet(
      nul, s, s, a, null, null, theta, null, apConfig);

    // ### intermediate result

    // Beta = S + A + Theta:
    // 6, 5, 4, nul, nul
    // 8, 4, 7, 5, 6

    // ### expected output

    double[][] expectedEtaForSparse = {
      {-5, -6, -6, nul, nul},
      {-7, -8, -8, -8, -8}
    };

    DataSet<ApMultiMatrixCell> inputDataSet = env.fromCollection(inputList);
    List<ApMultiMatrixCell> result = getApSparseDs().apPart1OneOfNConstraint(inputDataSet).collect();

    for (ApMultiMatrixCell cell : result) {
      int i = ApTestUtils.gradoopIdToInteger(cell.getRowId());
      int j = ApTestUtils.gradoopIdToInteger(cell.getColumnId());
      assertEquals("eta value is incorrect",
        expectedEtaForSparse[i][j], cell.getEta(), 0.00001);
    }

    assertEquals("number of ApMultiMatrix-Cells in the result is incorrect", 8, result.size());
  }

  /**
   * Test the calculation of Theta.
   * Gamma_kj = S_ij + A_ij + Eta_ij
   * Theta_ij = min[0, -1 * max_{k != i}(Gamma_kj)]
   * => -1 * maximal column value of all other cells of that column (of the same clean source!), only negative
   * values taken
   */
  @Test
  public void apPart2CleanSourceConstraint() throws Exception {
    // ### input
    double[][] s = {
      {3, 5, 10},
      {2, -5, 2},
      {6, 8, 10},
      {nul, -3, 3},
      {nul, 7, 4},
      {3, 2, 1}
    };

    double[][] eta = {
      {1, 1, 1},
      {1, 1, 1},
      {1, 1, 1},
      {nul, 1, 1},
      {nul, 1, 1},
      {1, 1, 1}
    };

    double[][] a = {
      {-4, -4, -4},
      {-4, -4, -4},
      {-4, -4, -4},
      {nul, -4, -4},
      {nul, -4, -4},
      {-4, -4, -4}
    };

    String[] rowCleanSrc = {"b", "a", "b", "", "a", "b"};

    // ### intermediate result

    // Gamma = S + Eta + A:
    // 0       2       7      b
    // -1     -8      -1          a
    // 3       5       7      b
    // nul    -6       0              -
    // nul    4        1          a
    // 0     -1       -2      b

    // ### expected output

    double[][] expectedTheta = {
      {-3, -5, -7},
      {0, -4, -1},
      {0, -2, -7},
      {nul, 0, 0},
      {nul, 0, 0},
      {-3, -5, -7}
    };

    List<ApMultiMatrixCell> inputList = ApTestUtils
      .createApMultiMatrixDataSet(nul, s, s, a, null, eta, null, rowCleanSrc, apConfig);

    DataSet<ApMultiMatrixCell> inputDataSet = env.fromCollection(inputList);
    List<ApMultiMatrixCell> result = getApSparseDs().apPart2CleanSourcesConstraint(inputDataSet).collect();

    for (ApMultiMatrixCell cell : result) {
      int i = ApTestUtils.gradoopIdToInteger(cell.getRowId());
      int j = ApTestUtils.gradoopIdToInteger(cell.getColumnId());
      assertEquals("theta value is incorrect", expectedTheta[i][j], cell.getTheta(), 0.00001);
    }

    assertEquals("number of ApMultiMatrix-Cells in the result is incorrect", 16, result.size());
  }

  /**
   * Test the calculation of A and R.
   * R_ij = S_ij + Eta_ij + Theta_ij
   *
   * <p>
   * A_ij = Sum_{k != j}( max(0, R_kj )                           for diagonal (i == j)
   * <ul>
   *   <li>=> Column-Sum of positive elements without self</li>
   * </ul>
   *
   * <p>
   * A_ij = min[0,  R_jj + Sum_{k != i,j}(max(0, R_kj))]          for non-diagonal (i != j)
   * <ul>
   *   <li>=> Column-Sum of positive elements without self and diagonal  + diagonal(also negative)</li>
   *   <li>=> take only values <= 0</li>
   * </ul>
   *
   * <p>
   * The tested configuration tests all the special cases of A and R calculation:
   * <ul>
   *   <li>damping</li>
   *   <li>A results > 0</li>
   *   <li>A results < 0</li>
   *   <li>positive diagonal value</li>
   *   <li>negative diagonal value</li>
   * </ul>
   */
  @Test
  public void apPart3ExemplarConsistencyConstraint() throws Exception {
    // ### input
    double[][] s = {
      {3, 5, 1},
      {2, -5, 2},
      {-6, -4, -30},
      {nul, -3, 3},
      {nul, -7, 4}
    };

    double[][] eta = {
      {1, 1, 1},
      {1, 1, 1},
      {1, 1, 1},
      {nul, 1, 1},
      {nul, 1, 1}
    };

    double[][] theta = {
      {2, 2, 2},
      {2, 2, 2},
      {2, 2, 2},
      {nul, 2, 2},
      {nul, 2, 2}
    };

    double[][] r = {
      {3, 3, 3},
      {3, -3, 3},
      {-3, -3, -3},
      {nul, 0, 3},
      {nul, -3, -3}
    };

    double[][] a = {
      {-4, -4, -4},
      {-4, -4, -4},
      {-4, -4, -4},
      {nul, -4, -4},
      {nul, -4, -4}
    };

    List<ApMultiMatrixCell> inputList = ApTestUtils.createApMultiMatrixDataSet(
      nul, s, s, a, r, eta, theta, null, apConfig);

    // ### intermediate result

    // R = S + Eta + Theta:          damped
    // 6, 8,  4                         1.2,   1.6      0.8
    // 5, -2,  5                          1     -0.4      1
    // -3, -1, -27                        -0.6    -0.2     -5.4
    // nul, 0,  6                        nul    0         1.2
    // nul, -4,  7                        nul    -0.8     1.4

    // ### expected output

    // damping R = oldR * dampingFactor + newR * (1-dampingFactor)
    double[][] expectedR = {
      {3.6, 4, 3.2},
      {3.4, -2.8, 3.4},
      {-3, -2.6, -7.8},
      {nul, 0, 3.6},
      {nul, -3.2, -1}
    };

    // ### intermediate result

    // A                        damped
    // 3.4    -2.8    -0.8         0.68    -0.56     -0.16
    // 0     4       -1              0     0.8      -0.2
    // 0     0     10,2             0      0     2.04
    // nul    0     -1,2           nul     0     -0.24
    // nul    0      0           nul     0       0

    // damping A = oldA * dampingFactor + newA * (1-dampingFactor)
    // -4 * 0.8 = -3.2

    // ### expected output

    double[][] expectedA = {
      {-2.52, -3.76, -3.36},
      {-3.2, -2.4, -3.4},
      {-3.2, -3.2, -1.16},
      {nul, -3.2, -3.44},
      {nul, -3.2, -3.2}
    };

    DataSet<ApMultiMatrixCell> inputDataSet = env.fromCollection(inputList);
    apConfig.setDampingFactor(0.8);
    List<ApMultiMatrixCell> result =
      getApSparseDs().apPart3ExemplarConsistencyConstraint(inputDataSet).collect();

    for (ApMultiMatrixCell cell : result) {
      int i = ApTestUtils.gradoopIdToInteger(cell.getRowId());
      int j = ApTestUtils.gradoopIdToInteger(cell.getColumnId());
      assertEquals("rho value is incorrect", expectedR[i][j], cell.getR(), 0.00001);
      assertEquals("alpha value is incorrect", expectedA[i][j], cell.getA(), 0.00001);
    }

    assertEquals("number of ApMultiMatrix-Cells in the result is incorrect", 13, result.size());
  }

  /**
   * Test the execution of the three AP message calculation steps together in a loop of 5 iterations.
   */
  @Test
  public void testLoop() throws Exception {
    double[][] s = {
      {0.45, 0.2, nul, nul, nul},
      {0.2, 0.45, 0.8, 0.3, nul},
      {nul, 0.8, 0.45, nul, 0.6},
      {nul, 0.3, nul, 0.45, 0.2},
      {nul, nul, 0.6, 0.2, 0.45}
    };

    String[] cleanSources = {"", "a", "", "", "a"};

    int loops = 5;

    double[][] expectedAlpha = {
      {0,     -0.005667,  nul,        nul,    nul},
      {0,     0.070157,   -0.111867,  0,      nul},
      {nul,   -0.072359,  0.066868,   nul,    -0.002458},
      {nul,   -0.005667,  nul,        0,      -0.002458},
      {nul,   nul,        -0.044999,  0,      0}
    };

    double[][] expectedRho = {
      {0.172654,    -0.168080,  nul,        nul,        nul},
      {-0.289858,   -0.121778,  0.128111,   -0.222626,  nul},
      {nul,         0.136922,   -0.210764,  nul,        -0.109916},
      {nul,         -0.100848,  nul,        0.105422,   -0.168080},
      {nul,         nul,        -0.081141,  -0.180368,  0.088020}
    };

    List<ApMultiMatrixCell> inputList = ApTestUtils
      .createApMultiMatrixDataSet(nul, s, s, null, null, null, null, cleanSources, apConfig);

    DataSet<ApMultiMatrixCell> inputDataSet = env.fromCollection(inputList);
    apConfig.setDampingFactor(0.8);

    IterativeDataSet<ApMultiMatrixCell> loop = inputDataSet.iterate(loops);
    DataSet<ApMultiMatrixCell> newMatrix = getApSparseDs().apPart1OneOfNConstraint(loop);
    newMatrix = getApSparseDs().apPart2CleanSourcesConstraint(newMatrix);
    newMatrix = getApSparseDs().apPart3ExemplarConsistencyConstraint(newMatrix);
    DataSet<ApMultiMatrixCell> finalMatrix = loop.closeWith(newMatrix);

    List<ApMultiMatrixCell> result = finalMatrix.collect();

    for (ApMultiMatrixCell cell : result) {
      int i = ApTestUtils.gradoopIdToInteger(cell.getRowId());
      int j = ApTestUtils.gradoopIdToInteger(cell.getColumnId());
      assertEquals("alpha value incorrect", expectedAlpha[i][j], cell.getA(), 0.00001);
      assertEquals("rho value incorrect", expectedRho[i][j], cell.getR(), 0.00001);
    }

    assertEquals("number of ApMultiMatrix-Cells in the result is incorrect", 15, result.size());
  }

  @Override
  @Test
  public void testNotConvergingAllPossibleAdaptions() throws Throwable {
    LogicalGraph outputGraph;
    LogicalGraph inputGraph;
    String[] sources;
    List<String> cleanSources;

    double[][] s = {
      {0.8, 0.81, 0.8, 0.8, 0.8},
      {0.81, 0.8, 0.8, 0.8, 0.8},
      {0.8, 0.8, 0.8, 0.8, 0.8},
      {0.8, 0.8, 0.8, 0.8, 0.81},
      {0.8, 0.8, 0.8, 0.81, 0.8}
    };

    sources = new String[] { "a", "", "a", "", "" };
    cleanSources = Collections.singletonList("a");

    apConfig.setDampingFactor(0.5);
    apConfig.setNoiseDecimalPlace(-1);
    preferenceConfig.setPreferenceFixValueDirtySrc(0.4);
    preferenceConfig.setPreferenceFixValueCleanSrc(0.5);
    preferenceConfig.setPreferenceAdaptionStep(0.2);

    apConfig.setMaxApIteration(2000);
    apConfig.setMaxAdaptionIteration(3);
    apConfig.setConvergenceIter(3);
    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    // 1) too many adaptions => create singletons for unconverged components
    apConfig.setSingletonsForUnconvergedComponents(true);
    int[][] expectedClusters = {{0}, {1}, {2}, {3}, {4}};

    outputGraph = ap.execute(inputGraph);
    List<EPGMVertex> resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);

    // 2) too many adaptions => no convergence exception
    createNewApInstance(true);
    apConfig.setSingletonsForUnconvergedComponents(false);

    /* tries = (5 * 5) -1
     * => 5 tries for one raise of dmp.
     * => 5 dmp raises until everything tried.
     * => -1 because first try = 0
     */
    int expectedTries = 24;

    apConfig.setCleanSources(cleanSources);

    outputGraph = ap.execute(inputGraph);

    exceptionRule.expect(ConvergenceException.class);
    exceptionRule.expectMessage(String.format(
      "The algorithm could not converge for a connected component. " +
        "It tried all possible parameter settings. (%d tries)",
      expectedTries));

    try {
      outputGraph.getVertices().collect();
    } catch (JobExecutionException e) {
      throw e.getCause();
    }
  }
}
