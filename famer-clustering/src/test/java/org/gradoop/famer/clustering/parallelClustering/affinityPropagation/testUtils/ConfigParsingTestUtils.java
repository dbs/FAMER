/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.AbstractAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.PreferenceConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.dataStuctures.HapConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.dataStuctures.HapExemplarAssignmentStrategy;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

/**
 * Utility class to provide methods for the unit testing of the parsing of the {@link ApConfig}.
 */
public class ConfigParsingTestUtils {

  /**
   * Do assertions for all parameters in {@link ApConfig} of the provided AP instance to be equal to the
   * json config in the testApConfig.json resources file.
   *
   * @param ap instance of {@link AbstractAffinityPropagation}
   */
  public static void testApConfigParsing(AbstractAffinityPropagation ap) {

    ApConfig apConfig = ap.getApConfig();
    PreferenceConfig preferenceConfig = apConfig.getPreferenceConfig();

    assertTrue(ap.isEdgesBiDirected());
    assertEquals(ap.getClusteringOutputType(), ClusteringOutputType.GRAPH_COLLECTION);
    assertEquals(ap.getMaxIteration(), 123456);

    assertEquals("maxApIteration incorrectly parsed from json.",
      999, apConfig.getMaxApIteration());
    assertEquals("maxAdaptionIteration incorrectly parsed from json.",
      888, apConfig.getMaxAdaptionIteration());
    assertEquals("convergenceIter incorrectly parsed from json.",
      777, apConfig.getConvergenceIter());
    assertEquals("dampingFactor incorrectly parsed from json.",
      0.66, apConfig.getDampingFactor(), 0);
    assertEquals("dampingAdaptionStep incorrectly parsed from json.",
      0.11, apConfig.getDampingAdaptionStep(), 0);
    assertEquals("allSameSimClusteringThreshold incorrectly parsed from json.",
      0.55, apConfig.getAllSameSimClusteringThreshold(), 0);
    assertEquals("noieseDecimalPlace incorrectly parsed from json.",
      4, apConfig.getNoiseDecimalPlace());
    assertTrue("singletonsForUnconvergedComponents incorrectly parsed from json.",
      apConfig.isSingletonsForUnconvergedComponents());
    assertTrue("allSourcesClean incorrectly parsed from json.",
      apConfig.isAllSourcesClean());
    assertEquals("sourceDirtinessVertexProperty incorrectly parsed from json.",
      "abc", apConfig.getSourceDirtinessVertexProperty());
    assertArrayEquals("cleanSources incorrectly parsed from json.",
      new String[] {"def", "ghi"}, apConfig.getCleanSources().toArray());

    assertFalse("preferenceUseMinSimilarityDirtySrc incorrectly parsed from json.",
      preferenceConfig.isPreferenceUseMinSimilarityDirtySrc());
    assertTrue("preferenceUseMinSimilarityCleanSrc incorrectly parsed from json.",
      preferenceConfig.isPreferenceUseMinSimilarityCleanSrc());
    assertEquals("preferenceFixValueDirtySrc incorrectly parsed from json.",
      0.44, preferenceConfig.getPreferenceFixValueDirtySrc(), 0);
    assertEquals("preferenceFixValueCleanSrc incorrectly parsed from json.",
      0.33, preferenceConfig.getPreferenceFixValueCleanSrc(), 0);
    assertEquals("preferencePercentileDirtySrc incorrectly parsed from json.",
      22, preferenceConfig.getPreferencePercentileDirtySrc());
    assertEquals("preferencePercentileCleanSrc incorrectly parsed from json.",
      11, preferenceConfig.getPreferencePercentileCleanSrc());
    assertEquals("preferenceAdaptionStep incorrectly parsed from json.",
      0.011, preferenceConfig.getPreferenceAdaptionStep(), 0);
  }

  /**
   * Do assertions for all parameters in {@link HapConfig} of the provided AP instance to be equal to the
   * json config in the testHapConfig.json resources file.
   *
   * @param hap instance of {@link HierarchicalAffinityPropagation}
   */
  public static void testHapConfigParsing(HierarchicalAffinityPropagation hap) {

    HapConfig hapConfig = hap.getHapConfig();

    assertEquals("maxPartitionSize incorrectly parsed from json.",
      123, hapConfig.getMaxPartitionSize());
    assertEquals("maxHierarchyDepth incorrectly parsed from json.",
      456, hapConfig.getMaxHierarchyDepth());
    assertEquals("hapExemplarAssignmentStrategy incorrectly parsed from json.",
      HapExemplarAssignmentStrategy.HIGHEST_SIMILARITY, hapConfig.getHapExemplarAssignmentStrategy());
  }
}
