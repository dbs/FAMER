/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation;

import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.runtime.client.JobExecutionException;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.AbstractAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.PreferenceConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils.ApTestUtils;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Abstract test class to execute the same unit tests for all implementations of MSCD-Affinity-Propagation.
 */
public abstract class AffinityPropagationTestBase {

  /**
   * Flink Execution Environment used for the tests
   */
  protected ExecutionEnvironment env;

  /**
   * Instance of Sparse MSCD-AP used for the tests.
   */
  protected AbstractAffinityPropagation ap;

  /**
   * Configurations for the Sparse MSCD-AP clustering algorithm.
   */
  protected ApConfig apConfig;

  /**
   * Configuration of the preference parameter for the Sparse MSCD-AP clustering algorithm
   */
  protected PreferenceConfig preferenceConfig;

  /**
   * Null value for sparse matrices to mark an edge as not present.
   */
  protected final double nul = Double.NEGATIVE_INFINITY;

  /**
   * Initialize the execution environment, the AP instance and its configuration
   */
  @Before
  public void setUp() {
    /* CollectionsEnvironment allows faster execution of unit tests, but may does not behave always like
     * LocalEnvironment. Switch to LocalEnvironment for testing as in production environment. For automated
     * tests keep CollectionsEnvironment. All the tests were successfully executed on both.
     */
    env = ExecutionEnvironment.createCollectionsEnvironment();
    createNewApInstance(false);
  }

  /**
   * Create a new instance of the derived Flink Affinity Propagation implementation using the old configuration or a
   * new default one.
   *
   * @param keepConfig if true, use the old {@link #apConfig}. Otherwise use a default one.
   */
  protected void createNewApInstance(boolean keepConfig) {
    if (keepConfig) {
      ap = getNewApInstance();
      ap.setApConfig(apConfig);
    } else {
      ap = getNewApInstance();
      apConfig = ap.getApConfig();
      preferenceConfig = apConfig.getPreferenceConfig();
    }
  }

  /**
   * Create a new instance of the derived Flink Affinity Propagation implementation
   *
   * @return new instance of the derived Flink Affinity Propagation implementation
   */
  protected abstract AbstractAffinityPropagation getNewApInstance();

  /**
   * Used to test, whether a method throws an expected exception.
   */
  @Rule
  public ExpectedException exceptionRule = ExpectedException.none();

  /**
   * Test AP to throw a {@link ConvergenceException} when it can't converge because
   * {@link ApConfig#getMaxApIteration()} is reached.
   */
  @Test
  public void testNotConvergingMaxApIterations() throws Throwable {
    LogicalGraph outputGraph;
    LogicalGraph inputGraph;

    double[][] s = {
      {nul, 0.2, nul, nul,  nul},
      {0.2, nul, 0.8, 0.3,  0.9},
      {nul, 0.8, nul, nul,  0.6},
      {nul, 0.3, nul, nul,  0.2},
      {nul, 0.9, 0.6, 0.72, nul}
    };

    String[] sources = new String[] { "", "a", "", "", "a" };
    List<String> cleanSources = Collections.singletonList("a");

    apConfig.setDampingFactor(0.8);
    apConfig.setCleanSources(cleanSources);
    // set parameters so that ap can not converge
    int maxIterations = 16;
    apConfig.setMaxApIteration(maxIterations);
    apConfig.getPreferenceConfig().setPreferenceFixValueDirtySrc(0.3);
    apConfig.getPreferenceConfig().setPreferenceFixValueCleanSrc(0.5);
    apConfig.setMaxAdaptionIteration(15);
    apConfig.setConvergenceIter(15);

    exceptionRule.expect(ConvergenceException.class);
    exceptionRule.expectMessage("The algorithm did not converge for all components in " +
      maxIterations + " iterations");

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    outputGraph = ap.execute(inputGraph);

    // extract the ConvergenceException from the thrown JobExecutionException
    try {
      outputGraph.getVertices().collect();  // collect, to execute the Flink job without a sink
    } catch (JobExecutionException e) {
      throw e.getCause();
    }
  }

  /**
   * Test AP to throw a {@link ConvergenceException} when it can't converge because all possible parameter
   * values were tried.
   */
  @Test
  public void testNotConvergingAllPossibleAdaptions() throws Throwable {
    LogicalGraph outputGraph;
    LogicalGraph inputGraph;
    double[][] s;
    String[] sources;
    List<String> cleanSources;

    s = new double[][] {
      {nul, 0.2, nul},
      {0.2, nul, 0.8},
      {nul, 0.8, nul}
    };

    sources = new String[] { "", "", "" };
    cleanSources = new ArrayList<>();

    apConfig.setDampingFactor(0.5);
    apConfig.setNoiseDecimalPlace(-1);
    preferenceConfig.setPreferenceFixValueDirtySrc(0.4);
    preferenceConfig.setPreferenceFixValueCleanSrc(0.5);
    preferenceConfig.setPreferenceAdaptionStep(0.05);

    apConfig.setMaxApIteration(2000);
    apConfig.setMaxAdaptionIteration(3);
    apConfig.setConvergenceIter(3);

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);

    // 1) too many adaptions => create singletons for unconverged components
    apConfig.setSingletonsForUnconvergedComponents(true);
    int[][] expectedClusters = {{0}, {1}, {2}};

    outputGraph = ap.execute(inputGraph);
    List<EPGMVertex> resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);

    // 2) too many adaptions => no convergence exception
    createNewApInstance(true);
    apConfig.setSingletonsForUnconvergedComponents(false);

    /* tries = (19 * 5) -1
     * => 19 tries for one raise of dmp.
     * => 5 dmp raises until everything tried.
     * => -1 because first try = 0
     */
    int expectedTries = 94;

    outputGraph = ap.execute(inputGraph);

    exceptionRule.expect(ConvergenceException.class);
    exceptionRule.expectMessage(String.format(
      "The algorithm could not converge for a connected component. " +
        "It tried all possible parameter settings. (%d tries)",
      expectedTries));

    // extract the ConvergenceException from the thrown JobExecutionException
    try {
      outputGraph.getVertices().collect();  // collect, to execute the Flink job without a sink
    } catch (JobExecutionException e) {
      throw e.getCause();
    }
  }

  /**
   * Test the MSCD-Affinity-Propagation clustering for all implementations by some minimal examples.
   */
  @Test
  public void testMscdApClusteringWithMinimalExamples() throws Exception {

    LogicalGraph outputGraph;
    List<EPGMVertex> resultVertices;
    LogicalGraph inputGraph;

    apConfig.setDampingFactor(0.8);
    apConfig.getPreferenceConfig().setPreferenceFixValueDirtySrc(0.45);
    apConfig.getPreferenceConfig().setPreferenceFixValueCleanSrc(0.6);

    // 1. simple real world example

    double[][] s = {
      {nul, 0.9880952380952381, 0.7404177338161265, nul, nul, nul},
      {0.9880952380952381, nul, 0.7404177338161265, nul, nul, nul},
      {0.7404177338161265, 0.7404177338161265, nul, nul, nul, nul},
      {nul, nul, nul, nul, 0.9876718824087245, nul},
      {nul, nul, nul, 0.9876718824087245, nul, nul},
      {nul, nul, nul, nul, nul, nul}
    };

    String[] sources = {"", "", "", "", "", ""};
    List<String> cleanSources = new ArrayList<>();
    int[][] expectedClusters = {{0, 1, 2}, {3, 4}, {5}};


    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);
    outputGraph = ap.execute(inputGraph);
    resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);

    // 2. test sparse matrix without source information

    s = new double[][] {
      {nul, 0.2, nul, nul, nul},
      {0.2, nul, 0.8, 0.3, 0.9},
      {nul, 0.8, nul, nul, 0.6},
      {nul, 0.3, nul, nul, 0.2},
      {nul, 0.9, 0.6, 0.2, nul}
    };

    sources = new String[] { "", "", "", "", "" };
    cleanSources = new ArrayList<>();
    expectedClusters = new int[][] {{0}, {1, 2, 4}, {3}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    createNewApInstance(true);
    apConfig.setCleanSources(cleanSources);
    outputGraph = ap.execute(inputGraph);
    resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);

    // 3. test sparse matrix including source information

    sources = new String[] { "", "a", "", "", "a" };
    cleanSources = Collections.singletonList("a");
    expectedClusters = new int[][] {{0}, {1, 2}, {3}, {4}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    createNewApInstance(true);
    apConfig.setCleanSources(cleanSources);
    outputGraph = ap.execute(inputGraph);
    resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);

    // 4. test singleton generated by clean source constraint

    s = new double[][] {
      {nul, 0.9, nul, nul, 0.1},
      {0.9, nul, 0.8, 0.3, 0.9},
      {nul, 0.8, nul, nul, 0.1},
      {nul, 0.3, nul, nul, 0.2}, // row 3 has only sims to cols of the same source
      {0.1, 0.9, 0.1, 0.2, nul}
    };

    sources = new String[] { "", "a", "", "a", "a" };
    cleanSources = Collections.singletonList("a");
    expectedClusters = new int[][] {{0, 1, 2}, {3}, {4}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    createNewApInstance(true);
    apConfig.setCleanSources(cleanSources);
    outputGraph = ap.execute(inputGraph);
    resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);
  }

  /**
   * Test the clustering of multiple connected components.
   */
  @Test
  public void testClusteringMultipleComponents() throws Exception {
    LogicalGraph outputGraph;
    List<EPGMVertex> resultVertices;
    LogicalGraph inputGraph;
    double[][] s;
    String[] sources;
    List<String> cleanSources;
    int[][] expectedClusters;

    s = new double[][] {
      {nul, 0.5, 0.8, nul, nul, nul, nul},
      {0.5, nul, nul, 0.9, nul, nul, nul},
      {0.8, nul, nul, 0.7, nul, nul, nul},
      {nul, 0.9, 0.7, nul, nul, nul, nul},
      {nul, nul, nul, nul, nul, 0.8, 0.9},
      {nul, nul, nul, nul, 0.8, nul, 0.7},
      {nul, nul, nul, nul, 0.9, 0.7, nul}
    };

    // Std-AP

    sources = new String[] { "", "", "", "", "", "", "" };
    cleanSources = new ArrayList<>();
    expectedClusters = new int[][] {{0, 2}, {1, 3}, {4, 5, 6}};

    apConfig.setDampingFactor(0.5);
    apConfig.setNoiseDecimalPlace(-1);
    preferenceConfig.setPreferenceFixValueDirtySrc(0.4);
    preferenceConfig.setPreferenceFixValueCleanSrc(0.9);

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    apConfig.setCleanSources(cleanSources);
    outputGraph = ap.execute(inputGraph);
    resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);

    // MSCD-AP

    sources = new String[] { "", "", "", "", "", "a", "a" };
    cleanSources = new ArrayList<>();
    cleanSources.add("a");
    expectedClusters = new int[][] {{0, 2}, {1, 3}, {5}, {4, 6}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    createNewApInstance(true);
    apConfig.setCleanSources(cleanSources);
    outputGraph = ap.execute(inputGraph);
    resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);
  }

  /**
   * If all similarities of a connectedComponent are all equal then AP can not be executed for this component.
   * The following behavior is expected:
   * <ul>
   *   <li>when the similarity < $allEqualSimThreshold : put each entity in a separated cluster</li>
   *   <li>else: Put all dirtySource entities in the same cluster. Put all entities from the same clean
   *   source in a separated cluster, but put them together with other entities of different sources.</li>
   * </ul>
   */
  @Test
  public void testAllEqualComponents() throws Exception {
    /* tested:
     - full cluster > threshold:  0,1,2  => similarities of each to each > threshold
     - sparse cluster > threshold: 3,4,5  => similarity 4-5 is null
     - full cluster < threshold:  6,7,8  => similarities of each to each < threshold
     - sparse cluster < threshold: 9,10,11  => similarity 9-11 is null
     */

    apConfig.setAllSameSimClusteringThreshold(0.7);
    apConfig.setDampingFactor(0.8);
    apConfig.getPreferenceConfig().setPreferenceFixValueDirtySrc(0.45);
    apConfig.getPreferenceConfig().setPreferenceFixValueCleanSrc(0.45);

    double[][] s = {
      {nul, 0.7, 0.7, nul, nul, nul, nul, nul, nul, nul, nul, nul},
      {0.7, nul, 0.7, nul, nul, nul, nul, nul, nul, nul, nul, nul},
      {0.7, 0.7, nul, nul, nul, nul, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, nul, 0.8, 0.8, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, 0.8, nul, nul, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, 0.8, nul, nul, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, nul, 0.5, 0.5, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, 0.5, nul, 0.5, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, 0.5, 0.5, nul, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, nul, nul, nul, nul, 0.4, nul},
      {nul, nul, nul, nul, nul, nul, nul, nul, nul, 0.4, nul, 0.4},
      {nul, nul, nul, nul, nul, nul, nul, nul, nul, nul, 0.4, nul}
    };

    // 1. test without clean sources
    String[] sources = { "", "", "", "", "", "", "", "", "", "", "", "" };
    List<String> cleanSources = new ArrayList<>();
    int[][] expectedClusters = {{0, 1, 2}, {3, 4, 5}, {6}, {7}, {8}, {9}, {10}, {11}};

    LogicalGraph inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);

    apConfig.setCleanSources(cleanSources);

    LogicalGraph outputGraph = ap.execute(inputGraph);
    List<EPGMVertex> resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);

    // 2. test with clean sources
    sources = new String[] { "a", "", "a", "b", "b", "", "", "", "", "", "", "" };
    cleanSources = Arrays.asList("a", "b");
    expectedClusters = new int[][] {{0, 1}, {2}, {3, 5}, {4}, {6}, {7}, {8}, {9}, {10}, {11}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    createNewApInstance(true);
    apConfig.setCleanSources(cleanSources);

    outputGraph = ap.execute(inputGraph);
    resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);

    // 3. Test mixed with singletons and not allEqualComponents
    s = new double[][] {
      {nul, 0.7, 0.7, nul, nul, nul, nul, nul},
      {0.7, nul, 0.7, nul, nul, nul, nul, nul},
      {0.7, 0.7, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, nul, 0.9, 0.8, nul, nul},
      {nul, nul, nul, 0.9, nul, nul, nul, nul},
      {nul, nul, nul, 0.8, nul, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, nul, nul},
      {nul, nul, nul, nul, nul, nul, nul, nul}
    };

    sources = new String[] { "", "", "", "", "", "", "a", "" };
    cleanSources = Collections.singletonList("a");
    expectedClusters = new int[][] {{0, 1, 2}, {3, 4, 5}, {6}, {7}};

    inputGraph = ApTestUtils.getTestGraph(s, sources, nul, env);
    createNewApInstance(true);
    apConfig.setCleanSources(cleanSources);

    outputGraph = ap.execute(inputGraph);
    resultVertices = outputGraph.getVertices().collect();
    ApTestUtils.compareClusteringResult(expectedClusters, resultVertices);
  }
}
