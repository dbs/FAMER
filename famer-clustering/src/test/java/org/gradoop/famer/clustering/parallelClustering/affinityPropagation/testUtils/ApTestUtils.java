/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.testUtils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.HashBiMap;
import com.google.common.collect.Multimap;
import com.google.common.collect.Table;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple8;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.graph.Vertex;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.EdgeToSourceIdTargetIdSimValue;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms.AbstractMscdAffinityPropagationSeq;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.AffinityPropagationSparseGelly;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.ApVertexValueTuple;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * Utility class to provide methods for the unit testing of (MSCD-) Affinity Propagation clustering.
 */
public class ApTestUtils {

  /**
   * Creates a dataSet of {@link ApMultiMatrixCell}s for the input matrices.
   *
   * @param nullValue value for cells ij, where no edge is present between dataPoint i and j. No
   *                  {@link ApMultiMatrixCell} is generated for nullValue cells.
   * @param nullRef the input matrix, that contains the null values. THe cells of this matrix are compared
   *                to the nullValue. Only if a value is present, a {@link ApMultiMatrixCell} is generated.
   * @param s similarity matrix.
   * @param a ALPHA matrix
   * @param r RHO matrix
   * @param eta ETA matrix
   * @param theta THETA matrix
   * @param rowCleanSrc vector of clean source names for each row
   * @return dataSet of {@link ApMultiMatrixCell}s
   */
  public static List<ApMultiMatrixCell> createApMultiMatrixDataSet(double nullValue, double[][] nullRef,
    double[][] s, double[][] a, double[][] r, double[][] eta, double[][] theta, String[] rowCleanSrc,
    ApConfig apConfig) {

    int nrRows = nullRef.length;
    int nrColumns = nullRef[0].length;

    List<ApMultiMatrixCell> exampleDataSet = new ArrayList<>();

    for (int i = 0; i < nrRows; i++) {
      for (int j = 0; j < nrColumns; j++) {
        if (nullRef[i][j] != nullValue) {  // sparse matrix without cells for null values
          GradoopId iId = integerToGradoopId(i);
          GradoopId jId = integerToGradoopId(j);
          String cleanSrc = "";
          if (rowCleanSrc != null) {
            cleanSrc = rowCleanSrc[i];
          }

          exampleDataSet.add(
            new ApMultiMatrixCell(iId, jId, getNullableMatrixValue(s, i, j), getNullableMatrixValue(a, i, j),
              getNullableMatrixValue(r, i, j), getNullableMatrixValue(eta, i, j),
              getNullableMatrixValue(theta, i, j), cleanSrc, apConfig.getDampingFactor()));
        }
      }
    }

    return exampleDataSet;
  }

  /**
   * Get a cell value of a nullable matrix.
   *
   * @param matrix nullable matrix
   * @param i row
   * @param j column
   * @return The value of cell ij, if matrix is not null. Value of 0 otherwise.
   */
  public static double getNullableMatrixValue(double[][] matrix, int i, int j) {
    if (matrix == null) {
      return 0;
    } else {
      return matrix[i][j];
    }
  }

  /**
   * Transforms an integer value to a readable {@link GradoopId}. For example a value of 5 is transformed to
   * '000000000000000000000005'.
   *
   * @param value integer ID
   * @return GradoopId, representing the integer value.
   */
  public static GradoopId integerToGradoopId(int value) {
    StringBuilder stringValue = new StringBuilder(String.valueOf(value));
    while (stringValue.length() != 24) {
      stringValue.insert(0, "0");
    }
    return GradoopId.fromString(stringValue.toString());
  }

  /**
   * Create a readable {@link GradoopId} for an edge. For example an edge between the dataPoints 5 and 7
   * has an ID of '00000000000000000000a5a7'
   *
   * @param i ID of the dataPoint, that represents the row in a matrix
   * @param j ID of the dataPoint, that represents the column in a matrix
   * @return readable edge GradoopId
   */
  public static GradoopId createEdgeGradoopId(int i, int j) {
    StringBuilder stringValue = new StringBuilder("a" + i + "a" + j);
    while (stringValue.length() != 24) {
      stringValue.insert(0, "0");
    }
    return GradoopId.fromString(stringValue.toString());
  }

  /**
   * Transforms a readable {@link GradoopId} back to its original integer value. For example an ID of the
   * value '000000000000000000000005' is transformed to 5.
   *
   * @param value readable {@link GradoopId}
   * @return integer value of the readable {@link GradoopId}
   */
  public static int gradoopIdToInteger(GradoopId value) {
    String stringValue = value.toString();
    while (stringValue.startsWith("0")) {
      if (stringValue.length() == 1) {
        break;
      }
      stringValue = stringValue.substring(1);
    }
    return Integer.parseInt(stringValue);
  }

  /**
   * Get a vertex of a defined integer ID from a collection of {@link EPGMVertex}, using readable
   * {@link GradoopId}s, by transforming their IDs to integer values and compare them to the desired
   * integer ID.
   *
   * @param vertexId integer ID of the desired vertex
   * @param vertices collection of {@link EPGMVertex} using readable {@link GradoopId}s
   * @return the desired vertex
   */
  public static EPGMVertex getVertexWithIdFromCollection(int vertexId, Collection<EPGMVertex> vertices) {
    EPGMVertex resultVertex = null;
    for (EPGMVertex vertex : vertices) {
      if (gradoopIdToInteger(vertex.getId()) == vertexId) {
        resultVertex = vertex;
        break;
      }
    }
    return resultVertex;
  }

  /**
   * Transform a matrix that contains nullValues to a sparse {@link Table}.
   *
   * @param matrix sparse matrix containing cells of the nullValue
   * @param nullValue value that marks a cell to be null and not considered for the sparse {@link Table}
   * @return sparse {@link Table}
   */
  public static Table<Integer, Integer, Double> sparseMatrixToTable(double[][] matrix, double nullValue) {
    Table<Integer, Integer, Double> result = HashBasedTable.create();

    for (int i = 0; i < matrix.length; i++) {
      for (int j = 0; j < matrix[i].length; j++) {
        if (matrix[i][j] != nullValue) {
          result.put(i, j, matrix[i][j]);
        }
      }
    }

    return result;
  }

  /**
   * Transform an array of source names per dataPoint to a Map of source names to a list of dataPoint IDs.
   *
   * @param sources array of source names per dataPoint
   * @return Map of source names to a list of dataPoint IDs
   */
  public static ArrayListMultimap<String, Integer> cleanSourcesArrayToMultiMap(String[] sources) {
    ArrayListMultimap<String, Integer> result = ArrayListMultimap.create();
    for (int i = 0; i < sources.length; i++) {
      if (!sources[i].equals("")) {
        result.put(sources[i], i);
      }
    }
    return result;
  }

  /**
   * Create a {@link LogicalGraph} with readable vertices and readable edges out of the input
   * similarityMatrix and clean source names.
   *
   * @param s similarityMatrix with nullValues, where no edge is present. Similarity values are added to
   *          the {@link PropertyNames#SIM_VALUE} property of the edges.
   * @param sources array of source names per vertex. They are added to the
   * {@link PropertyNames#GRAPH_LABEL} property.
   * @param nullValue represents the value of the similarityMatrix, that marks a not present edge
   * @param env Flink {@link ExecutionEnvironment}
   * @return a {@link LogicalGraph} with readable vertices and readable edges
   */
  public static LogicalGraph getTestGraph(double[][] s, String[] sources, double nullValue,
    ExecutionEnvironment env) {
    int nrRows = s.length;
    int nrColumns = s[0].length;

    List<EPGMVertex> vertices = new ArrayList<>();
    List<EPGMEdge> edges = new ArrayList<>();

    // create vertices
    for (int i = 0; i < nrRows; i++) {
      GradoopId iId = integerToGradoopId(i);
      EPGMVertex vertex = new EPGMVertex();
      vertex.setId(iId);
      vertex.setProperty(PropertyNames.GRAPH_LABEL, sources[i]);
      vertices.add(vertex);
    }
    // create edges
    for (int i = 0; i < nrRows - 1; i++) {
      for (int j = i + 1; j < nrColumns; j++) {
        if (s[i][j] != nullValue) {  // sparse matrix without cells for null values
          GradoopId iId = integerToGradoopId(i);
          GradoopId jId = integerToGradoopId(j);
          EPGMEdge edge = new EPGMEdge();
          edge.setId(createEdgeGradoopId(i, j));
          edge.setSourceId(iId);
          edge.setTargetId(jId);
          edge.setProperty(PropertyNames.SIM_VALUE, s[i][j]);
          edges.add(edge);
        }
      }
    }

    DataSet<EPGMVertex> vertexSet = env.fromCollection(vertices);
    DataSet<EPGMEdge> edgeSet = env.fromCollection(edges);

    GradoopFlinkConfig flinkConfig = GradoopFlinkConfig.createConfig(env);

    return flinkConfig.getLogicalGraphFactory().fromDataSets(vertexSet, edgeSet);
  }

  /**
   * Compare the clustering result with the expected clusters and do the necessary assertions.
   *
   * @param expectedClusters two dimensional array of dataPoints that are expected to be clustered together.
   *                         Each second level array represents one cluster.
   * @param resultVertices vertices that contain the clustering result in their
   * {@link PropertyNames#CLUSTER_ID} property.
   */
  public static void compareClusteringResult(int[][] expectedClusters, List<EPGMVertex> resultVertices) {
    Multimap<String, Integer> resultClusters = ArrayListMultimap.create();

    for (EPGMVertex resultVertex : resultVertices) {
      String clusterId = resultVertex.getPropertyValue(PropertyNames.CLUSTER_ID).getString();
      int vertexId = gradoopIdToInteger(resultVertex.getId());

      resultClusters.put(clusterId, vertexId);
    }

    for (String clusterId : resultClusters.keySet()) {
      Collection<Integer> cluster = resultClusters.get(clusterId);
      int[] expCluster = null;
      // find the expected cluster that contains an element of the resultCluster
      for (int[] expectedCluster : expectedClusters) {
        if (cluster.contains(expectedCluster[0])) {
          expCluster = expectedCluster;
        }
      }
      // an expected cluster must be found
      assertNotNull("no expected cluster found for cluster " + cluster.toString(), expCluster);
      // the size of both clusters must be equal
      assertEquals("the size of cluster " + cluster.toString() + " differs from expected cluster " + Arrays.toString(expCluster),
        expCluster.length, cluster.size());
      // the result cluster must contain each element of the expected cluster
      for (int j : expCluster) {
        assertTrue("expected dataPoint " + j + " is not in cluster " + cluster.toString(), cluster.contains(j));
      }
    }
  }

  /**
   * Check the clustering result of MSCD-HAP. All vertices must be labeled as
   * {@link PropertyNames#IS_HAP_VERTEX}. Global exemplars must have a
   * {@link PropertyNames#HAP_HIERARCHY_DEPTH} that reaches {@code minHierarchyDepth}.
   *
   * @param resultVertices vertices that contain the clustering result in their
   *                      {@link PropertyNames#CLUSTER_ID} property, the hierarchy depth in their
   *                      {@link PropertyNames#HAP_HIERARCHY_DEPTH} property and the flag, whether they are
   *                      an exemplar, in their {@link PropertyNames#IS_CENTER} property.
   * @param minHierarchyDepth minimum of the expected hierarchy depth of global exemplars
   * @param minNumberOfClusters minimum number of expected clusters
   */
  public static void checkHapClusteringResult(List<EPGMVertex> resultVertices, int minHierarchyDepth,
    int minNumberOfClusters) {

    int countClusterCenters = 0;
    Set<String> clusterNames = new HashSet<>();

    for (EPGMVertex vertex : resultVertices) {
      clusterNames.add(vertex.getPropertyValue(PropertyNames.CLUSTER_ID).getString());

      assertTrue(String.format("vertex %d should be labeled as HAP vertex, but it is not",
        ApTestUtils.gradoopIdToInteger(vertex.getId())),
        vertex.getPropertyValue(PropertyNames.IS_HAP_VERTEX).getBoolean());

      int hierarchyDepth = vertex.getPropertyValue(PropertyNames.HAP_HIERARCHY_DEPTH).getInt();
      // global exemplar = IS_CENTER:true, HAP_NOT_ASSIGNABLE:false
      if (vertex.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        countClusterCenters++;
        if (!vertex.getPropertyValue(PropertyNames.HAP_NOT_ASSIGNABLE).getBoolean()) {
          assertTrue(String.format(
            "vertex %d is a global exemplar. It's hierarchy depth should be at least %d, but it is %d",
            ApTestUtils.gradoopIdToInteger(vertex.getId()), minHierarchyDepth, hierarchyDepth),
            hierarchyDepth >= minHierarchyDepth);
        }
      }
    }

    assertTrue(String.format("A minimum of %d clusters was expected, but %d were found",
      minNumberOfClusters, countClusterCenters),
      countClusterCenters >= minNumberOfClusters);

    assertEquals("The number of exemplars does not fit to the number of different clusters",
      countClusterCenters, clusterNames.size());
  }

  /**
   * Compare the clustering result of MSCD-AP with the expected exemplar indices.
   *
   * @param expectedExemplarIndices indices of the dataPoints that are expected to be exemplars
   * @param resultVertices vertices that contain the flag, whether they are an exemplar, in their
   *                       {@link PropertyNames#IS_CENTER} property.
   */
  public static void compareExemplars(List<Integer> expectedExemplarIndices,
    List<EPGMVertex> resultVertices) {

    for (EPGMVertex vertex : resultVertices) {
      int vertexId = gradoopIdToInteger(vertex.getId());
      boolean isCenter = false;
      if (vertex.hasProperty(PropertyNames.IS_CENTER)) {
        isCenter = vertex.getPropertyValue(PropertyNames.IS_CENTER).getBoolean();
      }
      if (isCenter) {
        assertTrue(String.format("vertex %d should be an exemplar", vertexId),
          expectedExemplarIndices.contains(vertexId));
      } else {
        assertFalse(String.format("vertex %d should not be an exemplar", vertexId),
          expectedExemplarIndices.contains(vertexId));
      }
    }
  }

  // #######################################################################################################
  // ################################# Sequential AP implementations #######################################
  // #######################################################################################################

  /**
   * Confirm the result of a sequential AP implementation by assertions on the expected values.
   *
   * @param ap instance of a sequential AP implementation
   * @param expectedLabels expected clustering labels
   * @param expectedExemplarIndices expected exemplars of the clustering
   */
  public static void confirmApResult(AbstractMscdAffinityPropagationSeq ap, int[] expectedLabels,
    Integer[] expectedExemplarIndices) {
    assertArrayEquals("clustering labels differ from expectation", expectedLabels, ap.getLabels());
    assertArrayEquals("exemplars differ from expectation", expectedExemplarIndices,
      ap.getExemplarIndices().toArray());
    assertTrue("AP is not converged", ap.isConverged());
    assertTrue("constraints are not satisfied", ap.areConstraintsSatisfied());
  }

  // #######################################################################################################
  // ################################## GELLY AFFINITY PROPAGATION #########################################
  // #######################################################################################################

  /**
   * Check, whether the result gelly vertices of {@link AffinityPropagationSparseGelly} contain the
   * expected parameter values, so that the parameter adatption run correctly.
   *
   * @param resultVertices result gelly vertices of {@link AffinityPropagationSparseGelly}
   * @param expectedPreferenceDirtySource expected preference value for dirty source dataPoints
   * @param expectedPreferenceCleanSource expected preference value for clean source dataPoints
   * @param expectedDamping expected damping factor
   */
  public static void checkGellyParameterAdaption(List<Vertex<GradoopId, ApVertexValueTuple>> resultVertices,
    double expectedPreferenceDirtySource, double expectedPreferenceCleanSource, double expectedDamping) {

    for (Vertex<GradoopId, ApVertexValueTuple> vertex : resultVertices) {
      assertEquals(String.format("adapted preference (dirty src) value of vertex %d is incorrect.",
        ApTestUtils.gradoopIdToInteger(vertex.getId())),
        expectedPreferenceDirtySource,
        vertex.getValue().getCurrentPreferenceDirtySrc(), 0.0001);

      assertEquals(String.format("adapted preference (clean src) value of vertex %d is incorrect.",
        ApTestUtils.gradoopIdToInteger(vertex.getId())),
        expectedPreferenceCleanSource,
        vertex.getValue().getCurrentPreferenceCleanSrc(), 0.0001);

      assertEquals(String.format("adapted damping value of vertex %d is incorrect.",
        ApTestUtils.gradoopIdToInteger(vertex.getId())),
        expectedDamping,
        vertex.getValue().getCurrentDamping(), 0.0001);
    }
  }

  /**
   * Compare the result of {@link AffinityPropagationSparseGelly#prepareGellyGraphForAP} with the expected
   * result. Do assertions for the correct determination of all-equal-components and singletons.
   *
   * @param resultVertices gelly vertices as a result of
   * {@link AffinityPropagationSparseGelly#prepareGellyGraphForAP}
   * @param expectedSingletons expected singletons
   * @param expectedClusters expected all-equal-component clusters
   */
  public static void compareGellyPreprocessingResult(List<Vertex<GradoopId, ApVertexValueTuple>> resultVertices,
    List<Integer> expectedSingletons, int[][] expectedClusters) {

    BiMap<Integer, Long> vertexIdRandomClusterIdMap = HashBiMap.create();
    for (Vertex<GradoopId, ApVertexValueTuple> vertex : resultVertices) {
      int vertexId = ApTestUtils.gradoopIdToInteger(vertex.getId());
      vertexIdRandomClusterIdMap.put(vertexId, vertex.getValue().getVertexId());
    }

    Map<Integer, Long> expectedClusterIds = new HashMap<>();
    for (int[] expectedCluster : expectedClusters) {
      long maxVertexRandomIdOfCluster = -1L;
      for (int vertexId : expectedCluster) {
        maxVertexRandomIdOfCluster =
          Math.max(vertexIdRandomClusterIdMap.get(vertexId), maxVertexRandomIdOfCluster);
      }
      for (int vertexId : expectedCluster) {
        expectedClusterIds.put(vertexId, maxVertexRandomIdOfCluster);
      }
    }

    for (Vertex<GradoopId, ApVertexValueTuple> vertex : resultVertices) {
      int vertexId = ApTestUtils.gradoopIdToInteger(vertex.getId());

      if (expectedSingletons.contains(vertexId)) {
        // check singletons
        assertTrue(String.format("Vertex %d should be converged (singleton), but it is not.", vertexId),
          vertex.getValue().isComponentConverged());
        assertTrue(String.format("Vertex %d should be an exemplar (singleton), but it is not.", vertexId),
          vertex.getValue().isExemplar());
        assertEquals(String.format("Vertex %d should choose itself as exemplar (singleton), but it didn't.",
          vertexId), vertex.getValue().getVertexId(), vertex.getValue().getClusterId());
      } else if (expectedClusterIds.containsKey(vertexId)) {
        // check allEqualComponents
        Long expectedClusterId = expectedClusterIds.get(vertexId);

        assertEquals(String.format("ClusterId of vertex %d is incorrect.", vertexId),
          vertexIdRandomClusterIdMap.inverse().get(expectedClusterId),
          vertexIdRandomClusterIdMap.inverse().get(vertex.getValue().getClusterId()));
        assertTrue(String.format("Vertex %d should be converged (all-equal-component), but it is not.",
          vertexId), vertex.getValue().isComponentConverged());
        if (expectedClusterId.equals(vertexIdRandomClusterIdMap.get(vertexId))) {
          assertTrue(String.format("Vertex %d should be an exemplar (all-equal-component), but it is not",
            vertexId), vertex.getValue().isExemplar());
        } else {
          assertFalse(String.format("Vertex %d should NOT be an exemplar (all-equal-component), but it is.",
            vertexId), vertex.getValue().isExemplar());
        }
      } else {
        assertFalse(String.format("Vertex %d converged but it shouldn't, because it is neither a singleton " +
            "nor an all-equal component.", vertexId),
          vertex.getValue().isComponentConverged());
        assertFalse(String.format("Vertex %d is an exemplar but it shouldn't, because it is neither a " +
            "singleton nor an all-equal component.", vertexId),
          vertex.getValue().isExemplar());
      }
    }
  }

  // #######################################################################################################
  // ########################### HIERARCHICAL AFFINITY PROPAGATION #########################################
  // #######################################################################################################

  /**
   * Execute the exemplar assignment for clean source dataPoints using the hungarian algorithm in
   * {@link HierarchicalAffinityPropagation#assignDataPointsToExemplarsByHungarianMethod}. The dataPoints
   * of the inputGraph are assign to the provided exemplars.
   *
   * @param hap instance of {@link HierarchicalAffinityPropagation}
   * @param inputGraph {@link LogicalGraph} of edges and vertices that shall be assigned
   * @param exemplars the exemplars, the vertices of the inputGraph are assigned to
   * @param env Flink {@link ExecutionEnvironment}
   * @return the resulting exemplar assignment of
   * {@link HierarchicalAffinityPropagation#assignDataPointsToExemplarsByHungarianMethod}
   */
  public static DataSet<EPGMVertex> getHungarianAssignedVertices(HierarchicalAffinityPropagation hap,
    LogicalGraph inputGraph, Integer[] exemplars, ExecutionEnvironment env) {

    String connCompName = "0";

    DataSet<Tuple4<GradoopId, String, EPGMVertex, Integer>> hungarianVertices =
      getHungarianVertices(inputGraph.getVertices(), connCompName, exemplars, env);

    DataSet<Tuple4<GradoopId, String, Integer, EPGMVertex>> hungarianExemplars =
      getHungarianExemplars(hungarianVertices);

    DataSet<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>
      hungarianEdges = getHungarianEdges(inputGraph.getEdges(), hungarianVertices);

    return hap.assignDataPointsToExemplarsByHungarianMethod(
      hungarianEdges, hungarianVertices, hungarianExemplars);
  }

  /**
   * Prepare the input vertices for the execution of the hungarian exemplar assignment. Mark the vertices
   * as exemplars, when their IDs are present in {@code exemplarIds} and set the connected component name.
   *
   * @param vertices the vertices that need to be prepared for the hungarian exemplar assignment
   * @param connCompName name of the connected component of the vertices
   * @param exemplarIds IDs of the exemplars, the other dataPoints need to be assigned to
   * @param env Flink {@link ExecutionEnvironment}
   * @return the input vertices, prepared for the hungarian exemplar assignment
   */
  private static DataSet<Tuple4<GradoopId, String, EPGMVertex, Integer>> getHungarianVertices(
    DataSet<EPGMVertex> vertices, String connCompName, Integer[] exemplarIds, ExecutionEnvironment env) {
    // get dataset of readable GradoopIds of the input integer datapoint ids
    List<GradoopId> gradoopExemplarIds = new ArrayList<>();
    for (Integer exemplarId : exemplarIds) {
      gradoopExemplarIds.add(ApTestUtils.integerToGradoopId(exemplarId));
    }
    DataSet<GradoopId> exemplarIdSet = env.fromCollection(gradoopExemplarIds);

    return vertices.map(new RichMapFunction<EPGMVertex, Tuple4<GradoopId, String, EPGMVertex, Integer>>() {

      private List<GradoopId> exemplarIds1;

      @Override
      public void open(Configuration parameters) {
        this.exemplarIds1 = getRuntimeContext().getBroadcastVariable("exemplarIds");
      }

      @Override
      public Tuple4<GradoopId, String, EPGMVertex, Integer> map(EPGMVertex vertex) {
        if (exemplarIds1.contains(vertex.getId())) {
          vertex.setProperty(PropertyNames.IS_CENTER, true);
          vertex.setProperty(PropertyNames.CLUSTER_ID,
            connCompName + "-" + ApTestUtils.gradoopIdToInteger(vertex.getId()));
        } else {
          vertex.setProperty(PropertyNames.IS_CENTER, false);
          vertex.setProperty(PropertyNames.CLUSTER_ID, connCompName);
        }

        return Tuple4.of(vertex.getId(), connCompName, vertex, 0);
      }
    }).withBroadcastSet(exemplarIdSet, "exemplarIds");
  }

  /**
   * Get the dataPoints that are marked es exemplars out of the input vertices.
   *
   * @param hungarianVertices vertices that are prepared for the hungarian exemplar assignment.
   * @return vertices for the dataPoints that are marked es exemplars
   */
  private static DataSet<Tuple4<GradoopId, String, Integer, EPGMVertex>> getHungarianExemplars(
    DataSet<Tuple4<GradoopId, String, EPGMVertex, Integer>> hungarianVertices) {

    return hungarianVertices.flatMap((FlatMapFunction<Tuple4<GradoopId, String, EPGMVertex, Integer>,
      Tuple4<GradoopId, String, Integer, EPGMVertex>>) (value, out) -> {
      if (value.f2.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        out.collect(Tuple4.of(value.f0, value.f1, value.f3, value.f2));
      }
    }).returns(new TypeHint<Tuple4<GradoopId, String, Integer, EPGMVertex>>() { });
  }

  /**
   * Prepare Edge tuples out of the edges and vertices, as it is needed for the execution of
   * {@link HierarchicalAffinityPropagation#assignDataPointsToExemplarsByHungarianMethod}.
   *
   * @param edges edges of the test graph
   * @param hungarianVertices vertices of the test graph, prepared for the execution of the hungarian
   *                          exemplar assignment
   * @return edge tuples, needed for
   * {@link HierarchicalAffinityPropagation#assignDataPointsToExemplarsByHungarianMethod}
   */
  private static DataSet<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>
  getHungarianEdges(DataSet<EPGMEdge> edges,
    DataSet<Tuple4<GradoopId, String, EPGMVertex, Integer>> hungarianVertices) {

    return edges.map(new EdgeToSourceIdTargetIdSimValue()).join(hungarianVertices)
      .where(0).equalTo(0)
      .with((JoinFunction<Tuple3<GradoopId, GradoopId, Double>, Tuple4<GradoopId, String, EPGMVertex, Integer>,
        Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>)
        (edge, vertexTuple) -> Tuple8.of(edge.f0, edge.f1, edge.f2, vertexTuple.f1, vertexTuple.f2, null, 0, 0))
      .returns(new TypeHint<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>() { })
      .join(hungarianVertices)
      .where(1).equalTo(0)
      .with((JoinFunction<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>,
        Tuple4<GradoopId, String, EPGMVertex, Integer>,
        Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>)
        (edge, vertexTuple) -> Tuple8.of(edge.f0, edge.f1, edge.f2, edge.f3, edge.f4, vertexTuple.f2, 0, 0))
      .returns(new TypeHint<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>() { });
  }
}
