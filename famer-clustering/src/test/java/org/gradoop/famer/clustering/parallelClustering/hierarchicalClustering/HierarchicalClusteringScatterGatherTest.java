/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering;

import com.google.common.collect.Lists;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.HierarchicalClusteringScatterGather;
import org.gradoop.famer.clustering.serialClustering.SerialHierarchicalClustering;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.gradoop.famer.clustering.common.PropertyNames.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * Test class for {@link HierarchicalClusteringScatterGather}
 */
public class HierarchicalClusteringScatterGatherTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  @Before
  public void setUpGraph() throws Exception {
    String graphString = "similarityGraph[" +
      "/* cluster 1 */" +
      "(v0 {id:0, " + GRAPH_LABEL + ":\"A\"})" +
      "(v1 {id:1, " + GRAPH_LABEL + ":\"B\"})" +
      "(v0)-[e0 {" + SIM_VALUE + ":0.9}]->(v1)" +
      "/* cluster 2 */" +
      "(v2 {id:2, " + GRAPH_LABEL + ":\"B\"})" +
      "(v3 {id:3, " + GRAPH_LABEL + ":\"B\"})" +
      "(v4 {id:4, " + GRAPH_LABEL + ":\"B\"})" +
      "(v0)-[e1 {" + SIM_VALUE + ":0.1}]->(v2)" +
      "(v0)-[e2 {" + SIM_VALUE + ":0.01}]->(v3)" +
      "(v2)-[e3 {" + SIM_VALUE + ":0.5}]->(v3)" +
      "(v2)-[e4 {" + SIM_VALUE + ":0.5}]->(v4)" +
      "(v3)-[e5 {" + SIM_VALUE + ":0.5}]->(v4)" +
      "/* cluster 3 */" +
      "(v5 {id:5, " + GRAPH_LABEL + ":\"A\"})" +
      "(v6 {id:6, " + GRAPH_LABEL + ":\"A\"})" +
      "(v7 {id:7, " + GRAPH_LABEL + ":\"B\"})" +
      "(v5)-[e6 {" + SIM_VALUE + ":0.8}]->(v6)" +
      "(v5)-[e7 {" + SIM_VALUE + ":0.6}]->(v7)" +
      "(v6)-[e8 {" + SIM_VALUE + ":0.5}]->(v7)" +
      "/* cluster 4 */" +
      "(v8 {id:8, " + GRAPH_LABEL + ":\"A\"})" +
      "(v9 {id:9, " + GRAPH_LABEL + ":\"A\"})" +
      "(v11 {id:11, " + GRAPH_LABEL + ":\"A\"})" +
      "(v11)-[e10 {" + SIM_VALUE + ":0.8}]->(v9)" +
      "(v8)-[e9 {" + SIM_VALUE + ":0.3}]->(v9)" +
      "/* cluster 5 - single vertex */" +
      "(v10 {id:10, " + GRAPH_LABEL + ":\"B\"})" +
      "]";
    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("similarityGraph");
  }

  @Test
  public void testClusteringSingleLinkageScatterGatherMaxPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 0.49, PrioritySelection.MAX, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 6);
  }

  @Test
  public void testClusteringSingleLinkageScatterGatherMinPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 0.49, PrioritySelection.MIN, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 6);
  }

  @Test
  public void testClusteringSingleLinkageLowerBoundThresholdScatterGatherMaxPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 0.0, PrioritySelection.MAX, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 4);
  }

  @Test
  public void testClusteringSingleLinkageLowerBoundThresholdScatterGatherMinPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 0.0, PrioritySelection.MIN, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 4);
  }

  @Test
  public void testClusteringSingleLinkageUpperBoundThresholdScatterGatherMaxPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 1.0, PrioritySelection.MAX, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 12);
  }

  @Test
  public void testClusteringSingleLinkageUpperBoundThresholdScatterGatherMinPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE, 1.0, PrioritySelection.MIN, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 12);
  }

  @Test
  public void testClusteringCompleteLinkageScatterGatherMaxPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 0.49, PrioritySelection.MAX, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 6);
  }

  @Test
  public void testClusteringCompleteLinkageScatterGatherMinPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 0.49, PrioritySelection.MIN, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 6);
  }

  @Test
  public void testClusteringCompleteLinkageLowerBoundThresholdScatterGatherMaxPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 0.0, PrioritySelection.MAX, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 6);
  }

  @Test
  public void testClusteringCompleteLinkageLowerBoundThresholdScatterGatherMinPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 0.0, PrioritySelection.MIN, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 6);
  }
  @Test
  public void testClusteringCompleteLinkageUpperBoundThresholdScatterGatherMaxPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 1.0, PrioritySelection.MAX, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 12);
  }

  @Test
  public void testClusteringCompleteLinkageUpperBoundThresholdScatterGatherMinPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE, 1.0, PrioritySelection.MIN, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 12);
  }

  @Test
  public void testClusteringAverageLinkageScatterGatherMaxPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 0.49, PrioritySelection.MAX, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 6);
  }

  @Test
  public void testClusteringAverageLinkageScatterGatherMinPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 0.49, PrioritySelection.MIN, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 6);
  }

  @Test
  public void testClusteringAverageLinkageLowerBoundThresholdScatterGatherMaxPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 0.0, PrioritySelection.MAX, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 4);
  }

  @Test
  public void testClusteringAverageLinkageLowerBoundThresholdScatterGatherMinPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 0.0, PrioritySelection.MIN, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 4);
  }

  @Test
  public void testClusteringAverageLinkageUpperBoundThresholdScatterGatherMaxPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 1.0, PrioritySelection.MAX, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 12);
  }

  @Test
  public void testClusteringAverageLinkageUpperBoundThresholdScatterGatherMinPriority() throws Exception {
    LogicalGraph clusteredGraph = new HierarchicalClusteringScatterGather(ClusteringOutputType.GRAPH, Integer.MAX_VALUE,
      SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE, 1.0, PrioritySelection.MIN, false).execute(inputGraph);

    createClusterListsAndCommonTests(clusteredGraph, 12);
  }

  /**
   * Creates a {@code List<Vertex>} for each cluster, holding the cluster vertices and collects them in a
   * list. Checks if all vertices have the properties for clusterId, isCenter and vertexPriority.
   * Checks if the expected number of clusters was created, checks if only one center per cluster exists
   * and checks if the cluster and center Id of a center vertex matches the vertex Id.
   *
   * @param clusteredGraph The clustered logical graph
   * @param clusterCount the expected cluster count
   * @throws Exception Thrown on errors while collecting the vertices from their DataSets
   */
  public void createClusterListsAndCommonTests(LogicalGraph clusteredGraph, int clusterCount) throws Exception {
    // check vertices and extract cluster id
    DataSet<Tuple2<String, EPGMVertex>> vertices = clusteredGraph.getVertices().flatMap(
      (FlatMapFunction<EPGMVertex, Tuple2<String, EPGMVertex>>) (vertex, out) -> {
        assertNotNull("vertex has no clusterId property",
          vertex.getPropertyValue(CLUSTER_ID).getString());
        assertNotNull("vertex has no vertexPriority property",
          vertex.getPropertyValue(VERTEX_PRIORITY));
        assertNotNull("vertex has no isCenter property",
          vertex.getPropertyValue(IS_CENTER));
        if (vertex.getPropertyValue(IS_CENTER).getBoolean()) {
          assertEquals("vertex is marked as center but its clusterId doesn't equal its vertexId",
            vertex.getId().toString(), vertex.getPropertyValue(CLUSTER_ID).getString());
          assertEquals("vertex is marked as center but its centerId doesn't equal its vertexId",
            vertex.getId(), vertex.getPropertyValue(CENTER_ID).getGradoopId());
        }
        out.collect(Tuple2.of(vertex.getPropertyValue(CLUSTER_ID).getString(), vertex));
      })
      .returns(new TypeHint<Tuple2<String, EPGMVertex>>() { });

    // collect cluster lists
    List<List<EPGMVertex>> clusterLists = vertices.groupBy(0)
      .reduceGroup((GroupReduceFunction<Tuple2<String, EPGMVertex>, List<EPGMVertex>>) (group, out) ->
        out.collect(Lists.newArrayList(group).stream().map(tuple -> tuple.f1).collect(Collectors.toList())))
      .returns(new TypeHint<List<EPGMVertex>>() { })
      .collect();

    // check if only one center per cluster exists
    for (List<EPGMVertex> cluster : clusterLists) {
      assertEquals("one cluster center expected per cluster, found none or more than one",
        1L, cluster.stream().filter(v -> v.getPropertyValue(IS_CENTER).getBoolean()).count());
    }

    // check clusters
    assertEquals("expected " + clusterCount + " clusters, but found " + clusterLists.size(),
      clusterCount, clusterLists.size());
  }
}
