/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common;

import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.clustering.common.PropertyNames.SIM_VALUE;

/**
 * Test class for {@link GenerateOutput}.
 */
public class GenerateOutputTest extends GradoopFlinkTestBase {

  private LogicalGraph clusteredGraph;

  @Before
  public void setUpGraph() throws Exception {
    String graphString = "clusteredGraph[" +
      "/* cluster 1 - overlapping with cluster 2 */" +
      "(v0 {id:0, " + CLUSTER_ID + ":\"1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ":\"1\"})" +
      "(v0)-[e0 {" + SIM_VALUE + ":0.9}]->(v1)" +
      "/* cluster 2 - overlapping with cluster 1 */" +
      "(v2 {id:2, " + CLUSTER_ID + ":\"1,2\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ":\"2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ":\"2\"})" +
      "(v0)-[e1 {" + SIM_VALUE + ":0.1}]->(v2)" +
      "(v0)-[e2 {" + SIM_VALUE + ":0.01}]->(v3)" +
      "(v2)-[e3 {" + SIM_VALUE + ":0.5}]->(v3)" +
      "(v2)-[e4 {" + SIM_VALUE + ":0.5}]->(v4)" +
      "(v3)-[e5 {" + SIM_VALUE + ":0.7}]->(v4)" +
      "/* cluster 3 */" +
      "(v5 {id:5, " + CLUSTER_ID + ":\"3\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ":\"3\"})" +
      "(v7 {id:7, " + CLUSTER_ID + ":\"3\"})" +
      "(v5)-[e6 {" + SIM_VALUE + ":0.8}]->(v6)" +
      "(v5)-[e7 {" + SIM_VALUE + ":0.6}]->(v7)" +
      "(v6)-[e8 {" + SIM_VALUE + ":0.5}]->(v7)" +
      "/* cluster 4 */" +
      "(v8 {id:8, " + CLUSTER_ID + ":\"4\"})" +
      "(v9 {id:9, " + CLUSTER_ID + ":\"4\"})" +
      "(v8)-[e9 {" + SIM_VALUE + ":0.8}]->(v9)" +
      "/* cluster 5 - connected but not overlapping with cluster 3 and 4 */" +
      "(v10 {id:10, " + CLUSTER_ID + ":\"5\"})" +
      "(v6)-[e10 {" + SIM_VALUE + ":0.0}]->(v10)" +
      "(v9)-[e11 {" + SIM_VALUE + ":0.0}]->(v10)" +
      "]";
    clusteredGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");
  }

  @Test
  public void testGenerateVertexSetAsOutput() throws Exception {
    LogicalGraph expected = getConfig().getLogicalGraphFactory().fromDataSets(clusteredGraph.getVertices());

    LogicalGraph output = new GenerateOutput(ClusteringOutputType.VERTEX_SET).execute(clusteredGraph);

    collectAndAssertTrue(expected.equalsByElementIds(output));
    collectAndAssertTrue(expected.equalsByElementData(output));
  }

  @Test
  public void testGenerateLogicalGraphAsOutput() throws Exception {
    LogicalGraph output = new GenerateOutput(ClusteringOutputType.GRAPH).execute(clusteredGraph);

    collectAndAssertTrue(clusteredGraph.equalsByElementIds(output));
    collectAndAssertTrue(clusteredGraph.equalsByElementData(output));
  }

  @Test
  public void testGenerateGraphCollectionAsOutput() throws Exception {
    String graphString = "reducedEdgesGraph[" +
      "/* cluster 1 - overlapping with cluster 2 */" +
      "(v0 {id:0, " + CLUSTER_ID + ":\"1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ":\"1\"})" +
      "(v0)-[e0 {" + SIM_VALUE + ":0.9}]->(v1)" +
      "/* cluster 2 - overlapping with cluster 1 */" +
      "(v2 {id:2, " + CLUSTER_ID + ":\"1,2\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ":\"2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ":\"2\"})" +
      "(v0)-[e1 {" + SIM_VALUE + ":0.1}]->(v2)" +
      "(v2)-[e3 {" + SIM_VALUE + ":0.5}]->(v3)" +
      "(v2)-[e4 {" + SIM_VALUE + ":0.5}]->(v4)" +
      "(v3)-[e5 {" + SIM_VALUE + ":0.7}]->(v4)" +
      "/* cluster 3 */" +
      "(v5 {id:5, " + CLUSTER_ID + ":\"3\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ":\"3\"})" +
      "(v7 {id:7, " + CLUSTER_ID + ":\"3\"})" +
      "(v5)-[e6 {" + SIM_VALUE + ":0.8}]->(v6)" +
      "(v5)-[e7 {" + SIM_VALUE + ":0.6}]->(v7)" +
      "(v6)-[e8 {" + SIM_VALUE + ":0.5}]->(v7)" +
      "/* cluster 4 */" +
      "(v8 {id:8, " + CLUSTER_ID + ":\"4\"})" +
      "(v9 {id:9, " + CLUSTER_ID + ":\"4\"})" +
      "(v8)-[e9 {" + SIM_VALUE + ":0.8}]->(v9)" +
      "/* cluster 5 */" +
      "(v10 {id:10, " + CLUSTER_ID + ":\"5\"})" +
      "]";
    LogicalGraph expected = getLoaderFromString(graphString).getLogicalGraphByVariable("reducedEdgesGraph");

    LogicalGraph output = new GenerateOutput(ClusteringOutputType.GRAPH_COLLECTION).execute(clusteredGraph);

    collectAndAssertTrue(expected.equalsByElementData(output));
  }
}
