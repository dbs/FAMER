/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;

/**
 * Extracts the vertex ids (row and column) from an edge tuple, for this edge tuple returns a
 * {@code Tuple3<RowId,SimValue,ConnCompId>} and {@code Tuple3<ColumnId,SimValue,ConnCompId>}.
 * The clean source information can't be taken, because the source of the row vertex can be different from
 * the source of the column vertex.
 */
public class ExtractVerticesFromEqualCompEdges implements
  FlatMapFunction<Tuple6<GradoopId, GradoopId, Double, String, String, Boolean>,
    Tuple3<GradoopId, Double, String>> {

  @Override
  public void flatMap(
    Tuple6<GradoopId, GradoopId, Double, String, String, Boolean> edgeTuple,
    Collector<Tuple3<GradoopId, Double, String>> out) throws Exception {
    // take only the edges where all similarities are equal
    if (edgeTuple.f5) {
      out.collect(Tuple3.of(edgeTuple.f0, edgeTuple.f2, edgeTuple.f3));  // collect the rowId
      out.collect(Tuple3.of(edgeTuple.f1, edgeTuple.f2, edgeTuple.f3));  // collect the columnID
    }
  }
}
