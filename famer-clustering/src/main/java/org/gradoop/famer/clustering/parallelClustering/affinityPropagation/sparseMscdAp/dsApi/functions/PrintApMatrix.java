/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

import java.util.ArrayList;
import java.util.List;

/**
 * Prints the AP multiMatrix for debugging purposes to the standard output. Only usable for test cases,
 * where GradoopIds were generated out of simple integers.
 */
public class PrintApMatrix implements GroupReduceFunction<ApMultiMatrixCell, ApMultiMatrixCell> {

  /**
   * Prints the AP multiMatrix for debugging purposes to the standard output. Only usable for test cases,
   * where GradoopIds were generated out of simple integers.
   *
   * @param iterable all cells of the AP multiMatrix
   * @param collector collects the unchanged input cells
   */
  @Override
  public void reduce(Iterable<ApMultiMatrixCell> iterable, Collector<ApMultiMatrixCell> collector) {

    List<ApMultiMatrixCell> cells = new ArrayList<>();
    int rowSize = 0;
    int colSize = 0;
    int iterCt = 0;
    int adaptionIterCt = 0;

    for (ApMultiMatrixCell cell : iterable) {
      cells.add(cell);
      collector.collect(cell);

      iterCt = cell.getIterCount();
      adaptionIterCt = cell.getAdaptionIterCount();
      int rowId = gradoopIdToInteger(cell.getRowId());
      int colId = gradoopIdToInteger(cell.getColumnId());
      if (rowId > rowSize) {
        rowSize = rowId;
      }
      if (colId > colSize) {
        colSize = colId;
      }
    }

    int size = Math.max(rowSize, colSize) + 1;
    double[][] a = new double[size][size];
    double[][] r = new double[size][size];
    double[][] s = new double[size][size];
    double[][] c = new double[size][size];
    double[][] h = new double[size][size];

    for (ApMultiMatrixCell cell : cells) {
      int rowId = gradoopIdToInteger(cell.getRowId());
      int colId = gradoopIdToInteger(cell.getColumnId());

      a[rowId][colId] = cell.getA();
      r[rowId][colId] = cell.getR();
      s[rowId][colId] = cell.getS();
      c[rowId][colId] = cell.getA() + cell.getR();
      h[rowId][colId] = c[rowId][colId] > 0 ? 1 : 0;
    }

    System.out.println("Iteration: " + iterCt + " (Adaption-Iteration: " + adaptionIterCt + ")");
    printMatrix("S", s);
    printMatrix("A", a);
    printMatrix("R", r);
    printMatrix("C", c);
    printMatrix("H", h);
  }

  /**
   * Print the matrix to the standard output.
   *
   * @param name Name of the matrix to print
   * @param matrix the matrix to print
   */
  private void printMatrix(String name, double[][] matrix) {
    System.out.print("\n" + name);
    int n = matrix.length;
    for (int i = 0; i < n; i++) {
      System.out.print("\n");
      for (int j = 0; j < n; j++) {
        System.out.print(matrix[i][j] + "\t\t");
      }
    }
    System.out.println("\n");
  }

  /**
   * Transform a GradoopId, that was generated out of an integer, back to its original integer value.
   *
   * @param value GradoopId of a vertex
   * @return integer value of the input ID
   */
  private int gradoopIdToInteger(GradoopId value) {
    String stringValue = value.toString();
    while (stringValue.startsWith("0")) {
      if (stringValue.length() == 1) {
        break;
      }
      stringValue = stringValue.substring(1);
    }
    return Integer.parseInt(stringValue);
  }
}
