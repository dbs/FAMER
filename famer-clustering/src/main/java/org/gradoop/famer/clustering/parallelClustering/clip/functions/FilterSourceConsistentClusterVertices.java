/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

import java.util.ArrayList;
import java.util.List;

/**
 * Reduce function for groups (cluster) of {@code Tuple2<Vertex, ClusterId>} to keep all vertices in a
 * cluster if those vertices are source consistent
 */
public class FilterSourceConsistentClusterVertices implements
  GroupReduceFunction<Tuple2<EPGMVertex, String>, EPGMVertex> {

  /**
   * Number of sources
   */
  private final int sourceNumber;

  /**
   * Creates an instance of FilterSourceConsistentClusterVertices with {@link #sourceNumber} = -1
   */
  public FilterSourceConsistentClusterVertices() {
    this(-1);
  }

  /**
   * Creates an instance of FilterSourceConsistentClusterVertices
   *
   * @param sourceNumber Number of sources
   */
  public FilterSourceConsistentClusterVertices(int sourceNumber) {
    this.sourceNumber = sourceNumber;
  }

  @Override
  public void reduce(Iterable<Tuple2<EPGMVertex, String>> group, Collector<EPGMVertex> out) throws Exception {
    List<String> sources = new ArrayList<>();
    List<EPGMVertex> vertices = new ArrayList<>();
    boolean isSourceConsistent = true;
    for (Tuple2<EPGMVertex, String> groupItem : group) {
      String source = groupItem.f0.getPropertyValue(PropertyNames.GRAPH_LABEL).toString();
      if (!sources.contains(source)) {
        sources.add(source);
      } else {
        isSourceConsistent = false;
      }
      vertices.add(groupItem.f0);
    }
    if (isSourceConsistent) {
      if ((sourceNumber == -1) || (vertices.size() == sourceNumber)) {
        for (EPGMVertex vertex : vertices) {
          out.collect(vertex);
        }
      }
    }
  }
}
