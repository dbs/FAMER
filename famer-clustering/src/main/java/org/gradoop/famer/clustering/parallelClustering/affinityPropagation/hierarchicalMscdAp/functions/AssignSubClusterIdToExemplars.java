/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * For each vertex tuple of Tuple4<VertexId,ClusterId,Partition,Vertex> in a group grouped by
 * ClusterId, extends the ClusterId by an increasing sub cluster number
 */
public class AssignSubClusterIdToExemplars implements
  GroupReduceFunction<Tuple4<GradoopId, String, Integer, EPGMVertex>,
    Tuple4<GradoopId, String, Integer, EPGMVertex>> {

  @Override
  public void reduce(Iterable<Tuple4<GradoopId, String, Integer, EPGMVertex>> group,
    Collector<Tuple4<GradoopId, String, Integer, EPGMVertex>> out) throws Exception {
    int subClusterId = 0;
    for (Tuple4<GradoopId, String, Integer, EPGMVertex> tuple : group) {
      String newClusterId = tuple.f1 + "-" + subClusterId;
      tuple.f3.setProperty(PropertyNames.CLUSTER_ID, newClusterId);
      subClusterId++;
      out.collect(tuple);
    }
  }
}
