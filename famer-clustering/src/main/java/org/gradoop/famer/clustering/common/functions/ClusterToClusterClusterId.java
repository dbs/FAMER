/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.famer.clustering.common.dataStructures.Cluster;

/**
 * Maps a Cluster to a {@code Tuple2<Cluster, ClusterId>}
 */
public class ClusterToClusterClusterId implements MapFunction<Cluster, Tuple2<Cluster, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple2<Cluster, String> reuseTuple = new Tuple2<>();

  @Override
  public Tuple2<Cluster, String> map(Cluster cluster) throws Exception {
    reuseTuple.f0 = cluster;
    reuseTuple.f1 = cluster.getClusterId();
    return reuseTuple;
  }
}
