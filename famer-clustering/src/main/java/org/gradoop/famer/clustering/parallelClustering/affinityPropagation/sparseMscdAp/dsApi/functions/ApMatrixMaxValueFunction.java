/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.ReduceFunction;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * Calculates the maximum tmp matrix values of a {@link ApMultiMatrixCell}-Matrix row or column.
 * The highest value is stored in the field {@link ApMultiMatrixCell#getTmp()}. The second highest value is
 * stored in the field {@link ApMultiMatrixCell#getTmp2()}.
 */
public class ApMatrixMaxValueFunction implements ReduceFunction<ApMultiMatrixCell> {

  /**
   * Defines whether the row maximum (value is true) or the column maximum (value is false) of the
   * multiMatrix needs to be found
   */
  private final boolean findRowMax;

  /**
   * Constructs ApMatrixMaxValueFunction
   *
   * @param findRowMax if true, find the row maximum. Otherwise find the column maximum.
   */
  public ApMatrixMaxValueFunction(boolean findRowMax) {
    this.findRowMax = findRowMax;
  }

  /**
   * Execute the reduce-comparing of multiMatrixCells to find the maximum value of a row or column. Return
   * the cell which has the higher value value in {@link ApMultiMatrixCell#getTmp()}. Set the second
   * highest value to {@link ApMultiMatrixCell#getTmp2()}.
   *
   * @param cell1 multiMatrixCell with the value that needs to be compared in the tmp field and the second
   *            highest value of previous comparisons in the tmp2 field
   * @param cell2 multiMatrixCell with the value that needs to be compared in the tmp field and the second
   *            highest value of previous comparisons in the tmp2 field
   * @return a new multiMatrixCell, representing the one with the higher value
   */
  @Override
  public ApMultiMatrixCell reduce(ApMultiMatrixCell cell1, ApMultiMatrixCell cell2) {
    double highestValue = cell1.getTmp();
    double secondHighestValue = cell2.getTmp();
    GradoopId highestElementId;

    if (findRowMax) {
      highestElementId = cell1.getColumnId();
    } else {
      highestElementId = cell1.getRowId();
    }

    if (cell2.getTmp() > cell1.getTmp()) {
      highestValue = cell2.getTmp();
      secondHighestValue = cell1.getTmp();
      if (findRowMax) {
        highestElementId = cell2.getColumnId();
      } else {
        highestElementId = cell2.getRowId();
      }

      if (cell2.getTmp2() > secondHighestValue) {   // is the winners secondValue > than the losers first?
        secondHighestValue = cell2.getTmp2();
      }
    } else {
      if (cell1.getTmp2() > secondHighestValue) {
        secondHighestValue = cell1.getTmp2();
      }
    }

    // firstId always equal because grouped by; store secondHighestValue into tmp
    ApMultiMatrixCell result;
    if (findRowMax) {
      result = new ApMultiMatrixCell(cell1.getRowId(), highestElementId);
    } else {
      result = new ApMultiMatrixCell(highestElementId, cell1.getColumnId());
    }
    result.setRowCleanSource(cell1.getRowCleanSource());  // must be set because this is a grouping criteria

    result.setTmp(highestValue);
    result.setTmp2(secondHighestValue);
    result.setFlagActivated(true);        // mark the cell as processed by this reduce function

    return result;
  }
}
