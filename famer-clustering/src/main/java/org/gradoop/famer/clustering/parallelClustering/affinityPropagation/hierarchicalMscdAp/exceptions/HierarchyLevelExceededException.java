/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.exceptions;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;

/**
 * Exception class for the case that the hierarchical divide-and-conquer clustering of a connected
 * component in {@link HierarchicalAffinityPropagation} exceeds the maximum hierarchy level.
 */
public class HierarchyLevelExceededException extends Exception {

  /**
   * Constructs a new HierarchyLevelExceededException with the specified detail message.
   *
   * @param message the detail message
   */
  public HierarchyLevelExceededException(String message) {
    super(message);
  }
}
