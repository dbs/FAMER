/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.correlationClustering;

import org.apache.flink.graph.Graph;
import org.apache.flink.graph.spargel.ScatterGatherConfiguration;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.functions.LongMaxAggregator;
import org.gradoop.famer.clustering.parallelClustering.common.functions.EdgeToGellyEdgeCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.GellyVertexToVertexCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.ModifyLogicalGraphForClustering;
import org.gradoop.famer.clustering.parallelClustering.correlationClustering.functions.CorrelationGatherFunction;
import org.gradoop.famer.clustering.parallelClustering.correlationClustering.functions.CorrelationScatterFunction;
import org.gradoop.famer.clustering.parallelClustering.correlationClustering.functions.VertexToGellyVertexForCorrelation;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import static org.gradoop.famer.clustering.parallelClustering.correlationClustering.functions.CorrelationGatherFunction.MAX_DEGREE_AGGREGATOR;

/**
 * The Gradoop/Flink implementation of Correlation Clustering (Pivot) algorithm.
 */
public class CorrelationClustering extends AbstractParallelClustering {

  /**
   * Value between 0.0 and 1.0 used in the computation for the probability for a vertex to get selected as
   * cluster center
   */
  private final double epsilon;

  /**
   * Whether edges are bidirectional
   */
  private final boolean isEdgesBiDirected;

  /**
   * The output type for the clustering result
   */
  private final ClusteringOutputType clusteringOutputType;

  /**
   * Number of maximum iterations for the used scatter gather iteration
   */
  private final int maxIteration;

  /**
   * Creates an instance of CorrelationClustering with {@link #epsilon} set to 0.9
   *
   * @param isEdgesBiDirected Whether edges are bidirectional
   * @param clusteringOutputType The output type of the clustering result
   * @param maxIteration Number of maximum iterations for the used scatter gather iteration
   */
  public CorrelationClustering(boolean isEdgesBiDirected, ClusteringOutputType clusteringOutputType,
    int maxIteration) {
    this(0.9, isEdgesBiDirected, clusteringOutputType, maxIteration);
  }

  /**
   * Creates an instance of CorrelationClustering
   *
   * @param epsilon Value between 0.0 and 1.0 used in the computation for the probability for a vertex to
   *                get selected as cluster center
   * @param isEdgesBiDirected Whether edges are bidirectional
   * @param clusteringOutputType The output type of the clustering result
   * @param maxIteration Number of maximum iterations for the used scatter gather iteration
   */
  public CorrelationClustering(double epsilon, boolean isEdgesBiDirected,
    ClusteringOutputType clusteringOutputType, int maxIteration) {
    this.epsilon = epsilon;
    this.isEdgesBiDirected = isEdgesBiDirected;
    this.clusteringOutputType = clusteringOutputType;
    this.maxIteration = maxIteration;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public CorrelationClustering(JSONObject jsonConfig) {
    try {
      this.epsilon = jsonConfig.getDouble("epsilon");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Clustering: value for epsilon could not be found or parsed",
        ex);
    }
    this.isEdgesBiDirected = getIsEdgesBiDirectedFromJsonConfig(jsonConfig);
    this.clusteringOutputType = getClusteringOutputTypeFromJsonConfig(jsonConfig);
    this.maxIteration = getMaxIterationFromJsonConfig(jsonConfig);
  }

  public double getEpsilon() {
    return epsilon;
  }

  public boolean isEdgesBiDirected() {
    return isEdgesBiDirected;
  }

  public ClusteringOutputType getClusteringOutputType() {
    return clusteringOutputType;
  }

  public int getMaxIteration() {
    return maxIteration;
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph inputGraph) {
    inputGraph = inputGraph.callForGraph(new ModifyLogicalGraphForClustering());
    Graph<GradoopId, EPGMVertex, Double> gellyGraph = Graph.fromDataSet(
      inputGraph.getVertices().map(new VertexToGellyVertexForCorrelation()),
      inputGraph.getEdges().flatMap(new EdgeToGellyEdgeCommon(isEdgesBiDirected)),
      inputGraph.getConfig().getExecutionEnvironment());

    ScatterGatherConfiguration parameters = new ScatterGatherConfiguration();
    LongMaxAggregator longMaxAggregator = new LongMaxAggregator();
    parameters.registerAggregator(MAX_DEGREE_AGGREGATOR, longMaxAggregator);
    parameters.setSolutionSetUnmanagedMemory(true);

    Graph<GradoopId, EPGMVertex, Double> resultGraph = gellyGraph.runScatterGatherIteration(
      new CorrelationScatterFunction(epsilon), new CorrelationGatherFunction(), maxIteration, parameters);

    LogicalGraph resultLogicalGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(), resultGraph.getVertices().map(new GellyVertexToVertexCommon()),
      inputGraph.getEdges());

    return resultLogicalGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  @Override
  public String getName() {
    return CorrelationClustering.class.getName();
  }
}




