/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.center.functions;

import org.apache.flink.graph.Edge;
import org.apache.flink.graph.spargel.ScatterFunction;
import org.apache.flink.types.LongValue;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.CenterMessage;

import static org.gradoop.famer.clustering.parallelClustering.center.functions.CenterGatherFunction.HAS_FINISHED_AGGREGATOR;

/**
 * Scatter function used in Center clustering algorithm
 */
public class CenterScatterFunction extends ScatterFunction<GradoopId, EPGMVertex, CenterMessage, Double> {

  @Override
  public void sendMessages(org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> vertex) {
    int superstepMod2 = getSuperstepNumber() % 2;
    long previousHasFinishedValue = 0L;
    LongValue previousHasFinishedAggregate = getPreviousIterationAggregate(HAS_FINISHED_AGGREGATOR);
    if (previousHasFinishedAggregate != null) {
      previousHasFinishedValue = previousHasFinishedAggregate.getValue();
    }
    long vertexPriority =
      vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
    CenterMessage msg = new CenterMessage(vertexPriority, 0.0);

    if (getSuperstepNumber() == 1 ||
      ((superstepMod2 == 1) && (previousHasFinishedValue != 0))) { // for the first or all odd supersteps
      // identify potential centers
      boolean hasEdges = false;
      for (Edge<GradoopId, Double> edge : getEdges()) {
        msg.setSimDegree(edge.getValue());
        sendMessageTo(edge.getTarget(), msg);
        hasEdges = true;
      }
      // handle unconnected vertices, send message to itself if not marked as center already
      if (!hasEdges && !vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        msg.setSimDegree(-1.0);
        sendMessageTo(vertex.getId(), msg);
      }
    } else if ((superstepMod2 == 0) && (previousHasFinishedValue != 0)) { // for all even supersteps
      String addInfo = "";
      if (vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        addInfo += PropertyNames.IS_CENTER;
      } else if (vertex.f1.getPropertyValue(PropertyNames.IS_NON_CENTER).getBoolean()) {
        addInfo += PropertyNames.IS_NON_CENTER;
      } else {
        addInfo += vertex.f1.getPropertyValue(PropertyNames.DECIDING);
      }
      msg.setAdditionalInfo(addInfo);
      for (Edge<GradoopId, Double> edge : getEdges()) {
        msg.setSimDegree(edge.getValue());
        sendMessageTo(edge.getTarget(), msg);
      }
    }
  }
}
