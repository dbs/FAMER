/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple8;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApType;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms.AbstractMscdAffinityPropagationSeq;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms.MscdAffinityPropagationSeq;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Executes a the sequential implementation of (MSCD-) AP in {@link MscdAffinityPropagationSeq} for the
 * grouped data points of a connected component. This class is for the usage in a hierarchical structure.
 * It returns just the local Exemplars.
 */
public class ReduceGroupByHierarchicalAPFunction extends AbstractReduceGroupByApFunction implements
  GroupReduceFunction<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>,
    Tuple4<GradoopId, String, Integer, EPGMVertex>> {

  /**
   * Constructs ReduceGroupByHierarchicalAPFunction with the clustering config.
   *
   * @param apConfig Configuration of the (MSCD-) Affinity-Propagation clustering algorithm.
   */
  public ReduceGroupByHierarchicalAPFunction(ApConfig apConfig) {
    super(apConfig);
  }

  /**
   * Execute (MSCD-) AP on the grouped and partitioned data points of a connected component with the aim to
   * retrieve local exemplars. If AP did not converge, adapt the parameters until a solution was found or
   * until all possible parameter values were tried. Add the clustering statistics and the
   * {@link PropertyNames#IS_CENTER} Property to the vertices of the local exemplars and return them.
   *
   * @param group Tuples of the edges of the similarity-graph, which are joined to the vertices and contain
   *             connected component information.
   * @param out collects ONLY the vertices of the local exemplars, enriched by the clustering information.
   *
   * @throws ConvergenceException when all possible parameter values are tried and no converging solution
   * could be found.
   */
  @Override
  public void reduce(
    Iterable<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>> group,
    Collector<Tuple4<GradoopId, String, Integer, EPGMVertex>> out) throws ConvergenceException {

    transformInput(group);

    List<Integer> exemplarIds;
    int iterCount;
    boolean unconvergedCompSingletons = false;

    try {
      // execute sequential AP to retrieve the local exemplars of the partition
      AbstractMscdAffinityPropagationSeq ap = runAndAdaptAP();
      exemplarIds = ap.getExemplarIndices();
      iterCount = ap.getIterCount();
    } catch (ConvergenceException e) {
      if (apConfig.isSingletonsForUnconvergedComponents()) {
        // if the component could not converge, each vertex becomes an exemplar (singleton)
        exemplarIds = new ArrayList<>(vertexNumberMap.values());
        iterCount = apConfig.getMaxAdaptionIteration();
        unconvergedCompSingletons = true;
      } else {
        throw e;
      }
    }

    for (Integer exemplarId : exemplarIds) {
      GradoopId vertexId = vertexNumberMap.inverse().get(exemplarId);
      EPGMVertex vertex = vertexIdMap.get(vertexId);
      vertex.setProperty(PropertyNames.IS_CENTER, true);
      // The value is overwritten by higher hierarchy levels. Only the state of the top hierarchy level is
      // relevant, because unconverged singletons are clustered again in new partitions on higher hierarchy
      // levels and may converge. So they have no negative influence on the clustering result.
      vertex.setProperty(PropertyNames.IS_SINGLETON_FROM_UNCONVERGED_COMPONENT, unconvergedCompSingletons);

      setVertexHierarchyDepth(vertex);
      appendCounterToMap(vertex, PropertyNames.NR_ITERATIONS, iterCount);
      appendCounterToMap(vertex, PropertyNames.NR_ADAPTIONS, numberOfAdaptions);
      appendCounterToMap(vertex, PropertyNames.SUM_ITERATIONS, sumIterations);

      out.collect(Tuple4.of(vertexId, originalClusterId, originalPartitionId, vertex));
    }
  }

  /**
   * Stores the counter value (e.g {@link PropertyNames#NR_ITERATIONS} or {@link PropertyNames#NR_ADAPTIONS})
   * of the AP process of the current hierarchy level in a map. So in the evaluation it can be checked,
   * which iteration took how much time. The map has the key: originalClusterId-hierarchyLevel-partitionId.
   *
   * @param vertex the vertex whose property value shall be set.
   * @param propertyName the name of the property, whose value shall be set.
   * @param counterValue the counter of the property (nr_iterations or nr_adaptions) that shall be stored
   *                     for the current hierarchy level.
   */
  private void appendCounterToMap(EPGMVertex vertex, String propertyName, int counterValue) {
    Map<PropertyValue, PropertyValue> clusHierachyPartitionToCounterMap = new HashMap<>();
    if (vertex.hasProperty(propertyName)) {
      clusHierachyPartitionToCounterMap = vertex.getPropertyValue(propertyName).getMap();
    }
    String key = originalClusterId +
      "-" + vertex.getPropertyValue(PropertyNames.HAP_HIERARCHY_DEPTH).getInt() +
      "-" + originalPartitionId;

    clusHierachyPartitionToCounterMap.put(PropertyValue.create(key), PropertyValue.create(counterValue));
    vertex.setProperty(propertyName, clusHierachyPartitionToCounterMap);
  }

  /**
   * Set the {@link PropertyNames#HAP_HIERARCHY_DEPTH} Property of the vertex for the current hierarchy level.
   *
   * @param vertex the vertex whose property value shall be set.
   */
  private void setVertexHierarchyDepth(EPGMVertex vertex) {
    int hierarchyDepth = 0;
    if (vertex.hasProperty(PropertyNames.HAP_HIERARCHY_DEPTH)) {
      hierarchyDepth = vertex.getPropertyValue(PropertyNames.HAP_HIERARCHY_DEPTH).getInt();
    }
    hierarchyDepth++;
    vertex.setProperty(PropertyNames.HAP_HIERARCHY_DEPTH, hierarchyDepth);
  }

  /**
   * Define Affinity Propagation of a hierarchical structure as the current AP-Type.
   *
   * @return {@link ApType#HIERARCHICAL_AFFINITY_PROPAGATION}
   */
  @Override
  protected ApType getApType() {
    return ApType.HIERARCHICAL_AFFINITY_PROPAGATION;
  }
}
