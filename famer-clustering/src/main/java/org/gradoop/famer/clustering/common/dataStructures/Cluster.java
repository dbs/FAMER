/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.dataStructures;

import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * The cluster structure used for postprocessing
 */
public class Cluster {

  /**
   * The vertices of the cluster
   */
  private List<EPGMVertex> vertices;

  /**
   * The inner edges of the cluster, where source and target are part of the cluster
   */
  private List<EPGMEdge> intraEdges;

  /**
   * The edges of the cluster where either source or target is part of another cluster
   */
  private List<EPGMEdge> interLinks;

  /**
   * The id of the cluster
   */
  private String clusterId;

  /**
   * Component id for the cluster
   */
  private String componentId;

  /**
   * Whether the cluster is perfect
   */
  private boolean isPerfect;

  /**
   * Whether the cluster is complete perfect
   */
  private boolean isCompletePerfect;

  /**
   * Whether the cluster is perfect isolated
   */
  private boolean isPerfectIsolated;

  /**
   * Creates an instance of Cluster
   */
  public Cluster() {
    vertices = new ArrayList<>();
    intraEdges = new ArrayList<>();
    interLinks = new ArrayList<>();
    clusterId = "";
    componentId = "";
    isPerfect = false;
    isCompletePerfect = false;
    isPerfectIsolated = false;
  }

  /**
   * Creates an instance of Cluster as copy from another Cluster
   *
   * @param cluster The other cluster
   */
  public Cluster(Cluster cluster) {
    vertices = cluster.getVertices();
    intraEdges = cluster.getIntraEdges();
    interLinks = cluster.getInterLinks();
    clusterId = cluster.getClusterId();
    isPerfect = cluster.isPerfect();
    isCompletePerfect = cluster.isCompletePerfect();
    componentId = cluster.getComponentId();
    isPerfectIsolated = cluster.isPerfectIsolated();
  }

  /**
   * Creates an instance of Cluster
   *
   * @param vertices Vertices of the cluster
   */
  public Cluster(List<EPGMVertex> vertices) {
    this();
    this.vertices = vertices;
  }

  /**
   * Creates an instance of Cluster
   *
   * @param vertices Vertices of the cluster
   * @param clusterId Cluster id
   */
  public Cluster(List<EPGMVertex> vertices, String clusterId) {
    this();
    this.vertices = vertices;
    this.clusterId = clusterId;
  }

  /**
   * Creates an instance of Cluster
   *
   * @param vertices The vertices of the cluster
   * @param intraEdges The inner edges of the cluster
   * @param interLinks The edges of the cluster where either source or target is part of another cluster
   * @param clusterId The id of the cluster
   * @param componentId The component id for the cluster
   */
  public Cluster(List<EPGMVertex> vertices, List<EPGMEdge> intraEdges, List<EPGMEdge> interLinks,
    String clusterId, String componentId) {
    this.vertices = vertices;
    this.intraEdges = intraEdges;
    this.interLinks = interLinks;
    this.clusterId = clusterId;
    this.componentId = componentId;
    isPerfect = false;
    isCompletePerfect = false;
    isPerfectIsolated = false;
  }

  /**
   * Returns the vertices of the cluster
   *
   * @return The cluster vertices
   */
  public List<EPGMVertex> getVertices() {
    return vertices;
  }

  /**
   * Sets the vertices of the cluster
   *
   * @param vertices The cluster vertices
   */
  public void setVertices(List<EPGMVertex> vertices) {
    this.vertices = vertices;
  }

  /**
   * Adds a vertex to the cluster vertices
   *
   * @param vertex The vertex to add
   */
  public void addToVertices(EPGMVertex vertex) {
    vertices.add(vertex);
  }

  /**
   * Retrieves a vertex for the given vertexId from the cluster vertices
   *
   * @param vertexId The vertex id
   *
   * @return The vertex where the vertex id matches
   */
  public EPGMVertex getFromVertices(GradoopId vertexId) {
    for (EPGMVertex vertex : vertices) {
      if (vertex.getId().equals(vertexId)) {
        return vertex;
      }
    }
    return null;
  }

  /**
   * Removes a vertex for the given vertexId from the cluster vertices
   *
   * @param vertexId The vertex id
   */
  public void removeFromVertices(GradoopId vertexId) {
    for (EPGMVertex vertex : vertices) {
      if (vertex.getId().equals(vertexId)) {
        vertices.remove(vertex);
        break;
      }
    }
  }

  /**
   * Returns the inner edges of the cluster
   *
   * @return The inner edges
   */
  public List<EPGMEdge> getIntraEdges() {
    return intraEdges;
  }

  /**
   * Returns the edges of the cluster where either source or target is part of another cluster
   *
   * @param intraEdges Edges where either source or target is part of another cluster
   */
  public void setIntraEdges(List<EPGMEdge> intraEdges) {
    this.intraEdges = intraEdges;
  }

  /**
   * Returns all source or target vertices from the intra edges where the given vertexId matches
   *
   * @param vertexId The vertex id to look for
   *
   * @return A list with all source or target vertices from the intra edges where the vertexId matches
   */
  public List<EPGMVertex> getRelatedVerticesFromIntraEdges(GradoopId vertexId) {
    List<EPGMVertex> relatedVertices = new ArrayList<>();
    for (EPGMEdge edge : intraEdges) {
      if (edge.getSourceId().equals(vertexId)) {
        relatedVertices.add(getFromVertices(edge.getTargetId()));
      } else if (edge.getTargetId().equals(vertexId)) {
        relatedVertices.add(getFromVertices(edge.getSourceId()));
      }
    }
    return relatedVertices;
  }

  /**
   * Returns all inner edges of the cluster
   *
   * @return The inner edges
   */
  public List<EPGMEdge> getInterLinks() {
    return interLinks;
  }

  /**
   * Sets the inner edges of the cluster
   *
   * @param interLinks The inner edges to set
   */
  public void setInterLinks(List<EPGMEdge> interLinks) {
    this.interLinks = interLinks;
  }

  /**
   * Returns the cluster id
   *
   * @return The cluster id
   */
  public String getClusterId() {
    return clusterId;
  }

  /**
   * Sets the cluster id
   *
   * @param clusterId The cluster id to set
   */
  public void setClusterId(String clusterId) {
    this.clusterId = clusterId;
  }

  /**
   * Returns the component id
   *
   * @return The component id
   */
  public String getComponentId() {
    return componentId;
  }

  /**
   * Sets the component id
   *
   * @param componentId The component id to set
   */
  public void setComponentId(String componentId) {
    this.componentId = componentId;
  }

  /**
   * Whether the cluster is perfect
   *
   * @return {@code true} if the cluster is perfect
   */
  public boolean isPerfect() {
    return isPerfect;
  }

  /**
   * Sets the isPerfect value
   *
   * @param isPerfect The isPerfect value
   */
  public void setPerfect(boolean isPerfect) {
    this.isPerfect = isPerfect;
  }

  /**
   * Whether the cluster is complete perfect
   *
   * @return {@code true} if the cluster is complete perfect
   */
  public boolean isCompletePerfect() {
    return isCompletePerfect;
  }

  /**
   * For the given number of sources, computes if the cluster is complete perfect.
   *
   * @param sourceNo Number of sources for the cluster
   */
  public void setCompletePerfect(int sourceNo) {
    // which factors? No. of links or degree of them, or a combination?
    // should we consider the non-determinstic vertices in computing association degree?
    isCompletePerfect = false;
    isPerfect = true;
    List<String> sources = new ArrayList<>();
    for (EPGMVertex v : vertices) {
      String source = v.getPropertyValue(PropertyNames.GRAPH_LABEL).toString();
      if (!sources.contains(source)) {
        sources.add(source);
      } else {
        isPerfect = false;
        break;
      }
    }
    if (isPerfect && (sources.size() == sourceNo)) {
      isCompletePerfect = true;
    }
    boolean isIsolated = false;
    if (interLinks.size() == 0) {
      isIsolated = true;
    }
    if (isPerfect && isIsolated && !isCompletePerfect) {
      isPerfectIsolated = true;
    }
  }

  /**
   * Whether the cluster is perfect isolated
   *
   * @return {@code true} if the cluster is perfect isolated
   */
  public boolean isPerfectIsolated() {
    return isPerfectIsolated;
  }

  /**
   * Sets the isPerfectIsolated value
   *
   * @param isPerfectIsolated The isPerfectIsolated value
   */
  public void setPerfectIsolated(boolean isPerfectIsolated) {
    this.isPerfectIsolated = isPerfectIsolated;
  }

  /**
   * <pre>
   * Computes the association degree for a vertex of the cluster, which is:
   * (sum of all simValues from allowed edges associated to the vertex) / (number of allowed vertices)
   * where allowed vertices are all vertices assigned to only 1 cluster. And the allowed edges are all intra
   * edges where either source or target is the given vertex and target resp. source is an allowed vertex.
   * </pre>
   *
   * @param vertexId Id for the vertex to compute the association degree
   *
   * @return The computed degree
   */
  public double getAssociationDegree(GradoopId vertexId) {
    double sum = 0d;
    List<GradoopId> forbiddenVerticesIds = new ArrayList<>();

    for (EPGMVertex vertex : vertices) {
      int overlapLength = vertex.getPropertyValue(PropertyNames.CLUSTER_ID).toString().split(",").length;
      if (overlapLength > 1) {
        if (!forbiddenVerticesIds.contains(vertex.getId())) {
          forbiddenVerticesIds.add(vertex.getId());
        }
      }
    }

    for (EPGMEdge edge : intraEdges) {
      if ((edge.getTargetId().equals(vertexId) && !forbiddenVerticesIds.contains(edge.getSourceId())) ||
        (edge.getSourceId().equals(vertexId) && !forbiddenVerticesIds.contains(edge.getTargetId()))) {
        sum += Double.parseDouble(edge.getPropertyValue(PropertyNames.SIM_VALUE).toString());
      }
    }

    int clusterSize = vertices.size() - (forbiddenVerticesIds.size() + 1);
    return sum / (double) clusterSize;
  }

  /**
   * Checks if this and another cluster differ by comparing the ids of the contained vertices
   *
   * @param other The other cluster
   *
   * @return {@code true}, if both clusters are different
   */
  public boolean isDifferent(Cluster other) {
    if (other.getVertices().size() != vertices.size()) {
      return true;
    }

    List<String> thisIds = vertices.stream().map(v -> v.getId().toString()).collect(Collectors.toList());
    List<String> otherIds = other.getVertices().stream().map(v -> v.getId().toString())
      .collect(Collectors.toList());

    for (String id : thisIds) {
      if (!otherIds.contains(id)) {
        return true;
      }
    }
    return false;
  }
}
