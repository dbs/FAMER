/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.dataStructures;

/**
 * Enumeration for the center selection based on the vertex priority
 */
public enum PrioritySelection {
  /**
   * Select the vertex with the lowest vertex priority as center
   */
  MIN(0),
  /**
   * Select the vertex with the highest vertex priority as center
   */
  MAX(1);

  /**
   * The priority numeral representing the priority selection
   */
  private final int priorityNumeral;

  /**
   * Creates an instance of PrioritySelection with the given int numeral
   *
   * @param priorityNumeral The priority numeral representing the priority selection
   */
  PrioritySelection(int priorityNumeral) {
    this.priorityNumeral = priorityNumeral;
  }

  public int getIntValue() {
    return this.priorityNumeral;
  }
}
