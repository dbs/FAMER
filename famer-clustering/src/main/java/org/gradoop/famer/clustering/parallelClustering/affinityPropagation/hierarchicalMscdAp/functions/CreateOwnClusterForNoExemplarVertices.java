/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Assigns an updated ClusterId (extended by increasing id) to vertices which are no exemplars and sets some
 * defaults properties for non-exemplars
 */
public class CreateOwnClusterForNoExemplarVertices implements
  GroupReduceFunction<Tuple3<GradoopId, String, EPGMVertex>, Tuple3<GradoopId, String, EPGMVertex>> {

  @Override
  public void reduce(Iterable<Tuple3<GradoopId, String, EPGMVertex>> group,
    Collector<Tuple3<GradoopId, String, EPGMVertex>> out) throws Exception {
    int subClusterId = 0;
    for (Tuple3<GradoopId, String, EPGMVertex> tuple : group) {
      // ne = no exemplar
      tuple.f1 = tuple.f1 + "-ne-" + subClusterId;
      tuple.f2.setProperty(PropertyNames.CLUSTER_ID, tuple.f1);
      tuple.f2.setProperty(PropertyNames.HAP_NOT_ASSIGNABLE, true);
      tuple.f2.setProperty(PropertyNames.IS_CENTER, true);
      out.collect(tuple);
      subClusterId++;
    }
  }
}
