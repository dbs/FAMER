/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.dataStuctures;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;

/**
 * Setting for the assignment of clean source dataPoints to exemplars in the
 * {@link HierarchicalAffinityPropagation} Algorithm. This setting is only relevant for connected
 * components that are larger than {@link HapConfig#getMaxPartitionSize()}. Smaller Components are not
 * partitioned and don't need to be assigned.
 */
public enum HapExemplarAssignmentStrategy {

  /**
   * Clean Source Restriction is not respected. Each dataPoint of a HAP Component is assigned to the
   * exemplar with the highest similarity.
   */
  HIGHEST_SIMILARITY(0),

  /**
   * Clean Source Restriction is respected. Each dataPoint of a dirty source is assigned to the exemplar
   * with the highest similarity. Each dataPoint of a clean source is assigned to an exemplar by the
   * Hungarian Algorithm for linear assignment, so that there are never two dataPoints of the same clean
   * source assigned to the same exemplar.
   */
  HUNGARIAN(1);

  /**
   * Integer value representation of the enum value.
   */
  private final int value;

  /**
   * Constructs HapExemplarAssignmentStrategy.
   * @param value number of the desired enum value.
   */
  HapExemplarAssignmentStrategy(int value) {
    this.value = value;
  }

  /**
   * Get the integer value representation of the enum value.
   *
   * @return integer value representation of the enum value
   */
  public int getValue() {
    return value;
  }
}
