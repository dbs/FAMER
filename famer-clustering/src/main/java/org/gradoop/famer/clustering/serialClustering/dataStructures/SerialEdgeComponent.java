/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering.dataStructures;

/**
 * Edge structure for serial clustering algorithms
 */
public class SerialEdgeComponent {
  /**
   * The source id of the edge component
   */
  private String sourceId;
  /**
   * The target id of the edge component
   */
  private String targetId;
  /**
   * The degree for the edge component
   */
  private double degree;

  /**
   * Creates an instance of SerialEdgeComponent
   *
   * @param sourceId The source id of the edge component
   * @param targetId The target id of the edge component
   * @param degree The degree for the edge component
   */
  public SerialEdgeComponent(String sourceId, String targetId, double degree) {
    this.sourceId = sourceId;
    this.targetId = targetId;
    this.degree = degree;
  }

  public String getSourceId() {
    return sourceId;
  }

  public void setSourceId(String sourceId) {
    this.sourceId = sourceId;
  }

  public String getTargetId() {
    return targetId;
  }

  public void setTargetId(String targetId) {
    this.targetId = targetId;
  }

  public double getDegree() {
    return degree;
  }

  public void setDegree(double degree) {
    this.degree = degree;
  }
}
