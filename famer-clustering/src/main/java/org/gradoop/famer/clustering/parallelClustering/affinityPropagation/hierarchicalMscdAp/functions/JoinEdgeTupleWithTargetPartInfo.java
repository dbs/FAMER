/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.api.java.tuple.Tuple8;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Joins an edge tuple with {@code Tuple6<SourceId,TargetId,SimValue,ClusterId,SourceVertex,SourcePartition>}
 * with the according target vertex tuple of {@code Tuple4<VertexId,ClusterId,Vertex,Partition>} if
 * TargetId equals VertexId and returns a
 * {@code Tuple8<SourceId,TargetId,SimValue,ClusterId,SrcVertex,TgtVertex,SrcPartition,TgtPartition>}
 */
@FunctionAnnotation.ForwardedFieldsFirst("f0;f1;f2;f3;f4;f5->f6")
@FunctionAnnotation.ForwardedFieldsSecond("f2->f5;f3->f7")
public class JoinEdgeTupleWithTargetPartInfo implements
  JoinFunction<Tuple6<GradoopId, GradoopId, Double, String, EPGMVertex, Integer>,
    Tuple4<GradoopId, String, EPGMVertex, Integer>,
    Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>
    reuseTuple = new Tuple8<>();

  @Override
  public Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer> join(
    Tuple6<GradoopId, GradoopId, Double, String, EPGMVertex, Integer> edgeTuple,
    Tuple4<GradoopId, String, EPGMVertex, Integer> vertexTuple) throws Exception {
    reuseTuple.f0 = edgeTuple.f0;
    reuseTuple.f1 = edgeTuple.f1;
    reuseTuple.f2 = edgeTuple.f2;
    reuseTuple.f3 = edgeTuple.f3;
    reuseTuple.f4 = edgeTuple.f4;
    reuseTuple.f5 = vertexTuple.f2;
    reuseTuple.f6 = edgeTuple.f5;
    reuseTuple.f7 = vertexTuple.f3;
    return reuseTuple;
  }
}
