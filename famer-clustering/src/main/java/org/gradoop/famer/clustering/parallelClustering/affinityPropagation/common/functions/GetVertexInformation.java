/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.CleanSourceInformationUtils;

/**
 * Map function to retrieve the vertex information about the vertex's connected component and clean source
 * from its property values.
 */
public class GetVertexInformation implements MapFunction<EPGMVertex, Tuple3<GradoopId, String, String>> {

  /**
   * The configuration of the Affinity Propagation clustering process. It contains settings that are
   * necessary to retrieve the clean source information.
   */
  private final ApConfig apConfig;

  /**
   * Reduce object instantiation
   */
  private final Tuple3<GradoopId, String, String> reuseTuple;

  /**
   * Creates an Instance of GetVertexInformation.
   *
   * @param apConfig The configuration of the Affinity Propagation clustering process. It contains settings
   *                that are necessary to retrieve the clean source information.
   */
  public GetVertexInformation(ApConfig apConfig) {
    this.apConfig = apConfig;
    this.reuseTuple = new Tuple3<>();
  }

  @Override
  public Tuple3<GradoopId, String, String> map(EPGMVertex vertex) {
    // method can also be used for a graph where connectedComponents was not executed jet
    String connectedCompId = "";
    if (vertex.hasProperty(PropertyNames.CLUSTER_ID)) {
      connectedCompId = vertex.getPropertyValue(PropertyNames.CLUSTER_ID).getString();
    }

    reuseTuple.f0 = vertex.getId();
    reuseTuple.f1 = connectedCompId;
    reuseTuple.f2 = CleanSourceInformationUtils.getVertexCleanSourceInformation(vertex, apConfig);
    return reuseTuple;
  }
}
