/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple7;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms.HungarianAlgorithmSeq;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Assigns data points, grouped by their clean source name and their connected component, to the global
 * exemplars which are the result of the hierarchical divide-and-conquer clustering in the delta iteration
 * of {@link HierarchicalAffinityPropagation}. For the assignment a sequential implementation of the
 * hungarian algorithm ({@link HungarianAlgorithmSeq}) is used, so that two points of the same clean
 * source can not be assigned to the same global exemplar.
 */
public class AssignDataPointsToExemplarsHungarian implements
  GroupReduceFunction<Tuple7<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, String>,
    Tuple4<GradoopId, String, EPGMVertex, Boolean>> {

  /**
   * Value that is used in the {@link #similarities} matrix, when there is no similarity between the data
   * point and the exemplar. A value of 100 is used, because the similarities are negated. The algorithm
   * searches for the minimum costs.
   */
  private static final double NULL_VALUE = 100d;

  /**
   * Maps the ids of the data points in the {@link #similarities} matrix to their {@link GradoopId}
   */
  private BiMap<GradoopId, Integer> dataPointIdMap;

  /**
   * Maps the exemplar ids of the {@link #similarities} matrix to their clusterId. So the data points that
   * are assigned to the exemplar can pick the clusterId from this map.
   */
  private BiMap<String, Integer> exemplarIdClusterIdMap;

  /**
   * Unique set that contains each data point once, that occurs in the edge tuples of the input iterable.
   */
  private Set<EPGMVertex> dataPointSet;

  /**
   * MxN matrix of M data points and N exemplars, that contains the similarities between them. Used by the
   * sequential implementation of the hungarian algorithm in {@link HungarianAlgorithmSeq}.
   */
  private double[][] similarities;

  /**
   * Name of the connected component that was partitioned and clustered.
   */
  private String baseClusterName = null;

  /**
   * Source name of the data points that are assigned to the global exemplars.
   */
  private String dataPointsSource = null;

  /**
   * Assigns data points, grouped by their clean source name and their connected component, to the global
   * exemplars which are the result of the hierarchical divide-and-conquer clustering in the delta iteration
   * of {@link HierarchicalAffinityPropagation}. If the source is dirty, the data points are assigned to
   * the global exemplars by their highest similarity. Multiple data points can be assigned to the same
   * exemplar. If the source is clean, each data point is assigned to a different global exemplar,
   * maximizing the sum of the similarity for all assignments, using the sequential implementation of the
   * hungarian algorithm in {@link HungarianAlgorithmSeq}.
   *
   * The input tuples must be a collection of edges with:
   * <ul>
   *   <li>only edges between exemplars and dataPoints</li>
   *   <li>no dataPoint-dataPoint or exemplar-exemplar edges</li>
   *   <li>exemplars must already contain a unique CLUSTER_ID property for their originalCluster</li>
   * </ul>
   *
   * @param group edge tuples for the edges between all global exemplars and all data points of the same
   *               clean source and the same connected component, grouped by ClusterId and DataPointGraphLabel
   * @param out collects a set of vertex tuples as Tuple4<VertexId,ClusterId,Vertex,SuccessfulAssignedFlag>
   */
  @Override
  public void reduce(
    Iterable<Tuple7<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, String>> group,
    Collector<Tuple4<GradoopId, String, EPGMVertex, Boolean>> out) {

    transformInput(group);

    if (dataPointsSource.equals("")) {
      // dataPoints are from dirty sources => execute standard assignment
      // => there can be multiple vertices assigned to the same exemplar
      for (EPGMVertex dataPoint : dataPointSet) {
        int dataPointId = dataPointIdMap.get(dataPoint.getId());
        double minSim = NULL_VALUE;
        int minSimId = -1;
        for (int j = 0; j < exemplarIdClusterIdMap.keySet().size(); j++) {
          if (similarities[dataPointId][j] < minSim) {
            minSim = similarities[dataPointId][j];
            minSimId = j;
          }
        }

        String clusterId = exemplarIdClusterIdMap.inverse().get(minSimId);
        dataPoint.setProperty(PropertyNames.CLUSTER_ID, clusterId);

        out.collect(Tuple4.of(dataPoint.getId(), clusterId, dataPoint, true));
      }
    } else {
      // Source is a clean source => execute hungarian algorithm
      // Jobs (j) = Exemplars
      // Workers (i) = DataPoints from the same CleanSource
      HungarianAlgorithmSeq hu = new HungarianAlgorithmSeq(similarities);
      int[] resHU = hu.execute();

      for (EPGMVertex dataPoint : dataPointSet) {
        int dataPointId = dataPointIdMap.get(dataPoint.getId());

        if (resHU[dataPointId] == -1) {
          // data point could not be assigned to any exemplar => mark with false
          out.collect(Tuple4.of(dataPoint.getId(), baseClusterName, dataPoint, false));
        } else {
          String clusterId = exemplarIdClusterIdMap.inverse().get(resHU[dataPointId]);
          dataPoint.setProperty(PropertyNames.CLUSTER_ID, clusterId);

          out.collect(Tuple4.of(dataPoint.getId(), clusterId, dataPoint, true));
        }
      }
    }
  }

  /**
   * Transform the input edge tuples to an NxM similarity matrix for N data points and M global exemplars.
   * Store the integer IDs of the data points for the similarity matrix in the {@link #dataPointIdMap}, the
   * exemplars with their clusterId in the {@link #exemplarIdClusterIdMap} and all data points in the
   * {@link #dataPointSet}. So the input can be processed by the sequential implementation of the hungarian
   * algorithm in {@link HungarianAlgorithmSeq} and the result can be used to enrich the data point vertices.
   *
   * @param group edge tuples for the edges between all global exemplars and all data points of the same
   *              clean source and the same connected component, grouped by ClusterId and DataPointGraphLabel
   */
  private void transformInput(
    Iterable<Tuple7<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, String>> group) {
    // input: Tuple7<ExemplarId,DatapointId,SimValue,ClusterId,Exemplar,Datapoint,DatapointGraphLabel>

    List<Tuple7<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, String>> tuples =
      new ArrayList<>();

    dataPointIdMap = HashBiMap.create();
    exemplarIdClusterIdMap = HashBiMap.create();
    dataPointSet = new HashSet<>();

    int nextDataPointId = 0;
    int nextExemplarId = 0;

    // fill dataPointMap and exemplarMap
    for (Tuple7<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, String> tuple : group) {
      tuples.add(tuple);
      if (baseClusterName == null) {
        baseClusterName = tuple.f3;
      }
      if (dataPointsSource == null) {
        dataPointsSource = tuple.f6;
      }

      String exemplarClusterId = tuple.f4.getPropertyValue(PropertyNames.CLUSTER_ID).getString();
      if (exemplarIdClusterIdMap.putIfAbsent(exemplarClusterId, nextExemplarId) == null) {
        nextExemplarId++;
      }
      if (dataPointIdMap.putIfAbsent(tuple.f1, nextDataPointId) == null) {
        nextDataPointId++;
        dataPointSet.add(tuple.f5);
      }
    }

    // initialize similarities
    similarities = new double[dataPointIdMap.keySet().size()][exemplarIdClusterIdMap.keySet().size()];
    for (int i = 0; i < dataPointIdMap.keySet().size(); i++) {
      Arrays.fill(similarities[i], NULL_VALUE);
    }

    for (Tuple7<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, String> tuple : tuples) {
      int i = dataPointIdMap.get(tuple.f1);
      int j = exemplarIdClusterIdMap.get(tuple.f4.getPropertyValue(PropertyNames.CLUSTER_ID).getString());
      similarities[i][j] = 1 - tuple.f2;  // inverse of similarity, because hungarian searches for minimum
    }
  }
}
