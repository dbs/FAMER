/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.functions;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.utils.DataSetUtils;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.flink.model.api.operators.UnaryGraphToGraphOperator;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * Used to assign a random unique {@code long} value as vertex priority to each vertex of a
 * {@link LogicalGraph}. It is used before applying a clustering algorithm to a graph. The priority value
 * must be greater than 0.
 */
public class ModifyLogicalGraphForClustering implements UnaryGraphToGraphOperator {

  @Override
  public LogicalGraph execute(final LogicalGraph graph) {
    DataSet<EPGMVertex> verticesWithVertexPriority = DataSetUtils.zipWithUniqueId(graph.getVertices())
      .map(new AssignIncrementedLongIdToProperty(PropertyNames.VERTEX_PRIORITY));

    return graph.getConfig().getLogicalGraphFactory().fromDataSets(
      graph.getGraphHead(), verticesWithVertexPriority, graph.getEdges());
  }

  @Override
  public String getName() {
    return ModifyLogicalGraphForClustering.class.getName();
  }
}
