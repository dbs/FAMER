/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple7;
import org.apache.flink.api.java.tuple.Tuple8;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Maps each hierarchical iteration solution edge tuple to an exemplar edge tuple of
 * Tuple7<ExemplarId,DatapointId,SimValue,ClusterId,Exemplar,Datapoint,DatapointGraphLabel>
 */
@FunctionAnnotation.ForwardedFields("f2;f3")
@FunctionAnnotation.ReadFields("f4;f5")
public class MapSolutionEdgesToExemplarEdges implements
  FlatMapFunction<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>,
    Tuple7<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple7<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, String> reuseTuple =
    new Tuple7<>();

  @Override
  public void flatMap(
    Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer> solutionTuple,
    Collector<Tuple7<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, String>> out)
    throws Exception {
    EPGMVertex v1 = solutionTuple.f4;
    EPGMVertex v2 = solutionTuple.f5;
    boolean v1IsCenter = false;
    boolean v2IsCenter = false;

    if (v1.hasProperty(PropertyNames.IS_CENTER)) {
      v1IsCenter = v1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean();
    }
    if (v2.hasProperty(PropertyNames.IS_CENTER)) {
      v2IsCenter = v2.getPropertyValue(PropertyNames.IS_CENTER).getBoolean();
    }
    String v1Src = v1.getPropertyValue(PropertyNames.GRAPH_LABEL).getString();
    String v2Src = v2.getPropertyValue(PropertyNames.GRAPH_LABEL).getString();

    // one of the vertices must be an exemplar, but not both - two exemplars can't be assigned to each other
    if ((v1IsCenter || v2IsCenter) && !(v1IsCenter && v2IsCenter)) {
      // exemplar and dataPoint must be of different clean sources or one of them must be from a dirty source
      if (v1Src.equals("") || v2Src.equals("") || !v1Src.equals(v2Src)) {
        reuseTuple.f2 = solutionTuple.f2;
        reuseTuple.f3 = solutionTuple.f3;
        if (v1IsCenter) {
          reuseTuple.f0 = solutionTuple.f0;
          reuseTuple.f1 = solutionTuple.f1;
          reuseTuple.f4 = solutionTuple.f4;
          reuseTuple.f5 = solutionTuple.f5;
          reuseTuple.f6 = v2Src;
        } else {
          reuseTuple.f0 = solutionTuple.f1;
          reuseTuple.f1 = solutionTuple.f0;
          reuseTuple.f4 = solutionTuple.f5;
          reuseTuple.f5 = solutionTuple.f4;
          reuseTuple.f6 = v1Src;
        }
        out.collect(reuseTuple);
      }
    }
  }
}
