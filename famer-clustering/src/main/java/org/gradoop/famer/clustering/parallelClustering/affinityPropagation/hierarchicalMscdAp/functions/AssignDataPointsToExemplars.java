/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple8;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Assigns data points, grouped by their connected component, to the global exemplars which are the result
 * of the hierarchical divide-and-conquer clustering in the delta iteration of
 * {@link HierarchicalAffinityPropagation}. The data points are assigned to the global exemplars by their
 * highest similarity. Multiple data points can be assigned to the same global exemplar.
 */
public class AssignDataPointsToExemplars implements
  GroupReduceFunction<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>,
    EPGMVertex> {

  /**
   * Contains all Vertices from the input iterable
   */
  private Set<EPGMVertex> vertexSet;

  /**
   * Maps Integer-SubClusterIds to the {@link GradoopId}s of the global exemplars
   */
  private Map<GradoopId, Integer> exemplarSubClusterIdMap;

  /**
   * Similarity table <vertexId, exemplarId, simVal> for all Vertex-Exemplar-Combinations
   */
  private Table<GradoopId, GradoopId, Double> similarityTable;

  /**
   * Holds the next unassigned SubClusterId for the creation of the exemplarSubClusterIdMap and for
   * vertices that can't be assigned to any exemplar and become exemplar itself.
   */
  private int nextSubClusterId;

  /**
   * Execute the highest similarity assignment of data points to the global exemplars.
   * For each vertex within an edge tuple of the input-group:
   * a) assign it to his own subCluster if he is a global exemplar
   * b) assign it to the exemplar with the highest similarity to the vertex, if he is not a global exemplar
   * c) assign it to his own subCluster, if it has no edge to any of the global exemplars.
   *
   * @param group edge tuples for the edges between all global exemplars and all data points, grouped by
   *              their connected component
   * @param out collects each vertex of the connected component, enriched by the clustering
   *                  information. The properties {@link PropertyNames#CLUSTER_ID} and
   *                  {@link PropertyNames#IS_CENTER} are set regarding to the cluster assignment. If a
   *                  vertex was not assignable, the Property {@link PropertyNames#HAP_NOT_ASSIGNABLE} is
   *                  set to true.
   */
  @Override
  public void reduce(
    Iterable<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>> group,
    Collector<EPGMVertex> out) {

    transformInput(group);

    for (EPGMVertex vertex : this.vertexSet) {
      String baseClusterName = vertex.getPropertyValue(PropertyNames.CLUSTER_ID).getString();

      if (vertex.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        // a) the vertex is an exemplar --> add him to his own subCluster
        int subClusterName = exemplarSubClusterIdMap.get(vertex.getId());
        vertex.setProperty(PropertyNames.CLUSTER_ID, baseClusterName + "-" + subClusterName);
      } else {
        // b) find the exemplar with the highest similarity for the vertex
        Map<GradoopId, Double> possibleExemplars = similarityTable.row(vertex.getId());
        double highestSim = -1;
        GradoopId highestSimExemplar = null;
        for (Map.Entry<GradoopId, Double> entry : possibleExemplars.entrySet()) {
          if (entry.getValue() > highestSim) {
            highestSim = entry.getValue();
            highestSimExemplar = entry.getKey();
          }
        }
        if (highestSim > -1) {
          int subClusterName = exemplarSubClusterIdMap.get(highestSimExemplar);
          vertex.setProperty(PropertyNames.CLUSTER_ID, baseClusterName + "-" + subClusterName);
        } else {
          /* c) The dataPoint could not be assigned to an exemplar.
           * Non of the exemplars has an edge to the dataPoint.
           * This is a consequence of Hierarchical Affinity Propagation. When exemplars are clustered
           * on a higher hierarchy level, then not all dataPoints can be considered anymore.
           * Consequently these dataPoints must form a own cluster.
           * The bigger the partition size, the fewer dataPoints will have this problem and the better the
           * clusteringQuality.
           */
          vertex.setProperty(PropertyNames.CLUSTER_ID, baseClusterName + "-" + nextSubClusterId);
          nextSubClusterId++;
          vertex.setProperty(PropertyNames.IS_CENTER, true);
          vertex.setProperty(PropertyNames.HAP_NOT_ASSIGNABLE, true);
        }
      }

      vertex.setProperty(PropertyNames.IS_HAP_VERTEX, true);
      out.collect(vertex);
    }
  }

  /**
   * Creates a similarityTable <vertexId, exemplarId, simVal> for all Vertex-Exemplar-Combinations.
   *
   * @param group edge tuples for the edges between all global exemplars and all data points, grouped by
   *              their connected component
   */
  private void transformInput(
    Iterable<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>> group) {
    this.vertexSet = new HashSet<>();
    this.exemplarSubClusterIdMap = new HashMap<>();
    this.similarityTable = HashBasedTable.create();   // stores the data as HashMap<R, HashMap<C, V>>

    nextSubClusterId = 0;

    for (Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer> tuple :
      group) {
      // fill the vertexIdMap
      this.vertexSet.add(tuple.f4); // with source vertex (v1)
      this.vertexSet.add(tuple.f5); // with target vertex (v2)

      if (tuple.f4.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        // v1 is an Exemplar - add its similarity value to the map of v2
        similarityTable.put(tuple.f1, tuple.f0, tuple.f2);
        if (exemplarSubClusterIdMap.putIfAbsent(tuple.f0, nextSubClusterId) == null) {
          nextSubClusterId++;
        }
      }
      if (tuple.f5.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        // v2 is an Exemplar - add its similarity value to the map of v1
        similarityTable.put(tuple.f0, tuple.f1, tuple.f2);
        if (exemplarSubClusterIdMap.putIfAbsent(tuple.f1, nextSubClusterId) == null) {
          nextSubClusterId++;
        }
      }
    }
  }
}
