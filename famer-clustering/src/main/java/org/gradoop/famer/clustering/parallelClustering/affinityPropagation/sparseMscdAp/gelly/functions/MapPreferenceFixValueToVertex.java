/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.PreferenceConfig;

/**
 * Maps the fixed preference values for clean and dirty sources from the {@link PreferenceConfig} to the
 * vertex properties {@link PropertyNames#PREFERENCE_DIRTY_SOURCE} and
 * {@link PropertyNames#PREFERENCE_CLEAN_SOURCE}.
 */
public class MapPreferenceFixValueToVertex implements MapFunction<EPGMVertex, EPGMVertex> {

  /**
   * Configuration of the preference parameter for the (MSCD-) Affinity Propagation clustering algorithm.
   */
  private final PreferenceConfig preferenceConfig;

  /**
   * Constructs MapPreferenceFixValueToVertex
   *
   * @param preferenceConfig Configuration of the preference parameter for the (MSCD-) Affinity Propagation
   *                       clustering algorithm.
   */
  public MapPreferenceFixValueToVertex(PreferenceConfig preferenceConfig) {
    this.preferenceConfig = preferenceConfig;
  }

  @Override
  public EPGMVertex map(EPGMVertex vertex)  {
    vertex.setProperty(PropertyNames.PREFERENCE_DIRTY_SOURCE,
      preferenceConfig.getPreferenceFixValueDirtySrc());
    vertex.setProperty(PropertyNames.PREFERENCE_CLEAN_SOURCE,
      preferenceConfig.getPreferenceFixValueCleanSrc());

    return vertex;
  }
}
