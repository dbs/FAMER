/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.dataStuctures;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;

import java.util.Locale;

/**
 * Configuration class for {@link HierarchicalAffinityPropagation} (HAP) and its extension Multi-Source
 * Clean-Dirty HAP (MSCD-HAP), which is implemented in the same class.
 */
public class HapConfig extends ApConfig {

  /**
   * Maximum size of the partitions of a connected component for the hierarchical devide-and-conquer
   * clustering. HAP forms equal size partitions of a size as close as possible to maxPartitionSize.
   */
  private int maxPartitionSize;

  /**
   * Maximum hierarchy depth sets a limit to HAP's delta iteration. So in the exceptional case, where AP
   * creates only singletons on all partitions so that the number of local exemplars doesn't shrink, the
   * algorithm still can terminate.
   */
  private int maxHierarchyDepth;

  /**
   * Sets for the assignment strategy for clean source dataPoints of partitioned connected components.
   */
  private HapExemplarAssignmentStrategy hapExemplarAssignmentStrategy;

  /**
   * Creates an instance of HapConfig with default values.
   */
  public HapConfig() {
    super();
    this.maxPartitionSize = 1000;
    this.maxHierarchyDepth = 10;
    this.hapExemplarAssignmentStrategy = HapExemplarAssignmentStrategy.HUNGARIAN;
  }

  /**
   * Checks if all parameter values are in an allowed value range.
   * @throws IllegalArgumentException if a parameter value is outside of the allowed range.
   */
  @Override
  public void checkConfigCorrectness() throws IllegalArgumentException {
    super.checkConfigCorrectness();

    if (maxPartitionSize < 3) {
      throw new IllegalArgumentException("maxPartitionSize must be greater than 2");
    }

    if (maxHierarchyDepth < 2) {
      throw new IllegalArgumentException("maxHierarchyDepth must be greater than 1");
    }
  }

  /**
   * Create a name for the config that contains the most important parameter values. This name can be used
   * as a file name for evaluation results.
   * @return String with important parameters and values.
   */
  @Override
  public String getConfigName() {
    return String.format(Locale.US, super.getConfigName() + "__part_%d__ass_%d",
      maxPartitionSize, hapExemplarAssignmentStrategy.getValue());
  }

  public int getMaxPartitionSize() {
    return maxPartitionSize;
  }

  public void setMaxPartitionSize(int maxPartitionSize) {
    this.maxPartitionSize = maxPartitionSize;
  }

  public int getMaxHierarchyDepth() {
    return maxHierarchyDepth;
  }

  public void setMaxHierarchyDepth(int maxHierarchyDepth) {
    this.maxHierarchyDepth = maxHierarchyDepth;
  }

  public HapExemplarAssignmentStrategy getHapExemplarAssignmentStrategy() {
    return hapExemplarAssignmentStrategy;
  }

  public void setHapExemplarAssignmentStrategy(HapExemplarAssignmentStrategy hapExemplarAssignmentStrategy) {
    this.hapExemplarAssignmentStrategy = hapExemplarAssignmentStrategy;
  }
}
