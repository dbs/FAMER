/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions;

import org.apache.flink.graph.Edge;
import org.apache.flink.graph.spargel.ScatterFunction;
import org.apache.flink.types.LongValue;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.CenterMessage;

import static org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions.MergeCenterGatherFunction.HAS_FINISHED_AGGREGATOR;
import static org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions.MergeCenterGatherFunction.PHASE_2_AGGREGATOR;
import static org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions.MergeCenterGatherFunction.PHASE_3_AGGREGATOR;

/**
 * Scatter function used for the MergeCenter algorithm
 */
public class MergeCenterScatterFunction extends
  ScatterFunction<GradoopId, EPGMVertex, CenterMessage, Double> {

  @Override
  public void sendMessages(org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> vertex) {
    int superstepMod2 = getSuperstepNumber() % 2;
    // get previous aggregate values
    long previousHasFinishedValue = 0L;
    LongValue previousHasFinishedAggregate = getPreviousIterationAggregate(HAS_FINISHED_AGGREGATOR);
    if (previousHasFinishedAggregate != null) {
      previousHasFinishedValue = previousHasFinishedAggregate.getValue();
    }
    long previousPhase2Value = 0L;
    LongValue previousPhase2Aggregate = getPreviousIterationAggregate(PHASE_2_AGGREGATOR);
    if (previousPhase2Aggregate != null) {
      previousPhase2Value = previousPhase2Aggregate.getValue();
    }
    long previousPhase3Value = 0L;
    LongValue previousPhase3Aggregate = getPreviousIterationAggregate(PHASE_3_AGGREGATOR);
    if (previousPhase3Aggregate != null) {
      previousPhase3Value = previousPhase3Aggregate.getValue();
    }

    long vertexPriority =
      vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
    long clusterId = vertex.f1.getPropertyValue(PropertyNames.CLUSTER_ID).getLong();

    if ((getSuperstepNumber() == 1) ||
      ((superstepMod2 == 1) && (previousHasFinishedValue != 0L))) { // for the first or all odd supersteps
      // identify potential centers
      CenterMessage msg = new CenterMessage(vertexPriority, 0.0);
      boolean hasEdges = false;
      for (Edge<GradoopId, Double> edge : getEdges()) {
        msg.setSimDegree(edge.getValue());
        sendMessageTo(edge.getTarget(), msg);
        hasEdges = true;
      }
      // handle unconnected vertices, send message to itself if not marked as center already
      if (!hasEdges && !vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        msg.setSimDegree(-1.0);
        sendMessageTo(vertex.getId(), msg);
      }
    } else if ((superstepMod2 == 0) && (previousHasFinishedValue != 0L)) { // for all even supersteps
      CenterMessage msg = new CenterMessage(vertexPriority, 0.0);
      String addInfo;
      if (vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        addInfo = PropertyNames.IS_CENTER;
      } else if (vertex.f1.getPropertyValue(PropertyNames.IS_NON_CENTER).getBoolean()) {
        addInfo = PropertyNames.IS_NON_CENTER;
      } else {
        addInfo = vertex.f1.getPropertyValue(PropertyNames.DECIDING).toString();
      }
      msg.setAdditionalInfo(addInfo);
      for (Edge<GradoopId, Double> edge : getEdges()) {
        msg.setSimDegree(edge.getValue());
        sendMessageTo(edge.getTarget(), msg);
      }
    } else if (previousPhase2Value != 0L) {
      CenterMessage msg = new CenterMessage(clusterId, 0.0);
      if (vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        msg.setAdditionalInfo(PropertyNames.IS_CENTER + "," +
          vertex.f1.getPropertyValue(PropertyNames.CENTER_DEGREE).toString());
      }
      for (Edge<GradoopId, Double> edge : getEdges()) {
        msg.setSimDegree(edge.getValue());
        sendMessageTo(edge.getTarget(), msg);
      }
      msg.setSimDegree(Double.MIN_VALUE);
      sendMessageTo(vertex.getId(), msg);
    } else if (previousPhase3Value != 0L) {
      if (vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        CenterMessage msg = new CenterMessage(clusterId, 0.0);
        for (Edge<GradoopId, Double> edge : getEdges()) {
          msg.setSimDegree(edge.getValue());
          sendMessageTo(edge.getTarget(), msg);
        }
      }
    }
  }
}
