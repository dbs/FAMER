/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;

/**
 * Join function to get the Edge from a joined {@code Tuple2<Edge, GradoopId>}
 */
@FunctionAnnotation.ForwardedFieldsFirst("f0->*")
public class RetrieveEdgeFromJoinedTuple implements
  JoinFunction<Tuple2<EPGMEdge, GradoopId>, Tuple1<GradoopId>, EPGMEdge> {

  @Override
  public EPGMEdge join(Tuple2<EPGMEdge, GradoopId> edgeEdgeId, Tuple1<GradoopId> edgeId) throws Exception {
    return edgeEdgeId.f0;
  }
}
