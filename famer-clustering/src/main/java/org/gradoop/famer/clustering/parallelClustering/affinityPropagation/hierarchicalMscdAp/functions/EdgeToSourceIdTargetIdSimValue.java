/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Maps an edge to a {@code Tuple3<SourceId,TargetId,SimValue>}
 */
public class EdgeToSourceIdTargetIdSimValue implements
  MapFunction<EPGMEdge, Tuple3<GradoopId, GradoopId, Double>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<GradoopId, GradoopId, Double> reuseTuple = new Tuple3<>();

  @Override
  public Tuple3<GradoopId, GradoopId, Double> map(EPGMEdge epgmEdge) throws Exception {
    reuseTuple.f0 = epgmEdge.getSourceId();
    reuseTuple.f1 = epgmEdge.getTargetId();
    reuseTuple.f2 = epgmEdge.getPropertyValue(PropertyNames.SIM_VALUE).getDouble();
    return reuseTuple;
  }
}
