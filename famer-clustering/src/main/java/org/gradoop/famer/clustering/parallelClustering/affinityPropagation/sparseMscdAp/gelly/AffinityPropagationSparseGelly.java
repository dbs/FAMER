/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.Graph;
import org.apache.flink.graph.Vertex;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMElement;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.AbstractAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.ApVertexValueTuple;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.PreprocessingVertexValue;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.AddNoiseToEdges;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.ApComputeStepped;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.ApPreprocessingCompute;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.CalculateLogicalGraphPreference;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.CheckApResultIsValid;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.JoinGellyVertexForAffinityPropagationToVertex;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.SelfEdgeCreationWithPreference;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.VertexToGellyVertexForAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.famer.clustering.parallelClustering.common.functions.EdgeToGellyEdgeCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.ModifyLogicalGraphForClustering;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.AffinityPropagationSparseDS;

/**
 *  The Gradoop/Flink implementation of the Affinity Propagation Algorithm (AP) by Frey and Dueck and its
 *  extension, Multi-Source Clean-Dirty AP (MSCD-AP). This implementation takes benefit of a low density of
 *  the input similarity graph. Messages between data points are only necessary, when an edge is present in
 *  the similarity graph. Only these necessary messages are calculated. That's why it's called Sparse AP.
 *  Different to {@link AffinityPropagationSparseDS}, this implementation uses Flink's Gelly library to
 *  execute AP as a vertex centric iteration.
 *  <p>
 *  <b>original paper of AP:</b> FREY, Brendan J.; DUECK, Delbert. Clustering by passing messages between
 *  data points. science, 2007, 315. Jg., Nr. 5814, S. 972-976.
 */
public class AffinityPropagationSparseGelly extends AbstractAffinityPropagation {

  /**
   * Constructor with default parameter settings.
   */
  public AffinityPropagationSparseGelly() {
    super();
  }

  /**
   * Constructor with default parameter settings and custom random seed for reproducible results.
   *
   * @param randomSeed random seed for reproducible results.
   */
  public AffinityPropagationSparseGelly(long randomSeed) {
    super(randomSeed);
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public AffinityPropagationSparseGelly(JSONObject jsonConfig) {
    super(jsonConfig);
  }

  @Override
  protected String getConfigName() {
    return "apConfig";
  }

  @Override
  protected ApConfig getNewConfig() {
    return new ApConfig();
  }

  @Override
  public String getName() {
    return AffinityPropagationSparseGelly.class.getName();
  }

  /**
   * Execute the sparse (MSCD-) Affinity Propagation clustering algorithm for the passed similarity-graph.
   * Creates a clustered graph with each vertex containing the following properties:
   * <ul>
   *   <li>{@link PropertyNames#CLUSTER_ID}</li>
   *   <li>{@link PropertyNames#IS_CENTER}</li>
   *   <li>{@link PropertyNames#NR_ITERATIONS}</li>
   *   <li>{@link PropertyNames#NR_ADAPTIONS}</li>
   *   <li>{@link PropertyNames#SUM_ITERATIONS}</li>
   * </ul>
   *
   * @param inputGraph Similarity-Graph of the linking step.
   * @return Clustered graph with the vertices containing the clustering information.
   */
  @Override
  public LogicalGraph runClustering(LogicalGraph inputGraph) {

    apConfig.checkConfigCorrectness();

    /* Execute the 4 steps of preprocessing
     * 1) generate unique Long vertexIds for usage as clusterId
     * 2) remove edges from the same clean source
     * 3) get connected components
     * 4) get preference
     */
    LogicalGraph preprocessedGraph = preprocessInputGraph(inputGraph);

    /* Create Gelly graph
     * 5) find all-equal-components and singletons, mark them as converged
     * 6) add self-similarity edges using the preference value
     */
    Graph<GradoopId, ApVertexValueTuple, Double> gellyGraph = prepareGellyGraphForAP(preprocessedGraph);

    /* AP Execution
     * 7) add noise
     * 8) execute the iterative (MSCD-) AP message passing on the prepared gelly graph
     */
    gellyGraph = executeAffinityPropagation(gellyGraph, inputGraph.getConfig().getExecutionEnvironment());

    // check if maxApIterations was exceeded
    // this is done outside of the AP execution for better unit separation and testing
    DataSet<Vertex<GradoopId, ApVertexValueTuple>> gellyVertices = gellyGraph.getVertices()
      .map(new CheckApResultIsValid(apConfig));

    // 9) Transform the clustering result to a LogicalGraph
    DataSet<EPGMVertex> resultVertices = gellyVertices.join(inputGraph.getVertices())
      .where(0).equalTo(EPGMElement::getId)
      .with(new JoinGellyVertexForAffinityPropagationToVertex());

    LogicalGraph resultGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(), resultVertices, inputGraph.getEdges());

    return resultGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  /**
   * Preprocessing of the input graph. Executes the following steps of the clustering procedure:
   * <ul>
   *   <li>1) generate unique Long vertexIds for usage as clusterId</li>
   *   <li>2) remove edges from the same clean source</li>
   *   <li>3) get connected components</li>
   *   <li>4) get preference</li>
   * </ul>
   *
   * @param inputGraph Similarity-Graph of the linking step.
   * @return preprocessed graph, containing connected ids and preference information
   */
  public LogicalGraph preprocessInputGraph(LogicalGraph inputGraph) {
    // 1) set a random unique vertex_priority to each vertex (later used as cluster_id)
    LogicalGraph preprocessedGraph = inputGraph.callForGraph(new ModifyLogicalGraphForClustering());

    // 2) remove edges of two entities from the same clean source
    if (apConfig.isCleanSourceExtensionActivated()) {
      preprocessedGraph = prepareGraphForMSCD(preprocessedGraph);
    }

    // 3) compute connected components
    preprocessedGraph = preprocessedGraph.callForGraph(new ConnectedComponents(maxIteration));

    // 4) get preference
    preprocessedGraph = preprocessedGraph.callForGraph(new CalculateLogicalGraphPreference(apConfig));

    return preprocessedGraph;
  }

  /**
   * Preprocessing of the input graph and transformation to gelly graph that contains two edges between
   * each connected vertex pair, one for each direction, and self-edges.
   * Executes the following steps of the clustering procedure:
   * <ul>
   *   <li>5) Find all-equal-components and singletons, mark them as converged. Prepare gelly graph.</li>
   *   <li>6) Add self-similarity edges using the preference value</li>
   * </ul>
   *
   * @param preprocessedLG preprocessed similarity logical graph, containing preference information in the
   *                       vertex properties
   * @return gelly graph that contains two edges between each connected vertex pair, one for each direction,
   * and self-edges.
   */
  public Graph<GradoopId, ApVertexValueTuple, Double> prepareGellyGraphForAP(LogicalGraph preprocessedLG) {
    // 5) all same similarity? singleton? 2-vertex-component? => generate gelly graph
    // transform Gradoop edges to Gelly edges (two for each edge, if not biDirectional)
    DataSet<Edge<GradoopId, Double>> gellyEdges =
      preprocessedLG.getEdges().flatMap(new EdgeToGellyEdgeCommon(isEdgesBiDirected));

    // transform Gradoop vertices to Gelly vertices
    DataSet<Vertex<GradoopId, ApVertexValueTuple>> gellyVertices =
      preprocessedLG.getVertices().map(new VertexToGellyVertexForAffinityPropagation(apConfig));

    // map gelly vertices to preprocessing vertices for the detection of all-equal-components and singletons
    DataSet<Vertex<GradoopId, PreprocessingVertexValue>> preprocessedGellyVertices = gellyVertices
      .map((MapFunction<Vertex<GradoopId, ApVertexValueTuple>, Vertex<GradoopId, PreprocessingVertexValue>>)
        vertex -> new Vertex<>(vertex.getId(), new PreprocessingVertexValue(vertex.getValue())))
      .returns(new TypeHint<Vertex<GradoopId, PreprocessingVertexValue>>() { });

    /* execute the preprocessing on the gelly graph in a vertex centric iteration
     * => detect singletons and all-equal-components
     * => mark them to be immediately converged
     *
     * Scatter-Gather does not work for vertices without edges (singletons), we need vertex centric iteration.
     */
    Graph<GradoopId, PreprocessingVertexValue, Double> preprocessedGellyGraph = Graph.fromDataSet(
        preprocessedGellyVertices, gellyEdges, preprocessedLG.getConfig().getExecutionEnvironment())
      .runVertexCentricIteration(new ApPreprocessingCompute(apConfig), null, maxIteration,
        ApPreprocessingCompute.getParameters());

    // get preprocessed vertices
    DataSet<Vertex<GradoopId, ApVertexValueTuple>> finalGellyVertices =
      preprocessedGellyGraph.getVertices().map(
        (MapFunction<Vertex<GradoopId, PreprocessingVertexValue>, Vertex<GradoopId, ApVertexValueTuple>>)
        vertex -> new Vertex<>(vertex.getId(), vertex.getValue().getApVertexValueTuple()))
      .returns(new TypeHint<Vertex<GradoopId, ApVertexValueTuple>>() { });

    // 6) Add self-similarity edges using the preference value
    DataSet<Edge<GradoopId, Double>> selfEdges =
      preprocessedLG.getVertices().map(new SelfEdgeCreationWithPreference(apConfig));
    DataSet<Edge<GradoopId, Double>> finalGellyEdges = preprocessedGellyGraph.getEdges().union(selfEdges);

    // create the gelly graph for affinity propagation
    return Graph.fromDataSet(finalGellyVertices, finalGellyEdges,
      preprocessedLG.getConfig().getExecutionEnvironment());
  }

  /**
   * Run (MSCD-) Affinity Propagation message passing on the prepared gelly graph.
   * Executes the following steps of the clustering procedure:
   * <ul>
   *   <li>7) add noise</li>
   *   <li>8) run the iterative (MSCD-) AP message passing</li>
   * </ul>
   *
   * @param gellyGraph Gelly graph that was generated by the AP preprocessing and contains preference and
   *                   connected component information.
   * @param env The Flink execution environment.
   * @return Gelly graph with the clustering result.
   */
  public Graph<GradoopId, ApVertexValueTuple, Double> executeAffinityPropagation(
    Graph<GradoopId, ApVertexValueTuple, Double> gellyGraph, ExecutionEnvironment env) {
    // 7) add noise
    DataSet<Edge<GradoopId, Double>> gellyEdges =
      gellyGraph.getEdges().map(new AddNoiseToEdges(apConfig, random));

    gellyGraph = Graph.fromDataSet(gellyGraph.getVertices(), gellyEdges, env);

    /* One AP iteration takes different number of steps (iterations) depending on the connectivity of the
    graph and the random convergence behavior of the nodes. Thus a determination of the iteration number
    is not possible and we use this generous setting. The message passing stops, when maxApIteration is
    reached, so the iterations limit should be never exceeded. */
    int iterations = 100 * apConfig.getMaxApIteration();

    // 8) run the iterative (MSCD-) AP message passing
    gellyGraph = gellyGraph.runVertexCentricIteration(new ApComputeStepped(apConfig), null, iterations,
      ApComputeStepped.getParameters());

    return gellyGraph;
  }
}
