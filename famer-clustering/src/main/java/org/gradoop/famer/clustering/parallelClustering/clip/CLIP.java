/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.common.functions.FindMaxSimilarityEdges;
import org.gradoop.famer.clustering.common.functions.VertexToVertexClusterId;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.CLIPResolver;
import org.gradoop.famer.clustering.parallelClustering.clip.dataStructures.CLIPConfig;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.AddFakeEdge;
import org.gradoop.famer.clustering.common.functions.Minus;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.EdgeToEdgeEndVertexId;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.EdgeToEdgeEndVertexId.EndType;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.FilterEdgesOnSelectedStatus;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.FilterSourceConsistentClusterVertices;
import org.gradoop.famer.clustering.common.functions.MinusEdgeGroupReducer;
import org.gradoop.famer.clustering.common.functions.MinusVertexGroupReducer;
import org.gradoop.famer.clustering.common.functions.SetIsSelectedStatusAsProperty;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.VertexToVertexVertexId;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.functions.EdgeToEdgeEndIdOtherEndGraphLabel;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.tuple.Value0Of2;

/**
 * The Gradoop/Flink implementation of the CLIP algorithm.
 */
public class CLIP extends AbstractParallelClustering {

  /**
   * Cluster id prefix for phase 1 used in connected components algorithm
   */
  private static final String PREFIX_PHASE1 = "ph1-";

  /**
   * Cluster id prefix for phase 2 used in connected components algorithm
   */
  private static final String PREFIX_PHASE2 = "ph2-";

  /**
   * The output type for the clustering result
   */
  private final ClusteringOutputType clusteringOutputType;

  /**
   * The configuration for the algorithm
   */
  private final CLIPConfig clipConfig;

  /**
   * Number of maximum iterations for the used connected components algorithm
   */
  private final int maxIteration;

  /**
   * Creates an instance of CLIP
   *
   * @param clipConfig The configuration for the algorithm
   * @param clusteringOutputType The output type for the clustering result
   * @param maxIteration Number of maximum iterations for the used connected components algorithm
   */
  public CLIP(CLIPConfig clipConfig, ClusteringOutputType clusteringOutputType, int maxIteration) {
    this.clipConfig = clipConfig;
    this.clusteringOutputType = clusteringOutputType;
    this.maxIteration = maxIteration;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public CLIP(JSONObject jsonConfig) {
    try {
      JSONObject clipConfigObject = jsonConfig.getJSONObject("clipConfig");
      double delta = clipConfigObject.getDouble("delta");
      int sourceNumber = clipConfigObject.getInt("sourceNumber");
      boolean removeSourceConsistentVertices;
      if (clipConfigObject.getString("removeSourceConsistentVertices").equals("true") ||
        clipConfigObject.getString("removeSourceConsistentVertices").equals("false")) {
        removeSourceConsistentVertices =
          clipConfigObject.getBoolean("removeSourceConsistentVertices");
      } else {
        throw new UnsupportedOperationException("Clustering: value for removeSourceConsistentVertices is " +
          "not 'true' or 'false'");
      }
      double simValueCoef = clipConfigObject.getDouble("simValueCoef");
      double degreeCoef = clipConfigObject.getDouble("degreeCoef");
      double strengthCoef = clipConfigObject.getDouble("strengthCoef");

      this.clipConfig = new CLIPConfig(delta, sourceNumber, removeSourceConsistentVertices, simValueCoef,
        degreeCoef, strengthCoef);
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Clustering: clipConfig could not be found or parsed", ex);
    }
    this.clusteringOutputType = getClusteringOutputTypeFromJsonConfig(jsonConfig);
    this.maxIteration = getMaxIterationFromJsonConfig(jsonConfig);
  }

  public ClusteringOutputType getClusteringOutputType() {
    return clusteringOutputType;
  }

  public CLIPConfig getClipConfig() {
    return clipConfig;
  }

  public int getMaxIteration() {
    return maxIteration;
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph inputGraph) {
    // add selected status as property to edge
    DataSet<EPGMEdge> allEdgesWithSelectedStatus =
      new EdgeToEdgeSourceVertexTargetVertex(inputGraph).execute()
        .flatMap(new EdgeToEdgeEndIdOtherEndGraphLabel())
        .groupBy(1, 2)
        .reduceGroup(new FindMaxSimilarityEdges(clipConfig.getDelta()))
        .groupBy(1)
        .reduceGroup(new SetIsSelectedStatusAsProperty());

    // get edges with selected status, former: common.functions.FilterOutSpecificLinks
    DataSet<EPGMEdge> strongEdges = allEdgesWithSelectedStatus.filter(new FilterEdgesOnSelectedStatus(1));

    // get vertices and edges of complete clusters
    LogicalGraph tmpGraph = inputGraph.getConfig().getLogicalGraphFactory()
      .fromDataSets(inputGraph.getGraphHead(), inputGraph.getVertices(), strongEdges);
    tmpGraph = tmpGraph.callForGraph(new ConnectedComponents(maxIteration, PREFIX_PHASE1));
    DataSet<Tuple2<EPGMVertex, GradoopId>> completeVerticesWithId = tmpGraph.getVertices()
      .flatMap(new VertexToVertexClusterId(PropertyNames.CLUSTER_ID, false))
      .groupBy(1)
      .reduceGroup(new FilterSourceConsistentClusterVertices(clipConfig.getSourceNumber()))
      .map(new VertexToVertexVertexId());

    // remove complete vertices
    DataSet<Tuple2<EPGMVertex, GradoopId>> allVerticesWithId =
      inputGraph.getVertices().map(new VertexToVertexVertexId());
    DataSet<EPGMVertex> remainingVertices = new Minus<>(new MinusVertexGroupReducer())
      .execute(allVerticesWithId, completeVerticesWithId);

    // remove edges of complete clusters or related to complete clusters
    DataSet<Tuple2<EPGMEdge, GradoopId>> completeVerticesIdsFakeEdge = completeVerticesWithId
      .map(new AddFakeEdge());
    DataSet<Tuple2<EPGMEdge, GradoopId>> allNonWeakEdgesWithSourceId = allEdgesWithSelectedStatus
      .filter(new FilterEdgesOnSelectedStatus(0))
      .map(new EdgeToEdgeEndVertexId(EndType.SOURCE));
    DataSet<EPGMEdge> tmpNonWeakEdges = new Minus<>(new MinusEdgeGroupReducer())
      .execute(allNonWeakEdgesWithSourceId, completeVerticesIdsFakeEdge);

    DataSet<Tuple2<EPGMEdge, GradoopId>>
      tmpNonWeakEdgesWithTargetId = tmpNonWeakEdges.map(new EdgeToEdgeEndVertexId(EndType.TARGET));
    DataSet<EPGMEdge> remainingEdges = new Minus<>(new MinusEdgeGroupReducer())
      .execute(tmpNonWeakEdgesWithTargetId, completeVerticesIdsFakeEdge);
    tmpGraph = inputGraph.getConfig().getLogicalGraphFactory()
      .fromDataSets(inputGraph.getGraphHead(), remainingVertices, remainingEdges);

    // phase 2 is added here:
    // all source consistent clusters smaller than the given number of sources are removed from the graph
    if (clipConfig.isRemoveSourceConsistentVertices()) {
      tmpGraph = tmpGraph.callForGraph(new ConnectedComponents(maxIteration, PREFIX_PHASE2));

      DataSet<Tuple2<EPGMVertex, GradoopId>> sourceConsistentVerticesWithId = tmpGraph.getVertices()
        .flatMap(new VertexToVertexClusterId(PropertyNames.CLUSTER_ID, false))
        .groupBy(1)
        .reduceGroup(new FilterSourceConsistentClusterVertices())
        .map(new VertexToVertexVertexId());
      allVerticesWithId = tmpGraph.getVertices().map(new VertexToVertexVertexId());
      remainingVertices = new Minus<>(new MinusVertexGroupReducer())
        .execute(allVerticesWithId, sourceConsistentVerticesWithId);

      DataSet<Tuple2<EPGMEdge, GradoopId>> sourceConsistentVerticesIdsFakeEdge =
        sourceConsistentVerticesWithId.map(new AddFakeEdge());
      allNonWeakEdgesWithSourceId = remainingEdges.map(new EdgeToEdgeEndVertexId(EndType.SOURCE));
      tmpNonWeakEdges = new Minus<>(new MinusEdgeGroupReducer())
        .execute(allNonWeakEdgesWithSourceId, sourceConsistentVerticesIdsFakeEdge);
      tmpNonWeakEdgesWithTargetId = tmpNonWeakEdges.map(new EdgeToEdgeEndVertexId(EndType.TARGET));
      remainingEdges = new Minus<>(new MinusEdgeGroupReducer())
        .execute(tmpNonWeakEdgesWithTargetId, sourceConsistentVerticesIdsFakeEdge);

      completeVerticesWithId = completeVerticesWithId.union(sourceConsistentVerticesWithId);

      tmpGraph = inputGraph.getConfig().getLogicalGraphFactory()
        .fromDataSets(inputGraph.getGraphHead(), remainingVertices, remainingEdges);
    }

    tmpGraph = tmpGraph.callForGraph(new CLIPResolver(clipConfig.getSimValueCoef(),
      clipConfig.getStrengthCoef(), maxIteration));

    LogicalGraph resultGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(),
      tmpGraph.getVertices().union(completeVerticesWithId.map(new Value0Of2<>())),
      inputGraph.getEdges());

    return resultGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  @Override
  public String getName() {
    return CLIP.class.getName();
  }
}
