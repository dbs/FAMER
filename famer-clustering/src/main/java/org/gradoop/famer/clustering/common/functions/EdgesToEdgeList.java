/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMEdge;

import java.util.ArrayList;
import java.util.List;

/**
 * Reduces a group of {@code Tuple3<Edge, ClusterId, IsInterEdge>}, grouped on the ClusterId to one
 * {@code Tuple2<List<Tuple2<Edge, IsInterEdge>,ClusterId>} with all edges in the group put to the list.
 */
public class EdgesToEdgeList implements
  GroupReduceFunction<Tuple3<EPGMEdge, String, Boolean>, Tuple2<List<Tuple2<EPGMEdge, Boolean>>, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple2<List<Tuple2<EPGMEdge, Boolean>>, String> reuseTuple = new Tuple2<>();

  @Override
  public void reduce(Iterable<Tuple3<EPGMEdge, String, Boolean>> group,
    Collector<Tuple2<List<Tuple2<EPGMEdge, Boolean>>, String>> out) throws Exception {
    List<Tuple2<EPGMEdge, Boolean>> edgeEdgeTypeList = new ArrayList<>();
    String clusterId = "";

    for (Tuple3<EPGMEdge, String, Boolean> groupItem : group) {
      // TODO: test of List.contains(Object) compares the object content or only the object itself
      if (!edgeEdgeTypeList.contains(Tuple2.of(groupItem.f0, groupItem.f2))) {
        edgeEdgeTypeList.add(Tuple2.of(groupItem.f0, groupItem.f2));
      }
      clusterId = groupItem.f1;
    }

    reuseTuple.f0 = edgeEdgeTypeList;
    reuseTuple.f1 = clusterId;
    out.collect(reuseTuple);
  }
}
