/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;

/**
 * Maps a Gradoop edge to a {@code Tuple2<Edge, EndVertexId>} where the EndVertexId is the id of either the
 * source vertex or the target vertex of the edge, defined in {@link #endType}.
 */
public class EdgeToEdgeEndVertexId implements MapFunction<EPGMEdge, Tuple2<EPGMEdge, GradoopId>> {

  /**
   * Enumeration for the end vertex types
   */
  public enum EndType {
    /**
     * Choose source vertex as end vertex
     */
    SOURCE,
    /**
     * Choose target vertex as end vertex
     */
    TARGET
  }

  /**
   * The end vertex type
   */
  private final EndType endType;

  /**
   * Reduce object instantiation
   */
  private final Tuple2<EPGMEdge, GradoopId> reuseTuple;

  /**
   * Creates an instance of EdgeToEdgeEndVertexId
   *
   * @param endType The end vertex type
   */
  public EdgeToEdgeEndVertexId(EndType endType) {
    this.endType = endType;
    this.reuseTuple = new Tuple2<>();
  }

  @Override
  public Tuple2<EPGMEdge, GradoopId> map(EPGMEdge edge) throws Exception {
    reuseTuple.f0 = edge;
    if (endType == EndType.SOURCE) {
      reuseTuple.f1 = edge.getSourceId();
    } else {
      reuseTuple.f1 = edge.getTargetId();
    }
    return reuseTuple;
  }
}
