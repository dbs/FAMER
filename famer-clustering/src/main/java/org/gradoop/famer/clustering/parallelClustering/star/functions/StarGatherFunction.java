/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.star.functions;

import org.apache.flink.graph.spargel.GatherFunction;
import org.apache.flink.graph.spargel.MessageIterator;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.CenterMessage;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.parallelClustering.star.Star.StarType;

import java.util.Arrays;

/**
 * Gather function used in Star clustering algorithm
 */
public class StarGatherFunction extends GatherFunction<GradoopId, EPGMVertex, CenterMessage> {

  /**
   * Which vertex to select as center based on its vertex priority, see {@link PrioritySelection}
   */
  private final PrioritySelection prioritySelection;

  /**
   * The type of Star algorithm
   */
  private final StarType starType;

  /**
   * Creates an instance of StarGatherFunction
   *
   * @param prioritySelection Which vertex to select as center based on its vertex priority
   * @param starType The type of Star algorithm
   */
  public StarGatherFunction(PrioritySelection prioritySelection, StarType starType) {
    this.prioritySelection = prioritySelection;
    this.starType = starType;
  }

  @Override
  public void updateVertex(org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> vertex,
    MessageIterator<CenterMessage> messages) {
    if (getSuperstepNumber() == 1) { // for the first superstep
      double degree = 0.0;
      int messageCount = 0;
      for (CenterMessage msg : messages) {
        if (msg.getSimDegree() != 0.0) {
          degree += msg.getSimDegree();
          messageCount++;
        }
      }
      if ((starType == StarType.TWO) && (messageCount != 0)) {
        degree /= messageCount;
      }
      vertex.f1.setProperty(PropertyNames.DEGREE, degree);
      setNewVertexValue(vertex.f1);
    } else { // for the following supersteps
      if ((getSuperstepNumber() % 2) == 0) { // for all even superstep numbers
        CenterMessage maxMsg = new CenterMessage(0L, -1.0);
        for (CenterMessage msg : messages) {
          if (msg.getSimDegree() != -1.0) {
            if (msg.getSimDegree() > maxMsg.getSimDegree()) {
              maxMsg = msg;
            } else if (msg.getSimDegree() == maxMsg.getSimDegree()) {
              if (((msg.getClusterId() < maxMsg.getClusterId()) &&
                (prioritySelection == PrioritySelection.MIN)) ||
                ((msg.getClusterId() > maxMsg.getClusterId()) &&
                  (prioritySelection == PrioritySelection.MAX))) {
                maxMsg = msg;
              }
            }
          }
        }
        long vertexPriority =
          vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
        if ((maxMsg.getClusterId() != 0) && (maxMsg.getClusterId() == vertexPriority)) {
          vertex.f1.setProperty(PropertyNames.IS_CENTER, true);
        }
        setNewVertexValue(vertex.f1);
      } else  { // for all odd superstep numbers
        StringBuilder clusterIdsBuilder = new StringBuilder();
        for (CenterMessage msg : messages) {
          if (msg.getAdditionalInfo().contains(PropertyNames.IS_CENTER)) {
            // check for duplicate cluster id entries
            String[] currentClusterIds = clusterIdsBuilder.toString().split(",");
            if (!Arrays.asList(currentClusterIds).contains(String.valueOf(msg.getClusterId()))) {
              clusterIdsBuilder.append(",").append(msg.getClusterId());
            }
          }
        }
        String clusterIds = clusterIdsBuilder.toString();
        if (!clusterIds.equals("")) {
          clusterIds = clusterIds.substring(1);
        }
        vertex.f1.setProperty(PropertyNames.CLUSTER_ID, clusterIds);
        setNewVertexValue(vertex.f1);
      }
    }
  }
}
