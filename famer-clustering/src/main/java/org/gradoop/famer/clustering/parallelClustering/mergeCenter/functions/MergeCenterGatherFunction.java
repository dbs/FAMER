/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.mergeCenter.functions;

import org.apache.flink.api.common.aggregators.LongSumAggregator;
import org.apache.flink.graph.spargel.GatherFunction;
import org.apache.flink.graph.spargel.MessageIterator;
import org.apache.flink.types.LongValue;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.CenterMessage;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Gather function used in the MergeCenter algorithm.
 */
public class MergeCenterGatherFunction extends GatherFunction<GradoopId, EPGMVertex, CenterMessage> {

  /**
   * The name for the finished aggregator
   */
  public static final String HAS_FINISHED_AGGREGATOR = "hasFinished";

  /**
   * The name for the phase 2 aggregator
   */
  public static final String PHASE_2_AGGREGATOR = "phase2";

  /**
   * The name for the phase 3 aggregator
   */
  public static final String PHASE_3_AGGREGATOR = "phase3";

  /**
   * The aggregator to determine if cluster identification has finished
   */
  private LongSumAggregator hasFinishedAggregator = new LongSumAggregator();

  /**
   * The aggregator to determine if phase 2 is running
   */
  private LongSumAggregator phase2Aggregator = new LongSumAggregator();
//  private LongSumAggregator phase3Aggregator = new LongSumAggregator();

  /**
   * Which vertex to select as center based on its vertex priority, see {@link PrioritySelection}
   */
  private final PrioritySelection prioritySelection;

  /**
   * Threshold for the similarity degree which decides if clusters are merged
   */
  private final double simDegMergeThreshold;

  /**
   * Creates an instance of MergeCenterGatherFunction
   *
   * @param prioritySelection Which vertex to select as center based on its vertex priority
   * @param simDegMergeThreshold Threshold for the similarity degree which decides if clusters are merged
   */
  public MergeCenterGatherFunction(PrioritySelection prioritySelection, double simDegMergeThreshold) {
    this.prioritySelection = prioritySelection;
    this.simDegMergeThreshold = simDegMergeThreshold;
  }

  @Override
  public void preSuperstep() {
    hasFinishedAggregator = getIterationAggregator(HAS_FINISHED_AGGREGATOR);
    phase2Aggregator = getIterationAggregator(PHASE_2_AGGREGATOR);
//    phase3Aggregator = getIterationAggregator(PHASE_3_AGGREGATOR);
  }

  @Override
  public void updateVertex(org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> vertex,
    MessageIterator<CenterMessage> messages) {
    int superstepMod2 = getSuperstepNumber() % 2;
    // get previous aggregates
    long previousHasFinishedValue = 0L;
    LongValue previousHasFinishedAggregate = getPreviousIterationAggregate(HAS_FINISHED_AGGREGATOR);
    if (previousHasFinishedAggregate != null) {
      previousHasFinishedValue = previousHasFinishedAggregate.getValue();
    }
    long previousPhase2Value = 0L;
    LongValue previousPhase2Aggregate = getPreviousIterationAggregate(PHASE_2_AGGREGATOR);
    if (previousPhase2Aggregate != null) {
      previousPhase2Value = previousPhase2Aggregate.getValue();
    }
    long previousPhase3Value = 0L;
    LongValue previousPhase3Aggregate = getPreviousIterationAggregate(PHASE_3_AGGREGATOR);
    if (previousPhase3Aggregate != null) {
      previousPhase3Value = previousPhase3Aggregate.getValue();
    }

    if ((getSuperstepNumber() == 1) ||
      ((superstepMod2 == 1) && (previousHasFinishedValue != 0L))) { // for the first or all odd supersteps
      // identify potential centers
      String deciding = "";
      if (!vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean() &&
        !vertex.f1.getPropertyValue(PropertyNames.IS_NON_CENTER).getBoolean()) {
        int state =
          Integer.parseInt(vertex.f1.getPropertyValue(PropertyNames.STATE).toString());
        CenterMessage[] msgArray = fillAndSortMsgArray(messages);
        if (state < msgArray.length) {
          if (msgArray[state].getSimDegree() != -1.0) {
            deciding += msgArray[state].getClusterId() + "," +
              vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).toString();
          } else {
            // handle unconnected vertices, process message from itself and mark it as center
            vertex.f1.setProperty(PropertyNames.IS_CENTER, true);
            vertex.f1.setProperty(PropertyNames.CLUSTER_ID,
              vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong());
          }
        }
      }
      hasFinishedAggregator.aggregate(1L);
      vertex.f1.setProperty(PropertyNames.DECIDING, deciding);
      setNewVertexValue(vertex.f1);
    } else if ((superstepMod2 == 0) && (previousHasFinishedValue != 0)) { // for all even supersteps
      phase2Aggregator.aggregate(1L);
      if (!vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean() &&
        !vertex.f1.getPropertyValue(PropertyNames.IS_NON_CENTER).getBoolean()) {
        int state =
          Integer.parseInt(vertex.f1.getPropertyValue(PropertyNames.STATE).toString());
        CenterMessage[] msgArray = fillAndSortMsgArray(messages);
        for (int i = state; i < msgArray.length; i++) {
          if (msgArray[i].getAdditionalInfo().contains(PropertyNames.IS_CENTER)) {
            vertex.f1.setProperty(PropertyNames.CLUSTER_ID, msgArray[i].getClusterId());
            vertex.f1.setProperty(PropertyNames.IS_NON_CENTER, true);
            break;
          }
          if (msgArray[i].getAdditionalInfo().contains(PropertyNames.IS_NON_CENTER)) {
            vertex.f1.setProperty(PropertyNames.STATE, state + 1);
          } else {
            hasFinishedAggregator.aggregate(1L);
            long selectedClusterId = Long.parseLong(msgArray[i].getAdditionalInfo().split(",")[0]);
            long origin = Long.parseLong(msgArray[i].getAdditionalInfo().split(",")[1]);
            long vertexPriority =
              vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
            if (selectedClusterId == vertexPriority) {
              if (((origin > vertexPriority) && (prioritySelection == PrioritySelection.MIN)) ||
                ((origin < vertexPriority) && (prioritySelection == PrioritySelection.MAX))) {
                vertex.f1.setProperty(PropertyNames.IS_CENTER, true);
                vertex.f1.setProperty(PropertyNames.CLUSTER_ID, vertexPriority);
                vertex.f1.setProperty(PropertyNames.CENTER_DEGREE,
                  msgArray[i].getSimDegree());
              }
              break;
            }
            break;
          }
        }
      }
      setNewVertexValue(vertex.f1);
    } else if (previousPhase2Value != 0L) {
//      phase3Aggregator.aggregate(1);
      CenterMessage selectedMessage = new CenterMessage(Long.MAX_VALUE, 0.0);
      boolean isDiff = false;
      if (vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        for (CenterMessage msg : messages) {
          if ((msg.getSimDegree() >= simDegMergeThreshold) &&
            (msg.getSimDegree() <=
              vertex.f1.getPropertyValue(PropertyNames.CENTER_DEGREE).getDouble()) &&
            (msg.getClusterId() < selectedMessage.getClusterId())) {
            selectedMessage = msg;
          }
        }
      } else {
        long tempClusterId = vertex.f1.getPropertyValue(PropertyNames.CLUSTER_ID).getLong();
        for (CenterMessage msg : messages) {
          if ((msg.getSimDegree() >= simDegMergeThreshold) &&
            msg.getAdditionalInfo().contains(PropertyNames.IS_CENTER) &&
            (msg.getSimDegree() <= Double.parseDouble(msg.getAdditionalInfo().split(",")[1]))) {
            if (msg.getClusterId() != tempClusterId) {
              isDiff = true;
            }
            tempClusterId = msg.getClusterId();
            if (msg.getClusterId() < selectedMessage.getClusterId()) {
              selectedMessage = msg;
            }
          }
        }
      }
      if (selectedMessage.getClusterId() != Long.MAX_VALUE) {
        if (selectedMessage.getClusterId() <
          vertex.f1.getPropertyValue(PropertyNames.CLUSTER_ID).getLong()) {
          vertex.f1.setProperty(PropertyNames.CLUSTER_ID, selectedMessage.getClusterId());
          phase2Aggregator.aggregate(1L);
        }
      }
      if (isDiff) {
        phase2Aggregator.aggregate(1L);
      }
      setNewVertexValue(vertex.f1);
    } else if (previousPhase3Value != 0L) {
      CenterMessage selectedMessage = new CenterMessage(0L, Double.MAX_VALUE);
      if (vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
        for (CenterMessage msg : messages) {
          if (msg.getSimDegree() > selectedMessage.getSimDegree()) {
            selectedMessage = msg;
          } else if (msg.getSimDegree() == selectedMessage.getSimDegree()) {
            if (((msg.getClusterId() < selectedMessage.getClusterId()) &&
              (prioritySelection == PrioritySelection.MIN)) ||
              ((msg.getClusterId() > selectedMessage.getClusterId()) &&
                (prioritySelection == PrioritySelection.MAX))) {
              selectedMessage = msg;
            }
          }
        }
      }
      setNewVertexValue(vertex.f1);
    }
  }

  /**
   * Creates an array of the incoming messages and sorts it based on the similarity degree or, if this
   * is equal, on the vertex priority selection.
   *
   * @param messages The incoming messages
   *
   * @return The sorted array of messages
   */
  private CenterMessage[] fillAndSortMsgArray(MessageIterator<CenterMessage> messages) {
    List<CenterMessage> msgList = new ArrayList<>();
    for (CenterMessage msg : messages) {
      msgList.add(msg);
    }
    CenterMessage[] msgArray = msgList.toArray(new CenterMessage[msgList.size()]);
    Arrays.sort(msgArray, (in1, in2) -> {
      if (in1.getSimDegree() > in2.getSimDegree()) {
        return -1;
      }
      if (in1.getSimDegree() < in2.getSimDegree()) {
        return 1;
      }
      if ((in1.getClusterId() < in2.getClusterId()) && (prioritySelection == PrioritySelection.MIN)) {
        return -1;
      }
      if ((in1.getClusterId() > in2.getClusterId()) && (prioritySelection == PrioritySelection.MAX)) {
        return -1;
      }
      if ((in1.getClusterId() > in2.getClusterId()) && (prioritySelection == PrioritySelection.MIN)) {
        return 1;
      }
      if ((in1.getClusterId() < in2.getClusterId()) && (prioritySelection == PrioritySelection.MAX)) {
        return 1;
      }
      return 0;
    });
    return msgArray;
  }
}
