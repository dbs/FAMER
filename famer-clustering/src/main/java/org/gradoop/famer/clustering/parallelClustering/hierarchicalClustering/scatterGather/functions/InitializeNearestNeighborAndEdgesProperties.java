/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.functions;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.NeighborsFunctionWithVertexValue;
import org.apache.flink.graph.Vertex;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.serialClustering.SerialHierarchicalClustering;

/**
 * Used to initialize the nearest neighbor (nn) and edge property for the scatter gather implementation of
 * hierarchical clustering
 *
 * The edge property is implemented using a HashMap, mapping the center vertex id of each connected cluster to
 * a string value:
 * - for single linkage the string value is the current max similarity of all edges connecting both clusters
 * - for complete linkage the string value is the current min similarity of all edges connecting both
 *   clusters and the number of edges, separated by comma
 * - for average linkage the string value is the current sum of all edge similarities connecting both clusters
 */
public class InitializeNearestNeighborAndEdgesProperties implements
  NeighborsFunctionWithVertexValue<GradoopId, EPGMVertex, Double, Vertex<GradoopId, EPGMVertex>> {

  /**
   * Merge threshold parameter for hierarchical clustering
   */
  private final double threshold;

  /**
   * Which vertex to select as nn if they have the same similarity value
   */
  private final PrioritySelection prioritySelection;

  /**
   * The linkage type used in hierarchical clustering
   */
  private final SerialHierarchicalClustering.LinkageType linkage;

  /**
   * Creates a new instance of InitializeNearestNeighborAndEdgesProperty
   *
   * @param threshold merge threshold parameter for hierarchical clustering
   * @param prioritySelection which vertex to select as nn if they have the same similarity value
   * @param linkage the linkage type used in hierarchical clustering
   */
  public InitializeNearestNeighborAndEdgesProperties(double threshold, PrioritySelection prioritySelection,
    SerialHierarchicalClustering.LinkageType linkage) {
    this.threshold = threshold;
    this.prioritySelection = prioritySelection;
    this.linkage = linkage;
  }

  @Override
  public void iterateNeighbors(Vertex<GradoopId, EPGMVertex> vertex,
    Iterable<Tuple2<Edge<GradoopId, Double>, Vertex<GradoopId, EPGMVertex>>> neighbors,
    Collector<Vertex<GradoopId, EPGMVertex>> out) throws Exception {
    double maxSim = 0d;
    GradoopId nn = null;
    long nnPrio = 0L;

    for (Tuple2<Edge<GradoopId, Double>, Vertex<GradoopId, EPGMVertex>> neighborTuple : neighbors) {
      Edge<GradoopId, Double> neighborEdge = neighborTuple.f0;
      Vertex<GradoopId, EPGMVertex> neighborVertex = neighborTuple.f1;
      double neighborSim = neighborEdge.getValue();
      long neighborPriority = neighborVertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();

      if ((neighborSim > threshold) &&
        ((neighborSim > maxSim) || ((neighborSim == maxSim) &&
          (((prioritySelection == PrioritySelection.MAX) && (nnPrio < neighborPriority)) ||
            ((prioritySelection == PrioritySelection.MIN) && (nnPrio > neighborPriority)))))) {
        maxSim = neighborSim;
        nn = neighborTuple.f1.getId();
        nnPrio = neighborPriority;
      }

      String neighborSimString = Double.toString(neighborSim);
      if (linkage == SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE) {
        neighborSimString = neighborSimString + ",1";
      }
      vertex.f1.getPropertyValue(PropertyNames.EDGES).getMap().put(
        PropertyValue.create(neighborVertex.f0), PropertyValue.create(neighborSimString));
    }
    if (nn != null) {
      vertex.f1.setProperty(PropertyNames.NEAREST_NEIGHBOR, nn);
    }
    out.collect(vertex);
  }
}
