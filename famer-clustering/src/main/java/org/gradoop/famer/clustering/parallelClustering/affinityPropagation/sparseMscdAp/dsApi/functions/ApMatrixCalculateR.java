/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * Calculate the RHO value of the AP multiMatrix as sum of S, ETA and THETA, damped by its old value. The
 * value is set to {@link ApMultiMatrixCell#getR()} AND to {@link ApMultiMatrixCell#getTmp()}, so it can be
 * processed in the columnSum calculation.
 */
public class ApMatrixCalculateR implements MapFunction<ApMultiMatrixCell, ApMultiMatrixCell> {

  /**
   * damps RHO by a ratio of its value from the last iteration
   */
  private final double dampingFactor;

  /**
   * Constructs ApMatrixCalculateR
   *
   * @param dampingFactor damps RHO by a ratio of its value from the last iteration
   */
  public ApMatrixCalculateR(double dampingFactor) {
    this.dampingFactor = dampingFactor;
  }

  @Override
  public ApMultiMatrixCell map(ApMultiMatrixCell cell) {
    double newR = cell.getS() + cell.getEta() + cell.getTheta();
    // damping
    double oldValueDamped = cell.getR() * dampingFactor;
    newR = (newR * (1 - dampingFactor)) + oldValueDamped;
    cell.setR(newR);
    cell.setTmp(newR);    // set value also to tmp for columnSum calculation
    return cell;
  }
}
