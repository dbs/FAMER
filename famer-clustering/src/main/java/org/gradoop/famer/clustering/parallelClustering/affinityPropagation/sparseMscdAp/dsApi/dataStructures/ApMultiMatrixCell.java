/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures;

import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.ParameterAdaptionUtils;

/**
 * Pojo Class to represent the matrix(rowId,columnId) cells of multiple matrices (alpha, beta, ...) of the
 * MSCD-AP message passing procedure at once. This allows easier calculations using the Flink DataSet-API.
 */
public class ApMultiMatrixCell {

  /**
   * Name of the field {@link #rowId}. Used to access the field by it's name in Flink dataSet operations.
   */
  public static final String ROW_ID_FIELD = "rowId";

  /**
   * Name of the field {@link #columnId}. Used to access the field by it's name in Flink dataSet operations.
   */
  public static final String COLUMN_ID_FIELD = "columnId";

  /**
   * Name of the field {@link #rowCleanSource}. Used to access the field by it's name in Flink dataSet
   * operations.
   */
  public static final String ROW_CLEAN_SOURCE_FIELD = "rowCleanSource";

  /**
   * Name of the field {@link #connectedComponentId}. Used to access the field by it's name in Flink
   * dataSet operations.
   */
  public static final String CONNECTED_COMP_ID_FIELD = "connectedComponentId";

  /**
   * RowId of this MultiMatrix-Cell. Equal to the i of Matrix_ij.
   */
  private GradoopId rowId;

  /**
   * ColumnId of this MultiMatrix-Cell. Equal to the j of Matrix_ij.
   */
  private GradoopId columnId;

  /**
   * Similarity value between the dataPoint of the row and the dataPoint of the column.
   */
  private double s;

  /**
   * ALPHA_ij value, with i being the {@link #rowId} and j being the {@link #columnId}.
   */
  private double a;

  /**
   * RHO_ij value, with i being the {@link #rowId} and j being the {@link #columnId}.
   */
  private double r;

  /**
   * ETA_ij value, with i being the {@link #rowId} and j being the {@link #columnId}.
   */
  private double eta;

  /**
   * THETA_ij value, with i being the {@link #rowId} and j being the {@link #columnId}.
   */
  private double theta;

  /**
   * Field for temporary values, like GAMMA or BETA
   */
  private double tmp;

  /**
   * Field for temporary values, like row or column max values or sums
   */
  private double tmp2;

  /**
   * Count of the iterations of the MSCD-AP procedure since the parameter values where last time adapted.
   */
  private int adaptionIterCount;

  /**
   * Count the number of parameter adaptions.
   */
  private int numberOfAdaptions;

  /**
   * Count of the overall iterations of the MSCD-AP procedure, including all parameter adaptions.
   */
  private int iterCount;

  /**
   * Name of the connected component of the two vertices, which are represented by the {@link #rowId} and
   * the {@link #columnId}.
   */
  private String connectedComponentId;

  /**
   * Original preference value for dirty source dataPoints (before adaption)
   */
  private double origPrefDirtySrc;

  /**
   * Original preference value for clean source dataPoints (before adaption)
   */
  private double origPrefCleanSrc;

  /**
   * Current preference value for dirty source dataPoints (after adaption)
   */
  private double currentPrefDirtySrc;

  /**
   * Current preference value for dirty source dataPoints (after adaption)
   */
  private double currentPrefCleanSrc;

  /**
   * Current damping factor (after parameter adaption)
   */
  private double currentDamping;

  /**
   * Contains the source of this row's vertex, but only if it is from a clean source. It contains an empty
   * String otherwise.
   */
  private String rowCleanSource;

  /**
   * Flag to mark a cell as processed for different purposes.
   * e.g.: mark cell as being processed by a reduce function - so the results of singleton groups can be
   * filtered.
   */
  private boolean flagActivated;

  /**
   * Only relevant for diagonal elements. It contains the summed decision over the last iterations, whether
   * this dataPoint is an exemplar or not.
   * ExemplarDecisionSum starts with a value of 0. If the entity is an exemplar, a value of 1 is added
   * if the current value is zero or positive. If the value is negative it is reset to 1. If the entity is
   * not an exemplar, a value of -1 is added if the current value is zero or negative.  If the value is
   * positive it is reset to -1.
   * <ul>
   *   <li>e.g. {@link ApConfig#getConvergenceIter} = 15:</li>
   *   <li>exemplarDecisionSum must be 15 or -15 if the decision didn't change over the last 15 iterations.
   *   </li>
   *   <li><b>if exemplarDecisionSum equals 12:</b> The entity was an exemplar for the last 12 iterations.
   *   The algorithm didn't converge by now.</li>
   *   <li><b>if exemplarDecisionSum equals -5:</b> The entity was not an exemplar for the last 5 iterations
   *   </li>
   * </ul>
   */
  private int exemplarDecisionSum;

  /**
   * Constructor for serialization
   */
  public ApMultiMatrixCell() {
  }

  /**
   * Constructs ApMultiMatrixCell
   *
   * @param rowId RowId of this MultiMatrix-Cell. Equal to the i of Matrix_ij.
   * @param columnId ColumnId of this MultiMatrix-Cell. Equal to the j of Matrix_ij.
   */
  public ApMultiMatrixCell(GradoopId rowId, GradoopId columnId) {
    this.rowId = rowId;
    this.columnId = columnId;
  }

  /**
   * Constructs ApMultiMatrixCell
   *
   * @param rowId RowId of this MultiMatrix-Cell. Equal to the i of Matrix_ij.
   * @param columnId ColumnId of this MultiMatrix-Cell. Equal to the j of Matrix_ij.
   * @param s Similarity value between the dataPoint of the row and the dataPoint of the column.
   * @param connectedComponentId Name of the connected component of the two vertices, which are represented
   *                            by the {@link #rowId} and the {@link #columnId}.
   * @param currentDamping initial damping factor
   */
  public ApMultiMatrixCell(GradoopId rowId, GradoopId columnId, double s, String connectedComponentId,
    double currentDamping) {
    this.rowId = rowId;
    this.columnId = columnId;
    this.s = s;
    this.connectedComponentId = connectedComponentId;
    this.currentDamping = currentDamping;
  }

  /**
   * Constructs ApMultiMatrixCell
   *
   * @param rowId RowId of this MultiMatrix-Cell. Equal to the i of Matrix_ij.
   * @param columnId ColumnId of this MultiMatrix-Cell. Equal to the j of Matrix_ij.
   * @param s Similarity value between the dataPoint of the row and the dataPoint of the column.
   * @param a ALPHA_ij value, with i being the {@link #rowId} and j being the {@link #columnId}.
   * @param r RHO_ij value, with i being the {@link #rowId} and j being the {@link #columnId}.
   * @param eta ETA_ij value, with i being the {@link #rowId} and j being the {@link #columnId}.
   * @param theta THETA_ij value, with i being the {@link #rowId} and j being the {@link #columnId}.
   * @param rowCleanSource name of the clean source of the vertex, represented by the {@link #rowId}. Use
   *                       an empty string for dirty source data points.
   * @param currentDamping initial damping factor
   */
  public ApMultiMatrixCell(GradoopId rowId, GradoopId columnId, double s, double a, double r, double eta,
    double theta, String rowCleanSource, double currentDamping) {
    this.rowId = rowId;
    this.columnId = columnId;
    this.s = s;
    this.a = a;
    this.r = r;
    this.eta = eta;
    this.theta = theta;
    this.rowCleanSource = rowCleanSource;
    this.currentDamping = currentDamping;
  }

  /**
   * Reset all calculated values of the inner matrix-cells (ALPHA_ij, RHO_ij, ...). This is useful for
   * parameter adaption, to restart the AP procedure.
   */
  public void resetCalculatedValues() {
    a = 0d;
    r = 0d;
    eta = 0d;
    theta = 0d;
    tmp = 0d;
    tmp2 = 0d;
    adaptionIterCount = 0;
    exemplarDecisionSum = 0;
    flagActivated = false;
  }

  /**
   * Adapt the preference which is the similarity value in {@link #s} of diagonal cells. If all possible
   * preference values tried, adapt the damping factor and reset the preference.
   *
   * @param apConfig Configuration of the AP clustering algorithm, containing the parameter adaption steps
   *
   * @throws ConvergenceException when all possible parameter values are tried.
   */
  public void adaptParameters(ApConfig apConfig) throws ConvergenceException {
    ParameterAdaptionUtils newParams = ParameterAdaptionUtils.getNewParameterValues(
      origPrefDirtySrc, origPrefCleanSrc,
      currentPrefDirtySrc, currentPrefCleanSrc,
      currentDamping, numberOfAdaptions, apConfig);

    currentPrefDirtySrc = newParams.getNewPreferenceDirtySrc();
    currentPrefCleanSrc = newParams.getNewPreferenceCleanSrc();
    currentDamping = newParams.getNewDampingFactor();

    if (isDiagonal()) {
      if (rowCleanSource.equals("")) {
        s = currentPrefDirtySrc;
      } else {
        s = currentPrefCleanSrc;
      }
    }

    numberOfAdaptions++;
  }

  /**
   * Check, whether this cell is a diagonal cell and its dataPoint was selected as an exemplar
   *
   * @return true, when the cell has both: it is a diagonal cell and its dataPoint was selected as an exemplar
   */
  public boolean isDiagonalExemplar() {
    return isDiagonal() && hasCriterionSatisfied();
  }

  /**
   * Check whether the cell is a diagonal cell. {@link #rowId} and {@link #columnId} would be equal.
   *
   * @return true, when the cell is a diagonal cell.
   */
  public boolean isDiagonal() {
    return rowId.equals(columnId);
  }

  /**
   * Check, whether the criterion matrix value C_ij is greater than zero.
   *
   * @return true when the criterion matrix value C_ij is greater than zero.
   */
  public boolean hasCriterionSatisfied() {
    return a + r > 0;
  }

  /**
   * Get a unique key for this cell, consisting of {@link #rowId} and {@link #columnId}.
   *
   * @return unique key for this cell.
   */
  public String getKey() {
    return rowId.toString() + columnId.toString();
  }

  /**
   * Get a string representation of this cell, which contains all information.
   *
   * @return string representation of this cell, which contains all information.
   */
  @Override
  public String toString() {
    return "ApMultiMatrixCell{" + "rowId=" + rowId + ", columnId=" + columnId + ", s=" + s + ", a=" + a +
      ", r=" + r + ", eta=" + eta + ", theta=" + theta + ", tmp=" + tmp + ", tmp2=" + tmp2 +
      ", adaptionIterCount=" + adaptionIterCount + ", numberOfAdaptions=" + numberOfAdaptions +
      ", iterCount=" + iterCount + ", connectedComponentId='" + connectedComponentId + '\'' +
      ", origPrefDirtySrc=" + origPrefDirtySrc + ", origPrefCleanSrc=" + origPrefCleanSrc +
      ", currentPrefDirtySrc=" + currentPrefDirtySrc + ", currentPrefCleanSrc=" + currentPrefCleanSrc +
      ", currentDamping=" + currentDamping + ", rowCleanSource='" + rowCleanSource + '\'' +
      ", flagActivated=" + flagActivated + ", exemplarDecisionSum=" + exemplarDecisionSum + '}';
  }

  public GradoopId getRowId() {
    return rowId;
  }

  public void setRowId(GradoopId rowId) {
    this.rowId = rowId;
  }

  public GradoopId getColumnId() {
    return columnId;
  }

  public void setColumnId(GradoopId columnId) {
    this.columnId = columnId;
  }

  public double getS() {
    return s;
  }

  public void setS(double s) {
    this.s = s;
  }

  public double getA() {
    return a;
  }

  public void setA(double a) {
    this.a = a;
  }

  public double getR() {
    return r;
  }

  public void setR(double r) {
    this.r = r;
  }

  public double getEta() {
    return eta;
  }

  public void setEta(double eta) {
    this.eta = eta;
  }

  public double getTheta() {
    return theta;
  }

  public void setTheta(double theta) {
    this.theta = theta;
  }

  public double getTmp() {
    return tmp;
  }

  public void setTmp(double tmp) {
    this.tmp = tmp;
  }

  public double getTmp2() {
    return tmp2;
  }

  public void setTmp2(double tmp2) {
    this.tmp2 = tmp2;
  }

  public String getRowCleanSource() {
    return rowCleanSource;
  }

  public void setRowCleanSource(String rowCleanSource) {
    this.rowCleanSource = rowCleanSource;
  }

  public boolean isFlagActivated() {
    return flagActivated;
  }

  public void setFlagActivated(boolean flagActivated) {
    this.flagActivated = flagActivated;
  }

  public int getExemplarDecisionSum() {
    return exemplarDecisionSum;
  }

  public void setExemplarDecisionSum(int exemplarDecisionSum) {
    this.exemplarDecisionSum = exemplarDecisionSum;
  }

  public int getAdaptionIterCount() {
    return adaptionIterCount;
  }

  public void setAdaptionIterCount(int adaptionIterCount) {
    this.adaptionIterCount = adaptionIterCount;
  }

  public String getConnectedComponentId() {
    return connectedComponentId;
  }

  public void setConnectedComponentId(String connectedComponentId) {
    this.connectedComponentId = connectedComponentId;
  }

  public double getOrigPrefDirtySrc() {
    return origPrefDirtySrc;
  }

  public void setOrigPrefDirtySrc(double origPrefDirtySrc) {
    this.origPrefDirtySrc = origPrefDirtySrc;
  }

  public int getIterCount() {
    return iterCount;
  }

  public void setIterCount(int iterCount) {
    this.iterCount = iterCount;
  }

  public double getCurrentDamping() {
    return currentDamping;
  }

  public void setCurrentDamping(double currentDamping) {
    this.currentDamping = currentDamping;
  }

  public int getNumberOfAdaptions() {
    return numberOfAdaptions;
  }

  public void setNumberOfAdaptions(int numberOfAdaptions) {
    this.numberOfAdaptions = numberOfAdaptions;
  }

  public double getOrigPrefCleanSrc() {
    return origPrefCleanSrc;
  }

  public void setOrigPrefCleanSrc(double origPrefCleanSrc) {
    this.origPrefCleanSrc = origPrefCleanSrc;
  }

  public double getCurrentPrefDirtySrc() {
    return currentPrefDirtySrc;
  }

  public void setCurrentPrefDirtySrc(double currentPrefDirtySrc) {
    this.currentPrefDirtySrc = currentPrefDirtySrc;
  }

  public double getCurrentPrefCleanSrc() {
    return currentPrefCleanSrc;
  }

  public void setCurrentPrefCleanSrc(double currentPrefCleanSrc) {
    this.currentPrefCleanSrc = currentPrefCleanSrc;
  }
}
