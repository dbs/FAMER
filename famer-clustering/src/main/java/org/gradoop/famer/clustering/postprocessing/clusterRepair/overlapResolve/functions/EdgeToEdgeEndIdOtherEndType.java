/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Maps a {@code Tuple3<Edge, SourceVertex, TargetVertex} to a {@code Tuple3<Edge, SourceId, TargetType>} and
 * {@code Tuple3<Edge, TargetId, SourceType>}, depending on the type defined in the {@code type} property
 * of the source and target vertex.
 */
@FunctionAnnotation.ForwardedFields("f0")
@FunctionAnnotation.ReadFields("f1;f2")
public class EdgeToEdgeEndIdOtherEndType implements
  FlatMapFunction<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>, Tuple3<EPGMEdge, GradoopId, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMEdge, GradoopId, String> reuseTuple = new Tuple3<>();

  @Override
  public void flatMap(Tuple3<EPGMEdge, EPGMVertex, EPGMVertex> edgeSourceVertexTargetVertex,
    Collector<Tuple3<EPGMEdge, GradoopId, String>> out) throws Exception {
    reuseTuple.f0 = edgeSourceVertexTargetVertex.f0;
    String typeValue = "";

    // first output
    reuseTuple.f1 = edgeSourceVertexTargetVertex.f1.getId();
    if (edgeSourceVertexTargetVertex.f2.hasProperty(PropertyNames.GRAPH_LABEL)) {
      typeValue =
        edgeSourceVertexTargetVertex.f2.getPropertyValue(PropertyNames.GRAPH_LABEL).toString();
    }
    reuseTuple.f2 = typeValue;
    out.collect(reuseTuple);

    // second output
    reuseTuple.f1 = edgeSourceVertexTargetVertex.f2.getId();
    if (edgeSourceVertexTargetVertex.f1.hasProperty(PropertyNames.GRAPH_LABEL)) {
      typeValue =
        edgeSourceVertexTargetVertex.f1.getPropertyValue(PropertyNames.GRAPH_LABEL).toString();
    }
    reuseTuple.f2 = typeValue;
    out.collect(reuseTuple);
  }
}
