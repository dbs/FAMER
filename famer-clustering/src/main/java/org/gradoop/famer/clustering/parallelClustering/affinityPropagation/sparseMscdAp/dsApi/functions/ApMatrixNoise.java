/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.NoiseUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

import java.util.Random;

/**
 * Adds random gaussian noise to the similarity values of the {@link ApMultiMatrixCell}s. The noise is
 * shifted by the configured decimal places in {@link ApConfig#getNoiseDecimalPlace()}. Returns the cell
 * with noised similarity value
 */
public class ApMatrixNoise implements MapFunction<ApMultiMatrixCell, ApMultiMatrixCell> {

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering.
   */
  private final ApConfig apConfig;

  /**
   * random number generator to create noise
   */
  private final Random random;

  /**
   * Constructs ApMatrixNoise
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering.
   * @param random random number generator to create noise
   */
  public ApMatrixNoise(ApConfig apConfig, Random random) {
    this.apConfig = apConfig;
    this.random = random;
  }

  @Override
  public ApMultiMatrixCell map(ApMultiMatrixCell cell) {
    if (apConfig.getNoiseDecimalPlace() > 0) {
      cell.setS(cell.getS() + NoiseUtils.getNoise(apConfig, random));
    }
    return cell;
  }
}
