/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures;

import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.AffinityPropagationSparseGelly;

/**
 * Represents the GAMMA-Max information of a column and a clean source in
 * {@link AffinityPropagationSparseGelly}. Extends Flinks tuple class to achieve better performance than
 * using a POJO.
 */
public class GammaMaxTuple extends Tuple3<Double, Double, GradoopId> {

  /**
   * Constructs GammaMaxTuple
   */
  public GammaMaxTuple() {
    super();
  }

  /**
   * Initializes the tuple values
   */
  public void initialize() {
    reset();
  }

  /**
   * Reset the tuple values to the initial values and return this GammaMaxTuple instance. Resetting the
   * existing instance prevents object instantiations and garbage collection.
   *
   * @return this GammaMaxTuple instance with initialized tuple values.
   */
  public GammaMaxTuple reset() {
    this.f0 = Double.NEGATIVE_INFINITY;
    this.f1 = Double.NEGATIVE_INFINITY;
    this.f2 = GradoopId.NULL_VALUE;
    return this;
  }

  /**
   * Get the maximum GAMMA value of the column and the clean source that is represented by this tuple.
   *
   * @return maximum GAMMA value of the column and the clean source that is represented by this tuple.
   */
  public double getGammaMax() {
    return f0;
  }

  /**
   * Set the maximum GAMMA value of the column and the clean source that is represented by this tuple.
   *
   * @param gammaMax maximum GAMMA value of the column and the clean source that is represented by this tuple.
   */
  public void setGammaMax(double gammaMax) {
    this.f0 = gammaMax;
  }

  /**
   * Get the second largest GAMMA value of the column and the clean source that is represented by this tuple.
   *
   * @return second largest GAMMA value of the column and the clean source that is represented by this tuple.
   */
  public double getGammaSecondMax() {
    return f1;
  }

  /**
   * Set the second largest GAMMA value of the column and the clean source that is represented by this tuple.
   *
   * @param gammaSecondMax second largest GAMMA value of the column and the clean source that is represented
   *                       by this tuple.
   */
  public void setGammaSecondMax(double gammaSecondMax) {
    this.f1 = gammaSecondMax;
  }

  /**
   * Get the ID of the vertex, on whose row the GAMMA value of this column and clean source is maximal.
   *
   * @return ID of the vertex, on whose row the GAMMA value of this column and clean source is maximal.
   */
  public GradoopId getGammaMaxId() {
    return f2;
  }

  /**
   * Set the ID of the vertex, on whose row the GAMMA value of this column and clean source is maximal.
   *
   * @param gammaMaxId ID of the vertex, on whose row the GAMMA value of this column and clean source is
   *                   maximal.
   */
  public void setGammaMaxId(GradoopId gammaMaxId) {
    this.f2 = gammaMaxId;
  }
}
