/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.graph.Edge;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.NoiseUtils;

import java.util.Random;

/**
 * Adds random gaussian noise to the similarity values of the gelly edges. The noise is
 * shifted by the configured decimal places in {@link ApConfig#getNoiseDecimalPlace()}.
 */
public class AddNoiseToEdges implements MapFunction<Edge<GradoopId, Double>, Edge<GradoopId, Double>> {

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering.
   */
  private final ApConfig apConfig;

  /**
   * random number generator to create noise
   */
  private final Random random;

  /**
   * Constructs AddNoiseToEdges
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering.
   * @param random random number generator to create noise
   */
  public AddNoiseToEdges(ApConfig apConfig, Random random) {
    this.apConfig = apConfig;
    this.random = random;
  }

  /**
   * Adds random gaussian noise to the similarity values of the edges. The noise is
   * shifted by the configured decimal places in {@link ApConfig#getNoiseDecimalPlace()}.
   *
   * @param edge gelly edge with the edge value being the similarity value between the connected vertices
   * @return the input edge with noise added to its value
   */
  @Override
  public Edge<GradoopId, Double> map(Edge<GradoopId, Double> edge) {
    if (apConfig.getNoiseDecimalPlace() > 0) {
      edge.setValue(edge.getValue() + NoiseUtils.getNoise(apConfig, random));
    }
    return edge;
  }
}
