/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common;

import org.apache.flink.api.java.DataSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.clustering.common.functions.RemoveInterClusterEdges;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.api.operators.UnaryGraphToGraphOperator;

/**
 * Generates a {@link LogicalGraph} of an input {@link LogicalGraph} depending on the output type of the
 * clustering.
 */
public class GenerateOutput implements UnaryGraphToGraphOperator {

  /**
   * The clustering output type
   */
  private final ClusteringOutputType clusteringOutputType;

  /**
   * Creates an instance of GenerateOutput
   *
   * @param clusteringOutputType The clustering output type
   */
  public GenerateOutput(ClusteringOutputType clusteringOutputType) {
    this.clusteringOutputType = clusteringOutputType;
  }

  @Override
  public LogicalGraph execute(LogicalGraph input) {
    switch (clusteringOutputType) {
    case VERTEX_SET:
      return input.getConfig().getLogicalGraphFactory().fromDataSets(input.getVertices());
    case GRAPH:
      return input;
    case GRAPH_COLLECTION:
      DataSet<EPGMEdge> reducedEdges = new EdgeToEdgeSourceVertexTargetVertex(input).execute()
        .flatMap(new RemoveInterClusterEdges());
      return input.getConfig().getLogicalGraphFactory().fromDataSets(input.getVertices(), reducedEdges);
    default:
      throw new IllegalArgumentException("Unknown clustering output type");
    }
  }

  @Override
  public String getName() {
    return GenerateOutput.class.getName();
  }
}
