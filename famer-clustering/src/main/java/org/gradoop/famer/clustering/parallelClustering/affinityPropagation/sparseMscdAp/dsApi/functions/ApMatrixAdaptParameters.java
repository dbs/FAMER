/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;

/**
 * Reset all calculated values of all multiMatrixCells and adapt the preference parameter of diagonal ones.
 * The {@link ConvergenceException} is thrown when {@link ApConfig#getMaxApIteration()} is exceeded, so that
 * the algorithm couldn't find a clustering solution for the connected component in the defined maximum
 * number of iterations
 */
public class ApMatrixAdaptParameters implements MapFunction<ApMultiMatrixCell, ApMultiMatrixCell> {

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering algorithm
   */
  private final ApConfig apConfig;

  /**
   * Constructs ApMatrixAdaptParameters
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering algorithm
   */
  public ApMatrixAdaptParameters(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  @Override
  public ApMultiMatrixCell map(ApMultiMatrixCell cell) throws ConvergenceException {

    if (cell.getIterCount() >= apConfig.getMaxApIteration()) {
      throw new ConvergenceException(
        "The algorithm did not converge for all components in " + apConfig.getMaxApIteration() +
          " iterations");
    }
    // adapt preference parameter
    if (cell.getAdaptionIterCount() >= apConfig.getMaxAdaptionIteration()) {
      cell.resetCalculatedValues();
      try {
        cell.adaptParameters(apConfig);
      } catch (ConvergenceException e) {
        if (apConfig.isSingletonsForUnconvergedComponents()) {
          cell.setFlagActivated(true);
          // set A and R so that the criterion value is > 1 and the cell is an exemplar
          if (cell.isDiagonal()) {
            cell.setA(1);
            cell.setR(1);
          }
        } else {
          throw e;
        }
      }
    }
    return cell;
  }
}
