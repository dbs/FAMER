/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.functions;

import org.apache.flink.graph.Vertex;
import org.apache.flink.graph.spargel.GatherFunction;
import org.apache.flink.graph.spargel.MessageIterator;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.dataStructures.HierarchicalClusteringMessage;
import org.gradoop.famer.clustering.serialClustering.SerialHierarchicalClustering;

/**
 * Gather function for the scatter gather implementation of Hierarchical Clustering
 */
public class HierarchicalClusteringGatherFunction extends
  GatherFunction<GradoopId, EPGMVertex, HierarchicalClusteringMessage> {

  /**
   * The linkage type used in this algorithm
   */
  private final SerialHierarchicalClustering.LinkageType linkageType;

  /**
   * The threshold value that determines until which similarity value clusters will be merged
   */
  private final double threshold;

  /**
   * Which vertex to select as center based on its vertex priority, see {@link PrioritySelection}
   */
  private final PrioritySelection prioritySelection;

  /**
   * Creates a new instance of HierarchicalClusteringGatherFunction
   *
   * @param linkageType the linkage type used in this algorithm
   * @param threshold the threshold value that determines until which similarity value clusters will be merged
   * @param prioritySelection which vertex to select as center based on its vertex priority
   */
  public HierarchicalClusteringGatherFunction(SerialHierarchicalClustering.LinkageType linkageType,
    double threshold, PrioritySelection prioritySelection) {
    this.linkageType = linkageType;
    this.threshold = threshold;
    this.prioritySelection = prioritySelection;
  }

  @Override
  public void updateVertex(Vertex<GradoopId, EPGMVertex> vertex,
    MessageIterator<HierarchicalClusteringMessage> messageIterator) throws Exception {
    long vertexPriority = vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
    PropertyValue edgeMap = vertex.f1.getPropertyValue(PropertyNames.EDGES);
    PropertyValue ownCluster = vertex.f1.getPropertyValue(PropertyNames.CLUSTER_MEMBERS);
    PropertyValue nn = vertex.f1.getPropertyValue(PropertyNames.NEAREST_NEIGHBOR);
    boolean isCenter = vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean();

    switch (getSuperstepNumber() % 4) {
    case 1:
      // check if incoming nn equals own nn and merge if it is
      for (HierarchicalClusteringMessage msg : messageIterator) {
        if (msg.getSender().equals(nn.getGradoopId())) {
          // case 1: other vertex is center
          if (((prioritySelection == PrioritySelection.MAX) && (msg.getVertexPriority() > vertexPriority)) ||
            ((prioritySelection == PrioritySelection.MIN) && (msg.getVertexPriority() < vertexPriority))) {
            vertex.f1.setProperty(PropertyNames.CLUSTER_ID, msg.getSender().toString());
            vertex.f1.setProperty(PropertyNames.IS_CENTER, false);
            vertex.f1.setProperty(PropertyNames.CENTER_ID, msg.getSender());
            vertex.f1.setProperty(PropertyNames.CENTER_CHANGED, true);
            // case 2: vertex is center
          } else {
            // add vertex to list of own cluster
            ownCluster.getSet().add(PropertyValue.create(msg.getSender()));
          }
          // both cases: remove the connecting links between merged clusters
          edgeMap.getMap().remove(PropertyValue.create(msg.getSender()));
        }
      }
      setNewVertexValue(vertex.f1);
      break;
    case 2:
      for (HierarchicalClusteringMessage msg : messageIterator) {
        // ignore fake message
        if (!msg.getSender().equals(vertex.getId())) {
          if (msg.getMessageType() == 0) {
            // if vertex is non-center then message contains new cluster and centerId
            if (!isCenter) {
              vertex.f1.setProperty(PropertyNames.CLUSTER_ID, msg.getNewCenter().toString());
              vertex.f1.setProperty(PropertyNames.CENTER_ID, msg.getNewCenter());
            }
            // if vertex is center, then message contains new cluster members
            if (isCenter) {
              ownCluster.getSet().addAll(msg.getNewClusterMembers());
            }
          } else {
            /* message contains info from neighbor who changed its center status to false,
               so all edges have to be adjusted to only connect the center vertices */
            GradoopId oldCenterId = msg.getSender();
            GradoopId newCenterId = msg.getNewCenter();
            // check if there is already a link to the new center
            if (edgeMap.getMap().containsKey(PropertyValue.create(newCenterId))) {
              String oldValue = edgeMap.getMap().get(PropertyValue.create(oldCenterId)).getString();
              String newPartialValue = edgeMap.getMap().get(PropertyValue.create(newCenterId)).getString();
              String newValue = updateEdgeProperty(oldValue, newPartialValue);
              edgeMap.getMap().put(PropertyValue.create(newCenterId), PropertyValue.create(newValue));
            } else {
              edgeMap.getMap().put(PropertyValue.create(newCenterId),
                edgeMap.getMap().get(PropertyValue.create(oldCenterId)));
            }
            edgeMap.getMap().remove(PropertyValue.create(oldCenterId));
          }
        }
      }
      setNewVertexValue(vertex.f1);
      break;
    case 3:
      // centers update their edge list
      for (HierarchicalClusteringMessage msg : messageIterator) {
        if (!msg.getSender().equals(vertex.getId())) {
          PropertyValue newCenter = PropertyValue.create(msg.getNewCenter());
          // check if theres is already a link to the new center
          if (edgeMap.getMap().containsKey(newCenter)) {
            String newValue =
              updateEdgeProperty(edgeMap.getMap().get(newCenter).getString(), msg.getSimValues());
            edgeMap.getMap().put(newCenter, PropertyValue.create(newValue));
          } else {
            edgeMap.getMap().put(newCenter, PropertyValue.create(msg.getSimValues()));
          }
        } else if (!vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean() &&
          msg.getSender().equals(vertex.getId())) {
          vertex.f1.setProperty(PropertyNames.CENTER_CHANGED, false);
          vertex.f1.removeProperty(PropertyNames.CLUSTER_MEMBERS);
          vertex.f1.removeProperty(PropertyNames.EDGES);
        }
      }
      setNewVertexValue(vertex.f1);
      break;
    case 0:
      // update cluster similarities
      GradoopId newNN = null;
      double maxSim = 0.0;
      long nnPriority = 0L;

      for (HierarchicalClusteringMessage msg : messageIterator) {
        // ignore fake message
        if (!msg.getSender().equals(vertex.getId())) {
          int countAllConnections = (ownCluster.getSet().size() + 1) * msg.getClusterSize();
          String stringValues = edgeMap.getMap().get(PropertyValue.create(msg.getSender())).getString();
          double sim = calculateSimilarity(stringValues, countAllConnections);
          if (((sim > maxSim) && (sim > threshold)) || ((sim == maxSim) && (newNN != null) &&
            ((prioritySelection == PrioritySelection.MAX) && (msg.getVertexPriority() > nnPriority) ||
              (prioritySelection == PrioritySelection.MIN) && (msg.getVertexPriority() < nnPriority)))) {
            maxSim = sim;
            newNN = msg.getSender();
            nnPriority = msg.getVertexPriority();
          }
        }
      }
      if (newNN != null) {
        vertex.f1.setProperty(PropertyNames.NEAREST_NEIGHBOR, newNN);
      } else {
        vertex.f1.removeProperty(PropertyNames.NEAREST_NEIGHBOR);
      }
      setNewVertexValue(vertex.f1);
      break;
    default:
      break;
    }
  }

  /**
   * Method that calculates the similarity of two clusters depending on the algorithms linkage type
   *
   * @param stringValues link value dependent on the linkage, the current max/min for single/complete
   *                     linkage or sum for average linkage, as well as the number of edges for complete
   *                     linkage
   * @param countConnections the number of possible links between the two clusters, i.e. size(cluster1) *
   *                         size (cluster2)
   * @return the new inter-cluster similarity
   */
  private double calculateSimilarity(String stringValues, int countConnections) {
    double sim = 0;
    if (linkageType == SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE) {
      sim = Double.parseDouble(stringValues);
    } else if (linkageType == SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE) {
      double minSim = Double.parseDouble(stringValues.split(",")[0]);
      int count = Integer.parseInt(stringValues.split(",")[1]);
      // new cluster similarity is 0 if not all vertices are connected
      if (countConnections == count) {
        sim = minSim;
      }
    } else if (linkageType == SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE) {
      double sum = Double.parseDouble(stringValues);
      sim = sum / countConnections;
    }
    return sim;
  }

  /**
   * Updates the edges property of a vertex dependent on the linkage strategy by aggregating the edges from
   * two merged clusters:
   *  - for single linkage the value is only updated if the new value is greater than the old one
   *  - for complete linkage the value is only updated if the new value is less than the old one, but the
   *    count of links is updated in any case
   *  - for average linkage the sum of old and new values is calculated
   *
   * @param oldValues a string representing all links of the first cluster
   * @param newValues a string representing all links of the second cluster
   * @return a new aggregated string representing the merged clusters links
   */
  private String updateEdgeProperty(String oldValues, String newValues) {
    String result = null;
    if (linkageType == SerialHierarchicalClustering.LinkageType.SINGLE_LINKAGE) {
      if (Double.parseDouble(oldValues) < Double.parseDouble(newValues)) {
        result = newValues;
      } else {
        result = oldValues;
      }
    } else if (linkageType == SerialHierarchicalClustering.LinkageType.AVERAGE_LINKAGE) {
      double sum = Double.parseDouble(oldValues) + Double.parseDouble(newValues);
      result = String.valueOf(sum);
    } else if (linkageType == SerialHierarchicalClustering.LinkageType.COMPLETE_LINKAGE) {
      double oldSim = Double.parseDouble(oldValues.split(",")[0]);
      double newSim = Double.parseDouble(newValues.split(",")[0]);
      int oldCount = Integer.parseInt(oldValues.split(",")[1]);
      int newCount = Integer.parseInt(newValues.split(",")[1]);
      int count = oldCount + newCount;
      if (oldSim > newSim) {
        result = newSim + "," + count;
      } else {
        result = oldSim + "," + count;
      }
    }
    return result;
  }
}
