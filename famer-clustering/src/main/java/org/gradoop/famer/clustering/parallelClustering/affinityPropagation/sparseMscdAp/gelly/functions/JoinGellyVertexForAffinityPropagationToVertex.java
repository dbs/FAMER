/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.graph.Vertex;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.ApVertexValueTuple;

/**
 * Joins the gelly vertices with the gradoop vertices of the input graph and writes the clustering result
 * information to the epgm vertex properties.
 */
public class JoinGellyVertexForAffinityPropagationToVertex implements
  JoinFunction<Vertex<GradoopId, ApVertexValueTuple>, EPGMVertex, EPGMVertex> {

  @Override
  public EPGMVertex join(Vertex<GradoopId, ApVertexValueTuple> gellyVertex, EPGMVertex epgmVertex) {
    epgmVertex.setProperty(PropertyNames.COMPONENT_ID, gellyVertex.getValue().getComponentId());
    epgmVertex.setProperty(PropertyNames.CLUSTER_ID, gellyVertex.getValue().getClusterId().toString());
    epgmVertex.setProperty(PropertyNames.IS_CENTER, gellyVertex.getValue().isExemplar());
    epgmVertex.setProperty(PropertyNames.NR_ITERATIONS, gellyVertex.getValue().getNumberOfIterations());
    epgmVertex.setProperty(PropertyNames.NR_ADAPTIONS, gellyVertex.getValue().getNumberOfAdaptions());
    epgmVertex.setProperty(PropertyNames.SUM_ITERATIONS, gellyVertex.getValue().getSumOfOverallIterations());

    return epgmVertex;
  }
}
