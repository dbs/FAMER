/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple7;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Group reducer used for CLIP resolving
 *
 * T.f0 = sourceType
 * T.f1 = targetType
 * T.f2 = connectedComponentId
 * T.f3 = edgeId
 * T.f4 = sourceId
 * T.f5 = targetId
 * T.f6 = prioValue
 *
 * @param <T> Tuple type of grouped items
 */
public class CLIPResolveReducer<T extends Tuple7<String, String, String, GradoopId, GradoopId, GradoopId, Double>>
  implements GroupReduceFunction<T, Tuple1<GradoopId>> {

  @Override
  public void reduce(Iterable<T> group, Collector<Tuple1<GradoopId>> out) throws Exception {
    HashMap<String, String> vertexIdToClusterIdMap = new HashMap<>();
    HashMap<String, List<String>> clusterIdToSourcesMap = new HashMap<>();
    for (T groupItem : group) {
      String sourceClusterId = vertexIdToClusterIdMap.get(groupItem.f4.toString());
      String targetClusterId = vertexIdToClusterIdMap.get(groupItem.f5.toString());
      if ((sourceClusterId == null) && (targetClusterId == null)) {
        vertexIdToClusterIdMap.put(groupItem.f4.toString(), groupItem.f4.toString());
        vertexIdToClusterIdMap.put(groupItem.f5.toString(), groupItem.f4.toString());
        List<String> sources = new ArrayList<>();
        sources.add(groupItem.f0);
        sources.add(groupItem.f1);
        clusterIdToSourcesMap.put(groupItem.f4.toString(), sources);
        out.collect(Tuple1.of(groupItem.f3));
      } else if ((sourceClusterId == null) || (targetClusterId == null)) {
        List<String> sources;
        String newVertexId;
        String newSource;
        String clusterId;
        if (sourceClusterId == null) {
          sources = clusterIdToSourcesMap.get(targetClusterId);
          newVertexId = groupItem.f4.toString();
          newSource = groupItem.f0;
          clusterId = targetClusterId;
        } else {
          sources = clusterIdToSourcesMap.get(sourceClusterId);
          newVertexId = groupItem.f5.toString();
          newSource = groupItem.f1;
          clusterId = sourceClusterId;
        }
        if (!sources.contains(newSource)) {
          sources.add(newSource);
          clusterIdToSourcesMap.replace(clusterId, sources);
          vertexIdToClusterIdMap.put(newVertexId, clusterId);
          out.collect(Tuple1.of(groupItem.f3));
        }
      } else {
        List<String> sourceSources = clusterIdToSourcesMap.get(sourceClusterId);
        List<String> targetSources = clusterIdToSourcesMap.get(targetClusterId);
        if (isCompatible(sourceSources, targetSources)) {
          sourceSources.addAll(targetSources);
          clusterIdToSourcesMap.replace(sourceClusterId, sourceSources);
//          clusterIdToSourcesMap.replace(targetClusterId, sourceSources);
          clusterIdToSourcesMap.remove(targetClusterId);
          // convert target cluster id to source target id
          List<String> keyList = new ArrayList<>(vertexIdToClusterIdMap.keySet());
          List<String> changingVertexIds = new ArrayList<>();
          for (int i = keyList.size() - 1; i >= 0; i--) {
            // get key
            String vertexId = keyList.get(i);
            // get value corresponding to key
            String clusterId = vertexIdToClusterIdMap.get(vertexId);
            if (clusterId.equals(targetClusterId)) {
              changingVertexIds.add(vertexId);
            }
          }
          for (String id : changingVertexIds) {
            vertexIdToClusterIdMap.replace(id, sourceClusterId);
          }
          out.collect(Tuple1.of(groupItem.f3));
        }
      }
    }
  }

  /**
   * Checks if any element in the target sources is also part of the source sources.
   *
   * @param sourceSources List of source sources
   * @param targetSources List of target sources
   * @return {@code false} if any target source is also part of the source sources, {@code false} otherwise
   */
  private boolean isCompatible(List<String> sourceSources, List<String> targetSources) {
    for (String source : targetSources) {
      if (sourceSources.contains(source)) {
        return false;
      }
    }
    return true;
  }
}
