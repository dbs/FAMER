/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.graph.Edge;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.CleanSourceInformationUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;

/**
 * Generates a gelly self edge for each epgm vertex, using the preference for clean resp. dirty source data
 * points as edge value.
 */
public class SelfEdgeCreationWithPreference implements MapFunction<EPGMVertex, Edge<GradoopId, Double>> {

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  private final ApConfig apConfig;

  /**
   * Constructs SelfEdgeCreationWithPreference
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  public SelfEdgeCreationWithPreference(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  @Override
  public Edge<GradoopId, Double> map(EPGMVertex vertex) {
    String csInfo = CleanSourceInformationUtils.getVertexCleanSourceInformation(vertex, apConfig);

    if (csInfo.equals("")) {
      return new Edge<>(vertex.getId(), vertex.getId(),
        vertex.getPropertyValue(PropertyNames.PREFERENCE_DIRTY_SOURCE).getDouble());
    } else {
      return new Edge<>(vertex.getId(), vertex.getId(),
        vertex.getPropertyValue(PropertyNames.PREFERENCE_CLEAN_SOURCE).getDouble());
    }
  }
}
