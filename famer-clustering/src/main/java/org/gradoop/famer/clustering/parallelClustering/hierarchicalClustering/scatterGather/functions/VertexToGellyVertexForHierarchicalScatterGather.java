/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.graph.Vertex;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.clustering.common.PropertyNames;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * Maps a Gradoop vertex to a Gelly vertex for the scatter gather iteration used in the hierarchical
 * clustering algorithm.
 */
public class VertexToGellyVertexForHierarchicalScatterGather implements
  MapFunction<EPGMVertex, Vertex<GradoopId, EPGMVertex>> {

  /**
   * Reduce object instantiation
   */
  private final org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> reuseVertex =
    new org.apache.flink.graph.Vertex<>();

  @Override
  public Vertex<GradoopId, EPGMVertex> map(EPGMVertex vertex) throws Exception {
    vertex.setProperty(PropertyNames.CLUSTER_ID, vertex.getId().toString());
    vertex.setProperty(PropertyNames.IS_CENTER, true);
    vertex.setProperty(PropertyNames.CENTER_CHANGED, false);
    vertex.setProperty(PropertyNames.CENTER_ID, vertex.getId());

    Set<PropertyValue> clusterMembers = new HashSet<>();
    vertex.setProperty(PropertyNames.CLUSTER_MEMBERS, clusterMembers);

    Map<PropertyValue, PropertyValue> edges = new HashMap<>();
    vertex.setProperty(PropertyNames.EDGES, edges);

    reuseVertex.setId(vertex.getId());
    reuseVertex.setValue(vertex);
    return reuseVertex;
  }
}
