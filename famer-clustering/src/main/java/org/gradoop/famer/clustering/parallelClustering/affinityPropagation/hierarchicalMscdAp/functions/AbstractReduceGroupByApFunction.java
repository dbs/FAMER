/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.apache.flink.api.java.tuple.Tuple;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApType;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.CleanSourceInformationUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.ParameterAdaptionUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.PreferenceUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms.AbstractMscdAffinityPropagationSeq;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms.MscdAffinityPropagationSeq;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Abstract base class of the GroupReduce-Affinity-Propagation functions. It provides the basic
 * functionality for the execution of the sequential AP implementation {@link MscdAffinityPropagationSeq}
 * in an hierarchical or non-hierarchical use case.
 */
public abstract class AbstractReduceGroupByApFunction implements Serializable {

  /**
   * Configuration of the (MSCD-) AP clustering process.
   */
  protected ApConfig apConfig;

  /**
   * Maps each clean source (by its name) to a list of integer IDs of its elements in the similarity-matrix.
   */
  protected ArrayListMultimap<String, Integer> cleanSourceElementIndices;

  /**
   * Maps each {@link GradoopId} of a vertex to its integer ID in the similarity-matrix.
   */
  protected BiMap<GradoopId, Integer> vertexNumberMap;

  /**
   * Maps each {@link GradoopId} of a vertex to the vertex value. It stores the vertices during the AP
   * process, so they can be enriched by the result afterwards.
   */
  protected Map<GradoopId, EPGMVertex> vertexIdMap;

  /**
   * The similarity-matrix. It is an NxN matrix representation of the similarity-graph.
   */
  protected double[][] simMatrix;

  /**
   * The preference parameter for data points from dirty sources of this connected component.
   */
  protected double preferenceDirtySources;

  /**
   * The preference parameter for data points from clean sources of this connected component.
   */
  protected double preferenceCleanSources;

  /**
   * Contains the connected component id.
   */
  protected String originalClusterId;

  /**
   * The partition of the connected component that shall be clustered. Only relevant for
   * {@link HierarchicalAffinityPropagation}.
   */
  protected Integer originalPartitionId;

  /**
   * Count, how many different parameter configurations (adaptions) are tested for this data point set.
   */
  protected int numberOfAdaptions;

  /**
   * Sum of all iterations over all tries of different parameter configurations.
   */
  protected int sumIterations;

  /**
   * Constructor for AbstractReduceGroupByApFunction
   *
   * @param apConfig Configuration of the (MSCD-) AP clustering process
   */
  public AbstractReduceGroupByApFunction(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  /**
   * Execute the sequential implementation of (MSCD-) Affinity Propagation. If it didn't find a converging
   * solution that satisfied the constraints, adapt the preference and damping parameters. Repeat until a
   * solution was found or all possible parameter values were tried.
   *
   * @return Instance of {@link AbstractMscdAffinityPropagationSeq} that found a solution.
   * @throws ConvergenceException when all possible parameter values are tried and no converging solution
   * could be found.
   */
  protected AbstractMscdAffinityPropagationSeq runAndAdaptAP() throws ConvergenceException {

    double lastPreferenceDirtySrc = preferenceDirtySources;
    double lastPreferenceCleanSrc = preferenceCleanSources;
    double lastDamping = apConfig.getDampingFactor();
    numberOfAdaptions = 0;
    sumIterations = 0;
    AbstractMscdAffinityPropagationSeq ap;

    boolean solutionFound = false;

    ap = new MscdAffinityPropagationSeq(simMatrix, lastPreferenceDirtySrc, lastPreferenceCleanSrc,
      lastDamping, apConfig, cleanSourceElementIndices);

    while (!solutionFound) {
      ap.setPreferenceDirtySrc(lastPreferenceDirtySrc);
      ap.setPreferenceCleanSrc(lastPreferenceCleanSrc);
      ap.setDamping(lastDamping);

      ap.fit();
      sumIterations += ap.getIterCount();

      if (ap.isConverged() && ap.areConstraintsSatisfied()) {
        solutionFound = true;
      } else {

        ParameterAdaptionUtils newParams = ParameterAdaptionUtils.getNewParameterValues(
          preferenceDirtySources,
          preferenceCleanSources,
          lastPreferenceDirtySrc,
          lastPreferenceCleanSrc,
          lastDamping,
          numberOfAdaptions,
          apConfig);

        lastPreferenceDirtySrc = newParams.getNewPreferenceDirtySrc();
        lastPreferenceCleanSrc = newParams.getNewPreferenceCleanSrc();
        lastDamping = newParams.getNewDampingFactor();

        numberOfAdaptions++;
      }
    }

    return ap;
  }

  /**
   * Transform the input similarity edges to a format which can be used by the sequential AP implementation
   * and to use its output to enrich the vertices by the clustering result. Therefore a 2-dimensional array
   * representation of the similarity matrix is generated. A {@link #vertexNumberMap} is generated, to map
   * the GradoopIds of the vertices to their indices in the similarity-array. A {@link #vertexIdMap} helps
   * to store the vertices during the AP process, so they can be enriched by the result. The
   * {@link #cleanSourceElementIndices} are filled to get the source information for MSCD-AP.
   *
   * @param iterable Tuples of the edges of the similarity-graph, which are joined to the vertices and
   *                 contain connected component information.
   */
  protected void transformInput(Iterable<? extends Tuple> iterable) {
    vertexNumberMap = HashBiMap.create();
    vertexIdMap = new HashMap<>();
    cleanSourceElementIndices = ArrayListMultimap.create();
    List<Tuple3<GradoopId, GradoopId, Double>> tmpSimilarities = new ArrayList<>();

    int nextIndex = 0;
    int iterableN = 0;

    for (Tuple tuple : iterable) {
      GradoopId sourceId = tuple.getField(0);
      GradoopId targetId = tuple.getField(1);
      Double simVal = tuple.getField(2);
      String connCompId = tuple.getField(3);
      EPGMVertex sourceVertex = tuple.getField(4);
      EPGMVertex targetVertex = tuple.getField(5);

      if (iterableN == 0) {
        if (getApType().equals(ApType.HIERARCHICAL_AFFINITY_PROPAGATION)) {
          Integer sourcePartitionId = tuple.getField(6);
          this.originalClusterId = connCompId;
          this.originalPartitionId = sourcePartitionId;
        }
      }

      // fill the vertexNumberMap
      if (this.vertexNumberMap.putIfAbsent(sourceId, nextIndex) == null) {
        appendVertexSource(sourceVertex, nextIndex);
        nextIndex++;
      }
      if (this.vertexNumberMap.putIfAbsent(targetId, nextIndex) == null) {
        appendVertexSource(targetVertex, nextIndex);
        nextIndex++;
      }
      iterableN++;

      // fill the vertexIdMap and (for HAP) initialize the vertices as not being exemplars
      fillVertexIdMap(sourceVertex);
      fillVertexIdMap(targetVertex);

      // iterable can be called only once - tmp structure to store similarities for the similarity matrix
      tmpSimilarities.add(Tuple3.of(sourceId, targetId, simVal));
    }

    generateSimilarityMatrixAndSetPreference(vertexNumberMap, tmpSimilarities);
  }

  /**
   * Put the vertex into the vertexId-Map. For HAP initialize the vertices as not being exemplars.
   *
   * @param vertex the vertex to be put into the map.
   */
  private void fillVertexIdMap(EPGMVertex vertex) {
    if (!vertexIdMap.containsKey(vertex.getId())) {
      if (getApType().equals(ApType.HIERARCHICAL_AFFINITY_PROPAGATION)) {
        vertex.setProperty(PropertyNames.IS_CENTER, false);
      }
      vertexIdMap.put(vertex.getId(), vertex);
    }
  }

  /**
   * Retrieve the clean source information of the vertex and, if the vertex is of a clean source, put it to
   * the {@link #cleanSourceElementIndices} map for the usage in the sequential AP implementation.
   *
   * @param vertex The vertex whose source shall be retrieved.
   * @param vertexIndex The index of the vertex for the {@link #cleanSourceElementIndices} map.
   */
  private void appendVertexSource(EPGMVertex vertex, int vertexIndex) {
    String vertexSource = CleanSourceInformationUtils.getVertexCleanSourceInformation(vertex, apConfig);

    if (!vertexSource.equals("")) {
      cleanSourceElementIndices.put(vertexSource, vertexIndex);
    }
  }

  /**
   * Create a NxN similarity matrix S for all the vertices N. The similarity values are taken from the
   * iterable. All combinations that not occur in the iterable stay zero.
   * Retrieve the preference parameters that might by dependant on the similarities.
   *
   * @param vertexNumberMap Bi-directional Map of Gradoop-Vertex-Ids and integer numbers
   * @param iterable Collection of vertice-pairs and their similarity
   */
  private void generateSimilarityMatrixAndSetPreference(BiMap<GradoopId, Integer> vertexNumberMap,
    List<Tuple3<GradoopId, GradoopId, Double>> iterable) {

    // generate similarity matrix
    int n = vertexNumberMap.size();
    simMatrix = new double[n][n];
    double[] similarities = new double[iterable.size()];
    int c = 0;
    for (Tuple3<GradoopId, GradoopId, Double> tuple : iterable) {
      Integer i = vertexNumberMap.get(tuple.f0);
      Integer j = vertexNumberMap.get(tuple.f1);
      simMatrix[i][j] = tuple.f2;
      simMatrix[j][i] = tuple.f2;
      similarities[c] = tuple.f2;
      c++;
    }

    this.preferenceDirtySources =
      PreferenceUtils.getPreferenceValue(apConfig.getPreferenceConfig(), similarities, true);
    this.preferenceCleanSources =
      PreferenceUtils.getPreferenceValue(apConfig.getPreferenceConfig(), similarities, false);
  }

  /**
   * Get the Type of Affinity Propagation, so the base class can distinguish between the execution of AP in
   * a hierarchical or flat structure.
   *
   * @return Type of AP that is executed.
   */
  protected abstract ApType getApType();
}
