/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMElement;
import org.gradoop.common.model.impl.id.GradoopId;

/**
 * Maps a {@code Tuple2<EPGMElement, ElementId>} to a {@code Tuple3<EPGMElement, ElementId, Identification>},
 * with the identification used to distinguish between the elements origin after a later union operation.
 *
 * @param <E> The EPGMElement
 */
@FunctionAnnotation.ForwardedFields("f0;f1")
public class CreateIdentificationTuple<E extends EPGMElement>
  implements MapFunction<Tuple2<E, GradoopId>, Tuple3<E, GradoopId, String>> {

  /**
   * Identification for later distinction
   */
  private final String identification;

  /**
   * Reduce object instantiation
   */
  private final Tuple3<E, GradoopId, String> reuseTuple;

  /**
   * Creates an instance of CreateIdentificationTuple
   *
   * @param identification Identification for later distinction
   */
  public CreateIdentificationTuple(String identification) {
    this.identification = identification;
    this.reuseTuple = new Tuple3<>();
  }

  @Override
  public Tuple3<E, GradoopId, String> map(Tuple2<E, GradoopId> elementElementId) throws Exception {
    reuseTuple.f0 = elementElementId.f0;
    reuseTuple.f1 = elementElementId.f1;
    reuseTuple.f2 = identification;
    return reuseTuple;
  }
}
