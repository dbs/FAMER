/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.FilterFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * Filter the AP multiMatrix by the multi purpose field {@link ApMultiMatrixCell#isFlagActivated()} for the
 * {@link #desiredFlagState}.
 */
public class ApMatrixFlagFilterFunction  implements FilterFunction<ApMultiMatrixCell> {

  /**
   * The boolean state which the {@link ApMultiMatrixCell#isFlagActivated()} should be set to, for the
   * cell to be returned
   */
  private final Boolean desiredFlagState;

  /**
   * Constructs ApMatrixFlagFilterFunction
   *
   * @param desiredFlagState The boolean state which the {@link ApMultiMatrixCell#isFlagActivated()} should
   *                       be set to, for the cell to be returned
   */
  public ApMatrixFlagFilterFunction(boolean desiredFlagState) {
    this.desiredFlagState = desiredFlagState;
  }

  /**
   * Filter the AP multiMatrix for cells whose {@link ApMultiMatrixCell#isFlagActivated()} state equals the
   * {@link #desiredFlagState}.
   *
   * @param cell AP multiMatrix to be filtered
   * @return true, of the {@link ApMultiMatrixCell#isFlagActivated()} state equals the
   * {@link #desiredFlagState}.
   */
  @Override
  public boolean filter(ApMultiMatrixCell cell) {
    return desiredFlagState.equals(cell.isFlagActivated());
  }
}
