/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;

import java.util.ArrayList;
import java.util.List;

/**
 * Reduces a group of {@code Tuple3<Edge, EdgeId, Identifier>} grouped by the EdgeId and returns a number of
 * edges depending on the Identifier.
 */
public class MinusEdgeGroupReducer implements
  GroupReduceFunction<Tuple3<EPGMEdge, GradoopId, String>, EPGMEdge> {

  @Override
  public void reduce(Iterable<Tuple3<EPGMEdge, GradoopId, String>> group, Collector<EPGMEdge> out)
    throws Exception {
    List<EPGMEdge> edges = new ArrayList<>();
    boolean isOut = true;

    for (Tuple3<EPGMEdge, GradoopId, String> groupItem : group) {
      if (groupItem.f2.equals(Minus.SECOND)) {
        isOut = false;
        break;
      } else {
        edges.add(groupItem.f0);
      }
    }

    if (isOut) {
      for (EPGMEdge edge : edges) {
        out.collect(edge);
      }
    }
  }
}
