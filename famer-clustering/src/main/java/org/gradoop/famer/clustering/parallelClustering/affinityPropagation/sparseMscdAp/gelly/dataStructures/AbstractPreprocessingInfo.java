/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.ApPreprocessingCompute;

import java.util.Collection;

/**
 * Abstract base class for messages and vertex values of the vertex centric iteration of sparse MSCD-AP's
 * preprocessing in {@link ApPreprocessingCompute}.
 */
public abstract class AbstractPreprocessingInfo {

  /**
   * The largest vertexId of all vertices of a connected component. This information can be used to
   * determine, whether information propagation reached all vertices of a connected component. When the
   * maxVertexId that a vertex received from it's neighbours (which they received from their neighbours and
   * so on) doesn't get larger for each vertex, then one information was propagated through the complete
   * component.
   */
  private Long componentMaxVertexId;

  /**
   * This value is true, when the all edges of the connected component have the same similarity value. This
   * information gets accumulated and propagated through the vertices of a connected component (see
   * {@link #componentMaxVertexId}).
   */
  private Boolean cumulatedSimilaritiesEqual;

  /**
   * A map of each clean source name and all vertices of each clean source of the connected component. This
   * is necessary for the preprocessing, when an all-equal-component was detected. When all similarities
   * between all data points of a connected component are equal (and lower than
   * {@link ApConfig#getAllSameSimClusteringThreshold()}), all data points form a single cluster. But clean
   * source data points must be separated. So of each clean source, each vertex with the highest vertexId
   * gets into the common cluster. All other vertices of that clean source form singletons.
   */
  private PreprocessingCleanSourceVerticesMap cleanSourceVertices;

  /**
   * Constructs AbstractPreprocessingInfo
   */
  public AbstractPreprocessingInfo() {
    this.cleanSourceVertices = new PreprocessingCleanSourceVerticesMap();
  }

  /**
   * Put the vertexId into the {@link #cleanSourceVertices} map for the specified source
   *
   * @param source name of the vertex's clean source
   * @param vertexId unique long ID of the vertex
   */
  public void putCleanSourceVertex(String source, Long vertexId) {
    cleanSourceVertices.putCleanSourceVertex(source, vertexId);
  }

  /**
   * Add all vertexIds to the {@link #cleanSourceVertices} set of the specified source.
   *
   * @param source clean source to which the vertexIds are mapped to
   * @param vertexIds set of vertexIds belonging to the clean source
   * @return true, if the set changed
   */
  public boolean putAllCleanSourceVertices(String source, Collection<Long> vertexIds) {
    return cleanSourceVertices.putAllCleanSourceVertices(source, vertexIds);
  }

  /**
   * Get the number of vertices for the specified source in {@link #cleanSourceVertices}
   *
   * @param source name of a clean source
   * @return count of vertices for the specified source
   */
  public int getSourceVertexCount(String source) {
    return cleanSourceVertices.getSourceVertexCount(source);
  }

  /**
   * Check whether the vertex is the largest one of its source in {@link #cleanSourceVertices}.
   *
   * @param vertexId Id of the vertex to check
   * @param source source of the vertex to check
   * @return true, if the vertex is the largest one of its source
   */
  public Boolean isVertexIdLargestOfSource(Long vertexId, String source) {
    return cleanSourceVertices.isVertexIdLargestOfSource(vertexId, source);
  }

  public Long getComponentMaxVertexId() {
    return componentMaxVertexId;
  }

  public void setComponentMaxVertexId(Long componentMaxVertexId) {
    this.componentMaxVertexId = componentMaxVertexId;
  }

  public Boolean getCumulatedSimilaritiesEqual() {
    return cumulatedSimilaritiesEqual;
  }

  public void setCumulatedSimilaritiesEqual(Boolean cumulatedSimilaritiesEqual) {
    this.cumulatedSimilaritiesEqual = cumulatedSimilaritiesEqual;
  }

  public PreprocessingCleanSourceVerticesMap getCleanSourceVertices() {
    return cleanSourceVertices;
  }

  public void setCleanSourceVertices(PreprocessingCleanSourceVerticesMap cleanSourceVertices) {
    this.cleanSourceVertices = cleanSourceVertices;
  }
}
