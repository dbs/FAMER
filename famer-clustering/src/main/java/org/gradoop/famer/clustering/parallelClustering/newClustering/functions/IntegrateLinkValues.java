/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.newClustering.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.parallelClustering.newClustering.dataStructures.LinkValue;

import java.util.Iterator;

/**
 * GroupReduce function for new clustering algorithm
 * TODO: was implemented without any description or naming - what does it do, is it fully implemented?
 */
public class IntegrateLinkValues implements
  GroupReduceFunction<Tuple5<GradoopId, EPGMEdge, GradoopId, String, LinkValue>,
    Tuple6<GradoopId, EPGMEdge, GradoopId, String, LinkValue, LinkValue>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple6<GradoopId, EPGMEdge, GradoopId, String, LinkValue, LinkValue> reuseTuple1 =
    new Tuple6<>();

  /**
   * Reduce object instantiation
   */
  private final Tuple6<GradoopId, EPGMEdge, GradoopId, String, LinkValue, LinkValue> reuseTuple2 =
    new Tuple6<>();

  @Override
  public void reduce(Iterable<Tuple5<GradoopId, EPGMEdge, GradoopId, String, LinkValue>> group,
    Collector<Tuple6<GradoopId, EPGMEdge, GradoopId, String, LinkValue, LinkValue>> out) throws Exception {

    /* // old code
       // - may produce an IndexOutOfBounds exception on outputTuples if group.size() > 2
       // - also may return an empty outputTuple, if group.size() == 1
    Tuple6<GradoopId, Edge, GradoopId, String, LinkValue, LinkValue>[] outputTuples = new Tuple6[2];
    LinkValue[] linkValues = new LinkValue[2];
    int i = 0;
    for (Tuple5<GradoopId, Edge, GradoopId, String, LinkValue> in : group) {
      outputTuples[i] = Tuple6.of(in.f0, in.f1, in.f2, in.f3, in.f4, new LinkValue());
      linkValues[i] = in.f4;
      i++;
    }
    outputTuples[0].f5 = linkValues[1];
    outputTuples[1].f5 = linkValues[0];
    out.collect(outputTuples[0]);
    out.collect(outputTuples[1]);
    */

    // alternative code to only get the first two group items
    Iterator<Tuple5<GradoopId, EPGMEdge, GradoopId, String, LinkValue>> groupIterator = group.iterator();
    LinkValue linkValue1 = new LinkValue();
    LinkValue linkValue2 = new LinkValue();
    boolean foundFirst = false;
    boolean foundSecond = false;

    // get first group item
    if (groupIterator.hasNext()) {
      Tuple5<GradoopId, EPGMEdge, GradoopId, String, LinkValue> groupItem = groupIterator.next();
      reuseTuple1.f0 = groupItem.f0;
      reuseTuple1.f1 = groupItem.f1;
      reuseTuple1.f2 = groupItem.f2;
      reuseTuple1.f3 = groupItem.f3;
      reuseTuple1.f4 = groupItem.f4;
      linkValue1 = groupItem.f4;
      foundFirst = true;
    }

    // get second group item
    if (groupIterator.hasNext()) {
      Tuple5<GradoopId, EPGMEdge, GradoopId, String, LinkValue> groupItem = groupIterator.next();
      reuseTuple2.f0 = groupItem.f0;
      reuseTuple2.f1 = groupItem.f1;
      reuseTuple2.f2 = groupItem.f2;
      reuseTuple2.f3 = groupItem.f3;
      reuseTuple2.f4 = groupItem.f4;
      linkValue2 = groupItem.f4;
      foundSecond = true;
    }

    if (foundFirst && foundSecond) {
      reuseTuple1.f5 = linkValue2;
      reuseTuple2.f5 = linkValue1;
      out.collect(reuseTuple1);
      out.collect(reuseTuple2);
    }
  }
}
