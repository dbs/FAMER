/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.serialClusteringOnConnectedComponents.functions;

import org.apache.flink.api.common.functions.CoGroupFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.Vertex;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.serialClustering.SerialHierarchicalClustering;
import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialEdgeComponent;
import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialVertexComponent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * CoGroupFunction that runs the {@link SerialHierarchicalClustering} algorithm on a given set of vertices
 * and edges
 */
public class SerialClusteringOnSubgraph implements
  CoGroupFunction<EPGMVertex, Tuple2<Edge<GradoopId, Double>, Vertex<GradoopId, EPGMVertex>>, EPGMVertex> {

  /**
   * The linkage type used in the algorithm
   */
  private final SerialHierarchicalClustering.LinkageType linkageType;

  /**
   * The threshold (stopping condition for cluster merging) used in the algorithm
   */
  private final double threshold;

  /**
   * Whether edges are bidirectional
   */
  private final boolean isEdgesBiDirected;

  /**
   * Creates an instance of SerialClusteringOnSubgraph
   *
   * @param linkageType the linkage type for the algorithm
   * @param threshold the threshold for the algorithm
   * @param isEdgesBiDirected whether edges are bidirectional
   */
  public SerialClusteringOnSubgraph(SerialHierarchicalClustering.LinkageType linkageType, double threshold,
    boolean isEdgesBiDirected) {
    this.linkageType = linkageType;
    this.threshold = threshold;
    this.isEdgesBiDirected = isEdgesBiDirected;
  }

  @Override
  public void coGroup(Iterable<EPGMVertex> vertices,
    Iterable<Tuple2<Edge<GradoopId, Double>, Vertex<GradoopId, EPGMVertex>>> edges,
    Collector<EPGMVertex> out) {

    List<EPGMVertex> saveIterable = new ArrayList<>();
    SerialHierarchicalClustering shc =
      new SerialHierarchicalClustering("", "", "", linkageType, threshold, isEdgesBiDirected);

    // convert Gradoop objects to serial clustering objects
    for (EPGMVertex vertex : vertices) {
      saveIterable.add(vertex);
      long vertexPriority = vertex.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
      String initialClusterId = vertex.getId().toString();
      SerialVertexComponent svc =
        new SerialVertexComponent(vertexPriority, initialClusterId, false);
      shc.getVertexComponentMap().put(vertex.getId().toString(), svc);
      Set<SerialVertexComponent> clusterVertices = new HashSet<>();
      clusterVertices.add(svc);
      shc.getClusterMembership().put(vertex.getId().toString(), clusterVertices);
    }

    for (Tuple2<Edge<GradoopId, Double>, Vertex<GradoopId, EPGMVertex>> edge: edges) {
      SerialEdgeComponent sec = new SerialEdgeComponent(
        edge.f0.getSource().toString(), edge.f0.getTarget().toString(), edge.f0.getValue());
      shc.getSerialEdgeComponentList().add(sec);
      shc.getClusterDistances().add(sec);
    }

    shc.doSerialClustering();
    HashMap<String, SerialVertexComponent> clusteringResult = shc.getVertexComponentMap();

    // convert result back to Gradoop Vertices
    for (EPGMVertex vertex : saveIterable) {
      String newClusterId = clusteringResult.get(vertex.getId().toString()).getStringClusterId();
      vertex.setProperty(PropertyNames.CLUSTER_ID, newClusterId);
      out.collect(vertex);
    }
  }
}
