/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * Assign data points to their exemplars resulting from the (MSCD-) Affinity Propagation iterative
 * matrix-value calculation. The criterion matrix (ALPHA + RHO) stores the exemplar decision of AP. Assign
 * each data point (represented by a row) to the other data point as it's exemplar (represented by a column),
 * where the criterion value of the row is maximal. If the diagonal element (the data point itself) is an
 * exemplar, assign it to itself. The criterion value must be stored inside of the
 * {@link ApMultiMatrixCell#getTmp()} field.
 * <p>
 * This method is based on {@link ApMatrixMaxValueFunction} to find the highest row value.
 */
public class ApMatrixAssignExemplars extends ApMatrixMaxValueFunction {

  /**
   * Constructs ApMatrixAssignExemplars
   */
  public ApMatrixAssignExemplars() {
    super(true);
  }

  /**
   * Execute the reduce-comparing of multiMatrixCells to find the maximum criterion value of the row. Return
   * the cell which is better suited to be an exemplar.
   *
   * @param cell1 multiMatrixCell with the criterion value in the {@link ApMultiMatrixCell#getTmp()} field
   * @param cell2 multiMatrixCell with the criterion value in the {@link ApMultiMatrixCell#getTmp()} field
   * @return a new multiMatrixCell, representing the one of both wich is better suited to be the rows exemplar
   */
  @Override
  public ApMultiMatrixCell reduce(ApMultiMatrixCell cell1, ApMultiMatrixCell cell2)  {
    ApMultiMatrixCell result;

    // in exemplar assignment: if the row entity is an exemplar itself than it must be assigned to itself
    // => so check first if the diagonal element of this row is an exemplar
    if (cell1.isDiagonalExemplar()) {
      result = new ApMultiMatrixCell(cell1.getRowId(), cell1.getColumnId());
      result.setTmp(cell1.getTmp());

    } else if (cell2.isDiagonalExemplar()) {
      result = new ApMultiMatrixCell(cell2.getRowId(), cell2.getColumnId());
      result.setTmp(cell2.getTmp());

    } else {
      // if the diagonal element of this row is not an exemplar: find the element with the highest a+r
      result = super.reduce(cell1, cell2);
    }

    return result;
  }
}
