/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures;

/**
 * Type of AP that defines, whether AP is used directly or hierarchical in a divide-and-conquer procedure.
 */
public enum ApType {

  /**
   * AP is executed directly on the connected component, without partitioning.
   */
  AFFINITY_PROPAGATION(0),

  /**
   * AP is executed on a partitioned component in a hierarchical structure.
   */
  HIERARCHICAL_AFFINITY_PROPAGATION(1);

  /**
   * Integer value of the enum.
   */
  private final int value;

  /**
   * Constructs the enum by the desired value.
   *
   * @param value integer value of the enum.
   */
  ApType(int value) {
    this.value = value;
  }

  /**
   * Get the integer value of the enum.
   *
   * @return integer value of the enum.
   */
  public int getValue() {
    return value;
  }
}
