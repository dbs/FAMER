/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApExemplarAssignmentType;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.PreferenceConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.functions.GetCleanSourceInfoForEdgeTuple;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.util.Random;

/**
 * The abstract class all Affinity Propagation implementations inherit from.
 */
public abstract class AbstractAffinityPropagation extends AbstractParallelClustering {

  /**
   * Whether edges are bidirectional
   */
  protected final boolean isEdgesBiDirected;

  /**
   * The output type for the clustering result
   */
  protected final ClusteringOutputType clusteringOutputType;

  /**
   * Number of maximum iterations for the used connected components algorithm
   */
  protected final int maxIteration;

  /**
   * Random number generator for noise generation.
   */
  protected final Random random;

  /**
   * All configuration settings that concern the Affinity Propagation algorithm.
   */
  protected ApConfig apConfig;

  /**
   * Constructor with default parameter settings.
   */
  public AbstractAffinityPropagation() {
    this.isEdgesBiDirected = false;
    this.clusteringOutputType = ClusteringOutputType.GRAPH;
    this.maxIteration = Integer.MAX_VALUE;
    this.apConfig = getNewConfig();
    this.random = new Random();
  }

  /**
   * Constructor with default parameter settings and custom random seed for reproducible results.
   *
   * @param randomSeed random seed for reproducible results.
   */
  public AbstractAffinityPropagation(long randomSeed) {
    this.isEdgesBiDirected = false;
    this.clusteringOutputType = ClusteringOutputType.GRAPH;
    this.maxIteration = Integer.MAX_VALUE;
    this.apConfig = getNewConfig();
    this.random = new Random(randomSeed);
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public AbstractAffinityPropagation(JSONObject jsonConfig) {
    this.isEdgesBiDirected = getIsEdgesBiDirectedFromJsonConfig(jsonConfig);
    this.clusteringOutputType = getClusteringOutputTypeFromJsonConfig(jsonConfig);
    this.maxIteration = getMaxIterationFromJsonConfig(jsonConfig);
    this.apConfig = getNewConfig();
    this.random = new Random();

    JSONObject apConfigObject;
    try {
      apConfigObject = jsonConfig.getJSONObject(getConfigName());
    } catch (JSONException ex) {
      throw new UnsupportedOperationException("Clustering: apConfig could not be found or parsed", ex);
    }

    // parse optional config values, given as defaults in apConfig already
    if (apConfigObject.has("maxApIteration")) {
      try {
        apConfig.setMaxApIteration(apConfigObject.getInt("maxApIteration"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for maxApIteration could not be parsed", e);
      }
    }
    if (apConfigObject.has("maxAdaptionIteration")) {
      try {
        apConfig.setMaxAdaptionIteration(apConfigObject.getInt("maxAdaptionIteration"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for maxAdaptionIteration could not be parsed", e);
      }
    }
    if (apConfigObject.has("convergenceIter")) {
      try {
        apConfig.setConvergenceIter(apConfigObject.getInt("convergenceIter"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for convergenceIter could not be parsed", e);
      }
    }
    if (apConfigObject.has("dampingFactor")) {
      try {
        apConfig.setDampingFactor(apConfigObject.getDouble("dampingFactor"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for dampingFactor could not be parsed", e);
      }
    }
    if (apConfigObject.has("dampingAdaptionStep")) {
      try {
        apConfig.setDampingAdaptionStep(apConfigObject.getDouble("dampingAdaptionStep"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for dampingAdaptionStep could not be parsed", e);
      }
    }
    if (apConfigObject.has("allSameSimClusteringThreshold")) {
      try {
        apConfig.setAllSameSimClusteringThreshold(apConfigObject.getDouble(
          "allSameSimClusteringThreshold"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for allSameSimClusteringThreshold could not be parsed", e);
      }
    }
    if (apConfigObject.has("noiseDecimalPlace")) {
      try {
        apConfig.setNoiseDecimalPlace(apConfigObject.getInt("noiseDecimalPlace"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for noiseDecimalPlace could not be parsed", e);
      }
    }
    if (apConfigObject.has("singletonsForUnconvergedComponents")) {
      try {
        apConfig.setSingletonsForUnconvergedComponents(apConfigObject.getBoolean(
          "singletonsForUnconvergedComponents"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for singletonsForUnconvergedComponents could not be parsed", e);
      }
    }
    if (apConfigObject.has("allSourcesClean")) {
      try {
        apConfig.setAllSourcesClean(apConfigObject.getBoolean("allSourcesClean"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for allSourcesClean could not be parsed", e);
      }
    }
    if (apConfigObject.has("sourceDirtinessVertexProperty")) {
      try {
        apConfig.setSourceDirtinessVertexProperty(apConfigObject.getString(
          "sourceDirtinessVertexProperty"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for sourceDirtinessVertexProperty could not be parsed", e);
      }
    }
    if (apConfigObject.has("cleanSources")) {
      try {
        JSONArray configArray = apConfigObject.getJSONArray("cleanSources");
        for (int i = 0; i < configArray.length(); i++) {
          apConfig.addCleanSource(configArray.getString(i));
        }
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for cleanSources could not be parsed", e);
      }
    }
    if (apConfigObject.has("apExemplarAssignmentType")) {
      try {
        apConfig.setApExemplarAssignmentType(ApExemplarAssignmentType.valueOf(
          apConfigObject.getString("apExemplarAssignmentType")));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for apExemplarAssignmentType could not be parsed", e);
      }
    }

    // parse mandatory preference config
    JSONObject prefConfigObject;
    try {
      prefConfigObject = apConfigObject.getJSONObject("preferenceConfig");
    } catch (JSONException ex) {
      throw new UnsupportedOperationException("Clustering: preferenceConfig could not be parsed", ex);
    }
    boolean preferenceUseMinSimilarityDirtySrc;
    boolean preferenceUseMinSimilarityCleanSrc;
    double preferenceFixValueDirtySrc;
    double preferenceFixValueCleanSrc;
    int preferencePercentileDirtySrc;
    int preferencePercentileCleanSrc;
    double preferenceAdaptionStep;

    try {
      preferenceUseMinSimilarityDirtySrc =
        prefConfigObject.getBoolean("preferenceUseMinSimilarityDirtySrc");
    } catch (JSONException e) {
      throw new UnsupportedOperationException(
        "Clustering: value for preferenceUseMinSimilarityDirtySrc could not be parsed", e);
    }
    try {
      preferenceUseMinSimilarityCleanSrc =
        prefConfigObject.getBoolean("preferenceUseMinSimilarityCleanSrc");
    } catch (JSONException e) {
      throw new UnsupportedOperationException(
        "Clustering: value for preferenceUseMinSimilarityCleanSrc could not be parsed", e);
    }
    try {
      preferenceFixValueDirtySrc = prefConfigObject.getDouble("preferenceFixValueDirtySrc");
    } catch (JSONException e) {
      throw new UnsupportedOperationException(
        "Clustering: value for preferenceFixValueDirtySrc could not be parsed", e);
    }
    try {
      preferenceFixValueCleanSrc = prefConfigObject.getDouble("preferenceFixValueCleanSrc");
    } catch (JSONException e) {
      throw new UnsupportedOperationException(
        "Clustering: value for preferenceFixValueCleanSrc could not be parsed", e);
    }
    try {
      preferencePercentileDirtySrc = prefConfigObject.getInt("preferencePercentileDirtySrc");
    } catch (JSONException e) {
      throw new UnsupportedOperationException(
        "Clustering: value for preferencePercentileDirtySrc could not be parsed", e);
    }
    try {
      preferencePercentileCleanSrc = prefConfigObject.getInt("preferencePercentileCleanSrc");
    } catch (JSONException e) {
      throw new UnsupportedOperationException(
        "Clustering: value for preferencePercentileCleanSrc could not be parsed", e);
    }
    try {
      preferenceAdaptionStep = prefConfigObject.getDouble("preferenceAdaptionStep");
    } catch (JSONException e) {
      throw new UnsupportedOperationException(
        "Clustering: value for preferenceAdaptionStep could not be parsed", e);
    }
    // construct a new preference object
    apConfig.setPreferenceConfig(new PreferenceConfig(preferenceUseMinSimilarityDirtySrc,
      preferenceUseMinSimilarityCleanSrc, preferenceFixValueDirtySrc, preferenceFixValueCleanSrc,
      preferencePercentileDirtySrc, preferencePercentileCleanSrc, preferenceAdaptionStep));
  }

  /**
   * Get the name of the config property of the derived Affinity Propagation implementation.
   *
   * @return name of the config property
   */
  protected abstract String getConfigName();

  /**
   * Create a new instance of the config object.
   *
   * @return new instance of the config object
   */
  protected abstract ApConfig getNewConfig();

  /**
   * Remove edges from the graph where both vertices are from the same clean source. This is a requirement
   * of MSCD-AP.
   *
   * @param inputGraph The input similarity graph of the linking step.
   *
   * @return Cleaned input graph for MSCD-AP.
   */
  protected LogicalGraph prepareGraphForMSCD(LogicalGraph inputGraph) {
    // get edges with source and target vertex
    DataSet<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>> edgeSourceTarget =
      new EdgeToEdgeSourceVertexTargetVertex(inputGraph).execute();

    // replace source and target vertex with clean source information
    DataSet<Tuple3<EPGMEdge, String, String>> edgeSourceCsTargetCs =
      edgeSourceTarget.map(new GetCleanSourceInfoForEdgeTuple(apConfig));

    // remove edges with the same clean source
    DataSet<EPGMEdge> cleanedEdges = edgeSourceCsTargetCs.flatMap(
      (FlatMapFunction<Tuple3<EPGMEdge, String, String>, EPGMEdge>) (edgeTuple, out) -> {
        if (edgeTuple.f1.equals("") || !edgeTuple.f1.equals(edgeTuple.f2)) {
          out.collect(edgeTuple.f0);
        }
      }).returns(new TypeHint<EPGMEdge>() { });

    return inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(), inputGraph.getVertices(), cleanedEdges);
  }

  public boolean isEdgesBiDirected() {
    return isEdgesBiDirected;
  }

  public ClusteringOutputType getClusteringOutputType() {
    return clusteringOutputType;
  }

  public int getMaxIteration() {
    return maxIteration;
  }

  public Random getRandom() {
    return random;
  }

  public ApConfig getApConfig() {
    return apConfig;
  }

  public void setApConfig(ApConfig apConfig) {
    this.apConfig = apConfig;
  }
}
