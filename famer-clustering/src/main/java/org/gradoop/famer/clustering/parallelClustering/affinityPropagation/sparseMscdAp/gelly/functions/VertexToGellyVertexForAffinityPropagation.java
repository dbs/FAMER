/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.CleanSourceInformationUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.ApVertexValueTuple;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.AffinityPropagationSparseGelly;

/**
 * Maps a Gradoop vertex to a Gelly vertex for the vertex-centric iteration used in the
 * {@link AffinityPropagationSparseGelly} algorithm. The gelly vertex's value uses a
 * {@link ApVertexValueTuple} that contains important information about the vertex that are used during
 * the clustering procedure.
 */
public class VertexToGellyVertexForAffinityPropagation implements
  MapFunction<EPGMVertex, org.apache.flink.graph.Vertex<GradoopId, ApVertexValueTuple>> {

  /**
   * Reduce object instantiation
   */
  private final org.apache.flink.graph.Vertex<GradoopId, ApVertexValueTuple> reuseVertex =
    new org.apache.flink.graph.Vertex<>();

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  private final ApConfig apConfig;

  /**
   * Constructs VertexToGellyVertexForAffinityPropagation
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  public VertexToGellyVertexForAffinityPropagation(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  @Override
  public org.apache.flink.graph.Vertex<GradoopId, ApVertexValueTuple> map(EPGMVertex vertex) {
    String csInfo = CleanSourceInformationUtils.getVertexCleanSourceInformation(vertex, apConfig);
    ApVertexValueTuple apVertexValueTuple = new ApVertexValueTuple(
      csInfo,
      vertex.getPropertyValue(PropertyNames.CLUSTER_ID).getString(),
      vertex.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong(),
      apConfig.getDampingFactor(),
      vertex.getPropertyValue(PropertyNames.PREFERENCE_DIRTY_SOURCE).getDouble(),
      vertex.getPropertyValue(PropertyNames.PREFERENCE_CLEAN_SOURCE).getDouble());

    reuseVertex.setId(vertex.getId());
    reuseVertex.setValue(apVertexValueTuple);
    return reuseVertex;
  }
}
