/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * JoinFunction to replace an edge target id with its corresponding target vertex on a
 * {@code Tuple3<Edge, SourceVertex, TargetId>}
 */
@FunctionAnnotation.ForwardedFieldsFirst("f0;f1")
public class ReplaceTargetIdWithTargetVertex implements
  JoinFunction<Tuple3<EPGMEdge, EPGMVertex, GradoopId>, EPGMVertex,
    Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>> {

  /**
   * Reduce object instantiation
   */
  private Tuple3<EPGMEdge, EPGMVertex, EPGMVertex> reuseTuple = new Tuple3<>();

  @Override
  public Tuple3<EPGMEdge, EPGMVertex, EPGMVertex> join(
    Tuple3<EPGMEdge, EPGMVertex, GradoopId> edgeSourceVertexTargetId,
    EPGMVertex targetVertex) throws Exception {
    reuseTuple.f0 = edgeSourceVertexTargetId.f0;
    reuseTuple.f1 = edgeSourceVertexTargetId.f1;
    reuseTuple.f2 = targetVertex;
    return reuseTuple;
  }
}
