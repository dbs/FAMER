/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Maps a Gradoop vertex to a {@code Tuple2<Vertex, ClusterId>}.
 */
public class VertexToVertexClusterId implements FlatMapFunction<EPGMVertex, Tuple2<EPGMVertex, String>> {
  /**
   * Whether for each cluster id in a comma-separated String of cluster ids an output tuple should be created
   */
  private final boolean reproduceOverlapped;

  /**
   * Property key to access the cluster id
   */
  private final String clusterIdProperty;

  /**
   * Reduce object instantiation
   */
  private final Tuple2<EPGMVertex, String> reuseTuple;

  /**
   * Creates an instance of VertexToVertexClusterId
   *
   * @param clusterIdProperty The property for the cluster id
   * @param reproduceOverlapped Whether for each cluster id in a comma-separated String of cluster ids an
   *                            output tuple should be created
   */
  public VertexToVertexClusterId(String clusterIdProperty, boolean reproduceOverlapped) {
    this.reproduceOverlapped = reproduceOverlapped;
    this.clusterIdProperty = clusterIdProperty;
    this.reuseTuple = new Tuple2<>();
  }

  @Override
  public void flatMap(EPGMVertex vertex, Collector<Tuple2<EPGMVertex, String>> out) throws Exception {
    reuseTuple.f0 = vertex;
    if (!reproduceOverlapped) {
      reuseTuple.f1 = vertex.getPropertyValue(clusterIdProperty).toString();
      out.collect(reuseTuple);
    } else {
      String[] clusterIds =
        vertex.getPropertyValue(clusterIdProperty).toString().split(",");
      for (String id : clusterIds) {
        reuseTuple.f1 = id;
        out.collect(reuseTuple);
      }
    }
  }
}
