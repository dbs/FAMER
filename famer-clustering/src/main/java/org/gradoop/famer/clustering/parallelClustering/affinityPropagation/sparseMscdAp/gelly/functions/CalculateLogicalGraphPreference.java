/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.operators.base.JoinOperatorBase;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.functions.GetVertexInformation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.functions.PreferencePercentileCalculation;
import org.gradoop.flink.model.api.operators.UnaryGraphToGraphOperator;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * Determines the preference parameter values for each connected component of the graph, considering the
 * {@link #apConfig} and enrich the vertices of the graph by this information, using the properties
 * {@link PropertyNames#PREFERENCE_DIRTY_SOURCE} and {@link PropertyNames#PREFERENCE_CLEAN_SOURCE}.
 */
public class CalculateLogicalGraphPreference implements UnaryGraphToGraphOperator {

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering process
   */
  private final ApConfig apConfig;

  /**
   * Constructs CalculateLogicalGraphPreference
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering process
   */
  public CalculateLogicalGraphPreference(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  @Override
  public LogicalGraph execute(LogicalGraph inputGraph) {
    DataSet<EPGMVertex> verticesWithPreference;

    if (apConfig.getPreferenceConfig().useOnlyFixedPreferenceValues()) {
      // only fix values needed
      verticesWithPreference =
        inputGraph.getVertices().map(new MapPreferenceFixValueToVertex(apConfig.getPreferenceConfig()));
    } else {
      // calculate preference values, bring vertex information to the edges (connCompId,cleanSourceInfo)
      // - first create vertex tuples of Tuple3<VertexId,ConnCompId,SrcInfo>
      DataSet<Tuple3<GradoopId, String, String>> vertexTuples =
        inputGraph.getVertices().map(new GetVertexInformation(apConfig));

      // - join with edges on sourceId and create
      // Tuple5<SourceId(RowId),TargetId(ColumnId),SimValue,ConnCompId,RowCleanSrcInfo>
      DataSet<Tuple5<GradoopId, GradoopId, Double, String, String>> edgeTuples =
        inputGraph.getEdges().join(vertexTuples, JoinOperatorBase.JoinHint.REPARTITION_HASH_SECOND)
          .where(EPGMEdge::getSourceId).equalTo(0)
          .with(new JoinEdgeWithSourceInfo());

      // create Tuple4<ConnCompId,PercentileValue,CleanSourcePercentileValue,MinSimilarity>
      DataSet<Tuple4<String, Double, Double, Double>> preferenceTuples =
        edgeTuples.groupBy(3).reduceGroup(new PreferencePercentileCalculation(apConfig));

      // bring the calculated preferences to the vertex properties
      verticesWithPreference = inputGraph.getVertices().map(
        (MapFunction<EPGMVertex, Tuple2<EPGMVertex, String>>) vertex ->
          Tuple2.of(vertex, vertex.getPropertyValue(PropertyNames.CLUSTER_ID).getString()))
        .returns(new TypeHint<Tuple2<EPGMVertex, String>>() { })
        .leftOuterJoin(preferenceTuples, JoinOperatorBase.JoinHint.BROADCAST_HASH_SECOND)
        .where(1).equalTo(0)
        .with(new PreferenceVertexCreation(apConfig.getPreferenceConfig()));
    }

    return inputGraph.getConfig().getLogicalGraphFactory()
      .fromDataSets(inputGraph.getGraphHead(), verticesWithPreference, inputGraph.getEdges());
  }

  @Override
  public String getName() {
    return CalculateLogicalGraphPreference.class.getName();
  }
}
