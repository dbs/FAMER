/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.util.Collector;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

import java.util.ArrayList;
import java.util.List;

/**
 * Check the {@link ApMultiMatrixCell#getExemplarDecisionSum()} of all cells of a connected component. All
 * cells of a component converge together. When the connected component contains a diagonal element with
 * changed decision over the last {@link #convergenceIter} iterations: set the
 * {@link ApMultiMatrixCell#isFlagActivated()} of all cells of the component to FALSE. The component didn't
 * converge and gets further processed in the next iteration. Otherwise set the flag of all cells to TRUE.
 * The component converged. Returns all input cells, enriched by the flag state that marks if the component
 * has converged.
 */
public class ApMatrixGetConnCompGroupConvergence implements
  GroupReduceFunction<ApMultiMatrixCell, ApMultiMatrixCell> {

  /**
   * number of iteration, which the exemplar decisions of all diagonal cells of a component must be
   * unchanged, so that the component is converged for a solution
   */
  private final int convergenceIter;

  /**
   * Constructs ApMatrixGetConnCompGroupConvergence
   *
   * @param convergenceIter number of iteration, which the exemplar decisions of all diagonal cells of a
   *                        component must be unchanged, so that the component is converged for a solution
   */
  public ApMatrixGetConnCompGroupConvergence(int convergenceIter) {
    this.convergenceIter = convergenceIter;
  }

  @Override
  public void reduce(Iterable<ApMultiMatrixCell> group, Collector<ApMultiMatrixCell> out) {
    boolean groupContainsUnconvergedDiagonalElement = false;
    List<ApMultiMatrixCell> cellsWithUnknownDecision = new ArrayList<>();

    for (ApMultiMatrixCell cell : group) {
      if (cell.isDiagonal()) {
        if ((cell.getExemplarDecisionSum() < convergenceIter) &&
          (cell.getExemplarDecisionSum() > (convergenceIter * -1))) {
          // decision is not constant over the last convergenceIter iterations
          groupContainsUnconvergedDiagonalElement = true;
        }
      }

      if (groupContainsUnconvergedDiagonalElement) {
        // as soon as one unconverged diagonal element is found, the cells can be collected directly
        cell.setFlagActivated(false); // false = not converged => cell goes to next iteration
        out.collect(cell);
      } else {
        // decision could change for these elements => remember them
        cellsWithUnknownDecision.add(cell);
      }
    }

    // at the end collect all remembered cells
    for (ApMultiMatrixCell cell : cellsWithUnknownDecision) {
      cell.setFlagActivated(!groupContainsUnconvergedDiagonalElement);
      out.collect(cell);
    }
  }
}
