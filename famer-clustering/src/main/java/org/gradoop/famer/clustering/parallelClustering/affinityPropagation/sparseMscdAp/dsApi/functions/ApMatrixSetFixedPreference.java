/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.PreferenceConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * Set the fixed preference values from the {@link PreferenceConfig} to the (diagonal)
 * {@link ApMultiMatrixCell}s. The preference is the self-similarity of a data point, so the values are set
 * to the S-attribute. Returns the diagonal cell, enriched by the fixed preference value
 */
public class ApMatrixSetFixedPreference implements MapFunction<ApMultiMatrixCell, ApMultiMatrixCell> {

  /**
   * Configuration of the preference parameter for the (MSCD-) Affinity Propagation algorithm.
   */
  private final PreferenceConfig preferenceConfig;

  /**
   * Constructs ApMatrixSetFixedPreference
   *
   * @param preferenceConfig Configuration of the preference parameter for the (MSCD-) Affinity Propagation
   *                        algorithm
   */
  public ApMatrixSetFixedPreference(PreferenceConfig preferenceConfig) {
    this.preferenceConfig = preferenceConfig;
  }

  @Override
  public ApMultiMatrixCell map(ApMultiMatrixCell cell) {
    if ((cell.getRowCleanSource() == null) || cell.getRowCleanSource().equals("")) {
      // preference value for dirty source entities
      cell.setS(preferenceConfig.getPreferenceFixValueDirtySrc());
    } else {
      // preference value for clean source entities
      cell.setS(preferenceConfig.getPreferenceFixValueCleanSrc());
    }

    cell.setOrigPrefDirtySrc(preferenceConfig.getPreferenceFixValueDirtySrc());
    cell.setOrigPrefCleanSrc(preferenceConfig.getPreferenceFixValueCleanSrc());

    cell.setCurrentPrefDirtySrc(preferenceConfig.getPreferenceFixValueDirtySrc());
    cell.setCurrentPrefCleanSrc(preferenceConfig.getPreferenceFixValueCleanSrc());

    return cell;
  }
}
