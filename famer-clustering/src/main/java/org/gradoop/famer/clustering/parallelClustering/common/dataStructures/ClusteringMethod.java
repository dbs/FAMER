/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.dataStructures;

/**
 * Enumeration for the available clustering algorithms. Each enum value holds the full class name for the
 * corresponding clustering algorithm, which is used in the famer configuration to instantiate the
 * classes with Java reflections.
 */
public enum ClusteringMethod {
  /**
   * Center algorithm
   */
  CENTER("org.gradoop.famer.clustering.parallelClustering.center.Center"),
  /**
   * CLIP algorithm
   */
  CLIP("org.gradoop.famer.clustering.parallelClustering.clip.CLIP"),
  /**
   * Connected components algorithm
   */
  CONNECTED_COMPONENTS("org.gradoop.famer.clustering.parallelClustering.common.connectedComponents" +
    ".ConnectedComponents"),
  /**
   * Correlation algorithm
   */
  CORRELATION_CLUSTERING("org.gradoop.famer.clustering.parallelClustering.correlationClustering" +
    ".CorrelationClustering"),
  /**
   * Limited correlation algorithm
   */
  LIMITED_CORRELATION_CLUSTERING("org.gradoop.famer.clustering.parallelClustering" +
    ".limitedCorrelationClustering.LimitedCorrelationClustering"),
  /**
   * Merge center algorithm
   */
  MERGE_CENTER("org.gradoop.famer.clustering.parallelClustering.mergeCenter.MergeCenter"),
  /**
   * Sparse Multi-Source Clean-Dirty Affinity Propagation algorithm - Dateset-API implementation
   */
  MSCD_AP_SPARSE_DS("org.gradoop.famer.clustering.parallelClustering.affinityPropagation." +
    "sparseMscdAp.dsApi.AffinityPropagationSparseDS"),
  /**
   * Sparse Multi-Source Clean-Dirty Affinity Propagation algorithm - Gelly implementation
   */
  MSCD_AP_SPARSE_GELLY("org.gradoop.famer.clustering.parallelClustering.affinityPropagation." +
    "sparseMscdAp.gelly.AffinityPropagationSparseGelly"),
  /***
   * Multi-Source Clean-Dirty Hierarchical Affinity Propagation algorithm
   */
  MSCD_HAP("org.gradoop.famer.clustering.parallelClustering.affinityPropagation." +
    "hierarchicalMscdAp.HierarchicalAffinityPropagation"),
  /**
   * Star algorithm
   */
  STAR("org.gradoop.famer.clustering.parallelClustering.star.Star"),
  /**
   * Hierarchical Clustering algorithm based on serial implementation on connected components
   */
  HIERARCHICAL("org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering" +
    ".HierarchicalClustering"),
  /**
   * Scatter Gather Hierarchical Clustering algorithm based on RNNs
   */
  RNN_HIERARCHICAL("org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering" +
    ".HierarchicalClusteringScatterGather"),
  /**
   * NONE - if no clustering algorithm needs to be instantiated, used in incremental module
   */
  NONE("");

  /**
   * The String representation for the class name used for reflections
   */
  private final String fullClassName;

  /**
   * Creates an instance of ClusteringMethod
   *
   * @param fullClassName The String representation for the class name used for reflections
   */
  ClusteringMethod(String fullClassName) {
    this.fullClassName = fullClassName;
  }

  /**
   * Returns the package name for the clustering algorithm
   *
   * @return The package name
   */
  public String getFullClassName() {
    return this.fullClassName;
  }
}
