/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.PreferenceConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.PreferenceUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * Set the preference as fixed value or as pre-calculated percentile or minimum value, depending on the
 * {@link PreferenceConfig}, to the (diagonal) {@link ApMultiMatrixCell}s. The preference is the
 * self-similarity of a data point, so the values are set to the S-attribute. Returns the diagonal cell,
 * enriched by the fixed preference value
 */
public class ApMatrixSetPreferenceGeneric implements
  JoinFunction<ApMultiMatrixCell, Tuple4<String, Double, Double, Double>, ApMultiMatrixCell> {

  /**
   * Configuration of the preference parameter for the (MSCD-) Affinity Propagation algorithm.
   */
  private final PreferenceConfig preferenceConfig;

  /**
   * Constructs ApMatrixSetPreferenceGeneric
   *
   * @param preferenceConfig Configuration of the preference parameter for the (MSCD-) Affinity Propagation
   *                        algorithm
   */
  public ApMatrixSetPreferenceGeneric(PreferenceConfig preferenceConfig) {
    this.preferenceConfig = preferenceConfig;
  }

  @Override
  public ApMultiMatrixCell join(ApMultiMatrixCell cell, Tuple4<String, Double, Double, Double> prefTuple) {
    // preference value for dirty source entities
    cell.setOrigPrefDirtySrc(
      PreferenceUtils.getPreferenceValue(preferenceConfig, prefTuple.f1, prefTuple.f3, true));
    cell.setCurrentPrefDirtySrc(cell.getOrigPrefDirtySrc());
    // preference value for clean source entities
    cell.setOrigPrefCleanSrc(
      PreferenceUtils.getPreferenceValue(preferenceConfig, prefTuple.f2, prefTuple.f3, false));
    cell.setCurrentPrefCleanSrc(cell.getOrigPrefCleanSrc());

    if ((cell.getRowCleanSource() == null) || cell.getRowCleanSource().equals("")) {
      cell.setS(cell.getOrigPrefDirtySrc());
    } else {
      cell.setS(cell.getOrigPrefCleanSrc());
    }

    return cell;
  }
}
