/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * Join singleton vertices to the ApMatrix where the vertex id matches the row id. Singletons are vertices
 * where no edge exists. The ApMatrix contains a cell for each vertex that is present in a row or a column.
 * When the join of a vertex finds no ApMatrixCell, then it is a singleton. Create an additional
 * {@link ApMultiMatrixCell} for each singleton. Enrich the clean source information of the existing cells.
 * Set the {@link ApMultiMatrixCell#isFlagActivated()} field to true for singletons and false otherwise.
 */
public class JoinSingletonsToApMatrix implements
  JoinFunction<Tuple3<GradoopId, String, String>, ApMultiMatrixCell, ApMultiMatrixCell> {

  @Override
  public ApMultiMatrixCell join(Tuple3<GradoopId, String, String> vertexTuple,
    ApMultiMatrixCell apMultiMatrixCell) {
    ApMultiMatrixCell result;
    if (apMultiMatrixCell == null) {
      // singleton
      result = new ApMultiMatrixCell(vertexTuple.f0, vertexTuple.f0);
      result.setFlagActivated(true);
    } else {
      // not a singleton
      apMultiMatrixCell.setFlagActivated(false);
      apMultiMatrixCell.setRowCleanSource(vertexTuple.f2);
      result = apMultiMatrixCell;
    }
    return result;
  }
}
