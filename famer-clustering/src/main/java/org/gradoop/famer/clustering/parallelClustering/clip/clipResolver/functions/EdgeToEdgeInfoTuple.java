/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Maps a Gradoop edge to a {@code Tuple4<EdgeId, SourceId, TargetId, PrioValue>}
 */
public class EdgeToEdgeInfoTuple implements
  MapFunction<EPGMEdge, Tuple4<GradoopId, GradoopId, GradoopId, Double>> {

  /**
   * The similarity value coefficient
   */
  private final double simValueCoef;

  /**
   * The strength coefficient
   */
  private final double strengthCoeff;

  /**
   * Reduce object instantiation
   */
  private final Tuple4<GradoopId, GradoopId, GradoopId, Double> reuseTuple;

  /**
   * Creates an instance of EdgeToEdgeInfoTuple
   *
   * @param simValueCoef The similarity value coefficient from the configuration object
   * @param strengthCoeff The strength coefficient from the configuration object
   */
  public EdgeToEdgeInfoTuple(double simValueCoef, double strengthCoeff) {
    this.simValueCoef = simValueCoef;
    this.strengthCoeff = strengthCoeff;
    this.reuseTuple = new Tuple4<>();
  }

  @Override
  public Tuple4<GradoopId, GradoopId, GradoopId, Double> map(EPGMEdge edge) throws Exception {
    double edgeValue =
      Double.parseDouble(edge.getPropertyValue(PropertyNames.SIM_VALUE).toString());
    int isSelectedValue = edge.getPropertyValue(PropertyNames.IS_SELECTED).getInt();
    double prioValue = (simValueCoef * edgeValue) + (strengthCoeff * (double) isSelectedValue);
    reuseTuple.f0 = edge.getId();
    reuseTuple.f1 = edge.getSourceId();
    reuseTuple.f2 = edge.getTargetId();
    reuseTuple.f3 = prioValue;
    return reuseTuple;
  }
}
