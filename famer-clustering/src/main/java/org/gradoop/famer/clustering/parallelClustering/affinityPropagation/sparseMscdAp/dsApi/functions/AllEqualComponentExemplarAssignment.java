/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Assign the vertices of the all-equal-components to their exemplars. All-equal-components are connected
 * components where all similarity values of the edges are equal. If this similarity reaches the
 * {@link #allSameSimClusteringThreshold}, all data points of dirty sources are assigned to the same
 * cluster. Each clean source entity is assigned to a separate cluster. The first is put into the dirty
 * source cluster. One entity of each clean source can be together with one entity of each other clean
 * source. If {@link #allSameSimClusteringThreshold} is not reached, each data point forms a singleton.
 * Input is a group of vertex tuples {@code Tuple4<VertexId,SimValue,ConnCompId,CsInfo>}, grouped by its
 * connected component id. Output are pairs of {@code Tuple2<VertexId,ExemplarId>}
 */
public class AllEqualComponentExemplarAssignment implements
  GroupReduceFunction<Tuple4<GradoopId, Double, String, String>, Tuple2<GradoopId, GradoopId>> {

  /**
   * Defines the threshold for the shared similarity. If it is lower than the threshold, form a singleton
   * for each data point. Otherwise form clusters, separating the clean sources.
   */
  private final double allSameSimClusteringThreshold;

  /**
   * Constructs AllEqualComponentExemplarAssignment
   *
   * @param allSameSimClusteringThreshold threshold for the shared similarity
   */
  public AllEqualComponentExemplarAssignment(double allSameSimClusteringThreshold) {
    this.allSameSimClusteringThreshold = allSameSimClusteringThreshold;
  }

  @Override
  public void reduce(Iterable<Tuple4<GradoopId, Double, String, String>> group,
    Collector<Tuple2<GradoopId, GradoopId>> out) {
    Iterator<Tuple4<GradoopId, Double, String, String>> iterator = group.iterator();
    Multimap<String, Tuple4<GradoopId, Double, String, String>> cleanSourceTuples =
      ArrayListMultimap.create();

    // Contains the IDs of the exemplars of each cluster. As all vertices are equal suited to be the
    // exemplar, the first tuple of the group which is assigned to a new cluster becomes the exemplar
    List<GradoopId> clusterExemplarIds = new ArrayList<>();
    Tuple4<GradoopId, Double, String, String> firstTuple = iterator.next();
    GradoopId firstClusterId = null;

    if (!firstTuple.f3.equals("")) {
      cleanSourceTuples.put(firstTuple.f3, firstTuple);
    } else {
      firstClusterId = firstTuple.f0;
    }

    if (firstTuple.f1 < allSameSimClusteringThreshold) {
      // put each element in its own cluster
      out.collect(getSelfAssignedTuple(firstTuple));
      while (iterator.hasNext()) {
        out.collect(getSelfAssignedTuple(iterator.next()));
      }
    } else {
      /* if no clean source is present take all elements in the same cluster,
       if clean source present:
       one entity of each clean source can be together with one entity of each other clean source
       - all dirtySource entities are assigned to cluster 0
       - each cleanSource source entity is assigned to a separated cluster 0,1,2,...
       - centroid of a cluster becomes the entity that is first assigned to it */
      if (firstTuple.f3.equals("")) {
        // make sure first tuple is collected if it is not from a clean source
        out.collect(getSelfAssignedTuple(firstTuple));
      }

      while (iterator.hasNext()) {
        Tuple4<GradoopId, Double, String, String> nextTuple = iterator.next();
        if (!nextTuple.f3.equals("")) {
          cleanSourceTuples.put(nextTuple.f3, nextTuple);
        } else {
          // put dirty source element into the first cluster
          if (firstClusterId == null) {
            firstClusterId = nextTuple.f0;
            clusterExemplarIds.add(firstClusterId);
          }
          out.collect(Tuple2.of(nextTuple.f0, firstClusterId));
        }
      }
    }

    // for each clean source:
    // => add each vertex to a separated cluster
    for (String src : cleanSourceTuples.keySet()) {
      int currentClusterNr = 0;
      for (Tuple4<GradoopId, Double, String, String> tuple : cleanSourceTuples.get(src)) {
        if (currentClusterNr >= clusterExemplarIds.size()) {
          // open new cluster
          clusterExemplarIds.add(tuple.f0);
          out.collect(getSelfAssignedTuple(tuple));
        } else {
          // add to next existing cluster
          GradoopId clusterId = clusterExemplarIds.get(currentClusterNr);
          out.collect(Tuple2.of(tuple.f0, clusterId));
        }
        currentClusterNr++;
      }
    }
  }

  /**
   * Assign this Vertex to itself as its exemplar. This vertex becomes a singleton.
   *
   * @param tuple tuple of the vertex that becomes a singleton
   *
   * @return a {@code Tuple2<VertexId,ExemplarVertexId>}, where both values are equal
   */
  private Tuple2<GradoopId, GradoopId> getSelfAssignedTuple(Tuple4<GradoopId, Double, String, String> tuple) {
    return Tuple2.of(tuple.f0, tuple.f0);
  }
}
