/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.clip.clipResolver.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Maps a Gradoop vertex to a {@code Tuple3<VertexId,GraphLabel,ClusterId>}
 */
public class VertexToVertexInfoTuple implements MapFunction<EPGMVertex, Tuple3<GradoopId, String, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<GradoopId, String, String> reuseTuple = new Tuple3<>();

  @Override
  public Tuple3<GradoopId, String, String> map(EPGMVertex vertex) throws Exception {
    reuseTuple.f0 = vertex.getId();
    reuseTuple.f1 = vertex.getPropertyValue(PropertyNames.GRAPH_LABEL).toString();
    reuseTuple.f2 = vertex.getPropertyValue(PropertyNames.CLUSTER_ID).toString();
    return reuseTuple;
  }
}
