/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.star.functions;

import org.apache.flink.api.common.aggregators.LongSumAggregator;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.spargel.ScatterFunction;
import org.apache.flink.types.LongValue;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.CenterMessage;
import org.gradoop.famer.clustering.parallelClustering.star.Star.StarType;

/**
 * Scatter function used in Star clustering algorithm
 */
public class StarScatterFunction extends ScatterFunction<GradoopId, EPGMVertex, CenterMessage, Double> {

  /**
   * The aggregator name
   */
  public static final String HAS_FINISHED_AGGREGATOR = "hasFinished";

  /**
   * The aggregator to determine if cluster identification has finished
   */
  private LongSumAggregator hasFinishedAggregator = new LongSumAggregator();

  /**
   * The type of Star algorithm
   */
  private final StarType starType;

  /**
   * Creates an instance of StarScatterFunction
   *
   * @param starType the clustering type
   */
  public StarScatterFunction(StarType starType) {
    this.starType = starType;
  }

  @Override
  public void preSuperstep() {
    hasFinishedAggregator = getIterationAggregator(HAS_FINISHED_AGGREGATOR);
  }

  @Override
  public void sendMessages(org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> vertex) {
    if (getSuperstepNumber() == 1) { // for the first superstep
      // identify vertex degrees
      long vertexPriority =
        vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
      CenterMessage msg = new CenterMessage(vertexPriority, 0.0);
      for (Edge<GradoopId, Double> edge : getEdges()) {
        if (starType == StarType.ONE) {
          msg.setSimDegree(1.0);
        } else if (starType == StarType.TWO) {
          msg.setSimDegree(edge.getValue());
        }
        sendMessageTo(edge.getTarget(), msg);
      }
      msg.setSimDegree(0.0);
      sendMessageTo(vertex.getId(), msg);
    } else { // for the following supersteps
      if ((getSuperstepNumber() % 2) == 0) { // for all even superstep numbers
        // identify centers
        long vertexPriority =
          vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
        CenterMessage msg = new CenterMessage(vertexPriority, -1.0);
        if (vertex.f1.getPropertyValue(PropertyNames.CLUSTER_ID).toString().equals("")) {
          msg.setSimDegree(vertex.f1.getPropertyValue(PropertyNames.DEGREE).getDouble());
          hasFinishedAggregator.aggregate(1L);
        }
        for (Edge<GradoopId, Double> edge : getEdges()) {
          sendMessageTo(edge.getTarget(), msg);
        }
        sendMessageTo(vertex.getId(), msg);
      } else { // for all odd superstep numbers
        // check previous aggregate value first
        long previousHasFinishedValue = 0L;
        LongValue previousHasFinishedAggregate = getPreviousIterationAggregate(HAS_FINISHED_AGGREGATOR);
        if (previousHasFinishedAggregate != null) {
          previousHasFinishedValue = previousHasFinishedAggregate.getValue();
        }
        if (previousHasFinishedValue != 0L) {
          // identify clusters
          long vertexPriority =
            vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
          CenterMessage msg = new CenterMessage(vertexPriority, 0.0);
          if (vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
            msg.setAdditionalInfo(PropertyNames.IS_CENTER);
          }
          for (Edge<GradoopId, Double> edge : getEdges()) {
            sendMessageTo(edge.getTarget(), msg);
          }
          sendMessageTo(vertex.getId(), msg);
        }
      }
    }
  }
}
