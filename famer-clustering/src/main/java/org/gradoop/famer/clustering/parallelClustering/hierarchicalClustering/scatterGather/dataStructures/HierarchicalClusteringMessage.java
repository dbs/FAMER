/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.dataStructures;

import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.properties.PropertyValue;

import java.io.Serializable;
import java.util.Set;

/**
 * Message structure used in the scatter gather implementation of hierarchical clustering
 */
public class HierarchicalClusteringMessage implements Serializable {
  /**
   * Id of the sending vertex
   */
  private GradoopId sender;

  /**
   * Id of the new cluster center
   */
  private GradoopId newCenter;

  /**
   * Similarity values as string
   */
  private String simValues;

  /**
   * Code to distinguish between multiple incoming messages
   */
  private int messageType;

  /**
   * Vertex priority of the sending vertex
   */
  private long vertexPriority;

  /**
   * Cluster size of the sending vertex
   */
  private int clusterSize;

  /**
   * Set of new cluster members
   */
  private Set<PropertyValue> newClusterMembers;

  /**
   * Creates a new instance of HierarchicalClusteringMessage
   *
   * @param sender id of the sending vertex
   * @param vertexPriority vertex priority of the sending vertex
   */
  public HierarchicalClusteringMessage(GradoopId sender, long vertexPriority) {
    this.sender = sender;
    this.vertexPriority = vertexPriority;
    this.newCenter = sender;
    this.simValues = "";
    this.messageType = 0;
    this.clusterSize = 0;
  }

  public GradoopId getSender() {
    return sender;
  }

  public void setSender(GradoopId sender) {
    this.sender = sender;
  }

  public GradoopId getNewCenter() {
    return newCenter;
  }

  public void setNewCenter(GradoopId newCenter) {
    this.newCenter = newCenter;
  }

  public String getSimValues() {
    return simValues;
  }

  public void setSimValues(String simValues) {
    this.simValues = simValues;
  }

  public int getMessageType() {
    return messageType;
  }

  public void setMessageType(int messageType) {
    this.messageType = messageType;
  }

  public long getVertexPriority() {
    return vertexPriority;
  }

  public void setVertexPriority(long vertexPriority) {
    this.vertexPriority = vertexPriority;
  }

  public int getClusterSize() {
    return clusterSize;
  }

  public void setClusterSize(int clusterSize) {
    this.clusterSize = clusterSize;
  }

  public Set<PropertyValue> getNewClusterMembers() {
    return newClusterMembers;
  }

  public void setNewClusterMembers(Set<PropertyValue> newClusterMembers) {
    this.newClusterMembers = newClusterMembers;
  }
}
