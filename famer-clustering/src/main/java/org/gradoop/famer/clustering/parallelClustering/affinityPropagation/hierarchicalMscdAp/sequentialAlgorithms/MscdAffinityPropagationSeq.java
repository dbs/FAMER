/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * NOTICE: THIS FILE HAS BEEN MODIFIED BY Leipzig University (Database Research Group) UNDER COMPLIANCE WITH
 * THE APACHE 2.0 LICENCE FROM THE ORIGINAL WORK OF Taylor G Smith. THE FOLLOWING IS THE COPYRIGHT OF THE
 * ORIGINAL DOCUMENT:
 *
 *
 * Copyright 2015, 2016 Taylor G Smith
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms;

import com.google.common.collect.Multimap;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.MatrixUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.NoiseUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * <p>
 * Sequential implementation of the Multi-Source Clean-Dirty Affinity Propagation clustering algorithm.
 * Based on Affinity Propagation by Frey and Dueck, this extension provides the possibility to cluster
 * dataPoints of multiple duplicate containing (dirty) and duplicate free (clean) sources, while consistently
 * separating all dataPoints of clean sources into different clusters.
 * <p>
 * This implementation is based on the Affinity Propagation implementation in the clust4j project by Taylor G
 * Smith (<a href="https://github.com/tgsmith61591/clust4j">clust4j github project</a>).
 * <p>
 * <b>original paper of AP:</b> FREY, Brendan J.; DUECK, Delbert. Clustering by passing messages between
 * data points. science, 2007, 315. Jg., Nr. 5814, S. 972-976.
 */
public class MscdAffinityPropagationSeq extends AbstractMscdAffinityPropagationSeq {
  /*
   * This class is based on the AffinityPropagation class of the clust4j project by Taylor G Smith.
   * The first license is for the modifications in this project. The second license in this file is the
   * original one. The original source code can be found on:
   *
   *    https://github.com/tgsmith61591/clust4j
   *
   * Compared to the original class, the following modifications were made:
   * - the major workflow and the convergence check was taken from clust4j
   * - most of the other code was changed and adapted to the MSCD extension of Affinity Propagation
   * - additional features were added: constraints check, handling of all-equal-components
   */

  /**
   * Constructs AffinityPropagationForMSCDImpl
   *
   * @param similarityMatrix NxN similarity matrix for N dataPoints. Cell ij represents the similarity
   *                         between dataPoint i and dataPoint j.
   * @param preferenceDirtySrc The self-similarity of dirty source dataPoints
   * @param preferenceCleanSrc The self-similarity of clean source dataPoints
   * @param damping Damping factor for the message values of ALPHA and RHO, so they can be damped by a
   *                portion of their old value from the last iteration
   * @param apConfig Configuration for the generic AP parameters
   * @param cleanSourceElementIndices Maps each clean source (by its name) to a list of integer IDs of its
   *                                 elements in the similarity-matrix.
   */
  public MscdAffinityPropagationSeq(double[][] similarityMatrix, double preferenceDirtySrc,
    double preferenceCleanSrc, double damping, ApConfig apConfig, Multimap<String,
    Integer> cleanSourceElementIndices) {
    super(similarityMatrix, preferenceDirtySrc, preferenceCleanSrc, damping, apConfig,
      cleanSourceElementIndices);
  }

  /**
   * Constructs AffinityPropagationForMSCDImpl
   *
   * @param similarityMatrix NxN similarity matrix for N dataPoints. Cell ij represents the similarity
   *                         between dataPoint i and dataPoint j.
   * @param preference shared self-similarity for clean and dirty source dataPoints
   */
  public MscdAffinityPropagationSeq(double[][] similarityMatrix, double preference) {
    super(similarityMatrix, preference);
  }

  /**
   * Check whether all similarity values in the {@link #similarityMatrix} are equal.
   *
   * @return true if all equal.
   */
  protected boolean isSimilarityAllEqual() {
    boolean allEqual = true;
    double lastValue = this.similarityMatrix[0][1];
    for (int i = 0; i < this.similarityMatrix.length; i++) {
      for (int j = 0; j < this.similarityMatrix.length; j++) {
        if (this.similarityMatrix[i][j] != 0d) {
          // double comparison with threshold
          if (Math.abs(lastValue - this.similarityMatrix[i][j]) > 0.0001) {
            allEqual = false;
            break;
          }
        }
      }
    }
    return  allEqual;
  }

  /**
   * Execute the clustering for the case, when all similarity values are equal. If the
   * {@link ApConfig#getAllSameSimClusteringThreshold} is reached, put all dirty source dataPoints in one
   * cluster and separate all clean source dataPoints, but create as few clusters as possible. When the
   * threshold is not reached, each dataPoint becomes a singleton.
   */
  private void clusterAllEqualComponent() {
    // algorithm converged immediately due to all elements being equal in input matrix
    this.converged = true;
    this.allEqualComponent = true;
    this.exemplarIndices.clear();

    if (this.similarityMatrix[0][1] < this.apConfig.getAllSameSimClusteringThreshold()) {
      // put each element in its own cluster
      for (int i = 0; i < m; i++) {
        this.labels[i] = i;
        this.exemplarIndices.add(i);
      }
    } else {
      if (this.cleanSourceElementIndices.keySet().size() == 0) {
          /* Else: all are in the same cluster
             take just the first element as exemplar */
        this.exemplarIndices.add(0);
      } else {
          /* MSCD:
           one entity of each clean source can be together with one entity of each other clean source
           - all dirtySource entities are assigned to cluster 0
           - each cleanSource source entity is assigned to a separated cluster 0,1,2,...
           - centroid of a cluster becomes the entity that is first assigned to it */
        this.exemplarIndices.add(0);

        for (String source : this.cleanSourceElementIndices.keySet()) {
          List<Integer> sourceDataPoints = new ArrayList<>(this.cleanSourceElementIndices.get(source));
          Collections.sort(sourceDataPoints);   // list conversion and sorting not necessary.
          // just to get reproducible results.
          int currentClusterId = 0;
          for (int dataPointId : sourceDataPoints) {
            this.labels[dataPointId] = currentClusterId;
            // set centroid if first of cluster
            if (currentClusterId >= this.exemplarIndices.size()) {
              this.exemplarIndices.add(dataPointId);
            }
            currentClusterId++;
          }
        }
      }
    }
  }

  /**
   * Execute the MSCD Affinity Propagation clustering algorithm.
   */
  public void fit() {
    resetResults();

    // check if the dataPoints form an all-equal-component
    if (this.isSimilarityAllEqual()) {
      clusterAllEqualComponent();
      return;
    }

    double[][] simMatrix = new double[m][m];
    // copy similarity matrix, so the noise is not added to the input matrix
    for (int i = 0; i < m; i++) {
      System.arraycopy(this.similarityMatrix[i], 0, simMatrix[i], 0, m);
    }

    // set the dirty source preference
    for (int i = 0; i < m; i++) {
      simMatrix[i][i] = this.preferenceDirtySrc;
    }
    // set clean source preference
    for (String src : this.cleanSourceElementIndices.keySet()) {
      for (int i : this.cleanSourceElementIndices.get(src)) {
        simMatrix[i][i] = this.preferenceCleanSrc;
      }
    }

    // add noise
    final double noiseDecimalShift = NoiseUtils.getNoiseDecimalShift(this.apConfig.getNoiseDecimalPlace());
    if (this.apConfig.getNoiseDecimalPlace() > 0) {
      addNoise(simMatrix, this.random, noiseDecimalShift);
    }

    // initialize the message-matrices, which are not temporary inside of an iteration
    double[][] alpha = new double[m][m];
    double[][] rho = new double[m][m];
    double[][] theta = new double[m][m];

    double[][] e = new double[m][this.apConfig.getConvergenceIter()];
    double[] sumE;

    for (this.iterCount = 0; this.iterCount < this.apConfig.getMaxAdaptionIteration(); this.iterCount++) {
      double[][] eta = new double[m][m];
      // calculate the first piece of AP - compute ETA
      affinityPiece1getEta(alpha, simMatrix, theta, eta);

      theta = new double[m][m];
      // calculate the second piece of AP - compute THETA (containing the main MSCD extension)
      affinityPiece2getTheta(alpha, simMatrix, theta, eta, this.cleanSourceElementIndices);

      // calculate the third piece of AP - compute R and A
      affinityPiece3getA(alpha, simMatrix, theta, eta, rho, this.damping);

      // Set the mask in `e`
      final double[] mask = new double[m];
      int idx = this.iterCount % this.apConfig.getConvergenceIter();
      for (int i = 0; i < m; i++) {
        mask[i] = alpha[i][i] + rho[i][i] > 0.0 ? 1.0 : 0.0;
        e[i][idx] = mask[i];
      }

      // get the number of clusters
      int numClusters = 0;
      for (double d : mask) {
        numClusters += (int) d;
      }

      if (this.iterCount >= this.apConfig.getConvergenceIter()) { // Time to check convergence criteria...
        // calculate the rowSums of e
        sumE = new double[m];
        for (int i = 0; i < m; i++) {
          double rowSum = 0.0;
          for (double d : e[i]) {
            rowSum += d;
          }
          sumE[i] = rowSum;
        }

        // masking
        int maskCt = 0;
        for (int i = 0; i < sumE.length; i++) {
          maskCt += sumE[i] == 0 || sumE[i] == this.apConfig.getConvergenceIter() ? 1 : 0;
        }

        this.converged = maskCt == m;

        if (this.converged || this.iterCount == this.apConfig.getMaxAdaptionIteration()) {
          if (numClusters < 1) {
            this.converged = false;
          }
          break;
        }
      }
    } // end iteration

    double[][] criterionMatrix = MatrixUtils.add(alpha, rho);

    calculateResultLabeling(criterionMatrix, simMatrix);
    calculateOneOfNConstraint(criterionMatrix);
  }

  /**
   * Computes the first portion of the AffinityPropagation iteration sequence. Calculate the message values
   * for the BETA and ETA matrices. Separating this piece from the {@link #fit()} method itself allows
   * easier testing.
   *
   * @param alpha Matrix of the ALPHA messages
   * @param simMatrix Similarity Matrix
   * @param theta Matrix of the THETA messages
   * @param eta Matrix of the ETA messages
   */
  protected static void affinityPiece1getEta(double[][] alpha, double[][] simMatrix, double[][] theta,
    double[][] eta) {
    final int nrRows = simMatrix.length;
    final int nrCols = simMatrix[0].length;

    for (int i = 0; i < nrRows; i++) {
      // Compute row maxes
      double runningMax = Double.NEGATIVE_INFINITY;
      double secondMax  = Double.NEGATIVE_INFINITY;
      int runningMaxIdx = 0;  // Idx of max row element -- start at 0 in case metric produces -Infs

      for (int j = 0; j < nrCols; j++) {    // Create tmp as A + sim_mat
        // StandardAP: Beta = A+S
        // MSCD-AP: Beta = A+S+T
        double betaIJ = alpha[i][j] + simMatrix[i][j] + theta[i][j];

        if (betaIJ > runningMax) {
          secondMax = runningMax;
          runningMax = betaIJ;
          runningMaxIdx = j;
        } else if (betaIJ > secondMax) {
          secondMax = betaIJ;
        }
      }

      for (int j = 0; j < nrCols; j++) {
        eta[i][j] = runningMax * -1;
      }
      eta[i][runningMaxIdx] = secondMax * -1;
    }
  }

  /**
   * Computes the second portion of the AffinityPropagation iteration sequence. Calculate the message
   * values for the GAMMA and THETA matrices.
   *
   * @param alpha Matrix of the ALPHA messages
   * @param sim Similarity Matrix
   * @param theta Matrix of the THETA messages
   * @param eta Matrix of the ETA messages
   * @param cleanSourceElementIndices Maps each clean source (by its name) to a list of integer IDs of its
   *                                 elements in the similarity-matrix.
   */
  protected static void affinityPiece2getTheta(double[][] alpha, double[][] sim, double[][] theta,
    double[][] eta, Multimap<String, Integer> cleanSourceElementIndices) {
    final int nrCols = sim[0].length;

    for (String src : cleanSourceElementIndices.keySet()) {
      Collection<Integer> srcPoints = cleanSourceElementIndices.get(src);

      if (srcPoints.size() > 1) {                         // - there must be at least 2 points to prevent them
                                                         //   from being in the same cluster
        for (int j = 0; j < nrCols; j++) {                     // - for each column j - iterate rows i
          // Compute col maxes
          double runningMax = Double.NEGATIVE_INFINITY;
          double secondMax  = Double.NEGATIVE_INFINITY;
          int runningMaxIdx = 0;

          for (Integer i : srcPoints) {                   // - i are only rows from the current clean source
            // GAMMA = A + S + ETA
            double gammaIJ = alpha[i][j] + sim[i][j] + eta[i][j];

            if (gammaIJ > runningMax) {
              secondMax = runningMax;
              runningMax = gammaIJ;
              runningMaxIdx = i;
            } else if (gammaIJ > secondMax) {
              secondMax = gammaIJ;
            }
          }

          // Theta = -1 * maxValue of all other rows of that column, excluding self
          // Theta = 0 if > 0
          for (Integer i : srcPoints) {
            if (i == runningMaxIdx) {
              theta[i][j] = secondMax * -1;
            } else {
              theta[i][j] = runningMax * -1;
            }
            if (theta[i][j] > 0) {
              theta[i][j] = 0;
            }
          }
        }
      }
    }
  }

  /**
   * Computes the third portion of the AffinityPropagation iteration sequence. Calculate the message values
   * for the RHO and ALPHA matrices.
   *
   * @param alpha Matrix of the ALPHA messages
   * @param sim Similarity Matrix
   * @param theta Matrix of the THETA messages
   * @param eta Matrix of the ETA messages
   * @param rho Matrix of the RHO messages
   * @param damping damping factor, to damp RHO and ALPHA by a portion of their value from the last iteration
   */
  protected static void affinityPiece3getA(double[][] alpha, double[][] sim, double[][] theta,
    double[][] eta, double[][] rho, double damping) {

    final int nrRows = sim.length;
    final int nrCols = sim[0].length;
    final double omd = 1.0 - damping;

    for (int j = 0; j < nrCols; j++) {
      // first iterate over the column to get the colSum for all rows of the column
      double columnSum = 0;

      for (int i = 0; i < nrRows; i++) {
        // R = S + ETA + THETA
        double newValueForR = sim[i][j] + eta[i][j] + theta[i][j];
        rho[i][j] = (rho[i][j] * damping) + (newValueForR * omd);

        if (i != j) {                  // don't use the diagonal value for the columnSums
          if (rho[i][j] > 0) {           // only positive values for the columnSums
            columnSum += rho[i][j];
          }
        }
      }

      // A [diagonal] = sum of positive values of the column of R, excluding diagonalValue
      // A [others] = diagonalValue + ( sum of positive values of the column of R, excluding diagonalValue
      //                                and self)   => values > 0  => 0
      for (int i = 0; i < nrRows; i++) {
        double newValueForA;
        if (i == j) {                  // on the diagonal: take the columnSum
          newValueForA = columnSum;
        } else {                      // rest: remove own value from columnSum and add diagonalValue
          newValueForA = columnSum - Math.max(0, rho[i][j]) + rho[j][j];
          newValueForA = Math.min(0, newValueForA);
        }
        alpha[i][j] = (alpha[i][j] * damping) + (newValueForA * omd);
      }
    }
  }

  /**
   * Add random gaussian noise to the matrix.
   *
   * @param matrix the matrix to be noised
   * @param random random number generator
   * @param noiseDecimalShift decimal shift that is multiplied to a random noise value, to add the main
   *                          part of the noise to a certain position behind the comma
   */
  private void addNoise(double[][] matrix, Random random, double noiseDecimalShift) {
    int nrRows = matrix.length;
    int nrCols = matrix[0].length;

    for (int i = 0; i < nrRows; i++) {
      for (int j = 0; j < nrCols; j++) {
        matrix[i][j] += noiseDecimalShift * random.nextGaussian();
      }
    }
  }
}
