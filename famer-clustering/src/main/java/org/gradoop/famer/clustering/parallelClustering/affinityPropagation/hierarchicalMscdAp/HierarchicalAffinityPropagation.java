/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp;

import org.apache.flink.api.common.functions.FlatJoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DeltaIteration;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.api.java.tuple.Tuple7;
import org.apache.flink.api.java.tuple.Tuple8;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMElement;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.AbstractAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.dataStuctures.HapConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.dataStuctures.HapExemplarAssignmentStrategy;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.AssignDataPointsToExemplars;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.AssignDataPointsToExemplarsHungarian;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.AssignSubClusterIdToExemplars;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.AssignUniqueClusterIdToUnassigned;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.CheckHierarchyDepth;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.CreateOwnClusterForNoExemplarVertices;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.EdgeToSourceIdTargetIdSimValue;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.JoinEdgeTupleWithSourceExemplar;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.JoinEdgeTupleWithSourcePartInfo;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.JoinEdgeTupleWithTargetExemplar;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.JoinEdgeTupleWithTargetPartInfo;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.JoinHapPartitionTupleWithAssignedVertex;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.JoinIterationTupleWithSourcePartInfo;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.JoinLocalExemplarTupleWithEdgeTuples;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.MapSolutionEdgesToExemplarEdges;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.PartitionFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.ReduceGroupByAPFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.ReduceGroupByHierarchicalAPFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions.VertexToVertexClusterTuple;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms.HungarianAlgorithmSeq;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms.MscdAffinityPropagationSeq;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.bool.False;

/**
 * The Gradoop/Flink implementation of the Hierarchical Affinity Propagation Algorithm (HAP) by Liu et al.
 * and its extension, Multi-Source Clean-Dirty HAP (MSCD-HAP). The algorithm uses a divide-and-conquer
 * strategy to create partitions of large connected components. Each partition is clustered by a sequential
 * AP implementation ({@link MscdAffinityPropagationSeq}) inside of a groupReduce function, to find local
 * exemplars. The local exemplars are put together and partitioned again. This gets repeated in a delta
 * iteration, until there is only one partition of a connected component left. Then the results of the
 * sequential AP are global exemplars. The other data points are assigned to them by the highest similarity
 * for dirty source data points or by a sequential implementation of the Kuhn-Munkres algorithm
 * ({@link HungarianAlgorithmSeq}) inside of a groupReduce function.
 *
 * <p>
 * <b>original paper of HAP:</b> LIU, Xiaonan, et al. An improved affinity propagation clustering algorithm
 * for large-scale data sets. In: 2013 Ninth International Conference on Natural Computation (ICNC). IEEE,
 * 2013. S. 894-899.
 */
public class HierarchicalAffinityPropagation extends AbstractAffinityPropagation {
  /* -------------------------------
     Idea of the clustering process:
     -------------------------------
    (0) preprocessing:
        - (mscd) remove edges of two entities from the same clean source
        - get connected components
    (1) Partitioning of the Connected Components
        - create partitions for large connected components
    (2) Clustering unpartitioned points
        - Run sequential (MSCD-) Affinity Propagation for unpartitioned data points
    (3) Iterative hierarchical clustering of partitioned components
        - filter for edges of the same partition
        - check if the maximal hierarchy depth is reached
        - run sequential (MSCD-) Affinity Propagation on the loop data points
        (A) get the global exemplars from highest hierarchy level of their partition..
        - they will go to the solution set
        (B) get the local exemplars from lower hierarchy levels..
        - and prepare them as input for the next iteration
        - partitioning the local exemplars of the same connectedComponent
        - get the edge information for these local exemplars (from inital working set)
    (4) Exemplar assignment preprocessing
        - prepare subCluster Ids for Hungarian assignment
        - join global exemplars on edges
    (5) Exemplar assignment of partitioned components to global exemplars
        - hungarian or highest similarity assignment
    (6) Unite results
        - unite partitioned and unpartitioned data points
        - unite with singletons
     */

  /**
   * Constructor with default parameter settings.
   */
  public HierarchicalAffinityPropagation() {
    super();
    this.apConfig = new HapConfig();
  }

  /**
   * Constructor with default parameter settings and custom random seed for reproducible results.
   *
   * @param randomSeed random seed for reproducible results.
   */
  public HierarchicalAffinityPropagation(long randomSeed) {
    super(randomSeed);
    this.apConfig = new HapConfig();
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public HierarchicalAffinityPropagation(JSONObject jsonConfig) {
    super(jsonConfig);

    JSONObject hapConfigObject;
    try {
      hapConfigObject = jsonConfig.getJSONObject(getConfigName());
    } catch (JSONException ex) {
      throw new UnsupportedOperationException("Clustering: hapConfig could not be found or parsed", ex);
    }

    if (hapConfigObject.has("maxPartitionSize")) {
      try {
        getHapConfig().setMaxPartitionSize(hapConfigObject.getInt("maxPartitionSize"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for maxPartitionSize could not be parsed", e);
      }
    }

    if (hapConfigObject.has("maxHierarchyDepth")) {
      try {
        getHapConfig().setMaxHierarchyDepth(hapConfigObject.getInt("maxHierarchyDepth"));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for maxHierarchyDepth could not be parsed", e);
      }
    }

    if (hapConfigObject.has("hapExemplarAssignmentStrategy")) {
      try {
        getHapConfig().setHapExemplarAssignmentStrategy(HapExemplarAssignmentStrategy.valueOf(
          hapConfigObject.getString("hapExemplarAssignmentStrategy")));
      } catch (JSONException e) {
        throw new UnsupportedOperationException(
          "Clustering: value for hapExemplarAssignmentStrategy could not be parsed", e);
      }
    }
  }

  @Override
  protected String getConfigName() {
    return "hapConfig";
  }

  @Override
  protected ApConfig getNewConfig() {
    return new HapConfig();
  }

  /**
   * Get the HAP clustering configuration.
   *
   * @return HAP clustering configuration.
   */
  public HapConfig getHapConfig() {
    return (HapConfig) this.apConfig;
  }

  /**
   * Execute the HAP clustering algorithm for the passed similarity-graph. Creates a clustered graph with
   * each vertex containing the following properties:
   * <ul>
   *   <li>{@link PropertyNames#CLUSTER_ID}</li>
   *   <li>{@link PropertyNames#IS_CENTER}</li>
   *   <li>{@link PropertyNames#IS_HAP_VERTEX}</li>
   *   <li>{@link PropertyNames#HAP_NOT_ASSIGNABLE}</li>
   *   <li>{@link PropertyNames#HAP_HIERARCHY_DEPTH}</li>
   *   <li>{@link PropertyNames#ORIG_COMPONENT_ID}</li>
   *   <li>{@link PropertyNames#NR_ITERATIONS}</li>
   *   <li>{@link PropertyNames#SUM_ITERATIONS}</li>
   *   <li>{@link PropertyNames#NR_ADAPTIONS}</li>
   * </ul>
   *
   * @param inputGraph Similarity-Graph of the linking step.
   *
   * @return Clustered graph with the vertices containing the clustering information.
   */
  @Override
  public LogicalGraph runClustering(LogicalGraph inputGraph) {
    /* (0) Preprocessing ---------------------------------------------------------------------------------*/
    apConfig.checkConfigCorrectness();
    if (getHapConfig().isCleanSourceExtensionActivated()) {
      // remove edges of two entities from the same clean source
      inputGraph = prepareGraphForMSCD(inputGraph);
    }
    // get connected components
    // - a component id is added to each vertex (CLUSTER_ID = connected component ID)
    // - this can be used to group and partition the data points
    inputGraph = inputGraph.callForGraph(new ConnectedComponents(maxIteration));
    // create initial tuples for edges with ids and sim-value
    DataSet<Tuple3<GradoopId, GradoopId, Double>> sourceIdTargetIdSimValue =
      inputGraph.getEdges().map(new EdgeToSourceIdTargetIdSimValue());
    // create initial tuples for vertices with clusterID and default properties for not being an exemplar
    DataSet<Tuple3<GradoopId, String, EPGMVertex>> vertexIdClusterIdVertex = inputGraph.getVertices()
      .map(new VertexToVertexClusterTuple());

    /* (1) Partitioning of the Connected Components -------------------------------------------------------*/
    PartitionFunction partitionFunction = new PartitionFunction(getHapConfig().getMaxPartitionSize());
    // create partitions for large connected components:
    // - partition = 0 : no partitioning, component is small.
    // - partition > 0 : partitions created
    DataSet<Tuple4<GradoopId, String, EPGMVertex, Integer>> vertexIdClusterIdVertexPartition =
      vertexIdClusterIdVertex.groupBy(1).reduceGroup(partitionFunction);

    // filter partitioned vertices for partition > 0  (for HAP exemplar assignment)
    DataSet<Tuple4<GradoopId, String, EPGMVertex, Integer>> hapVertexPartitionTuples =
      vertexIdClusterIdVertexPartition.filter(tuple -> tuple.f3 > 0);

    // Bring vertex information to the edges tuples, join with source and target vertex information
    // - result is Tuple8(SourceId,TargetId,SimValue,ClusterId,Source,Target,SourcePart,TargetPart)
    DataSet<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>
      edgeTuplesWithPartitionInfo = sourceIdTargetIdSimValue.join(vertexIdClusterIdVertexPartition)
      .where(0).equalTo(0)
      .with(new JoinEdgeTupleWithSourcePartInfo()).join(vertexIdClusterIdVertexPartition)
      .where(1).equalTo(0)
      .with(new JoinEdgeTupleWithTargetPartInfo());

    // filter edgeTuplesWithPartitionInfo for edges of partition > 0 to run (MSCD-) HAP
    DataSet<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>> hapSet =
      edgeTuplesWithPartitionInfo.filter(tuple -> (tuple.f6 > 0) && (tuple.f7 > 0));

    // filter edgeTuplesWithPartitionInfo for edges of partition 0 to run standard (MSCD-) AP
    DataSet<Tuple6<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex>> standardApSet =
      edgeTuplesWithPartitionInfo.filter(tuple -> (tuple.f6 == 0) && (tuple.f7 == 0))
        .project(0, 1, 2, 3, 4, 5);

    /* (2) Clustering of unpartitioned points -------------------------------------------------------------
     * Run sequential (MSCD-) Affinity Propagation for unpartitioned data points, grouped by cluster id,
     * for each connected component in parallel */
    DataSet<EPGMVertex> standardApClusteredVertices =
      standardApSet.groupBy(3).reduceGroup(new ReduceGroupByAPFunction(getHapConfig()));

    /* (3) Iterative hierarchical clustering of partitioned points ----------------------------------------
     * - result is Tuple4<VertexId,OrigClusterId,OrigPartition,Vertex> */
    DataSet<Tuple4<GradoopId, String, Integer, EPGMVertex>> solutionSetForHierarchicalClustering =
      executeIterativeHierarchicalClustering(hapSet, inputGraph.getConfig().getExecutionEnvironment(),
        partitionFunction);

    /* (4) Exemplar assignment preprocessing --------------------------------------------------------------*/
    if (getHapConfig().isCleanSourceExtensionActivated() &&
      (getHapConfig().getHapExemplarAssignmentStrategy() == HapExemplarAssignmentStrategy.HUNGARIAN)) {
      // - assign a subClusterId to each exemplar of each originalCluster
      // - only for hungarian assignment: in stdAssignment own subClusterIds are generated
      solutionSetForHierarchicalClustering = solutionSetForHierarchicalClustering.groupBy(1)
        .reduceGroup(new AssignSubClusterIdToExemplars());
    }
    // The final exemplars with their IS_CENTER=true property must now be joined to the edge tuples again
    // to get the similarities that are necessary for the cluster assignment
    DataSet<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>
      finalSolutionEdgeTuples = hapSet.leftOuterJoin(solutionSetForHierarchicalClustering)
      .where(0).equalTo(0)
      .with(new JoinEdgeTupleWithSourceExemplar()).leftOuterJoin(solutionSetForHierarchicalClustering)
      .where(1).equalTo(0)
      .with(new JoinEdgeTupleWithTargetExemplar());

    /* (5) Exemplar assignment of partitioned components to global exemplars ------------------------------*/
    DataSet<EPGMVertex> hapClusteredVertices;
    if (getHapConfig().isCleanSourceExtensionActivated() &&
      (getHapConfig().getHapExemplarAssignmentStrategy() == HapExemplarAssignmentStrategy.HUNGARIAN)) {
      // Hungarian exemplar assignment
      hapClusteredVertices = assignDataPointsToExemplarsByHungarianMethod(
        finalSolutionEdgeTuples, hapVertexPartitionTuples, solutionSetForHierarchicalClustering);
    } else {
      // Standard exemplar assignment (by highest similarity)
      hapClusteredVertices =
        finalSolutionEdgeTuples.groupBy(3).reduceGroup(new AssignDataPointsToExemplars());
    }

    /* (6) Unite results ----------------------------------------------------------------------------------*/
    // unite stdAP and HAP results
    DataSet<EPGMVertex> allClusteredVertices = hapClusteredVertices.union(standardApClusteredVertices);
    // Get the singleton vertices from the Input Graph
    DataSet<EPGMVertex> clusteredVerticesAndSingletons =
      inputGraph.getVertices().leftOuterJoin(allClusteredVertices)
        .where(EPGMElement::getId).equalTo(EPGMElement::getId)
        .with((FlatJoinFunction<EPGMVertex, EPGMVertex, EPGMVertex>) (inputVertex, clusteredVertex, out) -> {
          if (clusteredVertex == null) {
            // singleton: set isCenter to true for consistency
            inputVertex.setProperty(PropertyNames.IS_CENTER, true);
            inputVertex.setProperty(PropertyNames.IS_HAP_VERTEX, false);
            out.collect(inputVertex);
          } else {
            out.collect(clusteredVertex);
          }
        }).returns(new TypeHint<EPGMVertex>() { });

    LogicalGraph resultGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(), clusteredVerticesAndSingletons, inputGraph.getEdges());

    return resultGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  /**
   * Execute the iterative hierarchical clustering and get the global exemplars.
   *
   * @param hapSet initial workingSet for the HAP delta iteration. Contains all edge tuples of the partitioned
   *               dataPoints.
   * @param env Flink execution environment
   * @param partitionFunction function for the partitioning of the dataPoints of a connected component
   *
   * @return dataSet of global exemplars as
   * {@code Tuple4<VertexId,ConnectedComponentId,OriginalPartition,Vertex>}
   */
  private DataSet<Tuple4<GradoopId, String, Integer, EPGMVertex>> executeIterativeHierarchicalClustering(
    DataSet<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>> hapSet,
    ExecutionEnvironment env, PartitionFunction partitionFunction) {

    // Generate an empty DataSet for the top hierarchy level exemplar results of the loop
    DataSet<Tuple4<GradoopId, String, Integer, EPGMVertex>> initialSolutionSet =
      env.fromElements(Tuple4.of(new GradoopId(), "", 1, new EPGMVertex())).filter(new False<>());

    // ################################ Iteration Block - Start ##########################################
    // Start iteration with first result exemplars
    DeltaIteration<Tuple4<GradoopId, String, Integer, EPGMVertex>,
      Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>> iteration =
      initialSolutionSet.iterateDelta(hapSet, getHapConfig().getMaxHierarchyDepth() + 1, 0);

    // Filter working set for edges of the same partition && check if the maximal hierarchy depth is reached
    // (so we can throw a special HierarchyLevelExceededException and not just simply terminate the iteration)
    DataSet<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>
      hapSetSamePartitionEdges = iteration.getWorkset()
      .map(new CheckHierarchyDepth(getHapConfig().getMaxHierarchyDepth()))
      .filter(edgeTuple -> edgeTuple.f6.equals(edgeTuple.f7));

    // Run sequential (MSCD-) Affinity Propagation on the loop input edges (same partition edges), grouped
    // by ClusterId and SourcePartition, result is Tuple4<VertexId,OrigClusterId,OrigPartition,Vertex>
    DataSet<Tuple4<GradoopId, String, Integer, EPGMVertex>> newExemplarTuples =
      hapSetSamePartitionEdges.groupBy(3, 6)
        .reduceGroup(new ReduceGroupByHierarchicalAPFunction(getHapConfig()));

    // (A) Get global exemplars from highest hierarchy level of their partition, for the solution set
    DataSet<Tuple4<GradoopId, String, Integer, EPGMVertex>> exemplarsFromTopHierarchyLevel =
      newExemplarTuples.filter(exemplarTuple -> exemplarTuple.f2.equals(0));

    // (B) Get local exemplars from lower hierarchy levels, project to Tuple3<VertexId,OrigClusterId,Vertex>
    DataSet<Tuple3<GradoopId, String, EPGMVertex>> localExemplars =
      newExemplarTuples.filter(exemplarTuple -> exemplarTuple.f2 > 0).project(0, 1, 3);

    // .. prepare them as input for next iteration: partitioning of exemplars from the same connectedComponent
    // - result is Tuple4<VertexId,OrigClusterId,Vertex,Partition>
    DataSet<Tuple4<GradoopId, String, EPGMVertex, Integer>> partitionedLocalExemplars =
      localExemplars.groupBy(1).reduceGroup(partitionFunction);

    // Get the edge information for these local exemplars:
    // - join the source and target vertex of the edge tuple
    // - join always with the full edges set from the initial working set, because it contains similarities
    // between now partitioned data points, that are not in the current working set
    DataSet<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>
      nextIterationHapSet = iteration.getInitialWorkset().join(partitionedLocalExemplars)
      .where(0).equalTo(0)
      .with(new JoinIterationTupleWithSourcePartInfo()).join(partitionedLocalExemplars)
      .where(1).equalTo(0)
      .with(new JoinEdgeTupleWithTargetPartInfo());

    // filter for same partition edges
    nextIterationHapSet = nextIterationHapSet.filter(edgeTuple -> edgeTuple.f6.equals(edgeTuple.f7));

    // get the partitioned local exemplars that have no edge present in the working set for the next
    // iteration (same partition edges).
    // Because they can't go to the next iteration, they become global exemplars immediately.
    partitionedLocalExemplars = partitionedLocalExemplars.leftOuterJoin(nextIterationHapSet)
      .where(0).equalTo(0)
      .with(new JoinLocalExemplarTupleWithEdgeTuples()).leftOuterJoin(nextIterationHapSet)
      .where(0).equalTo(1)
      .with(new JoinLocalExemplarTupleWithEdgeTuples());

    // add the local exemplars without an edge in their partition to the solution set of the global exemplars
    exemplarsFromTopHierarchyLevel =
      exemplarsFromTopHierarchyLevel.union(partitionedLocalExemplars.project(0, 1, 3, 2));

    // ################################ Iteration Block - End ##########################################
    // Feed back new exemplars into next iteration. The iteration stops before maxHierarchyDepth or if
    // nextIterationHapSet is empty and returns the final exemplars.
    return iteration.closeWith(exemplarsFromTopHierarchyLevel, nextIterationHapSet);
  }

  /**
   * Execute the hungarian data point assignment of all points from partitioned connected components to the
   * global exemplars, which are the result of the iterative hierarchical clustering procedure. The
   * assignment is done by the help of the Kuhn-Munkres algorithm (hungarian method) by a sequential
   * implementation inside of a group reduce function. Each component and each clean source are processed
   * in parallel.
   * @param finalSolutionEdgeTuples all edges of the partitioned components, including the hierarchical
   *                                clustering output
   * @param hapVertexPartitionTuples all data points of the partitioned components
   * @param solutionSetOfExemplars global exemplars in the result of the iterative hierarchical clustering
   * @return all vertices of the partitioned components with a {@link PropertyNames#CLUSTER_ID} according
   * to the exemplar assignment
   */
  public DataSet<EPGMVertex> assignDataPointsToExemplarsByHungarianMethod(
    DataSet<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>>
      finalSolutionEdgeTuples,
    DataSet<Tuple4<GradoopId, String, EPGMVertex, Integer>> hapVertexPartitionTuples,
    DataSet<Tuple4<GradoopId, String, Integer, EPGMVertex>> solutionSetOfExemplars) {

    // 1. Discard edges that are not connecting an exemplar AND
    // edges that connect a vertex and an exemplar of the same clean source.
    // - result is Tuple7<ExemplarId,DatapointId,SimValue,ClusterId,Exemplar,Datapoint,DatapointGraphLabel>
    DataSet<Tuple7<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, String>>
      finalSolutionExemplarEdges = finalSolutionEdgeTuples.flatMap(new MapSolutionEdgesToExemplarEdges());

    // 2. Get all DataPoints with hungarian assignment to the exemplars, group by ClusterId and DpGraphLabel:
    // DpGraphLabel => Hungarian Assignment (if DpGraphLabel = '' => standard assignment)
    // - result is Tuple4<VertexId,ClusterId,Vertex,AssignedFlag>
    DataSet<Tuple4<GradoopId, String, EPGMVertex, Boolean>> hungarianVerticesAssignmentFlag =
      finalSolutionExemplarEdges.groupBy(3, 6).reduceGroup(new AssignDataPointsToExemplarsHungarian());

    // get the unassigned vertices
    DataSet<Tuple3<GradoopId, String, EPGMVertex>> unassignedVertices =
      hungarianVerticesAssignmentFlag.filter(vertexTuple -> !vertexTuple.f3).project(0, 1, 2);

    // assign unique cluster ids to the unassigned vertices
    unassignedVertices = unassignedVertices.groupBy(1).reduceGroup(new AssignUniqueClusterIdToUnassigned());

    // get the assigned vertices
    DataSet<Tuple3<GradoopId, String, EPGMVertex>> assignedVertices =
      hungarianVerticesAssignmentFlag.filter(vertexTuple -> vertexTuple.f3).project(0, 1, 2);

    // unite assigned vertices and unassigned vertices with new ids
    DataSet<Tuple3<GradoopId, String, EPGMVertex>> hungarianAssignedVertices =
      assignedVertices.union(unassignedVertices);

    // unite the hungarian assignments (dataPoints) with the exemplars
    hungarianAssignedVertices = hungarianAssignedVertices.union(solutionSetOfExemplars.project(0, 1, 3));

    // 3. get the HAP vertices with no edge to any exemplar
    DataSet<Tuple3<GradoopId, String, EPGMVertex>> hapNoExemplarVertices =
      hapVertexPartitionTuples.leftOuterJoin(hungarianAssignedVertices)
        .where(0).equalTo(0)
        // take only vertices that are not in the set of the hungarianAssignedVertices
        .with(new JoinHapPartitionTupleWithAssignedVertex());

    // put each of the NoExemplarVertices into its own cluster (origClusterID-ne-subClusterId)
    hapNoExemplarVertices =
      hapNoExemplarVertices.groupBy(1).reduceGroup(new CreateOwnClusterForNoExemplarVertices());

    // unite hungarianAssignedVertices and noExemplarVertices => map to AssignmentResult
    return hapNoExemplarVertices.union(hungarianAssignedVertices).map(
      (MapFunction<Tuple3<GradoopId, String, EPGMVertex>, EPGMVertex>) vertexTuple -> {
        vertexTuple.f2.setProperty(PropertyNames.IS_HAP_VERTEX, true);
        return vertexTuple.f2;
      }).returns(new TypeHint<EPGMVertex>() { });
  }

  @Override
  public String getName() {
    return HierarchicalAffinityPropagation.class.getName();
  }
}
