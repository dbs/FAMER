/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.serialClusteringOnConnectedComponents;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.EdgeDirection;
import org.apache.flink.graph.Graph;
import org.apache.flink.graph.Vertex;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.clustering.common.functions.FindMaxSimilarityEdges;
import org.gradoop.famer.clustering.common.functions.SetIsSelectedStatusAsProperty;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.FilterEdgesOnSelectedStatus;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.functions.EdgeToEdgeEndIdOtherEndGraphLabel;
import org.gradoop.famer.clustering.parallelClustering.common.functions.EdgeToGellyEdgeCommon;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.serialClusteringOnConnectedComponents.functions.EPGMVertexClusterIdSelector;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.serialClusteringOnConnectedComponents.functions.EdgeSourceClusterIdSelector;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.serialClusteringOnConnectedComponents.functions.GellyGraphToEdgeWithSourceVertex;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.serialClusteringOnConnectedComponents.functions.SerialClusteringOnSubgraph;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.serialClusteringOnConnectedComponents.functions.VertexToGellyVertex;
import org.gradoop.famer.clustering.serialClustering.SerialHierarchicalClustering;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * The Gradoop/Flink implementation of the hierarchical clustering algorithm
 */
public class HierarchicalClustering extends AbstractParallelClustering {

  /**
   * The output type for the clustering result
   */
  private final ClusteringOutputType clusteringOutputType;

  /**
   * Whether edges are bidirectional
   */
  private final boolean isEdgesBiDirected;

  /**
   * Number of maximum iterations used for the scatter gather iteration for connected Components
   */
  private final int maxIteration;

  /**
   * Linkage type
   */
  private final SerialHierarchicalClustering.LinkageType linkageType;

  /**
   * Stopping condition for merging the clusters
   */
  private final double threshold;

  /**
   * Whether weak links will be removed prior to clustering
   */
  private final boolean removeWeakLinks;

  /**
   * @param clusteringOutputType the output type of the clustering result
   * @param isEdgesBiDirected Whether edges are bidirectional
   * @param maxIteration the number of maximum iterations used for the scatter gather iteration for
   *                     connected Components
   * @param linkageType the linkage type used in the algorithm
   * @param threshold the stopping condition for merging clusters
   * @param removeWeakLinks if weak links will be removed prior to clustering
   */
  public HierarchicalClustering(ClusteringOutputType clusteringOutputType, boolean isEdgesBiDirected,
    int maxIteration, SerialHierarchicalClustering.LinkageType linkageType, double threshold,
    boolean removeWeakLinks) {
    this.clusteringOutputType = clusteringOutputType;
    this.isEdgesBiDirected = isEdgesBiDirected;
    this.maxIteration = maxIteration;
    this.linkageType = linkageType;
    this.threshold = threshold;
    this.removeWeakLinks = removeWeakLinks;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public HierarchicalClustering(JSONObject jsonConfig) {
    this.clusteringOutputType = getClusteringOutputTypeFromJsonConfig(jsonConfig);
    this.maxIteration = getMaxIterationFromJsonConfig(jsonConfig);
    this.isEdgesBiDirected = getIsEdgesBiDirectedFromJsonConfig(jsonConfig);
    try {
      this.linkageType =
        SerialHierarchicalClustering.LinkageType.valueOf(jsonConfig.getString("linkageType"));
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Clustering: value for linkage type could not be found or parsed", ex);
    }
    try {
      this.threshold = jsonConfig.getDouble("threshold");
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Clustering: value for threshold could not be found or parsed", ex);
    }
    try {
      this.removeWeakLinks = jsonConfig.getBoolean("removeWeakLinks");
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Clustering: value for removeWeakLinks could not be found or parsed", ex);
    }
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph inputGraph) {

    DataSet<EPGMEdge> edges = inputGraph.getEdges();

    if (removeWeakLinks) {
      // remove all weak links for improved load balancing
      DataSet<EPGMEdge> allEdgesWithSelectedStatus =
        new EdgeToEdgeSourceVertexTargetVertex(inputGraph).execute()
          .flatMap(new EdgeToEdgeEndIdOtherEndGraphLabel())
          .groupBy(1, 2)
          .reduceGroup(new FindMaxSimilarityEdges(0.0))
          .groupBy(1)
          .reduceGroup(new SetIsSelectedStatusAsProperty());

      // keep all edges with selection status > 0 (0 = weak)
      edges = allEdgesWithSelectedStatus.filter(new FilterEdgesOnSelectedStatus(0));
    }

    // run connected components
    inputGraph = inputGraph.getConfig().getLogicalGraphFactory()
      .fromDataSets(inputGraph.getGraphHead(), inputGraph.getVertices(), edges)
      .callForGraph(new ConnectedComponents(maxIteration));

    // create a gelly graph from the result graph
    Graph<GradoopId, EPGMVertex, Double> gellyGraph = Graph.fromDataSet(
        inputGraph.getVertices().map(new VertexToGellyVertex()),
        edges.flatMap(new EdgeToGellyEdgeCommon(true)), // dont create reverse edges
        inputGraph.getConfig().getExecutionEnvironment());

    /* create a data set that contains each edge from the gelly graph and its source vertex,
       this way an edge can be associated with the cluster id of the connected component it is part of */
    DataSet<Tuple2<Edge<GradoopId, Double>, Vertex<GradoopId, EPGMVertex>>> edgesWithSourceVertex =
      gellyGraph.groupReduceOnNeighbors(new GellyGraphToEdgeWithSourceVertex(), EdgeDirection.OUT);

    // run the serial hierarchical clustering algorithm on each connected component
    DataSet<EPGMVertex> resultVertices = inputGraph.getVertices().coGroup(edgesWithSourceVertex)
      .where(new EPGMVertexClusterIdSelector())
      .equalTo(new EdgeSourceClusterIdSelector())
      .with(new SerialClusteringOnSubgraph(linkageType, threshold, isEdgesBiDirected));

    LogicalGraph resultGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(), resultVertices, inputGraph.getEdges());

    return resultGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  @Override
  public String getName() {
    return HierarchicalClustering.class.getName();
  }
}
