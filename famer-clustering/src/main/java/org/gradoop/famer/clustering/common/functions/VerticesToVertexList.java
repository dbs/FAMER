/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import java.util.ArrayList;
import java.util.List;

/**
 * Reduces a group of {@code Tuple2<Vertex, ClusterId>}, grouped on the ClusterId to one
 * {@code Tuple2<List<Vertex>,ClusterId>} with all vertices in the group put to the list.
 */
public class VerticesToVertexList implements
  GroupReduceFunction<Tuple2<EPGMVertex, String>, Tuple2<List<EPGMVertex>, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple2<List<EPGMVertex>, String> reuseTuple = new Tuple2<>();

  @Override
  public void reduce(Iterable<Tuple2<EPGMVertex, String>> group,
    Collector<Tuple2<List<EPGMVertex>, String>> out) throws Exception {
    List<EPGMVertex> vertices = new ArrayList<>();
    List<String> gradoopIds = new ArrayList<>();
    String clusterId = "";

    for (Tuple2<EPGMVertex, String> groupItem : group) {
      clusterId = groupItem.f1;
      if (!gradoopIds.contains(groupItem.f0.getId().toString())) {
        gradoopIds.add(groupItem.f0.getId().toString());
        vertices.add(groupItem.f0);
      }
    }
    reuseTuple.f0 = vertices;
    reuseTuple.f1 = clusterId;
    out.collect(reuseTuple);
  }
}
