/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.common.model.impl.id.GradoopId;

/**
 * Joins vertex tuples with tuples of all-equal-components. Enriches the vertex tuples with the simValue
 * and a boolean flag to determine membership in an all-equal-component.
 */
@FunctionAnnotation.ForwardedFieldsFirst("f0;f1->f2;f2->f3")
public class JoinVerticesForAllEqualComp implements JoinFunction<Tuple3<GradoopId, String, String>,
  Tuple3<GradoopId, Double, String>, Tuple5<GradoopId, Double, String, String, Boolean>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple5<GradoopId, Double, String, String, Boolean> reuseTuple = new Tuple5<>();

  @Override
  public Tuple5<GradoopId, Double, String, String, Boolean> join(
    Tuple3<GradoopId, String, String> vertexTuple,
    Tuple3<GradoopId, Double, String> allEqualCompVertexTuple) throws Exception {
    reuseTuple.f0 = vertexTuple.f0;
    reuseTuple.f2 = vertexTuple.f1;
    reuseTuple.f3 = vertexTuple.f2;
    if (allEqualCompVertexTuple == null) {
      // vertex is not in allEqualComponents
      reuseTuple.f1 = 0d;
      reuseTuple.f4 = false;
    } else {
      // vertex is in allEqualComponents
      reuseTuple.f1 = allEqualCompVertexTuple.f1;
      reuseTuple.f4 = true;
    }
    return reuseTuple;
  }
}
