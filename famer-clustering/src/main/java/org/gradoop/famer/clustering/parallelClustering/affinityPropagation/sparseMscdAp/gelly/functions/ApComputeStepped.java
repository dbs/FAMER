/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions;

import org.apache.flink.api.common.aggregators.LongSumAggregator;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.Vertex;
import org.apache.flink.graph.pregel.ComputeFunction;
import org.apache.flink.graph.pregel.MessageIterator;
import org.apache.flink.graph.pregel.VertexCentricConfiguration;
import org.apache.flink.types.LongValue;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.ParameterAdaptionUtils;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.ApMessageTuple;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.ApVertexValueTuple;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.ConvergenceInfoPropagationStatus;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.GammaMaxTuple;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;
import java.util.ArrayList;
import java.util.List;

/**
 * Executes the iterative message passing in the sparse (MSCD-) Affinity Propagation clustering algorithm.
 * This class represents the computation of each vertex at each superStep of the vertex centric iteration.
 * The action of each vertex varies between different superSteps. Each vertex stores a
 * {@link ApVertexValueTuple#getStep()} variable in its vertex value that represents the current step.
 * Depending on this step, different calculations are executed on the received messages. One iteration of
 * the AP message passing is done in 8 superSteps. When {@link ApConfig#getConvergenceIter()} is reached,
 * a variable number of additional superSteps is executed to check, whether a connected component has
 * converged and the message passing procedure can be terminated.
 */
public class ApComputeStepped extends
  ComputeFunction<GradoopId, ApVertexValueTuple, Double, ApMessageTuple> {

  /**
   * Name of the {@link #convergenceTokenChangesAggregator}
   */
  public static final String CONVERGENCE_AGGREGATOR_NAME = "sumConvergenceTokenChanges";

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  private final ApConfig apConfig;

  /**
   * The aggregator, which is used to check whether information about convergence and constraints
   * satisfaction was propagated through all vertices of all connected components. It counts the number of
   * vertices, whose information changed by propagated information from neighbouring vertices. When the
   * count is zero, all vertices share the same information.
   */
  private LongSumAggregator convergenceTokenChangesAggregator;

  /**
   * Constructs ApComputeStepped
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  public ApComputeStepped(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  /**
   * Get the parameters that can be used for the construction of the vertex centric iteration.
   *
   * @return parameters of the vertex centric iteration.
   */
  public static VertexCentricConfiguration getParameters() {
    VertexCentricConfiguration parameters = new VertexCentricConfiguration();
    parameters.setName("Gelly MSCD Affinity Propagation");
    parameters.registerAggregator(CONVERGENCE_AGGREGATOR_NAME, new LongSumAggregator());
    return parameters;
  }

  /**
   * Before each superStep, retrieve the {@link #convergenceTokenChangesAggregator}
   */
  @Override
  public void preSuperstep() {
    convergenceTokenChangesAggregator = getIterationAggregator(CONVERGENCE_AGGREGATOR_NAME);
  }

  /**
   * Execute the vertex centric iteration for the (MSCD-) AP message passing.
   *
   * @param vertex a gelly vertex on whose perspective the vertex centric iteration runs. It exchanges
   *               messages with its neighbours to find a clustering solution.
   * @param messages all messages the vertex has received from it's neighbours in this super step.
   */
  @Override
  public void compute(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages) throws ConvergenceException {
    /*
    STEPS EXECUTION: once(0), loop(1,2,3,4,5,6,7,8, loop[21,22])

    ########################################### MSCD-AP ###########################################
    A message tuple for MSCD-AP contains the following information:
    MSG: sender, sim, alpha, theta, rho, gamma, eta, sender_clean_src

    The values of alpha, theta, rho, ... represent cells of the sparse MSCD-AP matrices. A message of step
    0 from node i to node j represents the matrix-cell ij. In the following steps, the messages are
    forwarded. So in step 1, a vertex receives all cells of row i. In step 2, a vertex receives all cells
    of column j. In the following steps, the row-column-direction switches alternating, so that in
    consecutive superSteps column and row operations can be calculated. Intermediate results are stored in
    the vertex values.

    e.g. vertex 2 is sender, to targets 0,1,2,3,4
    x_i: = row messages - e.g.: x_20,x_21,x_22,x_23,x_24
    x_:j = column messages - e.g.:  x_02,x_12,x_22,x_32,x_42

    0. in: null
       msg-init: all values = 0
       msg-set: sim_i:           (edge value)
       msg-set: beta_i:          (from sim_i:)
       v-store: max beta
       out: msg_:i
       => step 2

    1. in: msg_i:
       msg-set: beta_i:                               (from alpha_i: , theta_i:, sim_i:)
       v-store: beta_row_max
       out: msg_i:

    2. (just switch directions)
       in: msg_:j
       out: msg_:j

    3. in: msg_i:
       msg-set: eta_i:          (from v-store beta_row_max)
       msg-set: gamma_i:        (from eta_i:, alpha_i:, sim_i:)
       msg-set: sender_clean_src_i:
       out: msg_i:

    4. in: msg_:j
       v-store: gamma_source_column_max     (from gamma_:j, sender_clean_src)
       out: msg_:j

    5. (just switch directions)
       in: msg_i:
       out: msg_i:

    6. in: msg_:j
       msg-set: theta_:j          (from v-store gamma_source_column_max, sender_clean_src)
       msg-set: rho_:j            (from theta_:j,  eta_:j, sim_i:,  old_rho_:j)
       v-store: rho_col_sums
       out: msg_:j

    7. (just switch directions)
       in: msg_i:
       out: msg_i:

    8. in: msg_:j
       msg-set: alpha_:j  (from v-store rho_col_sums, old_alpha_:j)
       out: msg_:j

    ########################################### constraints check ###########################################
    Check, whether the 1-of-N-Constraint, the Exemplar-Consistency-Constraint and the
    Clean-Source-Constraint are satisfied for each vertex. The information is propagated through all
    vertices of the connected component together with the convergence information (see next topic).

    8. in: msg_:j
       msg-set: sender_clustering_name_:j

    1. in msg_i:
       calc: criterion_row_max  (from alpha_i:, rho_i:)
       => g-constraint satisfied automatically
       v-store: is_exemplar, cluster_id  (from criterion_row_max, sender_clustering_name_i:)

    2. in: msg_:j
       just switch directions

    3. in msg_i:
       msg-set: sender_chosen_exemplar_i:

    4. in: msg_:j
       check: h-constraint => if sender_chosen_exemplar == vertexClusteringName, vertex must be exemplar
       check: t-constraint => Map<cs_name, count(sender_chosen_exemplar == vertexClusteringName)> lower 2
       v-store: componentConstraintsSatisfied

    ########################################### convergence ###########################################
    A connected component converged if all vertices didn't change their isExemplar-Decision for the last
    {@link #convergenceIter} iterations. Propagate the information of convergence and constraint-satisfaction
    through the component. Each vertex sends its convergence and constraints info to its neighbours. In the
    next step, each receiver AND-combines the received boolean infos and stores the accumulated info in its
    vertex-value. Then each vertex sends the accumulated info to its neighbours, which again combine the
    info. When the combined info did not change for each vertex, then the convergence info was propagated
    through all connected components. All vertices with a cumulated convergence value of true are converged
    and stop message passing (or adapt the parameter values if the constraints are not satisfied).
    A component with convergence=true and constraintsSatisfied=true propagates the info immediately to all
    vertices. In worst case, all vertices have all values set to true, but one has a value set to false.
    Then this information must pass each edge on the shortest path to each other vertex of the component.

    If the longest shortest path in between two vertices of the graph contains only two edges, in the worst
    case we need 10 steps per iteration (including 2 steps to see if something changed). For longer MAX
    (shortestPath), the steps 21 and 22 loop until the info is propagated to all vertices. So each additional
    edge on the MAX(shortestPath) takes two additional steps. Then we go inside of step 21 to step 1.

    1. v-store: exemplarDecisionSum, is_exemplar, cluster_id  (see constraints check)

    ...

    4. v-store: vertexConstraintsSatisfied (see constraints check)
       v-store: cumulatedConstraintsInfo  <-- vertexConstraintsSatisfied
       v-store: cumulatedConvergenceInfo  <-- f(exemplarDecisionSum, convergenceIter)

    5. msg-set: cumulatedConstraintsInfo  <-- v-store componentConstraintsSatisfied
       msg-set: cumulatedConvergenceInfo  <-- v-store cumulatedConvergenceInfo

    6. v-store: cumulatedConstraintsInfo  <-- AND combine all received values
       v-store: cumulatedConvergenceInfo  <-- AND combine all received values
       if changed cumulatedConstraintsInfo OR cumulatedConvergenceInfo
          agg-add: sumConvergenceTokenChanges <-- 1

    7. if agg-get: sumConvergenceTokenChanges > 0:
          msg-set: cumulatedConstraintsInfo  <-- v-store
          msg-set: cumulatedConvergenceInfo  <-- v-store
       else
          if cumulatedConvergenceInfo == true:
              if constraintsSatisfied
                  vertex stop message passing
              else
                  vertex adapt parameters
                  => step 0
          else
              continue AP: just switch directions
              => step 8

    8. like 6.

    21. like 7, except for:
              continue AP: execute step 1

    22. if cumulatedConvergenceToken == -1
          => go to 1
        else
          v-store: cumulatedConstraintsInfo  <-- AND combine all received values
          v-store: cumulatedConvergenceInfo  <-- AND combine all received values
          if changed cumulatedConstraintsInfo OR cumulatedConvergenceInfo
              agg-add: sumConvergenceTokenChanges <-- 1
          => go to 21
     */
    ApVertexValueTuple vertexValue = vertex.getValue();

    if (vertexValue.isComponentConverged() && (vertexValue.getNumberOfIterations() > 0)) {
      throw new IllegalStateException(
        String.format("The vertex %s of the connected component %s is still sending messages, " +
        "although the component is converged and it should have stopped message passing.",
        vertex.getId().toString(), vertexValue.getComponentId()));
    }

    switch (vertexValue.getStep()) {
    case 0:
      step0initBetaMax(vertexValue);
      break;
    case 1:
      step1setBetaMax(vertex, messages, vertexValue);
      break;
    case 2:
      step2SwitchDirections(vertex, messages);
      break;
    case 3:
      step3calcEtaAndGamma(vertex, messages, vertexValue);
      break;
    case 4:
      step4calcSrcGammaColumnMax(vertex, messages, vertexValue);
      break;
    case 5:
      step5initConvergenceMessages(vertex, messages, vertexValue);
      break;
    case 6:
      step6calcThetaAndRho(vertex, messages, vertexValue);
      break;
    case 7:
      step7sendCumulatedConvergenceMessages(vertex, messages, vertexValue);
      break;
    case 8:
      step8calcAlpha(vertex, messages, vertexValue);
      break;
    case 21:
      step21stopIfConverged(vertex, messages, vertexValue);
      break;
    case 22:
      step22accumulateConvergenceInfo(vertex, messages, vertexValue);
      break;
    default:
      throw new IllegalStateException("The vertex is in the undefined step " + vertexValue.getStep());
    }

    vertexValue.setStep(getNextStep(vertexValue.getStep()));
    setNewVertexValue(vertexValue);
  }

  /**
   * Get the next step of a vertex in the message passing procedure, by the following step-order:
   * <ul>
   *   <li>0 --> 2</li>
   *   <li>loop (1 --> 2 --> 3 --> ... --> 8 --> loop [21 --> 22 --> 21] --> 1)</li>
   * </ul>
   *
   * @param step current step
   * @return next step
   */
  private int getNextStep(int step) {
    int nextStep;
    if (step == 0) {
      nextStep = 2;
    } else if (step == 8) {
      nextStep = 21;
    } else if (step == 22) {
      // loop between 21 and 22 to propagate convergence info
      nextStep = 21;
    } else {
      nextStep = step + 1;
    }
    return nextStep;
  }

  /**
   * Let the vertex reply to a message by sending it back to its sender
   *
   * @param vertex sender of the reply message
   * @param message the message to be replied to
   */
  private void sendReplyMessage(Vertex<GradoopId, ApVertexValueTuple> vertex, ApMessageTuple message) {
    GradoopId target = message.getSender();
    message.setSender(vertex.getId());
    sendMessageTo(target, message);
  }

  /**
   * Initial step of the (MSCD-) AP message passing. For each outgoing edge, initialize a message tuple for
   * the sparse AP matrix cells and send it to the neighbouring node. Calculate the BETA max and secondMax
   * value out of the edge similarities and store it to the vertex value. This is done on behalf of step 1,
   * which uses additional message information for that which is not present now. So step 1 is jumped over
   * in the first iteration and the next step is step 2.
   *
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  private void step0initBetaMax(ApVertexValueTuple vertexValue) {
    /*
       0. in: null
       msg-init: all values = 0
       msg-set: sim_i:           (edge value)
       msg-set: beta_i:          (from sim_i:)
       v-store: max beta
       out: msg_:i
       => step 2
     */
    if (!vertexValue.isComponentConverged()) {
      // all-equal-components and singletons are computed in preprocessing - take them out of the game
      double maxBeta = Double.NEGATIVE_INFINITY;
      double secondMaxBeta = Double.NEGATIVE_INFINITY;
      GradoopId maxBetaId = GradoopId.NULL_VALUE;
      double initialPreference = 0d;

      for (Edge<GradoopId, Double> edge : getEdges()) {
        if (edge.getSource().equals(edge.getTarget())) {
          initialPreference = edge.getValue();
        }

        if (edge.getValue() > maxBeta) {
          secondMaxBeta = maxBeta;
          maxBeta = edge.getValue();
          maxBetaId = edge.getTarget();
        } else if (edge.getValue() > secondMaxBeta) {
          secondMaxBeta = edge.getValue();
        }

        /* In Step 0 the first messages are generated. The message object instances can be reused in all
        later steps. This reduces object instantiation and garbage collection.  */
        ApMessageTuple message =
          new ApMessageTuple(edge.getSource(), edge.getValue(), vertexValue.getVertexId());

        sendMessageTo(edge.getTarget(), message);
      }

      // get the noise of the initial preference (to be later reused for parameter adaption)
      if (vertexValue.getCleanSource().equals("")) {
        vertexValue.setPreferenceNoise(initialPreference - vertexValue.getInitialPreferenceDirtySource());
      } else {
        vertexValue.setPreferenceNoise(initialPreference - vertexValue.getInitalPreferenceCleanSource());
      }

      vertexValue.setBetaMax(maxBeta);
      vertexValue.setBetaSecondMax(secondMaxBeta);
      vertexValue.setBetaMaxId(maxBetaId);
    }
  }

  /**
   * First step of a single AP iteration. Calculate the BETA max and secondMax value and store it to the
   * vertex value. Calculate the criterion value (exemplar decision) of the last AP iteration and set the
   * vertex's clusterId regarding to that. Store whether the vertex is an exemplar or not. Update the
   * exemplar decision sum which is used to check if a component is converged.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  private void step1setBetaMax(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) {
    /*
    1. in: msg_i:
       msg-set: beta_i:                               (from alpha_i: , theta_i:, sim_i:)
       v-store: beta_row_max
       out: msg_i:
       ------------------------ constraints check ------------------------
       calc: criterion_row_max  (from alpha_i:, rho_i:)
       => g-constraint satisfied automatically
       v-store: is_exemplar, cluster_id  (from criterion_row_max, sender_clustering_name_i:)
       --------------------------- convergence ---------------------------
       v-store: exemplar_decision_sum
     */
    double maxBeta = Double.NEGATIVE_INFINITY;
    double secondMaxBeta = Double.NEGATIVE_INFINITY;
    GradoopId maxBetaId = GradoopId.NULL_VALUE;
    double maxCriterion = Double.NEGATIVE_INFINITY;
    Long maxCriterionClusteringName = vertexValue.getVertexId();
    double beta;
    double criterion;

    for (ApMessageTuple message: messages) {
      // (1) calculate max BETA
      beta = message.getAlpha() + message.getSim() + message.getTheta();
      if (beta > maxBeta) {
        secondMaxBeta = maxBeta;
        maxBeta = beta;
        maxBetaId = message.getSender();
      } else if (beta > secondMaxBeta) {
        secondMaxBeta = beta;
      }
      // (2) calculate max CRITERION
      criterion = message.getAlpha() + message.getRho();
      if (criterion > maxCriterion) {
        maxCriterion = criterion;
        maxCriterionClusteringName = message.getSenderClusteringName();
      }

      sendReplyMessage(vertex, message);
    }
    vertexValue.setBetaMax(maxBeta);
    vertexValue.setBetaSecondMax(secondMaxBeta);
    vertexValue.setBetaMaxId(maxBetaId);

    // (3) set the exemplar decision and cluster assignment
    if (maxCriterionClusteringName.equals(vertexValue.getVertexId())) {
      vertexValue.setExemplar(true);
      vertexValue.setClusterId(vertexValue.getVertexId());
    } else {
      vertexValue.setExemplar(false);
      vertexValue.setClusterId(maxCriterionClusteringName);
    }
    vertexValue.updateExemplarDecisionSum();
  }

  /**
   * Second Step of a single AP iteration. No calculations can be done in this step on the received column
   * matrix values. Just forward the messages.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   */
  private void step2SwitchDirections(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages) {
    /*
    2. switch directions
       in: msg_:j
       out: msg_:j
     */
    for (ApMessageTuple message: messages) {
      sendReplyMessage(vertex, message);
    }
  }

  /**
   * Third iteration of a single AP iteration. All received messages represent a row of the AP matrices.
   * Calculate ETA out of the stored BetaMax values and calculate GAMMA from the received messages.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  private void step3calcEtaAndGamma(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) {
    /*
    3. in: msg_i:
       msg-set: eta_i:          (from v-store beta_row_max)
       msg-set: gamma_i:        (from eta_i:, alpha_i:, sim_i:)
       msg-set: sender_clean_src_i:
       out: msg_i
       ------------------------ constraints check ------------------------
       msg-set: sender_chosen_exemplar_i:
     */
    for (ApMessageTuple message: messages) {
      // (1) calculate ETA
      if (message.getSender().equals(vertexValue.getBetaMaxId())) {
        message.setEta(-1 * vertexValue.getBetaSecondMax());
      } else {
        message.setEta(-1 * vertexValue.getBetaMax());
      }
      // (2) calculate GAMMA
      if (vertexValue.getCleanSource().equals("")) {
        message.setGamma(0d);
      } else {
        // dirty src vertices don't need to send gamma (complete row empty)
        message.setGamma(message.getEta() + message.getAlpha() + message.getSim());
      }
      message.setSenderCleanSrc(vertexValue.getCleanSource());
      // (3) set CONSTRAINTS CHECK message
      message.setSenderChosenExemplar(vertexValue.getClusterId());
//      message.setSenderExemplarDecisionSum(vertexValue.getExemplarDecisionSum());

      sendReplyMessage(vertex, message);
    }
  }

  /**
   * Fourth iteration of a single AP iteration. All received messages represent a column of the AP matrices.
   * Calculate the column max values of the received messages for GAMMA, if the sender is from a clean
   * source. Check if the exemplar-consistency-constraint and the clean-source-constraint are satisfied.
   * Also check whether the exemplarDecisionSum did not change for the last
   * {@link ApConfig#getConvergenceIter()} iterations to see if the component may is converged. Store
   * constraint satisfaction and convergence info to the vertex value.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  private void step4calcSrcGammaColumnMax(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) {
    /*
    4. in: msg_:j
       v-store: gamma_source_column_max     (from gamma_:j, sender_clean_src)
       out: msg_:j
       ------------------------ constraints check ------------------------
       check: h-constraint => if sender_chosen_exemplar == vertexClusteringName, vertex must be exemplar
       check: t-constraint => Map<cs_name, count(sender_chosen_exemplar == vertexClusteringName)> lower 2
       v-store: vertexConstraintsSatisfied
       --------------------------- convergence ---------------------------
       v-store: cumulatedConstraintsInfo  <-- vertexConstraintsSatisfied
       v-store: cumulatedConvergenceInfo  <-- f(exemplarDecisionSum, convergenceIter)
     */
    vertexValue.resetSrcGMaxValues();
//    vertexValue.resetSrcCountVerticesInCluster();
//    Map<String, Long> srcVertexCountInCluster = new HashMap<>();
    List<String> vertexCleanSourcesInCluster = new ArrayList<>();
    boolean hConstraintSatisfied = true;
    boolean tConstraintSatisfied = true;

    for (ApMessageTuple message: messages) {
      if (!message.getSenderCleanSrc().equals("")) {
        // (1) calculate GAMMA MAX
        // get gamma column max values of each source
        GammaMaxTuple srcMaxVal = vertexValue.getSrcGMaxTuple(message.getSenderCleanSrc());
        if (srcMaxVal == null) {
          srcMaxVal = new GammaMaxTuple();
          srcMaxVal.initialize();
        }
        if (message.getGamma() > srcMaxVal.getGammaMax()) {
          srcMaxVal.setGammaSecondMax(srcMaxVal.getGammaMax());
          srcMaxVal.setGammaMax(message.getGamma());
          srcMaxVal.setGammaMaxId(message.getSender());
        } else if (message.getGamma() > srcMaxVal.getGammaSecondMax()) {
          srcMaxVal.setGammaSecondMax(message.getGamma());
        }
        vertexValue.putSrcGMaxTuple(message.getSenderCleanSrc(), srcMaxVal);
        // (2) check T-CONSTRAINT
        if (tConstraintSatisfied) {
          if (message.getSenderChosenExemplar().equals(vertexValue.getVertexId())) {
            if (vertexCleanSourcesInCluster.contains(message.getSenderCleanSrc())) {
              tConstraintSatisfied = false;
            } else {
              vertexCleanSourcesInCluster.add(message.getSenderCleanSrc());
            }
          }
        }
      }
      // (3) check H-CONSTRAINT
      if (!vertexValue.isExemplar()) {
        if (message.getSenderChosenExemplar().equals(vertexValue.getVertexId())) {
          hConstraintSatisfied = false;
        }
      }
      sendReplyMessage(vertex, message);
    }
    vertexValue.setCumulatedConstraintsInfo(hConstraintSatisfied && tConstraintSatisfied);
    vertexValue.setCumulatedConvergenceInfo(
      (vertexValue.getExemplarDecisionSum() < (-1 * apConfig.getConvergenceIter())) ||
        (vertexValue.getExemplarDecisionSum() > apConfig.getConvergenceIter()));

  }

  /**
   * Fifth iteration of a single AP iteration. All received messages represent a row of the AP matrices.
   * No operation for the AP calculation can be done on the rows now. Put the information about convergence
   * and constraints satisfaction into the message.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  private void step5initConvergenceMessages(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) {
    /*
    5. switch directions
       in: msg_i:
       out: msg_i:
       --------------------------- convergence ---------------------------
       msg-set: cumulatedConstraintsInfo  <-- v-store componentConstraintsSatisfied
       msg-set: cumulatedConvergenceInfo  <-- v-store cumulatedConvergenceInfo
     */
    for (ApMessageTuple message: messages) {
      message.setCumulatedConstraintsInfo(vertexValue.isCumulatedConstraintsInfo());
      message.setCumulatedConvergenceInfo(vertexValue.isCumulatedConvergenceInfo());

      sendReplyMessage(vertex, message);
    }
  }

  /**
   * Sixth iteration of a single AP iteration. All received messages represent a column of the AP matrices.
   * Calculate THETA out of the stored GammaMax values. Calculate RHO from the messages and store the
   * RhoSums of the column into the vertex value. Accumulate the vertex's information about constraints
   * satisfaction and convergence with the info which the neighbouring nodes sent. Propagate the cumulated
   * info to the neighbours. If this vertex's info changed by the info of the neighbours, count it using
   * the {@link #convergenceTokenChangesAggregator} to globally inform that the info propagation is not
   * finished and all vertices of a component are not on the same information.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  private void step6calcThetaAndRho(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) {
    /*
    6. in: msg_:j
       msg-set: theta_:j          (from v-store gamma_source_column_max, sender_clean_src)
       msg-set: rho_:j            (from theta_:j,  eta_:j, sim_i:,  old_rho_:j)
       v-store: rho_col_sums
       out: msg_:j
       --------------------------- convergence ---------------------------
       v-store: cumulatedConstraintsInfo  <-- AND combine all received values
       v-store: cumulatedConvergenceInfo  <-- AND combine all received values
       if changed cumulatedConstraintsInfo OR cumulatedConvergenceInfo
          agg-add: sumConvergenceTokenChanges <-- 1
     */
    GammaMaxTuple gammaMaxTuple;
    double rhoSumNonDiag = 0.0;
    double rhoDiag = 0.0;

    boolean cumulatedConstraintsInfo = true;
    boolean cumulatedConvergenceInfo = true;

    for (ApMessageTuple message: messages) {
      // (1) calculate THETA
      if (message.getSenderCleanSrc().equals("")) {
        message.setTheta(0d);
      } else {
        gammaMaxTuple = vertexValue.getSrcGMaxTuple(message.getSenderCleanSrc());
        if (message.getSender().equals(gammaMaxTuple.getGammaMaxId())) {
          message.setTheta(Math.min(0, -1 * gammaMaxTuple.getGammaSecondMax()));
        } else {
          message.setTheta(Math.min(0, -1 * gammaMaxTuple.getGammaMax()));
        }
      }
      // (2) calculate RHO
      message.setRho(dampValue(message.getRho(),
        message.getEta() + message.getTheta() + message.getSim()));
      // (3) Sum RHO
      if (message.getSender().equals(vertex.getId())) {
        rhoDiag = message.getRho();
      } else {
        rhoSumNonDiag += Math.max(0, message.getRho());
      }
      // (4) accumulate CONVERGENCE info
      cumulatedConstraintsInfo = cumulatedConstraintsInfo && message.isCumulatedConstraintsInfo();
      cumulatedConvergenceInfo = cumulatedConvergenceInfo && message.isCumulatedConvergenceInfo();

      sendReplyMessage(vertex, message);
    }

    vertexValue.resetSrcGMaxValues();
    vertexValue.setRhoDiag(rhoDiag);
    vertexValue.setRhoSumNonDiag(rhoSumNonDiag);

    if (vertexValue.getNumberOfIterations() >= apConfig.getConvergenceIter()) {
      // only do the convergence info propagation after convergence iter steps
      // => makes sure we need only 8 message passing steps at the beginning
      if (!vertexValue.isCumulatedConstraintsInfo().equals(cumulatedConstraintsInfo) ||
        !vertexValue.isCumulatedConvergenceInfo().equals(cumulatedConvergenceInfo)) {
        convergenceTokenChangesAggregator.aggregate(1);
      }
    }

    vertexValue.setCumulatedConstraintsInfo(cumulatedConstraintsInfo);
    vertexValue.setCumulatedConvergenceInfo(cumulatedConvergenceInfo);
  }

  /**
   * Seventh iteration of a single AP iteration. All received messages represent a row of the AP matrices.
   * No AP calculation can be done on rows now. When the info propagation through all vertices is finished,
   * check whether the component is converged and the constraints are satisfied. If so, stop message
   * passing. All vertices of the component are out of the game now. When the constraints are hurt but the
   * component is converged, adapt the parameters and restart AP. Otherwise continue AP and the info
   * propagation.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  public void step7sendCumulatedConvergenceMessages(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) throws ConvergenceException {
    /*
    7. switch directions
       in: msg_i:
       out: msg_i:
       --------------------------- convergence ---------------------------
       if agg-get: sumConvergenceTokenChanges > 0:
          msg-set: cumulatedConstraintsInfo  <-- v-store
          msg-set: cumulatedConvergenceInfo  <-- v-store
       else
          if cumulatedConvergenceInfo == true:
              if constraintsSatisfied
                  vertex stop message passing
              else
                  vertex adapt parameters
                  => step 0
          else
              just switch directions
              => step 8
     */
    switch (getInfoPropagationStatus(vertexValue)) {
    case CONTINUE_INFO_PROPAGATION:
    case CONTINUE_AP:
      sendCumulatedVertexValueMessages(vertex, messages, vertexValue);
      break;
    case STOP_MESSAGE_PASSING:
      setComponentConverged(vertexValue);
      break;
    case ADAPT_PARAMETERS_AND_RESTART:
      try {
        restartApForComponentWithAdaptedParameters(vertex, messages, vertexValue);
      } catch (ConvergenceException e) {
        if (apConfig.isSingletonsForUnconvergedComponents()) {
          vertexValue.setClusterId(vertexValue.getVertexId());
          vertexValue.setExemplar(true);
          setComponentConverged(vertexValue);
        } else {
          throw e;
        }
      }
      break;
    default:
      throw new IllegalStateException("The InfoPropagationStatus of the vertex is in an unknown state.");
    }
  }

  /**
   * Eighth iteration of a single AP iteration. All received messages represent a column of the AP matrices.
   * Calculate ALPHA out of the stored RhoSums. The AP calculation of this iteration is finished by this
   * step. When {@link ApConfig#getConvergenceIter()} is not reached, continue with step 1 afterwards.
   * Otherwise continue the info propagation. Accumulated the received convergence and constraints info,
   * tell the {#convergenceTokenChangesAggregator} if new information was received and propagate the info
   * to the neighbours.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  public void step8calcAlpha(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) {
    /*
    8. in: msg_:j
       msg-set: alpha_:j  (from v-store rho_col_sums, old_alpha_:j)
       out: msg_:j
       ------------------------ constraints check ------------------------
       msg-set: sender_clustering_name_:j
       --------------------------- convergence ---------------------------
       if changed cumulatedConstraintsInfo OR cumulatedConvergenceInfo
          agg-add: sumConvergenceTokenChanges <-- 1
       v-store: cumulatedConstraintsInfo  <-- AND combine all received values
       v-store: cumulatedConvergenceInfo  <-- AND combine all received values
     */
    boolean cumulatedConstraintsInfo = true;
    boolean cumulatedConvergenceInfo = true;

    for (ApMessageTuple message: messages) {
      // (1) calculate ALPHA
      if (message.getSender().equals(vertex.getId())) {
        message.setAlpha(dampValue(message.getAlpha(), vertexValue.getRhoSumNonDiag()));
      } else {
        message.setAlpha(dampValue(message.getAlpha(),
          Math.min(0,
            vertexValue.getRhoSumNonDiag() - Math.max(0, message.getRho()) + vertexValue.getRhoDiag())));
      }
      // (2) sender CLUSTERING-NAME
      message.setSenderClusteringName(vertexValue.getVertexId());
      // (3) accumulate CONVERGENCE info
      cumulatedConstraintsInfo = cumulatedConstraintsInfo && message.isCumulatedConstraintsInfo();
      cumulatedConvergenceInfo = cumulatedConvergenceInfo && message.isCumulatedConvergenceInfo();

      sendReplyMessage(vertex, message);
    }

    if (vertexValue.getNumberOfIterations() >= apConfig.getConvergenceIter()) {
      // only do the convergence info propagation after convergence iter steps
      // => makes sure we need only 8 message passing steps at the beginning
      if (!vertexValue.isCumulatedConstraintsInfo().equals(cumulatedConstraintsInfo) ||
        !vertexValue.isCumulatedConvergenceInfo().equals(cumulatedConvergenceInfo)) {
        convergenceTokenChangesAggregator.aggregate(1);
      }
    }

    vertexValue.setNumberOfIterations(vertexValue.getNumberOfIterations() + 1);
    vertexValue.setSumOfOVerallIterations(vertexValue.getSumOfOverallIterations() + 1);

    if (vertexValue.getNumberOfIterations() > apConfig.getMaxAdaptionIteration()) {
      throw new IllegalStateException("The number of iterations of a vertex is larger then the configured " +
        "number of adaption iterations. Parameter adaption was not executed correctly.");
    }

    vertexValue.setCumulatedConstraintsInfo(cumulatedConstraintsInfo);
    vertexValue.setCumulatedConvergenceInfo(cumulatedConvergenceInfo);
  }

  /**
   * Step one of the loopy info propagation through all vertices of a connected component. Decide by the
   * info propagation status whether to continue the info propagation, stop the message passing or adapt
   * the parameters and restart AP.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  public void step21stopIfConverged(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) throws ConvergenceException {
    /*
    21. if agg-get: sumConvergenceTokenChanges > 0:
          msg-set: cumulatedConstraintsInfo  <-- v-store
          msg-set: cumulatedConvergenceInfo  <-- v-store
       else
          if cumulatedConvergenceInfo == true:
              if constraintsSatisfied
                  vertex stop message passing
              else
                  vertex adapt parameters
                  => step 0
          else
              execute step 1
     */
    if (vertexValue.getSumOfOverallIterations() <= apConfig.getMaxApIteration()) {
      // if maxApIteration is exceeded, stop the message passing without setting the component being converged

      switch (getInfoPropagationStatus(vertexValue)) {
      case CONTINUE_INFO_PROPAGATION:
        sendCumulatedVertexValueMessages(vertex, messages, vertexValue);
        break;
      case STOP_MESSAGE_PASSING:
        setComponentConverged(vertexValue);
        break;
      case ADAPT_PARAMETERS_AND_RESTART:
        try {
          restartApForComponentWithAdaptedParameters(vertex, messages, vertexValue);
        } catch (ConvergenceException e) {
          if (apConfig.isSingletonsForUnconvergedComponents()) {
            vertexValue.setClusterId(vertexValue.getVertexId());
            vertexValue.setExemplar(true);
            setComponentConverged(vertexValue);
          } else {
            throw e;
          }
        }
        break;
      case CONTINUE_AP:
        vertexValue.setStep(1);
        step1setBetaMax(vertex, messages, vertexValue);
        break;
      default:
        throw new IllegalStateException("The InfoPropagationStatus of the vertex is in an unknown state.");
      }
    }
  }

  /**
   * Step two of the loopy info propagation through all vertices of a connected component. Accumulate the
   * info messages and tell the {@link #convergenceTokenChangesAggregator} if new information was received.
   * Continue the information propagation through the component.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  public void step22accumulateConvergenceInfo(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) {
    /*
    22. v-store: cumulatedConstraintsInfo  <-- AND combine all received values
        v-store: cumulatedConvergenceInfo  <-- AND combine all received values
        if changed cumulatedConstraintsInfo OR cumulatedConvergenceInfo
              agg-add: sumConvergenceTokenChanges <-- 1
     */
    boolean cumulatedConstraintsInfo = true;
    boolean cumulatedConvergenceInfo = true;

    for (ApMessageTuple message: messages) {
      cumulatedConstraintsInfo = cumulatedConstraintsInfo && message.isCumulatedConstraintsInfo();
      cumulatedConvergenceInfo = cumulatedConvergenceInfo && message.isCumulatedConvergenceInfo();

      sendReplyMessage(vertex, message);
    }

    if (!vertexValue.isCumulatedConstraintsInfo().equals(cumulatedConstraintsInfo) ||
      !vertexValue.isCumulatedConvergenceInfo().equals(cumulatedConvergenceInfo)) {
      convergenceTokenChangesAggregator.aggregate(1);
    }

    vertexValue.setCumulatedConstraintsInfo(cumulatedConstraintsInfo);
    vertexValue.setCumulatedConvergenceInfo(cumulatedConvergenceInfo);
  }

  /**
   * Forward the received messages and tell the neighbours about the cumulated convergence and constraints
   * information of this vertex.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   */
  private void sendCumulatedVertexValueMessages(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) {
    /*
       switch directions
       --------------------------- convergence ---------------------------
       msg-set: cumulatedConstraintsInfo  <-- v-store
       msg-set: cumulatedConvergenceInfo  <-- v-store
     */
    for (ApMessageTuple message: messages) {
      message.setCumulatedConvergenceInfo(vertexValue.isCumulatedConvergenceInfo());
      message.setCumulatedConstraintsInfo(vertexValue.isCumulatedConstraintsInfo());

      sendReplyMessage(vertex, message);
    }
  }

  /**
   * Check whether the convergence and constraints information was propagated through all vertices of all
   * connected components. By the propagation status make a decision, which action the vertex needs to
   * execute in the current superStep.
   *
   * @param vertexValue value of the vertex used to store the cumulated convergence and constraints info
   * @return the {@link ConvergenceInfoPropagationStatus} which tells the vertex what to do in the current
   * superStep.
   */
  public ConvergenceInfoPropagationStatus getInfoPropagationStatus(ApVertexValueTuple vertexValue) {

    if (vertexValue.getNumberOfIterations() < apConfig.getConvergenceIter()) {
      // only do the convergence info propagation after convergence iter steps
      // => makes sure we need only 8 message passing steps at the beginning
      return ConvergenceInfoPropagationStatus.CONTINUE_AP;
    } else {
      LongValue prevAgg = getPreviousIterationAggregate(CONVERGENCE_AGGREGATOR_NAME);

      if (prevAgg.getValue() > 0) {
        // go on propagating convergence info
        return ConvergenceInfoPropagationStatus.CONTINUE_INFO_PROPAGATION;
      } else {
        // convergence info is propagated to all vertices
        if (vertexValue.isCumulatedConvergenceInfo()) {
          if (vertexValue.isCumulatedConstraintsInfo()) {
            // A) STOP MESSAGE PASSING - converged and constraints satisfied
            return ConvergenceInfoPropagationStatus.STOP_MESSAGE_PASSING;
          } else {
            // B.1) ADAPT PARAMETERS (cause: converged but constraints not satisfied)
            return ConvergenceInfoPropagationStatus.ADAPT_PARAMETERS_AND_RESTART;
          }
        } else {
          // Not converged jet.
          if (vertexValue.getNumberOfIterations() >= apConfig.getMaxAdaptionIteration()) {
            // B.2) ADAPT PARAMETERS (cause: not converged, but maxAdaptionIteration passed)
            return ConvergenceInfoPropagationStatus.ADAPT_PARAMETERS_AND_RESTART;
          } else {
            // C) CONTINUE AP MESSAGE PASSING normally step 1
            // (no additional direction switch is needed, because 21 comes directly after 8 or always two
            // steps later, so the row-column order is kept)
            return ConvergenceInfoPropagationStatus.CONTINUE_AP;
          }
        }
      }
    }
  }

  /**
   * Set the vertex value to tell the vertex that its connected component successfully converged and found
   * a clustering solution.
   *
   * @param vertexValue value of the vertex used to store the final clustering information
   */
  private void setComponentConverged(ApVertexValueTuple vertexValue) {
    vertexValue.setComponentConverged(true);
  }

  /**
   * Adapt the parameters for preference and damping, reset all calculated values and restart the (MSCD-)
   * AP message passing procedure. Do the calculation of {@link #step0initBetaMax}. Reset the received
   * messages and forward them to the neighbours.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   * @throws ConvergenceException when all possible parameter configurations for damping and preference
   * were tried for the connected component
   */
  private void restartApForComponentWithAdaptedParameters(Vertex<GradoopId, ApVertexValueTuple> vertex,
    MessageIterator<ApMessageTuple> messages, ApVertexValueTuple vertexValue) throws ConvergenceException {
    /*
       called by step21 (odd step, so it can directly pretend to be step 0 without an additional direction
       switch)
       in: msg_i:
       out: msg_i:
     */

    adaptParameters(vertexValue);
    vertexValue.resetVertexValue();

    double maxBeta = Double.NEGATIVE_INFINITY;
    double secondMaxBeta = Double.NEGATIVE_INFINITY;
    GradoopId maxBetaId = GradoopId.NULL_VALUE;

    for (Edge<GradoopId, Double> edge : getEdges()) {
      double edgeValue = edge.getValue();
      // use the new adapted preference values for self-edges
      if (edge.getSource().equals(edge.getTarget())) {
        if (vertexValue.getCleanSource().equals("")) {
          edgeValue = vertexValue.getCurrentPreferenceDirtySrc() + vertexValue.getPreferenceNoise();
        } else {
          edgeValue = vertexValue.getCurrentPreferenceCleanSrc() + vertexValue.getPreferenceNoise();
        }
      }

      if (edgeValue > maxBeta) {
        secondMaxBeta = maxBeta;
        maxBeta = edgeValue;
        maxBetaId = edge.getTarget();
      } else if (edgeValue > secondMaxBeta) {
        secondMaxBeta = edgeValue;
      }
    }

    for (ApMessageTuple message : messages) {
      /* In Step 0 the first messages are generated. The message object instances can be reused in all
      later steps. This reduces object instantiation and garbage collection.  */

      message.resetMessage();  // reset all calculated message values (keeping similarity and sender)

      if (message.getSender().equals(vertex.getId())) {
        if (vertexValue.getCleanSource().equals("")) {
          message.setSim(vertexValue.getCurrentPreferenceDirtySrc() + vertexValue.getPreferenceNoise());
        } else {
          message.setSim(vertexValue.getCurrentPreferenceCleanSrc() + vertexValue.getPreferenceNoise());
        }
      }

      message.setSenderClusteringName(vertexValue.getVertexId());
      sendReplyMessage(vertex, message);
    }

    vertexValue.setBetaMax(maxBeta);
    vertexValue.setBetaSecondMax(secondMaxBeta);
    vertexValue.setBetaMaxId(maxBetaId);

    vertexValue.setNumberOfIterations(0);
    vertexValue.setStep(0);   // pretend to be step 0
  }

  /**
   * Adapt the parameters for preference and damping.
   *
   * @param vertexValue value of the vertex used to store the step, intermediate results and the final
   *                    clustering information
   * @throws ConvergenceException when all possible parameter configurations for damping and preference
   * were tried for the connected component
   */
  private void adaptParameters(ApVertexValueTuple vertexValue) throws ConvergenceException {
    ParameterAdaptionUtils newParams = ParameterAdaptionUtils.getNewParameterValues(
      vertexValue.getInitialPreferenceDirtySource(),
      vertexValue.getInitalPreferenceCleanSource(),
      vertexValue.getCurrentPreferenceDirtySrc(),
      vertexValue.getCurrentPreferenceCleanSrc(),
      vertexValue.getCurrentDamping(),
      vertexValue.getNumberOfAdaptions(),
      apConfig);

    vertexValue.setNumberOfAdaptions(vertexValue.getNumberOfAdaptions() + 1);

    vertexValue.setCurrentPreferenceDirtySrc(newParams.getNewPreferenceDirtySrc());
    vertexValue.setCurrentPreferenceCleanSrc(newParams.getNewPreferenceCleanSrc());
    vertexValue.setCurrentDamping(newParams.getNewDampingFactor());
  }

  /**
   * Damp a value by a configurable percentage of its old value. The damping factor is configured in
   * {@link ApConfig#getDampingFactor()}.
   *
   * @param oldValue dampingFactor times part of the result
   * @param newValue 1 - dampingFactor times part of the result
   * @return new value damped by the old value
   */
  private Double dampValue(Double oldValue, Double newValue) {
    return apConfig.getDampingFactor() * oldValue + (1 - apConfig.getDampingFactor()) * newValue;
  }
}
