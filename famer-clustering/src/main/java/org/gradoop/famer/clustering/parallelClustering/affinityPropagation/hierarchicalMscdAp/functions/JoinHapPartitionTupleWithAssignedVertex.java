/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.FlatJoinFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Joins an original vertex-partition tuple with an hungarian assigned vertex tuple if the vertex id
 * matches. If there is no matching hungarian assigned tuple, the original vertex-partition tuples is
 * returned without its partition value -> return yet unassigned vertices.
 */
public class JoinHapPartitionTupleWithAssignedVertex implements
  FlatJoinFunction<Tuple4<GradoopId, String, EPGMVertex, Integer>, Tuple3<GradoopId, String, EPGMVertex>,
    Tuple3<GradoopId, String, EPGMVertex>> {

  @Override
  public void join(Tuple4<GradoopId, String, EPGMVertex, Integer> vertexPartitionTuple,
    Tuple3<GradoopId, String, EPGMVertex> assignedVertex,
    Collector<Tuple3<GradoopId, String, EPGMVertex>> out) throws Exception {
    // only take vertices that are not in the set of assigned vertices
    if (assignedVertex == null) {
      out.collect(Tuple3.of(vertexPartitionTuple.f0, vertexPartitionTuple.f1, vertexPartitionTuple.f2));
    }
  }
}
