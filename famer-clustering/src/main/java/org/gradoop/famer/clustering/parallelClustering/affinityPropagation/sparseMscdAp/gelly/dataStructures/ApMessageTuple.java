/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures;

import org.apache.flink.api.java.tuple.Tuple13;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.ApComputeStepped;

/**
 * Message value for the vertex centric iteration of sparse MSCD-AP in {@link ApComputeStepped}. It
 * contains all necessary matrix cell values ij of the sparse matrices in MSCD-AP, which are exchanged
 * between vertex i and j. Extends Flinks tuple class to achieve better performance than using a POJO.
 */
public class ApMessageTuple extends Tuple13<GradoopId, Double, Double, Double, Double, Double, Double,
  String, Long, Long, Long, Boolean, Boolean> {

  /**
   * Parameterless constructor for serialization
   */
  public ApMessageTuple() {
    super();
  }

  /**
   * Constructs ApMessageTuple
   *
   * @param sender unique GradoopId of the messages sender
   * @param similarity similarity value between sender and receiver
   * @param senderClusteringName unique long vertexId of the sender
   */
  public ApMessageTuple(GradoopId sender, Double similarity, Long senderClusteringName) {
    super(sender, similarity,
      0d, // eta
      0d, // theta
      0d, // alpha
      0d, // gamma
      0d, // rho
      "", // senderCleanSource
      senderClusteringName,
      -1L,    // senderChosenExemplar
      -1L,    // cumulatedConvergenceToken
      false,  // cumulatedConvergenceInfo
      false   // cumulatedConstraintsInfo
    );
  }

  /**
   * Reset all calculated from the AP iterations message values.
   * This method is used, when AP's parameters are adapted and AP message-passing starts new with new
   * parameter values.
   * Keep similarity and sender, so the message can be recycled to avoid object instantiation.
   */
  public void resetMessage() {
    // keep sender
    // keep similarity
    setEta(0d);
    setTheta(0d);
    setAlpha(0d);
    setGamma(0d);
    setRho(0d);
    setSenderCleanSrc("");
    setSenderClusteringName(-1L);
    setSenderChosenExemplar(-1L);
    setCumulatedConvergenceInfo(false);
    setCumulatedConstraintsInfo(false);
  }

  /**
   * Transforms the message to a string for debugging purposes.
   *
   * @param target GradoopId of the messages target
   * @param step current step of {@link ApComputeStepped}
   * @return String representation of the message
   */
  public String toString(GradoopId target, Integer step) {
    return String.format("%s - %s (step %d): S=%+.5f \tE=%+.5f \tG=%+.5f \tT=%+.5f \tR=%+.5f \tA=%+.5f",
      target.toString(), getSender().toString(), step, getSim(), getEta(), getGamma(), getTheta(), getRho(),
      getAlpha());
  }

  /**
   * Set the sender of the message.
   *
   * @param sender ID of the sending vertex.
   */
  public void setSender(GradoopId sender) {
    this.f0 = sender;
  }

  /**
   * Get the sender of the message.
   *
   * @return ID of the sending vertex.
   */
  public GradoopId getSender() {
    return this.f0;
  }

  /**
   * Set the noised similarity value between sender and receiver.
   *
   * @param similarity noised similarity value between sender and receiver.
   */
  public void setSim(Double similarity) {
    this.f1 = similarity;
  }

  /**
   * Get the noised similarity value between sender and receiver.
   *
   * @return noised similarity value between sender and receiver.
   */
  public Double getSim() {
    return this.f1;
  }

  /**
   * Set ETA_ij for sender i and target j.
   *
   * @param eta ETA_ij for sender i and target j.
   */
  public void setEta(Double eta) {
    this.f2 = eta;
  }

  /**
   * Get ETA_ij for sender i and target j.
   *
   * @return ETA_ij for sender i and target j.
   */
  public Double getEta() {
    return this.f2;
  }

  /**
   * Set THETA_ij for sender i and target j.
   *
   * @param theta THETA_ij for sender i and target j.
   */
  public void setTheta(Double theta) {
    this.f3 = theta;
  }

  /**
   * Get THETA_ij for sender i and target j.
   *
   * @return THETA_ij for sender i and target j.
   */
  public Double getTheta() {
    return this.f3;
  }

  /**
   * Set ALPHA_ij for sender i and target j.
   *
   * @param alpha ALPHA_ij for sender i and target j.
   */
  public void setAlpha(Double alpha) {
    this.f4 = alpha;
  }

  /**
   * Get ALPHA_ij for sender i and target j.
   *
   * @return ALPHA_ij for sender i and target j.
   */
  public Double getAlpha() {
    return this.f4;
  }

  /**
   * Set GAMMA_ij for sender i and target j.
   *
   * @param gamma GAMMA_ij for sender i and target j.
   */
  public void setGamma(Double gamma) {
    this.f5 = gamma;
  }

  /**
   * Get GAMMA_ij for sender i and target j.
   *
   * @return GAMMA_ij for sender i and target j.
   */
  public Double getGamma() {
    return this.f5;
  }

  /**
   * Set RHO_ij for sender i and target j.
   *
   * @param rho RHO_ij for sender i and target j.
   */
  public void setRho(Double rho) {
    this.f6 = rho;
  }

  /**
   * Get RHO_ij for sender i and target j.
   *
   * @return RHO_ij for sender i and target j.
   */
  public Double getRho() {
    return this.f6;
  }

  /**
   * Set the clean sources name of the sender.
   *
   * @param senderCleanSrc the clean sources name of the sender.
   */
  public void setSenderCleanSrc(String senderCleanSrc) {
    this.f7 = senderCleanSrc;
  }

  /**
   * Get the clean sources name of the sender.
   *
   * @return the clean sources name of the sender or an empty string, if the sender is from a dirty source.
   */
  public String getSenderCleanSrc() {
    return this.f7;
  }

  /**
   * Set the senders unique long ID, other vertices use to select the sender as their exemplar.
   *
   * @param senderClusteringName the senders unique long ID, other vertices use to select the sender as
   *                             their exemplar.
   */
  public void setSenderClusteringName(Long senderClusteringName) {
    this.f8 = senderClusteringName;
  }

  /**
   * Get the senders unique long ID, other vertices use to select the sender as their exemplar.
   *
   * @return the senders unique long ID, other vertices use to select the sender as their exemplar.
   */
  public Long getSenderClusteringName() {
    return this.f8;
  }

  /**
   * Set the unique long ID of the vertex, which the sender chose as its exemplar.
   *
   * @param senderChosenExemplar the unique long ID of the vertex, which the sender chose as its exemplar.
   */
  public void setSenderChosenExemplar(Long senderChosenExemplar) {
    this.f9 = senderChosenExemplar;
  }

  /**
   * Get the unique long ID of the vertex, which the sender chose as its exemplar.
   *
   * @return the unique long ID of the vertex, which the sender chose as its exemplar.
   */
  public Long getSenderChosenExemplar() {
    return this.f9;
  }

  /**
   * Set the cumulated information of the sender and its neighbours about the convergence status of the
   * connected component
   *
   * @param cumulatedConvergenceInfo true, if all cumulated infos by now are true. So all vertices whose
   *                                 information are part of this accumulation are successfully converged.
   */
  public void setCumulatedConvergenceInfo(Boolean cumulatedConvergenceInfo) {
    this.f11 = cumulatedConvergenceInfo;
  }

  /**
   * Get the cumulated information of the sender and its neighbours about the convergence status of the
   * connected component
   *
   * @return true, if all cumulated infos by now are true. So all vertices whose information are part of
   * this accumulation are successfully converged.
   */
  public Boolean isCumulatedConvergenceInfo() {
    return this.f11;
  }

  /**
   * Set the cumulated information of the sender and its neighbours about the constraints satisfaction
   * status of the connected component
   *
   * @param cumulatedConstraintsInfo true, if all cumulated infos by now are true. So all vertices whose
   *                                 information are part of this accumulation are satisfying the all
   *                                 constraints.
   */
  public void setCumulatedConstraintsInfo(Boolean cumulatedConstraintsInfo) {
    this.f12 = cumulatedConstraintsInfo;
  }

  /**
   * Get the cumulated information of the sender and its neighbours about the constraints satisfaction
   * status of the connected component
   *
   * @return true, if all cumulated infos by now are true. So all vertices whose information are part of
   * this accumulation are satisfying the all constraints.
   */
  public Boolean isCumulatedConstraintsInfo() {
    return this.f12;
  }
}
