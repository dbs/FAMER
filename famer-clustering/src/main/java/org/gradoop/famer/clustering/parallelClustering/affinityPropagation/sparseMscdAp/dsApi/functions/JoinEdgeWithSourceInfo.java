/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Builds edge tuples of {@code Tuple5<SourceId(RowId),TargetId(ColumnId),SimValue,ConnCompId,CsInfo>} by
 * joining an edge with its source vertex tuple.
 */
@FunctionAnnotation.ForwardedFieldsFirst("sourceId->f0;targetId->f1")
@FunctionAnnotation.ForwardedFieldsSecond("f1->f3;f2->f4")
public class JoinEdgeWithSourceInfo implements JoinFunction<EPGMEdge, Tuple3<GradoopId, String, String>,
  Tuple5<GradoopId, GradoopId, Double, String, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple5<GradoopId, GradoopId, Double, String, String> reuseTuple = new Tuple5<>();

  @Override
  public Tuple5<GradoopId, GradoopId, Double, String, String> join(EPGMEdge edge,
    Tuple3<GradoopId, String, String> vertexTuple) throws Exception {
    reuseTuple.f0 = edge.getSourceId();
    reuseTuple.f1 = edge.getTargetId();
    reuseTuple.f2 = edge.getPropertyValue(PropertyNames.SIM_VALUE).getDouble();
    reuseTuple.f3 = vertexTuple.f1;
    reuseTuple.f4 = vertexTuple.f2;
    return reuseTuple;
  }
}
