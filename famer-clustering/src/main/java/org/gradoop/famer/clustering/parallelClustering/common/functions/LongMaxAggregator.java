/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.functions;

import org.apache.flink.api.common.aggregators.Aggregator;
import org.apache.flink.types.LongValue;

/**
 * An {@link Aggregator} that calculates a long maximum.
 */
public class LongMaxAggregator implements Aggregator<LongValue> {

  /**
   * The aggregated maximum value
   */
  private long max;

  @Override
  public LongValue getAggregate() {
    return new LongValue(max);
  }

  @Override
  public void aggregate(LongValue element) {
    max = Math.max(element.getValue(), max);
  }

  /**
   * Compares the given long value to the current aggregate and keeps the maximum
   *
   * @param value The value to compare with the aggregate.
   */
  public void aggregate(long value) {
    max = Math.max(value, max);
  }

  /**
   * Compares the given int value to the current aggregate and keeps the maximum
   *
   * @param value The value to compare with the aggregate.
   */
  public void aggregate(int value) {
    max = Math.max(value, max);
  }

  @Override
  public void reset() {
    max = Long.MIN_VALUE;
  }
}
