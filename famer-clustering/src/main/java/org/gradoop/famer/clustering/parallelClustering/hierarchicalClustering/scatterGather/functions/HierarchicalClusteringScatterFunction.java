/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.functions;

import org.apache.flink.graph.Vertex;
import org.apache.flink.graph.spargel.ScatterFunction;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.dataStructures.HierarchicalClusteringMessage;

import java.util.Map;

/**
 * Scatter function for the scatter gather implementation of Hierarchical Clustering
 */
public class HierarchicalClusteringScatterFunction extends
  ScatterFunction<GradoopId, EPGMVertex, HierarchicalClusteringMessage, Double> {

  /**
   * Creates a new instance of HierarchicalClusteringScatterFunction
   */
  public HierarchicalClusteringScatterFunction() {
  }

  @Override
  public void sendMessages(Vertex<GradoopId, EPGMVertex> vertex) throws Exception {
    long vertexPriority = vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
    PropertyValue edgeMap = vertex.f1.getPropertyValue(PropertyNames.EDGES);
    PropertyValue ownCluster = vertex.f1.getPropertyValue(PropertyNames.CLUSTER_MEMBERS);
    GradoopId centerId = vertex.f1.getPropertyValue(PropertyNames.CENTER_ID).getGradoopId();
    PropertyValue nn = vertex.f1.getPropertyValue(PropertyNames.NEAREST_NEIGHBOR);
    boolean isCenter = vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean();

    switch (getSuperstepNumber() % 4) {
    case 1:
      // only send messages if vertex is center and still has nn with sim > threshold
      if (nn != null) {
        HierarchicalClusteringMessage msg = new HierarchicalClusteringMessage(vertex.getId(), vertexPriority);
        sendMessageTo(nn.getGradoopId(), msg);
        //fake message to keep vertex active, in case it is not the nn of any other vertex (gets no other msg)
        sendMessageTo(vertex.getId(), msg);
      }
      break;
    case 2:
      // if center was changed in previous superstep, inform all vertices from own cluster about new clusterId
      if (vertex.f1.getPropertyValue(PropertyNames.CENTER_CHANGED).getBoolean()) {
        HierarchicalClusteringMessage msg = new HierarchicalClusteringMessage(vertex.getId(), vertexPriority);
        msg.setNewCenter(centerId);
        for (PropertyValue vertexInCluster : ownCluster.getSet()) {
          // send message to cluster members containing the new centerID
          sendMessageTo(vertexInCluster.getGradoopId(), msg);
        }
        msg.setNewClusterMembers(ownCluster.getSet());
        sendMessageTo(centerId, msg);
        // inform all neighbors about new clusterId
        for (Map.Entry<PropertyValue, PropertyValue> edge : edgeMap.getMap().entrySet()) {
          // message to neighbor including new center id
          msg.setMessageType(1);
          sendMessageTo(edge.getKey().getGradoopId(), msg);
        }
      }
      // message to itself to keep vertex active in case no rnn was detected
      sendMessageTo(vertex.getId(), new HierarchicalClusteringMessage(vertex.getId(), vertexPriority));
      break;
    case 3:
      // now that all edges values were adjusted, send them to the new center
      if (vertex.f1.getPropertyValue(PropertyNames.CENTER_CHANGED).getBoolean()) {
        HierarchicalClusteringMessage msg = new HierarchicalClusteringMessage(vertex.getId(), vertexPriority);
        for (Map.Entry<PropertyValue, PropertyValue> edge : edgeMap.getMap().entrySet()) {
          msg.setNewCenter(edge.getKey().getGradoopId());
          msg.setSimValues(edge.getValue().getString());
          sendMessageTo(centerId, msg);
        }
        // fake msg to remove center changed status
        sendMessageTo(vertex.getId(), new HierarchicalClusteringMessage(vertex.getId(), vertexPriority));
      }
      // msg to keep centers active
      if (isCenter) {
        sendMessageTo(vertex.getId(), new HierarchicalClusteringMessage(vertex.getId(), vertexPriority));
      }
      break;
    case 0:
      // send cluster size to connected clusters
      if (isCenter) {
        HierarchicalClusteringMessage msg = new HierarchicalClusteringMessage(vertex.getId(), vertexPriority);
        for (Map.Entry<PropertyValue, PropertyValue> edge : edgeMap.getMap().entrySet()) {
          GradoopId neighbor = edge.getKey().getGradoopId();
          msg.setClusterSize(ownCluster.getSet().size() + 1);
          sendMessageTo(neighbor, msg);
        }
      }
      break;
    default:
      break;
    }
  }
}
