/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.HierarchicalClusteringScatterGather;

/**
 * All common property names used for clustering and cluster postprocessing
 */
public class PropertyNames {

  /**
   * Property key to access the graphLabel property
   */
  public static final String GRAPH_LABEL = "graphLabel";

  /**
   * Property key to access the cluster id property
   */
  public static final String CLUSTER_ID = "clusterId";

  /**
   * Property key to access the similarity value property
   */
  public static final String SIM_VALUE = "value";

  /**
   * Property key to access the vertex priority property
   */
  public static final String VERTEX_PRIORITY = "vertexPriority";

  /**
   * Property key to access the degree property
   */
  public static final String DEGREE = "degree";

  /**
   * Property key to access the isCenter property
   */
  public static final String IS_CENTER = "isCenter";

  /**
   * Property key to access the isNonCenter property
   */
  public static final String IS_NON_CENTER = "isNonCenter";

  /**
   * Property key to access the deciding property
   */
  public static final String DECIDING = "deciding";

  /**
   * Property key to access the state property
   */
  public static final String STATE = "state";

  /**
   * Property key to access the centerDegree property
   */
  public static final String CENTER_DEGREE = "centerDegree";

  /**
   * Property key to access the roundNo property
   */
  public static final String ROUND_NO = "roundNo";

  /**
   * Property key to access the isSelected property
   */
  public static final String IS_SELECTED = "isSelected";

  /**
   * Property key to access the centerSide property
   */
  public static final String CENTER_SIDE = "centerSide";

  /**
   * Property key to access the componentId property
   */
  public static final String COMPONENT_ID = "componentId";

  /**
   * Property key to access the property for the number of iterations of a clustering algorithm.
   * Affinity Propagation: number of iterations that the connectedComponent of this vertex took until it
   * converged (with the last parameter config).
   */
  public static final String NR_ITERATIONS = "nrIterations";

  /**
   * Property key to access the property for the sumIterations property.
   * Affinity Propagation: Property key to access the property for the sum of all iterations that the
   * connectedComponent of this vertex took including all parameter adaptions.
   */
  public static final String SUM_ITERATIONS = "sumIterations";

  /**
   * Property key to access the property for the nrAdaptions property.
   * Affinity Propagation: Property key to access the property for the number of adaptions that the
   * connectedComponent of this vertex took until it found a parameter configuration that converged.
   */
  public static final String NR_ADAPTIONS = "nrAdaptions";

  /**
   * Property key to access the property for the original connected component id.
   */
  public static final String ORIG_COMPONENT_ID = "origComponentId";

  /**
   * Property key to access the property for the preferenceDirtySource property.
   * The self similarity of a dirty source vertex in the Affinity Propagation Algorithm.
   * A vertex needs to know both, dirty and clean source preference, for parameter adaption.
   */
  public static final String PREFERENCE_DIRTY_SOURCE = "preferenceDirtySource";

  /**
   * Property key to access the property for the preferenceCleanSource property.
   * The self similarity of a clean source vertex in the Affinity Propagation Algorithm.
   * A vertex needs to know both, dirty and clean source preference, for parameter adaption.
   */
  public static final String PREFERENCE_CLEAN_SOURCE = "preferenceCleanSource";

  /**
   * Property key to access the property for the isSingletonFromUnconvergedComponent property.
   * The config parameter {@link ApConfig#isSingletonsForUnconvergedComponents()} enables MSCD-AP to
   * generate singletons for connected components, where no solution could be found. The property is true
   * for these vertices.
   */
  public static final String IS_SINGLETON_FROM_UNCONVERGED_COMPONENT = "isSingletonFromUnconvergedComponent";

  /**
   * Property key to access the property for the isHapVertex property.
   * {@link HierarchicalAffinityPropagation}: stores weather the vertex is part of a large connected
   * component, that needs to be partitioned.
   */
  public static final String IS_HAP_VERTEX = "isHapVertex";

  /**
   * Property key to access the property for the vertexNotAssignable property.
   * {@link HierarchicalAffinityPropagation}: stores weather a vertex of a partitioned connected component
   * could be assigned to an exemplar or not.
   */
  public static final String HAP_NOT_ASSIGNABLE = "vertexNotAssignable";

  /**
   * Property key to access the property for the {@link HierarchicalAffinityPropagation} hierarchy depth
   * that a vertex maximal reached in the hierarchical divide-and-conquer clustering..
   */
  public static final String HAP_HIERARCHY_DEPTH = "hapHierarchyDepth";

  /**
   * Property key to access the nearest neighbor property used in {@link HierarchicalClusteringScatterGather}
   */
  public static final String NEAREST_NEIGHBOR = "nearestNeighbor";

  /**
   * Property key to access the centerChanged property used in {@link HierarchicalClusteringScatterGather}.
   * Indicates if a vertex center property was set from true to false during the previous gather phase
   */
  public static final String CENTER_CHANGED = "centerChanged";

  /**
   * Property key to access the cluster members property used in {@link HierarchicalClusteringScatterGather}.
   * Includes a set of all vertices from the same cluster, if the vertex is the center vertex of the cluster
   */
  public static final String CLUSTER_MEMBERS = "clusterMembers";

  /**
   * Property key to access the edges property used in {@link HierarchicalClusteringScatterGather}.
   * Stores all connected clusters and their cluster similarity using a map. This is necessary since the
   * scatter gather implementation does not allow to change a graphs edges but only the vertices
   */
  public static final String EDGES = "edges";

  /**
   * Property key to access the centerId property used in {@link HierarchicalClusteringScatterGather}.
   * Stores the vertex Id of the center vertex of the cluster the vertex is part of
   */
  public static final String CENTER_ID = "centerId";
}
