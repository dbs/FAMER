/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.PreferenceConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.PreferenceUtils;

/**
 * Joins the calculated preference values for clean and dirty sources, considering the
 * {@link PreferenceConfig}, to the vertex properties {@link PropertyNames#PREFERENCE_DIRTY_SOURCE} and
 * {@link PropertyNames#PREFERENCE_CLEAN_SOURCE}.
 */
public class PreferenceVertexCreation implements
  JoinFunction<Tuple2<EPGMVertex, String>, Tuple4<String, Double, Double, Double>, EPGMVertex> {

  /**
   * Configuration of the preference parameter for the (MSCD-) Affinity Propagation clustering algorithm.
   */
  private final PreferenceConfig preferenceConfig;

  /**
   * Constructs PreferenceVertexCreation
   *
   * @param preferenceConfig Configuration of the preference parameter for the (MSCD-) Affinity Propagation
   *                       clustering algorithm.
   */
  public PreferenceVertexCreation(PreferenceConfig preferenceConfig) {
    this.preferenceConfig = preferenceConfig;
  }

  @Override
  public EPGMVertex join(Tuple2<EPGMVertex, String> vertexTuple,
    Tuple4<String, Double, Double, Double> prefTuple) {

    if (prefTuple != null) {
      // set dirty source preference
      vertexTuple.f0.setProperty(PropertyNames.PREFERENCE_DIRTY_SOURCE,
        PreferenceUtils.getPreferenceValue(preferenceConfig, prefTuple.f1, prefTuple.f3, true));
      // set clean source preference
      vertexTuple.f0.setProperty(PropertyNames.PREFERENCE_CLEAN_SOURCE,
        PreferenceUtils.getPreferenceValue(preferenceConfig, prefTuple.f2, prefTuple.f3, false));
    } else {
      // singleton - no pref needed
      vertexTuple.f0.setProperty(PropertyNames.PREFERENCE_DIRTY_SOURCE, 1d);
      vertexTuple.f0.setProperty(PropertyNames.PREFERENCE_CLEAN_SOURCE, 1d);
    }

    return vertexTuple.f0;
  }
}
