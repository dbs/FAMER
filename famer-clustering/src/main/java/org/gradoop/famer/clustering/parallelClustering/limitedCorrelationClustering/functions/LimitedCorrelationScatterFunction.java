/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.limitedCorrelationClustering.functions;

import org.apache.flink.graph.Edge;
import org.apache.flink.graph.spargel.ScatterFunction;
import org.apache.flink.types.LongValue;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

import java.util.concurrent.ThreadLocalRandom;

import static org.gradoop.famer.clustering.parallelClustering.limitedCorrelationClustering.functions.LimitedCorrelationGatherFunction.MAX_DEGREE_AGGREGATOR;

/**
 * Scatter function used for the Limited Correlation Clustering
 */
public class LimitedCorrelationScatterFunction extends ScatterFunction<GradoopId, EPGMVertex, Long, Double> {

  /**
   * Value between 0.0 and 1.0 used in the computation for the probability for a vertex to get selected as
   * cluster center
   */
  private final double epsilon;

  /**
   * Creates an instance of LimitedCorrelationScatterFunction
   *
   * @param epsilon Value between 0.0 and 1.0 used in the computation for the probability for a vertex to get
   *                selected as luster center
   */
  public LimitedCorrelationScatterFunction(double epsilon) {
    this.epsilon = epsilon;
  }

  @Override
  public void sendMessages(org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> vertex) {
    switch (getSuperstepNumber() % 3) {
    case 1: // find max degree
      if (vertex.f1.getPropertyValue(PropertyNames.CLUSTER_ID).getLong() == 0L) {
        for (Edge<GradoopId, Double> edge : getEdges()) {
          sendMessageTo(edge.getTarget(), 1L);
        }
        sendMessageTo(vertex.getId(), 0L);
      }
      break;
    case 2: // select centers
      if (vertex.f1.getPropertyValue(PropertyNames.CLUSTER_ID).getLong() == 0L) {
        long vertexPriority =
          vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();

        if (vertex.f1.getPropertyValue(PropertyNames.CENTER_SIDE).getBoolean()) {
          // get previous aggregate
          long previousMaxDegreeValue = 0L;
          LongValue previousMaxDegreeAggregate = getPreviousIterationAggregate(MAX_DEGREE_AGGREGATOR);
          if (previousMaxDegreeAggregate != null) {
            previousMaxDegreeValue = previousMaxDegreeAggregate.getValue();
          }
          double randomNum = ThreadLocalRandom.current().nextDouble();
          double centerSelectionProbability = epsilon / (double) previousMaxDegreeValue;
          if (randomNum <= centerSelectionProbability) {
            for (Edge<GradoopId, Double> edge : getEdges()) {
              sendMessageTo(edge.getTarget(), vertexPriority);
            }
            sendMessageTo(vertex.getId(), vertexPriority);
          } else { // send fake msg
            sendMessageTo(vertex.getId(), 0L);
          }
        } else {
          if (vertex.f1.getPropertyValue(PropertyNames.DEGREE).getLong() == 0L) {
            sendMessageTo(vertex.getId(), vertexPriority);
          } else {
            sendMessageTo(vertex.getId(), 0L);
          }
        }
      }
      break;
    case 0: // grow cluster around centers
      if (vertex.f1.getPropertyValue(PropertyNames.CLUSTER_ID).getLong() == 0L) {
        if (vertex.f1.getPropertyValue(PropertyNames.IS_CENTER).getBoolean()) {
          long vertexPriority =
            vertex.f1.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
          for (Edge<GradoopId, Double> edge : getEdges()) {
            sendMessageTo(edge.getTarget(), vertexPriority);
          }
          sendMessageTo(vertex.getId(), vertexPriority);
        } else { // send fake msg
          sendMessageTo(vertex.getId(), 0L);
        }
      }
      break;
    default:
      break;
    }
  }
}
