/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.graph.Edge;
import org.apache.flink.types.NullValue;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;

/**
 * Transforms a Gradoop edge into a Gelly edge. If {@link #filterOnLabel} is set to {@code true} by setting a
 * non-null {@link #similarityEdgeLabel}, for each Gradoop edge with the given label a corresponding Gelly
 * edge will be returned. If no {@link #similarityEdgeLabel} is set for each Gradoop edge a corresponding
 * Gelly edge will be returned.
 */
public class EdgeToGellyEdgeOnSimilarityLabel implements
  FlatMapFunction<org.gradoop.common.model.impl.pojo.EPGMEdge, Edge<GradoopId, NullValue>> {
  /**
   * Whether to filter the returning edges on the {@link #similarityEdgeLabel}
   */
  private final boolean filterOnLabel;
  /**
   * Edge label to filter the returning edges
   */
  private final String similarityEdgeLabel;
  /**
   * Reduce object instantiation
   */
  private final Edge<GradoopId, NullValue> reuseEdge;

  /**
   * Creates an instance of EdgeToGellyEdgeOnSimilarityLabel
   */
  public EdgeToGellyEdgeOnSimilarityLabel() {
    this(null);
  }
  /**
   * Creates an instance of EdgeToGellyEdgeOnSimilarityLabel
   *
   * @param similarityEdgeLabel Edge label to filter the returning edges
   */
  public EdgeToGellyEdgeOnSimilarityLabel(String similarityEdgeLabel) {
    this.filterOnLabel = similarityEdgeLabel != null;
    this.similarityEdgeLabel = similarityEdgeLabel;
    this.reuseEdge = new Edge<>();
  }


  @Override
  public void flatMap(org.gradoop.common.model.impl.pojo.EPGMEdge edge,
    Collector<Edge<GradoopId, NullValue>> out) throws Exception {
    if (!filterOnLabel || similarityEdgeLabel.equals(edge.getLabel())) {
      reuseEdge.setSource(edge.getSourceId());
      reuseEdge.setTarget(edge.getTargetId());
      reuseEdge.setValue(NullValue.getInstance());
      out.collect(reuseEdge);
    }
  }
}
