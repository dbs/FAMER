/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.serialClusteringOnConnectedComponents.functions;

import org.apache.flink.api.java.functions.KeySelector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * KeySelector for CoGroupFunction that selects the cluster id of a given gradoop vertex
 */
public class EPGMVertexClusterIdSelector implements KeySelector<EPGMVertex, String> {

  @Override
  public String getKey(EPGMVertex vertex) throws Exception {
    return vertex.getPropertyValue(PropertyNames.CLUSTER_ID).toString();
  }
}
