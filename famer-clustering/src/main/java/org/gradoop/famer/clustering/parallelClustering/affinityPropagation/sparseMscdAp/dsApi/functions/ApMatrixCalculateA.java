/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * Calculate the ALPHA value of the AP multiMatrix. Join the the column sums of the RHO field to the
 * columns of the multiMatrix and calculate ALPHA out of the column sum.
 */
public class ApMatrixCalculateA  implements JoinFunction<ApMultiMatrixCell, ApMultiMatrixCell, ApMultiMatrixCell> {

  /**
   * damps ALPHA by a ratio of its value from the last iteration
   */
  private final double dampingFactor;

  /**
   * Constructs ApMatrixCalculateA
   *
   * @param dampingFactor damps ALPHA by a ratio of its value from the last iteration
   */
  public ApMatrixCalculateA(double dampingFactor) {
    this.dampingFactor = dampingFactor;
  }

  @Override
  public ApMultiMatrixCell join(ApMultiMatrixCell allValuesCell, ApMultiMatrixCell columnSum) {
    if (!columnSum.isFlagActivated()) {
      throw new IllegalStateException(
        "Singleton found in alpha calculation, but it should have been filtered out before");
    }

    double columnSumOfAllOthers = columnSum.getTmp();
    // remove own value from columnSum
    // => we want the sum of the values of all other rows of that column
    if (allValuesCell.getRowId().equals(allValuesCell.getColumnId())) {
      // the diagonal values is always in the columnSum, just as it is
      columnSumOfAllOthers -= allValuesCell.getTmp();
    } else {
      // the other values are only included if they are positive
      columnSumOfAllOthers -= Math.max(0, allValuesCell.getTmp());
      // for non diagonal values => the result is only taken if it is negative
      columnSumOfAllOthers = Math.min(0, columnSumOfAllOthers);
    }

    // damping
    double oldValueDamped = allValuesCell.getA() * dampingFactor;
    double newValue = columnSumOfAllOthers * (1 - dampingFactor) + oldValueDamped;

    allValuesCell.setA(newValue);
    // remove R value from tmp
    allValuesCell.setTmp(0d);
    return allValuesCell;
  }
}
