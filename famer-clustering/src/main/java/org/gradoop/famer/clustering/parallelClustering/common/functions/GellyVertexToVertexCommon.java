/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Extracts the value (a Gradoop vertex) from a Gelly vertex - used in all parallel clustering algorithms.
 */
public class GellyVertexToVertexCommon implements
  MapFunction<org.apache.flink.graph.Vertex<GradoopId, EPGMVertex>, EPGMVertex> {

  @Override
  public EPGMVertex map(org.apache.flink.graph.Vertex<GradoopId, EPGMVertex> gellyVertex) throws Exception {
    return gellyVertex.getValue();
  }
}
