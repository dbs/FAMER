/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.postprocessing;

import org.gradoop.flink.model.api.operators.UnaryGraphToGraphOperator;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * The abstract class all cluster postprocessing algorithms inheriting from. Implements the Gradoop
 * {@link UnaryGraphToGraphOperator} to be executable as operator on a {@link LogicalGraph}.
 */
public abstract class AbstractClusterPostprocessing implements UnaryGraphToGraphOperator {

  /**
   * Creates an instance of AbstractParallelClustering
   */
  public AbstractClusterPostprocessing() {
  }

  @Override
  public LogicalGraph execute(LogicalGraph graph) {
    return this.runPostprocessing(graph);
  }

  /**
   * Executes the cluster postprocessing algorithm. Needs to be implemented by inheriting classes.
   *
   * @param graph The input {@link LogicalGraph}
   *
   * @return The processed clustered {@link LogicalGraph}
   */
  protected abstract LogicalGraph runPostprocessing(LogicalGraph graph);
}
