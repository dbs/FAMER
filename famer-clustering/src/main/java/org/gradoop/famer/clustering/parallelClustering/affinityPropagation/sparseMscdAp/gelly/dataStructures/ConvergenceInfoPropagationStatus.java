/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;

/**
 * Describes the status of the propagation of information about the convergence and the constraints
 * satisfaction through the vertices of a connected component. By the propagation status a vertex makes a
 * decision, which action it needs to execute in the current superStep.
 */
public enum ConvergenceInfoPropagationStatus {

  /**
   * The information is not propagated through all vertices of all connected components. Go on with the
   * info propagation.
   */
  CONTINUE_INFO_PROPAGATION,

  /**
   * All vertices of a connected component successfully converged and satisfied the constraints. Stop
   * message passing for all vertices of the component.
   */
  STOP_MESSAGE_PASSING,

  /**
   * All vertices of the connected component converged, but some did not satisfy the constraints. Adapt the
   * AP parameters and restart the message passing procedure.
   */
  ADAPT_PARAMETERS_AND_RESTART,

  /**
   * By now {@link ApConfig#getConvergenceIter()} is not reached, so info propagation is not necessary. Go
   * on with the AP calculation without propagating info through the component.
   */
  CONTINUE_AP
}
