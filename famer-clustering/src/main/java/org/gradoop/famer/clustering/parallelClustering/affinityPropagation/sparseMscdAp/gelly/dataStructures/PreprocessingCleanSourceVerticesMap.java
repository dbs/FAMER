/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * A map of each clean source name and all vertices of each clean source of the connected component. This
 * is necessary for the preprocessing, when an all-equal-component was detected. When all similarities
 * between all data points of a connected component are equal (and lower than
 * {@link ApConfig#getAllSameSimClusteringThreshold()}), all data points form a single cluster. But clean
 * source data points must be separated. So of each clean source, each vertex with the highest vertexId
 * gets into the common cluster. All other vertices of that clean source form singletons.
 */
public class PreprocessingCleanSourceVerticesMap {

  /**
   * Maps a set, containing all vertexIds of the vertices of a clean source, to their clean sources name.
   */
  private Map<String, Set<Long>> cleanSourceVertices;

  /**
   * Constructs PreprocessingCleanSourceVerticesMap
   */
  public PreprocessingCleanSourceVerticesMap() {
    cleanSourceVertices = new HashMap<>();
  }

  /**
   * Put the vertexId into the {@link #cleanSourceVertices} map for the specified source
   *
   * @param source name of the vertex's clean source
   * @param vertexId unique long ID of the vertex
   */
  public void putCleanSourceVertex(String source, Long vertexId) {
    if (!cleanSourceVertices.containsKey(source)) {
      cleanSourceVertices.put(source, new HashSet<>());
    }
    cleanSourceVertices.get(source).add(vertexId);
  }

  /**
   * Get the number of vertices for the specified source in {@link #cleanSourceVertices}
   *
   * @param source name of a clean source
   * @return count of vertices for the specified source
   */
  public int getSourceVertexCount(String source) {
    return cleanSourceVertices.get(source).size();
  }

  /**
   * Check whether the specified vertexId is the largest one of the specified clean source in
   * {@link #cleanSourceVertices}.
   *
   * @param vertexId ID to be checked, if it is the largest of its source
   * @param source name of the source whose vertexIds are to be checked
   * @return true, if the specified vertexId is the largest of its source
   */
  public Boolean isVertexIdLargestOfSource(Long vertexId, String source) {
    long maxVertexId = 0L;
    for (Long id : cleanSourceVertices.get(source)) {
      maxVertexId = Math.max(id, maxVertexId);
    }
    return vertexId == maxVertexId;
  }

  /**
   * Add all vertexIds to the {@link #cleanSourceVertices} set of the specified source.
   *
   * @param source clean source to which the vertexIds are mapped to
   * @param vertexIds set of vertexIds belonging to the clean source
   * @return true, if the set changed
   */
  public boolean putAllCleanSourceVertices(String source, Collection<Long> vertexIds) {
    if (!cleanSourceVertices.containsKey(source)) {
      cleanSourceVertices.put(source, new HashSet<>());
    }
    return cleanSourceVertices.get(source).addAll(vertexIds);
  }

  /**
   * Adds all map entries of {@code otherCsV} to {@link #cleanSourceVertices}.
   *
   * @param otherCsV map of cleanSourceName to vertexIds that is merged to {@link #cleanSourceVertices}
   * @return true, if new information was added to {@link #cleanSourceVertices} by this merge
   */
  public boolean mergeCleanSourceVertices(Map<String, Set<Long>> otherCsV) {
    boolean gotNewInformation = false;

    for (Map.Entry<String, Set<Long>> entry : otherCsV.entrySet()) {
      if (this.putAllCleanSourceVertices(entry.getKey(), entry.getValue())) {
        gotNewInformation = true;
      }
    }

    return gotNewInformation;
  }

  /**
   * Add all vertexIds of the {@code otherMap} to {@link #cleanSourceVertices}.
   *
   * @param otherMap Another instance of {@link PreprocessingCleanSourceVerticesMap} that shall be merged
   *                 with this one.
   * @return true, if new information was added to {@link #cleanSourceVertices} by this merge
   */
  public boolean addAllElementsOfOtherMap(PreprocessingCleanSourceVerticesMap otherMap) {
    return mergeCleanSourceVertices(otherMap.getCleanSourceVertices());
  }

  public Map<String, Set<Long>> getCleanSourceVertices() {
    return cleanSourceVertices;
  }

  public void setCleanSourceVertices(Map<String, Set<Long>> cleanSourceVertices) {
    this.cleanSourceVertices = cleanSourceVertices;
  }
}
