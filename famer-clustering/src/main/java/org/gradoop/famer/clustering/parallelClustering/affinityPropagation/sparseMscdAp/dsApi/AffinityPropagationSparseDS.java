/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi;

import org.apache.flink.api.common.functions.FlatJoinFunction;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.api.common.operators.base.JoinOperatorBase.JoinHint;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.ExecutionEnvironment;
import org.apache.flink.api.java.operators.DeltaIteration;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.api.java.utils.DataSetUtils;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMElement;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.AbstractAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.functions.GetVertexInformation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.functions.PreferencePercentileCalculation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.AllEqualComponentExemplarAssignment;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixAdaptParameters;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixAssignExemplars;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixCalculateA;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixCalculateDecisionSum;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixCalculateR;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixFlagFilterFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixGetConnCompGroupConvergence;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixMaxValueFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixNoise;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixSetFixedPreference;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ApMatrixSetPreferenceGeneric;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.CreateApMatrixFromEdgeTuples;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.CreateDiagonalApMatrixCells;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.ExtractVerticesFromEqualCompEdges;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.GetAllEqualComponentInformation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.JoinEdgeWithSourceInfo;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.JoinSingletonsToApMatrix;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions.JoinVerticesForAllEqualComp;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.AffinityPropagationSparseGelly;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.bool.False;
import org.gradoop.flink.model.impl.functions.tuple.Value1Of2;

/**
 *  The Gradoop/Flink implementation of the Affinity Propagation Algorithm (AP) by Frey and Dueck and its
 *  extension, Multi-Source Clean-Dirty AP (MSCD-AP). This implementation takes benefit of a low density of
 *  the input similarity graph. Messages between data points are only necessary, when an edge is present in
 *  the similarity graph. Only these necessary messages are calculated. That's why it's called Sparse AP.
 *  Different to {@link AffinityPropagationSparseGelly}, this implementation uses Flink's DataSet-API,
 *  to process AP by calculating and manipulating matrices.
 *  <p>
 *  <b>original paper of AP:</b> FREY, Brendan J.; DUECK, Delbert. Clustering by passing messages between
 *  data points. science, 2007, 315. Jg., Nr. 5814, S. 972-976.
 */
public class AffinityPropagationSparseDS extends AbstractAffinityPropagation {

  /**
   * Constructor with default parameter settings.
   */
  public AffinityPropagationSparseDS() {
    super();
  }

  /**
   * Constructor with default parameter settings and custom random seed for reproducible results.
   *
   * @param randomSeed random seed for reproducible results.
   */
  public AffinityPropagationSparseDS(long randomSeed) {
    super(randomSeed);
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public AffinityPropagationSparseDS(JSONObject jsonConfig) {
    super(jsonConfig);
  }

  @Override
  protected String getConfigName() {
    return "apConfig";
  }

  @Override
  protected ApConfig getNewConfig() {
    return new ApConfig();
  }

  /**
   * Execute the Sparse (MSCD-) AP clustering algorithm for the passed similarity-graph. Creates a clustered
   * graph with each vertex containing the following properties:
   * <ul>
   *   <li>{@link PropertyNames#CLUSTER_ID}</li>
   * </ul>
   *
   * @param inputGraph Similarity-Graph of the linking step.
   * @return Clustered graph with the vertices containing the clustering information.
   */
  @Override
  public LogicalGraph runClustering(LogicalGraph inputGraph) {
    apConfig.checkConfigCorrectness();

    if (apConfig.isCleanSourceExtensionActivated()) {
      // remove edges of two entities from the same clean source
      inputGraph = prepareGraphForMSCD(inputGraph);
    }

    /* (0) Preprocessing-----------------------------------------------------------------------------------*/
    // Get Connected Components
    inputGraph = inputGraph.callForGraph(new ConnectedComponents(maxIteration));

    // Build vertex info tuples (connectedComponentId, cleanSourceInformation)
    DataSet<Tuple3<GradoopId, String, String>> vertexConnCompCsInfo =
      inputGraph.getVertices().map(new GetVertexInformation(apConfig));

    // Build edge info tuples - result: Tuple5<SourceId(RowId),TargetId(ColumnId),SimValue,ConnCompId,CsInfo>
    DataSet<Tuple5<GradoopId, GradoopId, Double, String, String>> edgeTuples = inputGraph.getEdges()
      .join(vertexConnCompCsInfo, JoinHint.REPARTITION_HASH_SECOND)
      .where(EPGMEdge::getSourceId).equalTo(0)
      .with(new JoinEdgeWithSourceInfo());

    /* (1) Process All-equal-components ---------------------------------------------------------------------
     *     Find Connected Components where all similarities are equal:
     *      => exclude them from the rest of the AP pipeline
     *      => assign their cluster membership separately
     *     group by connectedComponent:
     *      => f4 = true  => all similarities equal
     *      => f4 = false => not all similarities equal */
    DataSet<Tuple6<GradoopId, GradoopId, Double, String, String, Boolean>> edgesWithAllEqualComponentInfo =
      edgeTuples.groupBy(3).reduceGroup(new GetAllEqualComponentInformation());

    // filtering => remove the edges of allEqual-ConnectedComponents for the AP Pipeline
    edgeTuples = edgesWithAllEqualComponentInfo.filter(edgeTuple -> !edgeTuple.f5).project(0, 1, 2, 3, 4);

    // Enrich vertices with the information, whether they belong to an all-equal-component or not.
    DataSet<Tuple5<GradoopId, Double, String, String, Boolean>> verticesWithAllEqualComponentInfo =
      getVerticesWithAllEqualComponentInfo(edgesWithAllEqualComponentInfo, vertexConnCompCsInfo);

    // filter for the vertices that are not in allEqualComponents, is later joined to the AP Matrix to get
    // the diagonal and find singletons, result: Tuple3<VertexId,ConnCompId,CsInfo>
    DataSet<Tuple3<GradoopId, String, String>> verticesNotInAllEqualComps =
      verticesWithAllEqualComponentInfo.filter(vertexTuple -> !vertexTuple.f4).project(0, 2, 3);

    // filter for vertices of allEqualComponents, now enriched with source info
    // - result: Tuple4<VertexId,SimValue,ConnCompId,CsInfo>
    DataSet<Tuple4<GradoopId, Double, String, String>> verticesOfAllEqualComponentsWithSourceInfo
      = verticesWithAllEqualComponentInfo.filter(vertexTuple -> vertexTuple.f4).project(0, 1, 2, 3);

    // assign the vertices of the allEqual-Components to their exemplars
    // => depending on allSameSimClusteringThreshold and cleanSourceInformation
    DataSet<Tuple2<GradoopId, GradoopId>> allEqualComponentExemplarAssignments =
      verticesOfAllEqualComponentsWithSourceInfo.groupBy(2)
        .reduceGroup(new AllEqualComponentExemplarAssignment(apConfig.getAllSameSimClusteringThreshold()));

    // get the distinct exemplars of the allEqualComponent clusters
    DataSet<Tuple1<GradoopId>> allEqualComponentExemplars =
      allEqualComponentExemplarAssignments.project(1);
    allEqualComponentExemplars = allEqualComponentExemplars.distinct(0);

    // transform exemplars to ap matrix cell so they can be united with the ap result exemplars and singletons
    DataSet<ApMultiMatrixCell> allEqualComponentExemplarApCells =
      allEqualComponentExemplars.map((MapFunction<Tuple1<GradoopId>, ApMultiMatrixCell>) tuple -> {
        ApMultiMatrixCell cell = new ApMultiMatrixCell();
        cell.setRowId(tuple.f0);
        return cell;
      }).returns(new TypeHint<ApMultiMatrixCell>() { });

    /* (2) Preference Percentile Calculation ----------------------------------------------------------------
     *  calculate preference percentiles (if configured) from edge tuples
     *  => for each connected component
     *  => so the algorithm runs as a whole for all components at the same time but work as they would be
     *     separated
     *  - result: Tuple4<ConnCompId,PercentileValueDS,PercentileValueCS,MinSimilarity> */
    DataSet<Tuple4<String, Double, Double, Double>> preferencePercentileTuples = null;
    if (!apConfig.getPreferenceConfig().useOnlyFixedPreferenceValues()) {
      preferencePercentileTuples =
        edgeTuples.groupBy(3).reduceGroup(new PreferencePercentileCalculation(apConfig));
    }

    /* (3) Create AP-MultiMatrix ---------------------------------------------------------------------------
     *  create a symmetric matrix of ApMultiMatrixCells out of the edges
     *  => add information of rowId, columnId, similarity, connectedComponentId */
    DataSet<ApMultiMatrixCell> apMatrix = edgeTuples.flatMap(new CreateApMatrixFromEdgeTuples(apConfig));

    /* (4) Process Singletons -----------------------------------------------------------------------------
     *  find singletons (set flagActivated = true)
     *  => enrich the edges with source information of the vertex
     *  => join only on the vertices that are not contained in allEqualComponents */
    apMatrix = verticesNotInAllEqualComps
      .leftOuterJoin(apMatrix, JoinHint.REPARTITION_HASH_FIRST)
      .where(0).equalTo(ApMultiMatrixCell.ROW_ID_FIELD)
      .with(new JoinSingletonsToApMatrix());

    // get the singletons from the matrix
    DataSet<ApMultiMatrixCell> singletons = apMatrix.filter(new ApMatrixFlagFilterFunction(true));

    // remove the singletons from the matrix
    apMatrix = apMatrix.filter(new ApMatrixFlagFilterFunction(false));

    /* (5) Prepare Ap-MultiMatrix Diagonal ----------------------------------------------------------------
     *  Create the diagonal ApMultiMatrixCells, containing the preference information.
     *  Exclude the singletons from the diagonal. */
    DataSet<ApMultiMatrixCell> diagonal =
      createDiagonalMultiMatrixCells(verticesNotInAllEqualComps, preferencePercentileTuples, singletons);

    /* (6) Add Noise -------------------------------------------------------------------------------------*/
    // add the diagonal to the similarity matrix
    apMatrix = apMatrix.union(diagonal);
    // add noise
    apMatrix = apMatrix.map(new ApMatrixNoise(apConfig, random));

    /* (7) Iterative Message Passing ----------------------------------------------------------------------*/
    DataSet<ApMultiMatrixCell> finalMatrix =
      executeApLoop(apMatrix, inputGraph.getConfig().getExecutionEnvironment());

    /* (8) Postprocessing -----------------------------------------------------------------------------------
     *  => unite iteration result, singletons and All-Equal-Components
     *  => assign dataPoints to exemplars by the criterion value A+R */
    DataSet<EPGMVertex> verticesWithClusterIds = getVerticesWithFinalClusterIds(finalMatrix, singletons,
      allEqualComponentExemplarApCells, allEqualComponentExemplarAssignments, inputGraph);

    LogicalGraph resultGraph = inputGraph.getConfig().getLogicalGraphFactory()
      .fromDataSets(inputGraph.getGraphHead(), verticesWithClusterIds, inputGraph.getEdges());

    return resultGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  /**
   * Enrich vertices with the information whether they belong to an all-equal-component or not.
   *
   * @param edgesWithAllEqualComponentInfo edge tuples with: rowId, columnId, similarity,
   *                                       connectedComponentId, rowSourceInfo and allEqualComponentInfo
   * @param vertexConnCompCsInfo vertex tuples with: vertexId, ConnectedComponentId and SourceName
   *
   * @return input vertex tuples, enriched by allEqualComponentInfo
   */
  public DataSet<Tuple5<GradoopId, Double, String, String, Boolean>> getVerticesWithAllEqualComponentInfo(
    DataSet<Tuple6<GradoopId, GradoopId, Double, String, String, Boolean>> edgesWithAllEqualComponentInfo,
    DataSet<Tuple3<GradoopId, String, String>> vertexConnCompCsInfo) {
    // get only the vertices of the all equal components, make sure each vertex is contained only once
    DataSet<Tuple3<GradoopId, Double, String>> verticesOfAllEqualComponents =
      edgesWithAllEqualComponentInfo.flatMap(new ExtractVerticesFromEqualCompEdges()).distinct(0);
    // enrich vertices with source information, set boolean flag for belonging to allEqualComponents
    return vertexConnCompCsInfo.leftOuterJoin(verticesOfAllEqualComponents, JoinHint.BROADCAST_HASH_SECOND)
      .where(0).equalTo(0)
      .with(new JoinVerticesForAllEqualComp());
  }

  /**
   * Create the diagonal {@link ApMultiMatrixCell}s, containing the preference information. Exclude the
   * singletons from the diagonal.
   *
   * @param verticesNotInAllEqualComps tuples for all vertices that need to be clustered by AP (including
   *                                   the singletons)
   * @param preferencePercentileTuples preference information tuples. The set is null, when only fixed
   *                                   preference values are used
   * @param singletons {@link ApMultiMatrixCell}s of singletons, to be excluded from the diagonal
   *
   * @return diagonal {@link ApMultiMatrixCell}s, containing the preference information
   */
  public DataSet<ApMultiMatrixCell> createDiagonalMultiMatrixCells(
    DataSet<Tuple3<GradoopId, String, String>> verticesNotInAllEqualComps,
    DataSet<Tuple4<String, Double, Double, Double>> preferencePercentileTuples,
    DataSet<ApMultiMatrixCell> singletons) {

    // create the diagonal elements (including the singletons)
    DataSet<ApMultiMatrixCell> diagonal =
      verticesNotInAllEqualComps.map(new CreateDiagonalApMatrixCells(apConfig));

    // add the preference value to the diagonal elements
    if (apConfig.getPreferenceConfig().useOnlyFixedPreferenceValues()) {
      // map the fixed preference values to the diagonal
      diagonal = diagonal.map(new ApMatrixSetFixedPreference(apConfig.getPreferenceConfig()));
    } else {
      // join the percentile preference values (or minimum value) to the diagonal
      diagonal = diagonal.join(preferencePercentileTuples, JoinHint.BROADCAST_HASH_SECOND)
        .where(ApMultiMatrixCell.CONNECTED_COMP_ID_FIELD).equalTo(0)
        .with(new ApMatrixSetPreferenceGeneric(apConfig.getPreferenceConfig()));
    }

    // remove singletons from the diagonal
    diagonal = diagonal.leftOuterJoin(singletons, JoinHint.REPARTITION_HASH_SECOND)
      .where(ApMultiMatrixCell.ROW_ID_FIELD).equalTo(ApMultiMatrixCell.ROW_ID_FIELD)
      .with((FlatJoinFunction<ApMultiMatrixCell, ApMultiMatrixCell, ApMultiMatrixCell>)
        (diagElement, singletonElement, out) -> {
        if (singletonElement == null) {
          // only collect diagonal elements if they are not in the singletons set
          out.collect(diagElement);
        }
      }).returns(new TypeHint<ApMultiMatrixCell>() { });

    return diagonal;
  }

  /**
   * Execute the iterative matrix-value calculation for the message-value-matrices in the ApMultiMatrix.
   * Calculate the values for all matrices in a delta iteration, check for convergence and if the
   * consistency constraints are satisfied.
   *
   * @param apMatrix ApMultiMatrix, excluding singletons and all-equal-components
   * @param env Flink execution environment
   *
   * @return the input ApMultiMatrix, containing the converged and consistent result of the AP iteration
   */
  public DataSet<ApMultiMatrixCell> executeApLoop(DataSet<ApMultiMatrixCell> apMatrix,
    ExecutionEnvironment env) {
    // generate an empty DataSet for the top hierarchy level exemplar results of the loop
    DataSet<Tuple2<String, ApMultiMatrixCell>> initialSolutionSet =
      env.fromElements(Tuple2.of("", new ApMultiMatrixCell())).filter(new False<>());

    DataSet<Tuple2<String, ApMultiMatrixCell>> apMatrixTupleSet =
      apMatrix.map((MapFunction<ApMultiMatrixCell, Tuple2<String, ApMultiMatrixCell>>) apMultiMatrixCell ->
        Tuple2.of(apMultiMatrixCell.getKey(), apMultiMatrixCell))
        .returns(new TypeHint<Tuple2<String, ApMultiMatrixCell>>() { });

    /*
     * Affinity Propagation Calculation Loop
     */
    // ################################ Iteration Block - Start ##########################################

    DeltaIteration<Tuple2<String, ApMultiMatrixCell>, Tuple2<String, ApMultiMatrixCell>> loop =
      initialSolutionSet.iterateDelta(apMatrixTupleSet, apConfig.getMaxApIteration(), 0);

    DataSet<ApMultiMatrixCell> newMatrix = loop.getWorkset().map(new Value1Of2<>());

    /*------------------------------------------
    (7.1) Execute AP Message Calculations
     ------------------------------------------*/

    newMatrix = apPart1OneOfNConstraint(newMatrix);

    if (apConfig.isCleanSourceExtensionActivated()) {
      newMatrix = apPart2CleanSourcesConstraint(newMatrix);
    }

    newMatrix = apPart3ExemplarConsistencyConstraint(newMatrix);

    /*------------------------------------------
    (7.2) Check for Convergence
     ------------------------------------------*/
    /*
     * AP converged if all exemplar decisions did not change in the last convergenceIter iterations
     * => calculate the decisionSum that represents the decisions of the last iterations
     */
    newMatrix = newMatrix.map(new ApMatrixCalculateDecisionSum());

    /* if connComp contains diagonal element with changed decision over the last convergenceIter iterations
     * => set flag of all cells of the component FALSE => take them to the next iteration
     * => else set flag of all cells of the component true => they converged and go to solution set (if they
     *    don't violate the constraints)
     */
    newMatrix = newMatrix.groupBy(ApMultiMatrixCell.CONNECTED_COMP_ID_FIELD)
      .reduceGroup(new ApMatrixGetConnCompGroupConvergence(apConfig.getConvergenceIter()));

    /* get the unconverged cells
     * if connectedComponent contains diagonal element with changed decision
     * => take all cells of the component for the next iteration
     * => if the nextIterationWorkingSet is empty then the iteration terminates
     */
    DataSet<ApMultiMatrixCell> nextIterationWorkingSet =
      newMatrix.filter(new ApMatrixFlagFilterFunction(false));

    // get the converged cells
    DataSet<ApMultiMatrixCell> convergedSet =
      newMatrix.filter(new ApMatrixFlagFilterFunction(true));

    /*------------------------------------------
    (7.3) Check Constraints
     ------------------------------------------*/

    // get all the connected components from the converged set that violate any of the constraints
    DataSet<Tuple1<String>> constraintViolatingComponents =
      getConnComponentsThatViolateConstraint(convergedSet);

    // mark the components that satisfy (true) and the ones that violate (false) the constraints with the flag
    // => adapt the preference of the constraint hurting components
    convergedSet = convergedSet.leftOuterJoin(constraintViolatingComponents, JoinHint.BROADCAST_HASH_SECOND)
      .where(ApMultiMatrixCell.CONNECTED_COMP_ID_FIELD).equalTo(0)
      .with((JoinFunction<ApMultiMatrixCell, Tuple1<String>, ApMultiMatrixCell>) (cell, connCompId) -> {
        if (connCompId == null) {
          cell.setFlagActivated(true);       // cell converged and satisfied constraints => to final matrix
        } else {
          cell.setFlagActivated(false);      // cell converged but hurt constraints => go to next iteration
          cell.setAdaptionIterCount(Integer.MAX_VALUE); // set iterCount to MAX so that parameters are adapted
        }
        return cell;
      }).returns(new TypeHint<ApMultiMatrixCell>() { });

    // converged components that satisfy the constraints go to the solution set
    DataSet<ApMultiMatrixCell> solutionSetDelta =
      convergedSet.filter(new ApMatrixFlagFilterFunction(true));

    // converged components that hurt the constraints go to the next iteration set
    DataSet<ApMultiMatrixCell> constraintViolatingSet =
      convergedSet.filter(new ApMatrixFlagFilterFunction(false));

    nextIterationWorkingSet = nextIterationWorkingSet.union(constraintViolatingSet);

    // adapt the preference of the next iteration set if they passed maxIterations
    nextIterationWorkingSet = nextIterationWorkingSet.map(new ApMatrixAdaptParameters(apConfig));

    if (apConfig.isSingletonsForUnconvergedComponents()) {
      // get singletons resulting from components that could not converge
      solutionSetDelta = solutionSetDelta.union(
        nextIterationWorkingSet.filter(cell -> cell.isFlagActivated() && cell.isDiagonal()));
      // the rest goes to the next iterations working set
      nextIterationWorkingSet = nextIterationWorkingSet.filter(new ApMatrixFlagFilterFunction(false));
    }

    // map next iteration working set and solution set to tuples containing the multiMatrixCell-Key
    DataSet<Tuple2<String, ApMultiMatrixCell>> nextIterationWorkingTupleSet = nextIterationWorkingSet
      .map((MapFunction<ApMultiMatrixCell, Tuple2<String, ApMultiMatrixCell>>) apMultiMatrixCell ->
        Tuple2.of(apMultiMatrixCell.getKey(), apMultiMatrixCell))
      .returns(new TypeHint<Tuple2<String, ApMultiMatrixCell>>() { });

    DataSet<Tuple2<String, ApMultiMatrixCell>> solutionTupleSetDelta = solutionSetDelta
      .map((MapFunction<ApMultiMatrixCell, Tuple2<String, ApMultiMatrixCell>>) apMultiMatrixCell ->
        Tuple2.of(apMultiMatrixCell.getKey(), apMultiMatrixCell))
      .returns(new TypeHint<Tuple2<String, ApMultiMatrixCell>>() { });

    DataSet<Tuple2<String, ApMultiMatrixCell>> finalMatrixTuples =
      loop.closeWith(solutionTupleSetDelta, nextIterationWorkingTupleSet);

    // ################################ Iteration Block - End ##########################################

    return finalMatrixTuples.map(new Value1Of2<>());
  }

  /**
   * Check the ApMultiMatrix to satisfy the consistency constraints. Get a DataSet of all connected
   * component names that violate one of the 3 consistency constraints
   *
   * @param apMatrix ApMultiMatrix that contains the result of a converged AP iteration
   *
   * @return a DataSet of all connected component names that violate one of the 3 consistency constraints
   */
  private DataSet<Tuple1<String>> getConnComponentsThatViolateConstraint(
    DataSet<ApMultiMatrixCell> apMatrix) {
    // check self selection constraint
    DataSet<Tuple1<String>> selfSelectionConstraintViolation =
      apMatrix.groupBy(ApMultiMatrixCell.CONNECTED_COMP_ID_FIELD, ApMultiMatrixCell.COLUMN_ID_FIELD)
        .reduceGroup((GroupReduceFunction<ApMultiMatrixCell, Tuple1<String>>) (group, out) -> {
          String connectedCompId = null;
          boolean diagonalChooseItself = false;
          boolean columnElementChoosenByRowElement = false;

          for (ApMultiMatrixCell cell : group) {
            if (cell.isDiagonal()) {
              diagonalChooseItself = cell.isDiagonalExemplar();
            } else if (cell.hasCriterionSatisfied()) {
              columnElementChoosenByRowElement = true;
            }

            if (connectedCompId == null) {
              connectedCompId = cell.getConnectedComponentId();
            }
          }

          if (columnElementChoosenByRowElement && !diagonalChooseItself) {
            // constraint hurt
            out.collect(Tuple1.of(connectedCompId));
          }
        }).returns(new TypeHint<Tuple1<String>>() { });

    // the row exemplar selection constraint is a little lowered here
    // each row must select at least one exemplar (and not exactly one - because later the one with the
    // highest value is selected)
    DataSet<Tuple1<String>> rowExemplarSelectionConstraintViolation =
      apMatrix.groupBy(ApMultiMatrixCell.CONNECTED_COMP_ID_FIELD, ApMultiMatrixCell.ROW_ID_FIELD)
        .reduceGroup((GroupReduceFunction<ApMultiMatrixCell, Tuple1<String>>) (group, out) -> {
          String connectedCompId = null;
          boolean rowSelectedAnExemplar = false;

          for (ApMultiMatrixCell cell : group) {
            if (connectedCompId == null) {
              connectedCompId = cell.getConnectedComponentId();
            }
            if (cell.hasCriterionSatisfied()) {
              rowSelectedAnExemplar = true;
              break;
            }
          }

          if (!rowSelectedAnExemplar) {
            out.collect(Tuple1.of(connectedCompId));
          }
        }).returns(new TypeHint<Tuple1<String>>() { });

    DataSet<Tuple1<String>> constraintViolatingComponents =
      selfSelectionConstraintViolation.union(rowExemplarSelectionConstraintViolation);

    if (apConfig.isCleanSourceExtensionActivated()) {
      DataSet<Tuple1<String>> cleanSourceConstraintViolation = apMatrix.groupBy(
        ApMultiMatrixCell.CONNECTED_COMP_ID_FIELD,
        ApMultiMatrixCell.ROW_CLEAN_SOURCE_FIELD,
        ApMultiMatrixCell.COLUMN_ID_FIELD)
        .reduceGroup((GroupReduceFunction<ApMultiMatrixCell, Tuple1<String>>) (group, out) -> {
          String connectedCompId = null;
          int nrOfRowsFromCsThatChooseThisColumn = 0;
          for (ApMultiMatrixCell cell : group) {
            if (cell.getRowCleanSource().equals("")) {
              // only groups of real clean sources need to be checked
              break;
            }
            if (connectedCompId == null) {
              connectedCompId = cell.getConnectedComponentId();
            }
            if (cell.hasCriterionSatisfied()) {
              nrOfRowsFromCsThatChooseThisColumn++;
            }
          }

          if (nrOfRowsFromCsThatChooseThisColumn > 1) {
            out.collect(Tuple1.of(connectedCompId));
          }
        }).returns(new TypeHint<Tuple1<String>>() { });

      constraintViolatingComponents = constraintViolatingComponents.union(cleanSourceConstraintViolation);
    }

    return constraintViolatingComponents.distinct();    // remove double entries
  }

  /**
   * Ap Part 1: calculate Eta
   * <p>
   * Computes the first portion of the AffinityPropagation iteration sequence. Calculate the message values
   * for the BETA and ETA matrices. Separating this part allows easier testing.
   *
   * @param apMatrix ApMultiMatrix to be calculated
   *
   * @return the input matrix, enriched by the new values for ETA
   */
  public DataSet<ApMultiMatrixCell> apPart1OneOfNConstraint(DataSet<ApMultiMatrixCell> apMatrix) {
    // 1.1 calculate Beta into tmp
    apMatrix = apMatrix.map((MapFunction<ApMultiMatrixCell, ApMultiMatrixCell>) cell -> {
      double beta = cell.getA() + cell.getS() + cell.getTheta();
      cell.setTmp(beta);
      return cell;
    }).returns(new TypeHint<ApMultiMatrixCell>() { });

    // 1.2 for each row:
    // beta 1st max value => tmp
    // beta 1st maxID => columnId
    // beta 2nd max value => tmp2
    DataSet<ApMultiMatrixCell> highestRowValue = apMatrix.groupBy(ApMultiMatrixCell.ROW_ID_FIELD)
      .reduce(new ApMatrixMaxValueFunction(true));

    // 1.3 calculate Eta
    // for each row we now have one element with highestValue, highestValueId and secondHighestValue
    // => join them to the matrix amd calculate the value for eta
    apMatrix = apMatrix.join(highestRowValue, JoinHint.REPARTITION_HASH_SECOND)
      .where(ApMultiMatrixCell.ROW_ID_FIELD).equalTo(ApMultiMatrixCell.ROW_ID_FIELD)
      .with((JoinFunction<ApMultiMatrixCell, ApMultiMatrixCell, ApMultiMatrixCell>)
        (allValues, maxValues) -> {
        if (!maxValues.isFlagActivated()) {
          throw new Exception(
            "Singleton found in eta calculation, but it should have been filtered out before");
        }

        // set the highest value for all row cells..
        double newValue = maxValues.getTmp();
        if (allValues.getColumnId().equals(maxValues.getColumnId())) {
          // .. except of the highest value row element => set the second highest value to it
          newValue = maxValues.getTmp2();
        }
        newValue = newValue * -1;   // and take it's negative value

        allValues.setEta(newValue);
        allValues.setTmp(0d);     // remove beta value from tmp
        return allValues;
      }).returns(new TypeHint<ApMultiMatrixCell>()  { });

    return apMatrix;
  }

  /**
   * AP-Part-2: calculate Theta
   * <p>
   * Computes the second portion of the AffinityPropagation iteration sequence. Calculate the message
   * values for the GAMMA and THETA matrices.
   *
   * @param apMatrix ApMultiMatrix to be calculated
   *
   * @return the input matrix, enriched by the new values for THETA
   */
  public DataSet<ApMultiMatrixCell> apPart2CleanSourcesConstraint(DataSet<ApMultiMatrixCell> apMatrix) {
    /*
    Concept of the Theta-Calculation:
    for each source
        for each column
            for each row of this source and of this column
                calc gamma
                get max value => it is only the max value of the source
            for each row of this source and of this column
                calc theta
     */

    // 2.1 calculate Gamma + filter for rows of clean sources
    DataSet<ApMultiMatrixCell> cleanSourceMatrix = apMatrix.flatMap(
      (FlatMapFunction<ApMultiMatrixCell, ApMultiMatrixCell>) (cell, out) -> {
        if (!"".equals(cell.getRowCleanSource())) {
          double gamma = cell.getA() + cell.getS() + cell.getEta();
          cell.setTmp(gamma);
          out.collect(cell);
        }
      }).returns(new TypeHint<ApMultiMatrixCell>() { });

    /* 2.2 group by cleanSource AND column - for each column:
     * gamma 1st max value => tmp
     * gamma 1st maxID => rowId
     * gamma 2nd max value => tmp2
     */
    DataSet<ApMultiMatrixCell> highestColumnValue =
      cleanSourceMatrix.groupBy(ApMultiMatrixCell.ROW_CLEAN_SOURCE_FIELD, ApMultiMatrixCell.COLUMN_ID_FIELD)
        .reduce(new ApMatrixMaxValueFunction(false))
        /*
         * Filter for processed cells:
         * There can be cleanSource-columnId groups that contain only one row.
         * GroupBy+reduce outputs them unprocessed. These outputs are irrelevant, because there are no two
         * entities of the same clean source in that column, that could end up in the same cluster.
         */
        .filter(new ApMatrixFlagFilterFunction(true));

    // 2.3 calculate Theta
    apMatrix = apMatrix.leftOuterJoin(highestColumnValue, JoinHint.REPARTITION_HASH_SECOND)
      .where(ApMultiMatrixCell.COLUMN_ID_FIELD, ApMultiMatrixCell.ROW_CLEAN_SOURCE_FIELD)
      .equalTo(ApMultiMatrixCell.COLUMN_ID_FIELD, ApMultiMatrixCell.ROW_CLEAN_SOURCE_FIELD)
      .with((JoinFunction<ApMultiMatrixCell, ApMultiMatrixCell, ApMultiMatrixCell>)
        (allValues, maxValues) -> {
        if (maxValues != null) {
          // set the highest value for all cells of this column-rowCleanSource-group..
          double newValue = maxValues.getTmp();
          if (allValues.getRowId().equals(maxValues.getRowId())) {
            // .. except of the highest value column element => set the second highest value to it
            newValue = maxValues.getTmp2();
          }
          newValue = newValue * -1;               // and take it's negative value
          newValue = Math.min(0, newValue);       // if value > 0  => take 0
          allValues.setTheta(newValue);
        }
          // remove gamma value from tmp, also for cells of columns with only one clean source row
          // [flagActivated = false]
        allValues.setTmp(0d);
        return allValues;
      }).returns(new TypeHint<ApMultiMatrixCell>() { });

    return apMatrix;
  }

  /**
   * AP-Part-3: calculate Alpha
   * <p>
   * Computes the third portion of the AffinityPropagation iteration sequence. Calculate the message values
   * for the RHO and ALPHA matrices.
   *
   * @param apMatrix ApMultiMatrix to be calculated
   *
   * @return the input matrix, enriched by the new values for ALPHA
   */
  public DataSet<ApMultiMatrixCell> apPart3ExemplarConsistencyConstraint(
    DataSet<ApMultiMatrixCell> apMatrix) {
    // 3.1 calculate R with damping
    apMatrix = apMatrix.map(new ApMatrixCalculateR(apConfig.getDampingFactor()));

    // 3.2 calculate column sums
    DataSet<ApMultiMatrixCell> columnSums = apMatrix.groupBy(ApMultiMatrixCell.COLUMN_ID_FIELD)
      .reduce((ReduceFunction<ApMultiMatrixCell>) (cell1, cell2) -> {
        double columnSum = 0d;

        if (cell1.getRowId() == null ||                    // rowID == null: element of earlier reduce step ..
          cell1.getRowId().equals(cell1.getColumnId())) {     // .. OR diagonal value ..
          columnSum += cell1.getTmp();                     // .. take the value as it is
        } else {                                         // for non-diagonal elements ..
          columnSum += Math.max(0, cell1.getTmp());        // .. only positive values are summed
        }

        if (cell2.getRowId() == null || cell2.getRowId().equals(cell2.getColumnId())) {
          columnSum += cell2.getTmp();
        } else {
          columnSum += Math.max(0, cell2.getTmp());
        }

        ApMultiMatrixCell result = new ApMultiMatrixCell(null, cell1.getColumnId());
        result.setTmp(columnSum);
        result.setFlagActivated(true);                    // mark column to be passed by this function

        return result;
      }).returns(new TypeHint<ApMultiMatrixCell>() { });

    // 3.3 calculate Alpha
    apMatrix = apMatrix.leftOuterJoin(columnSums, JoinHint.REPARTITION_HASH_SECOND)
      .where(ApMultiMatrixCell.COLUMN_ID_FIELD).equalTo(ApMultiMatrixCell.COLUMN_ID_FIELD)
      .with(new ApMatrixCalculateA(apConfig.getDampingFactor()));

    return apMatrix;
  }

  /**
   * Get the final clustering result. Postprocess the iteration result: assign each dataPoint to an
   * exemplar by the criterion value (A+R) in the final ApMultiMatrix. Unite the iteration result, the
   * singletons and the All-Equal-Components to the final clustering result.
   *
   * @param finalMatrix ApMultiMatrix, containing the final result of the (MSCD-) AP iteration
   * @param singletons {@link ApMultiMatrixCell}s of singletons
   * @param allEqualComponentExemplarApCells {@link ApMultiMatrixCell}s of all-equal-components
   * @param allEqualComponentExemplarAssignments the exemplar assignments for the all-equal-components
   * @param inputGraph the input graph of the AP process, so the clustering result can be joined to it
   *
   * @return the vertices of the input graph with the clustering result written to their
   * {@link PropertyNames#CLUSTER_ID}
   */
  public DataSet<EPGMVertex> getVerticesWithFinalClusterIds(DataSet<ApMultiMatrixCell> finalMatrix,
    DataSet<ApMultiMatrixCell> singletons, DataSet<ApMultiMatrixCell> allEqualComponentExemplarApCells,
    DataSet<Tuple2<GradoopId, GradoopId>> allEqualComponentExemplarAssignments,
    LogicalGraph inputGraph) {
    // exemplars are diagonal cells with A+R > 0
    DataSet<ApMultiMatrixCell> exemplars = finalMatrix.filter(ApMultiMatrixCell::isDiagonalExemplar);

    // combine the exemplars with the singletons and get unique cluster ids
    // NOTE: the groupReduce step can be replaced by a map function if we use the exemplars rowID as clusterID
    DataSet<Tuple2<Long, GradoopId>> exemplarClusterIds = DataSetUtils.zipWithIndex(
      exemplars.union(singletons).union(allEqualComponentExemplarApCells).map(ApMultiMatrixCell::getRowId));

    // for each row => choose the exemplar with the highest A+R
    // => filter columns to contain only exemplar columns
    // => set tmp=A+R for the maxValueFunction to choose the exemplar
    DataSet<ApMultiMatrixCell> exemplarChoosingMatrix =
      finalMatrix.join(exemplars, JoinHint.REPARTITION_HASH_SECOND)
        .where(ApMultiMatrixCell.COLUMN_ID_FIELD).equalTo(ApMultiMatrixCell.COLUMN_ID_FIELD)
        .with(
          (JoinFunction<ApMultiMatrixCell, ApMultiMatrixCell, ApMultiMatrixCell>) (cell, exemplarCell) -> {
            // only column-cells from the exemplars remain in this join
            cell.setTmp(cell.getA() + cell.getR());
            return cell;
          }).returns(new TypeHint<ApMultiMatrixCell>() { });

    // => Assign DataPoints to their exemplars: rowId=DataPoint, colId=Exemplar
    DataSet<ApMultiMatrixCell> exemplarAssignments =
      exemplarChoosingMatrix.groupBy(ApMultiMatrixCell.ROW_ID_FIELD).reduce(new ApMatrixAssignExemplars());

    // map the AP exemplar assignments so they can be united with the assignments of the allEqualComponents
    DataSet<Tuple2<GradoopId, GradoopId>> apExemplarAssignments =
      exemplarAssignments.map((MapFunction<ApMultiMatrixCell, Tuple2<GradoopId, GradoopId>>) cell ->
        Tuple2.of(cell.getRowId(), cell.getColumnId()))
        .returns(new TypeHint<Tuple2<GradoopId, GradoopId>>() { });

    // unite AP and allEqualComp exemplar assignments
    DataSet<Tuple2<GradoopId, GradoopId>> allExemplarAssignments =
      apExemplarAssignments.union(allEqualComponentExemplarAssignments);

    // assign the vertices to the final clusterIds
    DataSet<Tuple2<Long, GradoopId>> finalClusterIdVertexIdAssignments =
      // left outer join => so we don't loose the singletons
      exemplarClusterIds.leftOuterJoin(allExemplarAssignments, JoinHint.REPARTITION_HASH_FIRST)
        .where(1).equalTo(1)
        .with((JoinFunction<Tuple2<Long, GradoopId>, Tuple2<GradoopId, GradoopId>, Tuple2<Long, GradoopId>>)
          (clusterIdTuple, assignmentTuple) -> {
          if (assignmentTuple == null) {
            // singleton cluster
            return clusterIdTuple;
          } else {
            return Tuple2.of(clusterIdTuple.f0, assignmentTuple.f0);
          }
        }).returns(new TypeHint<Tuple2<Long, GradoopId>>() { });

    // join result clusterIds to the input graph vertices and fill the CLUSTER_ID property
    return inputGraph.getVertices().join(finalClusterIdVertexIdAssignments)
      .where(EPGMElement::getId).equalTo(1)
      .with((JoinFunction<EPGMVertex, Tuple2<Long, GradoopId>, EPGMVertex>) (vertex, clusterIdTuple) -> {
        vertex.setProperty(PropertyNames.CLUSTER_ID, String.valueOf(clusterIdTuple.f0));
        return vertex;
      }).returns(new TypeHint<EPGMVertex>() { });
  }

  @Override
  public String getName() {
    return AffinityPropagationSparseDS.class.getName();
  }
}
