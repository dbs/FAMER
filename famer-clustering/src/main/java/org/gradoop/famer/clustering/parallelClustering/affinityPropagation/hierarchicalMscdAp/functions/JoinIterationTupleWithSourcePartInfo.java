/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.api.java.tuple.Tuple8;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Joins an iteration tuple with
 * {@code Tuple8<SourceId,TargetId,SimValue,ClusterId,SrcVertex,TgtVertex,SrcPartition,TgtPartition>}
 * with the according source vertex tuple of {@code Tuple4<VertexId,OrigClusterId,Vertex,Partition>} if
 * SourceId equals VertexId and returns a
 * {@code Tuple6<SourceId,TargetId,SimValue,OrigClusterId,SourceVertex,SourcePartition>}
 */
@FunctionAnnotation.ForwardedFieldsFirst("f0;f1;f2")
@FunctionAnnotation.ForwardedFieldsSecond("f1->f3;f2->f4;f3->f5")
public class JoinIterationTupleWithSourcePartInfo implements JoinFunction<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer,
  Integer>, Tuple4<GradoopId, String, EPGMVertex, Integer>, Tuple6<GradoopId, GradoopId, Double,
  String, EPGMVertex, Integer>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple6<GradoopId, GradoopId, Double, String, EPGMVertex, Integer> reuseTuple = new Tuple6<>();

  @Override
  public Tuple6<GradoopId, GradoopId, Double, String, EPGMVertex, Integer> join(
    Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer> iterationTuple,
    Tuple4<GradoopId, String, EPGMVertex, Integer> vertexTuple) throws Exception {
    reuseTuple.f0 = iterationTuple.f0;
    reuseTuple.f1 = iterationTuple.f1;
    reuseTuple.f2 = iterationTuple.f2;
    reuseTuple.f3 = vertexTuple.f1;
    reuseTuple.f4 = vertexTuple.f2;
    reuseTuple.f5 = vertexTuple.f3;
    return reuseTuple;
  }
}
