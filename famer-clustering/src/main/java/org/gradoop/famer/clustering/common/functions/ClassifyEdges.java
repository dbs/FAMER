/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * For a {@code Tuple3<Edge, SourceVertex, TargetVertex>} creates an new
 * {@code Tuple3<Edge, ClusterId, IsInterEdge>}.
 */
public class ClassifyEdges implements
  FlatMapFunction<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>, Tuple3<EPGMEdge, String, Boolean>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMEdge, String, Boolean> reuseTuple = new Tuple3<>();

  @Override
  public void flatMap(Tuple3<EPGMEdge, EPGMVertex, EPGMVertex> edgeSourceVertexTargetVertex,
    Collector<Tuple3<EPGMEdge, String, Boolean>> out) throws Exception {

    List<String> sourceClusterIds = Arrays.asList(
      edgeSourceVertexTargetVertex.f1.getPropertyValue(PropertyNames.CLUSTER_ID).toString().split(","));
    List<String> targetClusterIds = Arrays.asList(
      edgeSourceVertexTargetVertex.f2.getPropertyValue(PropertyNames.CLUSTER_ID).toString().split(","));

    if ((sourceClusterIds.size() == 1) && (targetClusterIds.size() == 1) &&
      sourceClusterIds.get(0).equals(targetClusterIds.get(0))) {
      // intra edge
      reuseTuple.f0 = edgeSourceVertexTargetVertex.f0;
      reuseTuple.f1 = sourceClusterIds.get(0);
      reuseTuple.f2 = false;
      out.collect(reuseTuple);
      return;
    }
    List<String> union = findUnion(sourceClusterIds, targetClusterIds);
    for (String id : union) {
      if (sourceClusterIds.contains(id) && targetClusterIds.contains(id)) {
        // intra edge
        reuseTuple.f0 = edgeSourceVertexTargetVertex.f0;
        reuseTuple.f1 = id;
        reuseTuple.f2 = false;
        out.collect(reuseTuple);
        if ((sourceClusterIds.size() > 1) || (targetClusterIds.size() > 1)) {
          // inter edge
          reuseTuple.f0 = edgeSourceVertexTargetVertex.f0;
          reuseTuple.f1 = id;
          reuseTuple.f2 = true;
          out.collect(reuseTuple);
        }
      } else if ((sourceClusterIds.contains(id) && !targetClusterIds.contains(id)) ||
        (!sourceClusterIds.contains(id) && targetClusterIds.contains(id))) {
        // inter edge
        reuseTuple.f0 = edgeSourceVertexTargetVertex.f0;
        reuseTuple.f1 = id;
        reuseTuple.f2 = true;
        out.collect(reuseTuple);
      }
    }

  }

  /**
   * Creates the union of both given lists by combining all distinct elements
   *
   * @param first The first list
   * @param second The second list
   *
   * @return List with all distinct elements from both input lists
   */
  private List<String> findUnion(List<String> first, List<String> second) {
    List<String> outList = new ArrayList<>();
    for (String s : first) {
      if (!outList.contains(s)) {
        outList.add(s);
      }
    }
    for (String s : second) {
      if (!outList.contains(s)) {
        outList.add(s);
      }
    }
    return outList;
  }
}
