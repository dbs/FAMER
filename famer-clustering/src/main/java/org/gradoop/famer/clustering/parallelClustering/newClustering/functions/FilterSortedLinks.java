/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.newClustering.functions;

import com.google.common.collect.Lists;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.parallelClustering.newClustering.dataStructures.LinkValue;

import java.util.List;
import java.util.stream.Collectors;

/**
 * GroupReduce function for new clustering algorithm
 * TODO: was implemented without any description or naming - what does it do, is it fully implemented?
 */
public class FilterSortedLinks implements
  GroupReduceFunction<Tuple6<GradoopId, EPGMEdge, GradoopId, String, LinkValue, LinkValue>, EPGMEdge> {

  @Override
  public void reduce(Iterable<Tuple6<GradoopId, EPGMEdge, GradoopId, String, LinkValue, LinkValue>> group,
    Collector<EPGMEdge> out) throws Exception {

    List<Tuple3<EPGMEdge, LinkValue, LinkValue>> edgeLinkValuesTupleList = Lists.newArrayList(group).stream()
      .map(tuple -> Tuple3.of(tuple.f1, tuple.f4, tuple.f5)).collect(Collectors.toList());

    if (edgeLinkValuesTupleList.size() > 1) {
      edgeLinkValuesTupleList.sort((tuple1, tuple2) -> {
        if ((tuple1.f2.isPositive() && tuple2.f2.isPositive()) ||
          (!tuple1.f2.isPositive() && !tuple2.f2.isPositive())) {
          return Integer.compare(tuple1.f1.getRank(), tuple2.f1.getRank());
        }
        if (tuple1.f2.isPositive()) {
          return -1;
        }
        if (tuple2.f2.isPositive()) {
          return 1;
        }
        return 0;
      });
      out.collect(edgeLinkValuesTupleList.get(0).f0);
    }

    /* // old code
    List<Tuple6<GradoopId, Edge, GradoopId, String, LinkValue, LinkValue>>
      linkId_link_endId_otherEndType_value1_value2 = new ArrayList<>();
    for (Tuple6<GradoopId, Edge, GradoopId, String, LinkValue, LinkValue> in : group) {
      linkId_link_endId_otherEndType_value1_value2.add(in);
    }
    if (linkId_link_endId_otherEndType_value1_value2.size() != 1) {
      Collections.sort(linkId_link_endId_otherEndType_value1_value2, new LinkComparator());
      out.collect(linkId_link_endId_otherEndType_value1_value2.get(0).f1);
    }
    */
  }

  /* // old code
  private static class LinkComparator implements
    Comparator<Tuple6<GradoopId, Edge, GradoopId, String, LinkValue, LinkValue>> {
    @Override
    public int compare(Tuple6<GradoopId, Edge, GradoopId, String, LinkValue, LinkValue> o1,
      Tuple6<GradoopId, Edge, GradoopId, String, LinkValue, LinkValue> o2) {

      if ((o1.f5.isPositive() && o2.f5.isPositive()) ||
        (!o1.f5.isPositive() && !o2.f5.isPositive())) {
        return Integer.compare(o1.f4.getRank(), o2.f4.getRank());
      }
      if (o1.f5.isPositive()) {
        return -1;
      }
      if (o2.f5.isPositive()) {
        return 1;
      }
      return 0;
    }
  }
  */
}
