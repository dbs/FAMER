/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures;

import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.ApPreprocessingCompute;

/**
 * Message value for the vertex centric iteration of sparse MSCD-AP's preprocessing in
 * {@link ApPreprocessingCompute}.
 */
public class PreprocessingMessage extends AbstractPreprocessingInfo {

  /**
   * ID of the vertex who send the message.
   */
  private GradoopId sender;

  /**
   * Parameterless constructor for serialization
   */
  public PreprocessingMessage() {
    super();
  }

  /**
   * Constructs PreprocessingMessage
   *
   * @param sender ID of the vertex who send the message.
   */
  public PreprocessingMessage(GradoopId sender) {
    super();
    this.sender = sender;
  }

  public GradoopId getSender() {
    return sender;
  }

  public void setSender(GradoopId sender) {
    this.sender = sender;
  }
}
