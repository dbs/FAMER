/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.graph.Vertex;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.ApVertexValueTuple;

/**
 * Check whether the result of the Gelly Sparse (MSCD-) Affinity Propagation Clustering Algorithm is valid.
 * Throws the respective {@link ConvergenceException} when {@link ApConfig#getMaxApIteration()} is exceeded.
 */
public class CheckApResultIsValid implements
  MapFunction<Vertex<GradoopId, ApVertexValueTuple>, Vertex<GradoopId, ApVertexValueTuple>> {

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering algorithm
   */
  private final ApConfig apConfig;

  /**
   * Constructs CheckApResultVertices
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering algorithm
   */
  public CheckApResultIsValid(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  @Override
  public Vertex<GradoopId, ApVertexValueTuple> map(Vertex<GradoopId, ApVertexValueTuple> vertex)
    throws ConvergenceException {

    if (vertex.getValue().getSumOfOverallIterations() > apConfig.getMaxApIteration()) {
      throw new ConvergenceException("The algorithm did not converge for all components in " +
        apConfig.getMaxApIteration() + " iterations.");
    }

    return vertex;
  }
}
