/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * Create a diagonal ApMultiMatrixCell for each vertex tuple in form of
 * {@code Tuple3<VertexId,ConnCompId,CsInfo>}.
 */
public class CreateDiagonalApMatrixCells implements
  MapFunction<Tuple3<GradoopId, String, String>, ApMultiMatrixCell> {

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  private final ApConfig apConfig;

  /**
   * Constructs CreateDiagonalApMatrixCells
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  public CreateDiagonalApMatrixCells(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  /**
   * Create a diagonal ApMultiMatrixCell for each vertex.
   *
   * @param vertexTuple tuple representing a vertex, with vertexId, connCompId and cleanSourceName
   * @return diagonal {@link ApMultiMatrixCell} for the vertex
   */
  @Override
  public ApMultiMatrixCell map(Tuple3<GradoopId, String, String> vertexTuple) {
    ApMultiMatrixCell apMultiMatrixCell = new ApMultiMatrixCell(vertexTuple.f0, vertexTuple.f0);
    apMultiMatrixCell.setConnectedComponentId(vertexTuple.f1);
    apMultiMatrixCell.setRowCleanSource(vertexTuple.f2);
    apMultiMatrixCell.setCurrentDamping(apConfig.getDampingFactor());
    return apMultiMatrixCell;
  }
}
