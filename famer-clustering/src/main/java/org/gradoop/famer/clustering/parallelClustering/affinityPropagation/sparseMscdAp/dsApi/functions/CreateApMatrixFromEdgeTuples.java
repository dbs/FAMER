/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * Create a symmetric matrix of ApMultiMatrixCells out of the edge tuples in form of
 * {@code Tuple5<SourceId(RowId),TargetId(ColumnId),SimValue,ConnCompId,CsInfo>}. Add information of rowId,
 * columnId, similarity, connectedComponentId and initial damping factor. The output are two multi matrix
 * cells, on for each edge direction. We can't take the clean source info, because when mirroring row and
 * column the rows source can be different to the columns source.
 */
public class CreateApMatrixFromEdgeTuples implements
  FlatMapFunction<Tuple5<GradoopId, GradoopId, Double, String, String>, ApMultiMatrixCell> {

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  private final ApConfig apConfig;

  /**
   * Constructs CreateApMatrixFromEdgeTuples
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  public CreateApMatrixFromEdgeTuples(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  @Override
  public void flatMap(Tuple5<GradoopId, GradoopId, Double, String, String> tuple,
    Collector<ApMultiMatrixCell> out) {
    // v1-v2-sim-conCompId-srcInfo...
    out.collect(new ApMultiMatrixCell(tuple.f0, tuple.f1, tuple.f2, tuple.f3, apConfig.getDampingFactor()));
    // v2-v1-sim-conCompId-srcInfo...
    out.collect(new ApMultiMatrixCell(tuple.f1, tuple.f0, tuple.f2, tuple.f3, apConfig.getDampingFactor()));
  }
}
