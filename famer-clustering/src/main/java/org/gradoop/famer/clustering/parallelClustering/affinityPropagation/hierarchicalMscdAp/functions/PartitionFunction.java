/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import com.google.common.collect.Lists;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;

import java.util.List;

/**
 * GroupReduce that creates partitions of the grouped vertices of a connected component, for the hierarchical
 * divide-and-conquer clustering in {@link HierarchicalAffinityPropagation}.
 */
public class PartitionFunction implements GroupReduceFunction<Tuple3<GradoopId, String, EPGMVertex>,
  Tuple4<GradoopId, String, EPGMVertex, Integer>> {

  /**
   * The maximum size of a resulting partition. The created partitions are of a size as close as possible
   * to the maxPartitionSize.
   */
  private final int maxPartitionSize;

  /**
   * Constructs PartitionFunction with the {@link #maxPartitionSize}.
   *
   * @param maxPartitionSize the maximum size of a resulting partition
   */
  public PartitionFunction(int maxPartitionSize) {
    this.maxPartitionSize = maxPartitionSize;
  }

  /**
   * Performs a round robin partitioning on the group of vertex tuples (grouped by cluster id) if it is
   * larger than {@link #maxPartitionSize}.
   * Generates {@code m = N / {@link #maxPartitionSize}} partitions with PartitionId {@code 1..m} and
   * PartitionId 0 if {@code N < {@link #maxPartitionSize}}.
   */
  @Override
  public void reduce(Iterable<Tuple3<GradoopId, String, EPGMVertex>> group,
  Collector<Tuple4<GradoopId, String, EPGMVertex, Integer>> out) {
    List<Tuple3<GradoopId, String, EPGMVertex>> tuples = Lists.newArrayList(group);

    if (tuples.size() > maxPartitionSize) {
      // round robin partitioning
      int numberOfPartitions = (int) Math.ceil((double) tuples.size() / maxPartitionSize);
      int nextPartition = 1;
      for (Tuple3<GradoopId, String, EPGMVertex> tuple : tuples) {

        out.collect(Tuple4.of(tuple.f0, tuple.f1, tuple.f2, nextPartition));

        nextPartition++;
        if (nextPartition > numberOfPartitions) {
          nextPartition = 1;
        }
      }
    } else {
      // group is small enough - no partitioning
      for (Tuple3<GradoopId, String, EPGMVertex> tuple : tuples) {
        out.collect(Tuple4.of(tuple.f0, tuple.f1, tuple.f2, 0));
      }
    }
  }
}
