/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.commons.lang.ArrayUtils;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.famer.clustering.common.PropertyNames;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

/**
 * Reduce function for groups of {@code Tuple3<Edge, EndId, OtherEndType>} which returns all edges as
 * {@code Tuple3<Edge, EdgeId, Rank>} with the best similarity value or where the similarity value loss is
 * smaller the given {@link #delta} and the Rank representing the ranking of the similarity value starting
 * with 1.
 */
public class FindMaxSimilarityEdges implements
  GroupReduceFunction<Tuple3<EPGMEdge, GradoopId, String>, Tuple3<EPGMEdge, GradoopId, Integer>> {

  /**
   * The allowed similarity value loss
   */
  private final double delta;

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMEdge, GradoopId, Integer> reuseTuple;

  /**
   * Creates an instance of FindMaxSimilarityEdges
   *
   * @param delta The allowed similarity value loss
   */
  public FindMaxSimilarityEdges(double delta) {
    this.delta = delta;
    this.reuseTuple = new Tuple3<>();
  }

  @Override
  public void reduce(Iterable<Tuple3<EPGMEdge, GradoopId, String>> group,
    Collector<Tuple3<EPGMEdge, GradoopId, Integer>> out) throws Exception {
    List<Tuple4<EPGMEdge, GradoopId, String, Double>> edgeList = new ArrayList<>();
    for (Tuple3<EPGMEdge, GradoopId, String> groupItem : group) {
      double edgePropValue = 0.0;
      if (groupItem.f0.hasProperty(PropertyNames.SIM_VALUE)) {
        edgePropValue =
          Double.parseDouble(groupItem.f0.getPropertyValue(PropertyNames.SIM_VALUE).toString());
      }
      edgeList.add(Tuple4.of(groupItem.f0, groupItem.f1, groupItem.f2, edgePropValue));
    }
    Tuple4<EPGMEdge, GradoopId, String, Double>[] edgeArray = edgeList.toArray(new Tuple4[edgeList.size()]);
    Arrays.sort(edgeArray, Comparator.comparing(in -> in.f3));
    ArrayUtils.reverse(edgeArray);

    // collect edge with highest similarity
    reuseTuple.f0 = edgeArray[0].f0;
    reuseTuple.f1 = edgeArray[0].f0.getId();
    reuseTuple.f2 = 1;
    out.collect(reuseTuple);

    // collect edges with similarity loss smaller than configurable delta value
    int i = 1;
    while ((i < edgeArray.length) && (Math.abs(edgeArray[i].f3 - edgeArray[0].f3) <= delta)) {
      reuseTuple.f0 = edgeArray[i].f0;
      reuseTuple.f1 = edgeArray[i].f0.getId();
      reuseTuple.f2 = 1;
      out.collect(reuseTuple);
      i++;
    }

    // collect other edges
    for (; i < edgeArray.length; i++) {
      reuseTuple.f0 = edgeArray[i].f0;
      reuseTuple.f1 = edgeArray[i].f0.getId();
      reuseTuple.f2 = 0;
      out.collect(reuseTuple);
    }
  }
}
