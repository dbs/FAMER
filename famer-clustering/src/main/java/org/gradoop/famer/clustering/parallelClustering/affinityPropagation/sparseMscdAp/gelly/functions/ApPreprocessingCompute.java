/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions;

import org.apache.flink.api.common.aggregators.LongSumAggregator;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.Vertex;
import org.apache.flink.graph.pregel.ComputeFunction;
import org.apache.flink.graph.pregel.MessageIterator;
import org.apache.flink.graph.pregel.VertexCentricConfiguration;
import org.apache.flink.types.LongValue;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.PreprocessingMessage;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures.PreprocessingVertexValue;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;

/**
 * Detects and annotates singletons and all-equal-components in the graph. All-equal components are
 * connected components where each edge has the same similarity value. AP can't converge for them so they
 * must be detected in preprocessing. All-equal-components and singletons are marked as converged, so AP
 * does not initialize message passing for them. All-equal-components are considered as a cluster, if their
 * similarity is larger or equal to {@link ApConfig#getAllSameSimClusteringThreshold()}. Otherwise
 * each vertex of the component is marked as a singleton. Singletons are set as exemplars (cluster
 * centers). In all-equal-component clusters, the vertex with the largest (random) id becomes the exemplar.
 */
public class ApPreprocessingCompute extends
  ComputeFunction<GradoopId, PreprocessingVertexValue, Double, PreprocessingMessage> {

  /**
   * Name of the {@link #tokenChangesAggregator}
   */
  private static final String INFO_PROPAGATION_AGGREGATOR_NAME = "tokenChangesAgg";

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  private final ApConfig apConfig;

  /**
   * The aggregator which is used to check whether information was propagated through all vertices of all
   * connected components. It counts the number of vertices whose information changed by propagated
   * information from neighbouring vertices. When the count is zero, all vertices share the same information.
   */
  private LongSumAggregator tokenChangesAggregator;

  /**
   * Constructs ApPreprocessingCompute
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering algorithm.
   */
  public ApPreprocessingCompute(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  /**
   * Get the parameters that can be used for the construction of the vertex centric iteration.
   *
   * @return parameters of the vertex centric iteration.
   */
  public static VertexCentricConfiguration getParameters() {
    VertexCentricConfiguration parameters = new VertexCentricConfiguration();
    parameters.setName("Gelly AP Preprocessing");
    parameters.registerAggregator(INFO_PROPAGATION_AGGREGATOR_NAME, new LongSumAggregator());
    return parameters;
  }

  /**
   * Before each superStep, retrieve the {@link #tokenChangesAggregator}
   */
  @Override
  public void preSuperstep() {
    tokenChangesAggregator = getIterationAggregator(INFO_PROPAGATION_AGGREGATOR_NAME);
  }

  /**
   * Execute the vertex centric iteration to detect and annotate singletons and all-equal-components in the
   * graph.
   *
   * @param vertex a gelly vertex on whose perspective the vertex centric iteration runs. It exchanges
   *               messages with its neighbours to detect singletons and all-equal-components.
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   */
  @Override
  public void compute(Vertex<GradoopId, PreprocessingVertexValue> vertex,
    MessageIterator<PreprocessingMessage> messages) {
    /*
    --------------------------------------------
    ---------------- singletons ----------------
    --------------------------------------------

    0. if edges_count = 0  (singleton)
           v-store: component_converged, is_exemplar, cluster_id

    --------------------------------------------
    -------------- all same sim ? --------------
    --------------------------------------------

    Propagate the information like in the connected components algorithm. Each vertex tells it information
    to all its neighbours. Each neighbour replies with an accumulation of the received info and its known
    info (from vertex-store). Then each vertex accumulates all received information with its former
    information in the vertex store. When the information of each vertex did not change for an iteration,
    then the information is successfully propagated. Because the message-answers in step 2 don't contain
    the accumulation of ALL received messages, we always need a step2-step3-combination to propagate the info
    one edge further.

    0. initialize message passing

    1. msg-set: allSimEqual         <- true when all edge values of the vertex are equal
       msg-set: compMaxVertexId     <- vertexId
       msg-set: clean-src-vertices  <- Map<"srcName", Set<vertexId>>

    2. accumulate received message values:
       msg-set: allSimEqual         <- true v-store and received true
       msg-set: compMaxVertexId     <- max(v-store-compMaxVertexId, msg-compMaxVertexId)
       msg-set: clean-src-vertices  <- merge v-store and received
       v-store: allSimEqual         <- true when ALL received true
       v-store: compMaxVertexId     <- max(ALL received compMaxVertexId)
       v-store: clean-src-vertices  <- merge ALL received
       agg-set: 1, when equalSimToken changed to v-store

    3. agg-get: 0?
          the information was propagated through all connected components -> stop message passing
          v-store: component_converged, is_exemplar, cluster_id
       agg-get: 1?
          accumulate received message values like in step 2
     */

    PreprocessingVertexValue vertexValue = vertex.getValue();
    int step = vertexValue.getStep();

    switch (step) {
    case 0:
      step0(vertexValue);
      break;
    case 1:
      step1(vertex, messages, vertexValue);
      break;
    case 2:
      step2(vertex, messages, vertexValue);
      break;
    case 3:
      step3(vertex, messages, vertexValue);
      break;
    default:
      throw new IllegalStateException("The vertex is in the non defined step " + step);
    }

    vertexValue.setStep(getNextStep(step));
    setNewVertexValue(vertexValue);
  }

  /**
   * Get the next step of a vertex in the message passing procedure, by the following
   * step-order: 0 --> 1 --> loop(2,3)
   *
   * @param step current step
   * @return next step
   */
  private int getNextStep(int step) {
    int nextStep;
    if (step == 3) {
      nextStep = 2;
    } else {
      nextStep = step + 1;
    }
    return nextStep;
  }

  /**
   * Let the vertex reply to a message by resending it back to its sender
   *
   * @param vertex sender of the reply message
   * @param message the message to be replied to
   */
  private void sendReplyMessage(Vertex<GradoopId, PreprocessingVertexValue> vertex,
    PreprocessingMessage message) {
    GradoopId target = message.getSender();
    message.setSender(vertex.getId());
    sendMessageTo(target, message);
  }

  /**
   * Step 0: initialize message passing and find singletons.
   *
   * @param vertexValue value of the vertex used to store intermediate results and the final preprocessing
   *                    information
   */
  private void step0(PreprocessingVertexValue vertexValue) {
    long edgesCount = 0L;
    for (Edge<GradoopId, Double> edge : getEdges()) {
      edgesCount++;
      PreprocessingMessage message = new PreprocessingMessage(edge.getSource());
      sendMessageTo(edge.getTarget(), message);
    }
    if (edgesCount == 0) {   // singleton
      vertexValue.getApVertexValueTuple().setComponentConverged(true);
      vertexValue.getApVertexValueTuple().setExemplar(true);
      vertexValue.getApVertexValueTuple().setClusterId(vertexValue.getApVertexValueTuple().getVertexId());
    }
  }

  /**
   * Step 1: Check if the similarities to all neighbours are equal. Tell the neighbouring vertices about that.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs.
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store intermediate results and the final preprocessing
   *                    information
   */
  private void step1(Vertex<GradoopId, PreprocessingVertexValue> vertex,
    MessageIterator<PreprocessingMessage> messages, PreprocessingVertexValue vertexValue) {
    double maxSim = Double.NEGATIVE_INFINITY;
    double minSim = Double.POSITIVE_INFINITY;

    for (Edge<GradoopId, Double> edge : getEdges()) {
      maxSim = Math.max(maxSim, edge.getValue());
      minSim = Math.min(minSim, edge.getValue());
    }

    vertexValue.setCumulatedSimilaritiesEqual(maxSim == minSim);
    vertexValue.setComponentMaxVertexId(vertexValue.getApVertexValueTuple().getVertexId());

    if (!vertexValue.getApVertexValueTuple().getCleanSource().equals("")) {
      vertexValue.putCleanSourceVertex(vertexValue.getApVertexValueTuple().getCleanSource(),
        vertexValue.getApVertexValueTuple().getVertexId());
    }

    for (PreprocessingMessage message : messages) {
      message.setComponentMaxVertexId(vertexValue.getApVertexValueTuple().getVertexId());
      message.setCumulatedSimilaritiesEqual(maxSim == minSim);
      message.getCleanSourceVertices().addAllElementsOfOtherMap(vertexValue.getCleanSourceVertices());

      sendReplyMessage(vertex, message);
    }
  }

  /**
   * Step 2: Cumulate the vertex information with the received information of the neighbours. If the vertex
   * received new information, tell the {@link #tokenChangesAggregator} so the iteration is not over jet.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs.
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store intermediate results and the final preprocessing
   *                    information
   */
  private void step2(Vertex<GradoopId, PreprocessingVertexValue> vertex,
    MessageIterator<PreprocessingMessage> messages, PreprocessingVertexValue vertexValue) {

    if (cumulateMessages(vertex, messages, vertexValue)) {
      // aggregate 1 for all vertices that received new information
      tokenChangesAggregator.aggregate(1);
    }
  }

  /**
   * Step 3: Check if all vertices share the same information. If they do, form clusters for
   * all-equal-components and singletons and store the final result in the vertex value. Otherwise continue
   * to propagate and cumulate the all-equal-information.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs.
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store intermediate results and the final preprocessing
   *                    information
   */
  private void step3(Vertex<GradoopId, PreprocessingVertexValue> vertex,
    MessageIterator<PreprocessingMessage> messages, PreprocessingVertexValue vertexValue) {

    LongValue verticesWithChangedInfo = getPreviousIterationAggregate(INFO_PROPAGATION_AGGREGATOR_NAME);

    if (verticesWithChangedInfo.getValue() > 0) {
      cumulateMessages(vertex, messages, vertexValue);
    } else {
      // final all-equal info was propagated
      if (vertexValue.getCumulatedSimilaritiesEqual()) {
        vertexValue.getApVertexValueTuple().setComponentConverged(true);
        if (getEdges().iterator().next().getValue() >= apConfig.getAllSameSimClusteringThreshold()) {
          // form a cluster for all vertices of that component with the largest vertexId as exemplar
          vertexValue.getApVertexValueTuple().setExemplar(
            vertexValue.getComponentMaxVertexId().equals(vertexValue.getApVertexValueTuple().getVertexId()));
          vertexValue.getApVertexValueTuple().setClusterId(vertexValue.getComponentMaxVertexId());

          if (!vertexValue.getApVertexValueTuple().getCleanSource().equals("")) {
            if (!vertexValue.isVertexIdLargestOfSource(vertexValue.getApVertexValueTuple().getVertexId(),
            vertexValue.getApVertexValueTuple().getCleanSource())) {
              // The largest vertexId of a clean source gets into the cluster (by default).
              // Additional vertices of a clean source form singletons.
              vertexValue.getApVertexValueTuple().setExemplar(true);
              vertexValue.getApVertexValueTuple().setClusterId(
                vertexValue.getApVertexValueTuple().getVertexId());
            }
          }
        } else {
          // form single clusters for each vertex of the component
          vertexValue.getApVertexValueTuple().setExemplar(true);
          vertexValue.getApVertexValueTuple().setClusterId(vertexValue.getApVertexValueTuple().getVertexId());
        }
      }
    }
  }

  /**
   * Cumulate the all-equal-info and the max-vertex-id of the vertex with the information received by the
   * neighbours. If the information changed due to new information by the neighbours, return true.
   *
   * @param vertex gelly vertex on whose perspective the vertex centric iteration runs.
   * @param messages all messages, the vertex has received from it's neighbours in this super step
   * @param vertexValue value of the vertex used to store intermediate results (as the cumulated information)
   *                    and the final preprocessing result
   * @return true if something changed
   */
  private boolean cumulateMessages(Vertex<GradoopId, PreprocessingVertexValue> vertex,
    MessageIterator<PreprocessingMessage> messages, PreprocessingVertexValue vertexValue) {
    boolean cumulatedInfoChanged = false;
    boolean cumulatedAllEqual = vertexValue.getCumulatedSimilaritiesEqual();
    long cumulatedMaxVertexId = vertexValue.getComponentMaxVertexId();

    for (PreprocessingMessage message : messages) {
      // cumulate received values for next iteration
      cumulatedMaxVertexId = Math.max(cumulatedMaxVertexId, message.getComponentMaxVertexId());
      cumulatedAllEqual = cumulatedAllEqual && message.getCumulatedSimilaritiesEqual();
      if (vertexValue.getCleanSourceVertices().addAllElementsOfOtherMap(message.getCleanSourceVertices())) {
        cumulatedInfoChanged = true;
      }

      // prepare reply message (from last iteration values)
      message.setCumulatedSimilaritiesEqual(vertexValue.getCumulatedSimilaritiesEqual() &&
        message.getCumulatedSimilaritiesEqual());
      message.setComponentMaxVertexId(Math.max(vertexValue.getComponentMaxVertexId(),
        message.getComponentMaxVertexId()));
      message.getCleanSourceVertices().addAllElementsOfOtherMap(vertexValue.getCleanSourceVertices());

      sendReplyMessage(vertex, message);
    }

    if ((vertexValue.getCumulatedSimilaritiesEqual() != cumulatedAllEqual) ||
      (vertexValue.getComponentMaxVertexId() != cumulatedMaxVertexId)) {
      cumulatedInfoChanged = true;
    }

    vertexValue.setCumulatedSimilaritiesEqual(cumulatedAllEqual);
    vertexValue.setComponentMaxVertexId(cumulatedMaxVertexId);

    return cumulatedInfoChanged;
  }
}
