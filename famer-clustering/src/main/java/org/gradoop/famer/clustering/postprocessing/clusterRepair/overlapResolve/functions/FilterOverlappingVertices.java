/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Collects all vertices assigned to more than one cluster if no {@link #overlappingLength} is set, or
 * collects all vertices assigned to a given number of clusters simultaneously with the number of clusters
 * set in {@link #overlappingLength}.
 */
public class FilterOverlappingVertices implements FlatMapFunction<EPGMVertex, EPGMVertex> {

  /**
   * Number of clusters a vertex has to be assigned to simultaneously to be collected
    */
  private final int overlappingLength;

  /**
   * Creates an instance of FilterOverlappingVertices and sets {@link #overlappingLength} to -1
   */
  public FilterOverlappingVertices() {
    this(-1);
  }

  /**
   * Creates an instance of FilterOverlappingVertices
   *
   * @param overlappingLength Number of clusters a vertex has to be assigned to simultaneously to be collected
   */
  public FilterOverlappingVertices(int overlappingLength) {
    this.overlappingLength = overlappingLength;
  }

  @Override
  public void flatMap(EPGMVertex vertex, Collector<EPGMVertex> out) throws Exception {
    String clusterId = vertex.getPropertyValue(PropertyNames.CLUSTER_ID).toString();
    if (clusterId.contains(",")) {
      String[] clusterIds = clusterId.split(",");
      if ((overlappingLength == -1) || (clusterIds.length == overlappingLength)) {
        out.collect(vertex);
      }
    }
  }
}
