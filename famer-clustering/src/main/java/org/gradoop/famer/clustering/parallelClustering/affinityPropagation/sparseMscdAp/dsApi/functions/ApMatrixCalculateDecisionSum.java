/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.dataStructures.ApMultiMatrixCell;

/**
 * AP is converged if all exemplar decisions did not change in the last
 * {@link ApConfig#getConvergenceIter()} iterations. This function calculates the decisionSum that
 * represents the decisions of the last iterations. So it can be decided, whether AP is converged or not.
 * ExemplarDecisionSum starts with a value of 0. If the entity is an exemplar, a value of 1 is added
 * if the current value is zero or positive. If the value is negative it is reset to 1. If the entity is
 * not an exemplar, a value of -1 is added if the current value is zero or negative.  If the value is
 * positive it is reset to -1.
 * <ul>
 *   <li>e.g. {@link ApConfig#getConvergenceIter} = 15:</li>
 *   <li>exemplarDecisionSum must be 15 or -15 if the decision didn't change over the last 15 iterations.
 *   </li>
 *   <li><b>if exemplarDecisionSum equals 12:</b> The entity was an exemplar for the last 12 iterations.
 *   The algorithm didn't converge by now.</li>
 *   <li><b>if exemplarDecisionSum equals -5:</b> The entity was not an exemplar for the last 5 iterations
 *   </li>
 * </ul>
 */
public class ApMatrixCalculateDecisionSum implements MapFunction<ApMultiMatrixCell, ApMultiMatrixCell> {

  /**
   * Calculate the summed decision over the last iterations, whether a dataPoint is an exemplar or not.
   * Raise the iteration counter of each cell. Calculate the decisionSum of diagonal cells.
   *
   * @param cell AP multiMatrixCell whose exemplar decision is calculated, if it is diagonal.
   * @return input cell with raised iteration counter, enriched by the exemplaDecisionSum
   */
  @Override
  public ApMultiMatrixCell map(ApMultiMatrixCell cell) {
    // raise iteration counters
    cell.setIterCount(cell.getIterCount() + 1);
    cell.setAdaptionIterCount(cell.getAdaptionIterCount() + 1);

    if (cell.isDiagonal()) { // check only the diagonal cells.
      int decisionSum = cell.getExemplarDecisionSum();

      if ((cell.getA() + cell.getR()) > 0d) {
        // if A + R of a diagonal cell is > 0  => this entity is an exemplar
        if (decisionSum > 0) {
          decisionSum++;
        } else {
          decisionSum = 1;    // reset if last iteration was not positive
        }
      } else {
        // entity is not an exemplar
        if (decisionSum < 0) {
          decisionSum--;
        } else {
          decisionSum = -1;   // reset if last iteration was not negative
        }
      }
      cell.setExemplarDecisionSum(decisionSum);
    }

    return cell;
  }
}
