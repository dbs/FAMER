/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.newClustering;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.functions.EdgeToEdgeEndIdOtherEndGraphLabel;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.clustering.parallelClustering.newClustering.functions.FilterSortedLinks;
import org.gradoop.famer.clustering.parallelClustering.newClustering.functions.InitializeLinkValue;
import org.gradoop.famer.clustering.parallelClustering.newClustering.functions.IntegrateLinkValues;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * Parallel new clustering algorithm
 * TODO: was implemented without any description or naming - what does it do, is it fully implemented?
 */
public class NewClustering extends AbstractParallelClustering {
  /**
   * The output type for the clustering result
   */
  private final ClusteringOutputType clusteringOutputType;

  /**
   * Creates an instance of NewClustering
   *
   * @param clusteringOutputType The output type for the clustering result
   */
  public NewClustering(ClusteringOutputType clusteringOutputType) {
    this.clusteringOutputType = clusteringOutputType;
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph inputGraph) {
    DataSet<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>> edgeSourceVertexTargetVertex =
      new EdgeToEdgeSourceVertexTargetVertex(inputGraph).execute();

    DataSet<EPGMEdge> remainingLinks = edgeSourceVertexTargetVertex
      .flatMap(new EdgeToEdgeEndIdOtherEndGraphLabel())
      .groupBy(1, 2).reduceGroup(new InitializeLinkValue())
      .groupBy(0).reduceGroup(new IntegrateLinkValues())
      .groupBy(2, 3).reduceGroup(new FilterSortedLinks());

    LogicalGraph resultGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(), inputGraph.getVertices(), remainingLinks);

    return resultGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  @Override
  public String getName() {
    return NewClustering.class.getName();
  }
}
