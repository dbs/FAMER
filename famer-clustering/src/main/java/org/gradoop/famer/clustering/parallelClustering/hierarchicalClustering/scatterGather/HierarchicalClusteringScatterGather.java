/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.graph.EdgeDirection;
import org.apache.flink.graph.Graph;
import org.apache.flink.graph.Vertex;
import org.apache.flink.graph.spargel.ScatterGatherConfiguration;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.parallelClustering.common.functions.EdgeToGellyEdgeCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.GellyVertexToVertexCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.ModifyLogicalGraphForClustering;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.functions.HierarchicalClusteringGatherFunction;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.functions.HierarchicalClusteringScatterFunction;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.functions.InitializeNearestNeighborAndEdgesProperties;
import org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.scatterGather.functions.VertexToGellyVertexForHierarchicalScatterGather;
import org.gradoop.famer.clustering.serialClustering.SerialHierarchicalClustering;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
  * The Gradoop/Flink implementation of the hierarchical clustering algorithm based on the RNN-Hierarchical
  * Clustering algorithm:
  *
  * 1) initialize each vertex as a cluster
  * 2) determine all reciprocal nearest neighbors (RNNs)
  * 3) merge all RNNs if their similarity is greater than the specified threshold
  * 4) update the cluster similarities according to the chosen linkage method
  * 5) goto 2) and repeat until no more RNNs with similarity > threshold remain
  */
public class HierarchicalClusteringScatterGather extends AbstractParallelClustering {

  /**
   * The linkage type used in this algorithm
   */
  private final SerialHierarchicalClustering.LinkageType linkageType;

  /**
   * The threshold value that determines until which similarity value clusters will be merged
   */
  private final double threshold;

  /**
   * The number of maximum iterations for the used scatter gather iteration
   */
  private final int maxIteration;

  /**
   * The output type for the clustering result
   */
  private final ClusteringOutputType clusteringOutputType;

  /**
   * Which vertex to select as center based on its vertex priority, see {@link PrioritySelection}
   */
  private final PrioritySelection prioritySelection;

  /**
   * Whether edges are bidirectional
   */
  private final boolean isEdgesBiDirected;

  /**
   * Creates a new instance of HierarchicalClusteringScatterGather
   *
   * @param clusteringOutputType the output type for the clustering result
   * @param maxIteration the number of maximum iterations for the used scatter gather iteration
   * @param linkageType the linkage type used in this algorithm
   * @param threshold the threshold value that determines until which similarity value clusters will be merged
   * @param prioritySelection which vertex to select as center based on its vertex priority
   * @param isEdgesBiDirected whether edges are bidirectional
   */
  public HierarchicalClusteringScatterGather(ClusteringOutputType clusteringOutputType, Integer maxIteration,
    SerialHierarchicalClustering.LinkageType linkageType, double threshold,
    PrioritySelection prioritySelection, boolean isEdgesBiDirected) {
    this.prioritySelection = prioritySelection;
    this.clusteringOutputType = clusteringOutputType;
    this.maxIteration = maxIteration;
    this.linkageType = linkageType;
    this.threshold = threshold;
    this.isEdgesBiDirected = isEdgesBiDirected;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public HierarchicalClusteringScatterGather(JSONObject jsonConfig) {
    this.clusteringOutputType = getClusteringOutputTypeFromJsonConfig(jsonConfig);
    this.maxIteration = getMaxIterationFromJsonConfig(jsonConfig);
    this.prioritySelection = getPrioritySelectionFromJsonConfig(jsonConfig);
    this.isEdgesBiDirected = getIsEdgesBiDirectedFromJsonConfig(jsonConfig);
    try {
      this.linkageType =
        SerialHierarchicalClustering.LinkageType.valueOf(jsonConfig.getString("linkageType"));
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Clustering: value for linkage type could not be found or parsed", ex);
    }
    try {
      this.threshold = jsonConfig.getDouble("threshold");
    } catch (Exception ex) {
      throw new UnsupportedOperationException(
        "Clustering: value for threshold could not be found or parsed", ex);
    }
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph graph) {
    // init vertex priorities
    graph = graph.callForGraph(new ModifyLogicalGraphForClustering());

    // configure scatter gather iteration
    ScatterGatherConfiguration scatterGatherConfiguration = new ScatterGatherConfiguration();
    scatterGatherConfiguration.setSolutionSetUnmanagedMemory(true);

    // create gelly graph
    Graph<GradoopId, EPGMVertex, Double> gellyGraph = Graph.fromDataSet(
      graph.getVertices().map(new VertexToGellyVertexForHierarchicalScatterGather()),
      graph.getEdges().flatMap(new EdgeToGellyEdgeCommon(isEdgesBiDirected)),
      graph.getConfig().getExecutionEnvironment());

    // init vertex properties for nearest neighbor and edges
    DataSet<Vertex<GradoopId, EPGMVertex>> initializedVertices =
      gellyGraph.groupReduceOnNeighbors(new InitializeNearestNeighborAndEdgesProperties(threshold,
          prioritySelection, linkageType), EdgeDirection.OUT);

    gellyGraph = Graph.fromDataSet(
      initializedVertices,
      gellyGraph.getEdges(),
      graph.getConfig().getExecutionEnvironment());

    // run scatter gather iteration
    Graph<GradoopId, EPGMVertex, Double> resultGraph = gellyGraph.runScatterGatherIteration(
      new HierarchicalClusteringScatterFunction(),
      new HierarchicalClusteringGatherFunction(linkageType, threshold, prioritySelection),
      maxIteration,
      scatterGatherConfiguration);

    LogicalGraph resultLogicalGraph = graph.getConfig().getLogicalGraphFactory().fromDataSets(
      graph.getGraphHead(),
      resultGraph.getVertices().map(new GellyVertexToVertexCommon()),
      graph.getEdges());

    return resultLogicalGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  @Override
  public String getName() {
    return HierarchicalClusteringScatterGather.class.getName();
  }
}
