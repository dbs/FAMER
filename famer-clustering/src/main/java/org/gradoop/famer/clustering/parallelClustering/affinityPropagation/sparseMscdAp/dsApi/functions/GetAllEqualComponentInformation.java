/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.dsApi.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import java.util.ArrayList;
import java.util.List;

/**
 * Check whether all similarity values of edges in a connected component are equal. Input group are edge
 * tuples of type {@code Tuple5<SourceId(RowId),TargetId(ColumnId),SimValue,ConnCompId,CsInfo>},
 * grouped by connected component id.
 */
public class GetAllEqualComponentInformation implements
  GroupReduceFunction<Tuple5<GradoopId, GradoopId, Double, String, String>,
    Tuple6<GradoopId, GradoopId, Double, String, String, Boolean>> {

  @Override
  public void reduce(Iterable<Tuple5<GradoopId, GradoopId, Double, String, String>> group,
    Collector<Tuple6<GradoopId, GradoopId, Double, String, String, Boolean>> out) {

    Double firstCellSimilarityValue = null;
    boolean allSimilaritiesEqual = true;

    List<Tuple5<GradoopId, GradoopId, Double, String, String>> uncollectedTuples = new ArrayList<>();

    for (Tuple5<GradoopId, GradoopId, Double, String, String> tuple : group) {
      if (!allSimilaritiesEqual) {
        // if there was already difference in the similarities => collect the cells directly
        out.collect(Tuple6.of(tuple.f0, tuple.f1, tuple.f2, tuple.f3, tuple.f4, false));
      } else {
        // check for differences in the similarities
        if (firstCellSimilarityValue == null) {
          firstCellSimilarityValue = tuple.f2;
        } else {
          double difference = Math.abs(firstCellSimilarityValue - tuple.f2);
          if (difference > 0.0001) {
            allSimilaritiesEqual = false;
          }
        }
        uncollectedTuples.add(tuple);
      }
    }

    for (Tuple5<GradoopId, GradoopId, Double, String, String> tuple : uncollectedTuples) {
      out.collect(Tuple6.of(tuple.f0, tuple.f1, tuple.f2, tuple.f3, tuple.f4, allSimilaritiesEqual));
    }
  }
}
