/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.star;

import org.apache.flink.api.common.aggregators.LongSumAggregator;
import org.apache.flink.graph.Graph;
import org.apache.flink.graph.spargel.ScatterGatherConfiguration;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.common.GenerateOutput;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.parallelClustering.common.functions.EdgeToGellyEdgeCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.GellyVertexToVertexCommon;
import org.gradoop.famer.clustering.parallelClustering.common.functions.ModifyLogicalGraphForClustering;
import org.gradoop.famer.clustering.parallelClustering.star.functions.StarGatherFunction;
import org.gradoop.famer.clustering.parallelClustering.star.functions.StarScatterFunction;
import org.gradoop.famer.clustering.parallelClustering.star.functions.VertexToGellyVertexForStar;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import static org.gradoop.famer.clustering.parallelClustering.star.functions.StarScatterFunction.HAS_FINISHED_AGGREGATOR;

/**
 * The Gradoop/Flink implementation of Star algorithm.
 */
public class Star extends AbstractParallelClustering {
  /**
   * Enumeration for the type of Star algorithm
   */
  public enum StarType {
    /**
     * Star type 1
     */
    ONE,
    /**
     * Star type 2
     */
    TWO
  }

  /**
   * Whether edges are bidirectional
   */
  private final boolean isEdgesBiDirected;

  /**
   * The output type for the clustering result
   */
  private final ClusteringOutputType clusteringOutputType;

  /**
   * Which vertex to select as center based on its vertex priority, see {@link PrioritySelection}
   */
  private final PrioritySelection prioritySelection;

  /**
   * The type of Star algorithm
   */
  private final StarType starType;

  /**
   * Number of maximum iterations for the used scatter gather iteration
   */
  private final int maxIteration;

  /**
   * Creates an instance of Star
   *
   * @param prioritySelection Which vertex to select as center based on its vertex priority
   * @param starType The type of star algorithm
   * @param isEdgesBiDirected Whether edges are bidirectional
   * @param clusteringOutputType The output type for the clustering result
   * @param maxIteration Number of maximum iterations for the used scatter gather iteration
   */
  public Star(PrioritySelection prioritySelection, StarType starType, boolean isEdgesBiDirected,
    ClusteringOutputType clusteringOutputType, int maxIteration) {
    this.prioritySelection = prioritySelection;
    this.starType = starType;
    this.isEdgesBiDirected = isEdgesBiDirected;
    this.clusteringOutputType = clusteringOutputType;
    this.maxIteration = maxIteration;
  }

  /**
   * Constructor used for json parsing
   *
   * @param jsonConfig The clustering algorithm json config part
   */
  public Star(JSONObject jsonConfig) {
    this.prioritySelection = getPrioritySelectionFromJsonConfig(jsonConfig);
    try {
      this.starType = StarType.valueOf(jsonConfig.getString("starType"));
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Clustering: value for starType could not be found " +
        "or parsed", ex);
    }
    this.isEdgesBiDirected = getIsEdgesBiDirectedFromJsonConfig(jsonConfig);
    this.clusteringOutputType = getClusteringOutputTypeFromJsonConfig(jsonConfig);
    this.maxIteration = getMaxIterationFromJsonConfig(jsonConfig);
  }

  public boolean isEdgesBiDirected() {
    return isEdgesBiDirected;
  }

  public ClusteringOutputType getClusteringOutputType() {
    return clusteringOutputType;
  }

  public PrioritySelection getPrioritySelection() {
    return prioritySelection;
  }

  public StarType getStarType() {
    return starType;
  }

  public int getMaxIteration() {
    return maxIteration;
  }

  @Override
  protected LogicalGraph runClustering(LogicalGraph inputGraph) {
    inputGraph = inputGraph.callForGraph(new ModifyLogicalGraphForClustering());
    Graph<GradoopId, EPGMVertex, Double> gellyGraph = Graph.fromDataSet(
      inputGraph.getVertices().map(new VertexToGellyVertexForStar()),
      inputGraph.getEdges().flatMap(new EdgeToGellyEdgeCommon(isEdgesBiDirected)),
      inputGraph.getConfig().getExecutionEnvironment());

    ScatterGatherConfiguration parameters = new ScatterGatherConfiguration();
    LongSumAggregator longSumAggregator = new LongSumAggregator();
    parameters.registerAggregator(HAS_FINISHED_AGGREGATOR, longSumAggregator);
    parameters.setSolutionSetUnmanagedMemory(true);

    Graph<GradoopId, EPGMVertex, Double> resultGraph = gellyGraph.runScatterGatherIteration(
      new StarScatterFunction(starType), new StarGatherFunction(prioritySelection, starType), maxIteration,
      parameters);

    LogicalGraph resultLogicalGraph = inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(
      inputGraph.getGraphHead(), resultGraph.getVertices().map(new GellyVertexToVertexCommon()),
      inputGraph.getEdges());

    return resultLogicalGraph.callForGraph(new GenerateOutput(clusteringOutputType));
  }

  @Override
  public String getName() {
    return Star.class.getName();
  }
}





