/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple6;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApType;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.exceptions.ConvergenceException;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms.AbstractMscdAffinityPropagationSeq;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.sequentialAlgorithms.MscdAffinityPropagationSeq;

/**
 * Executes the sequential implementation of (MSCD-) AP in {@link MscdAffinityPropagationSeq} for the
 * grouped data points of a connected component. This class is for the usage in a non-hierarchical
 * structure. It returns all vertices with the complete clustering result.
 */
public class ReduceGroupByAPFunction extends AbstractReduceGroupByApFunction implements
  GroupReduceFunction<Tuple6<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex>, EPGMVertex> {

  /**
   * Constructs ReduceGroupByAPFunction with the clustering config.
   *
   * @param apConfig Configuration of the (MSCD-) Affinity-Propagation clustering algorithm.
   */
  public ReduceGroupByAPFunction(ApConfig apConfig) {
    super(apConfig);
  }

  /**
   * Execute (MSCD-) AP on the grouped data points of a connected component. If AP did not converge, adapt
   * the parameters until a solution was found or until all possible parameter values were tried. Add the
   * clustering result and some clustering statistics to the vertex properties and return the clustered
   * vertices.
   *
   * @param group Tuples of the edges of the similarity-graph, which are joined to the vertices and
   *                 contain connected component information.
   * @param out collects the vertices of the component, enriched by the clustering information.
   *
   * @throws ConvergenceException when all possible parameter values are tried and no converging solution
   * could be found.
   */
  @Override
  public void reduce(Iterable<Tuple6<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex>> group,
    Collector<EPGMVertex> out) throws ConvergenceException {

    transformInput(group);

    int[] labels;
    int iterCount;
    boolean unconvergedCompSingletons = false;

    try {
      AbstractMscdAffinityPropagationSeq ap = runAndAdaptAP();
      labels = ap.getLabels();
      iterCount = ap.getIterCount();
    } catch (ConvergenceException e) {
      if (apConfig.isSingletonsForUnconvergedComponents()) {
        // if the component could not converge, each vertex is labeled with its own ID and becomes a singleton
        labels = vertexNumberMap.values().stream().mapToInt(Integer::intValue).toArray();
        iterCount = apConfig.getMaxAdaptionIteration();
        unconvergedCompSingletons = true;
      } else {
        throw e;
      }
    }

    String origClusterID;
    String newClusterID;

    for (int i = 0; i < labels.length; i++) {
      GradoopId vertexId = vertexNumberMap.inverse().get(i);
      EPGMVertex vertex = vertexIdMap.get(vertexId);
      origClusterID = vertex.getPropertyValue(PropertyNames.CLUSTER_ID).getString();
      newClusterID = origClusterID + "-" + labels[i];
      vertex.setProperty(PropertyNames.CLUSTER_ID, newClusterID);
      // some info about the process
      vertex.setProperty(PropertyNames.NR_ITERATIONS, iterCount);
      vertex.setProperty(PropertyNames.SUM_ITERATIONS, sumIterations);
      vertex.setProperty(PropertyNames.NR_ADAPTIONS, numberOfAdaptions);
      vertex.setProperty(PropertyNames.ORIG_COMPONENT_ID, origClusterID);
      vertex.setProperty(PropertyNames.IS_SINGLETON_FROM_UNCONVERGED_COMPONENT, unconvergedCompSingletons);
      out.collect(vertex);
    }
  }

  /**
   * Define Affinity Propagation of a non-hierarchical structure as the current AP-Type.
   *
   * @return {@link ApType#AFFINITY_PROPAGATION}
   */
  @Override
  protected ApType getApType() {
    return ApType.AFFINITY_PROPAGATION;
  }
}
