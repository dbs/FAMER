/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Maps a vertex to a {@code Tuple3<VertexId,ClusterId,Vertex>}, having the vertex enriched with HAP default
 * property values for not being an exemplar.
 */
public class VertexToVertexClusterTuple implements MapFunction<EPGMVertex, Tuple3<GradoopId, String, EPGMVertex>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple3<GradoopId, String, EPGMVertex> reuseTuple = new Tuple3<>();

  @Override
  public Tuple3<GradoopId, String, EPGMVertex> map(EPGMVertex epgmVertex) throws Exception {
    // initialize all vertices as not being an exemplar
    epgmVertex.setProperty(PropertyNames.IS_CENTER, false);
    epgmVertex.setProperty(PropertyNames.HAP_HIERARCHY_DEPTH, 0);
    epgmVertex.setProperty(PropertyNames.HAP_NOT_ASSIGNABLE, false);
    epgmVertex.setProperty(PropertyNames.IS_HAP_VERTEX, false);

    reuseTuple.f0 = epgmVertex.getId();
    reuseTuple.f1 = epgmVertex.getPropertyValue(PropertyNames.CLUSTER_ID).getString();
    reuseTuple.f2 = epgmVertex;
    return reuseTuple;
  }
}
