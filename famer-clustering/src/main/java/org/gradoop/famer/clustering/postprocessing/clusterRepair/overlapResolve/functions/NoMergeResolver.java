/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.dataStructures.ResolveIteration;
import org.gradoop.famer.clustering.common.dataStructures.Cluster;

import java.util.ArrayList;
import java.util.List;

/**
 * Reduces a group of {@code Tuple3<vertexId, Vertex, Cluster>}, grouped on the vertexId, and returns a
 * number of {@code Tuple3<Vertex, oldClusterId, newClusterId>}, where the oldClusterId is one of the
 * clusterIds from the vertex and the newClusterId is either marked as singleton cluster or is set depending
 * on the engaging clusters for the group.
 */
public class NoMergeResolver implements
  GroupReduceFunction<Tuple3<GradoopId, EPGMVertex, Cluster>, Tuple3<EPGMVertex, String, String>> {

  /**
   * The iteration phase for resolving
   */
  private final ResolveIteration resolveIteration;

  /**
   * Creates an instance for NoMergeResolver
   *
   * @param resolveIteration The iteration phase for resolving
   */
  public NoMergeResolver(ResolveIteration resolveIteration) {
    this.resolveIteration = resolveIteration;
  }

  @Override
  public void reduce(Iterable<Tuple3<GradoopId, EPGMVertex, Cluster>> group,
    Collector<Tuple3<EPGMVertex, String, String>> out) throws Exception {
    List<Cluster> engagingClusters = new ArrayList<>();
    int relatedVerticesSize = 0;
    EPGMVertex vertex = null;
    for (Tuple3<GradoopId, EPGMVertex, Cluster> groupItem : group) {
      List<EPGMVertex> relatedVertices = groupItem.f2.getRelatedVerticesFromIntraEdges(groupItem.f1.getId());
      relatedVerticesSize += relatedVertices.size();
      vertex = groupItem.f1;
      if (!isAllOverlap(relatedVertices)) {
        engagingClusters.add(groupItem.f2);
      }
    }
    String newClusterId = "";
    assert vertex != null;
    long vertexPriority = vertex.getPropertyValue(PropertyNames.VERTEX_PRIORITY).getLong();
    if (relatedVerticesSize == 0) { // state 1: not connected to any vertex
      // singleton
      newClusterId = "s" + vertexPriority;
    } else if (engagingClusters.size() == 0) { // state 2: just connected to overlapped vertices
      // if iteration 1: unchanged
      // if iteration 2: singleton
      if (resolveIteration == ResolveIteration.ITERATION2) {
        newClusterId = "s" + vertexPriority;
      }
    } else if (engagingClusters.size() == 1) { // state 3: just connected to 1 cluster
      // resolve
      newClusterId = engagingClusters.get(0).getClusterId();
    } else { // state 4: connected to more than 1 clusters
      // resolve by computing associationDegree
      double maxDegree = 0.0;
      for (Cluster cluster : engagingClusters) {
        double associationDegree = cluster.getAssociationDegree(vertex.getId());
        if (associationDegree > maxDegree) {
          maxDegree = associationDegree;
          newClusterId = cluster.getClusterId();
        }
      }
    }
    if (!newClusterId.equals("")) {
      String[] clusterIds =
        vertex.getPropertyValue(PropertyNames.CLUSTER_ID).toString().split(",");
      for (String clusterId : clusterIds) {
        out.collect(Tuple3.of(vertex, clusterId, newClusterId));
      }
    }
  }

  /**
   * Checks if all given vertices have overlapping clusters
   *
   * @param relatedVertices The list of vertices to check for
   *
   * @return {@code true} if all vertices have overlapping clusters, {@code false} otherwise
   */
  private boolean isAllOverlap(List<EPGMVertex> relatedVertices) {
    for (EPGMVertex vertex : relatedVertices) {
      if (!vertex.getPropertyValue(PropertyNames.CLUSTER_ID).toString().contains(",")) {
        return false;
      }
    }
    return true;
  }
}
