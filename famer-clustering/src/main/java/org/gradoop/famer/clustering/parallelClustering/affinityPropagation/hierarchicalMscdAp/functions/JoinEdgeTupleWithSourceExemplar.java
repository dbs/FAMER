/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple8;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Joins an edge tuple of
 * {@code Tuple8<SourceId,TargetId,SimValue,ClusterId,SrcVertex,TgtVertex,SrcPartition,TgtPartition>} with
 * the according source exemplar tuple of {@code Tuple4<VertexId,ClusterId,Vertex,Partition>} if SourceId
 * equals VertexId. If a join partner was found, updates the SrcVertex of the edge tuple with the Vertex of
 * the exemplar tuple.
 */
@FunctionAnnotation.ForwardedFieldsFirst("f0;f1;f2;f3;f5;f6;f7")
public class JoinEdgeTupleWithSourceExemplar implements
  JoinFunction<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>,
    Tuple4<GradoopId, String, Integer, EPGMVertex>,
    Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>> {

  @Override
  public Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer> join(
    Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer> edgeTuple,
    Tuple4<GradoopId, String, Integer, EPGMVertex> exemplar) throws Exception {
    if (exemplar != null) {
      edgeTuple.f4 = exemplar.f3;
    }
    return edgeTuple;
  }
}
