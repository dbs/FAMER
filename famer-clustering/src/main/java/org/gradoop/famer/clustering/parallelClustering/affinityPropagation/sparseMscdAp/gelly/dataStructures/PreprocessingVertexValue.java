/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures;

import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.ApComputeStepped;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.ApPreprocessingCompute;

/**
 * POJO for vertex values of the vertex centric iteration of sparse MSCD-AP's preprocessing in
 * {@link ApPreprocessingCompute}.
 */
public class PreprocessingVertexValue extends AbstractPreprocessingInfo {

  /**
   * Vertex value of the vertex centric iteration in {@link ApComputeStepped}. Only this value is kept
   * after preprocessing. The other information is discarded. Preprocessing stores here the clustering
   * information of the immediately converged components (singletons and all-equal-components).
   */
  private ApVertexValueTuple apVertexValueTuple;

  /**
   * The current superStep of the vertex centric iteration in {@link ApPreprocessingCompute}.
   */
  private Integer step;

  /**
   * Parameterless constructor for serialization
   */
  public PreprocessingVertexValue() {
    super();
  }

  /**
   * Constructs PreprocessingVertexValue
   *
   * @param apVertexValueTuple stores the clustering information of the immediately converged components
   *                           (singletons and all-equal-components).
   */
  public PreprocessingVertexValue(ApVertexValueTuple apVertexValueTuple) {
    super();
    this.apVertexValueTuple = apVertexValueTuple;
    this.step = 0;
  }

  public ApVertexValueTuple getApVertexValueTuple() {
    return apVertexValueTuple;
  }

  public void setApVertexValueTuple(ApVertexValueTuple apVertexValueTuple) {
    this.apVertexValueTuple = apVertexValueTuple;
  }

  public Integer getStep() {
    return step;
  }

  public void setStep(Integer step) {
    this.step = step;
  }
}
