/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.hierarchicalClustering.serialClusteringOnConnectedComponents.functions;

import org.apache.flink.api.java.functions.KeySelector;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.graph.Edge;
import org.apache.flink.graph.Vertex;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Selects the cluster id of the source vertex for a given tuple of a gelly edge and its source vertex
 */
public class EdgeSourceClusterIdSelector implements
  KeySelector<Tuple2<Edge<GradoopId, Double>, Vertex<GradoopId, EPGMVertex>>, String> {

  @Override
  public String getKey(Tuple2<Edge<GradoopId, Double>, Vertex<GradoopId, EPGMVertex>> tuple) throws
    Exception {
    return tuple.f1.f1.getPropertyValue(PropertyNames.CLUSTER_ID).toString();
  }
}
