/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.FlatJoinFunction;
import org.apache.flink.api.java.functions.FunctionAnnotation;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple8;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Joins a vertex tuple for partitioned local exemplars with
 * {@code Tuple4<VertexId,OrigClusterId,Vertex,Partition>} with the according edge tuple for edges of
 * dataPoints from the same partition with
 * {@code Tuple8<SourceId,TargetId,SimValue,ClusterId,Source,Target,SourcePart,TargetPart>}. This join is
 * used to find local exemplars which are not represented in the edges set. Only exemplar tuples are
 * collected, when no join partner was found.
 */
@FunctionAnnotation.ForwardedFieldsFirst("f0;f1;f2;f3")
public class JoinLocalExemplarTupleWithEdgeTuples implements
  FlatJoinFunction<Tuple4<GradoopId, String, EPGMVertex, Integer>,
    Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>,
    Tuple4<GradoopId, String, EPGMVertex, Integer>> {

  @Override
  public void join(Tuple4<GradoopId, String, EPGMVertex, Integer> localExemplarTuple,
    Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer> edgeTuple,
    Collector<Tuple4<GradoopId, String, EPGMVertex, Integer>> out) throws Exception {

    if (edgeTuple == null) {
      out.collect(localExemplarTuple);
    }
  }
}
