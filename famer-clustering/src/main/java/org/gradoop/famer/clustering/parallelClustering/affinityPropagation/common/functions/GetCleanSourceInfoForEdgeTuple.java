/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.utils.CleanSourceInformationUtils;

/**
 * Maps the source and target vertex of a {@code Tuple3(Edge,SourceVertex,TargetVertex)} to their clean
 * source information and returns it as {@code Tuple3(Edge,SourceCsInfo,targetCsInfo)}
 */
public class GetCleanSourceInfoForEdgeTuple implements
  MapFunction<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>, Tuple3<EPGMEdge, String, String>> {

  /**
   * The configuration of the Affinity Propagation clustering process. It contains settings that are
   * necessary to retrieve the clean source information.
   */
  private final ApConfig apConfig;

  /**
   * Reduce object instantiation
   */
  private final Tuple3<EPGMEdge, String, String> reuseTuple;

  /**
   * Creates an instance of GetCleanSourceInfoForEdgeTuple
   *
   * @param apConfig The configuration of the Affinity Propagation clustering process
   */
  public GetCleanSourceInfoForEdgeTuple(ApConfig apConfig) {
    this.apConfig = apConfig;
    this.reuseTuple = new Tuple3<>();
  }

  @Override
  public Tuple3<EPGMEdge, String, String> map(Tuple3<EPGMEdge, EPGMVertex, EPGMVertex> edgeTuple)
    throws Exception {
    reuseTuple.f0 = edgeTuple.f0;
    reuseTuple.f1 = CleanSourceInformationUtils.getVertexCleanSourceInformation(edgeTuple.f1, apConfig);
    reuseTuple.f2 = CleanSourceInformationUtils.getVertexCleanSourceInformation(edgeTuple.f2, apConfig);
    return reuseTuple;
  }
}
