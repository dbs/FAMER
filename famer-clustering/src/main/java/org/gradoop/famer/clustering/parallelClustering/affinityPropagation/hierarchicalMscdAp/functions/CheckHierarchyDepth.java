/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.java.tuple.Tuple8;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.exceptions.HierarchyLevelExceededException;
import org.gradoop.famer.clustering.common.PropertyNames;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;

/**
 * Map function that checks the {@link PropertyNames#HAP_HIERARCHY_DEPTH} property of the source vertex of an
 * edge tuple for the hierarchical divide-and-conquer clustering in {@link HierarchicalAffinityPropagation}.
 * Checks if the {@link #maxHierarchyDepth} is exceeded. Throws a {@link HierarchyLevelExceededException}
 * which terminates the clustering procedure.
 */
public class CheckHierarchyDepth implements
  MapFunction<Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>,
    Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer>> {

  /**
   * The maximal allowed depth of the hierarchical clustering in {@link HierarchicalAffinityPropagation}.
   */
  private final int maxHierarchyDepth;

  /**
   * Constructs CheckHierarchyDepth with the desired {@link #maxHierarchyDepth}
   *
   * @param maxHierarchyDepth The maximal allowed depth of the hierarchical clustering.
   */
  public CheckHierarchyDepth(int maxHierarchyDepth) {
    this.maxHierarchyDepth = maxHierarchyDepth;
  }

  @Override
  public Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer> map(
    Tuple8<GradoopId, GradoopId, Double, String, EPGMVertex, EPGMVertex, Integer, Integer> edgeTuple)
    throws HierarchyLevelExceededException {

    int currentHierarchyLevel = edgeTuple.f4.getPropertyValue(PropertyNames.HAP_HIERARCHY_DEPTH).getInt();

    if (currentHierarchyLevel > maxHierarchyDepth) {
      throw new HierarchyLevelExceededException(
        "The maximum hierarchy level of " + maxHierarchyDepth + " was exceeded." +
          " The Hierarchical Affinity Propagation Algorithm can not succeed with the current settings.");
    }

    return edgeTuple;
  }
}
