/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering;

import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialEdgeComponent;
import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialVertexComponent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Serial implementation of the hierarchical clustering algorithm. Basic approach:
 * - initially every vertex represents one cluster
 * - find and merge the clusters with the highest similarity
 * - update the similarity values according to the specified linkage method
 * - proceed until the similarity of two clusters reach a specified threshold or only one cluster is left
 */
public class SerialHierarchicalClustering extends AbstractSerialClustering {

  /**
   * Enumeration for similarity measures of two clusters
   */
  public enum LinkageType {
    /**
     * The similarity of two clusters equals the degree of the connecting edge with the highest
     * similarity value
     */
    SINGLE_LINKAGE,
    /**
     * The similarity of two clusters equals the degree of the connecting edge with the lowest
     * similarity value, missing links are treated as links with similarity = 0
     */
    COMPLETE_LINKAGE,
    /**
     * The similarity of two clusters equals the average of all degrees of all connecting edges, i.e. the
     * sum of all edge values divided by size(cluster1) * size(cluster2)
     */
    AVERAGE_LINKAGE
  }

  /**
   * Queue of edge components used in this algorithm,
   * entries are ordered by prioritization via {@link #compareEdges(SerialEdgeComponent, SerialEdgeComponent)}
   */
  private PriorityQueue<SerialEdgeComponent> serialEdgeComponentList;

  /**
   * Queue of edge components that represents the links between the clusters, gets updated during execution,
   * entries are ordered by prioritization via {@link #compareEdges(SerialEdgeComponent, SerialEdgeComponent)}
   */
  private PriorityQueue<SerialEdgeComponent> clusterDistances;

  /**
   * HashMap that maps the cluster id to a set of all vertex components that belong to this cluster
   */
  private final HashMap<String, Set<SerialVertexComponent>> clusterMembership;

  /**
   * Threshold that determines until which similarity clusters get merged
   */
  private double threshold;

  /**
   * The linkage type for this algorithm
   */
  private final LinkageType type;

  /**
   * Whether edges are bidirectional
   */
  private final boolean isEdgesBiDirected;

  /**
   * Creates an instance of AbstractSerialClustering
   *
   * @param verticesInputPath File path to read the vertices from
   * @param edgesInputPath    File path to read the edges from
   * @param outputPath        File path for writing the clustering results
   * @param type              Linkage Type of the hierarchical clustering algorithm (single, complete or
   *                          average)
   * @param threshold         threshold that defines until which similarity clusters get merged
   * @param isEdgesBiDirected whether edges are bidirectional
   */
  public SerialHierarchicalClustering(String verticesInputPath, String edgesInputPath, String outputPath,
    LinkageType type, double threshold, boolean isEdgesBiDirected) {
    super(verticesInputPath, edgesInputPath, outputPath);
    this.type = type;
    this.serialVertexComponentMap = new HashMap<>();
    this.serialEdgeComponentList = new PriorityQueue<>(this::compareEdges);
    this.clusterDistances = new PriorityQueue<>(this::compareEdges);
    this.clusterMembership = new HashMap<>();
    this.threshold = threshold;
    this.isEdgesBiDirected = isEdgesBiDirected;
  }

  @Override
  protected void initializeSerialGraphComponents() throws IOException {
    getLinesFromFilePath(verticesInputPath).forEach(line -> {
      String[] lineData = line.split(",");
      SerialVertexComponent svc =
        new SerialVertexComponent(Long.parseLong(lineData[1]), lineData[0], false);
      svc.setId(lineData[0]);
      serialVertexComponentMap.put(lineData[0], svc);
      Set<SerialVertexComponent> clusterVertices = new HashSet<>();
      clusterVertices.add(svc);
      clusterMembership.put(lineData[0], clusterVertices);
    });

    getLinesFromFilePath(edgesInputPath).forEach(line -> {
      String[] lineData = line.split(",");
      SerialEdgeComponent sec =
        new SerialEdgeComponent(lineData[0], lineData[1], Double.parseDouble(lineData[2]));
      serialEdgeComponentList.add(sec);
      clusterDistances.add(sec);
    });
  }

  @Override
  public void doSerialClustering() {
    /* get the maximum similarity between two clusters
       (depends on the chosen linkage type but does not differ before the first iteration) */
    if (!clusterDistances.isEmpty()) {
      double linkage = clusterDistances.peek().getDegree();
      while (linkage > threshold) {
        // get the maximum degree edge (head of queue), remove it from queue
        SerialEdgeComponent secMax = clusterDistances.poll();
        // get source and target vertices from the maximum degree edge
        assert secMax != null;
        SerialVertexComponent source = serialVertexComponentMap.get(secMax.getSourceId());
        SerialVertexComponent target = serialVertexComponentMap.get(secMax.getTargetId());
        // merge the clusters and update the edge list that stores the similarities
        if (!source.getStringClusterId().equals(target.getStringClusterId())) {
          mergeClusters(source.getStringClusterId(), target.getStringClusterId());
          if (this.type == LinkageType.COMPLETE_LINKAGE) {
            updateCompleteLinkage(target.getStringClusterId());
          } else if (this.type == LinkageType.AVERAGE_LINKAGE) {
            updateAverageLinkage(target.getStringClusterId());
          }
        }
        // proceed with the next queue entry
        if (!clusterDistances.isEmpty()) {
          linkage = clusterDistances.peek().getDegree();
        } else {
          break;
        }
      }
    }
  }

  /**
   * Merges two clusters, i. e. sets all the vertices' clusterIds with clusterId1 to clusterId2
   *
   * @param clusterId1 clusterId of the source vertex of the currently processed edge
   * @param clusterId2 clusterId of the target vertex of the currently processed edge
   */
  private void mergeClusters(String clusterId1, String clusterId2) {
    for (SerialVertexComponent svc : serialVertexComponentMap.values()) {
      if (svc.getStringClusterId().equals(clusterId1)) {
        svc.setStringClusterId(clusterId2);
        clusterMembership.get(clusterId2).add(svc);
      }
    }
    for (SerialEdgeComponent sec : serialEdgeComponentList) {
      if (sec.getSourceId().equals(clusterId1)) {
        sec.setSourceId(clusterId2);
      }
      if (sec.getTargetId().equals(clusterId1)) {
        sec.setTargetId(clusterId2);
      }
    }
    for (SerialEdgeComponent sec : clusterDistances) {
      if (sec.getSourceId().equals(clusterId1)) {
        sec.setSourceId(clusterId2);
      }
      if (sec.getTargetId().equals(clusterId1)) {
        sec.setTargetId(clusterId2);
      }
    }
    clusterMembership.remove(clusterId1);
  }

  /**
   * Updates the list of edges so that per pair of clusters the edge with the minimum similarity is kept
   *
   * @param newClusterId id of the newly formed cluster
   */
  private void updateCompleteLinkage(String newClusterId) {
    ArrayList<SerialEdgeComponent> minimumEdges = new ArrayList<>();
    for (Map.Entry<String, Set<SerialVertexComponent>> cluster : clusterMembership.entrySet()) {
      String clusterId = cluster.getKey();
      removeFromQueue(clusterId, newClusterId);
      // for each other cluster
      if (!clusterId.equals(newClusterId)) {
        List<SerialEdgeComponent> secThisCluster = new ArrayList<>();
        SerialEdgeComponent minSimSec = new SerialEdgeComponent(clusterId, newClusterId, 1d);
        // find edges that connect the newly merged cluster
        for (SerialEdgeComponent sec : serialEdgeComponentList) {
          if (connectsCluster(sec, clusterId, newClusterId)) {
            secThisCluster.add(sec);
            if (compareEdges(sec, minSimSec) > 0) {
              minSimSec = sec;
            }
          }
        }

        int clusterSize = secThisCluster.size();
        int allConnections =
          clusterMembership.get(clusterId).size() * clusterMembership.get(newClusterId).size();
        if (isEdgesBiDirected) {
          clusterSize /= 2;
          allConnections /= 2;
        }
        // if there are unconnected vertices the minimum similarity will be 0.0
        if (clusterSize == allConnections) {
          minimumEdges.add(minSimSec);
        }
      }
    }
    clusterDistances.addAll(minimumEdges);
  }

  /**
   * Updates the list of edges so that per pair of clusters one edge that stores the average similarity
   * between them is kept
   *
   * @param newClusterId id of the newly merged cluster
   */
  private void updateAverageLinkage(String newClusterId) {
    ArrayList<SerialEdgeComponent> averageEdges = new ArrayList<>();
    for (Map.Entry<String, Set<SerialVertexComponent>> cluster : clusterMembership.entrySet()) {
      String clusterId = cluster.getKey();
      removeFromQueue(clusterId, newClusterId);
      int count = clusterMembership.get(newClusterId).size() * clusterMembership.get(clusterId).size();
      int countEdges = 0;
      if (!clusterId.equals(newClusterId)) {
        double sum = 0;
        // find edges that are connected to the newly merged cluster
        for (SerialEdgeComponent sec : serialEdgeComponentList) {
          if (connectsCluster(sec, clusterId, newClusterId)) {
            sum += sec.getDegree();
            countEdges++;
          }
        }
        /* if more than one connecting edge between the newly formed and another cluster was found,
           remove all the connecting edges and replace them with one edge that contains their average
           similarity */
        if (isEdgesBiDirected) {
          countEdges /= 2;
          count /= 2;
        }
        if (countEdges > 0) {
          double avg = sum / count;
          SerialEdgeComponent secAvg = new SerialEdgeComponent(newClusterId, clusterId, avg);
          averageEdges.add(secAvg);
        }
      }
    }
    clusterDistances.addAll(averageEdges);
  }

  /**
   * Removes all edges between cluster1 and cluster2 from the queue of cluster distances, so they can be
   * replaced later by an aggregated cluster similarity
   *
   * @param clusterId1 cluster Id of the first cluster
   * @param clusterId2 cluster Id of the second cluster
   */
  private void removeFromQueue(String clusterId1, String clusterId2) {
    clusterDistances.removeIf(sec -> sec.getSourceId().equals(clusterId1) &&
      sec.getTargetId().equals(clusterId2));
    clusterDistances.removeIf(sec -> sec.getSourceId().equals(clusterId2) &&
      sec.getTargetId().equals(clusterId1));
  }

  @Override
  protected void writeClusteringResultsToFile() throws IOException {
    List<String> clusteringResults = serialVertexComponentMap.entrySet().stream()
      .map(entry -> entry.getKey() + "," + entry.getValue().getStringClusterId())
      .collect(Collectors.toList());

    writeLinesToFilePath(outputPath, clusteringResults);
  }

  /**
   * Checks if a SerialEdgeComponent connects two given clusters
   *
   * @param sec the SerialEdgeComponents
   * @param clusterId1 the cluster Id of the first cluster
   * @param clusterId2 the cluster Id of the second cluster
   * @return true if the SerialEdgeComponent connects both clusters, false if not
   */
  private boolean connectsCluster(SerialEdgeComponent sec, String clusterId1, String clusterId2) {
    boolean result = false;
    SerialVertexComponent source = serialVertexComponentMap.get(sec.getSourceId());
    SerialVertexComponent target = serialVertexComponentMap.get(sec.getTargetId());
    // check if the current edge connects the newly merged and the current cluster
    if ((source.getStringClusterId().equals(clusterId1) &&
      target.getStringClusterId().equals(clusterId2)) ||
      (source.getStringClusterId().equals(clusterId2) &&
        target.getStringClusterId().equals(clusterId1))) {
      result = true;
    }
    return result;
  }

  /**
   * Method to compare two SerialEdgeComponent objects, used for prioritization in queues
   *
   * @param t0 the first SerialEdgeComponent to compare
   * @param t1 the second SerialEdgeComponent to compare
   * @return 1 if the second sec is greater, -1 if the first sec is greater, 0 if both are equal
   */
  private int compareEdges(SerialEdgeComponent t0, SerialEdgeComponent t1) {
    if (t0.getDegree() < t1.getDegree()) {
      return 1;
    }
    if (t0.getDegree() > t1.getDegree()) {
      return -1;
    }
    if ((t0.getDegree() == t1.getDegree()) && (t0.getSourceId().compareTo(t1.getSourceId()) < 0)) {
      return -1;
    }
    if ((t0.getDegree() == t1.getDegree()) && (t0.getSourceId().compareTo(t1.getSourceId()) > 0)) {
      return 1;
    }
    if ((t0.getDegree() == t1.getDegree()) && (t0.getSourceId().compareTo(t1.getSourceId()) == 0) &&
      (t0.getTargetId().compareTo(t1.getTargetId()) < 0)) {
      return -1;
    }
    if ((t0.getDegree() == t1.getDegree()) && (t0.getSourceId().compareTo(t1.getSourceId()) == 0) &&
      (t0.getTargetId().compareTo(t1.getTargetId()) > 0)) {
      return 1;
    }
    return 0;
  }

  public PriorityQueue<SerialEdgeComponent> getSerialEdgeComponentList() {
    return serialEdgeComponentList;
  }

  public void setSerialEdgeComponentList(PriorityQueue<SerialEdgeComponent> serialEdgeComponentList) {
    this.serialEdgeComponentList = serialEdgeComponentList;
  }

  public double getThreshold() {
    return threshold;
  }

  public void setThreshold(double threshold) {
    this.threshold = threshold;
  }

  public LinkageType getType() {
    return type;
  }

  public void setVertexComponentMap(HashMap<String, SerialVertexComponent> map) {
    this.serialVertexComponentMap = map;
  }

  public HashMap<String, SerialVertexComponent> getVertexComponentMap() {
    return this.serialVertexComponentMap;
  }

  public PriorityQueue<SerialEdgeComponent> getClusterDistances() {
    return clusterDistances;
  }

  public void setClusterDistances(PriorityQueue<SerialEdgeComponent> clusterDistances) {
    this.clusterDistances = clusterDistances;
  }

  public HashMap<String, Set<SerialVertexComponent>> getClusterMembership() {
    return clusterMembership;
  }
}
