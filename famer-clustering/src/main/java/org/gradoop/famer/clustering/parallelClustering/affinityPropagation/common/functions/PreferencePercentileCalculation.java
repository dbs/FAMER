/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.functions;

import org.apache.commons.math3.stat.descriptive.rank.Percentile;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.common.dataStructures.ApConfig;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Calculates the percentiles for dirty source data points, clean source data points and the minimum value
 * out of the similarities of the input edge tuples (grouped by their connected component id) in form of
 * {@code Tuple5<SourceId(RowId),TargetId(ColumnId),SimValue,ConnCompId,CsInfo>}. The output will be one
 * tuple for the whole group with the desired preference information as
 * {@code Tuple4<ConnCompId,PercentileValueDS,PercentileValueCS,MinSimilarity>}.
 */
public class PreferencePercentileCalculation implements
  GroupReduceFunction<Tuple5<GradoopId, GradoopId, Double, String, String>,
    Tuple4<String, Double, Double, Double>> {

  /**
   * Configuration of the (MSCD-) Affinity Propagation clustering process.
   */
  private final ApConfig apConfig;

  /**
   * Constructs PreferencePercentileCalculation.
   *
   * @param apConfig Configuration of the (MSCD-) Affinity Propagation clustering process.
   */
  public PreferencePercentileCalculation(ApConfig apConfig) {
    this.apConfig = apConfig;
  }

  @Override
  public void reduce(Iterable<Tuple5<GradoopId, GradoopId, Double, String, String>> group,
    Collector<Tuple4<String, Double, Double, Double>> out) {
    String connCompId = null;
    List<Double> similarities = new ArrayList<>();

    for (Tuple5<GradoopId, GradoopId, Double, String, String> tuple : group) {
      similarities.add(tuple.f2);
      if (connCompId == null) {
        connCompId = tuple.f3;
      }
    }
    Collections.sort(similarities);

    double[] simArray = new double[similarities.size()];
    for (int i = 0; i < similarities.size(); i++) {
      simArray[i] = similarities.get(i);
    }

    double minValue = Arrays.stream(simArray).min().orElse(0d);
    double percentileValueDirtySource = 0d;
    double percentileValueCleanSource = 0d;

    if (apConfig.getPreferenceConfig().getPreferencePercentileDirtySrc() > 0) {
      percentileValueDirtySource = new Percentile().evaluate(simArray,
        apConfig.getPreferenceConfig().getPreferencePercentileDirtySrc());
    }
    if (apConfig.getPreferenceConfig().getPreferencePercentileCleanSrc() > 0) {
      percentileValueCleanSource = new Percentile().evaluate(simArray,
        apConfig.getPreferenceConfig().getPreferencePercentileCleanSrc());
    }

    out.collect(Tuple4.of(connCompId, percentileValueDirtySource, percentileValueCleanSource, minValue));
  }
}
