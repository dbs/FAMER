/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.dataStructures;

import org.apache.flink.api.java.tuple.Tuple25;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.functions.ApComputeStepped;

import java.util.HashMap;
import java.util.Map;

/**
 * Vertex value for the vertex centric iteration of sparse MSCD-AP in {@link ApComputeStepped}. It
 * contains information of the vertex and the current processing state of AP, intermediate results for
 * several calculations and the final clustering solution.
 * Extends Flinks tuple class to achieve better performance than using a POJO.
 */
public class ApVertexValueTuple extends Tuple25<Boolean, String, String, Long, Long, Boolean, Integer,
  Integer, Double, Double, GradoopId, Double, Double, Boolean, Boolean, Double, Double,
  Double, Double, Double, Double, Integer, Integer, Integer, Map<String, GammaMaxTuple>> {

  /**
   * Parameterless constructor for serialization
   */
  public ApVertexValueTuple() {
    super();
  }

  /**
   * Constructs ApVertexValueTuple
   *
   * @param cleanSource name of the vertex's clean source. Empty String for dirty sources.
   * @param componentId ID of the vertex's connected component
   * @param vertexId unique long ID of the vertex which is used by other vertices to select this one as
   *                 their exemplar
   * @param damping initial damping factor
   * @param preferenceDirtySource initial preference of dirty source data points
   * @param preferenceCleanSource initial preference of clean source data points
   */
  public ApVertexValueTuple(String cleanSource, String componentId, Long vertexId,
    double damping, double preferenceDirtySource, double preferenceCleanSource) {
    super(
      false,    // isComponentConverged
      cleanSource,
      componentId,
      vertexId,
      vertexId, // clusterId <-- initialized with vertexId,
      false, // isExemplar,
      0, // exemplarDecisionSum
      0,  // step
      Double.NEGATIVE_INFINITY,   // betaMax
      Double.NEGATIVE_INFINITY, // betaSecondMax
      GradoopId.NULL_VALUE, // betaMaxId
      0d, // rhoSumNonDiag
      0d, // rhoDiag
      false, // cumulatedConstraintsInfo
      false, // cumulatedConvergenceInfo
      preferenceDirtySource, // currentPreferenceDirtySrc
      preferenceCleanSource,  // currentPreferenceCleanSrc
      0d, // preferenceNoise
      damping,  // currentDamping
      preferenceDirtySource,  // initialPreferenceDirtySource
      preferenceCleanSource, // initialPreferenceCleanSource
      0,  // number of iterations
      0,  // number of adaptions
      0,  // sum of overall iterations
      new HashMap<>() // srcGammaMaxValues
//      new HashMap<>() // srcCountVerticesInCluster
    );
  }

  /**
   * Reset all calculated from the AP iterations message values.
   * This method is used, when AP's parameters are adapted and AP message-passing starts new with new
   * parameter values.
   * Keep similarity and sender, so the message can be recycled to avoid object instantiation.
   */
  public void resetVertexValue() {
    setComponentConverged(false);
    // keep cleanSource
    // keep componentId
    // keep vertexId
    setClusterId(getVertexId());   // clusterId <-- initialized with vertexId,
    setExemplar(false);
    setExemplarDecisionSum(0);
    setStep(0);
    setBetaMax(Double.NEGATIVE_INFINITY);
    setBetaSecondMax(Double.NEGATIVE_INFINITY);
    setBetaMaxId(GradoopId.NULL_VALUE);
    setRhoSumNonDiag(0d);
    setRhoDiag(0d);
    setCumulatedConstraintsInfo(false);
    setCumulatedConvergenceInfo(false);
    // keep currentPreferenceDirtySrc
    // keep currentPreferenceCleanSrc
    // keep preferenceNoise
    // keep currentDamping
    // keep initialPreferenceDirtySource
    // keep initialPreferenceCleanSource
    setNumberOfIterations(0);
    // keep numberOfAdaptions
    // keep sumOfOverallIterations
    resetSrcGMaxValues();
  }

  /**
   * Update the exemplarDecisionSum by the current exemplar decision.
   * If the vertex is an exemplar raise a positive decisionSum and reset negative decisionSum to 1.
   * If the vertex is not an exemplar lower a negative decision sum and reset a positive one to 0.
   */
  public void updateExemplarDecisionSum() {
    if (this.isExemplar()) {
      if (getExemplarDecisionSum() > 0) {
        setExemplarDecisionSum(getExemplarDecisionSum() + 1);
      } else {
        setExemplarDecisionSum(1);
      }
    } else {
      if (getExemplarDecisionSum() < 0) {
        setExemplarDecisionSum(getExemplarDecisionSum() - 1);
      } else {
        setExemplarDecisionSum(-1);
      }
    }
  }

  // Source Gamma Max Tuples Wrapper

  /**
   * Get the {@link GammaMaxTuple} for the specified source.
   *
   * @param source name of a clean source
   * @return {@link GammaMaxTuple} for the specified source.
   */
  public GammaMaxTuple getSrcGMaxTuple(String source) {
    return this.f24.get(source);
  }

  /**
   * Put the {@link GammaMaxTuple} into a map, keyed by the name of the clean source
   *
   * @param source clean source name of the {@link GammaMaxTuple}
   * @param value {@link GammaMaxTuple} for the clean source
   */
  public void putSrcGMaxTuple(String source, GammaMaxTuple value) {
    this.f24.put(source, value);
  }

  /**
   * Clear the {@link GammaMaxTuple}-CleanSource-Map.
   */
  public void resetSrcGMaxValues() {
    f24.clear();
  }

  /**
   * Get the final result information, whether the connected component of the vertex is converged.
   *
   * @return true, if the connected component of the vertex is converged.
   */
  public boolean isComponentConverged() {
    return this.f0;
  }

  /**
   * Set the final result information, whether the connected component of the vertex is converged.
   *
   * @param componentConverged true, if the connected component of the vertex is converged.
   */
  public void setComponentConverged(boolean componentConverged) {
    this.f0 = componentConverged;
  }

  /**
   * Set the name of the vertex's clean source.
   *
   * @param cleanSource name of the vertex's clean source. Empty string, if the vertex is from a dirty source.
   */
  public void setCleanSource(String cleanSource) {
    this.f1 = cleanSource;
  }

  /**
   * Get the name of the vertex's clean source.
   *
   * @return name of the vertex's clean source. Empty string, if the vertex is from a dirty source.
   */
  public String getCleanSource() {
    return this.f1;
  }

  /**
   * Set the name of the vertex's connected component.
   *
   * @param componentId name of the vertex's connected component.
   */
  public void setComponentId(String componentId) {
    this.f2 = componentId;
  }

  /**
   * Get the name of the vertex's connected component.
   *
   * @return name of the vertex's connected component.
   */
  public String getComponentId() {
    return this.f2;
  }

  /**
   * Set the unique long ID of the vertex which is used by other vertices to select this one as their
   * exemplar.
   *
   * @param vertexId unique long ID of the vertex which is used by other vertices to select this one as
   *                 their exemplar
   */
  public void setVertexId(Long vertexId) {
    this.f3 = vertexId;
  }

  /**
   * Get the unique long ID of the vertex which is used by other vertices to select this one as their
   * exemplar.
   *
   * @return unique long ID of the vertex which is used by other vertices to select this one as their
   * exemplar.
   */
  public Long getVertexId() {
    return this.f3;
  }

  /**
   * Set the final result information of the cluster ID of the vertex. It is the unique long vertexId of
   * the vertex which was selected by this one as its exemplar.
   *
   * @param clusterId cluster ID of the vertex. It is the unique long vertexId of the vertex which was
   *                  selected by this one as its exemplar.
   */
  public void setClusterId(Long clusterId) {
    this.f4 = clusterId;
  }

  /**
   * Get the final result information of the cluster ID of the vertex. It is the unique long vertexId of
   * the vertex which was selected by this one as its exemplar.
   *
   * @return cluster ID of the vertex. It is the unique long vertexId of the vertex which was selected by
   * this one as its exemplar.
   */
  public Long getClusterId() {
    return this.f4;
  }

  /**
   * Set the info, whether this vertex is an exemplar.
   *
   * @param isExemplar true if this vertex is an exemplar.
   */
  public void setExemplar(Boolean isExemplar) {
    this.f5 = isExemplar;
  }

  /**
   * Get the info, whether this vertex is an exemplar.
   *
   * @return true if this vertex is an exemplar.
   */
  public Boolean isExemplar() {
    return this.f5;
  }

  /**
   * Set the positive or negative sum of consecutive AP iterations, this vertex was (positive) or was not
   * (negative) selected as an exemplar.
   *
   * @param exemplarDecisionSum sum of consecutive AP iterations, this vertex was or was not selected as an
   *                           exemplar.
   */
  public void setExemplarDecisionSum(Integer exemplarDecisionSum) {
    this.f6 = exemplarDecisionSum;
  }

  /**
   * Get the positive or negative sum of consecutive AP iterations, this vertex was (positive) or was not
   * (negative) selected as an exemplar.
   *
   * @return sum of consecutive AP iterations, this vertex was or was not selected as an exemplar.
   */
  public Integer getExemplarDecisionSum() {
    return this.f6;
  }

  /**
   * Set the current superStep of the vertex centric iteration in {@link ApComputeStepped}.
   *
   * @param step current superStep of the vertex centric iteration
   */
  public void setStep(Integer step) {
    this.f7 = step;
  }

  /**
   * Get the current superStep of the vertex centric iteration in {@link ApComputeStepped}.
   *
   * @return current superStep of the vertex centric iteration
   */
  public Integer getStep() {
    return this.f7;
  }

  /**
   * Set the maximal BETA value of the cells of the vertex's row.
   *
   * @param betaMax maximal BETA value of the cells of the vertex's row.
   */
  public void setBetaMax(Double betaMax) {
    this.f8 = betaMax;
  }

  /**
   * Get the maximal BETA value of the cells of the vertex's row.
   *
   * @return maximal BETA value of the cells of the vertex's row.
   */
  public Double getBetaMax() {
    return this.f8;
  }

  /**
   * Set the second largest BETA value of the cells of the vertex's row.
   *
   * @param betaSecondMax second largest BETA value of the cells of the vertex's row.
   */
  public void setBetaSecondMax(Double betaSecondMax) {
    this.f9 = betaSecondMax;
  }

  /**
   * Get the second largest BETA value of the cells of the vertex's row.
   *
   * @return second largest BETA value of the cells of the vertex's row.
   */
  public Double getBetaSecondMax() {
    return this.f9;
  }

  /**
   * Set the ID of the vertex on whose column the maximal BETA value of this vertex's row was found.
   *
   * @param betaMaxId ID of the vertex on whose column the maximal BETA value of this vertex's row was found.
   */
  public void setBetaMaxId(GradoopId betaMaxId) {
    this.f10 = betaMaxId;
  }

  /**
   * Get the ID of the vertex on whose column the maximal BETA value of this vertex's row was found.
   *
   * @return ID of the vertex on whose column the maximal BETA value of this vertex's row was found.
   */
  public GradoopId getBetaMaxId() {
    return this.f10;
  }

  /**
   * Set the sum of the nonDiagonal RHO values of this vertex's column
   *
   * @param rhoSumNonDiag sum of the nonDiagonal RHO values of this vertex's column
   */
  public void setRhoSumNonDiag(Double rhoSumNonDiag) {
    this.f11 = rhoSumNonDiag;
  }

  /**
   * Get the sum of the nonDiagonal RHO values of this vertex's column
   *
   * @return sum of the nonDiagonal RHO values of this vertex's column
   */
  public Double getRhoSumNonDiag() {
    return this.f11;
  }

  /**
   * Set the diagonal RHO value of this vertex's column
   *
   * @param rhoDiag diagonal RHO value of this vertex's column
   */
  public void setRhoDiag(Double rhoDiag) {
    this.f12 = rhoDiag;
  }

  /**
   * Get the diagonal RHO value of this vertex's column
   *
   * @return diagonal RHO value of this vertex's column
   */
  public Double getRhoDiag() {
    return this.f12;
  }

  /**
   * Get the cumulated information of this vertex and its neighbours about the constraints satisfaction
   * status of the connected component
   *
   * @return true, if all cumulated infos by now are true. So all vertices whose information are part of
   * this accumulation are satisfying the all constraints.
   */
  public Boolean isCumulatedConstraintsInfo() {
    return f13;
  }

  /**
   * Set the cumulated information of this vertex and its neighbours about the constraints satisfaction
   * status of the connected component
   *
   * @param cumulatedConstraintsInfo true, if all cumulated infos by now are true. So all vertices whose
   *                                 information are part of this accumulation are satisfying the all
   *                                 constraints.
   */
  public void setCumulatedConstraintsInfo(Boolean cumulatedConstraintsInfo) {
    this.f13 = cumulatedConstraintsInfo;
  }

  /**
   * Get the cumulated information of the vertex and its neighbours about the convergence status of the
   * connected component
   *
   * @return true, if all cumulated infos by now are true. So all vertices whose information are part of
   * this accumulation are successfully converged.
   */
  public Boolean isCumulatedConvergenceInfo() {
    return f14;
  }

  /**
   * Set the cumulated information of the vertex and its neighbours about the convergence status of the
   * connected component
   *
   * @param cumulatedConvergenceInfo true, if all cumulated infos by now are true. So all vertices whose
   *                                 information are part of this accumulation are successfully converged.
   */
  public void setCumulatedConvergenceInfo(Boolean cumulatedConvergenceInfo) {
    this.f14 = cumulatedConvergenceInfo;
  }

  /**
   * Get the current value for the preference parameter of dirty source data points. This value can change
   * due to parameter adaption.
   *
   * @return current value for the preference parameter of dirty source data points
   */
  public Double getCurrentPreferenceDirtySrc() {
    return this.f15;
  }

  /**
   * Set the current value for the preference parameter of dirty source data points. This value can change
   * due to parameter adaption.
   *
   * @param currentPreferenceDirtySrc current value for the preference parameter of dirty source data points
   */
  public void setCurrentPreferenceDirtySrc(Double currentPreferenceDirtySrc) {
    this.f15 = currentPreferenceDirtySrc;
  }

  /**
   * Get the current value for the preference parameter of clean source data points. This value can change
   * due to parameter adaption.
   *
   * @return current value for the preference parameter of clean source data points
   */
  public Double getCurrentPreferenceCleanSrc() {
    return this.f16;
  }

  /**
   * Set the current value for the preference parameter of clean source data points. This value can change
   * due to parameter adaption.
   *
   * @param currentPreferenceCleanSrc current value for the preference parameter of clean source data points
   */
  public void setCurrentPreferenceCleanSrc(Double currentPreferenceCleanSrc) {
    this.f16 = currentPreferenceCleanSrc;
  }

  /**
   * Get the noise value for the self-edge (preference) of this vertex. This value is stored, so that the
   * same noise can be used when the preference is adapted. So the gelly implementation stays comparable to
   * the other MSCD-AP implementations.
   *
   * @return noise value for the self-edge (preference) of this vertex
   */
  public Double getPreferenceNoise() {
    return this.f17;
  }

  /**
   * Set the noise value for the self-edge (preference) of this vertex. This value is stored, so that the
   * same noise can be used when the preference is adapted. So the gelly implementation stays comparable to
   * the other MSCD-AP implementations.
   *
   * @param preferenceNoise noise value for the self-edge (preference) of this vertex
   */
  public void setPreferenceNoise(Double preferenceNoise) {
    this.f17 = preferenceNoise;
  }

  /**
   * Get the current damping factor of the MSCD-AP process. This value can be changed due to parameter
   * adaption.
   *
   * @return current damping factor of the MSCD-AP process
   */
  public Double getCurrentDamping() {
    return this.f18;
  }

  /**
   * Set the current damping factor of the MSCD-AP process. This value can be changed due to parameter
   * adaption.
   *
   * @param currentDamping current damping factor of the MSCD-AP process
   */
  public void setCurrentDamping(Double currentDamping) {
    this.f18 = currentDamping;
  }

  /**
   * Get the initial value for the preference parameter of dirty source data points. Keeping the initial
   * value is necessary for parameter adaption.
   *
   * @return initial value for the preference parameter of dirty source data points
   */
  public Double getInitialPreferenceDirtySource() {
    return this.f19;
  }

  /**
   * Set the initial value for the preference parameter of dirty source data points. Keeping the initial
   * value is necessary for parameter adaption.
   *
   * @param initialPreferenceDirtySource initial value for the preference parameter of dirty source data
   *                                     points
   */
  public void setInitialPreferenceDirtySource(Double initialPreferenceDirtySource) {
    this.f19 = initialPreferenceDirtySource;
  }

  /**
   * Get the initial value for the preference parameter of clean source data points. Keeping the initial
   * value is necessary for parameter adaption.
   *
   * @return initial value for the preference parameter of clean source data points
   */
  public Double getInitalPreferenceCleanSource() {
    return this.f20;
  }

  /**
   * Set the initial value for the preference parameter of clean source data points. Keeping the initial
   * value is necessary for parameter adaption.
   *
   * @param initialPreferenceCleanSource initial value for the preference parameter of clean source data
   *                                     points
   */
  public void setInitialPreferenceCleanSource(Double initialPreferenceCleanSource) {
    this.f20 = initialPreferenceCleanSource;
  }

  /**
   * Get the number of AP iterations of the current try (current adaption step).
   *
   * @return number of AP iterations of the current try
   */
  public Integer getNumberOfIterations() {
    return this.f21;
  }

  /**
   * Set the number of AP iterations of the current try (current adaption step).
   *
   * @param nrOfIterations number of AP iterations of the current try
   */
  public void setNumberOfIterations(int nrOfIterations) {
    this.f21 = nrOfIterations;
  }

  /**
   * Get the number parameter adaptions that where made by now in the AP process. It represents the number
   * of tries for different parameter configurations.
   *
   * @return number parameter adaptions that where made by now in the AP process
   */
  public Integer getNumberOfAdaptions() {
    return this.f22;
  }

  /**
   * Set the number parameter adaptions that where made by now in the AP process. It represents the number
   * of tries for different parameter configurations.
   *
   * @param nrOfAdaptions number parameter adaptions that where made by now in the AP process
   */
  public void setNumberOfAdaptions(int nrOfAdaptions) {
    this.f22 = nrOfAdaptions;
  }

  /**
   * Get the number of all iterations of the AP process over all adaptions
   *
   * @return number of all iterations of the AP process over all adaptions
   */
  public Integer getSumOfOverallIterations() {
    return this.f23;
  }

  /**
   * Set the number of all iterations of the AP process over all adaptions
   *
   * @param sumOfOverallIterations number of all iterations of the AP process over all adaptions
   */
  public void setSumOfOVerallIterations(int sumOfOverallIterations) {
    this.f23 = sumOfOverallIterations;
  }
}
