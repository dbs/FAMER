/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.serialClustering;

import org.gradoop.famer.clustering.serialClustering.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialEdgeComponent;
import org.gradoop.famer.clustering.serialClustering.dataStructures.SerialVertexComponent;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * The serial implementation of new Merge Center algorithm
 */
public class SerialNewMergeCenter extends AbstractSerialClustering {

  /**
   * Which vertex to select as center based on its vertex priority
   */
  private final PrioritySelection prioritySelection;

  /**
   * List for the {@link SerialEdgeComponent} used in this algorithm
   */
  private List<SerialEdgeComponent> serialEdgeComponentList;

  /**
   * Creates an instance of SerialNewMergeCenter
   *
   * @param prioritySelection Which vertex to select as center based on its vertex priority
   * @param verticesInputPath File path to read the vertices from
   * @param edgesInputPath File path to read the edges from
   * @param outputDir Directory for writing the clustering results
   */
  SerialNewMergeCenter(PrioritySelection prioritySelection, String verticesInputPath, String edgesInputPath,
    String outputDir) {
    super(verticesInputPath, edgesInputPath, outputDir);
    this.prioritySelection = prioritySelection;
    this.serialEdgeComponentList = new ArrayList<>();
  }

  @Override
  protected void initializeSerialGraphComponents() throws IOException {
    getLinesFromFilePath(verticesInputPath).forEach(line -> {
      String[] lineData = line.split(",");
      SerialVertexComponent svc =
        new SerialVertexComponent(Long.parseLong(lineData[1]), Long.parseLong(lineData[1]), false);
      serialVertexComponentMap.put(lineData[0], svc);
    });
    getLinesFromFilePath(edgesInputPath).forEach(line -> {
      String[] lineData = line.split(",");
      SerialEdgeComponent sec =
        new SerialEdgeComponent(lineData[0], lineData[1], Double.parseDouble(lineData[2]));
      serialEdgeComponentList.add(sec);
    });
  }

  @Override
  protected void writeClusteringResultsToFile() throws IOException {
    List<String> clusteringResults = serialVertexComponentMap.entrySet().stream()
      .map(entry -> entry.getKey() + "," + entry.getValue().getClusterId())
      .collect(Collectors.toList());

    writeLinesToFilePath(outputPath, clusteringResults);
  }

  @Override
  protected void doSerialClustering() {
    SerialEdgeComponent[] secArray =
      serialEdgeComponentList.toArray(new SerialEdgeComponent[serialEdgeComponentList.size()]);

    java.util.Arrays.sort(secArray, (in1, in2) -> Double.compare(in2.getDegree(), in1.getDegree()));

    for (int i = 0; i < secArray.length; i++) {
      SerialEdgeComponent sec = secArray[i];
      SerialVertexComponent source = serialVertexComponentMap.get(sec.getSourceId());
      SerialVertexComponent target = serialVertexComponentMap.get(sec.getTargetId());
      long sourceVertexPrio = source.getVertexPrio();
      long targetVertexPrio = target.getVertexPrio();
      boolean isChanged = false;
      if (!source.getIsAssigned() && !target.getIsAssigned()) {
        if (prioritySelection == PrioritySelection.MIN) {
          if (sourceVertexPrio < targetVertexPrio) {
            source.setIsCenter(true);
          } else {
            target.setIsCenter(true);
          }
        } else if (prioritySelection == PrioritySelection.MAX) {
          if (sourceVertexPrio > targetVertexPrio) {
            source.setIsCenter(true);
          } else {
            target.setIsCenter(true);
          }
        }
        if (source.getIsCenter()) {
          source.setClusterId(sourceVertexPrio);
          target.setClusterId(sourceVertexPrio);
        } else if (target.getIsCenter()) {
          source.setClusterId(targetVertexPrio);
          target.setClusterId(targetVertexPrio);
        }
        source.setIsAssigned(true);
        target.setIsAssigned(true);
        isChanged = true;
      } else if (!source.getIsAssigned() && target.getIsCenter()) {
        source.setClusterId(target.getClusterId());
        source.setIsAssigned(true);
        isChanged = true;
      } else if (!target.getIsAssigned() && source.getIsCenter()) {
        target.setClusterId(source.getClusterId());
        target.setIsAssigned(true);
        isChanged = true;
      }
      if (isChanged) {
        serialVertexComponentMap.put(sec.getSourceId(), source);
        serialVertexComponentMap.put(sec.getTargetId(), target);
      }
    }
    for (int i = 0; i < secArray.length; i++) {
      SerialEdgeComponent sec = secArray[i];
      SerialVertexComponent source = serialVertexComponentMap.get(sec.getSourceId());
      SerialVertexComponent target = serialVertexComponentMap.get(sec.getTargetId());
      boolean isChanged = false;
      if (source.getIsCenter() && target.getIsCenter()) {
        long clusterId = 0L;
        long modifyingClusterId = 0L;
        if (prioritySelection == PrioritySelection.MIN) {
          if (source.getClusterId() < target.getClusterId()) {
            clusterId = source.getClusterId();
            modifyingClusterId = target.getClusterId();
            target.setClusterId(clusterId);
          } else if (source.getClusterId() > target.getClusterId()) {
            clusterId = target.getClusterId();
            modifyingClusterId = source.getClusterId();
            source.setClusterId(clusterId);
          }
        } else if (prioritySelection == PrioritySelection.MAX) {
          if (source.getClusterId() > target.getClusterId()) {
            clusterId = source.getClusterId();
            modifyingClusterId = target.getClusterId();
            target.setClusterId(clusterId);
          } else if (source.getClusterId() < target.getClusterId()) {
            clusterId = target.getClusterId();
            modifyingClusterId = source.getClusterId();
            source.setClusterId(clusterId);
          }
        }
        if (modifyingClusterId != clusterId) {
          for (Map.Entry<String, SerialVertexComponent> entry : serialVertexComponentMap.entrySet()) {
            if (entry.getValue().getClusterId() == modifyingClusterId) {
              SerialVertexComponent svc = entry.getValue();
              svc.setClusterId(clusterId);
              entry.setValue(svc);
            }
          }
        }
        isChanged = true;
      } else if (source.getIsCenter() || target.getIsCenter()) {
        long clusterId;
        long modifyingClusterId;
        if (target.getIsCenter()) {
          clusterId = target.getClusterId();
          modifyingClusterId = source.getClusterId();
          source.setClusterId(clusterId);
        } else {
          clusterId = source.getClusterId();
          modifyingClusterId = target.getClusterId();
          target.setClusterId(clusterId);
        }
        if (modifyingClusterId != clusterId) {
          for (Map.Entry<String, SerialVertexComponent> entry : serialVertexComponentMap.entrySet()) {
            if (entry.getValue().getClusterId() == modifyingClusterId) {
              SerialVertexComponent svc = entry.getValue();
              svc.setClusterId(clusterId);
              entry.setValue(svc);
            }
          }
        }
        isChanged = true;
      }
      if (isChanged) {
        serialVertexComponentMap.put(sec.getSourceId(), source);
        serialVertexComponentMap.put(sec.getTargetId(), target);
      }
    }
  }
}


