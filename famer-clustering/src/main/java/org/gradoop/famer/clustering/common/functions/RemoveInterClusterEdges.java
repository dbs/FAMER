/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.common.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Returns an edge if source vertex and target vertex both belong to the same cluster, w.r.t. overlapping
 * clusters.
 */
public class RemoveInterClusterEdges implements
  FlatMapFunction<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>, EPGMEdge> {

  @Override
  public void flatMap(Tuple3<EPGMEdge, EPGMVertex, EPGMVertex> edgeSourceTarget, Collector<EPGMEdge> out)
    throws Exception {

    String[] sourceClusterIds =
      edgeSourceTarget.f1.getPropertyValue(PropertyNames.CLUSTER_ID).toString().split(",");
    String[] targetClusterIds =
      edgeSourceTarget.f2.getPropertyValue(PropertyNames.CLUSTER_ID).toString().split(",");

    for (String sourceClusterId : sourceClusterIds) {
      for (String targetClusterId : targetClusterIds) {
        if (sourceClusterId.equals(targetClusterId)) {
          out.collect(edgeSourceTarget.f0);
          return;
        }
      }
    }
  }
}
