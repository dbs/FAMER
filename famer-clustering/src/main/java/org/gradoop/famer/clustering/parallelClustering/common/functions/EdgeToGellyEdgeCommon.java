/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.clustering.parallelClustering.common.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.graph.Edge;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.clustering.common.PropertyNames;

/**
 * Maps a Gradoop edge to a Gelly {@code Edge<GradoopId, Double>} used in all parallel clustering algorithms.
 * If {@link #isEdgesBiDirected} is {@code false}, a reversed edge is created for each edge, too.
 */
public class EdgeToGellyEdgeCommon implements
  FlatMapFunction<org.gradoop.common.model.impl.pojo.EPGMEdge, Edge<GradoopId, Double>> {

  /**
   * Defines if for each edge a reversed edge shall be created
   */
  private final boolean isEdgesBiDirected;

  /**
   * Reduces object instantiation
   */
  private final Edge<GradoopId, Double> reuseEdge;

  /**
   * Creates an instance of EdgeToGellyEdgeCommon
   *
   * @param isEdgesBiDirected Defines if for each edge a reversed edge shall be created
   */
  public EdgeToGellyEdgeCommon(boolean isEdgesBiDirected) {
    this.isEdgesBiDirected = isEdgesBiDirected;
    this.reuseEdge = new Edge<>();
  }

  @Override
  public void flatMap(org.gradoop.common.model.impl.pojo.EPGMEdge edge,
    Collector<Edge<GradoopId, Double>> out) throws Exception {
    GradoopId sourceId = edge.getSourceId();
    GradoopId targetId = edge.getTargetId();
    double propertyValue = 0.0;
    if (edge.hasProperty(PropertyNames.SIM_VALUE)) {
      propertyValue =
        Double.parseDouble(edge.getPropertyValue(PropertyNames.SIM_VALUE).toString());
    }
    reuseEdge.setValue(propertyValue);
    reuseEdge.setSource(sourceId);
    reuseEdge.setTarget(targetId);
    out.collect(reuseEdge);
    if (!isEdgesBiDirected) {
      reuseEdge.setSource(targetId);
      reuseEdge.setTarget(sourceId);
      out.collect(reuseEdge);
    }
  }
}
