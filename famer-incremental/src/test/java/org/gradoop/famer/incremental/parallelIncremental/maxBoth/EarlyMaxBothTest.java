/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth;

import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.famer.incremental.TestUtils;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;
import static org.gradoop.famer.clustering.common.PropertyNames.SIM_VALUE;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_LINK;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * JUnit tests for {@link EarlyMaxBoth}.
 */
public class EarlyMaxBothTest extends GradoopFlinkTestBase {

  private LogicalGraph sourceWiseInputGraph;

  private LogicalGraph entityWiseInputGraph;

  @Before
  public void setUp() {
    String swGraphString = "input[" +
      "(v1:Node {id:1, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v2:Node {id:2, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v3:Node {id:3, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src3\"})" +
      "(v4:Node {id:4, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v5:Node {id:5, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v6:Node {id:6, " + GRAPH_LABEL + ":\"src4\"})" +
      "(v7:Node {id:7, " + GRAPH_LABEL + ":\"src4\"})" +
      "(v8:Node {id:8, " + GRAPH_LABEL + ":\"src4\"})" +
      "(v1)-[e1]->(v2)" +
      "(v2)-[e2 {" + SIM_VALUE + ":1.0}]->(v3)" +
      "(v2)-[e3 {" + SIM_VALUE + ":0.95}]->(v6)" +
      "(v5)-[e4 {" + SIM_VALUE + ":0.99}]->(v7)" +
      "(v5)-[e5 {" + SIM_VALUE + ":1.0}]->(v8)" +
      "]";
    sourceWiseInputGraph = getLoaderFromString(swGraphString).getLogicalGraphByVariable("input");

    String ewGraphString = "input[" +
      "(v1:Node {id:1, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v2:Node {id:2, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v3:Node {id:3, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src3\"})" +
      "(v4:Node {id:4, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v5:Node {id:5, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v6:Node {id:6, " + GRAPH_LABEL + ":\"src4\"})" +
      "(v7:Node {id:7, " + GRAPH_LABEL + ":\"src2\"})" +
      "(v8:Node {id:8, " + GRAPH_LABEL + ":\"src1\"})" +
      "(v1)-[e1]->(v2)" +
      "(v2)-[e2 {" + SIM_VALUE + ":1.0}]->(v3)" +
      "(v1)-[e3 {" + SIM_VALUE + ":0.95}]->(v7)" +
      "(v6)-[e4 {" + SIM_VALUE + ":1.0, " + NEW_LINK + ":true}]->(v7)" +
      "(v2)-[e5 {" + SIM_VALUE + ":0.97}]->(v6)" +
      "(v5)-[e6 {" + SIM_VALUE + ":0.97}]->(v8)" +
      "]";
    entityWiseInputGraph = getLoaderFromString(ewGraphString).getLogicalGraphByVariable("input");
  }

  @Test
  public void testInstantiationWithJSONConfig() throws Exception {
    String jsonString = "{\n" +
      " \"repairingMethod\":\"MBM\",\n" +
      " \"clusterIdPrefix\":\"mbm\",\n" +
      " \"delta\":0.0,\n" +
      " \"isSCRemoving\":\"false\",\n" +
      " \"clustering\":{\n" +
      "   \"clusteringMethod\":\"CONNECTED_COMPONENTS\",\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      " }\n" +
      "}";

    EarlyMaxBoth earlyMaxBoth = new EarlyMaxBoth(new JSONObject(jsonString));

    assertTrue(earlyMaxBoth.getClusteringAlgorithm() instanceof ConnectedComponents);
    assertEquals("mbm", earlyMaxBoth.getPreClusteringPrefix());
    assertEquals(0.0, earlyMaxBoth.getDelta(), 0.0);
    assertFalse(earlyMaxBoth.isSCRemoving());
  }

  @Test(expected = RuntimeException.class)
  public void testInstantiationWithJSONConfigExpectException1() throws Exception {
    String jsonString = "{\n" +
      " \"repairingMethod\":\"MBM\",\n" +
      " \"delta\":0.0,\n" +
      " \"isSCRemoving\":\"false\",\n" +
      " \"clustering\":{\n" +
      "   \"clusteringMethod\":\"CONNECTED_COMPONENTS\",\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      " }\n" +
      "}";

    new EarlyMaxBoth(new JSONObject(jsonString));
  }

  @Test(expected = RuntimeException.class)
  public void testInstantiationWithJSONConfigExpectException2() throws Exception {
    String jsonString = "{\n" +
      " \"repairingMethod\":\"MBM\",\n" +
      " \"clusterIdPrefix\":\"mbm\",\n" +
      " \"isSCRemoving\":\"false\",\n" +
      " \"clustering\":{\n" +
      "   \"clusteringMethod\":\"CONNECTED_COMPONENTS\",\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      " }\n" +
      "}";

    new EarlyMaxBoth(new JSONObject(jsonString));
  }

  @Test(expected = RuntimeException.class)
  public void testInstantiationWithJSONConfigExpectException3() throws Exception {
    String jsonString = "{\n" +
      " \"repairingMethod\":\"MBM\",\n" +
      " \"clusterIdPrefix\":\"mbm\",\n" +
      " \"delta\":0.0,\n" +
      " \"clustering\":{\n" +
      "   \"clusteringMethod\":\"CONNECTED_COMPONENTS\",\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      " }\n" +
      "}";

    new EarlyMaxBoth(new JSONObject(jsonString));
  }

  @Test
  public void testSourceWiseWithoutPreClustering() throws Exception {
    LogicalGraph outputGraph =
      sourceWiseInputGraph.callForGraph(new EarlyMaxBoth(null, "sw-non", 0.0, false));

    assertEquals(8, outputGraph.getVertices().collect().size());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputGraph.getVertices());

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("1", "2", "3", "6")),
      new HashSet<>(Arrays.asList("4", "5", "8")),
      new HashSet<>(Collections.singletonList("7")));

    assertFalse(result.isEmpty());
    assertEquals(3, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testSourceWiseWithPreClusteringConnComp() throws Exception {
    LogicalGraph outputGraph =
      sourceWiseInputGraph.callForGraph(new EarlyMaxBoth(new ConnectedComponents(20), "sw-cc", 0.0, false));

    assertEquals(8, outputGraph.getVertices().collect().size());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputGraph.getVertices());

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("1", "2", "3", "6")),
      new HashSet<>(Arrays.asList("4", "5", "8")),
      new HashSet<>(Collections.singletonList("7")));

    assertFalse(result.isEmpty());
    assertEquals(3, result.size());
    assertEquals(expectedResult, result);
  }


  @Test
  public void testEntityWiseWithoutPreClustering() throws Exception {
    LogicalGraph outputGraph =
      entityWiseInputGraph.callForGraph(new EarlyMaxBoth(null, "ew-non", 0.0, true));

    assertEquals(8, outputGraph.getVertices().collect().size());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputGraph.getVertices());

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("1", "2", "3", "6")),
      new HashSet<>(Arrays.asList("4", "5")),
      new HashSet<>(Collections.singletonList("7")),
      new HashSet<>(Collections.singletonList("8")));

    assertFalse(result.isEmpty());
    assertEquals(4, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testEntityWiseWithPreClusteringConnComp() throws Exception {
    LogicalGraph outputGraph =
      entityWiseInputGraph.callForGraph(new EarlyMaxBoth(new ConnectedComponents(20), "ew-cc", 0.0, true));

    assertEquals(8, outputGraph.getVertices().collect().size());

    List<Set<String>> result = TestUtils.createSortedClusterSets(outputGraph.getVertices());

    List<Set<String>> expectedResult = Arrays.asList(
      new HashSet<>(Arrays.asList("1", "2", "3")),
      new HashSet<>(Arrays.asList("4", "5")),
      new HashSet<>(Arrays.asList("6", "7")),
      new HashSet<>(Collections.singletonList("8")));

    assertFalse(result.isEmpty());
    assertEquals(4, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testForBaseGraph() throws Exception {
    String graphString = "input[" +
      "(v1:Node {id:1, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v2:Node {id:2, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v3:Node {id:3, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src3\"})" +
      "(v4:Node {id:4, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v5:Node {id:5, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v1)-[e1]->(v2)" +
      "(v2)-[e2 {" + SIM_VALUE + ":1.0}]->(v3)" +
      "]";
    LogicalGraph baseGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    // edges will be removed and SOURCE_LIST property added to vertices
    LogicalGraph resultGraph =
      new EarlyMaxBoth(new ConnectedComponents(20), "ew-cc", 0.0, true).execute(baseGraph);

    baseGraph = getConfig().getLogicalGraphFactory().fromDataSets(baseGraph.getVertices());

    collectAndAssertTrue(baseGraph.equalsByElementIds(resultGraph));
  }
}
