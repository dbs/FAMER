/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;
import static org.gradoop.famer.incremental.common.PropertyNames.SOURCE_LIST;
import static org.junit.Assert.*;

/**
 * JUnit tests for {@link SourceListAdder}
 */
public class SourceListAdderTest extends GradoopFlinkTestBase {

  @Test
  public void testSourceAdding() throws Exception {
    String graphString = "input[" +
      "(v1:Node {id:1, " + CLUSTER_ID + ":\"c1\"," + GRAPH_LABEL + ":\"src1\"})" +
      "(v2:Node {id:2, " + CLUSTER_ID + ":\"c1\"," + GRAPH_LABEL + ":\"src2\"})" +
      "(v3:Node {id:3, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src3\"})" +
      "(v4:Node {id:4, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src1\"})" +
      "]";

    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    LogicalGraph outputGraph = inputGraph.callForGraph(new SourceListAdder());

    List<Tuple2<Integer, List<String>>> result = outputGraph.getVertices().collect().stream()
      .map(vertex -> {
        assertTrue(vertex.hasProperty(SOURCE_LIST));
        List<String> sourceList = vertex.getPropertyValue(SOURCE_LIST).getList().stream()
          .map(PropertyValue::getString)
          .sorted()
          .collect(Collectors.toList());
        return Tuple2.of(vertex.getPropertyValue("id").getInt(), sourceList);
      })
      .sorted(Comparator.comparing(tuple -> tuple.f0))
      .collect(Collectors.toList());

    List<Tuple2<Integer, List<String>>> expectedResult = Arrays.asList(
      Tuple2.of(1, Arrays.asList("src1", "src2", "src3")),
      Tuple2.of(2, Arrays.asList("src1", "src2", "src3")),
      Tuple2.of(3, Arrays.asList("src1", "src2", "src3")),
      Tuple2.of(4, Collections.singletonList("src1")));

    assertFalse(result.isEmpty());
    assertEquals(4, result.size());
    assertEquals(expectedResult, result);
  }
}
