/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.common.utils;

import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/**
 * JUnit tests for {@link Clusterer}
 */
public class ClustererTest extends GradoopFlinkTestBase {

  @Test
  public void testClusteringWithPrefix() throws Exception {
    String graphString = "input[" +
      "(v1:Node {id:1})" +
      "(v2:Node {id:2})" +
      "(v3:Node {id:3})" +
      "(v4:Node {id:4})" +
      "(v5:Node {id:5})" +
      "(v1)-[e1]->(v2)" +
      "(v3)-[e2]->(v4)" +
      "]";

    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");
    String prefix = "testPrefix";

    LogicalGraph clusteredGraph =
      new Clusterer(new ConnectedComponents(20), prefix).execute(inputGraph);

    clusteredGraph.getVertices().collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty(CLUSTER_ID));
      assertTrue(vertex.getPropertyValue(CLUSTER_ID).getString().startsWith(prefix + "-"));
    });

    assertEquals(3, clusteredGraph.splitBy(CLUSTER_ID).getGraphHeads().collect().size());
  }
}
