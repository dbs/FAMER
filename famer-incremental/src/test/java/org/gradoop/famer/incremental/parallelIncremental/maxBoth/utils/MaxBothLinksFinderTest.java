/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.gradoop.famer.clustering.common.PropertyNames.*;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_LINK;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_PREFIX;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * JUnit test for {@link MaxBothLinksFinder}
 */
public class MaxBothLinksFinderTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  @Before
  public void setUp() throws Exception {
    String graphString = "input[" +
      "(v1:Node {id:1, " + CLUSTER_ID + ":\"c1\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v2:Node {id:2, " + CLUSTER_ID + ":\"c2\", " + GRAPH_LABEL + ":\"src1\"})" +
      "(v3:Node {id:3, " + CLUSTER_ID + ":\"" + NEW_PREFIX + "c3\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v4:Node {id:4, " + CLUSTER_ID + ":\"" + NEW_PREFIX + "c4\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v5:Node {id:5, " + CLUSTER_ID + ":\"c5\", " + GRAPH_LABEL + ":\"src2\"})" +
      "(v6:Node {id:6, " + CLUSTER_ID + ":\"c6\", " + GRAPH_LABEL + ":\"src3\"})" +
      "(v7:Node {id:7, " + CLUSTER_ID + ":\"c7\", " + GRAPH_LABEL + ":\"src3\"})" +
      "(v8:Node {id:8, " + CLUSTER_ID + ":\"" + NEW_PREFIX + "c8\", " + GRAPH_LABEL + ":\"src3\"})" +
      "(v1)-[e1 {id: \"e1\", " + SIM_VALUE + ":0.95}]->(v3)" +
      "(v2)-[e2 {id: \"e2\", " + SIM_VALUE + ":0.85}]->(v3)" +
      "(v2)-[e3 {id: \"e3\", " + SIM_VALUE + ":0.95}]->(v4)" +
      "(v2)-[e4 {id: \"e4\", " + SIM_VALUE + ":1.0}]->(v5)" +
      "(v2)-[e5 {id: \"e5\", " + SIM_VALUE + ":0.8}]->(v8)" +
      "(v3)-[e6 {id: \"e6\", " + SIM_VALUE + ":0.98}]->(v6)" +
      "(v3)-[e7 {id: \"e7\", " + SIM_VALUE + ":0.95}]->(v7)" +
      "(v4)-[e8 {id: \"e8\", " + SIM_VALUE + ":0.99}]->(v7)" +
      "(v4)-[e9 {id: \"e9\", " + SIM_VALUE + ":1.0, " + NEW_LINK + ":true" + "}]->(v8)" +
      "(v5)-[e10 {id: \"e10\", " + SIM_VALUE + ":0.9}]->(v8)" + "]";

    inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");
  }

  @Test
  public void testWithDelta0() throws Exception {
    double delta = 0.0;

    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2 =
      new MaxBothLinksFinder(delta).execute(inputGraph);

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(simVertex1Vertex2);

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("1", "3"),
      Tuple2.of("2", "4"),
      Tuple2.of("2", "8"),
      Tuple2.of("3", "6"),
      Tuple2.of("4", "7"),
      Tuple2.of("5", "8"));

    assertFalse(result.isEmpty());
    assertEquals(6, result.size());
    assertEquals(expectedResult, result);
  }

  @Test
  public void testWithDelta005() throws Exception {
    double delta = 0.05;

    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2 =
      new MaxBothLinksFinder(delta).execute(inputGraph);

    List<Tuple2<String, String>> result = collectAndTransformResultTuples(simVertex1Vertex2);

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("1", "3"),
      Tuple2.of("2", "4"),
      Tuple2.of("2", "8"),
      Tuple2.of("3", "6"),
      Tuple2.of("3", "7"),
      Tuple2.of("4", "7"),
      Tuple2.of("5", "8"));

    assertFalse(result.isEmpty());
    assertEquals(7, result.size());
    assertEquals(expectedResult, result);
  }

  private List<Tuple2<String, String>> collectAndTransformResultTuples(
    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2) throws Exception {

    return simVertex1Vertex2.map(
      (MapFunction<Tuple3<Double, EPGMVertex, EPGMVertex>, Tuple2<String, String>>) input -> {
        String id1 = input.f1.getPropertyValue("id").toString();
        String id2 = input.f2.getPropertyValue("id").toString();
        if (id1.compareTo(id2) < 0) {
          return Tuple2.of(id1, id2);
        }
        return Tuple2.of(id2, id1);
      }).returns(new TypeHint<Tuple2<String, String>>() { })
      .collect().stream().sorted(Comparator.comparing(input -> input.f0 + input.f1))
      .collect(Collectors.toList());
  }
}
