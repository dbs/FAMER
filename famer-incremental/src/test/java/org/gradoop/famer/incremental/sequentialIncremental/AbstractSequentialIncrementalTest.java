/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.sequentialIncremental;

import org.codehaus.jettison.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * JUnit test for {@link AbstractSequentialIncremental}
 */
public class AbstractSequentialIncrementalTest {

  private final AbstractSequentialIncremental abstractSequentialIncremental =
    Mockito.mock(AbstractSequentialIncremental.class, Mockito.CALLS_REAL_METHODS);

  private JSONObject incrementalConfig;

  @Before
  public void setUp() throws Exception {
    String configString = "{\n" +
      " \"repairingMethod\":\"GREEDY_CLIP\",\n" +
      " \"basePath\":\"basePath\",\n" +
      " \"newPath\":\"newPath\",\n" +
      " \"outputPath\":\"outputPath\",\n" +
      " \"strengthCoef\":0.3,\n" +
      " \"simValueCoef\":0.5,\n" +
      " \"degreeCoef\":0.2 \n" +
      "}";
    incrementalConfig = new JSONObject(configString);
  }

  @Test
  public void testGetLinesFromFilePath() throws Exception {
    String filePath = new File(AbstractSequentialIncremental.class.getResource(
      "/sequentialIncremental/abstractSequentialIncremental/testReadLines.csv").getFile()).getAbsolutePath();
    Stream<String> lines = abstractSequentialIncremental.getLinesFromFilePath(filePath);

    List<String> linesList = lines.collect(Collectors.toList());

    assertEquals("expected 3 lines, but found " + linesList.size(), 3, linesList.size());
    assertEquals("first line read with errors", "1,one", linesList.get(0));
    assertEquals("second line read with errors", "2,two", linesList.get(1));
    assertEquals("third line read with errors", "3,three", linesList.get(2));
  }

  @Test
  public void testWriteLinesToFilePath() throws Exception {
    File tempOutFile = File.createTempFile("famerSequentialIncrementalTestOutput", ".csv");
    tempOutFile.deleteOnExit();

    List<String> linesToWrite = Arrays.asList("1,one", "2,two", "3,three");
    abstractSequentialIncremental.writeLinesToFilePath(tempOutFile.getPath(), linesToWrite);

    assertTrue(Files.exists(Paths.get(tempOutFile.getPath())));
  }

  @Test
  public void testGetBaseInputPathFromJsonConfig() throws Exception {
    assertEquals("basePath",
      abstractSequentialIncremental.getBaseInputPathFromJsonConfig(incrementalConfig));
  }

  @Test
  public void testGetNewPathFromJsonConfig() throws Exception {
    assertEquals("newPath",
      abstractSequentialIncremental.getNewPathFromJsonConfig(incrementalConfig));
  }

  @Test
  public void testGetOutputPathFromJsonConfig() throws Exception {
    assertEquals("outputPath",
      abstractSequentialIncremental.getOutputPathFromJsonConfig(incrementalConfig));
  }

  @Test(expected = RuntimeException.class)
  public void testGetBaseInputPathFromJsonConfigExpectException() throws Exception {
    String configString = "{\n" +
      " \"repairingMethod\":\"GREEDY_CLIP\",\n" +
      " \"newPath\":\"newPath\",\n" +
      " \"outputPath\":\"outputPath\",\n" +
      " \"strengthCoef\":0.3,\n" +
      " \"simValueCoef\":0.5,\n" +
      " \"degreeCoef\":0.2 \n" +
      "}";
    incrementalConfig = new JSONObject(configString);

    abstractSequentialIncremental.getBaseInputPathFromJsonConfig(incrementalConfig);
  }

  @Test(expected = RuntimeException.class)
  public void testGetNewPathFromJsonConfigExpectException() throws Exception {
    String configString = "{\n" +
      " \"repairingMethod\":\"GREEDY_CLIP\",\n" +
      " \"basePath\":\"basePath\",\n" +
      " \"outputPath\":\"outputPath\",\n" +
      " \"strengthCoef\":0.3,\n" +
      " \"simValueCoef\":0.5,\n" +
      " \"degreeCoef\":0.2 \n" +
      "}";
    incrementalConfig = new JSONObject(configString);

    abstractSequentialIncremental.getNewPathFromJsonConfig(incrementalConfig);
  }

  @Test(expected = RuntimeException.class)
  public void testGetOutputPathFromJsonConfigExpectException() throws Exception {
    String configString = "{\n" +
      " \"repairingMethod\":\"GREEDY_CLIP\",\n" +
      " \"basePath\":\"basePath\",\n" +
      " \"newPath\":\"newPath\",\n" +
      " \"strengthCoef\":0.3,\n" +
      " \"simValueCoef\":0.5,\n" +
      " \"degreeCoef\":0.2 \n" +
      "}";
    incrementalConfig = new JSONObject(configString);

    abstractSequentialIncremental.getOutputPathFromJsonConfig(incrementalConfig);
  }
}
