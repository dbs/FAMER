/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMEdgeFactory;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.pojo.EPGMVertexFactory;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.SOURCE_LIST;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

/**
 * JUnit tests for {@link ClusterMerger}
 */
public class ClusterMergerTest extends GradoopFlinkTestBase {

  private LogicalGraph inputGraph;

  @Before
  public void setUp() throws Exception {
    EPGMVertexFactory vf = new EPGMVertexFactory();

    EPGMVertex v0 = vf.createVertex("v0");
    v0.setProperty(SOURCE_LIST,
      PropertyValue.create(Collections.singletonList(PropertyValue.create("src1"))));
    v0.setProperty(CLUSTER_ID, "c0");

    EPGMVertex v1 = vf.createVertex("v1");
    v1.setProperty(SOURCE_LIST,
      PropertyValue.create(Arrays.asList(PropertyValue.create("src1"), PropertyValue.create("src2"))));
    v1.setProperty(CLUSTER_ID, "c1");

    EPGMVertex v2 = vf.createVertex("v2");
    v2.setProperty(SOURCE_LIST,
      PropertyValue.create(Arrays.asList(PropertyValue.create("src1"), PropertyValue.create("src2"))));
    v2.setProperty(CLUSTER_ID, "c1");

    EPGMVertex v3 = vf.createVertex("v3");
    v3.setProperty(SOURCE_LIST,
      PropertyValue.create(Collections.singletonList(PropertyValue.create("src2"))));
    v3.setProperty(CLUSTER_ID, "c2");

    EPGMVertex v4 = vf.createVertex("v4");
    v4.setProperty(SOURCE_LIST,
      PropertyValue.create(Arrays.asList(PropertyValue.create("src3"), PropertyValue.create("src4"))));
    v4.setProperty(CLUSTER_ID, "new1");

    EPGMVertex v5 = vf.createVertex("v5");
    v5.setProperty(SOURCE_LIST,
      PropertyValue.create(Arrays.asList(PropertyValue.create("src3"), PropertyValue.create("src4"))));
    v5.setProperty(CLUSTER_ID, "new1");

    EPGMVertex v6 = vf.createVertex("v6");
    v6.setProperty(SOURCE_LIST,
      PropertyValue.create(Collections.singletonList(PropertyValue.create("src3"))));
    v6.setProperty(CLUSTER_ID, "new2");

    EPGMVertex v7 = vf.createVertex("v7");
    v7.setProperty(SOURCE_LIST,
      PropertyValue.create(Collections.singletonList(PropertyValue.create("src4"))));
    v7.setProperty(CLUSTER_ID, "new3");

    EPGMEdgeFactory ef = new EPGMEdgeFactory();
    EPGMEdge e1 = ef.createEdge("e1", v1.getId(), v4.getId());
    EPGMEdge e2 = ef.createEdge("e2", v3.getId(), v7.getId());

    inputGraph = getConfig().getLogicalGraphFactory()
      .fromCollections(Arrays.asList(v0, v1, v2, v3, v4, v5, v6, v7), Arrays.asList(e1, e2));
  }

  @Test
  public void testMerging() throws Exception {

    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2 =
      new EdgeToEdgeSourceVertexTargetVertex(inputGraph).execute().map(
        (MapFunction<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>, Tuple3<Double, EPGMVertex, EPGMVertex>>) in ->
          Tuple3.of(0.0, in.f1, in.f2))
        .returns(new TypeHint<Tuple3<Double, EPGMVertex, EPGMVertex>>() { });

    DataSet<EPGMVertex> vertices = new ClusterMerger().execute(simVertex1Vertex2, inputGraph.getVertices());

    List<Tuple2<String, String>> result = vertices.collect().stream()
      .map(vertex -> Tuple2.of(vertex.getLabel(), vertex.getPropertyValue(CLUSTER_ID).toString()))
      .sorted(Comparator.comparing(tuple -> tuple.f0))
      .collect(Collectors.toList());

    List<Tuple2<String, String>> expectedResult = Arrays.asList(
      Tuple2.of("v0", "c0"),
      Tuple2.of("v1", "c1"),
      Tuple2.of("v2", "c1"),
      Tuple2.of("v3", "c2"),
      Tuple2.of("v4", "c1"),
      Tuple2.of("v5", "c1"),
      Tuple2.of("v6", "2"),
      Tuple2.of("v7", "c2"));

    assertFalse(result.isEmpty());
    assertEquals(8, result.size());
    assertEquals(expectedResult, result);
  }
}
