/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.api.java.tuple.Tuple5;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import java.util.ArrayList;
import java.util.List;

import static org.gradoop.famer.clustering.common.PropertyNames.SIM_VALUE;

/**
 * The reduce implementation for finding maximum links
 */
public class MaxLinkGetter implements
  GroupReduceFunction<Tuple5<EPGMEdge, EPGMVertex, EPGMVertex, String, String>,
    Tuple4<EPGMEdge, EPGMVertex, EPGMVertex, String>> {

  /**
   * Reduce object instantiation
   */
  private final Tuple4<EPGMEdge, EPGMVertex, EPGMVertex, String> reuseTuple;

  /**
   * The allowed similarity value loss
   */
  private final double delta;

  /**
   * Creates an instance of MaxLinkGetter
   *
   * @param delta The allowed similarity value loss
   */
  public MaxLinkGetter(double delta) {
    this.delta = delta;
    this.reuseTuple = new Tuple4<>();
  }

  @Override
  public void reduce(Iterable<Tuple5<EPGMEdge, EPGMVertex, EPGMVertex, String, String>> group,
    Collector<Tuple4<EPGMEdge, EPGMVertex, EPGMVertex, String>> out) {
    List<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>> linkV1V2 = new ArrayList<>();
    double maxSim = 0;

    for (Tuple5<EPGMEdge, EPGMVertex, EPGMVertex, String, String> item : group) {
      linkV1V2.add(Tuple3.of(item.f0, item.f1, item.f2));
      double sim = Double.parseDouble(item.f0.getPropertyValue(SIM_VALUE).toString());
      if (sim > maxSim) {
        maxSim = sim;
      }
    }

    for (Tuple3<EPGMEdge, EPGMVertex, EPGMVertex> item : linkV1V2) {
      double sim = Double.parseDouble(item.f0.getPropertyValue(SIM_VALUE).toString());
      if ((maxSim - sim) <= delta) {
        reuseTuple.f0 = item.f0;
        reuseTuple.f1 = item.f1;
        reuseTuple.f2 = item.f2;
        reuseTuple.f3 = item.f0.getId().toString();
        out.collect(reuseTuple);
      }
    }
  }
}
