/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.incremental.parallelIncremental.common.utils.IncrementalUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.SOURCE_LIST;

/**
 * The implementation of groupReduce function that selects one connection (link) among the multiple existing
 * links. The criteria for selection is the similarity value of the link and the size of clusters at the
 * end of the link.
 */
public class Selector implements GroupReduceFunction<Tuple4<Double, EPGMVertex, EPGMVertex, String>,
  Tuple3<Double, EPGMVertex, EPGMVertex>> {

  @Override
  public void reduce(Iterable<Tuple4<Double, EPGMVertex, EPGMVertex, String>> group,
    Collector<Tuple3<Double, EPGMVertex, EPGMVertex>> out) throws Exception {
    List<Tuple3<Double, EPGMVertex, EPGMVertex>> simV1V2List = new ArrayList<>();
    List<String> clusterIdList = new ArrayList<>();

    for (Tuple4<Double, EPGMVertex, EPGMVertex, String> item : group) {
      String clusterId = item.f2.getPropertyValue(CLUSTER_ID).toString();
      if (!clusterIdList.contains(clusterId)) {
        simV1V2List.add(Tuple3.of(item.f0, item.f1, item.f2));
        clusterIdList.add(clusterId);
      }
    }

    if (simV1V2List.size() == 1) {
      Tuple3<Double, EPGMVertex, EPGMVertex> item = simV1V2List.get(0);
      out.collect(Tuple3.of(item.f0, item.f1, item.f2));
    } else if (simV1V2List.size() > 1) {
      simV1V2List.sort(new SimSizeComparator());
      List<PropertyValue> currentSources = simV1V2List.get(0).f1.getPropertyValue(SOURCE_LIST).getList();
      for (Tuple3<Double, EPGMVertex, EPGMVertex> item : simV1V2List) {
        List<PropertyValue> newSources = item.f2.getPropertyValue(SOURCE_LIST).getList();
        if (IncrementalUtils.isConsistent(currentSources, newSources)) {
          out.collect(Tuple3.of(item.f0, item.f1, item.f2));
          currentSources.addAll(newSources);
        }
      }
    }
  }

  /**
   * <pre>
   * The comparator class the compares the two input objects by
   *  1) link similarity value, then
   *  2) the cluster size
   * The list that uses this comparator for sorting is sorted in descending order.
   * </pre>
   */
  private static class SimSizeComparator implements
    Serializable, Comparator<Tuple3<Double, EPGMVertex, EPGMVertex>> {

    @Override
    public int compare(Tuple3<Double, EPGMVertex, EPGMVertex> tuple1,
      Tuple3<Double, EPGMVertex, EPGMVertex> tuple2) {
      // compare on link similarity
      double sim1 = tuple1.f0;
      double sim2 = tuple2.f0;
      int simDiff = BigDecimal.valueOf(sim1).compareTo(BigDecimal.valueOf(sim2));
      if (simDiff > 0) {
        return -1;
      }
      if (simDiff < 0) {
        return 1;
      }
      // if link similarity is equal, compare on number of sources in cluster
      int sourceSize1 = tuple1.f2.getPropertyValue(SOURCE_LIST).getList().size();
      int sourceSize2 = tuple2.f2.getPropertyValue(SOURCE_LIST).getList().size();
      int sizeDiff = sourceSize1 - sourceSize2;
      if (sizeDiff > 0) {
        return -1;
      }
      if (sizeDiff < 0) {
        return 1;
      }
      // finally
      return 0;
    }
  }
}
