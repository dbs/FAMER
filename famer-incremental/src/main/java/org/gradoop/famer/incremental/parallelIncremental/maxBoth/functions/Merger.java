/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;

/**
 * The implementation of join function for merging clusters. It assigns an already existing clusterId to the
 * vertex in order to add it to the corresponding cluster.
 */
public class Merger implements JoinFunction<Tuple2<EPGMVertex, String>, Tuple2<String, String>, EPGMVertex> {

  @Override
  public EPGMVertex join(Tuple2<EPGMVertex, String> in1, Tuple2<String, String> in2) throws Exception {
    in1.f0.setProperty(CLUSTER_ID, in2.f1);
    return in1.f0;
  }
}
