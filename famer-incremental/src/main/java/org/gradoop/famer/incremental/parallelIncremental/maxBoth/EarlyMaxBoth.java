/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringMethod;
import org.gradoop.famer.incremental.parallelIncremental.AbstractParallelIncremental;
import org.gradoop.famer.incremental.parallelIncremental.common.functions.EPGMGraphIdResetter;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils.ClusterMerger;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils.ClusterSelector;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils.MaxBothLinksFinder;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils.PreClusterer;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils.SourceInconsistentRemover;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils.SourceListAdder;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * The Gradoop/Flink implementation of Early Max Both algorithm (MBM).
 * <p>
 *   Note: This algorithm does not return the original edges, but only the (re)clustered vertices of the
 *   linked input graph.
 * </p>
 */
public class EarlyMaxBoth extends AbstractParallelIncremental {

  /**
   * The algorithm used for clustering
   */
  private final AbstractParallelClustering clusteringAlgorithm;

  /**
   * The prefix string for clusterIds of pre-clustering
   */
  private final String preClusteringPrefix;

  /**
   * The allowed similarity value loss
   */
  private final double delta;

  /**
   * Whether the source consistency constraint should be applied
   */
  private final boolean isSCRemoving;

  /**
   * Creates an instance of EarlyMaxBoth
   *
   * @param clusteringAlgorithm The algorithm used for clustering
   * @param preClusteringPrefix The prefix string for clusterIds of pre-clustering
   * @param delta The allowed similarity value loss
   * @param isSCRemoving Whether the source consistency constraint should be applied
   */
  public EarlyMaxBoth(AbstractParallelClustering clusteringAlgorithm, String preClusteringPrefix,
    double delta, boolean isSCRemoving) {
    this.clusteringAlgorithm = clusteringAlgorithm;
    this.preClusteringPrefix = preClusteringPrefix;
    this.delta = delta;
    this.isSCRemoving = isSCRemoving;
  }

  /**
   * Constructor used for json parsing
   *
   * @param incrementalConfig Contains the incremental repairing configuration parameters
   */
  public EarlyMaxBoth(JSONObject incrementalConfig) {
    try {
      JSONObject clusteringConfig = incrementalConfig.getJSONObject("clustering");
      ClusteringMethod clusteringMethod =
        ClusteringMethod.valueOf(clusteringConfig.getString("clusteringMethod"));

      if (clusteringMethod.equals(ClusteringMethod.NONE)) {
        this.clusteringAlgorithm = null;
      } else {
        Class<?> clusteringClass = Class.forName(clusteringMethod.getFullClassName());
        this.clusteringAlgorithm = (AbstractParallelClustering) clusteringClass
          .getDeclaredConstructor(JSONObject.class).newInstance(clusteringConfig);
      }
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Incremental Repairing - EarlyMaxBoth: " +
        "Clustering object could not be parsed to instantiate clustering algorithm", ex);
    }
    try {
      this.preClusteringPrefix = incrementalConfig.getString("clusterIdPrefix");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Incremental Repairing - EarlyMaxBoth: " +
        "String value for clusterIdPrefix could not be found or parsed", ex);
    }
    try {
      this.delta = incrementalConfig.getDouble("delta");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Incremental Repairing - EarlyMaxBoth: " +
        "Double value for delta could not be found or parsed", ex);
    }
    try {
      this.isSCRemoving = incrementalConfig.getBoolean("isSCRemoving");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Incremental Repairing - EarlyMaxBoth: " +
        "Boolean value for isSCRemoving could not be found or parsed", ex);
    }
  }

  public AbstractParallelClustering getClusteringAlgorithm() {
    return clusteringAlgorithm;
  }

  public String getPreClusteringPrefix() {
    return preClusteringPrefix;
  }

  public double getDelta() {
    return delta;
  }

  public boolean isSCRemoving() {
    return isSCRemoving;
  }

  @Override
  public LogicalGraph runIncrementalRepairing(LogicalGraph inputGraph) {
    /* run preclustering, add cluster sources to vertex property */
    inputGraph = inputGraph.callForGraph(new PreClusterer(clusteringAlgorithm, preClusteringPrefix));
    inputGraph = inputGraph.callForGraph(new SourceListAdder());

    /* Select pairs which are paired via a max-both link */
    DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> simVertex1Vertex2 =
      new MaxBothLinksFinder(delta).execute(inputGraph);

    /* Remove pairs which can not get merged due to the source consistency constraint */
    simVertex1Vertex2 = new SourceInconsistentRemover(isSCRemoving).execute(simVertex1Vertex2);

    /* In case of having multiple max-both (and source-consistent) links, prioritize and select as many
       as possible of them */
    simVertex1Vertex2 = new ClusterSelector().execute(simVertex1Vertex2);

    /* Merge the corresponding clusters of each pair */
    DataSet<EPGMVertex> vertices = new ClusterMerger().execute(simVertex1Vertex2, inputGraph.getVertices());

    vertices = vertices.map(new EPGMGraphIdResetter<>());

    return inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(vertices);
  }
}
