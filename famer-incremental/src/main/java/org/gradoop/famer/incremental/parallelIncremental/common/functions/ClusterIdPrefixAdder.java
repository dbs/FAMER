/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.common.functions;

import org.apache.flink.api.common.functions.MapFunction;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;

/**
 * Prepends a given prefix string to the cluster id property of a vertex
 */
public class ClusterIdPrefixAdder implements MapFunction<EPGMVertex, EPGMVertex> {

  /**
   * The prefix for the clusterIds
   */
  private final String prefix;

  /**
   * Creates an instance of PrefixAdder
   *
   * @param prefix The prefix for the clusterIds
   */
  public ClusterIdPrefixAdder(String prefix) {
    this.prefix = prefix;
  }

  @Override
  public EPGMVertex map(EPGMVertex vertex) throws Exception {
    String newClusterId = prefix + "-" + vertex.getPropertyValue(CLUSTER_ID).getString();
    vertex.setProperty(CLUSTER_ID, newClusterId);
    return vertex;
  }
}
