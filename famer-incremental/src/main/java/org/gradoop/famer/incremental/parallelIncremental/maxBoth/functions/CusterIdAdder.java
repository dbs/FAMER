/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.incremental.parallelIncremental.common.utils.IncrementalUtils;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;

/**
 * Append the cluster id of the desired vertex to the input tuple
 */
public class CusterIdAdder implements
  FlatMapFunction<Tuple3<Double, EPGMVertex, EPGMVertex>, Tuple4<Double, EPGMVertex, EPGMVertex, String>> {

  /**
   * It specifies the type of the desired vertex. "old" refers to the existing vertices.
   */
  public static final String FLAG_OLD = "old";

  /**
   * It specifies the type of the desired vertex. "new" refers to the newly added vertices.
   */
  public static final String FLAG_NEW = "new";

  /**
   * It specifies the type of the desired vertex and is either {@link #FLAG_OLD} or {@link #FLAG_NEW}
   */
  private final String flag;

  /**
   * Creates an instance of CusterIdAdder
   *
   * @param flag It specifies the type of desired vertex (existing (old)/ new (new))
   */
  public CusterIdAdder(String flag) {
    this.flag = flag;
  }

  @Override
  public void flatMap(Tuple3<Double, EPGMVertex, EPGMVertex> simVertex1Vertex2,
    Collector<Tuple4<Double, EPGMVertex, EPGMVertex, String>> collector) throws Exception {

    String clsId1 = simVertex1Vertex2.f1.getPropertyValue(CLUSTER_ID).toString();
    String clsId2 = simVertex1Vertex2.f2.getPropertyValue(CLUSTER_ID).toString();

    if (flag.equals(FLAG_OLD)) {
      if (IncrementalUtils.isNew(clsId1) && !IncrementalUtils.isNew(clsId2)) {
        collector.collect(Tuple4.of(
          simVertex1Vertex2.f0, simVertex1Vertex2.f2, simVertex1Vertex2.f1, clsId2));
      } else if (!IncrementalUtils.isNew(clsId1) && IncrementalUtils.isNew(clsId2)) {
        collector.collect(Tuple4.of(
          simVertex1Vertex2.f0, simVertex1Vertex2.f1, simVertex1Vertex2.f2, clsId1));
      }
    }
    if (flag.equals(FLAG_NEW)) {
      if (IncrementalUtils.isNew(clsId1) && !IncrementalUtils.isNew(clsId2)) {
        collector.collect(Tuple4.of(
          simVertex1Vertex2.f0, simVertex1Vertex2.f1, simVertex1Vertex2.f2, clsId1));
      } else if (!IncrementalUtils.isNew(clsId1) && IncrementalUtils.isNew(clsId2)) {
        collector.collect(Tuple4.of(
          simVertex1Vertex2.f0, simVertex1Vertex2.f2, simVertex1Vertex2.f1, clsId2));
      }
    }
  }
}
