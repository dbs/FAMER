/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.ndr;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.functions.Minus;
import org.gradoop.famer.clustering.common.functions.MinusVertexGroupReducer;
import org.gradoop.famer.clustering.common.functions.VertexToVertexClusterId;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.VertexToVertexVertexId;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringMethod;
import org.gradoop.famer.incremental.parallelIncremental.AbstractParallelIncremental;
import org.gradoop.famer.incremental.parallelIncremental.common.functions.EPGMGraphIdResetter;
import org.gradoop.famer.incremental.parallelIncremental.common.functions.SingletonMaker;
import org.gradoop.famer.incremental.parallelIncremental.common.utils.Clusterer;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.gradoop.flink.model.impl.functions.epgm.PropertyRemover;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.NEIGHBOR;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_LINK;

/**
 * The Gradoop/Flink implementation of nDR (n-depth reclustering) with n=1. This implementation benefits
 * from some indexing done in the linking phase. Therefore the runtime is optimized.
 */
public class OneDR extends AbstractParallelIncremental {

  /**
   * The algorithm used for clustering
   */
  private final AbstractParallelClustering clusteringAlgorithm;

  /**
   * The prefix of newly clustered vertices
   */
  private final String clusterIdPrefix;

  /**
   * Creates an instance of OneDR
   *
   * @param clusteringAlgorithm The algorithm used for clustering
   * @param clusterIdPrefix The prefix of newly clustered vertices
   */
  public OneDR(AbstractParallelClustering clusteringAlgorithm, String clusterIdPrefix) {
    this.clusteringAlgorithm = clusteringAlgorithm;
    this.clusterIdPrefix = clusterIdPrefix;
  }

  /**
   * Constructor used for json parsing
   *
   * @param incrementalConfig Contains the incremental repairing configuration parameters
   */
  public OneDR(JSONObject incrementalConfig) {
    try {
      JSONObject clusteringConfig = incrementalConfig.getJSONObject("clustering");
      ClusteringMethod clusteringMethod =
        ClusteringMethod.valueOf(clusteringConfig.getString("clusteringMethod"));

      if (clusteringMethod.equals(ClusteringMethod.NONE)) {
        this.clusteringAlgorithm = null;
      } else {
        Class<?> clusteringClass = Class.forName(clusteringMethod.getFullClassName());
        this.clusteringAlgorithm = (AbstractParallelClustering) clusteringClass
          .getDeclaredConstructor(JSONObject.class).newInstance(clusteringConfig);
      }
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Incremental Repairing - OneDR: " +
        "Clustering object could not be parsed to instantiate clustering algorithm", ex);
    }
    try {
      this.clusterIdPrefix = incrementalConfig.getString("clusterIdPrefix");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Incremental Repairing - OneDR: " +
        "String value for clusterIdPrefix could not be found or parsed", ex);
    }
  }

  public AbstractParallelClustering getClusteringAlgorithm() {
    return clusteringAlgorithm;
  }

  public String getClusterIdPrefix() {
    return clusterIdPrefix;
  }

  @Override
  public LogicalGraph runIncrementalRepairing(LogicalGraph inputGraph) {
    LogicalGraphFactory logicalGraphFactory = inputGraph.getConfig().getLogicalGraphFactory();

    DataSet<EPGMVertex> allVerticesWithClusterId = inputGraph.getVertices().map(new SingletonMaker());
    DataSet<EPGMVertex> newVertices = inputGraph.getVertices().filter(v -> !v.hasProperty(CLUSTER_ID));

    DataSet<Tuple1<String>> firstDepthClusterIds = inputGraph.getEdges()
      .flatMap((FlatMapFunction<EPGMEdge, Tuple1<String>>) (edge, out) -> {
        if (edge.hasProperty(NEIGHBOR)) {
          out.collect(Tuple1.of(edge.getPropertyValue(NEIGHBOR).toString()));
        }
      }).returns(new TypeHint<Tuple1<String>>() { }).distinct(0);

    DataSet<EPGMVertex> firstDepthVertices = allVerticesWithClusterId
      .flatMap(new VertexToVertexClusterId(CLUSTER_ID, false))
      .join(firstDepthClusterIds)
      .where(1).equalTo(0)
      .with((JoinFunction<Tuple2<EPGMVertex, String>, Tuple1<String>, EPGMVertex>) (in1, in2) -> in1.f0)
      .returns(new TypeHint<EPGMVertex>() { });

    DataSet<EPGMVertex> verticesForReclustering = newVertices.union(firstDepthVertices);

    /* cluster new entities + 1-depth clusters (direct neighbors): build graph, run clustering */
    DataSet<EPGMVertex> reclusteredVertices =
      logicalGraphFactory.fromDataSets(verticesForReclustering, inputGraph.getEdges())
        .callForGraph(new Clusterer(clusteringAlgorithm, clusterIdPrefix)).getVertices();

    DataSet<EPGMVertex> vertices = new Minus<>(new MinusVertexGroupReducer()).execute(
      allVerticesWithClusterId.map(new VertexToVertexVertexId()),
      reclusteredVertices.map(new VertexToVertexVertexId()))
      .union(reclusteredVertices);

    /* removes the NEW_LINK property from edges so that the new edges of this increment are not mixed up
    with the new edges of the next increment */
    DataSet<EPGMEdge> edges = inputGraph.getEdges().map(new PropertyRemover<>(NEW_LINK));

    // remove Gradoop graph ids
    vertices = vertices.map(new EPGMGraphIdResetter<>());
    edges = edges.map(new EPGMGraphIdResetter<>());

    return logicalGraphFactory.fromDataSets(vertices, edges);
  }
}
