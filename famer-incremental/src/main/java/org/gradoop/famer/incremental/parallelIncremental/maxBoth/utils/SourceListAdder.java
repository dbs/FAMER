/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.apache.flink.api.java.DataSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.functions.VertexToVertexClusterId;
import org.gradoop.famer.incremental.parallelIncremental.common.functions.EPGMGraphIdResetter;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions.SourceListMaker;
import org.gradoop.flink.model.api.operators.UnaryGraphToGraphOperator;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;

/**
 * Add the source list property to each vertex. The value of the source list property is the list of
 * sources of the cluster that the vertex belongs to.
 */
public class SourceListAdder implements UnaryGraphToGraphOperator {

  @Override
  public LogicalGraph execute(LogicalGraph inputGraph) {
    DataSet<EPGMVertex> vertices = inputGraph.getVertices()
      .flatMap(new VertexToVertexClusterId(CLUSTER_ID, false))
      .groupBy(1)
      .reduceGroup(new SourceListMaker());

    vertices = vertices.map(new EPGMGraphIdResetter<>());
    DataSet<EPGMEdge> edges = inputGraph.getEdges().map(new EPGMGraphIdResetter<>());

    return inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(vertices, edges);
  }
}
