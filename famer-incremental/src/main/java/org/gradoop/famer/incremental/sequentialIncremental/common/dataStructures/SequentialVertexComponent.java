/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.sequentialIncremental.common.dataStructures;

/**
 * Vertex structure for sequential incremental repairing algorithms
 */
public class SequentialVertexComponent {

  /**
   * Id for the vertex component
   */
  private String id;

  /**
   * Cluster id for the vertex component
   */
  private String clusterId;

  /**
   * The origin source of the vertex (@GRAPH_LABEL)
   */
  private String source;

  /**
   * Creates an instance of SequentialVertexComponent
   *
   * @param source    The origin source of the vertex (@GRAPH_LABEL)
   * @param clusterId Cluster id for the vertex component
   */
  public SequentialVertexComponent(String source, String clusterId) {
    this.source = source;
    this.clusterId = clusterId;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getClusterId() {
    return clusterId;
  }

  public void setClusterId(String clusterId) {
    this.clusterId = clusterId;
  }

  public String getSource() {
    return source;
  }

  public void setSource(String source) {
    this.source = source;
  }

}
