/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.common.utils;

import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.famer.incremental.common.PropertyNames;

import java.util.List;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_PREFIX;

/**
 * Util class holding common methods for all packages in famer-incremental module and methods used in
 * linking module.
 */
public class IncrementalUtils {

  /**
   * Checks whether the input clusterId is related to a newly added vertex
   *
   * @param clusterId the input clusterId string
   *
   * @return true, if the input clusterId is related to a newly added vertex
   */
  public static boolean isNew(String clusterId) {
    return clusterId.startsWith(NEW_PREFIX);
  }

  /**
   * The method checks whether the two input source lists are compatible, thus there is no overlap.
   *
   * @param sourceList1 The first source list
   * @param sourceList2 The second source list
   *
   * @return whether the two input source lists are compatible
   */
  public static boolean isConsistent(List<PropertyValue> sourceList1, List<PropertyValue> sourceList2) {
    for (PropertyValue source : sourceList1) {
      if (sourceList2.contains(source)) {
        return false;
      }
    }
    return true;
  }

  /**
   * Retrieves the status of an edge by checking if source / target have already been clustered. Needed for
   * incremental module. Returns the following states:
   * <p>
   *   0 -> edge connects new entities only (new-to-new), edge gets {@link PropertyNames#NEW_LINK} property
   *   1 -> edge connects new entity and existing (already clustered) entity
   *   2 -> edge connects existing (already clustered) entities only
   * </p>
   *
   * @param vertex1 first vertex
   * @param vertex2 second vertex
   *
   * @return The retrieved status number
   */
  public static int getLinkStatus(EPGMVertex vertex1, EPGMVertex vertex2) {
    boolean hasClusterIdVertex1 = vertex1.hasProperty(CLUSTER_ID);
    boolean hasClusterIdVertex2 = vertex2.hasProperty(CLUSTER_ID);
    if (!hasClusterIdVertex1 && !hasClusterIdVertex2) { // new-to-new link
      return 0;
    } else if (hasClusterIdVertex1 && hasClusterIdVertex2) { // existing-to-existing link
      return 2;
    }
    // new-to-existing link
    return 1;
  }

  /**
   * Retrieves the neighbor clusterId of the new entity. Needed for incremental module. Expects that only 1
   * vertex has cluster id property.
   *
   * @param vertex1 first vertex
   * @param vertex2 second vertex
   *
   * @return The neighbor cluster id
   */
  public static String getNeighborClusterId(EPGMVertex vertex1, EPGMVertex vertex2) {
    String clusterId = "";
    if (vertex1.hasProperty(CLUSTER_ID)) {
      clusterId = vertex1.getPropertyValue(CLUSTER_ID).toString();
    } else if (vertex2.hasProperty(CLUSTER_ID)) {
      clusterId = vertex2.getPropertyValue(CLUSTER_ID).toString();
    }
    return clusterId;
  }
}
