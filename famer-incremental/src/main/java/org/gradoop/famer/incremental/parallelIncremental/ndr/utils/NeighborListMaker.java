/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.ndr.utils;

import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

import java.util.ArrayList;
import java.util.List;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.SPLITTER;

/**
 * Find the neighbors of each clusterId. The output is a dataset of Strings. Each String contains the
 * clusterIds that are separated using the {@code PropertyNames.SPLITTER}. The first substring of each
 * string is a clusterId with all its neighbors listed in the rest of string (separated by
 * {@code PropertyNames.SPLITTER})
 */
public class NeighborListMaker {

  /**
   * The implementation of the FlatMap function. It collects neighbor clusterIds from the input paired
   * vertices
   *
   * @param in The input vertex pairs
   * @param collector The output collected neighbor clusterId pairs
   */
  private static void flatMap(Tuple3<EPGMEdge, EPGMVertex, EPGMVertex> in,
    Collector<Tuple2<String, String>> collector) {
    String clsId1 = in.f1.getPropertyValue(CLUSTER_ID).toString();
    String clsId2 = in.f2.getPropertyValue(CLUSTER_ID).toString();
    collector.collect(Tuple2.of(clsId1, clsId2));
    collector.collect(Tuple2.of(clsId2, clsId1));
  }

  /**
   * The implementation of the Reduce function. It collects all neighbors of a clusterId as a String
   *
   * @param iterable The input grouped list of neighbor clusterIds for each clusterId
   * @param collector The output collected String for each clusterId
   */
  private static void reduce(Iterable<Tuple2<String, String>> iterable, Collector<String> collector) {
    List<String> neighborIdList = new ArrayList<>();
    String key = "";
    StringBuilder builder = new StringBuilder();
    for (Tuple2<String, String> item : iterable) {
      if (!neighborIdList.contains(item.f1) && !item.f1.equals(item.f0)) {
        builder.append(SPLITTER);
        builder.append(item.f1);
        neighborIdList.add(item.f1);
        key = item.f0;
      }
    }
    String neighborIdStr = builder.toString();
    if (!neighborIdStr.isEmpty()) {
      neighborIdStr = neighborIdStr.substring(1);
      collector.collect(key + SPLITTER + neighborIdStr);
    }
  }

  /**
   * Executes the class NeighborListMaker
   *
   * @param input The paired vertices
   *
   * @return The list of all neighbors of each clusterId represented as one string
   */
  public DataSet<String> execute(DataSet<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>> input) {
    DataSet<Tuple2<String, String>> clsId1ClsId2 = input.flatMap(
      (FlatMapFunction<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>, Tuple2<String, String>>)
        NeighborListMaker::flatMap);

    return clsId1ClsId2.groupBy(0).reduceGroup(
      (GroupReduceFunction<Tuple2<String, String>, String>) NeighborListMaker::reduce);
  }
}
