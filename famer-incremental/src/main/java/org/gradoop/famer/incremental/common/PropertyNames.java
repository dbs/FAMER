/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.common;

/**
 * All common property names used for incremental repairing
 */
public class PropertyNames {

  /**
   * Property key to access the list of sources of one cluster
   */
  public static final String SOURCE_LIST = "sourceList";

  /**
   * Prefix of the @CLUSTER_ID value of the new entities
   */
  public static final String NEW_PREFIX = "new";

  /**
   * Property key of the edges that connect new entities with existing ones. Its value is the existing
   * {@link org.gradoop.famer.clustering.common.PropertyNames#CLUSTER_ID}
   */
  public static final String NEIGHBOR = "neighborOf";

  /**
   * Property key of the edges that connect new entities only (new to new).
   */
  public static final String NEW_LINK = "newLink";

  /**
   * Property key of the splitter character when concatenating values in one string
   */
  public static final String SPLITTER = ",";
}
