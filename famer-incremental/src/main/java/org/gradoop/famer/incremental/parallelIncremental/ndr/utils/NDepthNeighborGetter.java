/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.ndr.utils;

import org.apache.flink.api.java.DataSet;
import org.gradoop.famer.incremental.parallelIncremental.ndr.functions.NextDepthGetter;

import static org.gradoop.famer.incremental.parallelIncremental.ndr.functions.NextDepthGetter.CURRENT_NEIGHBORS_BROADCAST;
import static org.gradoop.famer.incremental.parallelIncremental.ndr.functions.NextDepthGetter.NEIGHBORS_BROADCAST;

/**
 * Find the nDepth neighbors of newly added entities
 */
public class NDepthNeighborGetter {

  /**
   * Specifies the depth
   */
  private int n;

  /**
   * Creates an instance of NDepthNeighborGetter
   *
   * @param n Specifies the depth
   */
  public NDepthNeighborGetter(int n) {
    this.n = n;
  }

  /**
   * Executes the NDepthNeighborGetter
   *
   * @param newClusterIds The cluster ids of the newly added entities
   * @param neighborList The Map of all neighbors for each clusterId
   *
   * @return The nDepth clusterIds
   */
  public DataSet<String> execute(DataSet<String> newClusterIds, DataSet<String> neighborList) {
    DataSet<String> nDepthClusterIds = newClusterIds;

    while (n > 0) {
      newClusterIds = newClusterIds.flatMap(new NextDepthGetter())
        .withBroadcastSet(neighborList, NEIGHBORS_BROADCAST)
        .withBroadcastSet(nDepthClusterIds, CURRENT_NEIGHBORS_BROADCAST);

      nDepthClusterIds = nDepthClusterIds.union(newClusterIds);

      n--;
    }

    return nDepthClusterIds;
  }
}
