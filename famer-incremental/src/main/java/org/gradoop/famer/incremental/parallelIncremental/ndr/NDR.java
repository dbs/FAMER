/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.ndr;

import org.apache.flink.api.common.functions.JoinFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple1;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.clustering.common.functions.Minus;
import org.gradoop.famer.clustering.common.functions.MinusVertexGroupReducer;
import org.gradoop.famer.clustering.common.functions.VertexToVertexClusterId;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.clip.functions.VertexToVertexVertexId;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringMethod;
import org.gradoop.famer.incremental.parallelIncremental.AbstractParallelIncremental;
import org.gradoop.famer.incremental.parallelIncremental.common.functions.EPGMGraphIdResetter;
import org.gradoop.famer.incremental.parallelIncremental.common.functions.SingletonMaker;
import org.gradoop.famer.incremental.parallelIncremental.common.utils.Clusterer;
import org.gradoop.famer.incremental.parallelIncremental.ndr.utils.NDepthNeighborGetter;
import org.gradoop.famer.incremental.parallelIncremental.ndr.utils.NeighborListMaker;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.epgm.LogicalGraphFactory;
import org.gradoop.flink.model.impl.functions.tuple.ObjectTo1;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.gradoop.famer.incremental.common.PropertyNames.NEW_PREFIX;

/**
 * The Gradoop/Flink implementation of nDR algorithm (n-depth reclustering).
 */
public class NDR extends AbstractParallelIncremental {

  /**
   * The algorithm used for clustering
   */
  private final AbstractParallelClustering clusteringAlgorithm;

  /**
   * The prefix of newly clustered vertices
   */
  private final String clusterIdPrefix;

  /**
   * The specified depth
   */
  private final int depth;

  /**
   * Creates an instance of NDR
   *
   * @param clusteringAlgorithm The algorithm used for clustering
   * @param clusterIdPrefix The prefix of newly clustered vertices
   * @param depth The specified depth
   */
  public NDR(AbstractParallelClustering clusteringAlgorithm, String clusterIdPrefix, int depth) {
    this.clusteringAlgorithm = clusteringAlgorithm;
    this.clusterIdPrefix = clusterIdPrefix;
    this.depth = depth;
  }

  /**
   * Constructor used for json parsing
   *
   * @param incrementalConfig Contains the incremental repairing configuration parameters
   */
  public NDR(JSONObject incrementalConfig) {
    try {
      JSONObject clusteringConfig = incrementalConfig.getJSONObject("clustering");
      ClusteringMethod clusteringMethod =
        ClusteringMethod.valueOf(clusteringConfig.getString("clusteringMethod"));

      if (clusteringMethod.equals(ClusteringMethod.NONE)) {
        this.clusteringAlgorithm = null;
      } else {
        Class<?> clusteringClass = Class.forName(clusteringMethod.getFullClassName());
        this.clusteringAlgorithm = (AbstractParallelClustering) clusteringClass
          .getDeclaredConstructor(JSONObject.class).newInstance(clusteringConfig);
      }
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Incremental Repairing - NDR: " +
        "Clustering object could not be parsed to instantiate clustering algorithm", ex);
    }
    try {
      this.clusterIdPrefix = incrementalConfig.getString("clusterIdPrefix");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Incremental Repairing - NDR: " +
        "String value for clusterIdPrefix could not be found or parsed", ex);
    }
    try {
      this.depth = incrementalConfig.getInt("depth");
    } catch (Exception ex) {
      throw new UnsupportedOperationException("Incremental Repairing - NDR: " +
        "Integer value for depth could not be found or parsed", ex);
    }
  }

  public AbstractParallelClustering getClusteringAlgorithm() {
    return clusteringAlgorithm;
  }

  public String getClusterIdPrefix() {
    return clusterIdPrefix;
  }

  public int getDepth() {
    return depth;
  }

  @Override
  public LogicalGraph runIncrementalRepairing(LogicalGraph inputGraph) {
    LogicalGraphFactory logicalGraphFactory = inputGraph.getConfig().getLogicalGraphFactory();

    DataSet<EPGMVertex> allVertices = inputGraph.getVertices().map(new SingletonMaker());
    DataSet<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>> edgeSourceTarget =
      new EdgeToEdgeSourceVertexTargetVertex(allVertices, inputGraph.getEdges()).execute();

    /* Create list of neighbors and cluster ids for broadcasting */
    DataSet<String> neighborList = new NeighborListMaker().execute(edgeSourceTarget);
    // filter on new prefix in cluster id and get distinct cluster ids
    DataSet<String> newClusterIds = allVertices
      .filter(v -> v.getPropertyValue(CLUSTER_ID).toString().startsWith(NEW_PREFIX))
      .map(v -> v.getPropertyValue(CLUSTER_ID).toString())
      .distinct();

    DataSet<String> nDepthClusterIds =
      new NDepthNeighborGetter(depth).execute(newClusterIds, neighborList).distinct();

    /* Join */
    DataSet<Tuple2<EPGMVertex, String>> vertexClusterId = allVertices
      .flatMap(new VertexToVertexClusterId(CLUSTER_ID, false));
    DataSet<Tuple1<String>> nDepthClusterIdTuples = nDepthClusterIds.map(new ObjectTo1<>()).distinct(0);

    DataSet<EPGMVertex> nDepthVertices = vertexClusterId.join(nDepthClusterIdTuples)
      .where(1).equalTo(0)
      .with((JoinFunction<Tuple2<EPGMVertex, String>, Tuple1<String>, EPGMVertex>) (in1, in2) -> in1.f0)
      .returns(new TypeHint<EPGMVertex>() { });

    LogicalGraph nDepthGraph = logicalGraphFactory.fromDataSets(nDepthVertices, inputGraph.getEdges());

    /* Clustering */
    nDepthGraph = nDepthGraph.callForGraph(new Clusterer(clusteringAlgorithm, clusterIdPrefix));

    nDepthVertices = nDepthGraph.getVertices();

    DataSet<EPGMVertex> vertices = new Minus<>(new MinusVertexGroupReducer()).execute(
      allVertices.map(new VertexToVertexVertexId()), nDepthVertices.map(new VertexToVertexVertexId()))
      .union(nDepthVertices);

    vertices = vertices.map(new EPGMGraphIdResetter<>());
    DataSet<EPGMEdge> edges = inputGraph.getEdges().map(new EPGMGraphIdResetter<>());

    return logicalGraphFactory.fromDataSets(vertices, edges);
  }
}
