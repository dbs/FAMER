/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.ndr.functions;

import org.apache.flink.api.common.functions.RichFlatMapFunction;
import org.apache.flink.configuration.Configuration;
import org.apache.flink.util.Collector;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.gradoop.famer.incremental.common.PropertyNames.SPLITTER;

/**
 * The Flat Map implementation for getting the direct neighbors.
 */
public class NextDepthGetter extends RichFlatMapFunction<String, String> {

  /**
   * Broadcast name for the neighbors set
   */
  public static final String NEIGHBORS_BROADCAST = "neighbors";

  /**
   * Broadcast name for the current neighbors set
   */
  public static final String CURRENT_NEIGHBORS_BROADCAST = "currentNeighbors";

  /**
   * The Map contains the list of all neighbors for each clusterId
   */
  private Map<String, List<String>> neighbors;

  /**
   * The list of already found neighbors
   */
  private List<String> currentNeighbors;

  @Override
  public void open(Configuration parameters) throws Exception {
    List<String> neighborsList = getRuntimeContext().getBroadcastVariable(NEIGHBORS_BROADCAST);
    neighbors = new HashMap<>();
    for (String item : neighborsList) {
      String[] splitNeighbors = item.split(SPLITTER);
      String key = splitNeighbors[0];
      List<String> valueList = new ArrayList<>();
      for (int i = 1; i < splitNeighbors.length; i++) {
        valueList.add(splitNeighbors[i]);
      }
      neighbors.put(key, valueList);
    }
    currentNeighbors = getRuntimeContext().getBroadcastVariable(CURRENT_NEIGHBORS_BROADCAST);
  }

  @Override
  public void flatMap(String newClusterId, Collector<String> out) throws Exception {
    List<String> valueList = new ArrayList<>();

    if (neighbors.containsKey(newClusterId)) {
      valueList = neighbors.get(newClusterId);
    }

    for (String value : valueList) {
      if (!currentNeighbors.contains(value)) {
        out.collect(value);
      }
    }
  }
}

