/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions;

import org.apache.flink.api.common.functions.GroupReduceFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.util.Collector;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;

import java.util.ArrayList;
import java.util.List;

import static org.gradoop.famer.clustering.common.PropertyNames.GRAPH_LABEL;
import static org.gradoop.famer.incremental.common.PropertyNames.SOURCE_LIST;

/**
 * The implementation of groupReduce function that stores all existing sources of a cluster as a list in each
 * vertex in its source list property.
 */
public class SourceListMaker implements GroupReduceFunction<Tuple2<EPGMVertex, String>, EPGMVertex> {

  @Override
  public void reduce(Iterable<Tuple2<EPGMVertex, String>> group, Collector<EPGMVertex> out) {
    List<EPGMVertex> vertexList = new ArrayList<>();
    List<PropertyValue> sourceList = new ArrayList<>();

    for (Tuple2<EPGMVertex, String> item : group) {
      vertexList.add(item.f0);
      sourceList.add(item.f0.getPropertyValue(GRAPH_LABEL));
    }

    for (EPGMVertex vertex : vertexList) {
      vertex.setProperty(SOURCE_LIST, sourceList);
      out.collect(vertex);
    }
  }
}
