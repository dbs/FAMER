/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.common.utils;

import org.apache.flink.api.java.DataSet;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.incremental.parallelIncremental.common.functions.ClusterIdPrefixAdder;
import org.gradoop.flink.model.api.operators.UnaryGraphToGraphOperator;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * The class creates clusters according to the passed {@link #clusteringAlgorithm} and adds the
 * {@link #clusterIdPrefix} to each cluster id property.
 */
public class Clusterer implements UnaryGraphToGraphOperator {

  /**
   * The used clustering algorithm
   */
  private final AbstractParallelClustering clusteringAlgorithm;

  /**
   * The input prefix for clusterIds
   */
  private final String clusterIdPrefix;

  /**
   * Creates an instance of Clusterer
   *
   * @param clusteringAlgorithm The used clustering algorithm
   * @param clusterIdPrefix  The input prefix for clusterIds
   */
  public Clusterer(AbstractParallelClustering clusteringAlgorithm, String clusterIdPrefix) {
    this.clusteringAlgorithm = clusteringAlgorithm;
    this.clusterIdPrefix = clusterIdPrefix;
  }

  @Override
  public LogicalGraph execute(LogicalGraph inputGraph) {
    DataSet<EPGMVertex> vertices =
      clusteringAlgorithm.execute(inputGraph).getVertices().map(new ClusterIdPrefixAdder(clusterIdPrefix));

    return inputGraph.getConfig().getLogicalGraphFactory().fromDataSets(vertices, inputGraph.getEdges());
  }
}
