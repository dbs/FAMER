/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.incremental.parallelIncremental.maxBoth.utils;

import org.apache.flink.api.common.functions.FilterFunction;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple3;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.clustering.common.functions.EdgeToEdgeSourceVertexTargetVertex;
import org.gradoop.famer.incremental.parallelIncremental.common.utils.IncrementalUtils;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions.MaxBothVNewVGetter;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions.MaxLinkGetter;
import org.gradoop.famer.incremental.parallelIncremental.maxBoth.functions.V1IDV2SourceGetter;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;

/**
 * Find the max-both links
 */
public class MaxBothLinksFinder {

  /**
   * The allowed similarity value loss
   */
  private final double delta;

  /**
   * Creates an instance of MaxBothLinksFinder
   *
   * @param delta The allowed similarity value loss
   */
  public MaxBothLinksFinder(double delta) {
    this.delta = delta;
  }

  /**
   * Executes the MaxBothLinksFinder
   *
   * @param inputGraph The graph with vertices and edges
   *
   * @return The max-both pairs of vertices with the corresponding similarity value between them
   */
  public DataSet<Tuple3<Double, EPGMVertex, EPGMVertex>> execute(LogicalGraph inputGraph) {
    DataSet<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>> edgeSourceTarget =
      new EdgeToEdgeSourceVertexTargetVertex(inputGraph).execute();
    // keep new-to-existing / existing-to-new edges only for max both finding
    edgeSourceTarget = edgeSourceTarget.filter(
      (FilterFunction<Tuple3<EPGMEdge, EPGMVertex, EPGMVertex>>) in -> {
        boolean sourceIsNew = IncrementalUtils.isNew(in.f1.getPropertyValue(CLUSTER_ID).toString());
        boolean targetIsNew = IncrementalUtils.isNew(in.f2.getPropertyValue(CLUSTER_ID).toString());
        return (!sourceIsNew && targetIsNew) || (sourceIsNew && !targetIsNew);
      });

    return edgeSourceTarget.flatMap(new V1IDV2SourceGetter())
      .groupBy(3, 4)
      .reduceGroup(new MaxLinkGetter(delta))
      .groupBy(3)
      .reduceGroup(new MaxBothVNewVGetter());
  }
}
