/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.linking.selection;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.StandAloneConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.linking.selection.dataStructures.SelectionComponent;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRule;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRuleComponent;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRuleComponentType;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.ArithmeticOperator;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.Condition;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.ConditionOperator;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.LogicalOperator;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRule;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRuleComponent;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRuleComponentType;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to parse the JSON configuration for selection and build all needed objects.
 */
public class SelectionConfiguration implements StandAloneConfiguration {

  /**
   * The {@link SelectionComponent} needed for linking
   */
  private SelectionComponent selectionComponent;

  public SelectionComponent getSelectionComponent() {
    return selectionComponent;
  }

  /**
   * Parses and checks the linking JSON configuration object and tries to build the selection component.
   * A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configFilePath Path to the json configuration file for linking
   * @return The instance of {@link SelectionConfiguration} with the initialized component
   */
  @Override
  public SelectionConfiguration checkConfigAndBuildComponents(String configFilePath) {
    JSONObject linkingJsonConfig;
    try {
      String jsonString =
        new String(Files.readAllBytes(Paths.get(configFilePath)), StandardCharsets.UTF_8);
      linkingJsonConfig = new JSONObject(jsonString);
    } catch (Exception ex) {
      throw getSelectionConfigException("Could not parse JSON configuration file.", ex);
    }

    return checkConfigAndBuildComponents(linkingJsonConfig);
  }

  /**
   * Parses and checks the linking JSON configuration object and tries to build the selection component.
   * A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configJSON The json configuration object for linking
   * @return The instance of {@link SelectionConfiguration} with the initialized component
   */
  @Override
  public SelectionConfiguration checkConfigAndBuildComponents(JSONObject configJSON) {
    if (configJSON.optJSONObject("selectionComponent") == null) {
      throw getSelectionConfigException("No selectionComponent found in linking configuration");
    }

    try {
      JSONObject selectionJsonObject = configJSON.getJSONObject("selectionComponent");

      // build aggregation rule if present
      AggregatorRule aggregatorRule = null;
      JSONObject aggregationRuleObject = selectionJsonObject.optJSONObject("aggregationRule");
      if (aggregationRuleObject != null) {
        aggregatorRule = buildAggregatorRule(aggregationRuleObject);
      }

      // build selection rule if present
      SelectionRule selectionRule = null;
      JSONObject selectionRuleObject = selectionJsonObject.optJSONObject("selectionRule");
      if (selectionRuleObject != null) {
        selectionRule = buildSelectionRule(selectionRuleObject);
      }

      // build selection component
      selectionComponent = new SelectionComponent(selectionRule, aggregatorRule);

      return this;
    } catch (Exception ex) {
      throw getSelectionConfigException("Could not build selection component", ex);
    }
  }

  /**
   * Creates an {@link AggregatorRule} for the associated aggregation rule key in the given json object.
   *
   * @param aggregationRuleObject The json object that specify the aggregation rule.
   * @return An {@link AggregatorRule} for the given json object.
   */
  AggregatorRule buildAggregatorRule(JSONObject aggregationRuleObject) {
    try {
      double aggregationThreshold = aggregationRuleObject.getDouble("aggregationThreshold");

      JSONArray aggregatorRuleComponents = aggregationRuleObject.optJSONArray("ruleComponents");

      // if no rule components, aggregate default weighted average with threshold
      if ((aggregatorRuleComponents == null) || (aggregatorRuleComponents.length() == 0)) {
        return new AggregatorRule(aggregationThreshold);
      }

      // build and use rule components for aggregation
      List<AggregatorRuleComponent> componentList = new ArrayList<>();

      for (int i = 0; i < aggregatorRuleComponents.length(); i++) {
        JSONObject ruleObject = aggregatorRuleComponents.getJSONObject(i);
        AggregatorRuleComponentType componentType =
          AggregatorRuleComponentType.valueOf(ruleObject.getString("componentType"));

        switch (componentType) {
        case OPEN_PARENTHESIS:
          // add ( operator
          componentList.add(new AggregatorRuleComponent(componentType, "("));
          break;
        case CLOSE_PARENTHESIS:
          // add ) operator
          componentList.add(new AggregatorRuleComponent(componentType, ")"));
          break;
        case ARITHMETIC_OPERATOR:
          // build and add arithmetic operator, conversion used to check consistency
          ArithmeticOperator arithmeticOperator = ArithmeticOperator.valueOf(ruleObject.getString("value"));
          componentList.add(new AggregatorRuleComponent(componentType, arithmeticOperator.toString()));
          break;
        case SIMILARITY_FIELD_ID:
        case CONSTANT:
          String value = ruleObject.getString("value");
          componentList.add(new AggregatorRuleComponent(componentType, value));
          break;
        default:
          throw getSelectionConfigException("Unknown or non configurable aggregation rule component");
        }
      }

      return new AggregatorRule(componentList, aggregationThreshold);
    } catch (Exception ex) {
      throw getSelectionConfigException("Could not build aggregator rule", ex);
    }
  }

  /**
   * Create a {@link SelectionRule} for the associated selection rule key in the given json object.
   *
   * @param selectionRuleObject The json object that specify the selection rule.
   * @return A {@link SelectionRule} for the given json object.
   */
  SelectionRule buildSelectionRule(JSONObject selectionRuleObject) {
    try {
      JSONArray selectionRuleComponents = selectionRuleObject.getJSONArray("ruleComponents");

      // if no rule components are given do not use selection
      if (selectionRuleComponents.length() == 0) {
        return null;
      }

      List<Condition> conditions = new ArrayList<>();
      List<SelectionRuleComponent> componentList = new ArrayList<>();

      for (int i = 0; i < selectionRuleComponents.length(); i++) {
        JSONObject ruleObject = selectionRuleComponents.getJSONObject(i);
        SelectionRuleComponentType componentType =
          SelectionRuleComponentType.valueOf(ruleObject.getString("componentType"));

        switch (componentType) {
        case CONDITION:
          // build and collect condition
          String conditionId = ruleObject.getString("conditionId");
          String similarityFieldId = ruleObject.getString("similarityFieldId");
          ConditionOperator operator = ConditionOperator.valueOf(ruleObject.getString("operator"));
          double threshold = ruleObject.getDouble("threshold");
          Condition condition = new Condition(conditionId, similarityFieldId, operator, threshold);
          if (ruleObject.opt("defaultOnAttributeNull") != null) {
            condition.setDefaultOnAttributeNull(ruleObject.getBoolean("defaultOnAttributeNull"));
          }
          conditions.add(condition);
          // add condition id to selection rule
          componentList.add(new SelectionRuleComponent(componentType, conditionId));
          break;
        case SELECTION_OPERATOR:
          // build and add selection operator, conversion used to check consistency
          LogicalOperator logicalOperator = LogicalOperator.valueOf(ruleObject.getString("value"));
          componentList.add(new SelectionRuleComponent(componentType, logicalOperator.toString()));
          break;
        case OPEN_PARENTHESIS:
          // add ( operator
          componentList.add(new SelectionRuleComponent(componentType, "("));
          break;
        case CLOSE_PARENTHESIS:
          // add ) operator
          componentList.add(new SelectionRuleComponent(componentType, ")"));
          break;
        default:
          throw getSelectionConfigException("Unknown or non configurable selection rule component");
        }
      }

      return new SelectionRule(conditions, componentList);
    } catch (Exception ex) {
      throw getSelectionConfigException("Could not build selection rule", ex);
    }
  }

  /**
   * Creates a {@link FamerConfigException} for selection configuration
   *
   * @param message the detail message
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getSelectionConfigException(String message) {
    message = "Selection configuration failed:\n" + message;
    return new FamerConfigException(message);
  }

  /**
   * Creates a {@link FamerConfigException} for selection configuration
   *
   * @param message the detail message
   * @param cause   the cause
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getSelectionConfigException(String message, Throwable cause) {
    message = "Selection configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
