/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.sequential;

import org.codehaus.jettison.json.JSONArray;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Main entry class to execute sequentially implemented algorithms of FAMER.
 * It currently runs only the sequential incremental repairing algorithms.
 */
public class FamerSequentialConfiguration {

  /**
   * Main method to check the JSON configuration for consistency and run the single tasks.
   *
   * @param args Command line arguments
   */
  public static void main(String[] args) throws Exception {

    if (args.length == 0) {
      System.out.println("No path for json configuration file given");
      return;
    }

    // read json config file path from args, parse it to array of task sequences and check for content
    String jsonConfigFilePath = args[0];
    JSONArray taskSequences = parseJsonConfigFromFile(jsonConfigFilePath);

    // TODO: implement for sequential algorithms in famer-incremental and famer-clustering
    for (int i = 0; i < taskSequences.length(); i++) {
      // TODO
    }
  }

  /**
   * Reads the json configuration file and parses it into a {@link JSONArray}
   *
   * @param jsonConfigFilePath The path to the json configuration file
   * @return {@link JSONArray} with the FAMER configuration task sequences
   */
  private static JSONArray parseJsonConfigFromFile(String jsonConfigFilePath) {
    try {
      String jsonString =
        new String(Files.readAllBytes(Paths.get(jsonConfigFilePath)), StandardCharsets.UTF_8);
      return new JSONArray(jsonString);
    } catch (Exception ex) {
      throw getFamerConfigException("Could not parse JSON configuration file.", ex);
    }
  }

  /**
   * Creates a {@link FamerConfigException} for overall configuration
   *
   * @param message the detail message
   * @param cause   the cause
   * @return The {@link FamerConfigException}
   */
  private static FamerConfigException getFamerConfigException(String message, Throwable cause) {
    message = "FAMER sequential configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
