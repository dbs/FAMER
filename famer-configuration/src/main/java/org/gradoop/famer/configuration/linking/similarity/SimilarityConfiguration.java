/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.linking.similarity;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.StandAloneConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.SimilarityComponent;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to parse the JSON configuration for similarity measuring and build all needed objects.
 */
public class SimilarityConfiguration implements StandAloneConfiguration {

  /**
   * The list of {@link SimilarityComponent}s needed for linking
   */
  private List<SimilarityComponent> similarityComponents;

  public List<SimilarityComponent> getSimilarityComponents() {
    return similarityComponents;
  }

  /**
   * Parses and checks the linking JSON configuration file and tries to build the similarity components.
   * A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configFilePath Path to the json configuration file for linking
   *
   * @return The instance of {@link SimilarityConfiguration} with the initialized components
   */
  @Override
  public SimilarityConfiguration checkConfigAndBuildComponents(String configFilePath) {
    JSONObject linkingJsonConfig;
    try {
      String jsonString =
        new String(Files.readAllBytes(Paths.get(configFilePath)), StandardCharsets.UTF_8);
      linkingJsonConfig =  new JSONObject(jsonString);
    } catch (Exception ex) {
      throw getSimilarityConfigException("Could not parse JSON configuration file.", ex);
    }

    return checkConfigAndBuildComponents(linkingJsonConfig);
  }

  /**
   * Parses and checks the linking JSON configuration object and tries to build the similarity components.
   * A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param configJSON The json configuration object for linking
   *
   * @return The instance of {@link SimilarityConfiguration} with the initialized components
   */
  @Override
  public SimilarityConfiguration checkConfigAndBuildComponents(JSONObject configJSON) {
    if (configJSON.optJSONArray("similarityComponents") == null) {
      throw getSimilarityConfigException("No similarityComponents found in linking configuration");
    }
    if (configJSON.optJSONArray("similarityComponents").length() == 0) {
      throw getSimilarityConfigException("Array of similarityComponents is empty");
    }

    List<SimilarityComponent> tmpSimilarityComponents = new ArrayList<>();

    JSONArray similarityJson = configJSON.optJSONArray("similarityComponents");

    for (int i = 0; i < similarityJson.length(); i++) {

      try {
        JSONObject config = similarityJson.getJSONObject(i);
        String similarityMethod = config.getString("similarityMethod");

        Class<?> similarityComponentClass = Class.forName(
          "org.gradoop.famer.linking.similarityMeasuring.dataStructures." + similarityMethod);

        SimilarityComponent similarityComponent = (SimilarityComponent) similarityComponentClass
          .getDeclaredConstructor(JSONObject.class)
          .newInstance(config);

        tmpSimilarityComponents.add(similarityComponent);
      } catch (Exception ex) {
        throw getSimilarityConfigException("Could not instantiate similarity component.", ex);
      }
    }

    similarityComponents = tmpSimilarityComponents;

    return this;
  }

  /**
   * Creates a {@link FamerConfigException} for similarity configuration
   *
   * @param message the detail message
   *
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getSimilarityConfigException(String message) {
    message = "Similarity configuration failed:\n" + message;
    return new FamerConfigException(message);
  }

  /**
   * Creates a {@link FamerConfigException} for similarity configuration
   *
   * @param message the detail message
   * @param cause   the cause
   *
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getSimilarityConfigException(String message, Throwable cause) {
    message = "Similarity configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
