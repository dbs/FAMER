/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.clustering.ClusteringConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.configuration.incremental.IncrementalConfiguration;
import org.gradoop.famer.configuration.linking.LinkingConfiguration;
import org.gradoop.famer.configuration.postprocessing.PostprocessingConfiguration;
import org.gradoop.famer.configuration.preprocessing.PreprocessingConfiguration;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Main entry class to build and execute a deduplication workflow with FAMER
 * <p>
 * NOTE: This is the first naive version for the FAMER json configuration parsing. It will change over time!
 */
public class FamerConfiguration {

  /**
   * Available FAMER tasks
   */
  private enum Task {
    /**
     * Read and prepare graph data
     */
    PREPROCESSING,
    /**
     * Run full linking process to build a similarity graph
     */
    LINKING,
    /**
     * Run clustering to build a clustered graph
     */
    CLUSTERING,
    /**
     * Run incremental repairing algorithm to cluster the unclustered vertices
     */
    INCREMENTAL_REPAIRING,
    /**
     * Run evaluation, write graphs and results
     */
    POSTPROCESSING
  }

  /**
   * Available in- and output data types for tasks
   */
  public enum DataType {
    /**
     * Gradoop {@link LogicalGraph}
     */
    LOGICAL_GRAPH,
    /**
     * Gradoop {@link GraphCollection}
     */
    GRAPH_COLLECTION,
    /**
     * For benchmark data. {@code DataSet<Tuple2<?,?>>} with each Tuple holding the ids of a perfect
     * matched pair of entities
     */
    PERFECT_MAPPING,
    /**
     * For benchmark data. {@code DataSet<List<?>>} with each list entry holding the entity ids that belong
     * to one cluster
     */
    PERFECT_CLUSTERING
  }

  /**
   * Main method to check the JSON configuration for consistency and run the single tasks.
   *
   * @param args Command line arguments
   */
  public static void main(String[] args) throws Exception {

    if (args.length == 0) {
      System.out.println("No path for json configuration file given");
      return;
    }

    // read json config file path from args, parse it to array of task sequences and check for content
    String jsonConfigFilePath = args[0];
    JSONArray taskSequences = parseJsonConfigFromFile(jsonConfigFilePath);
    // consistency check for task sequences
    checkTaskSequenceConsistency(taskSequences);

    // process all task sequences
    for (int i = 0; i < taskSequences.length(); i++) {
      JSONArray tasks = taskSequences.getJSONArray(i);
      Object passedData = null;
      for (int j = 0; j < tasks.length(); j++) {
        JSONObject taskObject = tasks.getJSONObject(j);
        Task task = Task.valueOf(taskObject.getString("task"));
        switch (task) {
        case PREPROCESSING:
          PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
          preprocessingConfiguration.checkConfigAndBuildComponents(taskObject.getJSONObject("config"));
          passedData = preprocessingConfiguration.runPreprocessing();
          break;
        case LINKING:
          LinkingConfiguration linkingConfiguration = new LinkingConfiguration();
          linkingConfiguration.checkConfigAndBuildComponents(taskObject.getJSONObject("config"));
          if (passedData instanceof GraphCollection) {
            passedData = linkingConfiguration.runLinking((GraphCollection) passedData);
          } else if (passedData instanceof LogicalGraph) {
            passedData = linkingConfiguration.runLinking((LogicalGraph) passedData);
          }
          break;
        case CLUSTERING:
          assert passedData instanceof LogicalGraph;
          ClusteringConfiguration clusteringConfiguration =
            new ClusteringConfiguration().checkConfigAndBuildComponents(taskObject.getJSONObject("config"));
          passedData = clusteringConfiguration.runClustering((LogicalGraph) passedData);
          break;
        case INCREMENTAL_REPAIRING:
          IncrementalConfiguration incrementalConfiguration =
            new IncrementalConfiguration().checkConfigAndBuildComponents(taskObject.getJSONObject("config"));
          passedData = incrementalConfiguration.runIncrementalRepairing((LogicalGraph) passedData);
          break;
        case POSTPROCESSING:
          new PostprocessingConfiguration().runPostprocessing(taskObject.getJSONArray("config"), passedData);
          break;
        default:
          break;
        }
      }
    }
  }

  /**
   * Reads the json configuration file and parses it into a {@link JSONArray}
   *
   * @param jsonConfigFilePath The path to the json configuration file
   * @return {@link JSONArray} with the FAMER configuration task sequences
   */
  private static JSONArray parseJsonConfigFromFile(String jsonConfigFilePath) {
    try {
      String jsonString =
        new String(Files.readAllBytes(Paths.get(jsonConfigFilePath)), StandardCharsets.UTF_8);
      return new JSONArray(jsonString);
    } catch (Exception ex) {
      throw getFamerConfigException("Could not parse JSON configuration file.", ex);
    }
  }

  /**
   * Checks all task sequences for consistency. A {@link FamerConfigException} will be thrown for any
   * inconsistency or error. Constraints for a sequence and its single tasks are:
   * <pre>
   * - each sequence must start with preprocessing and end with postprocessing
   * - preprocessing can only be followed by linking, clustering or postprocessing
   * - linking can only be followed by clustering, incremental repairing or postprocessing
   * - clustering can only be followed by postprocessing
   * - incremental repairing can only be followed by postprocessing
   * - the output graph type of a task has to match the input graph type of the following task
   * - the parsing of the JSON config for each task must run without errors
   * </pre>
   *
   * @param taskSequences The {@link JSONArray} of task sequences
   */
  private static void checkTaskSequenceConsistency(JSONArray taskSequences) throws JSONException {
    if ((taskSequences == null) || (taskSequences.length() == 0)) {
      throw getFamerConfigException("Could not find any task sequences in JSON configuration file.");
    }
    for (int i = 0; i < taskSequences.length(); i++) {
      JSONArray tasks = taskSequences.getJSONArray(i);
      // check for mandatory tasks preprocessing on first position and postprocessing on last position
      if (!tasks.getJSONObject(0).getString("task").equals(Task.PREPROCESSING.toString())) {
        throw getFamerConfigException("Sequence " + i + ": First task of sequence must be PREPROCESSING");
      }
      if (!tasks.getJSONObject(tasks.length() - 1).getString("task").equals(Task.POSTPROCESSING.toString())) {
        throw getFamerConfigException("Sequence " + i + ": Last task of sequence must be POSTPROCESSING");
      }

      // check all tasks by checking the compatibility of the current with the following
      for (int j = 0; j < tasks.length() - 1; j++) {
        try {
          // check current task
          JSONObject currentTaskObject = tasks.getJSONObject(j);
          Task currentTask = Task.valueOf(currentTaskObject.getString("task"));
          if (currentTaskObject.opt("config") == null) {
            throw getFamerConfigException("No config found for task " + currentTask.toString());
          }
          JSONObject currentConfig = currentTaskObject.getJSONObject("config");
          // check next task
          JSONObject nextTaskObject = tasks.getJSONObject(j + 1);
          Task nextTask = Task.valueOf(nextTaskObject.getString("task"));
          if (nextTaskObject.opt("config") == null) {
            throw getFamerConfigException("No config found for task " + nextTask.toString());
          }
          if (nextTaskObject.optJSONObject("config") == null) {
            JSONArray nextConfig = nextTaskObject.getJSONArray("config");
          } else {
            JSONObject nextConfig = nextTaskObject.getJSONObject("config");
          }
          // check task order and compatibility and single task consistency
          switch (currentTask) {
          case PREPROCESSING:
            new PreprocessingConfiguration().checkConfigAndBuildComponents(currentConfig);
            DataType currentOutput = DataType.valueOf(currentConfig.getString("return"));
            switch (nextTask) {
            case LINKING:
              if ((currentOutput != DataType.GRAPH_COLLECTION) && (currentOutput != DataType.LOGICAL_GRAPH)) {
                throw getFamerConfigException("Incompatible return type of preprocessing:\n" +
                  "following task LINKING needs LOGICAL_GRAPH or GRAPH_COLLECTION as input");
              }
              break;
            case CLUSTERING:
              if (currentOutput != DataType.LOGICAL_GRAPH) {
                throw getFamerConfigException("Incompatible return type of preprocessing:\n" +
                  "following task CLUSTERING needs LOGICAL_GRAPH as input");
              }
              break;
            case POSTPROCESSING:
              new PostprocessingConfiguration()
                .checkPostprocessingConfig(nextTaskObject.getJSONArray("config"), currentOutput);
              break;
            default:
              throw getFamerConfigException("Unknown task or incompatible task order\n " +
                "PREPROCESSING can only be followed by LINKING, CLUSTERING or POSTPROCESSING");
            }
            break;
          case LINKING:
            new LinkingConfiguration().checkConfigAndBuildComponents(currentConfig);
            switch (nextTask) {
            case CLUSTERING:
            case INCREMENTAL_REPAIRING:
              break;
            case POSTPROCESSING:
              new PostprocessingConfiguration()
                .checkPostprocessingConfig(nextTaskObject.getJSONArray("config"));
              break;
            default:
              throw getFamerConfigException("Unknown task or incompatible task order\n " +
                "LINKING can only be followed by CLUSTERING, INCREMENTAL_REPAIRING or POSTPROCESSING");
            }
            break;
          case CLUSTERING:
            new ClusteringConfiguration().checkConfigAndBuildComponents(currentConfig);
            if (nextTask == Task.POSTPROCESSING) {
              new PostprocessingConfiguration()
                .checkPostprocessingConfig(nextTaskObject.getJSONArray("config"));
            } else {
              throw getFamerConfigException("Unknown task or incompatible task order:\n" +
                "CLUSTERING can only be followed by POSTPROCESSING");
            }
            break;
          case INCREMENTAL_REPAIRING:
            new IncrementalConfiguration().checkConfigAndBuildComponents(currentConfig);
            if (nextTask == Task.POSTPROCESSING) {
              new PostprocessingConfiguration().
                checkPostprocessingConfig(nextTaskObject.getJSONArray("config"));
            } else {
              throw getFamerConfigException("Unknown task or incompatible task order:\n" +
                "INCREMENTAL_REPAIRING can only be followed by POSTPROCESSING");
            }
            break;
          default:
            throw getFamerConfigException("Unknown task or incompatible task order");
          }
        } catch (Exception ex) {
          throw getFamerConfigException("Sequence " + i + " - Task " + j + ":", ex);
        }
      }
    }
  }

  /**
   * Creates a {@link FamerConfigException} for overall configuration
   *
   * @param message the detail message
   * @return The {@link FamerConfigException}
   */
  private static FamerConfigException getFamerConfigException(String message) {
    message = "FAMER configuration failed:\n" + message;
    return new FamerConfigException(message);
  }

  /**
   * Creates a {@link FamerConfigException} for overall configuration
   *
   * @param message the detail message
   * @param cause   the cause
   * @return The {@link FamerConfigException}
   */
  private static FamerConfigException getFamerConfigException(String message, Throwable cause) {
    message = "FAMER configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
