/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.postprocessing;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.core.fs.Path;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.FamerConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.postprocessing.io.flink.DataSetWriter;
import org.gradoop.famer.postprocessing.io.gradoop.GradoopGraphWriter;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.AbstractClusteringQuality;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQuality;
import org.gradoop.famer.postprocessing.quality.clusteredGraph.ClusteringQualityWithGTFile;
import org.gradoop.famer.postprocessing.quality.common.QualityComputation;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GTFileComponent;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.GoldenTruthFileType;
import org.gradoop.famer.postprocessing.quality.common.dataStructures.QualityEvalMethod;
import org.gradoop.famer.postprocessing.quality.similarityGraph.AbstractSimilarityGraphQuality;
import org.gradoop.famer.postprocessing.quality.similarityGraph.SimilarityGraphQuality;
import org.gradoop.famer.postprocessing.quality.similarityGraph.SimilarityGraphQualityWithGTFile;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.util.ArrayList;
import java.util.List;

/**
 * Class to parse the JSON configuration for post-processing like evaluation and result writing and build all
 * needed objects.
 */
public class PostprocessingConfiguration {

  /**
   * Available postprocessing tasks
   */
  private enum Task {
    /**
     * Write a Gradoop {@link LogicalGraph} or {@link GraphCollection} as CSV to disk
     */
    WRITE_GRAPH,
    /**
     * Write a Flink {@link DataSet} as CSV to disk
     */
    WRITE_DS,
    /**
     * Compute and write quality measures for a similarity graph or a clustering
     */
    QUALITY,
    /**
     * Compute and write graph statistics
     */
    GRAPH_STATISTICS
  }

  /**
   * Parses and checks the postprocessing JSON configuration object. A {@link FamerConfigException} will be
   * thrown for any inconsistency. The {@link FamerConfiguration.DataType} to run the postprocessing on
   * is set with the default value {@code DataType.LOGICAL_GRAPH}.
   *
   * @param postprocessingJsonConfig The json configuration object for linking
   */
  public void checkPostprocessingConfig(JSONArray postprocessingJsonConfig) throws JSONException {
    checkPostprocessingConfig(postprocessingJsonConfig, FamerConfiguration.DataType.LOGICAL_GRAPH);
  }

  /**
   * Parses and checks the postprocessing JSON configuration object. A {@link FamerConfigException} will be
   * thrown for any inconsistency.
   *
   * @param postprocessingTasks The json configuration object for postprocessing
   * @param dataType            {@link FamerConfiguration.DataType} to run the postprocessing on
   */
  public void checkPostprocessingConfig(JSONArray postprocessingTasks, FamerConfiguration.DataType dataType)
    throws JSONException {
    // check array content
    if ((postprocessingTasks == null) || (postprocessingTasks.length() == 0)) {
      throw getPostprocessingConfigException("Could not find any postprocessing tasks");
    }
    // validate single tasks
    for (int i = 0; i < postprocessingTasks.length(); i++) {
      JSONObject taskObject = postprocessingTasks.getJSONObject(i);
      Task task;
      try {
        task = Task.valueOf(taskObject.getString("task"));
      } catch (Exception ex) {
        throw getPostprocessingConfigException("Unknown or missing postprocessing task name", ex);
      }
      switch (task) {
      case WRITE_GRAPH:
        if (!dataType.equals(FamerConfiguration.DataType.LOGICAL_GRAPH) &&
          !dataType.equals(FamerConfiguration.DataType.GRAPH_COLLECTION)) {
          throw getPostprocessingConfigException(
            "Incompatible input data type for WRITE_GRAPH: must be LOGICAL_GRAPH or GRAPH_COLLECTION");
        }
        if (taskObject.optString("path").equals("")) {
          throw getPostprocessingConfigException("Could not find or parse path value for WRITE_GRAPH");
        } else {
          validateDataPath(taskObject.getString("path"));
        }
        if (!taskObject.optString("overwrite").equals("")) {
          try {
            taskObject.getBoolean("overwrite");
          } catch (Exception ex) {
            throw getPostprocessingConfigException("Could not parse overwrite value for WRITE_GRAPH", ex);
          }
        }
        break;
      case QUALITY:
        // we expect a logical graph only
        if (!dataType.equals(FamerConfiguration.DataType.LOGICAL_GRAPH)) {
          throw getPostprocessingConfigException(
            "Incompatible input data type for QUALITY: must be LOGICAL_GRAPH");
        }
        if (taskObject.optString("path").equals("")) {
          throw getPostprocessingConfigException("Could not find path value for QUALITY");
        } else {
          validateDataPath(taskObject.getString("path"));
        }
        if (!taskObject.optString("overwrite").equals("")) {
          try {
            taskObject.getBoolean("overwrite");
          } catch (Exception ex) {
            throw getPostprocessingConfigException("Could not parse overwrite value for QUALITY", ex);
          }
        }
        // try parsing eval config object
        if (taskObject.optJSONObject("config") == null) {
          throw getPostprocessingConfigException("Could not find or parse config object for QUALITY");
        } else {
          JSONObject config = taskObject.getJSONObject("config");
          QualityEvalMethod evalMethod;
          try {
            evalMethod = QualityEvalMethod.valueOf(config.getString("evalMethod"));
          } catch (Exception ex) {
            throw getPostprocessingConfigException("Unknown or missing quality evaluation method", ex);
          }
          if (evalMethod.equals(QualityEvalMethod.SIMILARITY_GT) ||
            evalMethod.equals(QualityEvalMethod.CLUSTERING_GT)) {
            // check for golden truth file values
            try {
              validateDataPath(config.getString("gtFilePath"));
              config.getString("splitter");
              GoldenTruthFileType.valueOf(config.getString("gtFileType"));
              config.getString("entityIdProperty");
            } catch (Exception ex) {
              throw getPostprocessingConfigException("Could not find or parse one of the golden truth " +
                "related values: gtFileType, gtFilePath, splitter, entityIdProperty", ex);
            }
          } else {
            // check for golden truth id property
            try {
              config.getString("goldenTruthIdProperty");
            } catch (Exception ex) {
              throw getPostprocessingConfigException(
                "Could not find or parse goldenTruthIdProperty value for QUALITY", ex);
            }
          }
          // check for clustering related value
          if (evalMethod.equals(QualityEvalMethod.CLUSTERING) ||
            evalMethod.equals(QualityEvalMethod.CLUSTERING_GT)) {
            try {
              config.getBoolean("hasOverlap");
            } catch (Exception ex) {
              throw getPostprocessingConfigException(
                "Could not find or parse hasOverlap boolean value for clustering QUALITY", ex);
            }
          }
        }
        break;
      case WRITE_DS:
        // TODO: implement
        break;
      case GRAPH_STATISTICS:
        // TODO: implement
        break;
      default:
        break;
      }
    }
  }

  /**
   * Runs the post-processing.
   *
   * @param postprocessingTasks The json configuration object for postprocessing
   * @param inputData           The data to run the postprocessing on
   */
  public void runPostprocessing(JSONArray postprocessingTasks, Object inputData) throws JSONException {
    for (int i = 0; i < postprocessingTasks.length(); i++) {
      JSONObject taskObject = postprocessingTasks.getJSONObject(i);
      Task task = Task.valueOf(taskObject.getString("task"));
      switch (task) {
      case WRITE_GRAPH:
        boolean overwriteGraph = true;
        if (taskObject.opt("overwrite") != null) {
          overwriteGraph = taskObject.getBoolean("overwrite");
        }
        String graphPath = taskObject.getString("path");
        try {
          if (inputData instanceof LogicalGraph) {
            new GradoopGraphWriter().writeLogicalGraphAsCSV(
              (LogicalGraph) inputData, graphPath, overwriteGraph);
          } else if (inputData instanceof GraphCollection) {
            new GradoopGraphWriter().writeGraphCollectionAsCSV(
              (GraphCollection) inputData, graphPath, overwriteGraph);
          }
        } catch (Exception ex) {
          throw getPostprocessingConfigException("There was an error while writing the graph data", ex);
        }
        break;
      case QUALITY:
        LogicalGraph inputGraph = (LogicalGraph) inputData;
        // instantiate quality computation class
        QualityComputation measurer = null;
        String qualityPath = taskObject.getString("path");
        boolean overwriteQuality = true;
        if (taskObject.opt("overwrite") != null) {
          overwriteQuality = taskObject.getBoolean("overwrite");
        }
        JSONObject config = taskObject.getJSONObject("config");
        QualityEvalMethod evalMethod = QualityEvalMethod.valueOf(config.getString("evalMethod"));
        if (evalMethod.equals(QualityEvalMethod.SIMILARITY_GT) ||
          evalMethod.equals(QualityEvalMethod.CLUSTERING_GT)) {
          String gtFilePath = config.getString("gtFilePath");
          String splitter = config.getString("splitter");
          GoldenTruthFileType gtFileType = GoldenTruthFileType.valueOf(config.getString("gtFileType"));
          GTFileComponent gtFileComponent = new GTFileComponent(gtFilePath, splitter, gtFileType);
          String entityIdProperty = config.getString("entityIdProperty");
          if (evalMethod.equals(QualityEvalMethod.SIMILARITY_GT)) {
            measurer = new SimilarityGraphQualityWithGTFile(
              inputGraph.getVertices(), inputGraph.getEdges(), gtFileComponent, entityIdProperty);
          } else {
            boolean hasOverlap = config.getBoolean("hasOverlap");
            measurer = new ClusteringQualityWithGTFile(
              inputGraph.getVertices(), gtFileComponent, entityIdProperty, hasOverlap);
          }
        } else {
          String goldenTruthIdProperty = config.getString("goldenTruthIdProperty");
          if (evalMethod.equals(QualityEvalMethod.SIMILARITY)) {
            measurer = new SimilarityGraphQuality(
              inputGraph.getVertices(), inputGraph.getEdges(), goldenTruthIdProperty);
          } else {
            boolean hasOverlap = config.getBoolean("hasOverlap");
            measurer = new ClusteringQuality(
              inputGraph.getVertices(), goldenTruthIdProperty, hasOverlap);
          }
        }
        // compute, collect and write quality results
        try {
          List<Tuple2<String, String>> results = new ArrayList<>();
          measurer.computeQuality();
          if (measurer instanceof AbstractSimilarityGraphQuality) {
            AbstractSimilarityGraphQuality simQuality = (AbstractSimilarityGraphQuality) measurer;
            results.add(Tuple2.of("true-positives", String.valueOf(simQuality.getTruePositives())));
            results.add(Tuple2.of("all-positives", String.valueOf(simQuality.getAllPositives())));
            results.add(Tuple2.of("golden-truth-records", String.valueOf(simQuality.getGtRecordNo())));
            results.add(Tuple2.of("precision", String.valueOf(simQuality.computePrecision())));
            results.add(Tuple2.of("recall", String.valueOf(simQuality.computeRecall())));
            results.add(Tuple2.of("f-measure", String.valueOf(simQuality.computeFMeasure())));
          } else if (measurer instanceof AbstractClusteringQuality) {
            AbstractClusteringQuality cluQuality = (AbstractClusteringQuality) measurer;
            results.add(Tuple2.of("true-positives", String.valueOf(cluQuality.getTruePositives())));
            results.add(Tuple2.of("all-positives", String.valueOf(cluQuality.getAllPositives())));
            results.add(Tuple2.of("golden-truth-records", String.valueOf(cluQuality.getGtRecordNo())));
            results.add(Tuple2.of("precision", String.valueOf(cluQuality.computePrecision())));
            results.add(Tuple2.of("recall", String.valueOf(cluQuality.computeRecall())));
            results.add(Tuple2.of("f-measure", String.valueOf(cluQuality.computeFMeasure())));
            results.add(Tuple2.of("number-of-clusters", String.valueOf(cluQuality.getClusterNo())));
            results.add(Tuple2.of("min-cluster-size", String.valueOf(cluQuality.getMinClusterSize())));
            results.add(Tuple2.of("max-cluster-size", String.valueOf(cluQuality.getMaxClusterSize())));
            results.add(Tuple2.of("avg-cluster-size", String.valueOf(cluQuality.getAverageClusterSize())));
            results.add(Tuple2.of("number-of-singletons", String.valueOf(cluQuality.getSingletons())));
          }
          // write results
          DataSet<Tuple2<String, String>> resultSet =
            inputGraph.getConfig().getExecutionEnvironment().fromCollection(results);
          DataSetWriter<Tuple2<String, String>> dsWriter = new DataSetWriter<>(":");
          dsWriter.writeDataSetAsCSV(resultSet, qualityPath, overwriteQuality);
        } catch (Exception ex) {
          throw getPostprocessingConfigException(
            "There was an error while computing and writing the quality results", ex);
        }
        break;
      case WRITE_DS:
        // TODO: implement
        break;
      case GRAPH_STATISTICS:
        // TODO: implement
        break;
      default:
        break;
      }
    }
  }

  /**
   * Checks if the given path value could be parsed and if the path exist.
   *
   * @param path The path value
   */
  private void validateDataPath(String path) {
    try {
      Path fsPath = new Path(path);
      // For a to be written csv file, validate the parent folder path only
      if (path.endsWith(".csv")) {
        fsPath = fsPath.getParent();
      }
      FileSystem fileSystem = fsPath.getFileSystem();
      if (!fileSystem.exists(fsPath)) {
        throw getPostprocessingConfigException("The given path or directory does not exist");
      }
    } catch (Exception ex) {
      throw getPostprocessingConfigException(
        "The path value could not be found or parsed or the path does not exist", ex);
    }
  }

  /**
   * Creates a {@link FamerConfigException} for postprocessing configuration
   *
   * @param message the detail message
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getPostprocessingConfigException(String message) {
    message = "Postprocessing configuration failed:\n" + message;
    return new FamerConfigException(message);
  }

  /**
   * Creates a {@link FamerConfigException} for postprocessing configuration
   *
   * @param message the detail message
   * @param cause   the cause
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getPostprocessingConfigException(String message, Throwable cause) {
    message = "Postprocessing configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
