/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.preprocessing;


import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.core.fs.Path;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.FamerConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.preprocessing.io.benchmarks.BenchmarkSetReaderBase;
import org.gradoop.famer.preprocessing.io.benchmarks.ExtBenchmarkSetReaderBase;
import org.gradoop.famer.preprocessing.io.benchmarks.abtbuy.AbtBuyReader;
import org.gradoop.famer.preprocessing.io.benchmarks.affiliations.AffiliationsReader;
import org.gradoop.famer.preprocessing.io.benchmarks.amazon.AmazonProductsReader;
import org.gradoop.famer.preprocessing.io.benchmarks.dblp.acm.DblpAcmReader;
import org.gradoop.famer.preprocessing.io.benchmarks.dblp.scholar.DblpScholarReader;
import org.gradoop.famer.preprocessing.io.benchmarks.geographic.GeographicSettlementsReader;
import org.gradoop.famer.preprocessing.io.benchmarks.musicbrainz.MusicBrainzReader;
import org.gradoop.famer.preprocessing.io.benchmarks.ncvoters.NorthCarolinaVotersReader;
import org.gradoop.famer.preprocessing.io.common.GraphToGraphCollection;
import org.gradoop.famer.preprocessing.io.gradoop.GradoopGraphReader;
import org.gradoop.famer.preprocessing.io.properties.GraphPropertyChecker;
import org.gradoop.famer.preprocessing.io.properties.VertexPropertyTransformer;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to parse the JSON configuration for data preprocessing and build all needed objects.
 */
public class PreprocessingConfiguration {

  /**
   * Available preprocessing tasks
   */
  private enum Task {
    /**
     * Read a Gradoop {@link LogicalGraph} or {@link GraphCollection}
     */
    READ,
    /**
     * Read benchmark data
     */
    BENCHMARK,
    /**
     * Combine different {@link LogicalGraph}s into a {@link GraphCollection}
     */
    COMBINE
  }

  /**
   * Available vertex property transformation tasks
   */
  private enum TransformTask {
    /**
     * Combines an arbitrary number of vertex properties
     */
    COMBINE,
    /**
     * Renames an arbitrary number of vertex properties
     */
    RENAME
  }

  /**
   * Available benchmark data. Each enum object is initialized with its corresponding benchmark reader class
   * and provides a method to signal whether it supports the return of perfect clustering and a method to
   * return the desired benchmark data.
   */
  private enum Benchmark {
    /**
     * E-Commerce data
     */
    ABT_BUY(new AbtBuyReader()),
    /**
     * E-Commerce data
     */
    AMAZON_GOOGLE(new AmazonProductsReader()),
    /**
     * Bibliographic data
     */
    DBLP_ACM(new DblpAcmReader()),
    /**
     * Bibliographic data
     */
    DBLP_SCHOLAR(new DblpScholarReader()),
    /**
     * Affiliation data
     */
    AFFILIATIONS(new AffiliationsReader()),
    /**
     * Geographic location data
     */
    GEOGRAPHIC(new GeographicSettlementsReader()),
    /**
     * North carolina voters data
     */
    NC_VOTERS(new NorthCarolinaVotersReader()),
    /**
     * Music data
     */
    MUSICBRAINZ(new MusicBrainzReader());

    /**
     * The benchmark reader class
     */
    private final BenchmarkSetReaderBase benchmarkReader;

    /**
     * Constructor
     *
     * @param benchmarkReader The corresponding benchmark reader class
     */
    Benchmark(BenchmarkSetReaderBase benchmarkReader) {
      this.benchmarkReader = benchmarkReader;
    }

    /**
     * Returns whether a benchmark reader provides a perfect clustering (has to be of type
     * {@link ExtBenchmarkSetReaderBase})
     *
     * @return {@code true} if a benchmark reader provides a perfect clustering
     */
    public boolean providesClustering() {
      return benchmarkReader instanceof ExtBenchmarkSetReaderBase;
    }

    /**
     * Returns the benchmark data with the given return data type from the given folder path.
     *
     * @param dataType   Return type for the benchmark data
     * @param folderPath Path to the benchmark data folder
     * @return Benchmark data as perfect mapping, perfect clustering or graph collection
     */
    public Object getBenchmarkData(FamerConfiguration.DataType dataType, String folderPath) {
      switch (dataType) {
      case PERFECT_MAPPING:
        return benchmarkReader.getPerfectMapping(folderPath);
      case PERFECT_CLUSTERING:
        return ((ExtBenchmarkSetReaderBase) benchmarkReader).getPerfectClustering(folderPath);
      case GRAPH_COLLECTION:
        return benchmarkReader.getBenchmarkDataAsGraphCollection(folderPath);
      default:
        return null;
      }
    }
  }

  /**
   * The consistent json config for preprocessing
   */
  private JSONObject checkedJsonConfig;
  /**
   * The preprocessing task
   */
  private Task task;
  /**
   * Reader for Gradoop graph format
   */
  private GradoopGraphReader gradoopGraphReader;
  /**
   * The benchmark object
   */
  private Benchmark benchmark;
  /**
   * The data type to return after preprocessing
   */
  private FamerConfiguration.DataType dataType;
  /**
   * The optional object for vertex property transformation
   */
  private JSONObject transformProperty;
  /**
   * The task for vertex property transformation
   */
  private TransformTask transformTask;
  /**
   * The config for the vertex property transformation task
   */
  private JSONArray transformConfig;

  /**
   * Parses and checks the preprocessing JSON configuration file and tries to build the preprocessing
   * components. A {@link FamerConfigException} will be thrown for any inconsistency.
   *
   * @param preprocessingConfigPath Path to the json configuration file for preprocessing
   */
  public void checkConfigAndBuildComponents(String preprocessingConfigPath) {
    JSONObject preprocessingJsonConfig;
    try {
      String jsonString =
        new String(Files.readAllBytes(Paths.get(preprocessingConfigPath)), StandardCharsets.UTF_8);
      preprocessingJsonConfig = new JSONObject(jsonString);
    } catch (Exception ex) {
      throw getPreprocessingConfigException("Could not parse JSON configuration file.", ex);
    }
    checkConfigAndBuildComponents(preprocessingJsonConfig);
  }

  /**
   * Parses and checks the preprocessing JSON configuration object. A {@link FamerConfigException} will be
   * thrown for any inconsistency.
   *
   * @param preprocessingJsonConfig The json configuration object for preprocessing
   */
  public void checkConfigAndBuildComponents(JSONObject preprocessingJsonConfig) {
    gradoopGraphReader = new GradoopGraphReader();
    // check task value
    try {
      task = Task.valueOf(preprocessingJsonConfig.getString("task"));
    } catch (Exception ex) {
      throw getPreprocessingConfigException("Unknown or missing preprocessing task name", ex);
    }
    // check return type value
    try {
      dataType = FamerConfiguration.DataType.valueOf(preprocessingJsonConfig.getString("return"));
    } catch (Exception ex) {
      throw getPreprocessingConfigException("Unknown or missing preprocessing return type", ex);
    }
    // check task config
    switch (task) {
    case READ:
      validateDataPath(preprocessingJsonConfig.optString("path"));
      try {
        if ((dataType.equals(FamerConfiguration.DataType.LOGICAL_GRAPH)) &&
          (preprocessingJsonConfig.optJSONObject("checkGraphLabel") != null)) {
          JSONObject checkGraphLabel = preprocessingJsonConfig.getJSONObject("checkGraphLabel");
          checkGraphLabel.getString("valueIfNotExists");
          checkGraphLabel.getBoolean("overwrite");
        }
      } catch (Exception ex) {
        throw getPreprocessingConfigException("Could not parse the checkGraphLabel element", ex);
      }
      break;
    case BENCHMARK:
      try {
        benchmark = Benchmark.valueOf(preprocessingJsonConfig.getString("name"));
      } catch (Exception ex) {
        throw getPreprocessingConfigException("Unknown or missing benchmark name", ex);
      }
      validateDataPath(preprocessingJsonConfig.optString("path"));
      if (dataType.equals(FamerConfiguration.DataType.PERFECT_CLUSTERING) &&
        !benchmark.providesClustering()) {
        throw getPreprocessingConfigException(
          "Incompatible return type: " + "PERFECT_CLUSTERING is not provided for this benchmark set");
      }
      break;
    case COMBINE:
      JSONArray graphs = preprocessingJsonConfig.optJSONArray("graphs");
      if ((graphs == null) || (graphs.length() == 0)) {
        throw getPreprocessingConfigException("Could not find any graphs to combine.");
      }

      for (int i = 0; i < graphs.length(); i++) {
        JSONObject graphObject = graphs.optJSONObject(i);
        validateDataPath(graphObject.optString("path"));
        try {
          if (graphObject.getJSONObject("checkGraphLabel") != null) {
            JSONObject checkGraphLabel = graphObject.getJSONObject("checkGraphLabel");
            checkGraphLabel.getString("valueIfNotExists");
            checkGraphLabel.getBoolean("overwrite");
          }
        } catch (Exception ex) {
          throw getPreprocessingConfigException("Could not parse the checkGraphLabel element", ex);
        }
      }
      break;
    default:
      break;
    }
    // check and build optional transform config
    try {
      if ((dataType.equals(FamerConfiguration.DataType.LOGICAL_GRAPH) ||
        dataType.equals(FamerConfiguration.DataType.GRAPH_COLLECTION)) &&
        (preprocessingJsonConfig.optJSONObject("transformProperty") != null)) {
        transformProperty = preprocessingJsonConfig.getJSONObject("transformProperty");
        transformTask = TransformTask.valueOf(transformProperty.getString("task"));
        transformConfig = parseTransformPropertyConfig();

        if (transformTask.equals(TransformTask.COMBINE)) {
          for (int i = 0; i < transformConfig.length(); i++) {
            JSONObject configPart = transformConfig.getJSONObject(i);
            configPart.getString("graphLabel");
            configPart.getString("combined");
            JSONArray properties = configPart.getJSONArray("properties");
            for (int j = 0; j < properties.length(); j++) {
              JSONObject property = properties.getJSONObject(j);
              property.getString("key");
              property.getBoolean("keep");
            }
          }
        } else if (transformTask.equals(TransformTask.RENAME)) {
          for (int i = 0; i < transformConfig.length(); i++) {
            JSONObject configPart = transformConfig.getJSONObject(i);
            configPart.getString("graphLabel");
            configPart.getString("key");
            configPart.getString("renamed");
            configPart.getBoolean("keep");
          }
        }
      }
    } catch (Exception ex) {
      throw getPreprocessingConfigException("Could not parse the transformProperty element", ex);
    }
    checkedJsonConfig = preprocessingJsonConfig;
  }

  /**
   * Runs the preprocessing tasks and returns the data.
   *
   * @return Data
   */
  public Object runPreprocessing() {
    if ((task == null) || (dataType == null) || (gradoopGraphReader == null)) {
      throw getPreprocessingConfigException(
        "No preprocessing components has been created yet." + "\nRun checkConfigAndBuildComponents() first.");
    }

    switch (task) {
    case READ:
      String graphPath = checkedJsonConfig.optString("path");

      if (dataType.equals(FamerConfiguration.DataType.LOGICAL_GRAPH)) {
        LogicalGraph graph = gradoopGraphReader.readLogicalGraphFromCSV(graphPath);
        if (checkedJsonConfig.optJSONObject("checkGraphLabel") != null) {
          JSONObject checkGraphLabel = checkedJsonConfig.optJSONObject("checkGraphLabel");
          String valueIfNotExists = checkGraphLabel.optString("valueIfNotExists");
          boolean overwrite = checkGraphLabel.optBoolean("overwrite");
          graph = new GraphPropertyChecker()
            .checkLogicalGraphForGraphLabelProperty(graph, valueIfNotExists, overwrite);
        }
        if (transformProperty != null) {
          if (transformTask.equals(TransformTask.COMBINE)) {
            graph = new VertexPropertyTransformer().combineProperties(graph, getConfigForPropCombination());
          } else if (transformTask.equals(TransformTask.RENAME)) {
            graph = new VertexPropertyTransformer().renameProperties(graph, getConfigForPropRenaming());
          }
        }
        return graph;
      } else if (dataType.equals(FamerConfiguration.DataType.GRAPH_COLLECTION)) {
        GraphCollection graphs = gradoopGraphReader.readGraphCollectionFromCSV(graphPath);
        if (transformProperty != null) {
          if (transformTask.equals(TransformTask.COMBINE)) {
            graphs = new VertexPropertyTransformer().combineProperties(graphs, getConfigForPropCombination());
          } else if (transformTask.equals(TransformTask.RENAME)) {
            graphs = new VertexPropertyTransformer().renameProperties(graphs, getConfigForPropRenaming());
          }
        }
        return graphs;
      }
      break;
    case BENCHMARK:
      String benchmarkPath = checkedJsonConfig.optString("path");
      return benchmark.getBenchmarkData(dataType, benchmarkPath);
    case COMBINE:
      List<LogicalGraph> graphsList = new ArrayList<>();
      JSONArray graphsArray = checkedJsonConfig.optJSONArray("graphs");
      for (int i = 0; i < graphsArray.length(); i++) {
        JSONObject graphObject = graphsArray.optJSONObject(i);

        String graphPathString = graphObject.optString("path");
        LogicalGraph logicalGraph = gradoopGraphReader.readLogicalGraphFromCSV(graphPathString);
        if (graphObject.optJSONObject("checkGraphLabel") != null) {
          JSONObject checkGraphLabel = graphObject.optJSONObject("checkGraphLabel");
          String valueIfNotExists = checkGraphLabel.optString("valueIfNotExists");
          boolean overwrite = checkGraphLabel.optBoolean("overwrite");
          logicalGraph = new GraphPropertyChecker()
            .checkLogicalGraphForGraphLabelProperty(logicalGraph, valueIfNotExists, overwrite);
        }
        graphsList.add(logicalGraph);
      }
      GraphCollection graphs = GraphToGraphCollection.execute(graphsList);
      if (transformProperty != null) {
        if (transformTask.equals(TransformTask.COMBINE)) {
          graphs = new VertexPropertyTransformer().combineProperties(graphs, getConfigForPropCombination());
        } else if (transformTask.equals(TransformTask.RENAME)) {
          graphs = new VertexPropertyTransformer().renameProperties(graphs, getConfigForPropRenaming());
        }
      }
      return graphs;
    default:
      break;
    }
    return null;
  }

  /**
   * Checks if the given path value could be parsed and if the path exist
   *
   * @param path The string path value
   */
  private void validateDataPath(String path) {
    try {
      Path fsPath = new Path(path);
      FileSystem fileSystem = fsPath.getFileSystem();
      if (!fileSystem.exists(fsPath)) {
        throw getPreprocessingConfigException("The given path or directory does not exist");
      }
    } catch (Exception ex) {
      throw getPreprocessingConfigException(
        "The path value could not be found or parsed or the path does " + "not exist", ex);
    }
  }

  /**
   * Parses the property transformation config from either the JSON array or the given path to the config
   * file
   *
   * @return {@link JSONArray} with the transformation config
   */
  private JSONArray parseTransformPropertyConfig() {
    try {
      JSONArray configElement = transformProperty.optJSONArray("config");
      if (configElement != null) {
        return configElement;
      } else if (!transformProperty.optString("config").equals("")) {
        String configPath = transformProperty.getString("config");
        String jsonString = new String(Files.readAllBytes(Paths.get(configPath)), StandardCharsets.UTF_8);
        return new JSONArray(jsonString);
      } else {
        throw getPreprocessingConfigException("Unknown format of property transformation config.\n" +
          "Should be JSON array or path to JSON file");
      }
    } catch (Exception ex) {
      throw getPreprocessingConfigException("Could not parse property transformation config.", ex);
    }
  }

  /**
   * Create the tuples for vertex property combination
   *
   * @return A list of the tuples for the property combination
   */
  private List<Tuple3<String, String, List<Tuple2<String, Boolean>>>> getConfigForPropCombination() {
    List<Tuple3<String, String, List<Tuple2<String, Boolean>>>> propertyTuples = new ArrayList<>();
    for (int i = 0; i < transformConfig.length(); i++) {
      JSONObject configPart = transformConfig.optJSONObject(i);
      Tuple3<String, String, List<Tuple2<String, Boolean>>> configTuple = new Tuple3<>();
      configTuple.f0 = configPart.optString("graphLabel");
      configTuple.f1 = configPart.optString("combined");
      configTuple.f2 = new ArrayList<>();

      JSONArray properties = configPart.optJSONArray("properties");
      for (int j = 0; j < properties.length(); j++) {
        JSONObject property = properties.optJSONObject(j);

        Tuple2<String, Boolean> propTuple = new Tuple2<>();
        propTuple.f0 = property.optString("key");
        propTuple.f1 = property.optBoolean("keep");
        configTuple.f2.add(propTuple);
      }
      propertyTuples.add(configTuple);
    }
    return propertyTuples;
  }

  /**
   * Create the tuples for vertex property renaming
   *
   * @return A list of the tuples for the property renaming
   */
  private List<Tuple4<String, String, String, Boolean>> getConfigForPropRenaming() {
    List<Tuple4<String, String, String, Boolean>> propertyTuples = new ArrayList<>();
    for (int i = 0; i < transformConfig.length(); i++) {
      JSONObject configPart = transformConfig.optJSONObject(i);
      Tuple4<String, String, String, Boolean> configTuple = new Tuple4<>();
      configTuple.f0 = configPart.optString("graphLabel");
      configTuple.f1 = configPart.optString("key");
      configTuple.f2 = configPart.optString("renamed");
      configTuple.f3 = configPart.optBoolean("keep");
      propertyTuples.add(configTuple);
    }
    return propertyTuples;
  }

  /**
   * Creates a {@link FamerConfigException} for preprocessing configuration
   *
   * @param message the detail message
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getPreprocessingConfigException(String message) {
    message = "Preprocessing configuration failed:\n" + message;
    return new FamerConfigException(message);
  }

  /**
   * Creates a {@link FamerConfigException} for preprocessing configuration
   *
   * @param message the detail message
   * @param cause   the cause
   * @return The {@link FamerConfigException}
   */
  private FamerConfigException getPreprocessingConfigException(String message, Throwable cause) {
    message = "Preprocessing configuration failed:\n" + message;
    return new FamerConfigException(message, cause);
  }
}
