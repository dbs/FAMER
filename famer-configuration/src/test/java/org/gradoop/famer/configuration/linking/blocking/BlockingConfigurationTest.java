/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.linking.blocking;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.linking.blocking.keyGeneration.dataStructures.*;
import org.gradoop.famer.linking.blocking.methods.dataStructures.*;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * JUnit tests for {@link BlockingConfiguration}.
 */
public class BlockingConfigurationTest extends GradoopFlinkTestBase {

  /**
   * Test the correct construction of graph pairs and category pairs from json.
   */
  @Test
  public void testBuildPair() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[],\n" +
      "   \"similarityComponents\":[\n" +
      "      {\n" +
      "         \"id\":\"camera_brand1\",\n" +
      "         \"sourceGraph\":\"b\",\n" +
      "         \"targetGraph\":\"a\",\n" +
      "         \"sourceLabel\":\"bla\",\n" +
      "         \"targetLabel\":\"foo\",\n" +
      "         \"sourceAttribute\":\"brand\",\n" +
      "         \"targetAttribute\":\"brand\",\n" +
      "         \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "         \"weight\":\"1\"\n" +
      "      },\n" +
      "      {\n" +
      "         \"id\":\"camera_brand2\",\n" +
      "         \"sourceGraph\":\"b\",\n" +
      "         \"targetGraph\":\"c\",\n" +
      "         \"sourceLabel\":\"foo\",\n" +
      "         \"targetLabel\":\"bar\",\n" +
      "         \"sourceAttribute\":\"brand\",\n" +
      "         \"targetAttribute\":\"brand\",\n" +
      "         \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "         \"weight\":\"1\"\n" +
      "      },\n" +
      "      {\n" +
      "         \"id\":\"camera_brand3\",\n" +
      "         \"sourceGraph\":\"d\",\n" +
      "         \"targetGraph\":\"c\",\n" +
      "         \"sourceLabel\":\"bar\",\n" +
      "         \"targetLabel\":\"foo\",\n" +
      "         \"sourceAttribute\":\"brand\",\n" +
      "         \"targetAttribute\":\"brand\",\n" +
      "         \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "         \"weight\":\"1\"\n" +
      "      }\n" +
      "   ]\n" +
      "}\n";

    JSONObject object = new JSONObject(json);

    BlockingConfiguration config = new BlockingConfiguration();

    Map<String, Set<String>> graphPairs = config.buildPairs(object, "sourceGraph", "targetGraph");
    Map<String, Set<String>> categoryPairs = config.buildPairs(object, "sourceLabel", "targetLabel");

    Map<String, Set<String>> expectedGraphPairs = new HashMap<>();

    Set<String> expectedGraphValuesB = new HashSet<>();
    expectedGraphValuesB.add("a");
    expectedGraphValuesB.add("c");
    expectedGraphPairs.put("b", expectedGraphValuesB);

    Set<String> expectedGraphValuesD = new HashSet<>();
    expectedGraphValuesD.add("c");
    expectedGraphPairs.put("d", expectedGraphValuesD);

    Map<String, Set<String>> expectedCategoryPairs = new HashMap<>();

    Set<String> expectedCategoryValuesBar = new HashSet<>();
    expectedCategoryValuesBar.add("foo");
    expectedCategoryPairs.put("bar", expectedCategoryValuesBar);

    Set<String> expectedCategoryValuesBla = new HashSet<>();
    expectedCategoryValuesBla.add("foo");
    expectedCategoryPairs.put("bla", expectedCategoryValuesBla);

    Set<String> expectedCategoryValuesFoo = new HashSet<>();
    expectedCategoryValuesFoo.add("bar");
    expectedCategoryPairs.put("foo", expectedCategoryValuesFoo);

    assertEquals(expectedGraphPairs, graphPairs);
    assertEquals(expectedCategoryPairs, categoryPairs);
  }

  @Test
  public void testBuildKeyGeneratorComponentForFullAttribute() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "   \"attribute\":\"category\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    KeyGeneratorComponent keyGeneratorComponent =
      new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);

    assertTrue(keyGeneratorComponent instanceof FullAttributeComponent);
    assertEquals("category", keyGeneratorComponent.getAttribute());
  }

  /**
   * Missing attribute needs to be tested only once
   */
  @Test(expected = FamerConfigException.class)
  public void testBuildKeyGeneratorComponentForFullAttributeWithMissingAttribute() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"FullAttributeComponent\"" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);
  }

  @Test
  public void testBuildKeyGeneratorComponentForPrefixLength() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"PrefixLengthComponent\",\n" +
      "   \"attribute\":\"category\",\n" +
      "   \"prefixLength\":\"5\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    KeyGeneratorComponent keyGeneratorComponent =
      new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);

    assertTrue(keyGeneratorComponent instanceof PrefixLengthComponent);
    assertEquals("category", keyGeneratorComponent.getAttribute());
    assertEquals(5, ((PrefixLengthComponent) keyGeneratorComponent).getPrefixLength());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildKeyGeneratorComponentForPrefixLengthWithMissingPrefixLength() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"PrefixLengthComponent\",\n" +
      "   \"attribute\":\"category\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildKeyGeneratorComponentForPrefixLengthWithWrongPrefixLength() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"PrefixLengthComponent\",\n" +
      "   \"attribute\":\"category\",\n" +
      "   \"prefixLength\":\"a\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);
  }

  @Test
  public void testBuildKeyGeneratorComponentForQGrams() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"QGramsComponent\",\n" +
      "   \"attribute\":\"category\",\n" +
      "   \"qgramNo\":\"2\",\n" +
      "   \"qgramThreshold\":\"0.5\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    KeyGeneratorComponent keyGeneratorComponent =
      new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);

    assertTrue(keyGeneratorComponent instanceof QGramsComponent);
    assertEquals("category", keyGeneratorComponent.getAttribute());
    assertEquals(2, ((QGramsComponent) keyGeneratorComponent).getQ());
    assertEquals(0.5, ((QGramsComponent) keyGeneratorComponent).getThreshold(), 0d);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildKeyGeneratorComponentForQGramsWithMissingQGramNo() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"QGramsComponent\",\n" +
      "   \"attribute\":\"category\",\n" +
      "   \"qgramThreshold\":\"0.5\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildKeyGeneratorComponentForQGramsWithWrongQGramNo() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"QGramsComponent\",\n" +
      "   \"attribute\":\"category\",\n" +
      "   \"qgramNo\":\"a\",\n" +
      "   \"qgramThreshold\":\"0.5\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildKeyGeneratorComponentForQGramsWithMissingQGramThreshold() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"QGramsComponent\",\n" +
      "   \"attribute\":\"category\",\n" +
      "   \"qgramNo\":\"2\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildKeyGeneratorComponentForQGramsWithWrongQGramThreshold() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"QGramsComponent\",\n" +
      "   \"attribute\":\"category\",\n" +
      "   \"qgramNo\":\"2\",\n" +
      "   \"qgramThreshold\":\"a\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);
  }

  @Test
  public void testBuildKeyGeneratorComponentForWordTokenizer() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"WordTokenizerComponent\",\n" +
      "   \"attribute\":\"category\",\n" +
      "   \"tokenizer\":\",\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    KeyGeneratorComponent keyGeneratorComponent =
      new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);

    assertTrue(keyGeneratorComponent instanceof WordTokenizerComponent);
    assertEquals("category", keyGeneratorComponent.getAttribute());
    assertEquals(",", ((WordTokenizerComponent) keyGeneratorComponent).getTokenizer());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildKeyGeneratorComponentForWordTokenizerWithMissingTokenizer() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"WordTokenizerComponent\",\n" +
      "   \"attribute\":\"category\"\n" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildKeyGeneratorComponentWithUnknownMethod() throws JSONException {
    String json = "{\n" +
      "   \"keyGenerationMethod\":\"A\"" +
      "}";
    JSONObject keyGenCompJson = new JSONObject(json);

    new BlockingConfiguration().buildKeyGenerationComponent(keyGenCompJson);
  }

  @Test
  public void testBuildBlockingComponentForCartesianProduct() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"CartesianProductComponent\",\n" +
      "       \"keyGenerationComponent\":{\n" +
      "           \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "           \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "      {\n" +
      "         \"id\":\"camera_brand\",\n" +
      "         \"sourceGraph\":\"*\",\n" +
      "         \"targetGraph\":\"*\",\n" +
      "         \"sourceLabel\":\"*\",\n" +
      "         \"targetLabel\":\"*\",\n" +
      "         \"sourceAttribute\":\"brand\",\n" +
      "         \"targetAttribute\":\"brand\",\n" +
      "         \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "         \"weight\":\"1\"\n" +
      "      }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    List<BlockingComponent> blockingComponents =
      new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson).getBlockingComponents();

    assertNotNull(blockingComponents);
    assertFalse(blockingComponents.isEmpty());
    assertTrue(blockingComponents.get(0) instanceof CartesianProductComponent);
    assertNotNull(blockingComponents.get(0).getGraphPairs());
    assertFalse(blockingComponents.get(0).getGraphPairs().isEmpty());
    assertNotNull(blockingComponents.get(0).getCategoryPairs());
    assertFalse(blockingComponents.get(0).getCategoryPairs().isEmpty());
  }

  @Test
  public void testBuildBlockingComponentForSortedNeighborhood() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"SortedNeighborhoodComponent\",\n" +
      "       \"windowSize\":\"20\",\n" +
      "       \"parallelismDegree\":\"96\", \n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    List<BlockingComponent> blockingComponents =
      new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson).getBlockingComponents();

    assertNotNull(blockingComponents);
    assertFalse(blockingComponents.isEmpty());
    assertTrue(blockingComponents.get(0) instanceof SortedNeighborhoodComponent);
    assertNotNull(blockingComponents.get(0).getGraphPairs());
    assertFalse(blockingComponents.get(0).getGraphPairs().isEmpty());
    assertNotNull(blockingComponents.get(0).getCategoryPairs());
    assertFalse(blockingComponents.get(0).getCategoryPairs().isEmpty());
    assertEquals(96, ((SortedNeighborhoodComponent) blockingComponents.get(0)).getParallelismDegree());
    assertEquals(20, ((SortedNeighborhoodComponent) blockingComponents.get(0)).getWindowSize());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildBlockingComponentForSortedNeighborhoodWithMissingWindowSize() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"SortedNeighborhoodComponent\",\n" +
      "       \"parallelismDegree\":\"96\", \n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildBlockingComponentForSortedNeighborhoodWithWrongWindowSize() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"SortedNeighborhoodComponent\",\n" +
      "       \"windowSize\":\"a\",\n" +
      "       \"parallelismDegree\":\"96\", \n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildBlockingComponentForSortedNeighborhoodWithMissingParallelism() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"SortedNeighborhoodComponent\",\n" +
      "       \"windowSize\":\"20\",\n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildBlockingComponentForSortedNeighborhoodWithWrongParallelism() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"SortedNeighborhoodComponent\",\n" +
      "       \"windowSize\":\"20\",\n" +
      "       \"parallelismDegree\":\"a\", \n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson);
  }

  @Test
  public void testBuildBlockingComponentForStandardBlocking() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"StandardBlockingComponent\",\n" +
      "       \"parallelismDegree\":\"96\", \n" +
      "       \"emptyKeyStrategy\":\"EMPTY_BLOCK\",\n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    List<BlockingComponent> blockingComponents =
      new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson).getBlockingComponents();

    assertNotNull(blockingComponents);
    assertFalse(blockingComponents.isEmpty());
    assertTrue(blockingComponents.get(0) instanceof StandardBlockingComponent);
    assertNotNull(blockingComponents.get(0).getGraphPairs());
    assertFalse(blockingComponents.get(0).getGraphPairs().isEmpty());
    assertNotNull(blockingComponents.get(0).getCategoryPairs());
    assertFalse(blockingComponents.get(0).getCategoryPairs().isEmpty());
    assertEquals(96, ((StandardBlockingComponent) blockingComponents.get(0)).getParallelismDegree());
    assertEquals(StandardBlockingEmptyKeyStrategy.EMPTY_BLOCK,
      ((StandardBlockingComponent) blockingComponents.get(0)).getEmptyKeyStrategy());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildBlockingComponentForStandardBlockingWithMissingParallelism() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"StandardBlockingComponent\",\n" +
      "       \"emptyKeyStrategy\":\"EMPTY_BLOCK\",\n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildBlockingComponentForStandardBlockingWithWrongParallelism() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"StandardBlockingComponent\",\n" +
      "       \"parallelismDegree\":\"a\", \n" +
      "       \"emptyKeyStrategy\":\"EMPTY_BLOCK\",\n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson);
  }

  @Test
  public void testBuildBlockingComponentForStandardBlockingWithMissingStrategy() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"StandardBlockingComponent\",\n" +
      "       \"parallelismDegree\":\"96\", \n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    List<BlockingComponent> blockingComponents =
      new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson).getBlockingComponents();

    assertNotNull(blockingComponents);
    assertFalse(blockingComponents.isEmpty());
    assertTrue(blockingComponents.get(0) instanceof StandardBlockingComponent);
    assertNotNull(blockingComponents.get(0).getGraphPairs());
    assertFalse(blockingComponents.get(0).getGraphPairs().isEmpty());
    assertNotNull(blockingComponents.get(0).getCategoryPairs());
    assertFalse(blockingComponents.get(0).getCategoryPairs().isEmpty());
    assertEquals(96, ((StandardBlockingComponent) blockingComponents.get(0)).getParallelismDegree());
    assertEquals(StandardBlockingEmptyKeyStrategy.EMPTY_BLOCK,
      ((StandardBlockingComponent) blockingComponents.get(0)).getEmptyKeyStrategy());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildBlockingComponentForStandardBlockingWithWrongStrategy() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"StandardBlockingComponent\",\n" +
      "       \"parallelismDegree\":\"96\", \n" +
      "       \"emptyKeyStrategy\":\"EMPTY\",\n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildBlockingComponentForUnknownMethod() throws JSONException {
    String json = "{\n" +
      "   \"blockingComponents\":[\n" +
      "     {\n" +
      "       \"blockingMethod\":\"A\",\n" +
      "       \"keyGenerationComponent\":{\n" +
      "         \"keyGenerationMethod\":\"FullAttributeComponent\",\n" +
      "         \"attribute\":\"brand\"\n" +
      "        }\n" +
      "     }\n" +
      "   ],\n" +
      "   \"similarityComponents\":[\n" +
      "     {\n" +
      "       \"id\":\"camera_brand\",\n" +
      "       \"sourceGraph\":\"*\",\n" +
      "       \"targetGraph\":\"*\",\n" +
      "       \"sourceLabel\":\"*\",\n" +
      "       \"targetLabel\":\"*\",\n" +
      "       \"sourceAttribute\":\"brand\",\n" +
      "       \"targetAttribute\":\"brand\",\n" +
      "       \"similarityMethod\":\"EditDistanceComponent\",\n" +
      "       \"weight\":\"1\"\n" +
      "     }\n" +
      "   ]\n" +
      "}";
    JSONObject blockingCompJson = new JSONObject(json);

    new BlockingConfiguration().checkConfigAndBuildComponents(blockingCompJson);
  }
}
