/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.preprocessing;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.id.GradoopId;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;

import static org.gradoop.famer.preprocessing.io.properties.GraphPropertyChecker.GRAPH_LABEL_PROPERTY;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * JUnit test for {@link PreprocessingConfiguration}
 */
public class PreprocessingConfigurationTest extends GradoopFlinkTestBase {

  @Test
  public void testRunPreprocessingForReadLogicalGraph() throws JSONException {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof LogicalGraph);
  }

  @Test
  public void testRunPreprocessingForReadLogicalGraphAndCheckGraphLabelProperty() throws Exception {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"checkGraphLabel\":{\n" +
      "  \"valueIfNotExists\":\"source1\",\n" +
      "  \"overwrite\":\"true\"\n" +
      "}\n" +
      "}";

    JSONObject configJson = new JSONObject(preConfig);
    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof LogicalGraph);
    ((LogicalGraph) inputData).getVertices().collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty(GRAPH_LABEL_PROPERTY));
      assertEquals("source1", vertex.getPropertyValue(GRAPH_LABEL_PROPERTY).getString());
    });
  }

  @Test
  public void testRunPreprocessingForReadGraphCollection() throws Exception {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_graph_collection_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"path\":\"" + path + "\"\n" +
      "}";

    JSONObject configJson = new JSONObject(preConfig);
    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof GraphCollection);
    assertEquals(2L, ((GraphCollection) inputData).getGraphHeads().count());
  }

  @Test
  public void testRunPreprocessingForCombine() throws Exception {
    String path1 = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();
    String path2 = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_2").getPath();

    String preConfig = "{\n" +
      "\"task\":\"COMBINE\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"graphs\":[\n" +
      "  {\n" +
      "    \"path\":\"" + path1 + "\",\n" +
      "    \"checkGraphLabel\":{\n" +
      "      \"valueIfNotExists\":\"source1\",\n" +
      "      \"overwrite\":\"true\"\n" +
      "    }\n" +
      "  },\n" +
      "  {\n" +
      "    \"path\":\"" + path2 + "\",\n" +
      "    \"checkGraphLabel\":{\n" +
      "      \"valueIfNotExists\":\"source2\",\n" +
      "      \"overwrite\":\"true\"\n" +
      "    }\n" +
      "  }\n" +
      "]\n" +
      "}";

    JSONObject configJson = new JSONObject(preConfig);
    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof GraphCollection);
    assertEquals(2L, ((GraphCollection) inputData).getGraphHeads().count());

    ((GraphCollection) inputData).getVertices().collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty(GRAPH_LABEL_PROPERTY));
      if (vertex.getGraphIds().contains(GradoopId.fromString("000000000000000000000001"))) {
        assertEquals("source1", vertex.getPropertyValue(GRAPH_LABEL_PROPERTY).getString());
      } else if (vertex.getGraphIds().contains(GradoopId.fromString("000000000000000000000002"))) {
        assertEquals("source2", vertex.getPropertyValue(GRAPH_LABEL_PROPERTY).getString());
      }
    });
  }

  @Test
  public void testRunPreprocessingWithPropertyRenamingAsJSON() throws Exception {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"transformProperty\":{\n" +
      "  \"task\":\"RENAME\",\n" +
      "  \"config\":[\n" +
      "    {\n" +
      "      \"graphLabel\":\"\",\n" +
      "      \"key\":\"a\",\n" +
      "      \"renamed\":\"new\",\n" +
      "      \"keep\":\"true\"\n" +
      "    }\n" +
      "  ]\n" +
      "}\n" +
      "}";

    JSONObject configJson = new JSONObject(preConfig);
    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof LogicalGraph);

    ((LogicalGraph) inputData).getVertices().collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty("new"));
      assertTrue(vertex.hasProperty("a"));
    });
  }

  @Test
  public void testRunPreprocessingWithPropertyRenamingAsFile() throws Exception {
    String transformConfig = "[\n" +
      "{\n" +
      "  \"graphLabel\":\"\",\n" +
      "  \"key\":\"a\",\n" +
      "  \"renamed\":\"new\",\n" +
      "  \"keep\":\"true\"\n" +
      "}\n" +
      "]";
    File tempFile = File.createTempFile("transformConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(transformConfig);
    bw.close();

    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"transformProperty\":{\n" +
      "  \"task\":\"RENAME\",\n" +
      "  \"config\":\"" + tempFile.getPath().replace("\\", "\\\\") + "\"\n" +
      "  }\n" +
      "}";

    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof LogicalGraph);

    ((LogicalGraph) inputData).getVertices().collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty("new"));
      assertTrue(vertex.hasProperty("a"));
    });
  }

  @Test
  public void testRunPreprocessingWithPropertyCombiningAsJSON() throws Exception {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"transformProperty\":{\n" +
      "  \"task\":\"COMBINE\",\n" +
      "  \"config\":[\n" +
      "    {\n" +
      "      \"graphLabel\":\"\",\n" +
      "      \"combined\":\"ab\",\n" +
      "      \"properties\":[\n" +
      "        {\n" +
      "          \"key\":\"a\",\n" +
      "          \"keep\":\"true\"\n" +
      "        },\n" +
      "        {\n" +
      "          \"key\":\"b\",\n" +
      "          \"keep\":\"true\"\n" +
      "        }\n" +
      "      ]\n" +
      "    }\n" +
      "  ]\n" +
      "}\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof LogicalGraph);

    ((LogicalGraph) inputData).getVertices().collect().forEach(vertex -> {
      if (vertex.hasProperty("a") && vertex.hasProperty("b")) {
        assertTrue(vertex.hasProperty("ab"));
      }
    });
  }

  @Test
  public void testRunPreprocessingWithPropertyCombiningAsFile() throws Exception {
    String transformConfig = "[\n" +
      "  {\n" +
      "    \"graphLabel\":\"\",\n" +
      "    \"combined\":\"ab\",\n" +
      "    \"properties\":[\n" +
      "      {\n" +
      "        \"key\":\"a\",\n" +
      "        \"keep\":\"true\"\n" +
      "      },\n" +
      "      {\n" +
      "        \"key\":\"b\",\n" +
      "        \"keep\":\"true\"\n" +
      "      }\n" +
      "    ]\n" +
      "  }\n" +
      "]";
    File tempFile = File.createTempFile("transformConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(transformConfig);
    bw.close();

    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"transformProperty\":{\n" +
      "  \"task\":\"COMBINE\",\n" +
      "  \"config\":\"" + tempFile.getPath().replace("\\", "\\\\") + "\"\n" +
      "}\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
    Object inputData = preprocessingConfiguration.runPreprocessing();

    assertNotNull(inputData);
    assertTrue(inputData instanceof LogicalGraph);

    ((LogicalGraph) inputData).getVertices().collect().forEach(vertex -> {
      if (vertex.hasProperty("a") && vertex.hasProperty("b")) {
        assertTrue(vertex.hasProperty("ab"));
      }
    });
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPreprocessingConfigurationWithMissingTask() throws JSONException {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    new PreprocessingConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPreprocessingConfigurationWithMissingReturnType() throws JSONException {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"path\":\"" + path + "\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    new PreprocessingConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPreprocessingConfigurationWithMissingPath() throws JSONException {
    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    new PreprocessingConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPreprocessingConfigurationWithInvalidPath() throws JSONException {
    String path = getClass().getClassLoader().getResource("gradoop/csv/").getPath() + "unknown";

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\"\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    new PreprocessingConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPreprocessingConfigurationWithInvalidCheckGraphLabelElement1() throws JSONException {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"checkGraphLabel\":{\n" +
      "  \"overwrite\":\"true\"\n" +
      "}\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    new PreprocessingConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPreprocessingConfigurationWithInvalidCheckGraphLabelElement2() throws JSONException {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"checkGraphLabel\":{\n" +
      "  \"valueIfNotExists\":\"testLabel\"\n" +
      "}\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    new PreprocessingConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPreprocessingConfigurationForCombineWithEmptyGraphArray() throws JSONException {

    String preConfig = "{\n" +
      "\"task\":\"COMBINE\",\n" +
      "\"return\":\"GRAPH_COLLECTION\",\n" +
      "\"graphs\":[\n" +
      "]\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    new PreprocessingConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testRunPreprocessingWithPropertyTransformForUnknownTask() throws Exception {
    String path = getClass().getClassLoader().getResource("gradoop/csv/input_logical_graph_1").getPath();

    String preConfig = "{\n" +
      "\"task\":\"READ\",\n" +
      "\"return\":\"LOGICAL_GRAPH\",\n" +
      "\"path\":\"" + path + "\",\n" +
      "\"transformProperty\":{\n" +
      "  \"task\":\"UNKNOWN\",\n" +
      "  \"config\":[\n" +
      "    {\n" +
      "      \"graphLabel\":\"\",\n" +
      "      \"key\":\"a\",\n" +
      "      \"renamed\":\"new\",\n" +
      "      \"keep\":\"true\"\n" +
      "    }\n" +
      "  ]\n" +
      "}\n" +
      "}";
    JSONObject configJson = new JSONObject(preConfig);

    PreprocessingConfiguration preprocessingConfiguration = new PreprocessingConfiguration();
    preprocessingConfiguration.checkConfigAndBuildComponents(configJson);
  }
}
