/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.linking.selection;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.linking.selection.dataStructures.SelectionComponent;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRuleComponent;
import org.gradoop.famer.linking.selection.dataStructures.aggregatorRule.AggregatorRuleComponentType;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.Condition;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.ConditionOperator;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRuleComponent;
import org.gradoop.famer.linking.selection.dataStructures.selectionRule.SelectionRuleComponentType;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.List;

import static org.junit.Assert.*;

/**
 * JUnit tests for {@link SelectionConfiguration}.
 */
public class SelectionConfigurationTest extends GradoopFlinkTestBase {

  @Test
  public void testBuildSelectionConfigWithDefaultAggregationRuleAndVerifyInteractions() throws JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"aggregationRule\":{\n" +
      "       \"aggregationThreshold\":\"0.5\"\n" +
      "     }\n" +
      "   }\n" +
      "}";
    JSONObject configJson = new JSONObject(selConfig);

    SelectionConfiguration selectionConfigSpy = Mockito.spy(new SelectionConfiguration());

    SelectionComponent component =
      selectionConfigSpy.checkConfigAndBuildComponents(configJson).getSelectionComponent();

    // verify method calls
    Mockito.verify(selectionConfigSpy, Mockito.times(1))
      .checkConfigAndBuildComponents(Mockito.any(JSONObject.class));
    Mockito.verify(selectionConfigSpy, Mockito.times(1))
      .buildAggregatorRule(Mockito.any(JSONObject.class));
    Mockito.verify(selectionConfigSpy, Mockito.times(1))
      .getSelectionComponent();
    Mockito.verifyNoMoreInteractions(selectionConfigSpy);

    assertNotNull(component);
    assertNull(component.getSelectionRule());
    assertNotNull(component.getAggregatorRule());
    assertEquals(0.5, component.getAggregatorRule().getAggregationThreshold(), 0.0);
    assertNull(component.getAggregatorRule().getAggregatorRuleComponents());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildSelectionConfigWithDefaultAggregationRuleForMissingThreshold() throws JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"aggregationRule\":{\n" +
      "     }\n" +
      "   }\n" +
      "}";
    JSONObject configJson = new JSONObject(selConfig);
    new SelectionConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test
  public void testBuildSelectionConfigWithAllAggregationRuleComponents() throws JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"aggregationRule\":{\n" +
      "       \"aggregationThreshold\":\"0.5\",\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "            \"componentType\":\"OPEN_PARENTHESIS\"\n" +
      "         },\n" +
      "         {\n" +
      "            \"componentType\":\"CLOSE_PARENTHESIS\"\n" +
      "         },\n" +
      "         {\n" +
      "            \"componentType\":\"SIMILARITY_FIELD_ID\",\n" +
      "            \"value\":\"sim1\"\n" +
      "         },\n" +
      "         {\n" +
      "            \"componentType\":\"ARITHMETIC_OPERATOR\",\n" +
      "            \"value\":\"PLUS\"\n" +
      "         },\n" +
      "         {\n" +
      "            \"componentType\":\"ARITHMETIC_OPERATOR\",\n" +
      "            \"value\":\"MINUS\"\n" +
      "         },\n" +
      "         {\n" +
      "            \"componentType\":\"ARITHMETIC_OPERATOR\",\n" +
      "            \"value\":\"MULTIPLY\"\n" +
      "         },\n" +
      "         {\n" +
      "            \"componentType\":\"ARITHMETIC_OPERATOR\",\n" +
      "            \"value\":\"DIVISION\"\n" +
      "         },\n" +
      "         {\n" +
      "            \"componentType\":\"CONSTANT\",\n" +
      "            \"value\":\"5\"\n" +
      "         }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}\n";
    JSONObject configJson = new JSONObject(selConfig);

    SelectionComponent component =
      new SelectionConfiguration().checkConfigAndBuildComponents(configJson).getSelectionComponent();

    assertNotNull(component);
    assertNull(component.getSelectionRule());
    assertNotNull(component.getAggregatorRule());
    assertEquals(0.5, component.getAggregatorRule().getAggregationThreshold(), 0.0);

    List<AggregatorRuleComponent> components = component.getAggregatorRule().getAggregatorRuleComponents();
    assertNotNull(components);
    assertEquals(8, components.size());

    assertEquals(components.get(0).getAggregatorRuleComponentType(),
      AggregatorRuleComponentType.OPEN_PARENTHESIS);
    assertEquals(components.get(0).getValue(), "(");
    assertEquals(components.get(1).getAggregatorRuleComponentType(),
      AggregatorRuleComponentType.CLOSE_PARENTHESIS);
    assertEquals(components.get(1).getValue(), ")");
    assertEquals(components.get(2).getAggregatorRuleComponentType(),
      AggregatorRuleComponentType.SIMILARITY_FIELD_ID);
    assertEquals(components.get(2).getValue(), "sim1");
    assertEquals(components.get(3).getAggregatorRuleComponentType(),
      AggregatorRuleComponentType.ARITHMETIC_OPERATOR);
    assertEquals(components.get(3).getValue(), "PLUS");
    assertEquals(components.get(4).getAggregatorRuleComponentType(),
      AggregatorRuleComponentType.ARITHMETIC_OPERATOR);
    assertEquals(components.get(4).getValue(), "MINUS");
    assertEquals(components.get(5).getAggregatorRuleComponentType(),
      AggregatorRuleComponentType.ARITHMETIC_OPERATOR);
    assertEquals(components.get(5).getValue(), "MULTIPLY");
    assertEquals(components.get(6).getAggregatorRuleComponentType(),
      AggregatorRuleComponentType.ARITHMETIC_OPERATOR);
    assertEquals(components.get(6).getValue(), "DIVISION");
    assertEquals(components.get(7).getAggregatorRuleComponentType(), AggregatorRuleComponentType.CONSTANT);
    assertEquals(components.get(7).getValue(), "5");
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildSelectionConfigWithUnknownAggregationRuleComponent() throws JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"aggregationRule\":{\n" +
      "       \"aggregationThreshold\":\"0.5\",\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "            \"componentType\":\"XYZ\"\n" +
      "         }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}\n";
    JSONObject configJson = new JSONObject(selConfig);

    new SelectionConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildSelectionConfigWithMissingAggregationRuleComponentValueForSimField() throws
    JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"aggregationRule\":{\n" +
      "       \"aggregationThreshold\":\"0.5\",\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "            \"componentType\":\"SIMILARITY_FIELD_ID\"\n" +
      "         }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}\n";
    JSONObject configJson = new JSONObject(selConfig);

    new SelectionConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildSelectionConfigWithMissingAggregationRuleComponentValueForArithmeticOp() throws
    JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"aggregationRule\":{\n" +
      "       \"aggregationThreshold\":\"0.5\",\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "            \"componentType\":\"ARITHMETIC_OPERATOR\"\n" +
      "         }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}\n";
    JSONObject configJson = new JSONObject(selConfig);

    new SelectionConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildSelectionConfigWithMissingAggregationRuleComponentValueForConstant() throws
    JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"aggregationRule\":{\n" +
      "       \"aggregationThreshold\":\"0.5\",\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "            \"componentType\":\"CONSTANT\"\n" +
      "         }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}\n";
    JSONObject configJson = new JSONObject(selConfig);

    new SelectionConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test
  public void testBuildSelectionConfigWithSelectionRuleForConditionAndVerifyInteractions() throws
    JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"selectionRule\":{  \n" +
      "       \"ruleComponents\":[\n" +
      "        {\n" +
      "           \"componentType\":\"CONDITION\",\n" +
      "           \"conditionId\":\"con1\",\n" +
      "           \"similarityFieldId\":\"sim1\",\n" +
      "           \"operator\":\"EQUAL\",\n" +
      "           \"threshold\":\"0.5\",\n" +
      "           \"defaultOnAttributeNull\":\"true\"" +
      "        }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}";
    JSONObject configJson = new JSONObject(selConfig);

    SelectionConfiguration selectionConfigSpy = Mockito.spy(new SelectionConfiguration());

    SelectionComponent component =
      selectionConfigSpy.checkConfigAndBuildComponents(configJson).getSelectionComponent();

    // verify method calls
    Mockito.verify(selectionConfigSpy, Mockito.times(1))
      .checkConfigAndBuildComponents(Mockito.any(JSONObject.class));
    Mockito.verify(selectionConfigSpy, Mockito.times(1))
      .buildSelectionRule(Mockito.any(JSONObject.class));
    Mockito.verify(selectionConfigSpy, Mockito.times(1))
      .getSelectionComponent();
    Mockito.verifyNoMoreInteractions(selectionConfigSpy);

    assertNotNull(component);
    assertNull(component.getAggregatorRule());
    assertNotNull(component.getSelectionRule());

    List<SelectionRuleComponent> ruleComponents = component.getSelectionRule().getSelectionRuleComponents();
    assertNotNull(ruleComponents);
    assertEquals(3, ruleComponents.size());
    assertEquals(SelectionRuleComponentType.OPEN_PARENTHESIS,
      ruleComponents.get(0).getSelectionRuleComponentType());
    assertEquals(SelectionRuleComponentType.CONDITION, ruleComponents.get(1).getSelectionRuleComponentType());
    assertEquals("con1", ruleComponents.get(1).getValue());
    assertEquals(SelectionRuleComponentType.CLOSE_PARENTHESIS,
      ruleComponents.get(2).getSelectionRuleComponentType());

    List<Condition> conditions = component.getSelectionRule().getConditions();
    assertNotNull(conditions);
    assertEquals(1, conditions.size());
    assertEquals("con1", conditions.get(0).getId());
    assertEquals("sim1", conditions.get(0).getSimilarityFieldId());
    assertEquals(ConditionOperator.EQUAL, conditions.get(0).getOperator());
    assertEquals(0.5, conditions.get(0).getThreshold(), 0.0);
    assertTrue(conditions.get(0).getDefaultOnAttributeNull());
  }

  @Test
  public void buildSelectionConfigWithAllSelectionRuleComponents() throws JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"selectionRule\":{\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "            \"componentType\":\"OPEN_PARENTHESIS\"\n" +
      "         },\n" +
      "         {\n" +
      "            \"componentType\":\"CLOSE_PARENTHESIS\"\n" +
      "         },\n" +
      "         {\n" +
      "           \"componentType\":\"CONDITION\",\n" +
      "           \"conditionId\":\"con1\",\n" +
      "           \"similarityFieldId\":\"sim1\",\n" +
      "           \"operator\":\"EQUAL\",\n" +
      "           \"threshold\":\"0.5\"\n" +
      "         },\n" +
      "         {\n" +
      "           \"componentType\":\"SELECTION_OPERATOR\",\n" +
      "           \"value\":\"AND\"" +
      "         },\n" +
      "         {\n" +
      "           \"componentType\":\"SELECTION_OPERATOR\",\n" +
      "           \"value\":\"OR\"" +
      "         }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}";
    JSONObject configJson = new JSONObject(selConfig);

    SelectionComponent component =
      new SelectionConfiguration().checkConfigAndBuildComponents(configJson).getSelectionComponent();

    assertNotNull(component);
    assertNull(component.getAggregatorRule());
    assertNotNull(component.getSelectionRule());

    List<SelectionRuleComponent> ruleComponents = component.getSelectionRule().getSelectionRuleComponents();
    assertNotNull(ruleComponents);
    assertEquals(7, ruleComponents.size());
    assertEquals(SelectionRuleComponentType.OPEN_PARENTHESIS,
      ruleComponents.get(0).getSelectionRuleComponentType());
    assertEquals(SelectionRuleComponentType.OPEN_PARENTHESIS,
      ruleComponents.get(1).getSelectionRuleComponentType());
    assertEquals("(", ruleComponents.get(1).getValue());
    assertEquals(SelectionRuleComponentType.CLOSE_PARENTHESIS,
      ruleComponents.get(2).getSelectionRuleComponentType());
    assertEquals(")", ruleComponents.get(2).getValue());
    assertEquals(SelectionRuleComponentType.CONDITION, ruleComponents.get(3).getSelectionRuleComponentType());
    assertEquals("con1", ruleComponents.get(3).getValue());
    assertEquals(SelectionRuleComponentType.SELECTION_OPERATOR,
      ruleComponents.get(4).getSelectionRuleComponentType());
    assertEquals("AND", ruleComponents.get(4).getValue());
    assertEquals(SelectionRuleComponentType.SELECTION_OPERATOR,
      ruleComponents.get(5).getSelectionRuleComponentType());
    assertEquals("OR", ruleComponents.get(5).getValue());
    assertEquals(SelectionRuleComponentType.CLOSE_PARENTHESIS,
      ruleComponents.get(6).getSelectionRuleComponentType());

    List<Condition> conditions = component.getSelectionRule().getConditions();
    assertNotNull(conditions);
    assertEquals(1, conditions.size());
    assertEquals("con1", conditions.get(0).getId());
    assertEquals("sim1", conditions.get(0).getSimilarityFieldId());
    assertEquals(ConditionOperator.EQUAL, conditions.get(0).getOperator());
    assertEquals(0.5, conditions.get(0).getThreshold(), 0.0);
    assertFalse(conditions.get(0).getDefaultOnAttributeNull());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildSelectionConfigForUnknownSelectionRuleComponent() throws JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"selectionRule\":{\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "           \"componentType\":\"XYZ\"\n" +
      "         }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}";
    JSONObject configJson = new JSONObject(selConfig);

    new SelectionConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildSelectionConfigWithMissingSelectionRuleComponentValueForCondition() throws
    JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"selectionRule\":{\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "           \"componentType\":\"CONDITION\"\n" +
      "         }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}";
    JSONObject configJson = new JSONObject(selConfig);

    new SelectionConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildSelectionConfigWithMissingSelectionRuleComponentValueForSelectionOp() throws
    JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"selectionRule\":{\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "           \"componentType\":\"SELECTION_OPERATOR\"\n" +
      "         }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}";
    JSONObject configJson = new JSONObject(selConfig);

    new SelectionConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test
  public void buildSelectionConfigWithAggregationAndSelectionRuleAndVerifyInteraction() throws JSONException {
    String selConfig = "{\n" +
      "   \"selectionComponent\":{\n" +
      "     \"selectionMethod\":\"MANUAL\",\n" +
      "     \"aggregationRule\":{\n" +
      "       \"aggregationThreshold\":\"0.5\",\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "            \"componentType\":\"SIMILARITY_FIELD_ID\",\n" +
      "            \"value\":\"sim1\"\n" +
      "         }\n" +
      "       ]\n" +
      "     },\n" +
      "     \"selectionRule\":{\n" +
      "       \"ruleComponents\":[\n" +
      "         {\n" +
      "           \"componentType\":\"CONDITION\",\n" +
      "           \"conditionId\":\"con1\",\n" +
      "           \"similarityFieldId\":\"sim1\",\n" +
      "           \"operator\":\"EQUAL\",\n" +
      "           \"threshold\":\"0.5\"\n" +
      "         }\n" +
      "       ]\n" +
      "     }\n" +
      "   }\n" +
      "}";
    JSONObject configJson = new JSONObject(selConfig);

    SelectionConfiguration selectionConfigSpy = Mockito.spy(new SelectionConfiguration());

    SelectionComponent component =
      selectionConfigSpy.checkConfigAndBuildComponents(configJson).getSelectionComponent();

    // verify method calls
    Mockito.verify(selectionConfigSpy, Mockito.times(1))
      .checkConfigAndBuildComponents(Mockito.any(JSONObject.class));
    Mockito.verify(selectionConfigSpy, Mockito.times(1))
      .buildAggregatorRule(Mockito.any(JSONObject.class));
    Mockito.verify(selectionConfigSpy, Mockito.times(1))
      .buildSelectionRule(Mockito.any(JSONObject.class));
    Mockito.verify(selectionConfigSpy, Mockito.times(1))
      .getSelectionComponent();
    Mockito.verifyNoMoreInteractions(selectionConfigSpy);

    assertNotNull(component);
    assertNotNull(component.getAggregatorRule());
    assertNotNull(component.getSelectionRule());

    assertNotNull(component.getAggregatorRule().getAggregatorRuleComponents());
    assertEquals(1, component.getAggregatorRule().getAggregatorRuleComponents().size());

    assertNotNull(component.getSelectionRule().getSelectionRuleComponents());
    assertEquals(3, component.getSelectionRule().getSelectionRuleComponents().size());

    assertNotNull(component.getSelectionRule().getConditions());
    assertEquals(1, component.getSelectionRule().getConditions().size());
  }
}
