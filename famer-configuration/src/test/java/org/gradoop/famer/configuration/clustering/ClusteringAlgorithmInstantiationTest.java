/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.clustering;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.clustering.parallelClustering.AbstractParallelClustering;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.hierarchicalMscdAp.HierarchicalAffinityPropagation;
import org.gradoop.famer.clustering.parallelClustering.affinityPropagation.sparseMscdAp.gelly.AffinityPropagationSparseGelly;
import org.gradoop.famer.clustering.parallelClustering.center.Center;
import org.gradoop.famer.clustering.parallelClustering.clip.CLIP;
import org.gradoop.famer.clustering.parallelClustering.common.connectedComponents.ConnectedComponents;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.ClusteringOutputType;
import org.gradoop.famer.clustering.parallelClustering.common.dataStructures.PrioritySelection;
import org.gradoop.famer.clustering.parallelClustering.correlationClustering.CorrelationClustering;
import org.gradoop.famer.clustering.parallelClustering.limitedCorrelationClustering.LimitedCorrelationClustering;
import org.gradoop.famer.clustering.parallelClustering.mergeCenter.MergeCenter;
import org.gradoop.famer.clustering.parallelClustering.star.Star;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * JUnit tests for clustering algorithm instantiation in {@link ClusteringConfiguration}.
 */
public class ClusteringAlgorithmInstantiationTest extends GradoopFlinkTestBase {

  private final ClusteringConfiguration clusteringConfiguration = new ClusteringConfiguration();

  @Test
  public void testBuildClusteringAlgorithmForCenter() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"CENTER\",\n" +
      "   \"prioritySelection\":\"MIN\",\n" +
      "   \"isEdgesBiDirected\":false,\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    AbstractParallelClustering clusteringAlgorithm =
      clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);

    assertNotNull(clusteringAlgorithm);
    assertTrue(clusteringAlgorithm instanceof Center);
    assertEquals(PrioritySelection.MIN, ((Center) clusteringAlgorithm).getPrioritySelection());
    assertFalse(((Center) clusteringAlgorithm).isEdgesBiDirected());
    assertEquals(ClusteringOutputType.GRAPH, ((Center) clusteringAlgorithm).getClusteringOutputType());
    assertEquals(Integer.MAX_VALUE, ((Center) clusteringAlgorithm).getMaxIteration());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForCenterWithMissingValues() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"CENTER\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test
  public void testBuildClusteringAlgorithmForCLIP() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"CLIP\",\n" +
      "   \"clipConfig\":{\n" +
      "     \"delta\":\"0.0\",\n" +
      "     \"sourceNumber\":\"1\",\n" +
      "     \"removeSourceConsistentVertices\":false,\n" +
      "     \"simValueCoef\":\"0.5\",\n" +
      "     \"degreeCoef\":\"0.2\",\n" +
      "     \"strengthCoef\":\"0.3\"\n" +
      "   },\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    AbstractParallelClustering clusteringAlgorithm =
      clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);

    assertNotNull(clusteringAlgorithm);
    assertTrue(clusteringAlgorithm instanceof CLIP);
    assertEquals(0d, ((CLIP) clusteringAlgorithm).getClipConfig().getDelta(), 0d);
    assertEquals(1, ((CLIP) clusteringAlgorithm).getClipConfig().getSourceNumber());
    assertFalse(((CLIP) clusteringAlgorithm).getClipConfig().isRemoveSourceConsistentVertices());
    assertEquals(0.5, ((CLIP) clusteringAlgorithm).getClipConfig().getSimValueCoef(), 0d);
    assertEquals(0.2, ((CLIP) clusteringAlgorithm).getClipConfig().getDegreeCoef(), 0d);
    assertEquals(0.3, ((CLIP) clusteringAlgorithm).getClipConfig().getStrengthCoef(), 0d);
    assertEquals(ClusteringOutputType.GRAPH, ((CLIP) clusteringAlgorithm).getClusteringOutputType());
    assertEquals(Integer.MAX_VALUE, ((CLIP) clusteringAlgorithm).getMaxIteration());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForCLIPWithMissingValues() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"CLIP\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForCLIPWithWrongConfigValues() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"CLIP\",\n" +
      "   \"clipConfig\":{\n" +
      "     \"delta\":\"a\",\n" +
      "     \"sourceNumber\":\"1\",\n" +
      "     \"removeSourceConsistentVertices\":false,\n" +
      "     \"simValueCoef\":\"0.5\",\n" +
      "     \"degreeCoef\":\"0.2\",\n" +
      "     \"strengthCoef\":\"0.3\"\n" +
      "   },\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test
  public void testBuildClusteringAlgorithmForConnectedComponents() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"CONNECTED_COMPONENTS\",\n" +
      "   \"clusterIdPrefix\":\"cc\",\n" +
      "   \"similarityEdgeLabel\":\"simEdge\",\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    AbstractParallelClustering clusteringAlgorithm =
      clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);

    assertNotNull(clusteringAlgorithm);
    assertTrue(clusteringAlgorithm instanceof ConnectedComponents);
    assertEquals("cc", ((ConnectedComponents) clusteringAlgorithm).getClusterIdPrefix());
    assertEquals("simEdge", ((ConnectedComponents) clusteringAlgorithm).getSimilarityEdgeLabel());
    assertEquals(ClusteringOutputType.GRAPH,
      ((ConnectedComponents) clusteringAlgorithm).getClusteringOutputType());
    assertEquals(Integer.MAX_VALUE, ((ConnectedComponents) clusteringAlgorithm).getMaxIteration());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForConnectedComponentsWithMissingValues() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"CONNECTED_COMPONENTS\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test
  public void testBuildClusteringAlgorithmForCorrelationClustering() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"CORRELATION_CLUSTERING\",\n" +
      "   \"epsilon\":\"0.9\",\n" +
      "   \"isEdgesBiDirected\":false,\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    AbstractParallelClustering clusteringAlgorithm =
      clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);

    assertNotNull(clusteringAlgorithm);
    assertTrue(clusteringAlgorithm instanceof CorrelationClustering);
    assertEquals(0.9, ((CorrelationClustering) clusteringAlgorithm).getEpsilon(), 0d);
    assertEquals(ClusteringOutputType.GRAPH,
      ((CorrelationClustering) clusteringAlgorithm).getClusteringOutputType());
    assertEquals(Integer.MAX_VALUE, ((CorrelationClustering) clusteringAlgorithm).getMaxIteration());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForCorrelationClusteringWithMissingValues() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"CORRELATION_CLUSTERING\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForCorrelationClusteringWithWrongValues() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"CORRELATION_CLUSTERING\",\n" +
      "   \"epsilon\":\"a\",\n" +
      "   \"isEdgesBiDirected\":false,\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test
  public void testBuildClusteringAlgorithmForLimitedCorrelationClustering() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"LIMITED_CORRELATION_CLUSTERING\",\n" +
      "   \"epsilon\":\"0.9\",\n" +
      "   \"centerType\":\"centerType\",\n" +
      "   \"isEdgesBiDirected\":false,\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    AbstractParallelClustering clusteringAlgorithm =
      clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);

    assertNotNull(clusteringAlgorithm);
    assertTrue(clusteringAlgorithm instanceof LimitedCorrelationClustering);
    assertEquals(0.9, ((LimitedCorrelationClustering) clusteringAlgorithm).getEpsilon(), 0d);
    assertEquals("centerType", ((LimitedCorrelationClustering) clusteringAlgorithm).getCenterType());
    assertEquals(ClusteringOutputType.GRAPH,
      ((LimitedCorrelationClustering) clusteringAlgorithm).getClusteringOutputType());
    assertEquals(Integer.MAX_VALUE, ((LimitedCorrelationClustering) clusteringAlgorithm).getMaxIteration());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForLimitedCorrelationClusteringWithMissingValues() throws
    JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"LIMITED_CORRELATION_CLUSTERING\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForLimitedCorrelationClusteringWithWrongValues() throws
    JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"LIMITED_CORRELATION_CLUSTERING\",\n" +
      "   \"epsilon\":\"a\",\n" +
      "   \"centerType\":\"centerType\",\n" +
      "   \"isEdgesBiDirected\":false,\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" + "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test
  public void testBuildClusteringAlgorithmForMergeCenter() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"MERGE_CENTER\",\n" +
      "   \"prioritySelection\":\"MIN\",\n" +
      "   \"simDegMergeThreshold\":\"0.5\",\n" +
      "   \"isEdgesBiDirected\":false,\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    AbstractParallelClustering clusteringAlgorithm =
      clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);

    assertNotNull(clusteringAlgorithm);
    assertTrue(clusteringAlgorithm instanceof MergeCenter);
    assertEquals(PrioritySelection.MIN, ((MergeCenter) clusteringAlgorithm).getPrioritySelection());
    assertEquals(0.5, ((MergeCenter) clusteringAlgorithm).getSimDegMergeThreshold(), 0d);
    assertFalse(((MergeCenter) clusteringAlgorithm).isEdgesBiDirected());
    assertEquals(ClusteringOutputType.GRAPH, ((MergeCenter) clusteringAlgorithm).getClusteringOutputType());
    assertEquals(Integer.MAX_VALUE, ((MergeCenter) clusteringAlgorithm).getMaxIteration());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForMergeCenterWithMissingValues() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"MERGE_CENTER\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForMergeCenterWithWrongValues() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"MERGE_CENTER\",\n" +
      "   \"prioritySelection\":\"MIN\",\n" +
      "   \"simDegMergeThreshold\":\"a\",\n" +
      "   \"isEdgesBiDirected\":false,\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test
  public void testBuildClusteringAlgorithmForMSCDSparseGelly() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"MSCD_AP_SPARSE_GELLY\",\n" +
      "   \"isEdgesBiDirected\": false,\n" +
      "   \"clusteringOutputType\": \"GRAPH\",\n" +
      "   \"maxIteration\": \"MAX_VALUE\",\n" +
      "   \"apConfig\": {\n" +
      "     \"maxApIteration\": 20000,\n" +
      "     \"maxAdaptionIteration\": 150,\n" +
      "     \"convergenceIter\": 15,\n" +
      "     \"dampingFactor\": 0.5,\n" +
      "     \"dampingAdaptionStep\": 0.1,\n" +
      "     \"allSameSimClusteringThreshold\": 0.7,\n" +
      "     \"noiseDecimalPlace\": 3,\n" +
      "     \"allSourcesClean\": false,\n" +
      "     \"sourceDirtinessVertexProperty\": \"isSrcDirty\",\n" +
      "     \"cleanSources\": [ \"ebay.com\", \"amazon.com\" ],\n" +
      "     \"preferenceConfig\": {\n" +
      "       \"preferenceUseMinSimilarityDirtySrc\": true,\n" +
      "       \"preferenceUseMinSimilarityCleanSrc\": false,\n" +
      "       \"preferenceFixValueDirtySrc\": -1,\n" +
      "       \"preferenceFixValueCleanSrc\": -1,\n" +
      "       \"preferencePercentileDirtySrc\": -1,\n" +
      "       \"preferencePercentileCleanSrc\": 30,\n" +
      "       \"preferenceAdaptionStep\": 0.05\n" +
      "     }\n" +
      "   }\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    AbstractParallelClustering clusteringAlgorithm =
      clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);

    assertNotNull(clusteringAlgorithm);
    assertTrue(clusteringAlgorithm instanceof AffinityPropagationSparseGelly);
    assertFalse(((AffinityPropagationSparseGelly) clusteringAlgorithm).isEdgesBiDirected());
    assertEquals(ClusteringOutputType.GRAPH, ((AffinityPropagationSparseGelly) clusteringAlgorithm).getClusteringOutputType());
    assertEquals(Integer.MAX_VALUE, ((AffinityPropagationSparseGelly) clusteringAlgorithm).getMaxIteration());
    assertNotNull(((AffinityPropagationSparseGelly) clusteringAlgorithm).getApConfig());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForMSCDSparseGellyForMissingApConfig() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"MSCD_AP_SPARSE_GELLY\",\n" +
      "   \"isEdgesBiDirected\": false,\n" +
      "   \"clusteringOutputType\": \"GRAPH\",\n" +
      "   \"maxIteration\": \"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test
  public void testBuildClusteringAlgorithmForMSCDHAP() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"MSCD_HAP\",\n" +
      "   \"isEdgesBiDirected\": false,\n" +
      "   \"clusteringOutputType\": \"GRAPH\",\n" +
      "   \"maxIteration\": \"MAX_VALUE\",\n" +
      "   \"hapConfig\": {\n" +
      "     \"maxApIteration\": 20000,\n" +
      "     \"maxAdaptionIteration\": 150,\n" +
      "     \"convergenceIter\": 15,\n" +
      "     \"dampingFactor\": 0.5,\n" +
      "     \"dampingAdaptionStep\": 0.1,\n" +
      "     \"allSameSimClusteringThreshold\": 0.7,\n" +
      "     \"noiseDecimalPlace\": 3,\n" +
      "     \"allSourcesClean\": false,\n" +
      "     \"sourceDirtinessVertexProperty\": \"isSrcDirty\",\n" +
      "     \"cleanSources\": [ \"ebay.com\", \"amazon.com\" ],\n" +
      "     \"maxPartitionSize\": 1000,\n" +
      "     \"maxHierarchyDepth\": 10,\n" +
      "     \"hapExemplarAssignmentStrategy\": \"HUNGARIAN\",\n" +
      "     \"preferenceConfig\": {\n" +
      "       \"preferenceUseMinSimilarityDirtySrc\": true,\n" +
      "       \"preferenceUseMinSimilarityCleanSrc\": false,\n" +
      "       \"preferenceFixValueDirtySrc\": -1,\n" +
      "       \"preferenceFixValueCleanSrc\": -1,\n" +
      "       \"preferencePercentileDirtySrc\": -1,\n" +
      "       \"preferencePercentileCleanSrc\": 30,\n" +
      "       \"preferenceAdaptionStep\": 0.05\n" +
      "     }\n" +
      "   }\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    AbstractParallelClustering clusteringAlgorithm =
      clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);

    assertNotNull(clusteringAlgorithm);
    assertTrue(clusteringAlgorithm instanceof HierarchicalAffinityPropagation);
    assertFalse(((HierarchicalAffinityPropagation) clusteringAlgorithm).isEdgesBiDirected());
    assertEquals(ClusteringOutputType.GRAPH, ((HierarchicalAffinityPropagation) clusteringAlgorithm).getClusteringOutputType());
    assertEquals(Integer.MAX_VALUE, ((HierarchicalAffinityPropagation) clusteringAlgorithm).getMaxIteration());
    assertNotNull(((HierarchicalAffinityPropagation) clusteringAlgorithm).getApConfig());
    assertNotNull(((HierarchicalAffinityPropagation) clusteringAlgorithm).getHapConfig());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForMSCDHAPForMissingHapConfig() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"MSCD_HAP\",\n" +
      "   \"isEdgesBiDirected\": false,\n" +
      "   \"clusteringOutputType\": \"GRAPH\",\n" +
      "   \"maxIteration\": \"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test
  public void testBuildClusteringAlgorithmForStar() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"STAR\",\n" +
      "   \"prioritySelection\":\"MIN\",\n" +
      "   \"starType\":\"ONE\",\n" +
      "   \"isEdgesBiDirected\":false,\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    AbstractParallelClustering clusteringAlgorithm =
      clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);

    assertNotNull(clusteringAlgorithm);
    assertTrue(clusteringAlgorithm instanceof Star);
    assertEquals(PrioritySelection.MIN, ((Star) clusteringAlgorithm).getPrioritySelection());
    assertEquals(Star.StarType.ONE, ((Star) clusteringAlgorithm).getStarType());
    assertFalse(((Star) clusteringAlgorithm).isEdgesBiDirected());
    assertEquals(ClusteringOutputType.GRAPH, ((Star) clusteringAlgorithm).getClusteringOutputType());
    assertEquals(Integer.MAX_VALUE, ((Star) clusteringAlgorithm).getMaxIteration());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForStarWithMissingValues() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"STAR\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusteringAlgorithmForStarWithWrongValues() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"STAR\",\n" +
      "   \"prioritySelection\":\"MIN\",\n" +
      "   \"starType\":\"A\",\n" +
      "   \"isEdgesBiDirected\":false,\n" +
      "   \"clusteringOutputType\":\"GRAPH\",\n" +
      "   \"maxIteration\":\"MAX_VALUE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);
  }
  @Test
  public void testBuildClusteringAlgorithmForNONE() throws JSONException {
    String json = "{\n" +
      "   \"clusteringMethod\":\"NONE\"\n" +
      "}";
    JSONObject clusterConfigJson = new JSONObject(json);

    AbstractParallelClustering clusteringAlgorithm =
      clusteringConfiguration.instantiateClusteringAlgorithm(clusterConfigJson);

    assertNull(clusteringAlgorithm);
  }
}
