/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.linking.similarity;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.linking.similarityMeasuring.dataStructures.*;
import org.gradoop.famer.linking.similarityMeasuring.methods.LongestCommonSubstring;
import org.gradoop.famer.linking.similarityMeasuring.methods.QGrams;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Test;
import org.mockito.Mockito;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.Assert.*;

/**
 * JUnit tests for {@link SimilarityConfiguration}.
 */
public class SimilarityConfigurationTest extends GradoopFlinkTestBase {

  String commonSimBaseConfig =
    "\"id\":\"authors\",\n" +
    "\"sourceGraph\":\"*\",\n" +
    "\"targetGraph\":\"*\",\n" +
    "\"sourceLabel\":\"acm\",\n" +
    "\"targetLabel\":\"dblp\",\n" +
    "\"sourceAttribute\":\"authors\",\n" +
    "\"targetAttribute\":\"authors\",\n" +
    "\"weight\":\"1\",";

  @Test
  public void testBuildSimilarityComponentBaseConfig() throws JSONException {
    String simConfig = "{\n" +
      "\"id\":\"authors\",\n" +
      "\"sourceGraph\":\"*\",\n" +
      "\"targetGraph\":\"*\",\n" +
      "\"sourceLabel\":\"acm\",\n" +
      "\"targetLabel\":\"dblp\",\n" +
      "\"sourceAttribute\":\"authorsA\",\n" +
      "\"targetAttribute\":\"authorsD\",\n" +
      "\"weight\":\"1\"\n" +
      "}";
    JSONObject configJson = new JSONObject(simConfig);

    SimilarityComponent similarityComponentMock = Mockito.mock(SimilarityComponent.class,
      Mockito.withSettings().useConstructor(configJson).defaultAnswer(Mockito.CALLS_REAL_METHODS));

    assertEquals("authors", similarityComponentMock.getComponentId());
    assertEquals("*", similarityComponentMock.getSourceGraph());
    assertEquals("*", similarityComponentMock.getTargetGraph());
    assertEquals("acm", similarityComponentMock.getSourceLabel());
    assertEquals("dblp", similarityComponentMock.getTargetLabel());
    assertEquals("authorsA", similarityComponentMock.getSourceAttribute());
    assertEquals("authorsD", similarityComponentMock.getTargetAttribute());
    assertEquals(1d, similarityComponentMock.getWeight(), 0d);
  }

  @Test(expected = Exception.class)
  public void testBuildSimilarityComponentBaseConfigWithMissingId() throws JSONException {
    String simConfig = "{\n" +
      "\"sourceGraph\":\"*\",\n" +
      "\"targetGraph\":\"*\",\n" +
      "\"sourceLabel\":\"acm\",\n" +
      "\"targetLabel\":\"dblp\",\n" +
      "\"sourceAttribute\":\"authors\",\n" +
      "\"targetAttribute\":\"authors\",\n" +
      "\"weight\":\"1\"\n" +
      "}";
    JSONObject configJson = new JSONObject(simConfig);

    SimilarityComponent similarityComponentMock = Mockito.mock(SimilarityComponent.class,
      Mockito.withSettings().useConstructor(configJson).defaultAnswer(Mockito.CALLS_REAL_METHODS));
  }

  @Test(expected = Exception.class)
  public void testBuildSimilarityComponentBaseConfigWithMissingSourceGraph() throws JSONException {
    String simConfig = "{\n" +
      "\"id\":\"authors\",\n" +
      "\"targetGraph\":\"*\",\n" +
      "\"sourceLabel\":\"acm\",\n" +
      "\"targetLabel\":\"dblp\",\n" +
      "\"sourceAttribute\":\"authors\",\n" +
      "\"targetAttribute\":\"authors\",\n" +
      "\"weight\":\"1\"\n" +
      "}";
    JSONObject configJson = new JSONObject(simConfig);

    SimilarityComponent similarityComponentMock = Mockito.mock(SimilarityComponent.class,
      Mockito.withSettings().useConstructor(configJson).defaultAnswer(Mockito.CALLS_REAL_METHODS));
  }

  @Test(expected = Exception.class)
  public void testBuildSimilarityComponentBaseConfigWithMissingTargetGraph() throws JSONException {
    String simConfig = "{\n" +
      "\"id\":\"authors\",\n" +
      "\"sourceGraph\":\"*\",\n" +
      "\"sourceLabel\":\"acm\",\n" +
      "\"targetLabel\":\"dblp\",\n" +
      "\"sourceAttribute\":\"authors\",\n" +
      "\"targetAttribute\":\"authors\",\n" +
      "\"weight\":\"1\"\n" +
      "}";
    JSONObject configJson = new JSONObject(simConfig);

    SimilarityComponent similarityComponentMock = Mockito.mock(SimilarityComponent.class,
      Mockito.withSettings().useConstructor(configJson).defaultAnswer(Mockito.CALLS_REAL_METHODS));
  }

  @Test(expected = Exception.class)
  public void testBuildSimilarityComponentBaseConfigWithMissingSourceLabel() throws JSONException {
    String simConfig = "{\n" +
      "\"id\":\"authors\",\n" +
      "\"sourceGraph\":\"*\",\n" +
      "\"targetGraph\":\"*\",\n" +
      "\"targetLabel\":\"dblp\",\n" +
      "\"sourceAttribute\":\"authors\",\n" +
      "\"targetAttribute\":\"authors\",\n" +
      "\"weight\":\"1\"\n" +
      "}";
    JSONObject configJson = new JSONObject(simConfig);

    SimilarityComponent similarityComponentMock = Mockito.mock(SimilarityComponent.class,
      Mockito.withSettings().useConstructor(configJson).defaultAnswer(Mockito.CALLS_REAL_METHODS));
  }

  @Test(expected = Exception.class)
  public void testBuildSimilarityComponentBaseConfigWithMissingTargetLabel() throws JSONException {
    String simConfig = "{\n" +
      "\"id\":\"authors\",\n" +
      "\"sourceGraph\":\"*\",\n" +
      "\"targetGraph\":\"*\",\n" +
      "\"sourceLabel\":\"acm\",\n" +
      "\"sourceAttribute\":\"authors\",\n" +
      "\"targetAttribute\":\"authors\",\n" +
      "\"weight\":\"1\"\n" +
      "}";
    JSONObject configJson = new JSONObject(simConfig);

    SimilarityComponent similarityComponentMock = Mockito.mock(SimilarityComponent.class,
      Mockito.withSettings().useConstructor(configJson).defaultAnswer(Mockito.CALLS_REAL_METHODS));
  }

  @Test(expected = Exception.class)
  public void testBuildSimilarityComponentBaseConfigWithMissingSourceAttribute() throws JSONException {
    String simConfig = "{\n" +
      "\"id\":\"authors\",\n" +
      "\"sourceGraph\":\"*\",\n" +
      "\"targetGraph\":\"*\",\n" +
      "\"sourceLabel\":\"acm\",\n" +
      "\"targetLabel\":\"dblp\",\n" +
      "\"targetAttribute\":\"authors\",\n" +
      "\"weight\":\"1\"\n" +
      "}";
    JSONObject configJson = new JSONObject(simConfig);

    SimilarityComponent similarityComponentMock = Mockito.mock(SimilarityComponent.class,
      Mockito.withSettings().useConstructor(configJson).defaultAnswer(Mockito.CALLS_REAL_METHODS));
  }

  @Test(expected = Exception.class)
  public void testBuildSimilarityComponentBaseConfigWithMissingTargetAttribute() throws JSONException {
    String simConfig = "{\n" +
      "\"id\":\"authors\",\n" +
      "\"sourceGraph\":\"*\",\n" +
      "\"targetGraph\":\"*\",\n" +
      "\"sourceLabel\":\"acm\",\n" +
      "\"targetLabel\":\"dblp\",\n" +
      "\"sourceAttribute\":\"authors\",\n" +
      "\"weight\":\"1\"\n" +
      "}";
    JSONObject configJson = new JSONObject(simConfig);

    SimilarityComponent similarityComponentMock = Mockito.mock(SimilarityComponent.class,
      Mockito.withSettings().useConstructor(configJson).defaultAnswer(Mockito.CALLS_REAL_METHODS));
  }

  @Test(expected = Exception.class)
  public void testBuildSimilarityComponentBaseConfigWithMissingWeight() throws JSONException {
    String simConfig = "{\n" +
      "\"id\":\"authors\",\n" +
      "\"sourceGraph\":\"*\",\n" +
      "\"targetGraph\":\"*\",\n" +
      "\"sourceLabel\":\"acm\",\n" +
      "\"targetLabel\":\"dblp\",\n" +
      "\"sourceAttribute\":\"authors\",\n" +
      "\"targetAttribute\":\"authors\"\n" +
      "}";
    JSONObject configJson = new JSONObject(simConfig);

    SimilarityComponent similarityComponentMock = Mockito.mock(SimilarityComponent.class,
      Mockito.withSettings().useConstructor(configJson).defaultAnswer(Mockito.CALLS_REAL_METHODS));
  }

  @Test(expected = Exception.class)
  public void testBuildSimilarityComponentBaseConfigWithWrongWeight() throws JSONException {
    String simConfig = "{\n" +
      "\"id\":\"authors\",\n" +
      "\"sourceGraph\":\"*\",\n" +
      "\"targetGraph\":\"*\",\n" +
      "\"sourceLabel\":\"acm\",\n" +
      "\"targetLabel\":\"dblp\",\n" +
      "\"sourceAttribute\":\"authors\",\n" +
      "\"targetAttribute\":\"authors\",\n" +
      "\"weight\":\"x\"\n" +
      "}";
    JSONObject configJson = new JSONObject(simConfig);

    SimilarityComponent similarityComponentMock = Mockito.mock(SimilarityComponent.class,
      Mockito.withSettings().useConstructor(configJson).defaultAnswer(Mockito.CALLS_REAL_METHODS));
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildSimilarityComponentForMissingMethod() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      "\"id\":\"authors\",\n" +
      "\"sourceGraph\":\"*\",\n" +
      "\"targetGraph\":\"*\",\n" +
      "\"sourceLabel\":\"acm\",\n" +
      "\"targetLabel\":\"dblp\",\n" +
      "\"sourceAttribute\":\"authors\",\n" +
      "\"targetAttribute\":\"authors\",\n" +
      "\"weight\":\"x\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);
    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildSimilarityComponentForUnknownMethod() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"XYZ\"" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test
  public void testBuildEditDistanceComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"EditDistanceComponent\"" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof EditDistanceComponent);
  }

  @Test
  public void testBuildEmbeddingComponent() throws JSONException, IOException {
    String wordVectorFile = File.createTempFile("wordVectorFile", ".vec").getPath();
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"EmbeddingComponent\",\n" +
      "\"wordVectorsFileUri\": \"" + wordVectorFile + "\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof EmbeddingComponent);
    assertNotNull(((EmbeddingComponent) components.get(0)).getDistributedCacheKey());
  }

  @Test
  public void testBuildExtendedJaccardComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"ExtendedJaccardComponent\",\n" +
      "\"tokenizer\": \"ab\",\n" +
      "\"threshold\": \"0.5\",\n" +
      "\"jaroWinklerThreshold\": \"1\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof ExtendedJaccardComponent);
    assertEquals("ab", ((ExtendedJaccardComponent) components.get(0)).getTokenizer());
    assertEquals(1d, ((ExtendedJaccardComponent) components.get(0)).getJaroWinklerThreshold(), 0d);
    assertEquals(0.5, ((ExtendedJaccardComponent) components.get(0)).getThreshold(), 0d);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildExtendedJaccardComponentForMissingTokenizer() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"ExtendedJaccardComponent\",\n" +
      "\"threshold\": \"0.5\",\n" +
      "\"jaroWinklerThreshold\": \"1\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildExtendedJaccardComponentForMissingThreshold() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"ExtendedJaccardComponent\",\n" +
      "\"tokenizer\": \"ab\",\n" +
      "\"jaroWinklerThreshold\": \"1\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildExtendedJaccardComponentForMissingJWThreshold() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"ExtendedJaccardComponent\",\n" +
      "\"tokenizer\": \"ab\",\n" +
      "\"threshold\": \"0.5\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }


  @Test
  public void testBuildGeoDistanceComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"GeoDistanceComponent\",\n" +
      "\"maxToleratedDistance\": \"5\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof GeoDistanceComponent);
    assertEquals(5d, ((GeoDistanceComponent) components.get(0)).getMaxToleratedDistance(), 0d);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildGeoDistanceComponentForMissingToleratedDistance() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"GeoDistanceComponent\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test
  public void testBuildJaroWinklerComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"JaroWinklerComponent\",\n" +
      "\"threshold\": \"1\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof JaroWinklerComponent);
    assertEquals(1d, ((JaroWinklerComponent) components.get(0)).getThreshold(), 0d);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildJaroWinklerComponentForMissingThreshold() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"JaroWinklerComponent\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test
  public void testBuildLongestCommonSubstringComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"LongestCommonSubstringComponent\",\n" +
      "\"secondMethod\": \"OVERLAP\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof LongestCommonSubstringComponent);
    assertEquals(LongestCommonSubstring.SecondMethod.OVERLAP,
      ((LongestCommonSubstringComponent) components.get(0)).getSecondMethod());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildLongestCommonSubstringComponentForMissingSecondMethod() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"LongestCommonSubstringComponent\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test
  public void buildMongeElkanComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"MongeElkanComponent\",\n" +
      "\"tokenizer\": \"ab\",\n" +
      "\"jaroWinklerThreshold\": \"0.5\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof MongeElkanComponent);
    assertEquals(0.5, ((MongeElkanComponent) components.get(0)).getJaroWinklerThreshold(), 0d);
    assertEquals("ab", ((MongeElkanComponent) components.get(0)).getTokenizer());
  }

  @Test(expected = FamerConfigException.class)
  public void buildMongeElkanComponentForMissingTokenizer() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"MongeElkanComponent\",\n" +
      "\"jaroWinklerThreshold\": \"0.5\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test(expected = FamerConfigException.class)
  public void buildMongeElkanComponentForMissingJWThreshold() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"MongeElkanComponent\",\n" +
      "\"tokenizer\": \"ab\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test
  public void buildNumericalSimilarityWithMaxDistanceComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"NumericalSimilarityWithMaxDistanceComponent\",\n" +
      "\"maxToleratedDistance\": \"5\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof NumericalSimilarityWithMaxDistanceComponent);
    assertEquals(5d,
      ((NumericalSimilarityWithMaxDistanceComponent) components.get(0)).getMaxToleratedDistance(), 0d);
  }

  @Test(expected = FamerConfigException.class)
  public void buildNumericalSimilarityWithMaxDistanceComponentForMissingToleratedDistance() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"NumericalSimilarityWithMaxDistanceComponent\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test
  public void buildNumericalSimilarityWithMaxPercentageComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"NumericalSimilarityWithMaxPercentageComponent\",\n" +
      "\"maxToleratedPercentage\": \"0.5\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof NumericalSimilarityWithMaxPercentageComponent);
    assertEquals(0.5,
      ((NumericalSimilarityWithMaxPercentageComponent) components.get(0)).getMaxToleratedPercentage(), 0d);
  }

  @Test(expected = FamerConfigException.class)
  public void buildNumericalSimilarityWithMaxPercentageComponentForMissingToleratedPercentage() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"NumericalSimilarityWithMaxPercentageComponent\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test
  public void testBuildQGramsComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"QGramsComponent\",\n" +
      "\"length\": \"2\",\n" +
      "\"padding\": \"true\",\n" +
      "\"secondMethod\": \"DICE\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof QGramsComponent);
    assertEquals(2, ((QGramsComponent) components.get(0)).getLength());
    assertTrue(((QGramsComponent) components.get(0)).isPadding());
    assertEquals(QGrams.SecondMethod.DICE, ((QGramsComponent) components.get(0)).getMethod());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildQGramsComponentForMissingLength() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"QGramsComponent\",\n" +
      "\"padding\": \"true\",\n" +
      "\"secondMethod\": \"DICE\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildQGramsComponentForMissingPadding() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"QGramsComponent\",\n" +
      "\"length\": \"2\",\n" +
      "\"secondMethod\": \"DICE\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildQGramsComponentForMissingSecondMethod() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"QGramsComponent\",\n" +
      "\"length\": \"2\",\n" +
      "\"padding\": \"true\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test
  public void testBuildTruncateBeginComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"TruncateBeginComponent\",\n" +
      "\"length\": \"2\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof TruncateBeginComponent);
    assertEquals(2, ((TruncateBeginComponent) components.get(0)).getLength());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildTruncateBeginComponentForMissingLength() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"TruncateBeginComponent\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test
  public void testBuildTruncateEndComponent() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"TruncateEndComponent\",\n" +
      "\"length\": \"2\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(1, components.size());
    assertTrue(components.get(0) instanceof TruncateEndComponent);
    assertEquals(2, ((TruncateEndComponent) components.get(0)).getLength());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildTruncateEndComponentForMissingLength() throws JSONException {
    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"TruncateEndComponent\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    new SimilarityConfiguration().checkConfigAndBuildComponents(simJson);
  }

  @Test
  public void testBuildMultipleSimilarityComponents() throws JSONException, IOException {
    String wordVectorFile = File.createTempFile("wordVectorFile", ".vec").getPath();

    String simConfig = "{\"similarityComponents\":[{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"EditDistanceComponent\"" +
      "},\n" +
      "{\n" +
      commonSimBaseConfig +
      "\"similarityMethod\": \"EmbeddingComponent\"," +
      "\"wordVectorsFileUri\": \"" + wordVectorFile + "\"\n" +
      "}]}";
    JSONObject simJson = new JSONObject(simConfig);

    List<SimilarityComponent> components =
      new SimilarityConfiguration().checkConfigAndBuildComponents(simJson).getSimilarityComponents();

    assertNotNull(components);
    assertEquals(2, components.size());
    assertTrue(components.get(0) instanceof EditDistanceComponent);
    assertTrue(components.get(1) instanceof EmbeddingComponent);
  }
}
