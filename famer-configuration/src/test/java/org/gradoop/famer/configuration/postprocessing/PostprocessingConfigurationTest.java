/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.postprocessing;

import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.gradoop.famer.configuration.FamerConfiguration;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.nio.file.Files;
import java.util.Objects;

import static org.gradoop.famer.clustering.common.PropertyNames.CLUSTER_ID;
import static org.junit.Assert.assertTrue;

/**
 * JUnit tests for {@link PostprocessingConfiguration}
 */
public class PostprocessingConfigurationTest extends GradoopFlinkTestBase {

  // Tests for graph writing

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingConfigurationWithEmptyTasks() throws JSONException {
    String postConfig = "[]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingConfigurationForUnknownTask() throws JSONException {
    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"A\"\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson);
  }

  @Test
  public void testRunPostprocessingForWriteGraph() throws IOException, JSONException {
    String graphString = "input[" +
      "(alice:Person {id:1, name:\"Alice\"})" +
      "(bob:Person {id:2, name:\"Bob\"})" +
      "(alice)-[]->(bob)" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    String tempOutDir = Files.createTempDirectory("postprocessingOutput").toString();

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"WRITE_GRAPH\",\n" +
      "    \"path\":\"" + tempOutDir.replace("\\", "\\\\") + "\"\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().runPostprocessing(configJson, inputGraph);

    File dir = new File(tempOutDir);
    assertTrue(dir.exists());
    assertTrue(Objects.requireNonNull(dir.list()).length > 0);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingConfigurationForWriteGraphWithIncompatibleType() throws IOException, JSONException {
    String tempOutDir = Files.createTempDirectory("postprocessingOutput").toString();

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"WRITE_GRAPH\",\n" +
      "    \"path\":\"" + tempOutDir + "\"\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson, FamerConfiguration.DataType.PERFECT_MAPPING);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingConfigurationForWriteGraphWithInvalidPath() throws IOException, JSONException {
    String tempOutDir = Files.createTempDirectory("postprocessingOutput").toString() + "/unknown";

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"WRITE_GRAPH\",\n" +
      "    \"path\":\"" + tempOutDir + "\"\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson);
  }

  // Tests for quality computation on similarity graph without and with golden truth file

  @Test
  public void testRunPostprocessingForComputeQualityOnSimilarity() throws IOException, JSONException {
    String graphString = "input[" +
      "(alise:Person {id:0, gtId:\"gt1\"})" +
      "(alice:Person {id:1, gtId:\"gt1\"})" +
      "(bob:Person {id:2, gtId:\"gt2\"})" +
      "(bonny:Person {id:3, gtId:\"gt3\"})" +
      "(alise)-[]->(alice)" +
      "(bob)-[]->(bonny)" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    String tempQualityOutDir = Files.createTempDirectory("postprocessingQualityOutput").toString();
    String tempQualityFile = tempQualityOutDir + File.separator + "quality_results.csv";

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"QUALITY\",\n" +
      "    \"path\":\"" + tempQualityFile.replace("\\", "\\\\") + "\",\n" +
      "    \"config\":{\n" +
      "      \"evalMethod\":\"SIMILARITY\",\n" +
      "      \"goldenTruthIdProperty\":\"gtId\"\n" +
      "    }\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    PostprocessingConfiguration postprocessingConfiguration = new PostprocessingConfiguration();
    postprocessingConfiguration.checkPostprocessingConfig(configJson, FamerConfiguration.DataType.LOGICAL_GRAPH);
    postprocessingConfiguration.runPostprocessing(configJson, inputGraph);

    File qualityFile = new File(tempQualityFile);
    assertTrue(qualityFile.exists());
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingForComputeQualityOnSimilarityForMissingPath() throws JSONException {
    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"QUALITY\",\n" +
      "    \"config\":{\n" +
      "      \"evalMethod\":\"SIMILARITY\",\n" +
      "      \"goldenTruthIdProperty\":\"gtId\"\n" +
      "    }\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingForComputeQualityOnSimilarityForMissingConfig() throws JSONException,
    IOException {
    String tempQualityOutDir = Files.createTempDirectory("postprocessingQualityOutput").toString();
    String tempQualityFile = tempQualityOutDir + File.separator + "quality_results.csv";

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"QUALITY\",\n" +
      "    \"path\":\"" + tempQualityFile.replace("\\", "\\\\") + "\",\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckPostprocessingForComputeQualityOnSimilarityForIncompatibleType() throws JSONException,
    IOException {
    String tempQualityOutDir = Files.createTempDirectory("postprocessingQualityOutput").toString();
    String tempQualityFile = tempQualityOutDir + File.separator + "quality_results.csv";

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"QUALITY\",\n" +
      "    \"path\":\"" + tempQualityFile.replace("\\", "\\\\") + "\",\n" +
      "    \"config\":{\n" +
      "      \"evalMethod\":\"SIMILARITY\",\n" +
      "      \"goldenTruthIdProperty\":\"gtId\"\n" +
      "    }\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    new PostprocessingConfiguration().checkPostprocessingConfig(configJson, FamerConfiguration.DataType.GRAPH_COLLECTION);
  }

  @Test
  public void testRunPostprocessingForComputeQualityOnSimilarityWithGT() throws IOException, JSONException {
    String graphString = "input[" +
      "(alise:Person {id:0})" +
      "(alice:Person {id:1})" +
      "(bob:Person {id:2})" +
      "(bonny:Person {id:3})" +
      "(alise)-[]->(alice)" +
      "(bob)-[]->(bonny)" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    // write the gt file as type MATCHED_PAIRS
    String gtString = "0,1";
    File tempGTFile = File.createTempFile("simGTFile", ".csv");
    FileOutputStream os = new FileOutputStream(tempGTFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(gtString);
    bw.close();

    String tempQualityOutDir = Files.createTempDirectory("postprocessingQualityOutput").toString();
    String tempQualityFile = tempQualityOutDir + File.separator + "quality_results.csv";

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"QUALITY\",\n" +
      "    \"path\":\"" + tempQualityFile.replace("\\", "\\\\") + "\",\n" +
      "    \"config\":{\n" +
      "      \"evalMethod\":\"SIMILARITY_GT\",\n" +
      "      \"gtFilePath\":\"" + tempGTFile.toString().replace("\\", "\\\\") + "\",\n" +
      "      \"splitter\":\",\",\n" +
      "      \"gtFileType\":\"MATCHED_PAIRS\",\n" +
      "      \"entityIdProperty\":\"id\"\n" +
      "    }\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    PostprocessingConfiguration postprocessingConfiguration = new PostprocessingConfiguration();
    postprocessingConfiguration.checkPostprocessingConfig(configJson, FamerConfiguration.DataType.LOGICAL_GRAPH);
    postprocessingConfiguration.runPostprocessing(configJson, inputGraph);

    File qualityFile = new File(tempQualityFile);
    assertTrue(qualityFile.exists());
  }

  // Tests for quality computation on clustering without and with golden truth file

  @Test
  public void testRunPostprocessingForComputeQualityOnClustering() throws IOException, JSONException {
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\", gtId: \"gt1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\", gtId: \"gt2\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\", gtId: \"gt2\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\", gtId: \"gt3\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c3\", gtId: \"gt3\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c3\", gtId: \"gt3\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\", gtId: \"gt3\"})" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");

    String tempQualityOutDir = Files.createTempDirectory("postprocessingQualityOutput").toString();
    String tempQualityFile = tempQualityOutDir + File.separator + "quality_results.csv";

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"QUALITY\",\n" +
      "    \"path\":\"" + tempQualityFile.replace("\\", "\\\\") + "\",\n" +
      "    \"config\":{\n" +
      "      \"evalMethod\":\"CLUSTERING\",\n" +
      "      \"goldenTruthIdProperty\":\"gtId\",\n" +
      "      \"hasOverlap\":\"false\"\n" +
      "    }\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    PostprocessingConfiguration postprocessingConfiguration = new PostprocessingConfiguration();
    postprocessingConfiguration.checkPostprocessingConfig(configJson, FamerConfiguration.DataType.LOGICAL_GRAPH);
    postprocessingConfiguration.runPostprocessing(configJson, inputGraph);

    File qualityFile = new File(tempQualityFile);
    assertTrue(qualityFile.exists());
  }

  @Test
  public void testRunPostprocessingForComputeQualityOnClusteringGT() throws IOException, JSONException {
    String graphString = "clusteredGraph[" +
      "(v0 {id:0, " + CLUSTER_ID + ": \"c1\"})" +
      "(v1 {id:1, " + CLUSTER_ID + ": \"c1\"})" +
      "(v2 {id:2, " + CLUSTER_ID + ": \"c1\"})" +
      "(v3 {id:3, " + CLUSTER_ID + ": \"c2\"})" +
      "(v4 {id:4, " + CLUSTER_ID + ": \"c3\"})" +
      "(v5 {id:5, " + CLUSTER_ID + ": \"c3\"})" +
      "(v6 {id:6, " + CLUSTER_ID + ": \"c3\"})" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("clusteredGraph");

    // write the gt file as type MATCHED_PAIRS
    String gtString = "1,2\n" +
      "3,4\n" +
      "3,5\n" +
      "3,6\n" +
      "4,5\n" +
      "4,6\n" +
      "5,6";
    File tempGTFile = File.createTempFile("cluGTFile", ".csv");
    FileOutputStream os = new FileOutputStream(tempGTFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(gtString);
    bw.close();

    String tempQualityOutDir = Files.createTempDirectory("postprocessingQualityOutput").toString();
    String tempQualityFile = tempQualityOutDir + File.separator + "quality_results.csv";

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"QUALITY\",\n" +
      "    \"path\":\"" + tempQualityFile.replace("\\", "\\\\") + "\",\n" +
      "    \"config\":{\n" +
      "      \"evalMethod\":\"CLUSTERING_GT\",\n" +
      "      \"gtFilePath\":\"" + tempGTFile.toString().replace("\\", "\\\\") + "\",\n" +
      "      \"splitter\":\",\",\n" +
      "      \"gtFileType\":\"MATCHED_PAIRS\",\n" +
      "      \"entityIdProperty\":\"id\",\n" +
      "      \"hasOverlap\":\"false\"\n" +
      "    }\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    PostprocessingConfiguration postprocessingConfiguration = new PostprocessingConfiguration();
    postprocessingConfiguration.checkPostprocessingConfig(configJson, FamerConfiguration.DataType.LOGICAL_GRAPH);
    postprocessingConfiguration.runPostprocessing(configJson, inputGraph);

    File qualityFile = new File(tempQualityFile);
    assertTrue(qualityFile.exists());
  }

  // Test for combined postprocessing tasks

  @Test
  public void testRunPostprocessingForSeveralTasks() throws IOException, JSONException {
    String graphString = "input[" +
      "(alise:Person {id:0, gtId:\"gt1\"})" +
      "(alice:Person {id:1, gtId:\"gt1\"})" +
      "(bob:Person {id:2, gtId:\"gt2\"})" +
      "(bonny:Person {id:3, gtId:\"gt3\"})" +
      "(alise)-[]->(alice)" +
      "(bob)-[]->(bonny)" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    String tempGraphOutDir = Files.createTempDirectory("postprocessingGraphOutput").toString();
    String tempQualityOutDir = Files.createTempDirectory("postprocessingQualityOutput").toString();
    String tempQualityFile = tempQualityOutDir + File.separator + "quality_results.csv";

    String postConfig = "[\n" +
      "  {\n" +
      "    \"task\":\"WRITE_GRAPH\",\n" +
      "    \"path\":\"" + tempGraphOutDir.replace("\\", "\\\\") + "\"\n" +
      "  },\n" +
      "  {\n" +
      "    \"task\":\"QUALITY\",\n" +
      "    \"path\":\"" + tempQualityFile.replace("\\", "\\\\") + "\",\n" +
      "    \"config\":{\n" +
      "      \"evalMethod\":\"SIMILARITY\",\n" +
      "      \"goldenTruthIdProperty\":\"gtId\"\n" +
      "    }\n" +
      "  }\n" +
      "]";
    JSONArray configJson = new JSONArray(postConfig);

    PostprocessingConfiguration postprocessingConfiguration = new PostprocessingConfiguration();
    postprocessingConfiguration.checkPostprocessingConfig(configJson, FamerConfiguration.DataType.LOGICAL_GRAPH);
    postprocessingConfiguration.runPostprocessing(configJson, inputGraph);

    // check if graph was written
    File graphDir = new File(tempGraphOutDir);
    assertTrue(graphDir.exists());
    assertTrue(Objects.requireNonNull(graphDir.list()).length > 0);
    // check if quality results were written
    File qualityFile = new File(tempQualityFile);
    assertTrue(qualityFile.exists());
  }
}
