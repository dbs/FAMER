/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.clustering;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.famer.clustering.postprocessing.AbstractClusterPostprocessing;
import org.gradoop.famer.clustering.postprocessing.clusterRepair.overlapResolve.OverlapResolveNoMerge;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * JUnit tests for cluster postprocessing algorithm instantiation in {@link ClusteringConfiguration}.
 */
public class PostprocessingAlgorithmInstantiationTest extends GradoopFlinkTestBase {

  private final ClusteringConfiguration clusteringConfiguration = new ClusteringConfiguration();

  @Test
  public void testBuildClusterPostprocessingAlgorithmForOverlapResolveNoMerge() throws JSONException {
    String json = "{\n" +
      "   \"postprocessingMethod\":\"OVERLAP_RESOLVE_NO_MERGE\",\n" +
      "   \"delta\":\"0.5\",\n" +
      "   \"runPhase2\":false\n" +
      "}";
    JSONObject postprocessConfigJson = new JSONObject(json);

    AbstractClusterPostprocessing postprocessingAlgorithm =
      clusteringConfiguration.instantiatePostprocessingAlgorithm(postprocessConfigJson);

    assertNotNull(postprocessingAlgorithm);
    assertTrue(postprocessingAlgorithm instanceof OverlapResolveNoMerge);
    assertEquals(0.5, ((OverlapResolveNoMerge) postprocessingAlgorithm).getDelta(), 0d);
    assertFalse(((OverlapResolveNoMerge) postprocessingAlgorithm).isRunPhase2());
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusterPostprocessingAlgorithmForOverlapResolveNoMergeWithMissingValues() throws
    JSONException {
    String json = "{\n" +
      "   \"postprocessingMethod\":\"OVERLAP_RESOLVE_NO_MERGE\",\n" +
      "   \"runPhase2\":false\n" +
      "}";
    JSONObject postprocessConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiatePostprocessingAlgorithm(postprocessConfigJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusterPostprocessingAlgorithmForOverlapResolveNoMergeWithWrongValues1() throws
    JSONException {
    String json = "{\n" +
      "   \"postprocessingMethod\":\"OVERLAP_RESOLVE_NO_MERGE\",\n" +
      "   \"delta\":\"a\",\n" +
      "   \"runPhase2\":false\n" +
      "}";
    JSONObject postprocessConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiatePostprocessingAlgorithm(postprocessConfigJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testBuildClusterPostprocessingAlgorithmForOverlapResolveNoMergeWithWrongValues2() throws
    JSONException {
    String json = "{\n" +
      "   \"postprocessingMethod\":\"OVERLAP_RESOLVE_NO_MERGE\",\n" +
      "   \"delta\":\"0.5\",\n" +
      "   \"runPhase2\":a\n" +
      "}";
    JSONObject postprocessConfigJson = new JSONObject(json);

    clusteringConfiguration.instantiatePostprocessingAlgorithm(postprocessConfigJson);
  }
}
