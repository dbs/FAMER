/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.configuration.linking;

import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.configuration.dataStructures.FamerConfigException;
import org.gradoop.famer.configuration.linking.blocking.BlockingConfiguration;
import org.gradoop.famer.configuration.linking.selection.SelectionConfiguration;
import org.gradoop.famer.configuration.linking.similarity.SimilarityConfiguration;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.stream.Collectors;

import static org.gradoop.famer.linking.linking.Linker.GRAPH_LABEL;
import static org.gradoop.famer.linking.linking.Linker.SIM_VALUE;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

/**
 * JUnit tests for {@link LinkingConfiguration}.
 * For comparison we are recreating the data used in LinkerTest from famer-linking mdoule.
 */
public class LinkingConfigurationTest extends GradoopFlinkTestBase {

  private static final String GRAPH_STRING =
    "input[" + "(alice:Person {id:1, name:\"Alice\", key: \"alice\", " + GRAPH_LABEL + ":\"g1\"})" +
      "(allan:Person {id:2, name:\"Allan\", key: \"allan\", " + GRAPH_LABEL + ":\"g2\"})" +
      "(bob:Person {id:3, name:\"Bob\", key: \"bob\", " + GRAPH_LABEL + ":\"g3\"})" +
      "(carol:Person {id:4, name:\"Carol\", key: \"carol\", " + GRAPH_LABEL + ":\"g4\"})" +
      "(bob)-[]->(carol)" +
      "]";

  private static final String LINKING_CONFIG = "{\n" +
    " \"keepCurrentEdges\":\"true\"," +
    " \"recomputeSimilarityForCurrentEdges\":\"true\"," +
    " \"blockingComponents\":[" +
    "   {\n" +
    "     \"blockingMethod\":\"CartesianProductComponent\"," +
    "     \"keyGenerationComponent\":{" +
    "       \"keyGenerationMethod\":\"FullAttributeComponent\"," +
    "       \"attribute\":\"key\"" +
    "     }\n" +
    "   }\n" +
    " ]," +
    " \"similarityComponents\":[" +
    "   {" +
    "     \"id\":\"simName\",\n" +
    "     \"sourceGraph\":\"*\",\n" +
    "     \"targetGraph\":\"*\",\n" +
    "     \"sourceLabel\":\"Person\",\n" +
    "     \"targetLabel\":\"Person\",\n" +
    "     \"sourceAttribute\":\"name\",\n" +
    "     \"targetAttribute\":\"name\",\n" +
    "     \"weight\":\"1\",\n" +
    "     \"similarityMethod\":\"TruncateBeginComponent\"," +
    "     \"length\":\"2\"" +
    "   }" +
    " ]," +
    " \"selectionComponent\":{" +
    "   \"selectionMethod\":\"MANUAL\",\n" +
    "   \"aggregationRule\":{\n" +
    "     \"aggregationThreshold\":\"0\",\n" +
    "     \"ruleComponents\":[\n" +
    "       {\n" +
    "          \"componentType\":\"SIMILARITY_FIELD_ID\",\n" +
    "          \"value\":\"simName\"\n" +
    "       },\n" +
    "       {\n" +
    "          \"componentType\":\"ARITHMETIC_OPERATOR\",\n" +
    "          \"value\":\"PLUS\"\n" +
    "       },\n" +
    "       {\n" +
    "          \"componentType\":\"CONSTANT\",\n" +
    "          \"value\":\"1\"\n" +
    "       }\n" +
    "     ]\n" +
    "   },\n" +
    "   \"selectionRule\":{\n" +
    "     \"ruleComponents\":[\n" +
    "       {\n" +
    "         \"componentType\":\"CONDITION\",\n" +
    "         \"conditionId\":\"greaterEqual\",\n" +
    "         \"similarityFieldId\":\"simName\",\n" +
    "         \"operator\":\"GREATER_EQUAL\",\n" +
    "         \"threshold\":\"1\"\n" +
    "       }\n" +
    "     ]\n" +
    "   }\n" +
    " }\n" +
    "}";

  @Test
  public void testCheckConfigAndBuildAllComponentsForJsonConfig() throws Exception {
    JSONObject configJson = new JSONObject(LINKING_CONFIG);
    LinkingConfiguration linkingConfiguration =
      new LinkingConfiguration().checkConfigAndBuildComponents(configJson);

    assertNotNull(linkingConfiguration.getBlockingComponents());
    assertNotNull(linkingConfiguration.getSimilarityComponents());
    assertNotNull(linkingConfiguration.getSelectionComponent());
    assertNotNull(linkingConfiguration.getLinkerComponent());
    assertNotNull(linkingConfiguration.getLinker());
  }

  @Test
  public void testCheckConfigAndBuildAllComponentsForConfigFile() throws Exception {
    File tempFile = File.createTempFile("linkingConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(LINKING_CONFIG);
    bw.close();

    LinkingConfiguration linkingConfiguration =
      new LinkingConfiguration().checkConfigAndBuildComponents(tempFile.getPath());

    assertNotNull(linkingConfiguration.getBlockingComponents());
    assertNotNull(linkingConfiguration.getSimilarityComponents());
    assertNotNull(linkingConfiguration.getSelectionComponent());
    assertNotNull(linkingConfiguration.getLinkerComponent());
    assertNotNull(linkingConfiguration.getLinker());
  }

  @Test
  public void testCheckConfigAndBuildBlockingComponentsForJsonConfig() throws Exception {
    JSONObject configJson = new JSONObject(LINKING_CONFIG);

    BlockingConfiguration blockingConfiguration =
      new BlockingConfiguration().checkConfigAndBuildComponents(configJson);

    assertNotNull(blockingConfiguration.getBlockingComponents());
  }

  @Test
  public void testCheckConfigAndBuildBlockingComponentsForConfigFile() throws Exception {
    File tempFile = File.createTempFile("linkingConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(LINKING_CONFIG);
    bw.close();

    BlockingConfiguration blockingConfiguration =
      new BlockingConfiguration().checkConfigAndBuildComponents(tempFile.getPath());

    assertNotNull(blockingConfiguration.getBlockingComponents());
  }

  @Test
  public void testCheckConfigAndBuildSimilarityComponentsForJsonConfig() throws Exception {
    JSONObject configJson = new JSONObject(LINKING_CONFIG);

    SimilarityConfiguration similarityConfiguration =
      new SimilarityConfiguration().checkConfigAndBuildComponents(configJson);

    assertNotNull(similarityConfiguration.getSimilarityComponents());
  }

  @Test
  public void testCheckConfigAndBuildSimilarityComponentsForConfigFile() throws Exception {
    File tempFile = File.createTempFile("linkingConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(LINKING_CONFIG);
    bw.close();

    SimilarityConfiguration similarityConfiguration =
      new SimilarityConfiguration().checkConfigAndBuildComponents(tempFile.getPath());

    assertNotNull(similarityConfiguration.getSimilarityComponents());
  }

  @Test
  public void testCheckConfigAndBuildSelectionComponentForJsonConfig() throws Exception {
    JSONObject configJson = new JSONObject(LINKING_CONFIG);

    SelectionConfiguration selectionConfiguration =
      new SelectionConfiguration().checkConfigAndBuildComponents(configJson);

    assertNotNull(selectionConfiguration.getSelectionComponent());
  }

  @Test
  public void testCheckConfigAndBuildSelectionComponentForConfigFile() throws Exception {
    File tempFile = File.createTempFile("linkingConfig", ".json");

    FileOutputStream os = new FileOutputStream(tempFile, true);
    BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
    bw.append(LINKING_CONFIG);
    bw.close();

    SelectionConfiguration selectionConfiguration =
      new SelectionConfiguration().checkConfigAndBuildComponents(tempFile.getPath());

    assertNotNull(selectionConfiguration.getSelectionComponent());
  }

  @Test
  public void testRunLinking() throws Exception {
    LogicalGraph inputGraph = getLoaderFromString(GRAPH_STRING).getLogicalGraphByVariable("input");

    JSONObject configJson = new JSONObject(LINKING_CONFIG);

    LinkingConfiguration linkingConfiguration =
      new LinkingConfiguration().checkConfigAndBuildComponents(configJson);

    GraphCollection inputAsCollection = getConfig().getGraphCollectionFactory().fromGraph(inputGraph);

    LogicalGraph linkedGraph = linkingConfiguration.runLinking(inputAsCollection);

    assertNotNull(linkedGraph);

    // expect 1 similarity edge between Alice with simValue 2.0

    assertEquals(4L, linkedGraph.getVertices().count());
    assertEquals(1L, linkedGraph.getEdges().count());

    EPGMEdge edge = linkedGraph.getEdges().collect().get(0);

    assertEquals(2d, edge.getPropertyValue(SIM_VALUE).getDouble(), 0d);

    List<EPGMVertex> vertices = linkedGraph.getVertices().collect();
    EPGMVertex source =
      vertices.stream().filter(v -> v.getId().equals(edge.getSourceId())).collect(Collectors.toList()).get(0);
    EPGMVertex target =
      vertices.stream().filter(v -> v.getId().equals(edge.getTargetId())).collect(Collectors.toList()).get(0);

    assertEquals("Alice", source.getPropertyValue("name").getString());
    assertEquals("Allan", target.getPropertyValue("name").getString());
  }

  @Test(expected = FamerConfigException.class)
  public void testRunLinkingWithoutCheckAndBuild() throws Exception {
    LogicalGraph inputGraph = getLoaderFromString(GRAPH_STRING).getLogicalGraphByVariable("input");
    GraphCollection inputAsCollection = getConfig().getGraphCollectionFactory().fromGraph(inputGraph);

    new LinkingConfiguration().runLinking(inputAsCollection);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckConfigWithMissingBlockingComponents() throws JSONException {
    String linkingConfig = "{\n" +
      " \"keepCurrentEdges\":\"true\"," +
      " \"recomputeSimilarityForCurrentEdges\":\"true\"," +
      " \"similarityComponents\":[" +
      "   {" +
      "     \"id\":\"simName\",\n" +
      "     \"sourceGraph\":\"*\",\n" +
      "     \"targetGraph\":\"*\",\n" +
      "     \"sourceLabel\":\"Person\",\n" +
      "     \"targetLabel\":\"Person\",\n" +
      "     \"sourceAttribute\":\"name\",\n" +
      "     \"targetAttribute\":\"name\",\n" +
      "     \"weight\":\"1\",\n" +
      "     \"similarityMethod\":\"TruncateBeginComponent\"," +
      "     \"length\":\"2\"" +
      "   }" +
      " ]," +
      " \"selectionComponent\":{" +
      "   \"selectionMethod\":\"MANUAL\",\n" +
      "   \"aggregationRule\":{\n" +
      "     \"aggregationThreshold\":\"0\",\n" +
      "     \"ruleComponents\":[\n" +
      "       {\n" +
      "          \"componentType\":\"SIMILARITY_FIELD_ID\",\n" +
      "          \"value\":\"simName\"\n" +
      "       },\n" +
      "       {\n" +
      "          \"componentType\":\"ARITHMETIC_OPERATOR\",\n" +
      "          \"value\":\"PLUS\"\n" +
      "       },\n" +
      "       {\n" +
      "          \"componentType\":\"CONSTANT\",\n" +
      "          \"value\":\"1\"\n" +
      "       }\n" +
      "     ]\n" +
      "   },\n" +
      "   \"selectionRule\":{\n" +
      "     \"ruleComponents\":[\n" +
      "       {\n" +
      "         \"componentType\":\"CONDITION\",\n" +
      "         \"conditionId\":\"greaterEqual\",\n" +
      "         \"similarityFieldId\":\"simName\",\n" +
      "         \"operator\":\"GREATER_EQUAL\",\n" +
      "         \"threshold\":\"1\"\n" +
      "       }\n" +
      "     ]\n" +
      "   }\n" +
      " }\n" +
      "}";
    JSONObject configJson = new JSONObject(linkingConfig);
    new LinkingConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckConfigWithMissingSimilarityComponents() throws JSONException {
    String linkingConfig = "{\n" +
      " \"keepCurrentEdges\":\"true\"," +
      " \"recomputeSimilarityForCurrentEdges\":\"true\"," +
      " \"blockingComponents\":[" +
      "   {\n" +
      "     \"blockingMethod\":\"CartesianProductComponent\"," +
      "     \"keyGenerationComponent\":{" +
      "       \"keyGenerationMethod\":\"FullAttributeComponent\"," +
      "       \"attribute\":\"key\"" +
      "     }\n" +
      "   }\n" +
      " ]," +
      " \"selectionComponent\":{" +
      "   \"selectionMethod\":\"MANUAL\",\n" +
      "   \"aggregationRule\":{\n" +
      "     \"aggregationThreshold\":\"0\",\n" +
      "     \"ruleComponents\":[\n" +
      "       {\n" +
      "          \"componentType\":\"SIMILARITY_FIELD_ID\",\n" +
      "          \"value\":\"simName\"\n" +
      "       },\n" +
      "       {\n" +
      "          \"componentType\":\"ARITHMETIC_OPERATOR\",\n" +
      "          \"value\":\"PLUS\"\n" +
      "       },\n" +
      "       {\n" +
      "          \"componentType\":\"CONSTANT\",\n" +
      "          \"value\":\"1\"\n" +
      "       }\n" +
      "     ]\n" +
      "   },\n" +
      "   \"selectionRule\":{\n" +
      "     \"ruleComponents\":[\n" +
      "       {\n" +
      "         \"componentType\":\"CONDITION\",\n" +
      "         \"conditionId\":\"greaterEqual\",\n" +
      "         \"similarityFieldId\":\"simName\",\n" +
      "         \"operator\":\"GREATER_EQUAL\",\n" +
      "         \"threshold\":\"1\"\n" +
      "       }\n" +
      "     ]\n" +
      "   }\n" +
      " }\n" +
      "}";
    JSONObject configJson = new JSONObject(linkingConfig);

    new LinkingConfiguration().checkConfigAndBuildComponents(configJson);
  }

  @Test(expected = FamerConfigException.class)
  public void testCheckConfigWithMissingSelectionComponent() throws JSONException {
    String linkingConfig = "{\n" +
      " \"keepCurrentEdges\":\"true\"," +
      " \"recomputeSimilarityForCurrentEdges\":\"true\"," +
      " \"blockingComponents\":[" +
      "   {\n" +
      "     \"blockingMethod\":\"CartesianProductComponent\"," +
      "     \"keyGenerationComponent\":{" +
      "       \"keyGenerationMethod\":\"FullAttributeComponent\"," +
      "       \"attribute\":\"key\"" +
      "     }\n" +
      "   }\n" +
      " ]," +
      " \"similarityComponents\":[" +
      "   {" +
      "     \"id\":\"simName\",\n" +
      "     \"sourceGraph\":\"*\",\n" +
      "     \"targetGraph\":\"*\",\n" +
      "     \"sourceLabel\":\"Person\",\n" +
      "     \"targetLabel\":\"Person\",\n" +
      "     \"sourceAttribute\":\"name\",\n" +
      "     \"targetAttribute\":\"name\",\n" +
      "     \"weight\":\"1\",\n" +
      "     \"similarityMethod\":\"TruncateBeginComponent\"," +
      "     \"length\":\"2\"" +
      "   }" +
      " ]" +
      "}";
    JSONObject configJson = new JSONObject(linkingConfig);

    new LinkingConfiguration().checkConfigAndBuildComponents(configJson);
  }
}
