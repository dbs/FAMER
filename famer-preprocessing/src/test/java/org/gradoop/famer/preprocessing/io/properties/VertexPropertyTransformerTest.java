/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.properties;

import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.api.java.tuple.Tuple4;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * JUnit tests for {@link VertexPropertyTransformer}
 */
public class VertexPropertyTransformerTest extends GradoopFlinkTestBase {

  @Test
  public void testCombinePropertiesForLogicalGraph() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, a:\"foo\", b:1, c:1.2, surname:\"Alice\", lastname:\"A\"})" +
      "(dave:Person {id:2, a:\"bar\", c:2.1})" +
      "(leipzig:Place {id:3, t:\"city\", lat:51.33962d, lon:12.37129d})" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");
    // combine a, b, c -> abc; delete a, b, c
    List<Tuple2<String, Boolean>> propertyNamesABC = Arrays.asList(
      Tuple2.of("a", false),
      Tuple2.of("b", false),
      Tuple2.of("c", false)
    );
    String newPropABC = "abc";
    // combine surname, lastname -> name; delete surname, lastname
    List<Tuple2<String, Boolean>> propertyNamesName = Arrays.asList(
      Tuple2.of("surname", false),
      Tuple2.of("lastname", false)
    );
    String newPropName = "name";
    // combine lat, lon -> pos; keep lat, lon
    List<Tuple2<String, Boolean>> propertyNamesPos = Arrays.asList(
      Tuple2.of("lat", true),
      Tuple2.of("lon", true)
    );
    String newPropPos = "pos";

    List<Tuple3<String, String, List<Tuple2<String, Boolean>>>> propertyNames = Arrays.asList(
      Tuple3.of("", newPropABC, propertyNamesABC),
      Tuple3.of("", newPropName, propertyNamesName),
      Tuple3.of("", newPropPos, propertyNamesPos)
    );

    LogicalGraph result =
      new VertexPropertyTransformer().combineProperties(inputGraph, propertyNames);

    assertNotNull(result);
    assertEquals(3L, result.getVertices().count());

    result.getVertices().collect().forEach(vertex -> {
      if (vertex.getPropertyValue("id").getInt() == 1) {
        assertTrue(vertex.hasProperty(newPropABC));
        List<PropertyValue> abcVal = vertex.getPropertyValue(newPropABC).getList();
        assertEquals(3, abcVal.size());
        assertTrue(abcVal.get(0).isString());
        assertTrue(abcVal.get(1).isInt());
        assertTrue(abcVal.get(2).isFloat());

        assertFalse(vertex.hasProperty("a"));
        assertFalse(vertex.hasProperty("b"));
        assertFalse(vertex.hasProperty("c"));

        assertTrue(vertex.hasProperty(newPropName));
        List<PropertyValue> nameVal = vertex.getPropertyValue(newPropName).getList();
        assertEquals(2, nameVal.size());
        assertTrue(nameVal.get(0).isString());
        assertTrue(nameVal.get(1).isString());

        assertFalse(vertex.hasProperty("surname"));
        assertFalse(vertex.hasProperty("lastname"));
      }
      if (vertex.getPropertyValue("id").getInt() == 2) {
        assertFalse(vertex.hasProperty(newPropABC));
        assertTrue(vertex.hasProperty("a"));
        assertTrue(vertex.hasProperty("c"));
      }
      if (vertex.getPropertyValue("id").getInt() == 3) {
        assertTrue(vertex.hasProperty(newPropPos));
        List<PropertyValue> posVal = vertex.getPropertyValue(newPropPos).getList();
        assertEquals(2, posVal.size());
        assertTrue(posVal.get(0).isDouble());
        assertTrue(posVal.get(1).isDouble());

        assertTrue(vertex.hasProperty("lat"));
        assertTrue(vertex.hasProperty("lon"));
      }
    });
  }

  @Test
  public void testCombinePropertiesForGraphCollection() throws Exception {
    String graphString = "input1[" +
      "(alice:Person {id:1, a:\"foo\", b:1, surname:\"Alice\", lastname:\"A\", graphLabel:\"1\"})" +
      "]" +
      "input2[" +
      "(dave:Person {id:2, a:\"bar\", b:2, surname:\"Dave\", lastname:\"D\", graphLabel:\"2\"})" +
      "]";
    GraphCollection inputCollection = getLoaderFromString(graphString)
      .getGraphCollectionByVariables("input1", "input2");

    // combine a, b -> abc; delete a, b
    List<Tuple2<String, Boolean>> propertyNamesAB = Arrays.asList(
      Tuple2.of("a", false),
      Tuple2.of("b", false)
    );
    String newPropAB = "ab";

    // combine surname, lastname -> name; delete surname, lastname
    List<Tuple2<String, Boolean>> propertyNamesName = Arrays.asList(
      Tuple2.of("surname", false),
      Tuple2.of("lastname", false)
    );
    String newPropName = "name";

    // combine id, graphLabel -> ident; keep both
    List<Tuple2<String, Boolean>> propertyNamesIdent = Arrays.asList(
      Tuple2.of("id", true),
      Tuple2.of("graphLabel", true)
    );
    String newPropIdent = "ident";

    List<Tuple3<String, String, List<Tuple2<String, Boolean>>>> propertyNames = Arrays.asList(
      Tuple3.of("1", newPropName, propertyNamesName),
      Tuple3.of("2", newPropAB, propertyNamesAB),
      Tuple3.of("", newPropIdent, propertyNamesIdent) // graphLabel empty -> apply on both graphs
    );

    GraphCollection result =
      new VertexPropertyTransformer().combineProperties(inputCollection, propertyNames);

    assertNotNull(result);
    assertEquals(2L, result.getVertices().count());

    result.getVertices().collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty(newPropIdent));
      assertTrue(vertex.hasProperty("id"));
      assertTrue(vertex.hasProperty("graphLabel"));
      if (vertex.getPropertyValue("id").getInt() == 1) {
        assertTrue(vertex.hasProperty(newPropName));
        assertFalse(vertex.hasProperty("surname"));
        assertFalse(vertex.hasProperty("lastname"));

        assertFalse(vertex.hasProperty(newPropAB));
        assertTrue(vertex.hasProperty("a"));
        assertTrue(vertex.hasProperty("b"));
      }
      if (vertex.getPropertyValue("id").getInt() == 2) {
        assertFalse(vertex.hasProperty(newPropName));
        assertTrue(vertex.hasProperty("surname"));
        assertTrue(vertex.hasProperty("lastname"));

        assertTrue(vertex.hasProperty(newPropAB));
        assertFalse(vertex.hasProperty("a"));
        assertFalse(vertex.hasProperty("b"));
      }
    });
  }

  @Test
  public void testRenamePropertiesForLogicalGraph() throws Exception {
    String graphString = "input[" +
      "(alice:Person {id:1, surname:\"Alice\"})" +
      "(bob:Person {id:2, name:\"Bob\"})" +
      "(dave:Person {id:3, alias:\"Dave\"})" +
      "]";
    LogicalGraph inputGraph = getLoaderFromString(graphString).getLogicalGraphByVariable("input");

    List<Tuple4<String, String, String, Boolean>> propertyNames = Arrays.asList(
      Tuple4.of("", "name", "surname", false),
      Tuple4.of("", "alias", "surname", true),
      Tuple4.of("", "foo", "bar", true)
    );

    LogicalGraph result = new VertexPropertyTransformer().renameProperties(inputGraph, propertyNames);

    assertNotNull(result);
    assertEquals(3L, result.getVertices().count());

    result.getVertices().collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty("surname"));
      assertFalse(vertex.hasProperty("bar"));
      if (vertex.getPropertyValue("id").getInt() == 2) {
        assertFalse(vertex.hasProperty("name"));
      }
      if (vertex.getPropertyValue("id").getInt() == 3) {
        assertTrue(vertex.hasProperty("alias"));
      }
    });
  }

  @Test
  public void testRenamePropertiesForGraphCollection() throws Exception {
    String graphString = "input1[" +
      "(alice:Person {id:1, name:\"Alice\", graphLabel:\"1\"})" +
      "(bob:Person {id:2, name1:\"Bob\", graphLabel:\"1\"})" +
      "(leipzig:Place {id:3, t:\"city\", lat:51.33962, lon:12.37129, graphLabel:\"1\"})" +
      "]" +
      "input2[" +
      "(dave:Person {id:4, n:\"Dave\", graphLabel:\"2\"})" +
      "(eve:Person {id:5, n:\"Eve\", graphLabel:\"2\"})" +
      "(uniLE:Place {id:6, type:\"university\", address:\"Augustusplatz\", graphLabel:\"2\"})" +
      "]";
    GraphCollection inputCollection = getLoaderFromString(graphString)
      .getGraphCollectionByVariables("input1", "input2");

    List<Tuple4<String, String, String, Boolean>> propertyNames = Arrays.asList(
      Tuple4.of("1", "name1", "name", false),
      Tuple4.of("1", "t", "type", false),
      Tuple4.of("2", "n", "name", false),
      Tuple4.of("", "id", "ident", true) // apply renaming for all graphs in collection
    );

    GraphCollection result = new VertexPropertyTransformer().renameProperties(inputCollection, propertyNames);

    assertNotNull(result);
    assertEquals(6L, result.getVertices().count());

    result.getVerticesByLabel("Person").collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty("id"));
      assertTrue(vertex.hasProperty("ident"));
      assertTrue(vertex.hasProperty("name"));
      assertFalse(vertex.hasProperty("n"));
      assertFalse(vertex.hasProperty("name1"));
    });

    result.getVerticesByLabel("Place").collect().forEach(vertex -> {
      assertTrue(vertex.hasProperty("id"));
      assertTrue(vertex.hasProperty("ident"));
      assertTrue(vertex.hasProperty("type"));
      assertFalse(vertex.hasProperty("t"));
    });
  }
}
