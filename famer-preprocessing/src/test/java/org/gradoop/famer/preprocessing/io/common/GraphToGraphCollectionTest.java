/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.common;

import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.junit.Test;

import java.util.Arrays;

/**
 * JUnit test of the {@link GraphToGraphCollection} class.
 */
public class GraphToGraphCollectionTest extends GradoopFlinkTestBase {

  @Test
  public void testExecuteGraphToGraphCollection() throws Exception {
    String graphString = "inputOne[" +
      "(a:A {foo : 42})" +
      "(b:B {foo : 42})" +
      "(a)-[:a {foo : 42}]->(b)-[:b {foo : 42}]->(a)" +
      "]" +
      "inputTwo[" +
      "(c:C {foo : 9})" +
      "(d:D {bla : 23})" +
      "(c)-[:c {foo : 4}]->(d)-[:d {bla : 23}]->(c)" +
      "]";

    LogicalGraph graph1 = getLoaderFromString(graphString).getLogicalGraphByVariable("inputOne");
    LogicalGraph graph2 = getLoaderFromString(graphString).getLogicalGraphByVariable("inputTwo");

    GraphCollection result = GraphToGraphCollection.execute(Arrays.asList(graph1, graph2));

    GraphCollection expected = getLoaderFromString(graphString)
      .getGraphCollectionByVariables("inputOne", "inputTwo");

    collectAndAssertTrue(result.equalsByGraphElementData(expected));
  }
}
