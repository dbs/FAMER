/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.generator.csv;

import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.io.LocalCollectionOutputFormat;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.pojo.EPGMVertexFactory;
import org.gradoop.flink.model.GradoopFlinkTestBase;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

/**
 * Class to test {@link CSVToVertex}.
 */
public class CSVToVertexTest extends GradoopFlinkTestBase {

  @Test
  public void createVertices() throws Exception {

    List<String> properties = Arrays.asList("id", "type", "name", "age");

    String vertexLine1 = "12345,\"person\",\"ann\",23";
    String vertexLine2 = "30117,\"person\",\"jacob\",22";
    String vertexLine3 = "11919,\"person\",\"carol\",24";
    String vertexLine4 = "98765,\"person\",\"bob\",23";

    DataSet<String> data = getConfig().getExecutionEnvironment().fromElements(vertexLine1, vertexLine2,
      vertexLine3, vertexLine4);

    DataSet<EPGMVertex> result = data.flatMap(new CSVToVertex(new EPGMVertexFactory(), properties));

    List<EPGMVertex> vertices = new ArrayList<>();
    result.output(new LocalCollectionOutputFormat<>(vertices));

    getExecutionEnvironment().execute();

    assertEquals(4, vertices.size());

    for (EPGMVertex v : vertices) {
      assertNotNull(v.getProperties());
      switch (String.valueOf(v.getPropertyValue("id"))) {
      case "12345":
        assertNotNull(v.getProperties());
        assertEquals(4, v.getProperties().size());
        assertEquals("person", v.getPropertyValue("type").getString());
        assertEquals("ann", v.getPropertyValue("name").getString());
        assertEquals("23", v.getPropertyValue("age").getString());
        break;
      case "30117":
        assertNotNull(v.getProperties());
        assertEquals(4, v.getProperties().size());
        assertEquals("person", v.getPropertyValue("type").getString());
        assertEquals("jacob", v.getPropertyValue("name").getString());
        assertEquals("22", v.getPropertyValue("age").getString());
        break;
      case "11919":
        assertNotNull(v.getProperties());
        assertEquals(4, v.getProperties().size());
        assertEquals("person", v.getPropertyValue("type").getString());
        assertEquals("carol", v.getPropertyValue("name").getString());
        assertEquals("24", v.getPropertyValue("age").getString());
        break;
      case "98765":
        assertNotNull(v.getProperties());
        assertEquals(4, v.getProperties().size());
        assertEquals("person", v.getPropertyValue("type").getString());
        assertEquals("bob", v.getPropertyValue("name").getString());
        assertEquals("23", v.getPropertyValue("age").getString());
        break;
      default:
        fail();
        break;
      }
    }
  }

  @Test
  public void createVertexWithSpecialCharacters() throws Exception {

    List<String> properties = Arrays.asList("first", "second");

    String colon = "00,\":\"";
    String comma = "01,\",\"";
    String dot = "02,\".\"";
    String underscore = "03,\"_\"";
    String and = "04,\"&\"";
    String slash = "05,\"/\"";
    String backslash = "06,\"\\\"";
    String dollar = "07,\"$\"";
    String paragraph = "08,\"§\"";
    String percent = "09,\"%\"";
    String space = "10,\" \"";
    String roundBrackets = "11,\"()\"";
    String squareBrackets = "12,\"[]\"";
    String curlyBrackets = "13,\"{}\"";
    String angleBrackets = "14,\"<>\"";
    String pipe = "15,\"|\"";
    String apostrophe = "16,\"'\"";
    String hash = "17,\"#\"";

    DataSet<String> data = getConfig().getExecutionEnvironment().fromElements(colon, comma, dot,
      underscore, and, slash, backslash, dollar, paragraph, percent, space, roundBrackets, squareBrackets,
      curlyBrackets, angleBrackets, pipe, apostrophe, hash);
    DataSet<EPGMVertex> result = data.flatMap(new CSVToVertex(new EPGMVertexFactory(), properties));

    List<EPGMVertex> vertex = new ArrayList<>();
    result.output(new LocalCollectionOutputFormat<>(vertex));

    getExecutionEnvironment().execute();

    assertEquals(18, vertex.size());

    for (EPGMVertex v : vertex) {
      assertNotNull(v.getProperties());
      switch (String.valueOf(v.getPropertyValue("first"))) {
      case "00":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals(":", v.getPropertyValue("second").getString());
        break;
      case "01":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals(",", v.getPropertyValue("second").getString());
        break;
      case "02":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals(".", v.getPropertyValue("second").getString());
        break;
      case "03":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("_", v.getPropertyValue("second").getString());
        break;
      case "04":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("&", v.getPropertyValue("second").getString());
        break;
      case "05":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("/", v.getPropertyValue("second").getString());
        break;
      case "06":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("\\", v.getPropertyValue("second").getString());
        break;
      case "07":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("$", v.getPropertyValue("second").getString());
        break;
      case "08":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("§", v.getPropertyValue("second").getString());
        break;
      case "09":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("%", v.getPropertyValue("second").getString());
        break;
      case "10":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals(" ", v.getPropertyValue("second").getString());
        break;
      case "11":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("()", v.getPropertyValue("second").getString());
        break;
      case "12":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("[]", v.getPropertyValue("second").getString());
        break;
      case "13":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("{}", v.getPropertyValue("second").getString());
        break;
      case "14":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("<>", v.getPropertyValue("second").getString());
        break;
      case "15":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("|", v.getPropertyValue("second").getString());
        break;
      case "16":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("'", v.getPropertyValue("second").getString());
        break;
      case "17":
        assertNotNull(v.getProperties());
        assertEquals(2, v.getProperties().size());
        assertEquals("#", v.getPropertyValue("second").getString());
        break;
      default:
        fail();
        break;
      }
    }
  }

  @Test
  public void parsePropertyType() throws Exception {

    List<String> properties = Arrays.asList("int", "String", "double", "boolean");

    DataSet<String> data = getConfig().getExecutionEnvironment().fromElements("101," +
      "\"I am a String!\",9.3,true");
    DataSet<EPGMVertex> result = data.flatMap(new CSVToVertex(new EPGMVertexFactory(), properties));

    List<EPGMVertex> vertex = new ArrayList<>();
    result.output(new LocalCollectionOutputFormat<>(vertex));

    getExecutionEnvironment().execute();

    for (EPGMVertex v : vertex) {
      assertEquals(101, Integer.parseInt(v.getPropertyValue("int").getString()));
      assertEquals("I am a String!", v.getPropertyValue("String").getString());
      assertEquals(9.3d, Double.parseDouble(v.getPropertyValue("double").getString()), 0.0);
      assertTrue(Boolean.parseBoolean(v.getPropertyValue("boolean").getString()));
    }
  }
}
