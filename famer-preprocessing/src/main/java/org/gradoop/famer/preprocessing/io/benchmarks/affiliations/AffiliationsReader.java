/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.benchmarks.affiliations;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.famer.preprocessing.io.benchmarks.BenchmarkSetReaderBase;
import org.gradoop.famer.preprocessing.io.common.GraphToGraphCollection;
import org.gradoop.flink.io.impl.graph.GraphDataSource;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.bool.False;

import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Class to read the Affiliations benchmark data.
 * {@see https://dbs.uni-leipzig.de/research/projects/object_matching/benchmark_datasets_for_entity_resolution}
 */
public class AffiliationsReader extends BenchmarkSetReaderBase<DataSet<Tuple2<Long, Long>>> {

  @Override
  public DataSet<Tuple2<Long, Long>> getPerfectMapping(String folderPath) {
    return getEnv().readCsvFile(folderPath + File.separator + "PerfectMapping.csv")
      .ignoreFirstLine()
      .parseQuotedStrings('"')
      .fieldDelimiter(",")
      .types(String.class, String.class)
      .map(stringTuple -> {
        long vertexId1 = Long.parseLong(stringTuple.f0.replace("\"", ""));
        long vertexId2 = Long.parseLong(stringTuple.f1.replace("\"", ""));
        return new Tuple2<>(vertexId1, vertexId2);
      }).returns(new TypeHint<Tuple2<Long, Long>>() { });
  }

  @Override
  public GraphCollection getBenchmarkDataAsGraphCollection(String folderPath) {
    DataSet<Tuple2<String, String>> affiliationEntries =
      getEnv().readCsvFile(folderPath + File.separator + "Affiliations.csv")
        .ignoreFirstLine()
        .parseQuotedStrings('"')
        .fieldDelimiter(",")
        .types(String.class, String.class);

    DataSet<ImportVertex<Long>> importVertices = affiliationEntries
      .map((MapFunction<Tuple2<String, String>, ImportVertex<Long>>) affiliationTuple -> {
        Map<String, Object> properties = new HashMap<>();
        properties.put("affiliationString", affiliationTuple.f1);
        properties.put(GRAPH_LABEL_PROPERTY, "Affiliations");
        long vertexId = Long.parseLong(affiliationTuple.f0.replace("\"", ""));
        return new ImportVertex<>(vertexId, "Affiliations", Properties.createFromMap(properties));
      }).returns(new TypeHint<ImportVertex<Long>>() { });

    DataSet<ImportEdge<Long>> importEdges = getEnv().fromElements(
      new ImportEdge<>(0L, 0L, 1L)).filter(new False<>());

    LogicalGraph affiliationGraph =
      new GraphDataSource<>(importVertices, importEdges, "id", getConfig()).getLogicalGraph();
    affiliationGraph = affiliationGraph.transformGraphHead((current, transformed) -> {
      current.setLabel("Affiliations");
      return current;
    });
    return GraphToGraphCollection.execute(Collections.singletonList(affiliationGraph));
  }
}
