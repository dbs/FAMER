/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.benchmarks.geographic;

import com.google.common.collect.Lists;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.codehaus.jettison.json.JSONArray;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.common.model.impl.properties.PropertyValue;
import org.gradoop.common.model.impl.properties.PropertyValueList;
import org.gradoop.famer.preprocessing.io.benchmarks.ExtBenchmarkSetReaderBase;
import org.gradoop.famer.preprocessing.io.common.GraphToGraphCollection;
import org.gradoop.flink.io.impl.graph.GraphDataSource;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.bool.False;
import org.gradoop.flink.model.impl.functions.tuple.Value1Of2;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * Class to read the Geographic Settlements benchmark data.
 * {@see https://dbs.uni-leipzig.de/research/projects/object_matching/benchmark_datasets_for_entity_resolution}
 */
public class GeographicSettlementsReader extends
  ExtBenchmarkSetReaderBase<DataSet<Tuple2<Long, Long>>, DataSet<List<Long>>> {

  /**
   * Converts the json element to a primitive type.
   *
   * @param parent The parent json element of the element should be converted.
   * @param key    The key of the element within the parent.
   * @return The primitive type of the given element.
   */
  private static Object getPrimitiveTypeOfJsonElement(JSONObject parent, String key) {
    try {
      return parent.getBoolean(key);
    } catch (JSONException e) {
      try {
        return parent.getInt(key);
      } catch (JSONException jsonException) {
        try {
          return parent.getDouble(key);
        } catch (JSONException exception) {
          return parent.optString(key);
        }
      }
    }
  }

  /**
   * Takes a json element and converts it ether to a primitive type or to an object.
   *
   * @param parent The parent json element of the element should be converted.
   * @param key    The key of the element within the parent.
   * @return The type of the given json element.
   * @throws IOException thrown if the given json element is an json array and the parsing to
   *                     PropertyValueList fails.
   */
  private static Object getTypeOfJsonElement(JSONObject parent, String key) throws IOException {
    try {
      JSONArray arrayElement = parent.getJSONArray(key);
      List<PropertyValue> array = new ArrayList<>();
      for (int i = 0; i < arrayElement.length(); i++) {
        array.add(PropertyValue.create(arrayElement.optString(i)));
      }
      PropertyValueList list = PropertyValueList.fromPropertyValues(array);

      return Lists.newArrayList(list);

    } catch (JSONException e) {
      try {
        JSONObject jsonElement = parent.getJSONObject(key);
        Map<String, Object> map = new HashMap<>();

        for (Iterator<?> it = jsonElement.keys(); it.hasNext();) {
          String s = (String) it.next();
          map.put(s, getTypeOfJsonElement(jsonElement, s));
        }
        return map;
      } catch (JSONException jsonException) { // not JSONObject
        return getPrimitiveTypeOfJsonElement(parent, key);
      }
    }
  }

  @Override
  public DataSet<Tuple2<Long, Long>> getPerfectMapping(String folderPath) {
    return getEnv().readTextFile(folderPath + File.separator + "PerfectMapping.json")
      .flatMap((FlatMapFunction<String, Tuple2<Long, Long>>) (line, out) -> {
        JSONObject jsonObject = new JSONObject(line);
        JSONArray cluster = jsonObject.getJSONObject("data").getJSONArray("clusteredVertices");
        for (int i = 0; i < cluster.length(); i++) {
          for (int j = i + 1; j < cluster.length(); j++) {
            out.collect(Tuple2.of(cluster.getLong(i), cluster.getLong(j)));
          }
        }
      }).returns(new TypeHint<Tuple2<Long, Long>>() { });
  }

  @Override
  public DataSet<List<Long>> getPerfectClustering(String folderPath) {
    return getEnv().readTextFile(folderPath + File.separator + "PerfectMapping.json")
      .flatMap((FlatMapFunction<String, List<Long>>) (line, out) -> {
        JSONObject jsonObject = new JSONObject(line);
        JSONArray cluster = jsonObject.getJSONObject("data").getJSONArray("clusteredVertices");
        List<Long> clusterIds = new ArrayList<>();
        for (int i = 0; i < cluster.length(); i++) {
          clusterIds.add(cluster.getLong(i));
        }
        out.collect(clusterIds);
      }).returns(new TypeHint<List<Long>>() { });
  }

  @Override
  public GraphCollection getBenchmarkDataAsGraphCollection(String folderPath) {
    DataSet<Tuple2<String, ImportVertex<Long>>> sourceVertexTuples =
      getEnv().readTextFile(folderPath + File.separator + "Settlements.json")
        .map((MapFunction<String, Tuple2<String, ImportVertex<Long>>>) line -> {
          JSONObject jsonObject = new JSONObject(line);

          Map<String, Object> properties = new HashMap<>();
          JSONObject data = jsonObject.getJSONObject("data");

          String sourceLabel = "";
          String ontology = data.getString("ontology");
          if (ontology.contains("dbpedia")) {
            sourceLabel = "dbpedia";
          } else if (ontology.contains("freebase")) {
            sourceLabel = "freebase";
          } else if (ontology.contains("nytimes")) {
            sourceLabel = "nytimes";
          } else if (ontology.contains("geonames")) {
            sourceLabel = "geonames";
          } else {
            sourceLabel = ontology;
          }
          properties.put(GRAPH_LABEL_PROPERTY, sourceLabel);

          for (Iterator<?> it = data.keys(); it.hasNext();) {
            String key = (String) it.next();
            properties.put(key, getTypeOfJsonElement(data, key));
          }

          long id = jsonObject.getLong("id");
          ImportVertex<Long> importVertex =
            new ImportVertex<>(id, "GeographicSettlements", Properties.createFromMap(properties));

          return Tuple2.of(sourceLabel, importVertex);
        }).returns(new TypeHint<Tuple2<String, ImportVertex<Long>>>() { });

    List<LogicalGraph> graphs = new ArrayList<>();

    graphs.add(createLogicalGraph(sourceVertexTuples.filter(t -> t.f0.equals("dbpedia")), "dbpedia"));
    graphs.add(createLogicalGraph(sourceVertexTuples.filter(t -> t.f0.equals("freebase")), "freebase"));
    graphs.add(createLogicalGraph(sourceVertexTuples.filter(t -> t.f0.equals("nytimes")), "nytimes"));
    graphs.add(createLogicalGraph(sourceVertexTuples.filter(t -> t.f0.equals("geonames")), "geonames"));

    return GraphToGraphCollection.execute(graphs);
  }

  /**
   * Create a logical graph fo the given vertices and source label. The graph contains empty edges.
   *
   * @param sourceVertexTuples Tuples holding the sourceLabel and the according vertices of the graph.
   * @param sourceLabel The label of the graph.
   *
   * @return A logical graph with the given vertices, label and empty edges.
   */
  private LogicalGraph createLogicalGraph(DataSet<Tuple2<String, ImportVertex<Long>>> sourceVertexTuples,
    String sourceLabel) {

    DataSet<ImportVertex<Long>> vertices = sourceVertexTuples.map(new Value1Of2<>());
    DataSet<ImportEdge<Long>> edges = getEnv().fromElements(
      new ImportEdge<>(0L, 0L, 1L)).filter(new False<>());

    LogicalGraph graph = new GraphDataSource<>(vertices, edges, "id", getConfig()).getLogicalGraph();
    graph = graph.transformGraphHead((current, transformed) -> {
      current.setLabel(sourceLabel);
      return current;
    });
    return graph;
  }
}
