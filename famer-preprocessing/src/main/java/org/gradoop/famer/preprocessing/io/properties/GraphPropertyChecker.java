/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.properties;

import org.gradoop.flink.model.impl.epgm.LogicalGraph;

/**
 * Class to check all vertices of a {@link LogicalGraph} for the mandatory <i>graphLabel</i> property.
 */
public class GraphPropertyChecker {

  /**
   * Property key for the graphLabel, which holds the origin source of a vertex as value.
   */
  public static final String GRAPH_LABEL_PROPERTY = "graphLabel";

  /**
   * Checks all vertices for the mandatory <i>graphLabel</i> property. The parameter for {@code overwrite} is
   * here set to {@code true}, so all vertices will have the property set to the value given in
   * {@code valueIfNotExists}. Overwrites the graph head label with this value, too.
   *
   * @param graph The logical graph to check the vertices
   * @param valueIfNotExists The new value for the graphLabel property
   *
   * @return The checked logical graph
   */
  public LogicalGraph checkLogicalGraphForGraphLabelProperty(LogicalGraph graph, String valueIfNotExists) {
    return checkLogicalGraphForGraphLabelProperty(graph, valueIfNotExists, true);
  }

  /**
   * Checks all vertices for the mandatory <i>graphLabel</i> property. The parameter for {@code overwrite}
   * determines if all vertices will have the property set to the value given in {@code valueIfNotExists}
   * or only vertices where the property not exists. Checks the graph head label - if empty, set it to the
   * value given in {@code valueIfNotExists} and overwrites it when {@code overwrite} is set to true.
   *
   * @param graph The logical graph to check the vertices
   * @param valueIfNotExists The new value for the graphLabel property
   * @param overwrite Whether or not to overwrite an already existing graphLabel property in a vertex
   *
   * @return The checked logical graph
   */
  public LogicalGraph checkLogicalGraphForGraphLabelProperty(LogicalGraph graph, String valueIfNotExists,
    boolean overwrite) {
    // set vertex properties
    graph = graph.transformVertices((current, transformed) -> {
      if (!current.hasProperty(GRAPH_LABEL_PROPERTY) || overwrite) {
        current.setProperty(GRAPH_LABEL_PROPERTY, valueIfNotExists);
      }
      return current;
    });
    // set graph head label
    graph = graph.transformGraphHead((current, transformed) -> {
      if (current.getLabel().isEmpty() || overwrite) {
        current.setLabel(valueIfNotExists);
      }
      return current;
    });

    return graph;
  }
}
