/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.generator;

import org.apache.flink.api.java.DataSet;
import org.gradoop.common.model.impl.pojo.EPGMVertex;

/**
 * Base interface to import vertices into a {@link DataSet} of {@link EPGMVertex}.
 */
public interface VertexGenerator {

  /**
   * Reads all lines of the file and converts each valid line to a {@link EPGMVertex}.
   *
   * @return DataSet of all imported vertices.
   */
  DataSet<EPGMVertex> generateVertices();
}
