/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.benchmarks;

import org.apache.flink.api.java.DataSet;
import org.gradoop.famer.preprocessing.io.ReaderBase;
import org.gradoop.flink.model.impl.epgm.GraphCollection;

/**
 * Abstract class all benchmark set readers, that provide data as {@link GraphCollection} and as
 * perfect mapping of type {@code T}, are inheriting from.
 *
 * @param <T> Return type for {@link #getPerfectMapping(String)}
 */
public abstract class BenchmarkSetReaderBase<T> extends ReaderBase {

  /**
   * Property key for the graphLabel, which holds the origin source of a vertex as value.
   */
  public static final String GRAPH_LABEL_PROPERTY = "graphLabel";

  /**
   * Reads the perfect mapping for the benchmark data and returns a {@code DataSet} with the the matching
   * pairs. The dataset is of generic type {@code T}.
   *
   * @param folderPath Path to the benchmark data folder
   *
   * @return A {@link DataSet} with the matching tuples
   */
  public abstract T getPerfectMapping(String folderPath);

  /**
   * Reads the benchmark set data and returns a {@link GraphCollection}, where each graph in the collection
   * contains the entities from one of the given sources.
   *
   * @param folderPath Path to the benchmark data folder
   *
   * @return A {@link GraphCollection} with the benchmark set data
   */
  public abstract GraphCollection getBenchmarkDataAsGraphCollection(String folderPath);
}
