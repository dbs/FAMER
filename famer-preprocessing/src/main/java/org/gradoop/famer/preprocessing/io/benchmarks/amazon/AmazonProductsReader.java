/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.benchmarks.amazon;

import org.apache.flink.api.common.functions.MapFunction;
import org.apache.flink.api.common.typeinfo.TypeHint;
import org.apache.flink.api.java.DataSet;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple5;
import org.gradoop.common.model.impl.properties.Properties;
import org.gradoop.famer.preprocessing.io.benchmarks.BenchmarkSetReaderBase;
import org.gradoop.famer.preprocessing.io.common.GraphToGraphCollection;
import org.gradoop.flink.io.impl.graph.GraphDataSource;
import org.gradoop.flink.io.impl.graph.tuples.ImportEdge;
import org.gradoop.flink.io.impl.graph.tuples.ImportVertex;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.model.impl.functions.bool.False;

import java.io.File;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Class to read the Amazon-Google products benchmark data.
 * {@see https://dbs.uni-leipzig.de/research/projects/object_matching/benchmark_datasets_for_entity_resolution}
 */
public class AmazonProductsReader extends BenchmarkSetReaderBase<DataSet<Tuple2<String, String>>> {

  @Override
  public DataSet<Tuple2<String, String>> getPerfectMapping(String folderPath) {
    return getEnv().readCsvFile(folderPath + File.separator + "PerfectMapping.csv")
      .ignoreFirstLine()
      .parseQuotedStrings('"')
      .fieldDelimiter(",")
      .types(String.class, String.class);
  }

  @Override
  public GraphCollection getBenchmarkDataAsGraphCollection(String folderPath) {
    List<LogicalGraph> graphs = new ArrayList<>();

    LogicalGraph amazonGraph =
      parseEntriesToLogicalGraph(folderPath  + File.separator + "Amazon.csv", "Amazon");
    if (amazonGraph != null) {
      graphs.add(amazonGraph);
    }

    LogicalGraph googleGraph =
      parseEntriesToLogicalGraph(folderPath + File.separator + "Google.csv", "Google");
    if (googleGraph != null) {
      graphs.add(googleGraph);
    }

    return GraphToGraphCollection.execute(graphs);
  }

  /**
   * Reads the file data and builds an edgeless {@link LogicalGraph} with all entries as vertices.
   *
   * @param filePath Path to the data file
   * @param graphName The name of the graph data
   *
   * @return {@link LogicalGraph} with all entries as vertices
   */
  private LogicalGraph parseEntriesToLogicalGraph(String filePath, String graphName) {
    DataSet<Tuple5<String, String, String, String, String>> entries = getEnv().readCsvFile(filePath)
      .ignoreFirstLine()
      .parseQuotedStrings('"')
      .fieldDelimiter(",")
      .types(String.class, String.class, String.class, String.class, String.class);

    DataSet<ImportVertex<String>> importVertices = entries.map(
      (MapFunction<Tuple5<String, String, String, String, String>, ImportVertex<String>>) tuple -> {
        Map<String, Object> properties = new HashMap<>();
        properties.put("title", tuple.f1);
        properties.put("description", tuple.f2);
        properties.put("manufacturer", tuple.f3);
        if (!tuple.f4.isEmpty()) {
          NumberFormat format = NumberFormat.getInstance(Locale.US);
          Number number = format.parse(tuple.f4.replaceAll("\\$", ""));
          properties.put("price", number.doubleValue());
        } else {
          properties.put("price", tuple.f4);
        }
        properties.put(GRAPH_LABEL_PROPERTY, graphName);
        return new ImportVertex<>(tuple.f0, graphName, Properties.createFromMap(properties));
      }).returns(new TypeHint<ImportVertex<String>>() { });

    DataSet<ImportEdge<String>> importEdges = getEnv().fromElements(
      new ImportEdge<>("0", "0", "1")).filter(new False<>());

    LogicalGraph graph =
      new GraphDataSource<>(importVertices, importEdges, "id", getConfig()).getLogicalGraph();
    graph = graph.transformGraphHead((current, transformed) -> {
      current.setLabel(graphName);
      return current;
    });
    return graph;
  }
}
