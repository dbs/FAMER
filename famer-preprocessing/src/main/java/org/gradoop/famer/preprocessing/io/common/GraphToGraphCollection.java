/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.common;

import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.util.List;

/**
 * Takes a list of {@link LogicalGraph}s as input and creates a {@link GraphCollection} of it.
 */
public class GraphToGraphCollection {

  /**
   * Transforms multiple {@link LogicalGraph}s into a {@link GraphCollection}.
   *
   * @param inputGraphs The list of the {@link LogicalGraph}s
   *
   * @return A {@link GraphCollection} of all input graphs.
   */
  public static GraphCollection execute(List<LogicalGraph> inputGraphs) {
    GraphCollection graphCollection = null;
    for (LogicalGraph logicalGraph : inputGraphs) {
      GraphCollection temp = logicalGraph.getConfig().getGraphCollectionFactory().fromGraph(logicalGraph);
      if (graphCollection == null) {
        graphCollection = temp;
      } else {
        graphCollection = graphCollection.union(temp);
      }
    }
    return graphCollection;
  }
}
