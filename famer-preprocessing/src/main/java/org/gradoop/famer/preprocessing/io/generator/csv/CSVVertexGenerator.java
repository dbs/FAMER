/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.generator.csv;

import org.apache.flink.api.java.DataSet;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.pojo.EPGMVertexFactory;
import org.gradoop.famer.preprocessing.io.generator.VertexGenerator;
import org.gradoop.flink.util.GradoopFlinkConfig;

import java.util.List;

/**
 * Class to generate a {@link DataSet} of {@link EPGMVertex} from a csv file. Each line corresponds to a
 * vertex, each token in a line corresponds to a property of the vertex. The charset conforms to UTF-8 and
 * the properties are separated by comma. The files must not contain a header line and the tokens must be
 * separated by comma.
 */
public class CSVVertexGenerator implements VertexGenerator {

  /**
   * The path the csv file is located
   */
  private String filePath;

  /**
   * The property names of the vertex.
   */
  private List<String> propertyNames;

  /**
   * Gradoop Flink configuration
   */
  private GradoopFlinkConfig gradoopFlinkConfig;

  /**
   * Creates a new VertexGenerator
   *
   * @param filePath The path to the csv file
   * @param propertyNames The property names
   * @param gradoopFlinkConfig Gradoop Flink configuration
   */
  public CSVVertexGenerator(String filePath, List<String> propertyNames,
    GradoopFlinkConfig gradoopFlinkConfig) {
    this.filePath = filePath;
    this.propertyNames = propertyNames;
    this.gradoopFlinkConfig = gradoopFlinkConfig;
  }

  @Override
  public DataSet<EPGMVertex> generateVertices() {
    DataSet<String> lines = gradoopFlinkConfig.getExecutionEnvironment().readTextFile(filePath);
    return lines.flatMap(new CSVToVertex(new EPGMVertexFactory(), propertyNames));
  }
}
