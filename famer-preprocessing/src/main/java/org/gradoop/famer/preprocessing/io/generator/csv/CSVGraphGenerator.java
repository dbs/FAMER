/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.generator.csv;

import org.apache.flink.api.java.DataSet;
import org.gradoop.common.model.impl.pojo.EPGMEdge;
import org.gradoop.common.model.impl.pojo.EPGMGraphHead;
import org.gradoop.common.model.impl.pojo.EPGMGraphHeadFactory;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.famer.preprocessing.io.generator.GraphGenerator;
import org.gradoop.famer.preprocessing.io.generator.VertexGenerator;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;
import org.gradoop.flink.util.GradoopFlinkConfig;

import java.util.List;

/**
 * Generates an edgeless graph from a csv file. Reads a csv file with the vertices of the graph. The files
 * must not contain a header line and the tokens must be separated by comma. The property names must be
 * passed as a list with the desired order.
 * <pre>
 * Example:
 * this line in a csv file:
 *    12345,"forum","databases",1995
 *
 * with this property names:
 *    {@code List<String>(){myId, description, name, year}}
 *
 * returns a {@link LogicalGraph} with this {@link EPGMVertex}:
 *    {id:GradoopId, label:"", properties:{myId:12345, description:"forum", name:"databases", year:1995}}
 * </pre>
 */
public class CSVGraphGenerator implements GraphGenerator {

  /**
   * The path to the csv vertex files
   */
  private String filePath;

  /**
   * The property names for a vertex.
   */
  private List<String> propertyNames;

  /**
   * Label of the graph
   */
  private String graphLabel;

  /**
   * Gradoop Flink configuration
   */
  private GradoopFlinkConfig gradoopFlinkConfig;

  /**
   * Constructor for the graph generator to create a {@link LogicalGraph}.
   *
   * @param filePath Path to the csv vertex file
   * @param graphLabel The label for the graph to generate
   * @param gradoopFlinkConfig Gradoop Flink configuration
   * @param propertyNames The names of the vertex properties
   */
  public CSVGraphGenerator(String filePath, String graphLabel, GradoopFlinkConfig gradoopFlinkConfig,
                           List<String> propertyNames) {
    this.filePath = filePath;
    this.graphLabel = graphLabel;
    this.gradoopFlinkConfig = gradoopFlinkConfig;
    this.propertyNames = propertyNames;
  }

  @Override
  public LogicalGraph generateGraph() {
    VertexGenerator vertexGenerator = new CSVVertexGenerator(filePath, propertyNames, gradoopFlinkConfig);

    EPGMGraphHead graphHead = new EPGMGraphHeadFactory().createGraphHead(graphLabel);
    DataSet<EPGMGraphHead> graphHeads = gradoopFlinkConfig.getExecutionEnvironment().fromElements(graphHead);

    DataSet<EPGMVertex> vertices = vertexGenerator.generateVertices();

    DataSet<EPGMEdge> edges = gradoopFlinkConfig.getLogicalGraphFactory().createEmptyGraph().getEdges();

    return gradoopFlinkConfig.getLogicalGraphFactory().fromDataSets(graphHeads, vertices, edges);
  }
}
