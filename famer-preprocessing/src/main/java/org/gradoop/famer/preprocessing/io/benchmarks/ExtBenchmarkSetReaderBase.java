/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.benchmarks;

import org.apache.flink.api.java.DataSet;
import org.gradoop.flink.model.impl.epgm.GraphCollection;

/**
 * Abstract class all benchmark set readers, that provide data as {@link GraphCollection}, as
 * perfect mapping of type {@code T1} and as perfect clustering of type {@code T2}, are inheriting from.
 *
 * @param <T1> Return type for {@link #getPerfectMapping(String)}
 * @param <T2> Return type for {@link #getPerfectClustering(String)}
 */
public abstract class ExtBenchmarkSetReaderBase<T1, T2> extends BenchmarkSetReaderBase<T1> {

  /**
   * Reads the perfect clustering for the benchmark data and returns a {@code DataSet<List>} with each entry
   * containing a list of all entity ids that belong to one cluster. The list is of generic type {@code T2}.
   *
   * @param folderPath Path to the benchmark data folder
   *
   * @return A {@link DataSet} with the perfect clustering
   */
  public abstract T2 getPerfectClustering(String folderPath);
}
