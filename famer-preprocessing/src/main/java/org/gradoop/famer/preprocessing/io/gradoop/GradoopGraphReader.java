/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.gradoop;

import org.gradoop.famer.preprocessing.io.ReaderBase;
import org.gradoop.flink.io.impl.csv.CSVDataSource;
import org.gradoop.flink.io.impl.edgelist.EdgeListDataSource;
import org.gradoop.flink.model.impl.epgm.GraphCollection;
import org.gradoop.flink.model.impl.epgm.LogicalGraph;

import java.io.IOException;

/**
 * Class for reading {@link LogicalGraph}s and {@link GraphCollection}s in Gradoop format.
 */
public class GradoopGraphReader extends ReaderBase {

  /**
   * Reads a {@link LogicalGraph} from Gradoops csv format files
   *
   * @param filePath The path to the graph data
   *
   * @return The {@link LogicalGraph}
   */
  public LogicalGraph readLogicalGraphFromCSV(String filePath) {
    CSVDataSource source = new CSVDataSource(filePath, getConfig());
    return source.getLogicalGraph();
  }

  /**
   * Reads a {@link GraphCollection} from Gradoops csv format files
   *
   * @param filePath The path to the graph data
   *
   * @return The {@link GraphCollection}
   */
  public GraphCollection readGraphCollectionFromCSV(String filePath) {
    CSVDataSource source = new CSVDataSource(filePath, getConfig());
    return source.getGraphCollection();
  }

  /**
   * Reads a {@link LogicalGraph} from edge list format files
   *
   * @param filePath The path to the graph data
   * @param tokenSeparator Id separator token
   *
   * @return The {@link LogicalGraph}
   */
  public LogicalGraph readLogicalGraphFromEdgeList(String filePath, String tokenSeparator)
    throws IOException {
    EdgeListDataSource source = new EdgeListDataSource(filePath, tokenSeparator, getConfig());
    return source.getLogicalGraph();
  }

  /**
   * Reads a {@link GraphCollection} from edge list format files
   *
   * @param filePath The path to the graph data
   * @param tokenSeparator Id separator token
   *
   * @return The {@link GraphCollection}
   */
  public GraphCollection readGraphCollectionFromEdgeList(String filePath, String tokenSeparator)
    throws IOException {
    EdgeListDataSource source = new EdgeListDataSource(filePath, tokenSeparator, getConfig());
    return source.getGraphCollection();
  }
}
