/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.properties.functions;

import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple4;
import org.apache.flink.configuration.Configuration;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;

import java.util.List;

/**
 * MapFunction for renaming of vertex properties
 */
public class RenamePropertiesMap extends RichMapFunction<EPGMVertex, EPGMVertex> {

  /**
   * Name for the broadcast set
   */
  public static final String RENAME_BROADCAST_SET = "renamePropertyNames";

  /**
   * Key for the graphLabel property
   */
  private final String graphLabelProperty;

  /**
   * A list of tuples with the information for property renaming
   */
  private List<Tuple4<String, String, String, Boolean>> propertyNames;

  /**
   * Creates an instance of RenamePropertiesMap
   *
   * @param graphLabelProperty Key for the graphLabel property
   */
  public RenamePropertiesMap(String graphLabelProperty) {
    this.graphLabelProperty = graphLabelProperty;
  }

  @Override
  public void open(Configuration parameters) throws Exception {
    super.open(parameters);
    propertyNames = getRuntimeContext().getBroadcastVariable(RENAME_BROADCAST_SET);
  }

  @Override
  public EPGMVertex map(EPGMVertex vertex) throws Exception {
    for (Tuple4<String, String, String, Boolean> propTuple : propertyNames) {
      // apply renaming if vertex is part of a single logical graph or if its graphLabel matches
      if (propTuple.f0.isEmpty() ||
        vertex.getPropertyValue(graphLabelProperty).getString().equals(propTuple.f0)) {
        if (vertex.hasProperty(propTuple.f1)) {
          PropertyValue propVal = vertex.getPropertyValue(propTuple.f1);
          vertex.setProperty(propTuple.f2, propVal);
          if (!propTuple.f3) {
            vertex.removeProperty(propTuple.f1);
          }
        }
      }
    }
    return vertex;
  }
}
