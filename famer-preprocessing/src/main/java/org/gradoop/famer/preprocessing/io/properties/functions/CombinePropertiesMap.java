/*
 * Copyright © 2016 - 2021 Leipzig University (Database Research Group)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.gradoop.famer.preprocessing.io.properties.functions;

import org.apache.flink.api.common.functions.RichMapFunction;
import org.apache.flink.api.java.tuple.Tuple2;
import org.apache.flink.api.java.tuple.Tuple3;
import org.apache.flink.configuration.Configuration;
import org.gradoop.common.model.impl.pojo.EPGMVertex;
import org.gradoop.common.model.impl.properties.PropertyValue;

import java.util.Arrays;
import java.util.List;

/**
 * MapFunction for combination of vertex properties
 */
public class CombinePropertiesMap extends RichMapFunction<EPGMVertex, EPGMVertex> {

  /**
   * Name for the broadcast set
   */
  public static final String COMBINE_BROADCAST_SET = "combinePropertyNames";

  /**
   * Key for the graphLabel property
   */
  private final String graphLabelProperty;

  /**
   * A list of tuples with the information for property combination
   */
  private List<Tuple3<String, String, List<Tuple2<String, Boolean>>>> propertyNameTuples;

  /**
   * Creates an instance of CombinePropertiesMap
   *
   * @param graphLabelProperty Key for the graphLabel property
   */
  public CombinePropertiesMap(String graphLabelProperty) {
    this.graphLabelProperty = graphLabelProperty;
  }

  @Override
  public void open(Configuration parameters) throws Exception {
    super.open(parameters);
    propertyNameTuples = getRuntimeContext().getBroadcastVariable(COMBINE_BROADCAST_SET);
  }

  @Override
  public EPGMVertex map(EPGMVertex vertex) throws Exception {
    for (Tuple3<String, String, List<Tuple2<String, Boolean>>> propertyNameTuple : propertyNameTuples) {
      // apply combination if vertex is part of a single logical graph or if its graphLabel matches
      if (propertyNameTuple.f0.isEmpty() ||
        vertex.getPropertyValue(graphLabelProperty).getString().equals(propertyNameTuple.f0)) {
        String combinedPropName = propertyNameTuple.f1;
        List<Tuple2<String, Boolean>> propertyNames = propertyNameTuple.f2;
        PropertyValue[] propValArray = new PropertyValue[propertyNames.size()];
        boolean missingPropVal = false;

        for (int i = 0; i < propertyNames.size(); i++) {
          Tuple2<String, Boolean> propTuple = propertyNames.get(i);
          if (vertex.hasProperty(propTuple.f0)) {
            propValArray[i] = vertex.getPropertyValue(propTuple.f0);
          } else {
            missingPropVal = true;
            break;
          }
        }

        if (!missingPropVal) {
          vertex.setProperty(combinedPropName, Arrays.asList(propValArray));
          propertyNames.forEach(t -> {
            if (!t.f1) {
              vertex.removeProperty(t.f0);
            }
          });
        }
      }
    }

    return vertex;
  }
}
